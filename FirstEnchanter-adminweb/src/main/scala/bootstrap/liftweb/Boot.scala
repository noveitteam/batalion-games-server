package bootstrap.liftweb

import net.liftweb.http.{Html5Properties, LiftRules, Req}
import net.liftweb.sitemap.{Menu, SiteMap}
import java.io.File
import com.typesafe.config.ConfigFactory
import akka.actor.{Props, ActorSystem}
import cz.noveit.games.cluster.{ClusterListener, ClusterMembersConsumer}
import net.liftweb.http
import akka.cluster.Cluster

/**
 * A class that's instantiated early and run.  It allows the application
 * to modify lift's environment
 */

object ActorSystemInjector {
  val config = ConfigFactory.parseFile(new File("webadmin.conf"))
  val system = ActorSystem("CardGameCluster", config)
}

class Boot {

  def boot {

    try{
      val system = ActorSystemInjector.system
      val consumer = system.actorOf(Props[ClusterMembersConsumer], "clusterConsumer")
      val clusterListener = system.actorOf(Props(classOf[ClusterListener], consumer))
    } catch {
      case t: Throwable => println(t)
    }
      // where to search snippet
    LiftRules.addToPackages("cz.noveit.games.cardgame.adminweb")

    // Build SiteMap
    def sitemap(): SiteMap = SiteMap(
      Menu.i("Home") / "index"
    )

    LiftRules.unloadHooks.append(() => {
      val cluster = Cluster.get(ActorSystemInjector.system)
      cluster.down(cluster.selfAddress)

      ActorSystemInjector.system.shutdown()
    })

    // Use HTML5 for rendering
    LiftRules.htmlProperties.default.set((r: Req) =>
      new Html5Properties(r.userAgent))
  }



}