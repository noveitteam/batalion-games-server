package cz.noveit.games.cardgame.adminweb.comet

import net.liftweb.http.CometActor

import net.liftweb.actor._
import bootstrap.liftweb.ActorSystemInjector
import cz.noveit.games.cluster.{MemberWithMetrics, MembersListing, RemoveCometActor, AddCometActor}
import akka.cluster.Cluster

/**
 * Created by Wlsek on 29.1.14.
 */



class Servers extends CometActor  {

    val consumer = ActorSystemInjector.system.actorSelection("/user/clusterConsumer")
    consumer ! AddCometActor(this)

    var members: List[MemberWithMetrics] = List()

    def render = ".servers" #> members.map(m => {
      <div class="server left">
        <div class="icon"><img src={if(m.state != "Up"){"/images/server_down.png"} else {"/images/server_up.png"}} /></div>
        <div class="address">{m.address.split("@").last}</div>
        <div class="role">{m.roles.map(role => role.toUpperCase)}</div>
        <div class={if(m.state != "Up"){"state orange"} else {"state green"}}>{m.state}</div>
        <div class="usage">{"Usage: " + ((m.usage * 100).toInt.toString + "%")}</div>

        {
          if(m.roles.contains("gameworker")){
            <div class="users">{"Users: " + m.users}</div>
            <div class="games">{"Games: " + m.games}</div>
          }
        }
      </div>
    })

    override def lowPriority = {
      case MembersListing(list) => {
          members = list.sortWith((m1: MemberWithMetrics, m2: MemberWithMetrics) => {(m1.address compareToIgnoreCase m2.address) < 0})
          reRender(false)
      }
    }

    override def localShutdown = {
      consumer ! RemoveCometActor(this)
    }
}
