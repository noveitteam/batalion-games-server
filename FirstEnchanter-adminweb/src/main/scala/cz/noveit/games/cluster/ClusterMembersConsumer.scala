package cz.noveit.games.cluster

import akka.actor.{Actor, ActorLogging}
import cz.noveit.games.cluster.balancer.{Metrics, RemoveMember, AddMember, MembersSnapshot}
import net.liftweb.http.CometActor
import akka.cluster.StandardMetrics.HeapMemory
import akka.cluster.Member

/**
 * Created by Wlsek on 30.1.14.
 */



case class AddCometActor(cometActor: CometActor)
case class RemoveCometActor(cometActor: CometActor)
case class MemberWithMetrics(address: String, state: String, usage: Float, users: Int, games: Int, roles: List[String])
case class MembersListing(members: List[MemberWithMetrics])

class ClusterMembersConsumer extends Actor with ActorLogging {

     var cometActorRegistry: List[CometActor] = List()
     var members: List[MemberWithMetrics] = List()

     def receive = {
       case AddCometActor(actor) =>
         cometActorRegistry = actor :: cometActorRegistry
         actor ! MembersListing(members)

       case RemoveCometActor(actor) =>
         cometActorRegistry = cometActorRegistry.filterNot(c => c.equals(actor))

       case AddMember(member) =>
         actualizeMember(member)
         sendToListeners

       case RemoveMember(member) =>
         println(member.status.toString)
         actualizeMember(member)
         sendToListeners

       case Metrics(metrics) =>
         metrics.foreach(m => {

             m match {
               case HeapMemory(address, timestamp, used, committed, max) =>

                 members.filter(n => n.address.equals(m.address.toString)).map(mwm => {
                   members = members.filterNot(n => n.address.equals(m.address.toString))
                   members = MemberWithMetrics(
                     mwm.address,
                     mwm.state,
                     max.map(maxMem => {
                       (maxMem - used) /  maxMem.toFloat
                     }).getOrElse({
                       ((used + committed) - used) / (used + committed).toFloat
                     }),
                     mwm.users,
                     mwm.games,
                     mwm.roles
                   ) :: members
                 })
             }
           })
         sendToListeners

       case MembersSnapshot(list) =>
         list.map(member =>{
             actualizeMember(member)
         })
         sendToListeners
     }

  def actualizeMember(member: Member) = {
    println("actualizating member")
    members = members.filter(n => n.address.equals(member.address.toString)).headOption.map(mwm => {
      members = members.filterNot(n => n.address.toString.equals(member.address.toString))
      MemberWithMetrics(member.address.toString, member.status.toString, mwm.usage, mwm.users, mwm.games, member.roles.toList)
    }).getOrElse({
      MemberWithMetrics(member.address.toString, member.status.toString, 0,0,0, member.roles.toList)
    }) :: members
    println("members: " +members.size)
  }

  def sendToListeners = {
     cometActorRegistry.map(comet => comet ! MembersListing(members))
  }

}
