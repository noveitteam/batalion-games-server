import akka.actor.{ActorLogging, Props, ActorSystem}
import akka.event.LogSource
import com.google.code.chatterbotapi.{ChatterBotType, ChatterBotFactory}
import com.mongodb.ServerAddress
import com.typesafe.config.ConfigFactory
import cz.noveit.database.{DatabaseControllerSettings, DatabaseController}
import cz.noveit.games.cardgame.engine.bots.{BotPlayerFactory, GetBot, BotPlayerHolder}
import cz.noveit.games.cardgame.engine.game._
import cz.noveit.games.cardgame.engine.game.MatchQuerySetup
import cz.noveit.games.cardgame.engine.game.helpers.CardSwappingRequirements
import cz.noveit.games.cardgame.engine.game.session._
import cz.noveit.games.cardgame.engine.player.{PlayerPlaysWithActiveDeck, PlayerIsTryingToFindGame}
import cz.noveit.games.cardgame.logging.NoActorLogging
import cz.noveit.games.cardgame.WorldServices
import scala.collection.immutable.HashMap
import scala.concurrent.Await
import scala.concurrent.duration._

import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import scala.concurrent.Await

/**
 * Created by Wlsek on 26.6.14.
 */
object TestAi extends App with NoActorLogging {


  override def main(args: Array[String]): Unit = {
    if (args contains ("all")) {
      /* try {
var AS: Option[ActorSystem] = None
  try {

    AS = Some(ActorSystem("FirstEnchanter"))

    AS.map(actorSystem => {
      //  println(actorSystem.settings.config.getList("akka.event-handlers"))
      //val log = akka.event.Logging(actorSystem, this)

      //Database creation

      debug("Creating database")

      val databaseParameters: HashMap[String, String] = HashMap(
        "databaseName" -> "FirstEnchanter",
        "avatarVersionCollection" -> "avatarsVersion",
        "deckIconVersionCollection" -> "deckIconsVersion",
        "cardSkinVersionCollection" -> "cardSkinsVersion",
        "userCollection" -> "users",
        "userUnlockedCollection" -> "usersUnlocked",
        "cardsCollection" -> "cards",
        "decksCollection" -> "decks",
        "userHasCardsCollection" -> "userHasCards",
        "friendshipsCollection" -> "friendships",
        "statCollection" -> "stats",
        "matchCollection" -> "matches",
        "tokensCollection" -> "tokens",
        "newsCollection" -> "news",
        "scoreBoardCollection" -> "scoreBoards",
        "transactionsCollection" -> "transactions",
        "conversationsCollection" -> "conversations",
        "conversationsMembershipCollection" -> "conversationMemberships",
        "conversationsInvitationCollection" -> "conversationInvitations",
        "conversationsBansCollection" -> "conversationBans",
        "botStatisticCollection" -> "botStatistic",
        "botDetailsCollection" -> "botDetails",
        "storeItemsCollection" -> "storeItems",
        "unlockAblesCollection" -> "unlockAbles",
        "unlockAblesHistoryCollection" -> "unlockAblesHistory",
        "codesCollection" -> "codes",
        "codesRedeemedCollection" -> "codesRedeemed"
      )

      val databaseController = actorSystem actorOf(
        Props(
          classOf[DatabaseController],
          DatabaseControllerSettings(List(new ServerAddress("127.0.0.1")))
        ),
        WorldServices.databaseService
        )

      actorSystem actorOf (Props(
        classOf[MatchResultController]
      ), WorldServices.matchResultController)

      val ms = MatchSetup(HashMap[String, FiniteDuration](
        "playersStartRoll" -> 60.seconds,
        "attackerPlays" -> 60.seconds,
        "defenderCounterPlays" -> 60.seconds,
        "defenderDefends" -> 60.seconds,
        "attackerAttacks" -> 60.seconds,
        "playersRoll" -> 60.seconds,
        "animation" -> 60.seconds
      ), 2, 8,8, "1", 20,  CardSwappingRequirements(2,1))

      //Bot creation
      debug("Creating bots")
      val botFactory = actorSystem actorOf (Props(classOf[BotPlayerFactory], databaseParameters, ms))

      implicit val timeout = Timeout(15 seconds)

      val future = botFactory ? GetBot(1500)
      val botPlayer = Await.result(future, timeout.duration).asInstanceOf[BotPlayerHolder]

      var future2 = botFactory ? GetBot(1500)

      debug("Bots created, polishing them.")

      val botPlayer2 = {
        var tBot = ""
        var tBotHolder: BotPlayerHolder = null
        while (tBot == "" || tBot == botPlayer.name) {
          log.debug("Name for bot2 taken or inappropriate, choosing another.")
          future2 = botFactory ? GetBot(1500)
          tBotHolder = Await.result(future2, timeout.duration).asInstanceOf[BotPlayerHolder]
          tBot = tBotHolder.name
        }

        tBotHolder
      }

      debug("Creating startup setup.")

      val playerPositions = StartingRollPosition(botPlayer.name, PLAYER_ROLL_POSITION_DEFENDER) ::
        StartingRollPosition(botPlayer2.name, PLAYER_ROLL_POSITION_ATTACKER) ::
        Nil

      val bots = PlayerIsTryingToFindGame(botPlayer.name, "botTeam1", botPlayer.avatarId, 1500, 0, botPlayer.challengeId, botPlayer.handler) ::
        PlayerIsTryingToFindGame(botPlayer2.name, "botTeam2", botPlayer2.avatarId, 1500, 0, botPlayer2.challengeId, botPlayer2.handler) ::
        Nil

      val decs = PlayerPlaysWithActiveDeck(botPlayer.name, botPlayer.deckId) ::
        PlayerPlaysWithActiveDeck(botPlayer2.name, botPlayer2.deckId) ::
        Nil

      //Match creation
      log.debug("Creating match")
      val matchCreator = actorSystem.actorOf(Props[MatchCreator])
      val startSetup = MatchQuerySetup(bots, None, MatchGameModePractice, MatchGameEnemyModePVE, MatchTeamModeOneVsOne, matchCreator)

      actorSystem.actorOf(Props(classOf[Match], startSetup, ms, playerPositions, decs))

    })
  } catch {
    case e: Exception =>
      println("Error while starting ai test: " + e.toString)
  } finally {
    Console.readLine()
    AS.map(as => as.shutdown())
  }
}
}
}*/
    }

    if(args contains("chat")) {
      val factory = new ChatterBotFactory();
      val bot1 = factory.create(ChatterBotType.CLEVERBOT);
      val bot1session = bot1.createSession();

      println("GL & HF!")
      var line = readLine()
      while(line.toLowerCase != "bye") {
        println(bot1session.think(line))
        line = readLine()
      }
    }
  }
}
