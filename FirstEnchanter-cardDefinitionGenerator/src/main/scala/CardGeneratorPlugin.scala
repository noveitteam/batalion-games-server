import java.io.{FileReader, BufferedReader, File}

import com.mongodb.casbah.Imports._
import cz.noveit.games.cardgame.adapters.{CardTargetingType, Description, CardPrice, Card}
import cz.noveit.games.cardgame.engine.card.CardHolder

/**
 * Created by arnostkuchar on 26.09.14.
 */


object CardGeneratorPlugin extends App {
  override  def main(args: Array[String]): Unit = {
    val connection = MongoConnection("localhost")
    val database = connection("FirstEnchanter")
    val cards = database("cards").find().toList

    println("I will generate " + cards.size.toString + " cards, continue? [y]es/[n]o")

    var value = choose
    while(value.isEmpty) {
      println(".....continue? [y]es/[n]o")
      value = choose
    }

    if(value.get) {
      println("File " + printToFile(new File("FirstEnchanter.cards"))(p => {
        p.println("{\"cards\" : [")
        cards.foreach(card =>  {
          if(cards.head != card) {
            p.println( "," + card.toString)
          } else {
            p.println(card.toString)
          }

        })
        p.println("]}")
      }).getAbsolutePath + " created.")

    }
  }

  def choose: Option[Boolean] = {
    var value = readLine().toLowerCase

    if(
      value == "n" || value == "no"
    ) {
      Some(false)
    } else if(value == "y" || value == "yes") {
      Some(true)
    } else {
      None
    }
  }

  def printToFile(f: java.io.File)(op: java.io.PrintWriter => Unit): java.io.File = {
    val p = new java.io.PrintWriter(f)
    try { op(p) } finally { p.close() }
    f
  }

}
