package cz.bataliongames.firstenchanter

import akka.actor._
import cz.bataliongames.firstenchanter.websocketclient.WebSocketClientContext

/**
 * Created by arnostkuchar on 15.09.14.
 */

class ApplicationContext {
 var playerName: String = ""
 var webContext: Option[WebSocketClientContext] = None
}

case object StartApplicationInstance
case class StopApplicationInstance(appContext: ApplicationContext)

class ApplicationController extends Actor with ActorLogging {

  private var controllers: List[Pair[ApplicationContext, ActorRef]] = List()

  def receive = {
    case StartApplicationInstance =>
      val aContext = new ApplicationContext
      val actor =  context.actorOf(Props(classOf[ApplicationInstanceController], aContext))
      context.watch(actor)
      controllers = aContext -> actor :: controllers

    case StopApplicationInstance(instance) =>
      controllers.find(c => c._1 == instance).map(found => found._2 ! Kill)
      controllers = controllers.filterNot(f => f._1 == instance)

    case Terminated(actor) =>
      controllers = controllers.filterNot(c => c._2 == actor)
      if(controllers.size == 0) {
        context.system.shutdown()
        System.exit(0)
      }
  }
}

