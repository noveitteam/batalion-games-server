package cz.bataliongames.firstenchanter

import java.net.URI

import akka.actor.{Props, ActorRef, Actor, ActorLogging}
import cz.bataliongames.firstenchanter.form.actors._
import cz.bataliongames.firstenchanter.form.model.PlayerControllType
import cz.bataliongames.firstenchanter.form.services.FormServices
import cz.bataliongames.firstenchanter.websocketclient._
import cz.noveit.proto.firstenchanter.FirstEnchanterLightWeightServerLobby._
import cz.noveit.proto.serialization.{MessageSerializer, MessageEvent}
import org.bouncycastle.util.encoders.Base64
import scala.concurrent.ExecutionContext
import ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.util.{Success, Failure}

/**
 * Created by arnostkuchar on 15.09.14.
 */

class ApplicationInstanceController(val appContext: ApplicationContext) extends Actor with ActorLogging with MessageSerializer {
  val lightWeightServerLobbyModule = "FirstEnchanterLightWeightServerLobby"
  val matchModule = "FirstEnchanterMatch"
  val conversationModule = "FirstEnchanterConversation"

  val configDispatcher = context.actorSelection("/user/" + FormServices.ConfigDispatcher.toString)

  val worldConfig = context.system.settings.config.getConfig("world")
  val bindOnPort = worldConfig.getConfig("web-socket-server").getInt("port")
  val address = worldConfig.getConfig("web-socket-server").getString("address")
  val webSocketClient = context.actorSelection("/user/" + FormServices.WebSocketClient.toString)

  webSocketClient ! ConnectToServerUri(new URI("ws://" + address + ":" + bindOnPort.toString))

  private var roomsActor: Option[ActorRef] = None
  private var playerSelectionActor: Option[ActorRef] = None
  private var roomActor: Option[ActorRef] = None
  private var matchActor: Option[ActorRef] = None
  private var connectAutomaticallyToRoom = ""
  private var presentingRoom = ""

  private var websocketContext: Option[WebSocketClientContext] = None

  def receive = {
    case e: MessageEvent =>

      log.debug("Accepted MessageEvent by AppInstanceController.")

      e.module match {
        case `matchModule` | `conversationModule`  =>
          log.debug("Accepted Match module message forwarding it to match acotr: " + matchActor + "| Message: " + e.message)
          matchActor.map(a => a ! e)

        case `lightWeightServerLobbyModule` =>
          e.message match {
            case msg: RoomList =>
              log.info("Room list came: " + msg.toString)
              if (connectAutomaticallyToRoom == "") {
                if (playerSelectionActor.isEmpty && matchActor.isEmpty && roomActor.isEmpty && roomsActor.isEmpty) {
                  println("showing rooms")
                  configDispatcher.resolveOne(3 seconds).onComplete {
                    case Success(value) =>
                      roomsActor = Some(context.actorOf(Props(classOf[RoomsActor], value, self, appContext)))
                      roomsActor.map(a => a ! msg)
                    case _ =>
                  }
                }  else if (playerSelectionActor.isEmpty && matchActor.isEmpty && roomActor.isEmpty && roomsActor.isDefined) {
                roomsActor.map(a => a ! msg)
                }

              } else if (msg.getRoomsList.contains(connectAutomaticallyToRoom)) {
                println("showing room")
                configDispatcher.resolveOne(3 seconds).onComplete {
                  case Success(value) =>
                    roomActor = Some(context.actorOf(Props(classOf[RoomActor], value, self, appContext)))
                    websocketContext.map(ctx => ctx.send(serializeToString(MessageEvent(
                      JoinRoom.newBuilder().setRoomName(connectAutomaticallyToRoom).setPlayerName(appContext.playerName).build(),
                      lightWeightServerLobbyModule))))
                    connectAutomaticallyToRoom = ""
            //        roomA.map(a => a ! msg)
                  case _ =>
                    connectAutomaticallyToRoom = ""
                }
              } else {
                log.error("Unknown state!!")
              }

            case msg: JoinRoomSuccessful =>
              presentingRoom = msg.getRoomName()
              roomActor.map(ra => ra ! msg)

            case msg: JoinRoomFailed =>
              log.error("Error joining room!!")
              roomActor.map(ra => ra ! CloseForm)
              roomActor = None
              websocketContext.map(ctx => ctx.send(serializeToString(MessageEvent(GetRoomList.newBuilder().build(), lightWeightServerLobbyModule))))

            case msg: PlayerJoinedRoom =>
              roomActor.map(ra => ra ! msg)

            case msg: PlayerChangedTeamInRoom =>
              roomActor.map(ra => ra ! msg)

            case msg: PlayerLeftRoom =>
              if(appContext.playerName == msg.getName) {
                roomActor.map(ra => ra ! CloseForm)
                roomActor = None
                websocketContext.map(ctx => ctx.send(serializeToString(MessageEvent(GetRoomList.newBuilder().build(), lightWeightServerLobbyModule))))
              } else {
                roomActor.map(ra => ra ! msg)
              }

            case msg: RemoveRoomFailed =>
              roomActor.map(ra => ra ! msg)

            case msg: RemovedRoom =>
              if(msg.getName == presentingRoom) {
                roomActor.map(ra => ra ! CloseForm)
                roomActor = None
                websocketContext.map(ctx => ctx.send(serializeToString(MessageEvent(GetRoomList.newBuilder().build(), lightWeightServerLobbyModule))))
              }


            case msg: CreateRoomSuccess =>
              println(" Creation success")

            // websocketContext.map(ctx => ctx.send(serializeToString(MessageEvent(JoinRoom.newBuilder().setPlayerName().build(), lightWeightServerLobbyModule))))
            case msg: CreateRoomFailed =>
              println(" Creation failed")
              websocketContext.map(ctx => ctx.send(serializeToString(MessageEvent(GetRoomList.newBuilder().build(), lightWeightServerLobbyModule))))

            case msg: GameStarted =>
              roomActor.map(a => a ! CloseForm)
              roomActor = None

              configDispatcher.resolveOne(3 seconds).onComplete {
                case Success(value) =>
                  matchActor = Some(context.actorOf(Props(classOf[TableActor], value, self, appContext)))
                case _ =>
              }

            case msg: GameStartFailed =>
               roomActor.map(ra => ra ! msg)
          }

        case _ =>
          log.error("Unknown module: " + e.module)
      }

    case CloseFormInteraction =>
      if (roomsActor.exists(a => a == sender)) {
        println("Closing whole application")
        sender ! CloseForm
        websocketContext.map(ctx => ctx.disconnect())
        context.stop(self)
      } else if(roomActor.exists(a => a == sender)){
        websocketContext.map(ctx => ctx.send(serializeToString(MessageEvent(LeaveRoom.newBuilder().build(), lightWeightServerLobbyModule))))
      } else {
        println("Returning to root form")
        roomActor.map(a => a ! CloseForm)
        roomActor = None

        playerSelectionActor.map(a => a ! CloseForm)
        playerSelectionActor = None

        matchActor.map(a => a ! CloseForm)
        matchActor = None

        websocketContext.map(ctx => ctx.send(serializeToString(MessageEvent(GetRoomList.newBuilder().build(), lightWeightServerLobbyModule))))
      }

    case JoinRoomInteraction(name, uName) =>
      appContext.playerName = uName
      sender ! CloseForm
      configDispatcher.resolveOne(3 seconds).onComplete {
        case Success(value) =>
          roomsActor.map(a => context.stop(a))
          roomsActor = None
          roomActor = Some(context.actorOf(Props(classOf[RoomActor], value, self, appContext)))
          websocketContext.map(ctx => ctx.send(serializeToString(MessageEvent(
            JoinRoom.newBuilder().setRoomName(name).setPlayerName(appContext.playerName).build(),
            lightWeightServerLobbyModule))))
        //        roomA.map(a => a ! msg)
        case _ =>
      }


    case CreateRoomInteraction(name, uName) =>
      connectAutomaticallyToRoom = name
      appContext.playerName = uName
      sender ! CloseForm

      configDispatcher.resolveOne(3 seconds).onComplete {
        case Success(value) =>
          roomsActor.map(a => context.stop(a))
          roomsActor = None
          playerSelectionActor = Some(context.actorOf(Props(classOf[PlayerSelectionActor], value, self, name)))
        case _ =>
      }

    case msg: SelectionCreateInteraction =>
      sender ! CloseForm
      playerSelectionActor.map(a => context.stop(a))
      playerSelectionActor = None
      websocketContext.map(ctx => ctx.send(
        serializeToString(MessageEvent(
        {
          val builder = CreateRoom.newBuilder()
          builder.setName(msg.name)
          msg.position1.map(p => builder.setPosition1(PlayerControllType.toProto(p)))
          msg.position2.map(p => builder.setPosition2(PlayerControllType.toProto(p)))
          msg.position3.map(p => builder.setPosition3(PlayerControllType.toProto(p)))
          msg.position4.map(p => builder.setPosition4(PlayerControllType.toProto(p)))
          builder.build()
        }
        , lightWeightServerLobbyModule))
      ))

    case msg: ChangeTeamInteraction =>
      websocketContext.map(ctx =>
        ctx.send(serializeToString(MessageEvent(PlayerChangeTeamInRoom.newBuilder().setTeam(msg.team).build(), lightWeightServerLobbyModule)))
      )

    case StartGameInteraction =>
      websocketContext.map(ctx =>
        ctx.send(serializeToString(MessageEvent(GameStart.newBuilder().build(), lightWeightServerLobbyModule)))
      )

    case CannotConnectToServerUri(uri) =>
      log.error("Cannot connect to: " + uri.toString)

    case ConnectedToServerUri(uri, ctx) =>
      log.info("Connected to: " + uri.toString)
      websocketContext = Some(ctx)
      appContext.webContext = websocketContext
      ctx.send(serializeToString(MessageEvent(GetRoomList.newBuilder().build(), lightWeightServerLobbyModule)))

    case ClientContextDestroyed(ctx) =>
      log.error("Disconnected")
  }


}


