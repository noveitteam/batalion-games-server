package cz.bataliongames.firstenchanter

import javafx.embed.swing.JFXPanel
import javafx.{fxml => jfxf, scene => jfxs}

import akka.actor.{ActorSystem, Props}
import cz.bataliongames.firstenchanter.form.actors.TableActor
import cz.bataliongames.firstenchanter.form.services.{ConfigDispatcher, FormServices}
import cz.bataliongames.firstenchanter.websocketclient.WebSocketClient
import cz.noveit.games.cardgame.logging.NoActorLogging

/**
 * Created by Wlsek on 26.6.14.
 */
object TestGame extends App with NoActorLogging {
  override def main(args: Array[String]) = {
    val as = ActorSystem("Test-Game-Application")
    as.actorOf( Props[ConfigDispatcher], FormServices.ConfigDispatcher.toString)
    as.actorOf(Props[WebSocketClient], FormServices.WebSocketClient.toString)
    val controller = as.actorOf(Props[ApplicationController],  FormServices.ApplicationController.toString)
    controller! StartApplicationInstance
    //controller! StartApplicationInstance
    new JFXPanel()
  }
}
