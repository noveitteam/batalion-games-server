package cz.bataliongames.firstenchanter.form.actors

import java.io.IOException
import java.util.Properties
import javafx.application.Platform
import javafx.event.EventHandler
import javafx.stage.WindowEvent
import cz.bataliongames.firstenchanter.form.controllers.{FormController, TableController}

import scalafx.scene.Scene
import scalafx.Includes._
import scalafx.stage.{StageStyle, Stage}
import javafx.{fxml => jfxf}
import javafx.{scene => jfxs}
import akka.actor.{ActorRef, Actor, ActorLogging}


/**
 * Created by arnostkuchar on 15.09.14.
 */

case object HideForm
case object CloseForm
case object CloseFormInteraction

trait FormActor extends Actor{
  val fxmlFile: String
  val configDispatcher: ActorRef
  val applicationInstanceController: ActorRef
  var presentedForm: FormController = _

  def createForm[T](props: Properties) = {
    Platform.runLater(new Runnable {
      override def run(): Unit = {
        println(getClass.getResource("."))
        val resource = getClass.getResource("/fxml/" + fxmlFile)
        if (resource == null) {
          throw new IOException("Cannot load resource: " + fxmlFile)
        }

        val loader = new jfxf.FXMLLoader(resource)
        val formController: jfxs.Parent = loader.load().asInstanceOf[jfxs.Parent]

        val stage = new Stage {
          scene = new Scene(formController)
        }

        stage.initStyle(StageStyle.UNDECORATED)

        stage.show()
        presentedForm = loader.getController[T].asInstanceOf[FormController]
        presentedForm.postInitialize(self, props)
      }
    })
  }

}
