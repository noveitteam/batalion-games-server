package cz.bataliongames.firstenchanter.form.actors

import java.io.IOException
import java.util.Properties
import javafx.application.Platform

import cz.bataliongames.firstenchanter.form.actors.CloseFormInteraction

import scalafx.scene.Scene
import scalafx.Includes._
import scalafx.stage.{StageStyle, Stage}
import javafx.{fxml => jfxf}
import javafx.{scene => jfxs}
import akka.actor.{Kill, ActorLogging, Actor, ActorRef}
import cz.bataliongames.firstenchanter.form.controllers.{RoomController, PlayerSelectionController}
import cz.bataliongames.firstenchanter.form.model.PlayerControllType
import cz.bataliongames.firstenchanter.form.services.{SaveConfigForForm, NoConfiguration, LoadedConfiguration, LoadConfigForForm}

/**
 * Created by Wlsek on 3.9.2014.
 */

trait PlayerSelectionInteraction
case class SelectionCreateInteraction(
                                       name: String,
                                       position1: Option[PlayerControllType.Value],
                                       position2: Option[PlayerControllType.Value],
                                       position3: Option[PlayerControllType.Value],
                                       position4: Option[PlayerControllType.Value]
                                       ) extends PlayerSelectionInteraction

class PlayerSelectionActor(val configDispatcher: ActorRef, val applicationInstanceController: ActorRef, roomName: String)  extends FormActor with ActorLogging  {
  val fxmlFile = "PlayerSelection.fxml"
  val formId = "PlayerSelection"

  configDispatcher ! LoadConfigForForm(formId)

  def receive = {
    case NoConfiguration(id) =>
      val prop = new Properties()
      val default = PlayerControllType.values.map(v => v.toString).head
      prop.setProperty("playerSelect1v1a", default)
      prop.setProperty("playerSelect1v1b", default)
      prop.setProperty("playerSelect2v2a", default)
      prop.setProperty("playerSelect2v2b", default)
      prop.setProperty("playerSelect2v2c", default)
      prop.setProperty("playerSelect2v2d", default)
      prop.setProperty("positionX", 0.toString)
      prop.setProperty("positionY", 0.toString)
      configDispatcher ! SaveConfigForForm(formId, prop)
      createForm[PlayerSelectionController](prop)

    case LoadedConfiguration(id, config) =>
      createForm[PlayerSelectionController](config)
      Platform.runLater(new Runnable {
        override def run(): Unit = {
          presentedForm.asInstanceOf[PlayerSelectionController].roomNameLabel.setText(roomName)
        }
      })

    case SaveConfigForForm(s, props) =>
      configDispatcher ! SaveConfigForForm(formId, props)

    case CloseFormInteraction =>
      applicationInstanceController ! CloseFormInteraction

    case msg: SelectionCreateInteraction => applicationInstanceController ! msg
    case CloseForm => presentedForm.close
    case HideForm => presentedForm.hide
  }

}
