package cz.bataliongames.firstenchanter.form.actors

import java.util.Properties
import javafx.application.Platform

import akka.actor.{ActorLogging, ActorRef}
import cz.bataliongames.firstenchanter.form.controllers.RoomController
import cz.bataliongames.firstenchanter.form.model.PlayerControllType
import cz.bataliongames.firstenchanter.{ApplicationContext, ApplicationController}
import cz.bataliongames.firstenchanter.form.actors.CloseFormInteraction
import cz.noveit.proto.firstenchanter.FirstEnchanterLightWeightServerLobby._

import scala.collection.JavaConverters._

/**
 * Created by arnostkuchar on 15.09.14.
 */

trait RoomInteraction

case class ChangeTeamInteraction(team: String) extends RoomInteraction

case object StartGameInteraction extends RoomInteraction

class RoomActor(val configDispatcher: ActorRef, val applicationInstanceController: ActorRef, appContext: ApplicationContext) extends FormActor with ActorLogging {
  val fxmlFile = "Room.fxml"
  val formId = "Room"
  var team = ""
  var allTeams: List[String] = List()
  var possibleTeamToChange = ""
  var changingTeam = ""

  val properties = new Properties()
  createForm[RoomActor](properties)

  def receive = {
    case msg: JoinRoomSuccessful =>
      Platform.runLater(new Runnable {
        override def run(): Unit = {
          val c = presentedForm.asInstanceOf[RoomController]
          c.presentPositions(
            if (msg.hasRoomPosition1) {
              Some(PlayerControllType.withProto(msg.getRoomPosition1))
            } else {
              None
            },
            if (msg.hasRoomPosition2) {
              Some(PlayerControllType.withProto(msg.getRoomPosition2))
            } else {
              None
            },
            if (msg.hasRoomPosition3) {
              Some(PlayerControllType.withProto(msg.getRoomPosition3))
            } else {
              None
            },
            if (msg.hasRoomPosition4) {
              Some(PlayerControllType.withProto(msg.getRoomPosition4))
            } else {
              None
            }
          )
          c.presentPlayerOnPosition(Symbol(msg.getPosition), appContext.playerName)

          if (msg.getReadyToStart && msg.getIsCreator) {
            c.startButton.setVisible(true)
            c.startButton.setDisable(false)
          } else if (!msg.getReadyToStart && msg.getIsCreator) {
            c.startButton.setVisible(true)
            c.startButton.setDisable(true)
          } else if (!msg.getIsCreator) {
            c.startButton.setVisible(false)
          }

          team = msg.getJoinedTeam

          allTeams = msg.getTeamsList.asScala.toList

          val possibleTeams = msg.getPossibleToChangeTeamList.asScala.filter(t => t != msg.getJoinedTeam)
          if (possibleTeams.size >= 1) {
            c.changeTeamButton.setDisable(false)
            possibleTeamToChange = possibleTeams.head
          } else {
            c.changeTeamButton.setDisable(true)
          }
        }


      })

    case msg: PlayerJoinedRoom =>
      Platform.runLater(new Runnable {
        override def run(): Unit = {
          val c = presentedForm.asInstanceOf[RoomController]

          c.presentPlayerOnPosition(Symbol(msg.getPosition), msg.getNickName)

          if (msg.getReadyToStart) {
            c.startButton.setDisable(false)
          } else {
            c.startButton.setDisable(true)
          }

          val possibleTeams = msg.getPossibleToChangeTeamList.asScala.filter(t => t != team)
          if ((possibleTeams.size >= 1)) {
            c.changeTeamButton.setDisable(false)
            possibleTeamToChange = possibleTeams.head
          } else {
            c.changeTeamButton.setDisable(true)
          }
        }
      })

    case msg: PlayerLeftRoom =>
      Platform.runLater(new Runnable {
        override def run(): Unit = {
          val c = presentedForm.asInstanceOf[RoomController]
          c.removePlayerFromPosition(msg.getName)

          if (msg.getReadyToStart) {
            c.startButton.setDisable(false)
          } else {
            c.startButton.setDisable(true)
          }

          val possibleTeams = msg.getPossibleToChangeTeamList.asScala.filter(t => t != team)
          if (possibleTeams.size >= 1) {
            c.changeTeamButton.setDisable(false)
            possibleTeamToChange = possibleTeams.head
          } else {
            c.changeTeamButton.setDisable(true)
          }
        }

      })


    case msg: PlayerChangedTeamInRoom =>
      Platform.runLater(new Runnable {
        override def run(): Unit = {
          val c = presentedForm.asInstanceOf[RoomController]
          if (msg.getPlayer == appContext.playerName) {
              possibleTeamToChange = team
            team = changingTeam
            changingTeam = ""
          }
          c.removePlayerFromPosition(msg.getPlayer)
          c.presentPlayerOnPosition(Symbol(msg.getPosition), msg.getPlayer)
        }
      })

    case msg: ChangeTeamInteraction =>
      changingTeam = possibleTeamToChange
      applicationInstanceController ! msg.copy(team = possibleTeamToChange)

    case StartGameInteraction =>
      applicationInstanceController ! StartGameInteraction

    case CloseFormInteraction =>
      applicationInstanceController ! CloseFormInteraction

    case CloseForm =>
      Platform.runLater(new Runnable {
        override def run(): Unit = {
          presentedForm.close
        }
      })

    case HideForm =>
      Platform.runLater(new Runnable {
        override def run(): Unit = {
          presentedForm.hide
        }
      })
  }
}
