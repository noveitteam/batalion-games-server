package cz.bataliongames.firstenchanter.form.actors

import java.util.Properties
import javafx.application.Platform

import akka.actor.{Kill, ActorRef, ActorLogging}
import cz.bataliongames.firstenchanter.ApplicationContext
import cz.bataliongames.firstenchanter.form.actors.CloseFormInteraction
import cz.bataliongames.firstenchanter.form.controllers.RoomsController
import cz.noveit.proto.firstenchanter.FirstEnchanterLightWeightServerLobby.{PlayerChangedTeamInRoom, RoomList}
import cz.noveit.proto.serialization.MessageEvent
import sun.awt.AppContext
import scala.collection.JavaConverters._
/**
 * Created by arnostkuchar on 15.09.14.
 */

trait RoomsInteraction
case class JoinRoomInteraction(name: String, userName: String) extends RoomsInteraction
case class CreateRoomInteraction(name: String, userName: String) extends RoomsInteraction

class RoomsActor(val configDispatcher: ActorRef, val applicationInstanceController: ActorRef, appContext: ApplicationContext)  extends FormActor with ActorLogging  {
  val fxmlFile = "Rooms.fxml"
  val formId = "Rooms"

  val properties = new Properties()
  createForm[RoomsActor](properties)

  def receive = {
    case e: RoomList => {
    Platform.runLater(new Runnable {
      override def run(): Unit = {
        if(e.getRoomsCount > 0){
          val c =  presentedForm.asInstanceOf[RoomsController]
          c.playerNicknameTextField.setText(appContext.playerName)
          c.renderListOfRooms(e.getRoomsList.asScala.toList)
        }
      }
    })

    }


    case ri: RoomsInteraction =>
      applicationInstanceController ! ri
    case CloseFormInteraction =>
      applicationInstanceController ! CloseFormInteraction
    case CloseForm =>
      println(" Closing roomns")
      presentedForm.close
    case HideForm => presentedForm.hide

  }
}
