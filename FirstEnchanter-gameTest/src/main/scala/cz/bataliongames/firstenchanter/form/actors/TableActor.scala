package cz.bataliongames.firstenchanter.form.actors

import java.io.IOException
import java.util.Properties
import javafx.application.Platform
import javafx.event.EventHandler
import javafx.stage.WindowEvent
import cz.bataliongames.firstenchanter.ApplicationContext
import cz.bataliongames.firstenchanter.form.controllers.table.chat.{ChatMode, ChatMessage}
import cz.bataliongames.firstenchanter.form.controllers.{TableState, CardLoader, FormController, TableController}
import cz.noveit.games.cardgame.adapters.{Card, Deck}
import cz.noveit.games.cardgame.engine.card._
import cz.noveit.games.cardgame.engine.game.session.{PlayerRollPosition, PLAYER_ROLL_POSITION_DEFENDER, PLAYER_ROLL_POSITION_ATTACKER, AttackerSwappedCards}
import cz.noveit.games.cardgame.engine.handlers.helpers.deserialization.{ProtoToNeutralDistribution, ProtoToCardTarget, ProtoToCardPosition}
import cz.noveit.games.cardgame.engine.handlers.helpers.serialization.{CardTargetToProto, CardPositionToProto, NeutralDistributionToProto}
import cz.noveit.games.cardgame.engine.player.{NeutralDistribution, CharmOfElements}
import cz.noveit.proto.firstenchanter.FirstEnchanterConversation._
import cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardPosition
import cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardPosition.CardPositionPlace
import cz.noveit.proto.firstenchanter.FirstEnchanterMatch.EvaluateAttackDone
import cz.noveit.proto.firstenchanter.FirstEnchanterMatch.EvaluateUsageDone
import cz.noveit.proto.firstenchanter.FirstEnchanterMatch._
import cz.noveit.games.cardgame.engine.game.{session => NITSession}
import cz.noveit.games.cardgame.engine.{card => NITCard}
import cz.noveit.proto.serialization.{MessageSerializer, MessageDeserializer, MessageEvent}

import scalafx.scene.Scene
import scalafx.Includes._
import scalafx.stage.{StageStyle, Stage}
import javafx.{fxml => jfxf}
import javafx.{scene => jfxs}
import scala.collection.JavaConverters._

import akka.actor.{FSM, ActorRef, Actor, ActorLogging}

/**
 * Created by Wlsek on 4.9.2014.
 */

trait TableInterAction {
  val UUID = {
    java.util.UUID.randomUUID.toString
  }

}

case class RollInteraction(rollPosition: PlayerRollPosition, roll: Int) extends TableInterAction

case class UsedCardInteraction(position: NITCard.CardPosition, cardId: String, target: Option[NITCard.CardTarget], neutralDistribution: NeutralDistribution = NeutralDistribution(0, 0, 0, 0), successHandler: (AnyRef *) => Unit, failHandler: () => Unit) extends TableInterAction

case class AttackerSwapsCardsInteraction(cards: List[NITCard.CardPosition], neutralDistribution: NeutralDistribution, successHandler: (AnyRef *) => Unit, failHandler: () => Unit) extends TableInterAction

case class CounterUsedCardInteraction(position: NITCard.CardPosition, cardId: String, target: Option[NITCard.CardTarget], neutralDistribution: NeutralDistribution = NeutralDistribution(0, 0, 0, 0), successHandler: (AnyRef *) => Unit, failHandler: () => Unit) extends TableInterAction

case class AttackedWithCardInteraction(position: NITCard.CardPosition, cardId: String, neutralDistribution: NeutralDistribution = NeutralDistribution(0, 0, 0, 0), successHandler: (AnyRef *) => Unit, failHandler: () => Unit) extends TableInterAction

case class DefendWithCardInteraction(position: NITCard.CardPosition, cardId: String, target: NITCard.CardTarget, neutralDistribution: NeutralDistribution = NeutralDistribution(0, 0, 0, 0), successHandler: (AnyRef *) => Unit, failHandler: () => Unit) extends TableInterAction

case object AttackerAttacksSubRoundEndInteraction extends TableInterAction

case object AttackerUsesSubRoundEndInteraction extends TableInterAction

case object DefenderCounterUsesSubRoundEndInteraction extends TableInterAction

case object DefenderDefendsSubRoundEndInteraction extends TableInterAction

case class TableInteractionHandlers(uuid: String, successHandler: (AnyRef *) => Unit, failHandler: () => Unit)

case class SendMessageInteraction(message: String, all: Boolean) extends TableInterAction

trait ClientState

case object INITIALIZING_MATCH extends ClientState

case object PLAYER_STARTS_ROLL extends ClientState

case object FORTUNE_ROLL extends ClientState

case object ATTACKER_TEAM_PLAY extends ClientState

case object DEFENDER_TEAM_COUNTER_PLAY extends ClientState

case object ATTACKER_TEAM_ATTACK extends ClientState

case object DEFENDER_TEAM_DEFENDS extends ClientState

case object EVALUATION_AND_ANIMATION extends ClientState


object PlayerRelationship extends Enumeration {
  val Me, Ally, Enemy1, Enemy2 = Value
}

case class PlayerSeesPlayer(player: String,
                            hand: Array[CardHolder],
                            onTable: Array[CardHolder],
                            graveyard: Array[CardHolder],
                            using: List[NITSession.AttackerUsesCard],
                            counterUsing: List[NITSession.DefenderUsesCard],
                            attacking: List[NITSession.AttackerAttacks],
                            defending: List[NITSession.DefenderDefends],
                            elements: CharmOfElements,
                            relationship: PlayerRelationship.Value,
                            fortuneIsSmiling: Boolean,
                            deckSize: Int,
                            handsSize: Int,
                            graveyardSize: Int
                             )

case class PlayerVision(players: List[PlayerSeesPlayer], tableState: TableState.Value, myTeamHp: Int = 0, enemyTeamHp: Int = 0, roundsPassed: Int = 1, attackers: List[String], defenders: List[String], timeLimit: Long, timeAnchor: Long, matchEnded: Boolean, win: Boolean) {
  def me = players.find(p => p.relationship == PlayerRelationship.Me).get
}

case class PlayerData(appContext: ApplicationContext, matchSetup: Option[NITSession.MatchSetup], vision: PlayerVision, requestHandlers: List[TableInteractionHandlers]) {
  def me = {
    vision.players.find(p => p.relationship == PlayerRelationship.Me).get
  }
}

class TableActor(val configDispatcher: ActorRef, val applicationInstanceController: ActorRef, appContext: ApplicationContext) extends FormActor with ActorLogging with FSM[ClientState, PlayerData] with MessageSerializer {

  val fxmlFile = "Table.fxml"
  val properties = new Properties()
  val cards = CardLoader("FirstEnchanter.cards")

  val playerDecks: List[Pair[String, Deck]] = List()

  val module = "FirstEnchanterMatch"

  var allChatConversationId: Option[String] = None
  var allyChatConversationId: Option[String] = None

  //.map(c => CardHolder(Deck.Empty, c))

  createForm[TableController](properties)

  startWith(INITIALIZING_MATCH, PlayerData(appContext, None, PlayerVision(List(), TableState.StartRolling, 0, 0, 0, List(), List(), 0, 0, false, false), List()))

  when(INITIALIZING_MATCH) {
    case Event(MessageEvent(msg: StateAsk, mod, rh, ctx), data) => {
      msg.getState match {
        case MatchState.PLAYER_STARTS_ROLL =>
          val vision = TableStateForPlayerToPlayerVision(msg.getPlayerState, msg.getState, appContext.playerName, msg.getTimeInThisState, msg.getTimeAnchor, playerDecks, cards)

          Platform.runLater(new Runnable {
            override def run(): Unit = {
              var myHand: List[CardHolder] = List()
              var allyHand: List[CardHolder] = List()
              var ownTable: List[CardHolder] = List()
              var allyTable: List[CardHolder] = List()
              var ownGraveyard: List[CardHolder] = List()

              var enemyTable: List[CardHolder] = List()
              var enemyGraveyard: List[CardHolder] = List()

              vision.players.find(p => p.relationship == PlayerRelationship.Me).map(p => {
                myHand = p.hand.toList
                ownTable = ownTable ::: p.onTable.toList
                ownGraveyard = ownGraveyard ::: p.graveyard.toList
              })

              vision.players.find(p => p.relationship == PlayerRelationship.Ally).map(p => {
                allyHand = p.hand.toList
                allyTable = allyTable ::: p.onTable.toList
                ownGraveyard = ownGraveyard ::: p.graveyard.toList
              })

              vision.players.find(p => p.relationship == PlayerRelationship.Enemy1).map(p => {
                enemyTable = enemyTable ::: p.onTable.toList
                enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
              })

              vision.players.find(p => p.relationship == PlayerRelationship.Enemy2).map(p => {
                enemyTable = enemyTable ::: p.onTable.toList
                enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
              })

              presentedForm.asInstanceOf[TableController].actualizeCardsInHand(myHand)
              presentedForm.asInstanceOf[TableController].actualizeOwnTable(ownTable)
              presentedForm.asInstanceOf[TableController].actualizeAllyTable(allyTable)
              presentedForm.asInstanceOf[TableController].actualizeOwnGraveyard(ownGraveyard)
              presentedForm.asInstanceOf[TableController].actualizeEnemyTable(enemyTable)
              presentedForm.asInstanceOf[TableController].actualizeEnemyGraveyard(enemyGraveyard)
              presentedForm.asInstanceOf[TableController].actualizePlayerVision(
                vision
              )
              presentedForm.asInstanceOf[TableController].setTableState(TableState.StartRolling)
              presentedForm.asInstanceOf[TableController].resetPossibilities
            }
          })

          appContext.webContext.map(ctx => ctx.send(serializeToString(MessageEvent(
            StateAck.newBuilder().setState(msg.getState).build(), module
          ))))

          goto(PLAYER_STARTS_ROLL) using (data.copy(vision = vision))
      }
    }
  }

  when(PLAYER_STARTS_ROLL) {
    case Event(MessageEvent(msg: AskPlayerRoll, mod, rh, ctx), data) => {
      Platform.runLater(new Runnable {
        override def run(): Unit = {
          presentedForm.asInstanceOf[TableController].goToStartRolling(if (msg.getRollPositionAttacker) {
            PLAYER_ROLL_POSITION_ATTACKER
          } else {
            PLAYER_ROLL_POSITION_DEFENDER
          })
        }
      })
      stay()
    }

    case Event(MessageEvent(msg: PlayerRollResult, mod, rh, ctx), data) => {
      Platform.runLater(new Runnable {
        override def run(): Unit = {
          presentedForm.asInstanceOf[TableController].fortuneWheel(msg, appContext.playerName)
        }
      })
      stay()
    }

    case Event(msg: RollInteraction, data) => {
      appContext.webContext.map(ctx => ctx.send(serializeToString(MessageEvent(
        PlayerRoll.newBuilder()
          .setPlayer(appContext.playerName)
          .setRollPositionAttacker(msg.rollPosition == PLAYER_ROLL_POSITION_ATTACKER)
          .setRoll(msg.roll)
          .build(), module
      ))))
      stay()
    }

    case Event(MessageEvent(msg: ElementsGenerated, mod, rh, ctx), data) => {
      val newVision = data.vision.players.find(p => p.player == msg.getPlayer).map(found => {
        data.vision.copy(players = found.copy(elements = found.elements + CharmOfElements(
          msg.getCharm.getFire,
          msg.getCharm.getWater,
          msg.getCharm.getEarth,
          msg.getCharm.getAir
        )) :: data.vision.players.filter(p => p.player != found.player))
      }).getOrElse(data.vision)

      Platform.runLater(new Runnable {
        override def run(): Unit = {
          var myHand: List[CardHolder] = List()
          var allyHand: List[CardHolder] = List()
          var ownTable: List[CardHolder] = List()
          var allyTable: List[CardHolder] = List()
          var ownGraveyard: List[CardHolder] = List()

          var enemyTable: List[CardHolder] = List()
          var enemyGraveyard: List[CardHolder] = List()

          newVision.players.find(p => p.relationship == PlayerRelationship.Me).map(p => {
            myHand = p.hand.toList
            ownTable = ownTable ::: p.onTable.toList
            ownGraveyard = ownGraveyard ::: p.graveyard.toList
          })

          newVision.players.find(p => p.relationship == PlayerRelationship.Ally).map(p => {
            allyHand = p.hand.toList
            allyTable = allyTable ::: p.onTable.toList
            ownGraveyard = ownGraveyard ::: p.graveyard.toList
          })

          newVision.players.find(p => p.relationship == PlayerRelationship.Enemy1).map(p => {
            enemyTable = enemyTable ::: p.onTable.toList
            enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
          })

          newVision.players.find(p => p.relationship == PlayerRelationship.Enemy2).map(p => {
            enemyTable = enemyTable ::: p.onTable.toList
            enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
          })

          presentedForm.asInstanceOf[TableController].actualizeCardsInHand(myHand)
          presentedForm.asInstanceOf[TableController].actualizeOwnTable(ownTable)
          presentedForm.asInstanceOf[TableController].actualizeAllyTable(allyTable)
          presentedForm.asInstanceOf[TableController].actualizeOwnGraveyard(ownGraveyard)
          presentedForm.asInstanceOf[TableController].actualizeEnemyTable(enemyTable)
          presentedForm.asInstanceOf[TableController].actualizeEnemyGraveyard(enemyGraveyard)
          presentedForm.asInstanceOf[TableController].actualizePlayerVision(newVision)

        }
      })
      stay using data.copy(vision = newVision)
    }

    case Event(MessageEvent(msg: PlayerDrawCards, mod, rh, ctx), data) => {
      var newVision = data.vision
      msg.getCardsList.asScala.map(c => {
        cards.find(card => card.id == c.getId).map(cardFound => {
          newVision.players.find(p => p.player == c.getPosition.getPlayer).map(found => {
            val deck = playerDecks.find(d => d._1 == found.player).map(p => p._2).getOrElse(Deck.Empty)
            newVision = newVision.copy(players = found.copy(
              hand = AppendElementToCardHolderArray(CardHolderBuilder(cardFound, deck
              ), c.getPosition.getPlaceId, found.hand)
            ) :: newVision.players.filter(p => p.player != found.player))
          }).getOrElse(newVision)
        })
      })



      Platform.runLater(new Runnable {
        override def run(): Unit = {
          var myHand: List[CardHolder] = List()
          var allyHand: List[CardHolder] = List()
          var ownTable: List[CardHolder] = List()
          var allyTable: List[CardHolder] = List()
          var ownGraveyard: List[CardHolder] = List()

          var enemyTable: List[CardHolder] = List()
          var enemyGraveyard: List[CardHolder] = List()

          newVision.players.find(p => p.relationship == PlayerRelationship.Me).map(p => {
            myHand = p.hand.toList
            ownTable = ownTable ::: p.onTable.toList
            ownGraveyard = ownGraveyard ::: p.graveyard.toList
          })

          newVision.players.find(p => p.relationship == PlayerRelationship.Ally).map(p => {
            allyHand = p.hand.toList
            allyTable = allyTable ::: p.onTable.toList
            ownGraveyard = ownGraveyard ::: p.graveyard.toList
          })

          newVision.players.find(p => p.relationship == PlayerRelationship.Enemy1).map(p => {
            enemyTable = enemyTable ::: p.onTable.toList
            enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
          })

          newVision.players.find(p => p.relationship == PlayerRelationship.Enemy2).map(p => {
            enemyTable = enemyTable ::: p.onTable.toList
            enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
          })

          presentedForm.asInstanceOf[TableController].actualizeCardsInHand(myHand)
          presentedForm.asInstanceOf[TableController].actualizeOwnTable(ownTable)
          presentedForm.asInstanceOf[TableController].actualizeAllyTable(allyTable)
          presentedForm.asInstanceOf[TableController].actualizeOwnGraveyard(ownGraveyard)
          presentedForm.asInstanceOf[TableController].actualizeEnemyTable(enemyTable)
          presentedForm.asInstanceOf[TableController].actualizeEnemyGraveyard(enemyGraveyard)
          presentedForm.asInstanceOf[TableController].actualizePlayerVision(newVision)

          // val ids = msg.getCardsList.asScala.filter(c => c.hasId)
          /* presentedForm.asInstanceOf[TableController].addCards(
             msg,
             ids
               .map(card => cards.find(c => c.id == card.getId)
               .map(found => CardHolderBuilder(found, card.getSkinId))
               .getOrElse(CardHolder.EMPTY)
               ).toList
           )*/
        }
      })
      stay using data.copy(vision = newVision)
    }

    case Event(MessageEvent(msg: StateAsk, mod, rh, ctx), data) => {
      val vision = TableStateForPlayerToPlayerVision(msg.getPlayerState, msg.getState, appContext.playerName, msg.getTimeInThisState, msg.getTimeAnchor, playerDecks, cards)

      msg.getState match {
        case MatchState.ATTACKER_TEAM_PLAY =>
          Platform.runLater(new Runnable {
            override def run(): Unit = {
              var myHand: List[CardHolder] = List()
              var allyHand: List[CardHolder] = List()
              var ownTable: List[CardHolder] = List()
              var allyTable: List[CardHolder] = List()
              var ownGraveyard: List[CardHolder] = List()

              var enemyTable: List[CardHolder] = List()
              var enemyGraveyard: List[CardHolder] = List()

              vision.players.find(p => p.relationship == PlayerRelationship.Me).map(p => {
                myHand = p.hand.toList
                ownTable = ownTable ::: p.onTable.toList
                ownGraveyard = ownGraveyard ::: p.graveyard.toList
              })

              vision.players.find(p => p.relationship == PlayerRelationship.Ally).map(p => {
                allyHand = p.hand.toList
                allyTable = allyTable ::: p.onTable.toList
                ownGraveyard = ownGraveyard ::: p.graveyard.toList
              })

              vision.players.find(p => p.relationship == PlayerRelationship.Enemy1).map(p => {
                enemyTable = enemyTable ::: p.onTable.toList
                enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
              })

              vision.players.find(p => p.relationship == PlayerRelationship.Enemy2).map(p => {
                enemyTable = enemyTable ::: p.onTable.toList
                enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
              })

              presentedForm.asInstanceOf[TableController].actualizeCardsInHand(myHand)
              presentedForm.asInstanceOf[TableController].actualizeOwnTable(ownTable)
              presentedForm.asInstanceOf[TableController].actualizeAllyTable(allyTable)
              presentedForm.asInstanceOf[TableController].actualizeOwnGraveyard(ownGraveyard)
              presentedForm.asInstanceOf[TableController].actualizeEnemyTable(enemyTable)
              presentedForm.asInstanceOf[TableController].actualizeEnemyGraveyard(enemyGraveyard)
              presentedForm.asInstanceOf[TableController].actualizePlayerVision(
                vision
              )
              presentedForm.asInstanceOf[TableController].setTableState(TableState.Using)
              presentedForm.asInstanceOf[TableController].resetPossibilities
            }
          })

          appContext.webContext.map(ctx => ctx.send(serializeToString(MessageEvent(
            StateAck.newBuilder().setState(msg.getState).build(), module
          ))))
          goto(ATTACKER_TEAM_PLAY) using data.copy(vision = vision)
      }
    }
  }

  when(FORTUNE_ROLL) {
    case Event(MessageEvent(msg: AskPlayerRoll, mod, rh, ctx), data) => {
      Platform.runLater(new Runnable {
        override def run(): Unit = {
          presentedForm.asInstanceOf[TableController].goToFortuneRolling(if (msg.getRollPositionAttacker) {
            PLAYER_ROLL_POSITION_ATTACKER
          } else {
            PLAYER_ROLL_POSITION_DEFENDER
          })
        }
      })
      stay()
    }

    case Event(MessageEvent(msg: PlayerRollResult, mod, rh, ctx), data) => {
      Platform.runLater(new Runnable {
        override def run(): Unit = {
          presentedForm.asInstanceOf[TableController].fortuneWheel(msg, appContext.playerName)
        }
      })
      stay()
    }

    case Event(msg: RollInteraction, data) => {
      appContext.webContext.map(ctx => ctx.send(serializeToString(MessageEvent(
        PlayerRoll.newBuilder()
          .setPlayer(appContext.playerName)
          .setRollPositionAttacker(msg.rollPosition == PLAYER_ROLL_POSITION_ATTACKER)
          .setRoll(msg.roll).build(), module
      ))))
      stay()
    }

    case Event(MessageEvent(msg: ElementsGenerated, mod, rh, ctx), data) => {
      Platform.runLater(new Runnable {
        override def run(): Unit = {
          presentedForm.asInstanceOf[TableController].addElements(msg)
        }
      })
      stay()
    }

    case Event(MessageEvent(msg: PlayerDrawCards, mod, rh, ctx), data) => {
      Platform.runLater(new Runnable {
        override def run(): Unit = {
          val ids = msg.getCardsList.asScala.filter(c => c.hasId)
          presentedForm.asInstanceOf[TableController].addCards(
            msg,
            ids
              .map(card => cards.find(c => c.id == card.getId)
              .map(found => CardHolderBuilder(found, card.getSkinId))
              .getOrElse(CardHolder.EMPTY)
              ).toList
          )
        }
      })
      stay()
    }

    case Event(MessageEvent(msg: StateAsk, mod, rh, ctx), data) => {
      val vision = TableStateForPlayerToPlayerVision(msg.getPlayerState, msg.getState, appContext.playerName, msg.getTimeInThisState, msg.getTimeAnchor, playerDecks, cards)

      msg.getState match {
        case MatchState.ATTACKER_TEAM_PLAY =>
          Platform.runLater(new Runnable {
            override def run(): Unit = {
              var myHand: List[CardHolder] = List()
              var allyHand: List[CardHolder] = List()
              var ownTable: List[CardHolder] = List()
              var allyTable: List[CardHolder] = List()
              var ownGraveyard: List[CardHolder] = List()

              var enemyTable: List[CardHolder] = List()
              var enemyGraveyard: List[CardHolder] = List()

              vision.players.find(p => p.relationship == PlayerRelationship.Me).map(p => {
                myHand = p.hand.toList
                ownTable = ownTable ::: p.onTable.toList
                ownGraveyard = ownGraveyard ::: p.graveyard.toList
              })

              vision.players.find(p => p.relationship == PlayerRelationship.Ally).map(p => {
                allyHand = p.hand.toList
                allyTable = allyTable ::: p.onTable.toList
                ownGraveyard = ownGraveyard ::: p.graveyard.toList
              })

              vision.players.find(p => p.relationship == PlayerRelationship.Enemy1).map(p => {
                enemyTable = enemyTable ::: p.onTable.toList
                enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
              })

              vision.players.find(p => p.relationship == PlayerRelationship.Enemy2).map(p => {
                enemyTable = enemyTable ::: p.onTable.toList
                enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
              })

              presentedForm.asInstanceOf[TableController].actualizeCardsInHand(myHand)
              presentedForm.asInstanceOf[TableController].actualizeOwnTable(ownTable)
              presentedForm.asInstanceOf[TableController].actualizeAllyTable(allyTable)
              presentedForm.asInstanceOf[TableController].actualizeOwnGraveyard(ownGraveyard)
              presentedForm.asInstanceOf[TableController].actualizeEnemyTable(enemyTable)
              presentedForm.asInstanceOf[TableController].actualizeEnemyGraveyard(enemyGraveyard)
              presentedForm.asInstanceOf[TableController].actualizePlayerVision(
                vision
              )
              presentedForm.asInstanceOf[TableController].setTableState(TableState.Using)
              presentedForm.asInstanceOf[TableController].resetPossibilities
            }
          })

          appContext.webContext.map(ctx => ctx.send(serializeToString(MessageEvent(
            StateAck.newBuilder().setState(msg.getState).build(), module
          ))))
          goto(ATTACKER_TEAM_PLAY) using data.copy(vision = vision)
      }
    }
  }

  when(ATTACKER_TEAM_PLAY) {
    case Event(AttackerUsesSubRoundEndInteraction, data) => {
      appContext.webContext.map(ctx => ctx.send(serializeToString(MessageEvent(
        AttackerEndsTurn.newBuilder().setUserName(data.appContext.playerName).build(), module
      ))))
      stay()
    }

    case Event(msg: AttackerSwapsCardsInteraction, data) => {
      appContext.webContext.map(ctx => ctx.send(serializeToString(MessageEvent(
        AttackerSwappsCards
          .newBuilder()
          .addAllCards(msg.cards.map(c => CardPositionToProto(c)).toList.asJava)
          .setDistribution(NeutralDistributionToProto(msg.neutralDistribution))
          .build(), module, msg.UUID
      ))))

      stay using data.copy(requestHandlers = TableInteractionHandlers(msg.UUID, msg.successHandler, msg.failHandler) :: data.requestHandlers)
    }

    case Event(MessageEvent(msg: AttackerSwappsCardsAccepted, mod, rh, ctx), data) => {
      data.requestHandlers.find(h => h.uuid == rh).map(found => {
        Platform.runLater(new Runnable {
          override def run(): Unit = {
            found.successHandler(Nil)
          }
        })
      })
      stay using data.copy(requestHandlers = data.requestHandlers.filter(f => f.uuid != rh))
    }

    case Event(MessageEvent(msg: AttackerSwappsCardsDeclined, mod, rh, ctx), data) => {
      data.requestHandlers.find(h => h.uuid == rh).map(found => {
        Platform.runLater(new Runnable {
          override def run(): Unit = {
            found.failHandler()
          }
        })
      })
      stay using data.copy(requestHandlers = data.requestHandlers.filter(f => f.uuid != rh))
    }

    case Event(MessageEvent(msg: AskAttackerToUseCard, mod, rh, ctx), data) => {
      val usages = msg.getPossibleUsagesList.asScala.map(pu => {
        NITSession.AttackerUsesCard(
          ProtoToCardPosition(pu.getPosition),
          if (pu.hasTarget) {
            Some(ProtoToCardTarget(pu.getTarget))
          } else {
            None
          },
          CardPositionToCardHolder(ProtoToCardPosition(pu.getPosition), data.vision),
          ProtoToNeutralDistribution(pu.getNeutralDistribution),
          List()
        )
      }).toList

      Platform.runLater(new Runnable {
        override def run(): Unit = {
          presentedForm.asInstanceOf[TableController].recreatePossibleUses(usages)
        }
      })
      stay()
    }

    case Event(MessageEvent(msg: AskDefenderToWait, mod, rh, ctx), data) => {
      stay()
    }

    case Event(msg: UsedCardInteraction, data) => {
      val builder = AttackerUsesCard
        .newBuilder()
        .setPosition(CardPositionToProto(msg.position))
        .setNeutralDistribution(NeutralDistributionToProto(msg.neutralDistribution))
        .setId(msg.cardId)

      msg.target.map(t => builder.setTarget(CardTargetToProto(t)))
      appContext.webContext.map(ctx => ctx.send(serializeToString(MessageEvent(
        builder.build(), module, msg.UUID
      ))))

      stay using data.copy(requestHandlers = TableInteractionHandlers(msg.UUID, msg.successHandler, msg.failHandler) :: data.requestHandlers)
    }

    case Event(MessageEvent(msg: AttackerUsedCardAccepted, mod, rh, ctx), data) => {
      val newVision = data.vision.copy(
        players = data.vision.players.find(p => p.player == msg.getPosition.getPlayer).map(found => {
          found.copy(
            //   hand = found.hand.filter(c => c != found.hand(msg.getPosition.getPlaceId)),
            handsSize = found.handsSize - 1,
            //   onTable = found.onTable :+ found.hand(msg.getPosition.getPlaceId),
            elements = found.elements -/(found.hand(msg.getPosition.getPlaceId).price, ProtoToNeutralDistribution(msg.getNeutralDistribution)),
            using = NITSession.AttackerUsesCard(
              ProtoToCardPosition(msg.getPosition),
              if (msg.hasTarget) {
                Some(ProtoToCardTarget(msg.getTarget))
              } else {
                None
              },
              found.hand(msg.getPosition.getPlaceId),
              ProtoToNeutralDistribution(msg.getNeutralDistribution),
              List()
            ) :: found.using
          ) :: data.vision.players.filter(p => p.player != msg.getPosition.getPlayer)
        }).getOrElse(data.vision.players)
      )

      data.requestHandlers.find(h => h.uuid == rh).map(found => {
        Platform.runLater(new Runnable {
          override def run(): Unit = {
            val newUses: List[NITSession.AttackerUsesCard] = msg.getPossibleUsagesList.asScala.map(pu => {
              NITSession.AttackerUsesCard(
                ProtoToCardPosition(pu.getPosition),
                if (pu.hasTarget) {
                  Some(ProtoToCardTarget(pu.getTarget))
                } else {
                  None
                },
                CardPositionToCardHolder(ProtoToCardPosition(pu.getPosition), data.vision),
                ProtoToNeutralDistribution(pu.getNeutralDistribution),
                List()
              )
            }).toList
            found.successHandler(newUses)
            var myHand: List[CardHolder] = List()
            var allyHand: List[CardHolder] = List()
            var ownTable: List[CardHolder] = List()
            var allyTable: List[CardHolder] = List()
            var ownGraveyard: List[CardHolder] = List()

            var enemyTable: List[CardHolder] = List()
            var enemyGraveyard: List[CardHolder] = List()

            newVision.players.find(p => p.relationship == PlayerRelationship.Me).map(p => {
              myHand = p.hand.filter(c => p.using.find(u => c == u.card).isEmpty).toList
              ownTable = ownTable ::: p.onTable.toList ::: p.using.map(u => u.card)
              ownGraveyard = ownGraveyard ::: p.graveyard.toList
            })

            newVision.players.find(p => p.relationship == PlayerRelationship.Ally).map(p => {
              allyHand = p.hand.toList
              allyTable = allyTable ::: p.onTable.toList
              ownGraveyard = ownGraveyard ::: p.graveyard.toList
            })

            newVision.players.find(p => p.relationship == PlayerRelationship.Enemy1).map(p => {
              enemyTable = enemyTable ::: p.onTable.toList
              enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
            })

            newVision.players.find(p => p.relationship == PlayerRelationship.Enemy2).map(p => {
              enemyTable = enemyTable ::: p.onTable.toList
              enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
            })

            presentedForm.asInstanceOf[TableController].actualizeCardsInHand(myHand)
            presentedForm.asInstanceOf[TableController].actualizeOwnTable(ownTable)
            presentedForm.asInstanceOf[TableController].actualizeAllyTable(allyTable)
            presentedForm.asInstanceOf[TableController].actualizeOwnGraveyard(ownGraveyard)
            presentedForm.asInstanceOf[TableController].actualizeEnemyTable(enemyTable)
            presentedForm.asInstanceOf[TableController].actualizeEnemyGraveyard(enemyGraveyard)
            presentedForm.asInstanceOf[TableController].actualizePlayerVision(newVision)
          }
        })
      })
      stay using data.copy(requestHandlers = data.requestHandlers.filter(f => f.uuid != rh), vision = newVision)
    }


    case Event(MessageEvent(msg: AttackerUsedCardDeclined, mod, rh, ctx), data) => {
      data.requestHandlers.find(h => h.uuid == rh).map(found => {
        Platform.runLater(new Runnable {
          override def run(): Unit = {
            found.failHandler()
          }
        })
      })
      stay using data.copy(requestHandlers = data.requestHandlers.filter(f => f.uuid != rh))
    }

    case Event(MessageEvent(msg: AttackerUsesCard, mod, rh, ctx), data) => {
      var card: CardHolder = CardHolder.EMPTY
      val newVision = data.vision.copy(
        players = data.vision.players.find(p => p.player == msg.getPosition.getPlayer).map(found => {

          card = found.hand(msg.getPosition.getPlaceId)
          if (card.id == CardHolder.EMPTY.id) {
            card = cards.find(c => c.id == msg.getId).map(c => {
              val deck = playerDecks.find(d => d._1 == msg.getPosition.getPlayer).map(p => p._2).getOrElse(Deck.Empty)
              CardHolderBuilder(c, deck)
            }).getOrElse(CardHolder.EMPTY)
          }

          val hands = found.hand
          hands(msg.getPosition.getPlaceId) = card

          found.copy(
            //  hand = found.hand.filter(c => c != card),
            hand = hands,
            handsSize = found.handsSize - 1,
            //   onTable = found.onTable :+ card,
            elements = found.elements -/(card.price, ProtoToNeutralDistribution(msg.getNeutralDistribution)),
            using = NITSession.AttackerUsesCard(
              ProtoToCardPosition(msg.getPosition),
              if (msg.hasTarget) {
                Some(ProtoToCardTarget(msg.getTarget))
              } else {
                None
              },
              card,
              ProtoToNeutralDistribution(msg.getNeutralDistribution),
              List()
            ) :: found.using
          ) :: data.vision.players.filter(p => p.player != msg.getPosition.getPlayer)
        }).getOrElse(data.vision.players)
      )

      Platform.runLater(new Runnable {
        override def run(): Unit = {
          var myHand: List[CardHolder] = List()
          var allyHand: List[CardHolder] = List()
          var ownTable: List[CardHolder] = List()
          var allyTable: List[CardHolder] = List()
          var ownGraveyard: List[CardHolder] = List()

          var enemyTable: List[CardHolder] = List()
          var enemyGraveyard: List[CardHolder] = List()

          newVision.players.find(p => p.relationship == PlayerRelationship.Me).map(p => {
            myHand = p.hand.toList
            ownTable = ownTable ::: p.onTable.toList
            ownGraveyard = ownGraveyard ::: p.graveyard.toList
          })

          newVision.players.find(p => p.relationship == PlayerRelationship.Ally).map(p => {
            allyHand = p.hand.toList
            allyTable = allyTable ::: p.onTable.toList ::: {
              if (msg.getPosition.getPlayer == p.player) {
                List(card)
              } else {
                List()
              }
            }
            ownGraveyard = ownGraveyard ::: p.graveyard.toList
          })

          newVision.players.find(p => p.relationship == PlayerRelationship.Enemy1).map(p => {
            enemyTable = enemyTable ::: p.onTable.toList ::: {
              if (msg.getPosition.getPlayer == p.player) {
                List(card)
              } else {
                List()
              }
            }
            enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
          })

          newVision.players.find(p => p.relationship == PlayerRelationship.Enemy2).map(p => {
            enemyTable = enemyTable ::: p.onTable.toList ::: {
              if (msg.getPosition.getPlayer == p.player) {
                List(card)
              } else {
                List()
              }
            }
            enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
          })

          presentedForm.asInstanceOf[TableController].actualizeCardsInHand(myHand)
          presentedForm.asInstanceOf[TableController].actualizeOwnTable(ownTable)
          presentedForm.asInstanceOf[TableController].actualizeAllyTable(allyTable)
          presentedForm.asInstanceOf[TableController].actualizeOwnGraveyard(ownGraveyard)
          presentedForm.asInstanceOf[TableController].actualizeEnemyTable(enemyTable)
          presentedForm.asInstanceOf[TableController].actualizeEnemyGraveyard(enemyGraveyard)
          presentedForm.asInstanceOf[TableController].actualizePlayerVision(newVision)
        }
      })
      stay using data.copy(vision = newVision)
    }
    /*
        case Event(MessageEvent(msg: DefenderNotificationAboutAttackerCardUse, mod, rh, ctx), data) => {

          val newVision = cards.find(c => c.id == msg.getCard.getId).map(c => {
            val deck = playerDecks.find(d => d._1 == msg.getCard.getPosition.getPlayer).map(p => p._2).getOrElse(Deck.Empty)
            val ch = CardHolderBuilder(c, deck)

            data.vision.copy(
              players = data.vision.players.find(p => p.player == msg.getCard.getPosition.getPlayer).map(found => {
                found.copy(
                  handsSize = found.handsSize - 1,
                  onTable = found.onTable :+ ch, //CH,
                  elements = found.elements -/(ch.price, ProtoToNeutralDistribution(msg.getNeutralDistribution)),
                  using = NITSession.AttackerUsesCard(
                    ProtoToCardPosition(msg.getCard.getPosition),
                    if (msg.hasTarget) {
                      Some(ProtoToCardTarget(msg.getTarget))
                    } else {
                      None
                    },
                    ch, //CH,
                    ProtoToNeutralDistribution(msg.getNeutralDistribution),
                    List()
                  ) :: found.using
                ) :: data.vision.players.filter(p => p.player != msg.getCard.getPosition.getPlayer)
              }).getOrElse(data.vision.players)
            )
          }).getOrElse(data.vision)


          Platform.runLater(new Runnable {
            override def run(): Unit = {
              presentedForm.asInstanceOf[TableController].actualizePlayerVision(newVision)
            }
          })
          stay using data.copy(vision = newVision)
        }
    */
    case Event(MessageEvent(msg: AttackerPossibleCardUses, mod, rh, ctx), data) => {
      Platform.runLater(new Runnable {
        override def run(): Unit = {
          presentedForm.asInstanceOf[TableController].recreatePossibleUses(msg.getPossibleUsagesList.asScala.map(pu => {
            NITSession.AttackerUsesCard(
              ProtoToCardPosition(pu.getPosition),
              if (pu.hasTarget) {
                Some(ProtoToCardTarget(pu.getTarget))
              } else {
                None
              },
              CardPositionToCardHolder(ProtoToCardPosition(pu.getPosition), data.vision),
              ProtoToNeutralDistribution(pu.getNeutralDistribution),
              List()
            )
          }).toList)
        }
      })
      stay()
    }

    case Event(MessageEvent(msg: AttackerEndsTurn, mod, rh, ctx), data) => {
      stay()
    }

    case Event(MessageEvent(msg: StateAsk, mod, rh, ctx), data) => {
      msg.getState match {
        case MatchState.DEFENDER_TEAM_COUNTER_PLAY =>
          val vision = TableStateForPlayerToPlayerVision(msg.getPlayerState, msg.getState, appContext.playerName, msg.getTimeInThisState, msg.getTimeAnchor, playerDecks, cards)

          Platform.runLater(new Runnable {
            override def run(): Unit = {
              var myHand: List[CardHolder] = List()
              var allyHand: List[CardHolder] = List()
              var ownTable: List[CardHolder] = List()
              var allyTable: List[CardHolder] = List()
              var ownGraveyard: List[CardHolder] = List()

              var enemyTable: List[CardHolder] = List()
              var enemyGraveyard: List[CardHolder] = List()

              vision.players.find(p => p.relationship == PlayerRelationship.Me).map(p => {
                myHand = p.hand.toList
                ownTable = ownTable ::: p.onTable.toList
                ownGraveyard = ownGraveyard ::: p.graveyard.toList
              })

              vision.players.find(p => p.relationship == PlayerRelationship.Ally).map(p => {
                allyHand = p.hand.toList
                allyTable = allyTable ::: p.onTable.toList
                ownGraveyard = ownGraveyard ::: p.graveyard.toList
              })

              vision.players.find(p => p.relationship == PlayerRelationship.Enemy1).map(p => {
                enemyTable = enemyTable :::
                  p.using.map(c => c.card) :::
                  p.counterUsing.map(c => c.card) :::
                  p.defending.filter(d => d.withPosition.isInstanceOf[InHandCardPosition]).map(c => c.defenderCard) :::
                  p.onTable.toList
                enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
              })

              vision.players.find(p => p.relationship == PlayerRelationship.Enemy2).map(p => {
                enemyTable = enemyTable :::
                  p.using.map(c => c.card) :::
                  p.counterUsing.map(c => c.card) :::
                  p.defending.filter(d => d.withPosition.isInstanceOf[InHandCardPosition]).map(c => c.defenderCard) :::
                  p.onTable.toList
                enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
              })

              presentedForm.asInstanceOf[TableController].actualizeCardsInHand(myHand)
              presentedForm.asInstanceOf[TableController].actualizeOwnTable(ownTable)
              presentedForm.asInstanceOf[TableController].actualizeAllyTable(allyTable)
              presentedForm.asInstanceOf[TableController].actualizeOwnGraveyard(ownGraveyard)
              presentedForm.asInstanceOf[TableController].actualizeEnemyTable(enemyTable)
              presentedForm.asInstanceOf[TableController].actualizeEnemyGraveyard(enemyGraveyard)
              presentedForm.asInstanceOf[TableController].actualizePlayerVision(
                vision
              )
              presentedForm.asInstanceOf[TableController].setTableState(TableState.CounterUsing)
              presentedForm.asInstanceOf[TableController].resetPossibilities
            }
          })
          appContext.webContext.map(ctx => ctx.send(serializeToString(MessageEvent(
            StateAck.newBuilder().setState(msg.getState).build(), module
          ))))
          goto(DEFENDER_TEAM_COUNTER_PLAY) using (data.copy(vision = vision))
      }
    }
  }

  when(DEFENDER_TEAM_COUNTER_PLAY) {
    case Event(DefenderCounterUsesSubRoundEndInteraction, data) => {
      appContext.webContext.map(ctx => ctx.send(serializeToString(MessageEvent(
        DefenderEndsTurn.newBuilder().setUserName(data.appContext.playerName).build(), module
      ))))
      stay()
    }

    case Event(MessageEvent(msg: AskAttackerToWait, mod, rh, ctx), data) => {
      stay()
    }

    case Event(MessageEvent(msg: AskDefenderToUseCard, mod, rh, ctx), data) => {
      Platform.runLater(new Runnable {
        override def run(): Unit = {
          presentedForm.asInstanceOf[TableController].recreatePossibleCounterUses(msg.getPossibleUsagesList.asScala.map(pu => {
            NITSession.DefenderUsesCard(
              ProtoToCardPosition(pu.getPosition),
              if (pu.hasTarget) {
                Some(ProtoToCardTarget(pu.getTarget))
              } else {
                None
              },
              CardHolder.EMPTY,
              ProtoToNeutralDistribution(pu.getNeutralDistribution),
              List()
            )
          }).toList)
        }
      })
      stay()
    }

    case Event(MessageEvent(msg: DefenderUsesCard, mod, rh, ctx), data) => {
      stay()
    }

    case Event(msg: CounterUsedCardInteraction, data) => {
      val builder = DefenderUsesCard
        .newBuilder()
        .setPosition(CardPositionToProto(msg.position))
        .setNeutralDistribution(NeutralDistributionToProto(msg.neutralDistribution))

      msg.target.map(t => builder.setTarget(CardTargetToProto(t)))
      appContext.webContext.map(ctx => ctx.send(serializeToString(MessageEvent(
        builder.build(), module, msg.UUID
      ))))

      stay using data.copy(requestHandlers = TableInteractionHandlers(msg.UUID, msg.successHandler, msg.failHandler) :: data.requestHandlers)
    }

    case Event(MessageEvent(msg: DefenderUsedCardAccepted, mod, rh, ctx), data) => {
      data.requestHandlers.find(h => h.uuid == rh).map(found => {
        Platform.runLater(new Runnable {
          override def run(): Unit = {
            found.successHandler(msg.getPossibleUsagesList.asScala.map(pu => {
              NITSession.DefenderUsesCard(
                ProtoToCardPosition(pu.getPosition),
                if (pu.hasTarget) {
                  Some(ProtoToCardTarget(pu.getTarget))
                } else {
                  None
                },
                CardPositionToCardHolder(ProtoToCardPosition(pu.getPosition), data.vision),
                ProtoToNeutralDistribution(pu.getNeutralDistribution),
                List()
              )
            }).toList)

          }
        })
      })
      stay using data.copy(requestHandlers = data.requestHandlers.filter(f => f.uuid != rh))
    }

    case Event(MessageEvent(msg: DefenderUsedCardDeclined, mod, rh, ctx), data) => {
      data.requestHandlers.find(h => h.uuid == rh).map(found => {
        Platform.runLater(new Runnable {
          override def run(): Unit = {
            found.failHandler()
          }
        })
      })
      stay using data.copy(requestHandlers = data.requestHandlers.filter(f => f.uuid != rh))
    }

    case Event(MessageEvent(msg: DefenderPossibleCardUses, mod, rh, ctx), data) => {
      Platform.runLater(new Runnable {
        override def run(): Unit = {
          presentedForm.asInstanceOf[TableController].recreatePossibleCounterUses(msg.getPossibleUsagesList.asScala.map(pu => {
            NITSession.DefenderUsesCard(
              ProtoToCardPosition(pu.getPosition),
              if (pu.hasTarget) {
                Some(ProtoToCardTarget(pu.getTarget))
              } else {
                None
              },
              CardPositionToCardHolder(ProtoToCardPosition(pu.getPosition), data.vision),
              ProtoToNeutralDistribution(pu.getNeutralDistribution),
              List()
            )
          }).toList)
        }
      })
      stay()
    }

    case Event(MessageEvent(msg: DefenderEndsTurn, mod, rh, ctx), data) => {
      stay()
    }

    case Event(MessageEvent(msg: StateAsk, mod, rh, ctx), data) => {
      msg.getState match {
        case MatchState.EVALUATION_AND_ANIMATION =>
          val vision = TableStateForPlayerToPlayerVision(msg.getPlayerState, msg.getState, appContext.playerName, msg.getTimeInThisState, msg.getTimeAnchor, playerDecks, cards)
          Platform.runLater(new Runnable {
            override def run(): Unit = {
              var myHand: List[CardHolder] = List()
              var allyHand: List[CardHolder] = List()
              var ownTable: List[CardHolder] = List()
              var allyTable: List[CardHolder] = List()
              var ownGraveyard: List[CardHolder] = List()

              var enemyTable: List[CardHolder] = List()
              var enemyGraveyard: List[CardHolder] = List()

              vision.players.find(p => p.relationship == PlayerRelationship.Me).map(p => {
                myHand = p.hand.toList
                ownTable = ownTable ::: p.onTable.toList
                ownGraveyard = ownGraveyard ::: p.graveyard.toList
              })

              vision.players.find(p => p.relationship == PlayerRelationship.Ally).map(p => {
                allyHand = p.hand.toList
                allyTable = allyTable ::: p.onTable.toList
                ownGraveyard = ownGraveyard ::: p.graveyard.toList
              })

              vision.players.find(p => p.relationship == PlayerRelationship.Enemy1).map(p => {
                enemyTable = enemyTable ::: p.onTable.toList
                enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
              })

              vision.players.find(p => p.relationship == PlayerRelationship.Enemy2).map(p => {
                enemyTable = enemyTable ::: p.onTable.toList
                enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
              })

              presentedForm.asInstanceOf[TableController].actualizeCardsInHand(myHand)
              presentedForm.asInstanceOf[TableController].actualizeOwnTable(ownTable)
              presentedForm.asInstanceOf[TableController].actualizeAllyTable(allyTable)
              presentedForm.asInstanceOf[TableController].actualizeOwnGraveyard(ownGraveyard)
              presentedForm.asInstanceOf[TableController].actualizeEnemyTable(enemyTable)
              presentedForm.asInstanceOf[TableController].actualizeEnemyGraveyard(enemyGraveyard)
              presentedForm.asInstanceOf[TableController].actualizePlayerVision(
                vision
              )
              presentedForm.asInstanceOf[TableController].setTableState(TableState.UseAndCounterUseEvaluating)
              presentedForm.asInstanceOf[TableController].resetPossibilities
            }
          })
          appContext.webContext.map(ctx => ctx.send(serializeToString(MessageEvent(
            StateAck.newBuilder().setState(msg.getState).build(), module
          ))))
          goto(EVALUATION_AND_ANIMATION) using data.copy(vision = vision)
      }
    }
  }

  when(ATTACKER_TEAM_ATTACK) {
    case Event(AttackerAttacksSubRoundEndInteraction, data) => {
      appContext.webContext.map(ctx => ctx.send(serializeToString(MessageEvent(
        AttackerEndsTurn.newBuilder().setUserName(data.appContext.playerName).build(), module
      ))))
      stay()
    }

    case Event(MessageEvent(msg: AskAttackerToAttack, mod, rh, ctx), data) => {
      Platform.runLater(new Runnable {
        override def run(): Unit = {
          presentedForm.asInstanceOf[TableController].recreatePossibleAttacks(msg.getPossibleAttacksList.asScala.map(pu => {
            NITSession.AttackerAttacks(
              ProtoToCardPosition(pu.getPosition),
              CardPositionToCardHolder(ProtoToCardPosition(pu.getPosition), data.vision),
              List()
            )
          }).toList)
          var myHand: List[CardHolder] = List()
          var allyHand: List[CardHolder] = List()
          var ownTable: List[CardHolder] = List()
          var allyTable: List[CardHolder] = List()
          var ownGraveyard: List[CardHolder] = List()

          var enemyTable: List[CardHolder] = List()
          var enemyGraveyard: List[CardHolder] = List()

          data.vision.players.find(p => p.relationship == PlayerRelationship.Me).map(p => {
            myHand = p.hand.toList
            ownTable = ownTable ::: p.onTable.toList
            ownGraveyard = ownGraveyard ::: p.graveyard.toList
          })

          data.vision.players.find(p => p.relationship == PlayerRelationship.Ally).map(p => {
            allyHand = p.hand.toList
            allyTable = allyTable ::: p.onTable.toList
            ownGraveyard = ownGraveyard ::: p.graveyard.toList
          })

          data.vision.players.find(p => p.relationship == PlayerRelationship.Enemy1).map(p => {
            enemyTable = enemyTable ::: p.onTable.toList
            enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
          })

          data.vision.players.find(p => p.relationship == PlayerRelationship.Enemy2).map(p => {
            enemyTable = enemyTable ::: p.onTable.toList
            enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
          })

          presentedForm.asInstanceOf[TableController].actualizeCardsInHand(myHand)
          presentedForm.asInstanceOf[TableController].actualizeOwnTable(ownTable)
          presentedForm.asInstanceOf[TableController].actualizeAllyTable(allyTable)
          presentedForm.asInstanceOf[TableController].actualizeOwnGraveyard(ownGraveyard)
          presentedForm.asInstanceOf[TableController].actualizeEnemyTable(enemyTable)
          presentedForm.asInstanceOf[TableController].actualizeEnemyGraveyard(enemyGraveyard)
          presentedForm.asInstanceOf[TableController].actualizePlayerVision(data.vision)
        }
      })
      stay()
    }

    case Event(msg: AttackedWithCardInteraction, data) => {
      val builder = AttackerAttacks
        .newBuilder()
        .setPosition(CardPositionToProto(msg.position))
        .setId(msg.cardId)
        .setNeutralDistribution(NeutralDistributionToProto(msg.neutralDistribution))
      // .setNeutralDistribution(NeutralDistributionToProto(msg.neutralDistribution))

      appContext.webContext.map(ctx => ctx.send(serializeToString(MessageEvent(
        builder.build(), module, msg.UUID
      ))))

      stay using data.copy(requestHandlers = TableInteractionHandlers(msg.UUID, msg.successHandler, msg.failHandler) :: data.requestHandlers)
    }

    case Event(MessageEvent(msg: PlayerAttackAccepted, mod, rh, ctx), data) => {
      val newVision = data.vision.copy(
        players = data.vision.players.find(p => p.player == msg.getPosition.getPlayer).map(found => {
          val pos = ProtoToCardPosition(msg.getPosition)
          val ch = CardPositionToCardHolder(pos, data.vision)
          found.copy(
            attacking = NITSession.AttackerAttacks(
              pos,
              ch,
              List()
            ) :: found.attacking
          ) :: data.vision.players.filter(p => p.player != msg.getPosition.getPlayer)
        }).getOrElse(data.vision.players)
      )

      data.requestHandlers.find(h => h.uuid == rh).map(found => {
        Platform.runLater(new Runnable {
          override def run(): Unit = {
            found.successHandler(msg.getPossibleAttacksList.asScala.map(pu => {
              NITSession.AttackerAttacks(
                ProtoToCardPosition(pu.getPosition),
                CardPositionToCardHolder(ProtoToCardPosition(pu.getPosition), data.vision),
                List()
              )
            }).toList)
            var myHand: List[CardHolder] = List()
            var allyHand: List[CardHolder] = List()
            var ownTable: List[CardHolder] = List()
            var allyTable: List[CardHolder] = List()
            var ownGraveyard: List[CardHolder] = List()

            var enemyTable: List[CardHolder] = List()
            var enemyGraveyard: List[CardHolder] = List()

            newVision.players.find(p => p.relationship == PlayerRelationship.Me).map(p => {
              myHand = p.hand.toList
              ownTable = ownTable ::: p.onTable.toList
              ownGraveyard = ownGraveyard ::: p.graveyard.toList
            })

            newVision.players.find(p => p.relationship == PlayerRelationship.Ally).map(p => {
              allyHand = p.hand.toList
              allyTable = allyTable ::: p.onTable.toList
              ownGraveyard = ownGraveyard ::: p.graveyard.toList
            })

            newVision.players.find(p => p.relationship == PlayerRelationship.Enemy1).map(p => {
              enemyTable = enemyTable ::: p.onTable.toList
              enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
            })

            newVision.players.find(p => p.relationship == PlayerRelationship.Enemy2).map(p => {
              enemyTable = enemyTable ::: p.onTable.toList
              enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
            })

            presentedForm.asInstanceOf[TableController].actualizeCardsInHand(myHand)
            presentedForm.asInstanceOf[TableController].actualizeOwnTable(ownTable)
            presentedForm.asInstanceOf[TableController].actualizeAllyTable(allyTable)
            presentedForm.asInstanceOf[TableController].actualizeOwnGraveyard(ownGraveyard)
            presentedForm.asInstanceOf[TableController].actualizeEnemyTable(enemyTable)
            presentedForm.asInstanceOf[TableController].actualizeEnemyGraveyard(enemyGraveyard)
            presentedForm.asInstanceOf[TableController].actualizePlayerVision(newVision)
          }
        })
      })
      stay using data.copy(requestHandlers = data.requestHandlers.filter(f => f.uuid != rh), vision = newVision)
    }

    case Event(MessageEvent(msg: PlayerAttackDeclined, mod, rh, ctx), data) => {
      data.requestHandlers.find(h => h.uuid == rh).map(found => {
        Platform.runLater(new Runnable {
          override def run(): Unit = {
            found.failHandler()
          }
        })
      })
      stay using data.copy(requestHandlers = data.requestHandlers.filter(f => f.uuid != rh))
    }

    case Event(MessageEvent(msg: PossibleAttacks, mod, rh, ctx), data) => {
      Platform.runLater(new Runnable {
        override def run(): Unit = {
          presentedForm.asInstanceOf[TableController].recreatePossibleAttacks(msg.getPossibleAttacksList.asScala.map(pu => {
            NITSession.AttackerAttacks(
              ProtoToCardPosition(pu.getPosition),
              CardPositionToCardHolder(ProtoToCardPosition(pu.getPosition), data.vision),
              List()
            )
          }).toList)
        }
      })
      stay()
    }

    case Event(MessageEvent(msg: AttackerAttacks, mod, rh, ctx), data) => {
      stay()
    }

    case Event(MessageEvent(msg: DefenderEndsTurn, mod, rh, ctx), data) => {
      stay()
    }

    case Event(MessageEvent(msg: StateAsk, mod, rh, ctx), data) => {
      msg.getState match {
        case MatchState.DEFENDER_TEAM_DEFENDS =>
          val vision = TableStateForPlayerToPlayerVision(msg.getPlayerState, msg.getState, appContext.playerName, msg.getTimeInThisState, msg.getTimeAnchor, playerDecks, cards)

          Platform.runLater(new Runnable {
            override def run(): Unit = {
              var myHand: List[CardHolder] = List()
              var allyHand: List[CardHolder] = List()
              var ownTable: List[CardHolder] = List()
              var allyTable: List[CardHolder] = List()
              var ownGraveyard: List[CardHolder] = List()

              var enemyTable: List[CardHolder] = List()
              var enemyGraveyard: List[CardHolder] = List()

              vision.players.find(p => p.relationship == PlayerRelationship.Me).map(p => {
                myHand = p.hand.toList
                ownTable = ownTable ::: p.onTable.toList
                ownGraveyard = ownGraveyard ::: p.graveyard.toList
              })

              vision.players.find(p => p.relationship == PlayerRelationship.Ally).map(p => {
                allyHand = p.hand.toList
                allyTable = allyTable ::: p.onTable.toList
                ownGraveyard = ownGraveyard ::: p.graveyard.toList
              })

              vision.players.find(p => p.relationship == PlayerRelationship.Enemy1).map(p => {
                enemyTable = enemyTable ::: p.onTable.toList
                enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
              })

              vision.players.find(p => p.relationship == PlayerRelationship.Enemy2).map(p => {
                enemyTable = enemyTable ::: p.onTable.toList
                enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
              })

              presentedForm.asInstanceOf[TableController].actualizeCardsInHand(myHand)
              presentedForm.asInstanceOf[TableController].actualizeOwnTable(ownTable)
              presentedForm.asInstanceOf[TableController].actualizeAllyTable(allyTable)
              presentedForm.asInstanceOf[TableController].actualizeOwnGraveyard(ownGraveyard)
              presentedForm.asInstanceOf[TableController].actualizeEnemyTable(enemyTable)
              presentedForm.asInstanceOf[TableController].actualizeEnemyGraveyard(enemyGraveyard)
              presentedForm.asInstanceOf[TableController].actualizePlayerVision(
                vision
              )
              presentedForm.asInstanceOf[TableController].setTableState(TableState.Defending)
              presentedForm.asInstanceOf[TableController].resetPossibilities
            }
          })
          appContext.webContext.map(ctx => ctx.send(serializeToString(MessageEvent(
            StateAck.newBuilder().setState(msg.getState).build(), module
          ))))
          goto(DEFENDER_TEAM_DEFENDS) using data.copy(vision = vision)
      }
    }
    /*
    AskAttackerToAttack
    PlayerAttackAccepted
    PlayerAttackDeclined
    PossibleAttacks
    AttackerAttacks
    DefenderEndsTurn
    */
  }

  when(DEFENDER_TEAM_DEFENDS) {
    case Event(DefenderDefendsSubRoundEndInteraction, data) => {
      appContext.webContext.map(ctx => ctx.send(serializeToString(MessageEvent(
        DefenderEndsTurn.newBuilder().setUserName(data.appContext.playerName).build(), module
      ))))
      stay()
    }

    case Event(MessageEvent(msg: DefenderDefends, mod, rh, ctx), data) => {
      stay()
    }

    case Event(MessageEvent(msg: AskAttackerToWaitForDefend, mod, rh, ctx), data) => {
      stay()
    }

    case Event(MessageEvent(msg: AskDefenderToDefend, mod, rh, ctx), data) => {
      Platform.runLater(new Runnable {
        override def run(): Unit = {
          presentedForm.asInstanceOf[TableController].recreatePossibleDefends(msg.getPossibleDefendsList.asScala.map(pu => {
            NITSession.DefenderDefends(
              ProtoToCardPosition(pu.getPosition),
              ProtoToCardTarget(pu.getTarget),
              CardPositionToCardHolder(ProtoToCardPosition(pu.getPosition), data.vision),
              ProtoToNeutralDistribution(pu.getNeutralDistribution),
              List()
            )
          }).toList)

          var myHand: List[CardHolder] = List()
          var allyHand: List[CardHolder] = List()
          var ownTable: List[CardHolder] = List()
          var allyTable: List[CardHolder] = List()
          var ownGraveyard: List[CardHolder] = List()

          var enemyTable: List[CardHolder] = List()
          var enemyGraveyard: List[CardHolder] = List()

          data.vision.players.find(p => p.relationship == PlayerRelationship.Me).map(p => {
            myHand = p.hand.toList
            ownTable = ownTable ::: p.onTable.toList
            ownGraveyard = ownGraveyard ::: p.graveyard.toList
          })

          data.vision.players.find(p => p.relationship == PlayerRelationship.Ally).map(p => {
            allyHand = p.hand.toList
            allyTable = allyTable ::: p.onTable.toList
            ownGraveyard = ownGraveyard ::: p.graveyard.toList
          })

          data.vision.players.find(p => p.relationship == PlayerRelationship.Enemy1).map(p => {
            enemyTable = enemyTable ::: p.onTable.toList
            enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
          })

          data.vision.players.find(p => p.relationship == PlayerRelationship.Enemy2).map(p => {
            enemyTable = enemyTable ::: p.onTable.toList
            enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
          })

          presentedForm.asInstanceOf[TableController].actualizeCardsInHand(myHand)
          presentedForm.asInstanceOf[TableController].actualizeOwnTable(ownTable)
          presentedForm.asInstanceOf[TableController].actualizeAllyTable(allyTable)
          presentedForm.asInstanceOf[TableController].actualizeOwnGraveyard(ownGraveyard)
          presentedForm.asInstanceOf[TableController].actualizeEnemyTable(enemyTable)
          presentedForm.asInstanceOf[TableController].actualizeEnemyGraveyard(enemyGraveyard)
          presentedForm.asInstanceOf[TableController].actualizePlayerVision(data.vision)
        }
      })
      stay()
    }

    case Event(msg: DefendWithCardInteraction, data) => {
      val builder = DefenderDefends
        .newBuilder()
        .setPosition(CardPositionToProto(msg.position))
        .setId(msg.cardId)
        .setNeutralDistribution(NeutralDistributionToProto(msg.neutralDistribution))
        .setTarget(CardTargetToProto(msg.target))

      appContext.webContext.map(ctx => ctx.send(serializeToString(MessageEvent(
        builder.build(), module, msg.UUID
      ))))

      stay using data.copy(requestHandlers = TableInteractionHandlers(msg.UUID, msg.successHandler, msg.failHandler) :: data.requestHandlers)
    }

    case Event(MessageEvent(msg: PlayerDefendsAccepted, mod, rh, ctx), data) => {
      val newVision = data.vision.copy(
        players = data.vision.players.find(p => p.player == msg.getPosition.getPlayer).map(found => {
          val pos = ProtoToCardPosition(msg.getPosition)
          val ch = CardPositionToCardHolder(pos, data.vision)
          found.copy(
            defending = NITSession.DefenderDefends(
              ProtoToCardPosition(msg.getPosition),
              ProtoToCardTarget(msg.getTarget),
              CardPositionToCardHolder(ProtoToCardPosition(msg.getPosition), data.vision),
              ProtoToNeutralDistribution(msg.getNeutralDistribution),
              List()
            ) :: found.defending
          ) :: data.vision.players.filter(p => p.player != msg.getPosition.getPlayer)
        }).getOrElse(data.vision.players)
      )

      data.requestHandlers.find(h => h.uuid == rh).map(found => {
        Platform.runLater(new Runnable {
          override def run(): Unit = {
            found.successHandler(msg.getPossibleDefendsList.asScala.map(pu => {
              NITSession.DefenderDefends(
                ProtoToCardPosition(pu.getPosition),
                ProtoToCardTarget(pu.getTarget),
                CardPositionToCardHolder(ProtoToCardPosition(pu.getPosition), data.vision),
                ProtoToNeutralDistribution(pu.getNeutralDistribution),
                List()
              )
            }).toList)

            var myHand: List[CardHolder] = List()
            var allyHand: List[CardHolder] = List()
            var ownTable: List[CardHolder] = List()
            var allyTable: List[CardHolder] = List()
            var ownGraveyard: List[CardHolder] = List()

            var enemyTable: List[CardHolder] = List()
            var enemyGraveyard: List[CardHolder] = List()

            newVision.players.find(p => p.relationship == PlayerRelationship.Me).map(p => {
              myHand = p.hand.toList
              ownTable = ownTable ::: p.onTable.toList
              ownGraveyard = ownGraveyard ::: p.graveyard.toList
            })

            newVision.players.find(p => p.relationship == PlayerRelationship.Ally).map(p => {
              allyHand = p.hand.toList
              allyTable = allyTable ::: p.onTable.toList
              ownGraveyard = ownGraveyard ::: p.graveyard.toList
            })

            newVision.players.find(p => p.relationship == PlayerRelationship.Enemy1).map(p => {
              enemyTable = enemyTable ::: p.onTable.toList
              enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
            })

            newVision.players.find(p => p.relationship == PlayerRelationship.Enemy2).map(p => {
              enemyTable = enemyTable ::: p.onTable.toList
              enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
            })

            presentedForm.asInstanceOf[TableController].actualizeCardsInHand(myHand)
            presentedForm.asInstanceOf[TableController].actualizeOwnTable(ownTable)
            presentedForm.asInstanceOf[TableController].actualizeAllyTable(allyTable)
            presentedForm.asInstanceOf[TableController].actualizeOwnGraveyard(ownGraveyard)
            presentedForm.asInstanceOf[TableController].actualizeEnemyTable(enemyTable)
            presentedForm.asInstanceOf[TableController].actualizeEnemyGraveyard(enemyGraveyard)
            presentedForm.asInstanceOf[TableController].actualizePlayerVision(newVision)
          }
        })
      })
      stay using data.copy(requestHandlers = data.requestHandlers.filter(f => f.uuid != rh), vision = newVision)
    }

    case Event(MessageEvent(msg: PlayerDefendsDeclined, mod, rh, ctx), data) => {
      data.requestHandlers.find(h => h.uuid == rh).map(found => {
        Platform.runLater(new Runnable {
          override def run(): Unit = {
            found.failHandler()
          }
        })
      })
      stay using data.copy(requestHandlers = data.requestHandlers.filter(f => f.uuid != rh))
    }

    case Event(MessageEvent(msg: PossibleDefending, mod, rh, ctx), data) => {
      Platform.runLater(new Runnable {
        override def run(): Unit = {
          presentedForm.asInstanceOf[TableController].recreatePossibleDefends(msg.getPossibleDefendsList.asScala.map(pu => {
            NITSession.DefenderDefends(
              ProtoToCardPosition(pu.getPosition),
              ProtoToCardTarget(pu.getTarget),
              CardPositionToCardHolder(ProtoToCardPosition(pu.getPosition), data.vision),
              ProtoToNeutralDistribution(pu.getNeutralDistribution),
              List()
            )
          }).toList)
        }
      })
      stay()
    }


    case Event(MessageEvent(msg: StateAsk, mod, rh, ctx), data) => {
      msg.getState match {
        case MatchState.EVALUATION_AND_ANIMATION =>
          val vision = TableStateForPlayerToPlayerVision(msg.getPlayerState, msg.getState, appContext.playerName, msg.getTimeInThisState, msg.getTimeAnchor, playerDecks, cards)

          Platform.runLater(new Runnable {
            override def run(): Unit = {
              var myHand: List[CardHolder] = List()
              var allyHand: List[CardHolder] = List()
              var ownTable: List[CardHolder] = List()
              var allyTable: List[CardHolder] = List()
              var ownGraveyard: List[CardHolder] = List()

              var enemyTable: List[CardHolder] = List()
              var enemyGraveyard: List[CardHolder] = List()

              vision.players.find(p => p.relationship == PlayerRelationship.Me).map(p => {
                myHand = p.hand.toList
                ownTable = ownTable ::: p.onTable.toList
                ownGraveyard = ownGraveyard ::: p.graveyard.toList
              })

              vision.players.find(p => p.relationship == PlayerRelationship.Ally).map(p => {
                allyHand = p.hand.toList
                allyTable = allyTable ::: p.onTable.toList
                ownGraveyard = ownGraveyard ::: p.graveyard.toList
              })

              vision.players.find(p => p.relationship == PlayerRelationship.Enemy1).map(p => {
                enemyTable = enemyTable ::: p.onTable.toList
                enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
              })

              vision.players.find(p => p.relationship == PlayerRelationship.Enemy2).map(p => {
                enemyTable = enemyTable ::: p.onTable.toList
                enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
              })

              presentedForm.asInstanceOf[TableController].actualizeCardsInHand(myHand)
              presentedForm.asInstanceOf[TableController].actualizeOwnTable(ownTable)
              presentedForm.asInstanceOf[TableController].actualizeAllyTable(allyTable)
              presentedForm.asInstanceOf[TableController].actualizeOwnGraveyard(ownGraveyard)
              presentedForm.asInstanceOf[TableController].actualizeEnemyTable(enemyTable)
              presentedForm.asInstanceOf[TableController].actualizeEnemyGraveyard(enemyGraveyard)
              presentedForm.asInstanceOf[TableController].actualizePlayerVision(
                vision
              )
              presentedForm.asInstanceOf[TableController].setTableState(TableState.AttackAndDefendEvaluating)
              presentedForm.asInstanceOf[TableController].resetPossibilities
            }
          })
          appContext.webContext.map(ctx => ctx.send(serializeToString(MessageEvent(
            StateAck.newBuilder().setState(msg.getState).build(), module
          ))))
          goto(EVALUATION_AND_ANIMATION) using data.copy(vision = vision)
      }
    }

    /*
    DefenderDefends
    AskAttackerToWaitForDefend
    AskDefenderToDefend
    PlayerDefendsAccepted
    PlayerDefendsDeclined
    PossibleDefending
    * */
  }

  when(EVALUATION_AND_ANIMATION) {

    case Event(MessageEvent(msg: EvaluateUsageDone, mod, rh, ctx), data) => {
      var newVision: PlayerVision = data.vision
      Platform.runLater(new Runnable {
        override def run(): Unit = {

          newVision = ChangesToPlayersVision(msg.getChangesList.asScala.toList, newVision)

          var myHand: List[CardHolder] = List()
          var allyHand: List[CardHolder] = List()
          var ownTable: List[CardHolder] = List()
          var allyTable: List[CardHolder] = List()
          var ownGraveyard: List[CardHolder] = List()

          var enemyTable: List[CardHolder] = List()
          var enemyGraveyard: List[CardHolder] = List()

          newVision.players.find(p => p.relationship == PlayerRelationship.Me).map(p => {
            myHand = p.hand.toList
            ownTable = ownTable ::: p.onTable.toList
            ownGraveyard = ownGraveyard ::: p.graveyard.toList
          })

          newVision.players.find(p => p.relationship == PlayerRelationship.Ally).map(p => {
            allyHand = p.hand.toList
            allyTable = allyTable ::: p.onTable.toList
            ownGraveyard = ownGraveyard ::: p.graveyard.toList
          })

          newVision.players.find(p => p.relationship == PlayerRelationship.Enemy1).map(p => {
            enemyTable = enemyTable ::: p.onTable.toList
            enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
          })

          newVision.players.find(p => p.relationship == PlayerRelationship.Enemy2).map(p => {
            enemyTable = enemyTable ::: p.onTable.toList
            enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
          })

          presentedForm.asInstanceOf[TableController].actualizeCardsInHand(myHand)
          presentedForm.asInstanceOf[TableController].actualizeOwnTable(ownTable)
          presentedForm.asInstanceOf[TableController].actualizeAllyTable(allyTable)
          presentedForm.asInstanceOf[TableController].actualizeOwnGraveyard(ownGraveyard)
          presentedForm.asInstanceOf[TableController].actualizeEnemyTable(enemyTable)
          presentedForm.asInstanceOf[TableController].actualizeEnemyGraveyard(enemyGraveyard)
          presentedForm.asInstanceOf[TableController].actualizePlayerVision(
            newVision
          )


          appContext.webContext.map(ctx => ctx.send(serializeToString(MessageEvent(
            AnimationDone.newBuilder().build(), module
          ))))
        }
      })

      stay using (data.copy(vision = newVision))
    }

    case Event(MessageEvent(msg: EvaluateAttackDone, mod, rh, ctx), data) => {
      var newVision: PlayerVision = data.vision
      Platform.runLater(new Runnable {
        override def run(): Unit = {
          newVision = ChangesToPlayersVision(msg.getChangesList.asScala.toList, newVision)

          var myHand: List[CardHolder] = List()
          var allyHand: List[CardHolder] = List()
          var ownTable: List[CardHolder] = List()
          var allyTable: List[CardHolder] = List()
          var ownGraveyard: List[CardHolder] = List()

          var enemyTable: List[CardHolder] = List()
          var enemyGraveyard: List[CardHolder] = List()

          newVision.players.find(p => p.relationship == PlayerRelationship.Me).map(p => {
            myHand = p.hand.toList
            ownTable = ownTable ::: p.onTable.toList
            ownGraveyard = ownGraveyard ::: p.graveyard.toList
          })

          newVision.players.find(p => p.relationship == PlayerRelationship.Ally).map(p => {
            allyHand = p.hand.toList
            allyTable = allyTable ::: p.onTable.toList
            ownGraveyard = ownGraveyard ::: p.graveyard.toList
          })

          newVision.players.find(p => p.relationship == PlayerRelationship.Enemy1).map(p => {
            enemyTable = enemyTable ::: p.onTable.toList
            enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
          })

          newVision.players.find(p => p.relationship == PlayerRelationship.Enemy2).map(p => {
            enemyTable = enemyTable ::: p.onTable.toList
            enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
          })

          presentedForm.asInstanceOf[TableController].actualizeCardsInHand(myHand)
          presentedForm.asInstanceOf[TableController].actualizeOwnTable(ownTable)
          presentedForm.asInstanceOf[TableController].actualizeAllyTable(allyTable)
          presentedForm.asInstanceOf[TableController].actualizeOwnGraveyard(ownGraveyard)
          presentedForm.asInstanceOf[TableController].actualizeEnemyTable(enemyTable)
          presentedForm.asInstanceOf[TableController].actualizeEnemyGraveyard(enemyGraveyard)
          presentedForm.asInstanceOf[TableController].actualizePlayerVision(
            newVision
          )


          appContext.webContext.map(ctx => ctx.send(serializeToString(MessageEvent(
            AnimationDone.newBuilder().build(), module
          ))))
        }
      })

      stay using (data.copy(vision = newVision))
    }

    case Event(MessageEvent(msg: StateAsk, mod, rh, ctx), data) => {
      msg.getState match {
        case MatchState.ATTACKER_TEAM_ATTACK =>
          val vision = TableStateForPlayerToPlayerVision(msg.getPlayerState, msg.getState, appContext.playerName, msg.getTimeInThisState, msg.getTimeAnchor, playerDecks, cards)

          Platform.runLater(new Runnable {
            override def run(): Unit = {
              var myHand: List[CardHolder] = List()
              var allyHand: List[CardHolder] = List()
              var ownTable: List[CardHolder] = List()
              var allyTable: List[CardHolder] = List()
              var ownGraveyard: List[CardHolder] = List()

              var enemyTable: List[CardHolder] = List()
              var enemyGraveyard: List[CardHolder] = List()

              vision.players.find(p => p.relationship == PlayerRelationship.Me).map(p => {
                myHand = p.hand.toList
                ownTable = ownTable ::: p.onTable.toList
                ownGraveyard = ownGraveyard ::: p.graveyard.toList
              })

              vision.players.find(p => p.relationship == PlayerRelationship.Ally).map(p => {
                allyHand = p.hand.toList
                allyTable = allyTable ::: p.onTable.toList
                ownGraveyard = ownGraveyard ::: p.graveyard.toList
              })

              vision.players.find(p => p.relationship == PlayerRelationship.Enemy1).map(p => {
                enemyTable = enemyTable ::: p.onTable.toList
                enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
              })

              vision.players.find(p => p.relationship == PlayerRelationship.Enemy2).map(p => {
                enemyTable = enemyTable ::: p.onTable.toList
                enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
              })

              presentedForm.asInstanceOf[TableController].actualizeCardsInHand(myHand)
              presentedForm.asInstanceOf[TableController].actualizeOwnTable(ownTable)
              presentedForm.asInstanceOf[TableController].actualizeAllyTable(allyTable)
              presentedForm.asInstanceOf[TableController].actualizeOwnGraveyard(ownGraveyard)
              presentedForm.asInstanceOf[TableController].actualizeEnemyTable(enemyTable)
              presentedForm.asInstanceOf[TableController].actualizeEnemyGraveyard(enemyGraveyard)
              presentedForm.asInstanceOf[TableController].actualizePlayerVision(
                vision
              )
              presentedForm.asInstanceOf[TableController].setTableState(TableState.Attacking)
              presentedForm.asInstanceOf[TableController].resetPossibilities
            }
          })
          appContext.webContext.map(ctx => ctx.send(serializeToString(MessageEvent(
            StateAck.newBuilder().setState(msg.getState).build(), module
          ))))
          goto(ATTACKER_TEAM_ATTACK) using data.copy(vision = vision)

        case MatchState.FORTUNE_ROLL =>
          val vision = TableStateForPlayerToPlayerVision(msg.getPlayerState, msg.getState, appContext.playerName, msg.getTimeInThisState, msg.getTimeAnchor, playerDecks, cards)

          Platform.runLater(new Runnable {
            override def run(): Unit = {
              var myHand: List[CardHolder] = List()
              var allyHand: List[CardHolder] = List()
              var ownTable: List[CardHolder] = List()
              var allyTable: List[CardHolder] = List()
              var ownGraveyard: List[CardHolder] = List()

              var enemyTable: List[CardHolder] = List()
              var enemyGraveyard: List[CardHolder] = List()

              vision.players.find(p => p.relationship == PlayerRelationship.Me).map(p => {
                myHand = p.hand.toList
                ownTable = ownTable ::: p.onTable.toList
                ownGraveyard = ownGraveyard ::: p.graveyard.toList
              })

              vision.players.find(p => p.relationship == PlayerRelationship.Ally).map(p => {
                allyHand = p.hand.toList
                allyTable = allyTable ::: p.onTable.toList
                ownGraveyard = ownGraveyard ::: p.graveyard.toList
              })

              vision.players.find(p => p.relationship == PlayerRelationship.Enemy1).map(p => {
                enemyTable = enemyTable ::: p.onTable.toList
                enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
              })

              vision.players.find(p => p.relationship == PlayerRelationship.Enemy2).map(p => {
                enemyTable = enemyTable ::: p.onTable.toList
                enemyGraveyard = enemyGraveyard ::: p.graveyard.toList
              })

              presentedForm.asInstanceOf[TableController].actualizeCardsInHand(myHand)
              presentedForm.asInstanceOf[TableController].actualizeOwnTable(ownTable)
              presentedForm.asInstanceOf[TableController].actualizeAllyTable(allyTable)
              presentedForm.asInstanceOf[TableController].actualizeOwnGraveyard(ownGraveyard)
              presentedForm.asInstanceOf[TableController].actualizeEnemyTable(enemyTable)
              presentedForm.asInstanceOf[TableController].actualizeEnemyGraveyard(enemyGraveyard)
              presentedForm.asInstanceOf[TableController].actualizePlayerVision(
                vision
              )
              presentedForm.asInstanceOf[TableController].setTableState(TableState.FortuneRolling)
              presentedForm.asInstanceOf[TableController].resetPossibilities
            }
          })
          appContext.webContext.map(ctx => ctx.send(serializeToString(MessageEvent(
            StateAck.newBuilder().setState(msg.getState).build(), module
          ))))
          goto(FORTUNE_ROLL) using data.copy(vision = vision)
      }
    }
  }

  whenUnhandled {
    case Event(CloseFormInteraction, data) => {
      applicationInstanceController ! CloseFormInteraction
      stay()
    }

    case Event(CloseForm, data) => {
      Platform.runLater(new Runnable {
        override def run(): Unit = {
          presentedForm.asInstanceOf[TableController].close
        }
      })
     stop()
    }


    case Event(MessageEvent(msg: MatchEnded, mod, rh, ctx), data) => {
      Platform.runLater(new Runnable {
        override def run(): Unit = {
          presentedForm.asInstanceOf[TableController].showMatchEnds(msg.getWin)
          presentedForm.asInstanceOf[TableController].showGains(msg.getMmrChange, msg.getEssences)
        }
      })
      stay()
    }

    case Event(SendMessageInteraction(msg, all), data) => {
      allChatConversationId.map(allConv => {
        allyChatConversationId.map(allyConv => {
          appContext.webContext.map(ctx => ctx.send(serializeToString(MessageEvent(
            SendMessage
              .newBuilder()
              .setConversationId(
                if (all) {
                  allConv
                } else {
                  allyConv
                }
              )
              .setMessage(
                msg
              ).build(), "FirstEnchanterConversation"
          ))))
        })
      })
      stay()
    }

    case Event(MessageEvent(msg: ConversationJoinSuccess, mod, rh, ctx), data) => {
      msg.getConversation.getConversationType match {
        case Conversation.ConversationType.GameAllyConversationType =>
          allyChatConversationId = Some(msg.getConversation.getId)
          appContext.webContext.map(ctx => ctx.send(serializeToString(MessageEvent(
            GetConversationHistory.newBuilder().setConversationId(msg.getConversation.getId).build(), mod
          ))))
        case Conversation.ConversationType.GameAllConversationType =>
          allChatConversationId = Some(msg.getConversation.getId)
          appContext.webContext.map(ctx => ctx.send(serializeToString(MessageEvent(
            GetConversationHistory.newBuilder().setConversationId(msg.getConversation.getId).build(), mod
          ))))
        case t =>
      }
      stay()
    }

    case Event(MessageEvent(ch: ConversationHistory, mod, rh, ctx), data) => {
      allChatConversationId.map(allConv => {
        allyChatConversationId.map(allyConv => {
          Platform.runLater(new Runnable {
            override def run(): Unit = {
              ch.getMessagesList.asScala.map(msg => {
                data.vision.players.find(p => p.player == msg.getBy).map(p => {
                  presentedForm.asInstanceOf[TableController].addMessage(ChatMessage(
                    msg.getBy,
                    msg.getTime,
                    msg.getContent,
                    p.relationship,
                    msg.getPredefined,
                    if (ch.getConversationId == allConv) {
                      ChatMode.All
                    } else {
                      ChatMode.Team
                    }
                  ))
                })
              })
            }
          })
        })
      })
      stay()
    }

    case Event(MessageEvent(msg: ReceivedMessage, mod, rh, ctx), data) => {
      allChatConversationId.map(allConv => {
        allyChatConversationId.map(allyConv => {
          Platform.runLater(new Runnable {
            override def run(): Unit = {
              data.vision.players.find(p => p.player == msg.getMessage.getBy).map(p => {
                presentedForm.asInstanceOf[TableController].addMessage(ChatMessage(
                  msg.getMessage.getBy,
                  msg.getMessage.getTime,
                  msg.getMessage.getContent,
                  p.relationship,
                  msg.getMessage.getPredefined,
                  if (msg.getConversationId == allConv) {
                    ChatMode.All
                  } else {
                    ChatMode.Team
                  }
                ))
              })
            }
          })
        })
      })
      stay()
    }

    case Event(e, data) => {
      log.error("Error in state" + stateName + "while consuming message" + e + "by TableActor")
      stay()
    }
  }
}

object CardPositionToCardHolder {
  def apply(position: NITCard.CardPosition, vision: List[PlayerSeesPlayer]): CardHolder = {
    position match {
      case ihcp: NITCard.InHandCardPosition =>
        vision.find(p => p.player == ihcp.player).map(found => if (
          ihcp.positionId + 1 <= found.hand.size
        ) {
          Some(found.hand(ihcp.positionId))
        } else {
          None
        }).flatten.getOrElse(CardHolder.EMPTY)

      case otcp: NITCard.OnTableCardPosition =>
        vision.find(p => p.player == otcp.player).map(found => if (
          otcp.positionId + 1 <= found.onTable.size
        ) {
          Some(found.onTable(otcp.positionId))
        } else {
          None
        }).flatten.getOrElse(CardHolder.EMPTY)

      case ogcp: NITCard.OnGraveyardPosition =>
        vision.find(p => p.player == ogcp.player).map(found => if (
          ogcp.positionId + 1 <= found.graveyard.size
        ) {
          Some(found.graveyard(ogcp.positionId))
        } else {
          None
        }).flatten.getOrElse(CardHolder.EMPTY)
    }
  }

  def apply(position: NITCard.CardPosition, vision: PlayerVision): CardHolder = {
    CardPositionToCardHolder(position, vision.players)
  }
}

object CardTargetToCardHolder {
  def apply(target: NITCard.CardTarget, players: List[PlayerSeesPlayer]): Option[CardHolder] = {
    target match {
      case t: NITCard.CardIsTarget => Some(CardPositionToCardHolder(t.position, players))
      case t => None
    }
  }
}

object CardHolderToCardPosition {
  def apply(card: CardHolder, players: List[PlayerSeesPlayer]): Option[NITCard.CardPosition] = {
    players.map(p =>
      if (p.hand.contains(card)) {
        Some(NITCard.InHandCardPosition(p.hand.indexOf(card), p.player))
      } else if (p.onTable contains (card)) {
        Some(NITCard.OnTableCardPosition(p.onTable.indexOf(card), p.player))
      } else if (p.graveyard.contains(card)) {
        Some(NITCard.OnGraveyardPosition(p.graveyard.indexOf(card), p.player))
      } else {
        None
      }
    ).filter(f => !f.isEmpty).head
  }
}

object TableStateForPlayerToPlayerVision {
  def apply(ts: TableStateForPlayer, matchState: MatchState, playerName: String, timeInState: Long, timeAnchor: Long, playerDecks: List[Pair[String, Deck]], loadedCards: List[Card]): PlayerVision = {

    val playersTurn = ts.getPlayersTurnList.asScala
    val playersNotTurn = ts.getPlayersList.asScala.filter(f => !playersTurn.contains(f.getPlayer)).map(p => p.getPlayer)

    val isAttacker = matchState match {
      case MatchState.FORTUNE_ROLL =>
        playersTurn.contains(playerName)

      case MatchState.PLAYER_STARTS_ROLL =>
        playersTurn.contains(playerName)

      case MatchState.ATTACKER_TEAM_PLAY =>
        playersTurn.contains(playerName)

      case MatchState.DEFENDER_TEAM_COUNTER_PLAY =>
        !playersTurn.contains(playerName)

      case MatchState.ATTACKER_TEAM_ATTACK =>
        playersTurn.contains(playerName)

      case MatchState.DEFENDER_TEAM_DEFENDS =>
        !playersTurn.contains(playerName)

      case MatchState.EVALUATION_AND_ANIMATION =>
        playersTurn.contains(playerName)
    }


    val teamMates = if (playersTurn.contains(playerName)) {
      playersTurn
    } else {
      playersNotTurn
    }

    val attackedCardsList = ts.getAttackedCardsList.asScala.map(ac => {
      val deck = playerDecks.find(d => d._1 == ac.getPosition.getPlayer).map(p => p._2).getOrElse(Deck.Empty)
      val c = loadedCards.find(c => c.id == ac.getId).map(c => CardHolderBuilder(c, deck)).getOrElse(CardHolder.EMPTY)
      NITSession.AttackerAttacks(
        ProtoToCardPosition(ac.getPosition),
        c,
        List()
      )
    }).toList

    val defenderCardList = ts.getDefendedCardsList.asScala.map(dc => {
      val deck = playerDecks.find(d => d._1 == dc.getPosition.getPlayer).map(p => p._2).getOrElse(Deck.Empty)
      val c = loadedCards.find(c => c.id == dc.getId).map(c => CardHolderBuilder(c, deck)).getOrElse(CardHolder.EMPTY)
      NITSession.DefenderDefends(
        ProtoToCardPosition(dc.getPosition),
        ProtoToCardTarget(dc.getTarget),
        c,
        ProtoToNeutralDistribution(dc.getNeutralDistribution),
        List()
      )
    }).toList

    val useCardList = ts.getUsedCardsList.asScala.map(ucl => {
      val deck = playerDecks.find(d => d._1 == ucl.getPosition.getPlayer).map(p => p._2).getOrElse(Deck.Empty)
      val c = loadedCards.find(c => c.id == ucl.getId).map(c => CardHolderBuilder(c, deck)).getOrElse(CardHolder.EMPTY)
      NITSession.AttackerUsesCard(
        ProtoToCardPosition(ucl.getPosition),
        if (ucl.hasTarget) {
          Some(ProtoToCardTarget(ucl.getTarget))
        } else {
          None
        },
        c,
        ProtoToNeutralDistribution(ucl.getNeutralDistribution),
        List()
      )
    }).toList

    val counterUsedCardList = ts.getCounterUsedCardsList.asScala.map(ucl => {
      val deck = playerDecks.find(d => d._1 == ucl.getPosition.getPlayer).map(p => p._2).getOrElse(Deck.Empty)
      val c = loadedCards.find(c => c.id == ucl.getId).map(c => CardHolderBuilder(c, deck)).getOrElse(CardHolder.EMPTY)
      NITSession.DefenderUsesCard(
        ProtoToCardPosition(ucl.getPosition),
        if (ucl.hasTarget) {
          Some(ProtoToCardTarget(ucl.getTarget))
        } else {
          None
        },
        c,
        ProtoToNeutralDistribution(ucl.getNeutralDistribution),
        List()
      )
    }).toList

    var enemies = 0


    val state = matchState match {
      case MatchState.FORTUNE_ROLL =>
        TableState.FortuneRolling

      case MatchState.PLAYER_STARTS_ROLL =>
        TableState.StartRolling

      case MatchState.ATTACKER_TEAM_PLAY =>
        if (isAttacker) {
          TableState.Using
        } else {
          TableState.WaitingForEnemyUse
        }

      case MatchState.DEFENDER_TEAM_COUNTER_PLAY =>
        if (isAttacker) {
          TableState.WaitingForEnemyCounterUse
        } else {
          TableState.CounterUsing
        }

      case MatchState.ATTACKER_TEAM_ATTACK =>
        if (isAttacker) {
          TableState.Attacking
        } else {
          TableState.WaitingForEnemyAttack
        }

      case MatchState.DEFENDER_TEAM_DEFENDS =>
        if (isAttacker) {
          TableState.WaitingForEnemyDefend
        } else {
          TableState.Defending
        }

      case MatchState.EVALUATION_AND_ANIMATION =>
        TableState.AttackAndDefendEvaluating
    }

    PlayerVision(
      ts.getPlayersList.asScala.map(pl => {
        val deck = playerDecks.find(d => d._1 == pl.getPlayer).map(p => p._2).getOrElse(Deck.Empty)

        val hands: Array[CardHolder] = (for (i <- 1 to pl.getVisibleHandsSize) yield CardHolder.EMPTY).toArray
        val table: Array[CardHolder] = (for (i <- 1 to pl.getVisibleTableSize) yield CardHolder.EMPTY).toArray
        val graveyard: Array[CardHolder] = (for (i <- 1 to pl.getVisibleGraveyardSize) yield CardHolder.EMPTY).toArray

        pl.getCardsList.asScala.map(c => {
          c.getPosition.getPlace match {
            case CardPosition.CardPositionPlace.IN_HAND =>
              loadedCards.find(lc => lc.id == c.getCard.getCardId).map(found => {
                hands(c.getPosition.getPlaceId) = CardHolderBuilder(found, deck)
              })

            case CardPosition.CardPositionPlace.ON_TABLE =>
              loadedCards.find(lc => lc.id == c.getCard.getCardId).map(found => {
                table(c.getPosition.getPlaceId) = CardHolderBuilder(found, deck)
              })

            case CardPosition.CardPositionPlace.ON_GRAVEYARD =>
              loadedCards.find(lc => lc.id == c.getCard.getCardId).map(found => {
                graveyard(c.getPosition.getPlaceId) = CardHolderBuilder(found, deck)
              })
          }
        })

        PlayerSeesPlayer(
          pl.getPlayer,
          hands,
          table,
          graveyard,
          useCardList.filter(f => f.onPosition.player == pl.getPlayer),
          counterUsedCardList.filter(f => f.onPosition.player == pl.getPlayer),
          attackedCardsList.filter(f => f.withPosition.player == pl.getPlayer),
          defenderCardList.filter(f => f.withPosition.player == pl.getPlayer),
          CharmOfElements(pl.getElements.getFire, pl.getElements.getWater, pl.getElements.getEarth, pl.getElements.getAir),
          if (playerName == pl.getPlayer) {
            PlayerRelationship.Me
          } else if (teamMates contains (pl.getPlayer)) {
            PlayerRelationship.Ally
          } else if (enemies == 0) {
            enemies = 1
            PlayerRelationship.Enemy1
          } else {
            PlayerRelationship.Enemy2
          },
          if (playersTurn.contains(pl.getPlayer)) {
            ts.getFortuneOnAttackerSide
          } else {
            !ts.getFortuneOnAttackerSide
          },
          pl.getVisibleDecksSize,
          pl.getVisibleHandsSize,
          pl.getVisibleGraveyardSize
        )
      }).toList,
      state,
      ts.getMyTeamHp,
      ts.getEnemyTeamHp,
      ts.getRoundNumber,
      playersTurn.toList,
      playersNotTurn.toList,
      timeInState,
      timeAnchor,
      false,
      false
    )

  }
}

object AppendElementToCardHolderArray {
  def apply(element: CardHolder, position: Int, array: Array[CardHolder]): Array[CardHolder] = {
    if (position >= array.size) {
      array :+ element
    } else if (position == 0) {
      element +: array
    } else {
      val splited = array.splitAt(position)
      (splited._1.slice(0, splited._1.size - 1) :+ element) ++ splited._2
    }
  }
}

object ChangesToPlayersVision {
  def apply(changes: List[TableChange], vision: PlayerVision): PlayerVision = {
    var temporaryVision = vision
    changes.map(sChange => {
      val change: Option[com.google.protobuf.Message] = try {

        val classOfProto = Class.forName(sChange.getChangePackage() + ".FirstEnchanterMatch$" + sChange.getChangeName + "$Builder")

        val merge = classOfProto.getMethod("mergeFrom", classOf[Array[Byte]])
        val build = classOfProto.getMethod("build")

        val declaredConstructors = classOfProto.getDeclaredConstructors;

        declaredConstructors.map(c => {
          c.setAccessible(true)
        })

        declaredConstructors.find(c => c.getParameterTypes.length == 0).map(bdinstance => {
          val builder = bdinstance.newInstance()

          merge.invoke(builder, sChange.getSerializedChange.toByteArray())

          Some(build.invoke(builder).asInstanceOf[com.google.protobuf.Message])
        }).getOrElse({
          None
        })
      } catch {
        case t: Throwable =>
          //    error("Parsing message failed!: " + t.getMessage)
          t.printStackTrace()
          None
      }

      change.map {
        case ch: StayOnTable =>
          temporaryVision = temporaryVision.copy(players = temporaryVision.players.find(p => p.player == ch.getPosition.getPlayer).map(p => {
            ch.getPosition.getPlace match {
              case CardPositionPlace.IN_HAND =>
                val cardHolder = p.hand(ch.getPosition.getPlaceId)
                p.hand(ch.getPosition.getPlaceId) = CardHolder.EMPTY
                p.copy(onTable = p.onTable :+ cardHolder)

              case CardPositionPlace.ON_TABLE =>
                p.copy()

              case CardPositionPlace.ON_GRAVEYARD =>
                val cardHolder = p.graveyard(ch.getPosition.getPlaceId)
                val newGraveyard = p.graveyard.filter(g => g == cardHolder)
                p.copy(graveyard = newGraveyard, onTable = p.onTable :+ cardHolder)
            }
          }).toList ::: temporaryVision.players.filter(p => p.player != ch.getPosition.getPlayer))

        case ch: PlayerPoolReceivedDamage =>
          temporaryVision.players.find(p => p.player == ch.getPlayersList.asScala.head).map(p => {
            if (p.relationship == PlayerRelationship.Me || p.relationship == PlayerRelationship.Ally) {
              temporaryVision = temporaryVision.copy(myTeamHp = temporaryVision.myTeamHp - ch.getValue)
            } else {
              temporaryVision = temporaryVision.copy(enemyTeamHp = temporaryVision.enemyTeamHp - ch.getValue)
            }
          })

        case ch: PlayerPoolReceivedHeal =>
          temporaryVision.players.find(p => p.player == ch.getPlayersList.asScala.head).map(p => {
            if (p.relationship == PlayerRelationship.Me || p.relationship == PlayerRelationship.Ally) {
              temporaryVision = temporaryVision.copy(myTeamHp = temporaryVision.myTeamHp + ch.getValue)
            } else {
              temporaryVision = temporaryVision.copy(enemyTeamHp = temporaryVision.enemyTeamHp + ch.getValue)
            }
          })

        case ch: CardChangeAttack =>
          temporaryVision = temporaryVision.copy(players = temporaryVision.players.find(p => p.player == ch.getPosition.getPlayer).map(p => {
            ch.getPosition.getPlace match {
              case CardPositionPlace.IN_HAND =>
                p.hand(ch.getPosition.getPlaceId) = p.hand(ch.getPosition.getPlaceId).copy(attack = p.hand(ch.getPosition.getPlaceId).attack + ch.getValue)
                p

              case CardPositionPlace.ON_TABLE =>
                p.onTable(ch.getPosition.getPlaceId) = p.onTable(ch.getPosition.getPlaceId).copy(attack = p.onTable(ch.getPosition.getPlaceId).attack + ch.getValue)
                p

              case CardPositionPlace.ON_GRAVEYARD =>
                p.graveyard(ch.getPosition.getPlaceId) = p.graveyard(ch.getPosition.getPlaceId).copy(attack = p.graveyard(ch.getPosition.getPlaceId).attack + ch.getValue)
                p
            }
          }).toList ::: temporaryVision.players.filter(p => p.player != ch.getPosition.getPlayer))

        case ch: CardChangeDefense =>
          temporaryVision = temporaryVision.copy(players = temporaryVision.players.find(p => p.player == ch.getPosition.getPlayer).map(p => {
            ch.getPosition.getPlace match {
              case CardPositionPlace.IN_HAND =>
                p.hand(ch.getPosition.getPlaceId) = p.hand(ch.getPosition.getPlaceId).copy(defense = p.hand(ch.getPosition.getPlaceId).defense + ch.getValue)
                p

              case CardPositionPlace.ON_TABLE =>
                p.onTable(ch.getPosition.getPlaceId) = p.onTable(ch.getPosition.getPlaceId).copy(defense = p.onTable(ch.getPosition.getPlaceId).defense + ch.getValue)
                p

              case CardPositionPlace.ON_GRAVEYARD =>
                p.graveyard(ch.getPosition.getPlaceId) = p.graveyard(ch.getPosition.getPlaceId).copy(defense = p.graveyard(ch.getPosition.getPlaceId).defense + ch.getValue)
                p
            }
          }).toList ::: temporaryVision.players.filter(p => p.player != ch.getPosition.getPlayer))

        case ch: CardBuffed =>

        case ch: CardConsumed =>/*
          temporaryVision = temporaryVision.copy(players = temporaryVision.players.find(p => p.player == ch.getPosition.getPlayer).map(p => {
            ch.getPosition.getPlace match {
              case CardPositionPlace.IN_HAND =>
                p.copy(
                  graveyard = p.graveyard :+ p.hand(ch.getPosition.getPlaceId),
                  hand = p.hand.filter(c => c != p.hand(ch.getPosition.getPlaceId))
                )

              case CardPositionPlace.ON_TABLE =>
                p.onTable(ch.getPosition.getPlaceId) = CardHolder.EMPTY
                p.copy(graveyard = p.graveyard :+ p.hand(ch.getPosition.getPlaceId))

              case CardPositionPlace.ON_GRAVEYARD =>
                p
            }
          }).toList ::: temporaryVision.players.filter(p => p.player != ch.getPosition.getPlayer))
*/
        case ch: CardDestroyedBy =>/*
          temporaryVision = temporaryVision.copy(players = temporaryVision.players.find(p => p.player == ch.getPosition.getPlayer).map(p => {
            ch.getPosition.getPlace match {
              case CardPositionPlace.IN_HAND =>
                p.copy(
                  graveyard = p.graveyard :+ p.hand(ch.getPosition.getPlaceId),
                  hand = p.hand.filter(c => c != p.hand(ch.getPosition.getPlaceId))
                )

              case CardPositionPlace.ON_TABLE =>

                p.onTable(ch.getPosition.getPlaceId) = CardHolder.EMPTY
                p.copy(graveyard = p.graveyard :+ p.hand(ch.getPosition.getPlaceId))

              case CardPositionPlace.ON_GRAVEYARD =>
                p
            }
          }).toList ::: temporaryVision.players.filter(p => p.player != ch.getPosition.getPlayer))
*/
        case ch: CardDefendedAgainst =>

        case ch: CardAttackCancel =>

        case ch: CardGenerated =>

        case ch: ElementsGenerated =>

        case ch: ElementsAbsorbed =>

        case ch: ElementsTransformed =>

        case ch: PlayersLost =>
          temporaryVision = temporaryVision.copy(
            matchEnded = true,
            win = !ch.getPlayersList().asScala.contains(temporaryVision.me.player)
          )

        case ch: CardStripEquipment =>

        case ch: AddedEquipmentToCard =>
      }
    })
    temporaryVision
  }
}