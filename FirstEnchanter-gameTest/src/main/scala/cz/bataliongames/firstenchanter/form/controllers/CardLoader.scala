package cz.bataliongames.firstenchanter.form.controllers

import java.io.{FileReader, BufferedReader, File}

import cz.noveit.games.cardgame.adapters.{CardTargetingType, Description, CardPrice, Card}
import cz.noveit.games.cardgame.engine.card.CardHolder

import spray.json._
import DefaultJsonProtocol._

/**
 * Created by arnostkuchar on 26.09.14.
 */

object CardLoader {
  def apply(file: String): List[Card] = {
    try {
      val br = new BufferedReader(new FileReader(file));

      val sb = new StringBuilder();
      var line = br.readLine();

      while (line != null) {
        sb.append(line);
        sb.append(System.lineSeparator());
        line = br.readLine();
      }

      br.close()
      val json = JsonParser(sb.toString()).asJsObject()
      // println(json.getFields("cards").head.prettyPrint)

      // println(JsValueToCardHolder(json.getFields("cards").head))
      JsValueToCardHolder(json.getFields("cards").head)
    } catch {
      case t =>
        println("ERROR: " + t.toString)
        List()
    }
  }
}

object JsValueToCardHolder {
  def apply(value: JsValue): List[Card] = {
    value match {
      case cards: JsArray =>
        cards.elements.map(cardValue => {
          val card = cardValue.asJsObject
          Card (
            card.getFields("_id").head.asInstanceOf[JsString].value,
            card.getFields("name").head.asInstanceOf[JsString].value,
            card.getFields("rarity").head.asInstanceOf[JsNumber].value.toInt,
            card.getFields("attack").head.asInstanceOf[JsNumber].value.toInt,
            card.getFields("defense").head.asInstanceOf[JsNumber].value.toInt,
            card.getFields("version").head.asInstanceOf[JsString].value,
            card.getFields("imageId").head.asInstanceOf[JsNumber].value.toInt,
            card.getFields("price").head.asInstanceOf[JsArray].elements.map(element => {
              CardPrice(
                element.asJsObject.getFields("element").head.asInstanceOf[JsNumber].value.toInt,
                element.asJsObject.getFields("price").head.asInstanceOf[JsNumber].value.toInt
              )
            }).toList,
            card.getFields("tags").head.convertTo[JsArray].elements.map(element => {
              element.asInstanceOf[JsString].value
            }).toList,
            card.getFields("description").head.convertTo[JsArray].elements.map(element => {
              Description(
                element.asJsObject.getFields("locale").head.asInstanceOf[JsString].value,
                element.asJsObject.getFields("description").head.asInstanceOf[JsString].value
              )
            }).toList,
            card.getFields("targetingType").headOption.map(value => CardTargetingType.withName(value.asInstanceOf[JsString].value)).getOrElse(CardTargetingType.None)
          )
        }).toList

      case o =>
        List()
    }
  }
}
