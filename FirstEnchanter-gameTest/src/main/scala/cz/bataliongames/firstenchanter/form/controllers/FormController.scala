package cz.bataliongames.firstenchanter.form.controllers

import java.util.Properties

import akka.actor.ActorRef

/**
 * Created by arnostkuchar on 15.09.14.
 */
trait FormController {
  def postInitialize(actor: ActorRef, properties: Properties)
  def close
  def hide
}
