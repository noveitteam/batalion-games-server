package cz.bataliongames.firstenchanter.form.controllers

import java.net.URL
import java.util.{Properties, ResourceBundle}
import javafx.application.Platform
import javafx.collections.{FXCollections, ObservableList}
import javafx.event.EventHandler
import javafx.scene.input.MouseEvent
import javafx.scene.layout.AnchorPane
import javafx.scene.{control => jfxsc}
import javafx.scene.{layout => jfxsl}
import javafx.stage.Stage
import javafx.{event => jfxe}
import javafx.{fxml => jfxf}
import cz.bataliongames.firstenchanter.form.actors.{SelectionCreateInteraction, CloseFormInteraction}

import scalafx.Includes._
import akka.actor.{Actor, ActorRef, ActorSystem}
import akka.remote.transport.ThrottlerTransportAdapter.Direction.Receive
import com.typesafe.config.ConfigFactory
import cz.bataliongames.firstenchanter.form.model.PlayerControllType
import cz.bataliongames.firstenchanter.form.services._
import scala.collection.JavaConverters._
import scalafx.application.Platform

import scalafx.Includes._
import scalafx.application.{JFXApp}
import scala.concurrent.{ExecutionContext, Await, Future}
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.global
import ExecutionContext.Implicits.global


import akka.util.Timeout
import scala.concurrent.duration._
import scala.concurrent.Await
import akka.pattern.ask

/**
 * Created by Wlsek on 3.9.2014.
 */
class PlayerSelectionController extends jfxf.Initializable with FormController {

  private var handler: ActorRef = _

  private var xOffset: Double = 0;
  private var yOffset: Double = 0;

  private var mode = 'oneVsOne

  @jfxf.FXML
   var root: jfxsl.Pane = _

  @jfxf.FXML
  var roomNameLabel: jfxsc.Label = _

  @jfxf.FXML
   var playerSelect1v1a: jfxsc.ComboBox[String] = _

  @jfxf.FXML
   var playerSelect1v1b: jfxsc.ComboBox[String] = _

  @jfxf.FXML
   var playerSelect2v2a: jfxsc.ComboBox[String] = _

  @jfxf.FXML
   var playerSelect2v2b: jfxsc.ComboBox[String] = _

  @jfxf.FXML
   var playerSelect2v2c: jfxsc.ComboBox[String] = _

  @jfxf.FXML
   var playerSelect2v2d: jfxsc.ComboBox[String] = _

  @jfxf.FXML
   var teamModeAccordition: jfxsc.Accordion = _

  @jfxf.FXML
   var startButton: jfxsc.Button = _

  @jfxf.FXML
  private def exitHandler(event: jfxe.Event): Unit = {
    saveConfig
    handler ! CloseFormInteraction
  }

  /*
    @jfxf.FXML
    private def startHandler(event: jfxe.Event): Unit = {
    }
  */
  @jfxf.FXML
  private def titleMousePressedHandler(event: jfxe.Event): Unit = {
    //    print(event)
    xOffset = event.asInstanceOf[MouseEvent].getSceneX();
    yOffset = event.asInstanceOf[MouseEvent].getSceneY();
  }

  @jfxf.FXML
  private def titleMouseDraggedHandler(event: jfxe.Event): Unit = {
    //    print(event)

    root.getScene.getWindow.setX(event.asInstanceOf[MouseEvent].getScreenX() - xOffset);
    root.getScene.getWindow.setY(event.asInstanceOf[MouseEvent].getScreenY() - yOffset);
  }

  def initialize(url: URL, rb: ResourceBundle): Unit = {
    val options: ObservableList[String] = FXCollections.observableList(PlayerControllType.values.map(v => v.toString).toList.asJava)

    playerSelect1v1a.setItems(options)
    playerSelect1v1b.setItems(options)
    playerSelect2v2a.setItems(options)
    playerSelect2v2b.setItems(options)
    playerSelect2v2c.setItems(options)
    playerSelect2v2d.setItems(options)

    teamModeAccordition.setExpandedPane(teamModeAccordition.getPanes.head)

    startButton.setOnMouseClicked(new EventHandler[MouseEvent]() {
      def handle(event: MouseEvent): Unit = {
        saveConfig
        mode match {
          case 'oneVsOne =>
            handler ! SelectionCreateInteraction(
              roomNameLabel.getText,
              Some(PlayerControllType.withName(playerSelect1v1a.getValue)),
              Some(PlayerControllType.withName(playerSelect1v1b.getValue)),
              None,
              None
            )
          case 'twoVsTwo =>
            handler ! SelectionCreateInteraction(
              roomNameLabel.getText,
              Some(PlayerControllType.withName(playerSelect2v2a.getValue)),
              Some(PlayerControllType.withName(playerSelect2v2b.getValue)),
              Some(PlayerControllType.withName(playerSelect2v2c.getValue)),
              Some(PlayerControllType.withName(playerSelect2v2d.getValue))
            )
        }
      }
    })
  }

  def saveConfig = {
    val props = new Properties()
    props.setProperty("playerSelect1v1a", playerSelect1v1a.getValue)
    props.setProperty("playerSelect1v1b", playerSelect1v1b.getValue)
    props.setProperty("playerSelect2v2a", playerSelect2v2a.getValue)
    props.setProperty("playerSelect2v2b", playerSelect2v2b.getValue)
    props.setProperty("playerSelect2v2c", playerSelect2v2c.getValue)
    props.setProperty("playerSelect2v2d", playerSelect2v2d.getValue)
    props.setProperty("positionX", root.getScene.getWindow.getX.toString)
    props.setProperty("positionY", root.getScene.getWindow.getY.toString)
    handler ! SaveConfigForForm("", props)
  }

  def postInitialize(handler: ActorRef, properties: Properties) = {
    this.handler = handler
    playerSelect1v1a.setValue(properties.getProperty("playerSelect1v1a"))
    playerSelect1v1b.setValue(properties.getProperty("playerSelect1v1b"))
    playerSelect2v2a.setValue(properties.getProperty("playerSelect2v2a"))
    playerSelect2v2b.setValue(properties.getProperty("playerSelect2v2b"))
    playerSelect2v2c.setValue(properties.getProperty("playerSelect2v2c"))
    playerSelect2v2d.setValue(properties.getProperty("playerSelect2v2d"))
    root.getScene.getWindow.setX(properties.getProperty("positionX").toDouble)
    root.getScene.getWindow.setY(properties.getProperty("positionY").toDouble)

    teamModeAccordition.expandedPaneProperty.onChange({
      if(teamModeAccordition.getExpandedPane != null){
        (teamModeAccordition.getExpandedPane.getContent.asInstanceOf[AnchorPane].getChildren.size() - 1) match {
          case 2 => mode = 'oneVsOne
          case 4 => mode = 'twoVsTwo
        }
        startButton.setDisable(false)
      } else {
        startButton.setDisable(true)
      }
    })
  }

  def close = {
    javafx.application.Platform.runLater(new Runnable {
      override def run(): Unit =  {
        root.getScene().getWindow.asInstanceOf[Stage].close()
      }
    })
  }

  def hide = {
    javafx.application.Platform.runLater(new Runnable {
      override def run(): Unit =  {
        root.getScene().getWindow.asInstanceOf[Stage].hide()
      }
    })
  }
}
