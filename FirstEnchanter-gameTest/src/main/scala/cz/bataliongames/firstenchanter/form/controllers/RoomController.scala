package cz.bataliongames.firstenchanter.form.controllers

import java.net.URL
import java.util.{Properties, ResourceBundle}
import javafx.application.Platform
import javafx.collections.{FXCollections, ObservableList}
import javafx.event.EventHandler
import javafx.scene.input.MouseEvent
import javafx.scene.{control => jfxsc, layout => jfxsl}
import javafx.stage.Stage
import javafx.{event => jfxe, fxml => jfxf}


import akka.actor.{Kill, ActorRef}
import cz.bataliongames.firstenchanter.form.actors.{ChangeTeamInteraction, StartGameInteraction, CloseFormInteraction}
import cz.bataliongames.firstenchanter.form.model.PlayerControllType
import cz.bataliongames.firstenchanter.form.services._

import scala.collection.JavaConverters._
import scalafx.Includes._

/**
 * Created by Wlsek on 3.9.2014.
 */

class RoomController extends jfxf.Initializable with FormController {

  private var handler: ActorRef = _
  private var properties: Properties = _

  private var xOffset: Double = 0;
  private var yOffset: Double = 0;

  private var presentedPlayers: List[Pair[Symbol, String]] = List()
  private var mode: Symbol = 'oneVsOne

  @jfxf.FXML
  var root: jfxsl.Pane = _

  @jfxf.FXML
  var loaderPane: jfxsl.Pane = _


  @jfxf.FXML
  var twoVsTwoA: jfxsc.Label = _

  @jfxf.FXML
  var twoVsTwoB: jfxsc.Label = _

  @jfxf.FXML
  var twoVsTwoC: jfxsc.Label = _

  @jfxf.FXML
  var twoVsTwoD: jfxsc.Label = _


  @jfxf.FXML
  var oneVsOneA: jfxsc.Label = _

  @jfxf.FXML
  var oneVsOneB: jfxsc.Label = _


  @jfxf.FXML
  var startButton: jfxsc.Button = _

  @jfxf.FXML
  var changeTeamButton: jfxsc.Button = _

  @jfxf.FXML
  private def exitHandler(event: jfxe.Event): Unit = {
    handler ! CloseFormInteraction
  }


  @jfxf.FXML
  private def starButtonClicked(event: jfxe.Event): Unit = {
    handler ! StartGameInteraction
  }

  @jfxf.FXML
  private def changeTeamClicked(event: jfxe.Event): Unit = {
    handler ! ChangeTeamInteraction("")
  }

  @jfxf.FXML
  private def titleMousePressedHandler(event: jfxe.Event): Unit = {
    //    print(event)
    xOffset = event.asInstanceOf[MouseEvent].getSceneX();
    yOffset = event.asInstanceOf[MouseEvent].getSceneY();
  }

  @jfxf.FXML
  private def titleMouseDraggedHandler(event: jfxe.Event): Unit = {
    //    print(event)
    root.getScene.getWindow.setX(event.asInstanceOf[MouseEvent].getScreenX() - xOffset);
    root.getScene.getWindow.setY(event.asInstanceOf[MouseEvent].getScreenY() - yOffset);
  }

  def initialize(url: URL, rb: ResourceBundle): Unit = {

  }

  override def postInitialize(h: ActorRef, p: Properties) = {
    handler = h
    properties = p

  }

  def presentPositions(
                        position1: Option[PlayerControllType.Value],
                        position2: Option[PlayerControllType.Value],
                        position3: Option[PlayerControllType.Value],
                        position4: Option[PlayerControllType.Value]
                        ): Unit = {
    if (position1.isDefined && position2.isDefined && position3.isEmpty && position4.isEmpty) {
      twoVsTwoA.setVisible(false)
      twoVsTwoB.setVisible(false)
      twoVsTwoC.setVisible(false)
      twoVsTwoD.setVisible(false)
      loaderPane.setVisible(false)
      mode = 'oneVsOne
    } else if (position1.isDefined && position2.isDefined && position3.isDefined && position4.isDefined) {
      oneVsOneA.setVisible(false)
      oneVsOneB.setVisible(false)
      loaderPane.setVisible(false)
      mode = 'twoVsTwo
    }
  }

  def presentPlayerOnPosition(position: Symbol, player: String): Unit = {
    position match {
      case 'position1 =>
        mode match {
          case 'twoVsTwo => twoVsTwoA.setText(player)
          case 'oneVsOne => oneVsOneA.setText(player)
        }
      case 'position2 =>
        mode match {
          case 'twoVsTwo => twoVsTwoB.setText(player)
          case 'oneVsOne => oneVsOneB.setText(player)
        }
      case 'position3 =>
        mode match {
          case 'twoVsTwo => twoVsTwoC.setText(player)
        }
      case 'position4 =>
        mode match {
          case 'twoVsTwo => twoVsTwoD.setText(player)
        }
    }

    presentedPlayers = position -> player :: presentedPlayers
  }

  def removePlayerFromPosition(player: String): Unit = {
    presentedPlayers.find(f => f._2 == player).map(found => {
      found._1 match {
        case 'position1 =>
          mode match {
            case 'twoVsTwo => twoVsTwoA.setText("...")
            case 'oneVsOne => oneVsOneA.setText("...")
          }
        case 'position2 =>
          mode match {
            case 'twoVsTwo => twoVsTwoB.setText("...")
            case 'oneVsOne => oneVsOneB.setText("...")
          }
        case 'position3 =>
          mode match {
            case 'twoVsTwo => twoVsTwoC.setText("...")
          }
        case 'position4 =>
          mode match {
            case 'twoVsTwo => twoVsTwoD.setText("...")
          }
      }
      presentedPlayers = presentedPlayers.filterNot(p => p._2 == player)
    })
  }

  def close = {
    Platform.runLater(new Runnable {
      override def run(): Unit = {
        root.getScene().getWindow.asInstanceOf[Stage].close()
      }
    })
  }

  def hide = {
    Platform.runLater(new Runnable {
      override def run(): Unit = {
        root.getScene().getWindow.asInstanceOf[Stage].hide()
      }
    })
  }
}
