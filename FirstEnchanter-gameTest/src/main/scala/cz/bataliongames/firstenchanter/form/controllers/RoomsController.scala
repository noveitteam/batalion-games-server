package cz.bataliongames.firstenchanter.form.controllers



import java.net.URL
import java.util.{Properties, ResourceBundle}
import javafx.application.Platform
import javafx.event.{EventHandler, Event}
import javafx.fxml.{FXML, Initializable}
import javafx.scene.control.{Label, Button, TextField}
import javafx.scene.input.MouseEvent
import javafx.scene.layout.{Pane, VBox}
import javafx.scene.paint.Color
import javafx.scene.text.Font
import javafx.stage.Stage


import akka.actor.{Kill, ActorRef}
import cz.bataliongames.firstenchanter.form.actors.{CloseFormInteraction, CreateRoomInteraction, JoinRoomInteraction}
import cz.noveit.proto.firstenchanter.FirstEnchanterLightWeightServerLobby.CreateRoom
import scalafx.Includes._

/**
 * Created by Wlsek on 3.9.2014.
 */
class RoomsController extends Initializable with FormController {

  private var handler: ActorRef = _
  private var properties: Properties = _

  private var xOffset: Double = 0;
  private var yOffset: Double = 0;

  private var selectedRoom = ""
  private var selectedPane: Pane = _

  @FXML
   var root: Pane = _

  @FXML
   var playerNicknameTextField: TextField = _

  @FXML
   var createRoomButton: Button = _

  @FXML
   var joinButton: Button = _

  @FXML
  var roomsListVBox: VBox = _


  @FXML
  private def exitHandler(event: Event): Unit = {
    handler ! CloseFormInteraction
  }

  @FXML
  private def joinRoomButtonClicked(event: Event): Unit = {
      println("Join room: " + selectedRoom)
     handler ! JoinRoomInteraction(selectedRoom, playerNicknameTextField.getText())
  }

  @FXML
  private def createRoomButtonClicked(event: Event): Unit = {
    println("Create room: " + playerNicknameTextField.getText())
    handler ! CreateRoomInteraction(playerNicknameTextField.getText(), playerNicknameTextField.getText())
  }

  @FXML
  private def titleMousePressedHandler(event: Event): Unit = {
    //    print(event)
    xOffset = event.asInstanceOf[MouseEvent].getSceneX();
    yOffset = event.asInstanceOf[MouseEvent].getSceneY();
  }

  @FXML
  private def titleMouseDraggedHandler(event: Event): Unit = {
    //    print(event)
    root.getScene.getWindow.setX(event.asInstanceOf[MouseEvent].getScreenX() - xOffset);
    root.getScene.getWindow.setY(event.asInstanceOf[MouseEvent].getScreenY() - yOffset);
  }

  def initialize(url: URL, rb: ResourceBundle): Unit = {

  }


  override def postInitialize(h: ActorRef, p: Properties) = {
    handler = h
    properties = p

    playerNicknameTextField.textProperty().onChange({
      println(playerNicknameTextField.getText())
      if(playerNicknameTextField.getText().trim != "") {
        createRoomButton.setDisable(false)
      } else {
        createRoomButton.setDisable(true)
      }

      if(roomsListVBox.getChildren.size() > 0 && playerNicknameTextField.getText().trim != "") {
        joinButton.setDisable(false)
      } else {
        joinButton.setDisable(true)
      }
    })

    roomsListVBox.getChildren.onChange({
      if(roomsListVBox.getChildren.size() > 0) {
        selectedPane = roomsListVBox.getChildren.head.asInstanceOf[Pane]
        selectedPane.setStyle("-fx-background-color: E8AF72;")
        val label = selectedPane.getChildren.head.asInstanceOf[Label]
        label.setTextFill(Color.WHITE)
        selectedRoom = label.getText

      }

      if(roomsListVBox.getChildren.size() > 0 && playerNicknameTextField.getText().trim != "") {
        joinButton.setDisable(false)
      } else {
        joinButton.setDisable(true)
      }
    })

  //  renderListOfRooms(List("Room1", "Room2", "Room3"))

  }

  def renderListOfRooms(rooms: List[String]) = {
        roomsListVBox.getChildren.clear()
        rooms.map(r => {
          val pane = new Pane()
          val label = new Label()
          label.setFont(new Font("Comic Sans MS", 18))
          label.setText(r)
          label.setTextFill(Color.web("D67F22"))
          pane.getChildren.add(label)

          pane.setOnMouseClicked(new EventHandler[MouseEvent] {
            override def handle(p1: MouseEvent): Unit = {
              if(selectedPane != null) {
                selectedPane.setStyle("-fx-background-color: white;")
                selectedPane.getChildren.head.asInstanceOf[Label].setTextFill(Color.web("D67F22"))
              }

              selectedPane = pane
              selectedPane.setStyle("-fx-background-color: E8AF72;")
              val label = selectedPane.getChildren.head.asInstanceOf[Label]
              label.setTextFill(Color.WHITE)
              selectedRoom = label.getText
            }
          })

          roomsListVBox.getChildren.add(pane)
        })

  }

  def close = {
    Platform.runLater(new Runnable {
      override def run(): Unit =  {
        root.getScene().getWindow.asInstanceOf[Stage].close()
      }
    })
  }

  def hide = {
    Platform.runLater(new Runnable {
      override def run(): Unit =  {
        root.getScene().getWindow.asInstanceOf[Stage].hide()
      }
    })
  }
}
