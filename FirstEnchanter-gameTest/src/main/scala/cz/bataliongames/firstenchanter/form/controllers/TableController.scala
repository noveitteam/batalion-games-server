package cz.bataliongames.firstenchanter.form.controllers

import java.awt.Point
import java.io.File
import java.net.URL
import java.util
import java.util.{Properties, ResourceBundle}
import javafx.animation._
import javafx.application.Platform
import javafx.beans.value.{ChangeListener, ObservableValue}
import javafx.event.{ActionEvent, EventHandler}
import javafx.geometry.{Pos, Dimension2D}
import javafx.scene.control.{ProgressBar, Button, Label, ScrollPane}
import javafx.scene.image.{Image, ImageView}
import javafx.scene.input.{DragEvent, ClipboardContent, TransferMode, MouseEvent}
import javafx.scene.layout.{HBox, VBox, AnchorPane}
import javafx.scene.paint.{Color, Paint}
import javafx.scene.shape._
import javafx.scene.text.Text
import javafx.scene.{control => jfxsc, layout => jfxsl, ImageCursor, Cursor, Node}
import javafx.stage.Stage
import javafx.util.Duration
import javafx.{event => jfxe, fxml => jfxf, animation}
import akka.actor.ActorRef
import cz.bataliongames.firstenchanter.form.actors._
import cz.bataliongames.firstenchanter.form.controllers.table.chat.{ChatMessage, ChatController}
import cz.bataliongames.firstenchanter.form.controllers.table.duplicator.{ChildrenDuplicator, LabelDuplicator}
import cz.bataliongames.firstenchanter.form.controllers.table._
import cz.bataliongames.firstenchanter.form.decorators.StrokeTextDecorator
import cz.noveit.games.cardgame.adapters.CardTargetingType
import cz.noveit.games.cardgame.engine.card._
import cz.noveit.games.cardgame.engine.game.{session => NITSession}
import cz.noveit.games.cardgame.engine.handlers.helpers.serialization.{CardPositionToProto, CardTargetToProto}
import cz.noveit.games.cardgame.engine.player.{NeutralDistribution, CharmOfElements}
import cz.noveit.proto.firstenchanter.FirstEnchanterMatch.{PlayerDrawCards, ElementsGenerated, PlayerRollResult}
import org.joda.time.{DateTimeZone, DateTime}

import scala.collection.JavaConverters._
import scala.collection.mutable
import scala.util.Random
import scalafx.Includes._
import scalafx.geometry.Point2D
import scalafx.scene.effect.GaussianBlur
import scalafx.scene.layout.{BorderPane, Pane}
import scalafx.scene.shape.{StrokeType}
import scalafx.scene.text.{FontWeight, Font}

import cz.noveit.games.cardgame.{engine => CEngine}

/**
 * Created by Wlsek on 4.9.2014.
 */


trait LinePanePairTarget

case class GlobalPointIsTarget(point: Point2D) extends LinePanePairTarget

case class PaneIsTarget(pane: Pane) extends LinePanePairTarget

case class LinePanePair(line: Line, pane: Pane, target: LinePanePairTarget)

case class PaneWithCardHolder(card: CardHolder, pane: Pane)

case class AttackerDefenderPair(attacker: Option[PaneWithCardHolder], defender: Option[PaneWithCardHolder])

case class CardOnTableWithIndex(c: CardHolder, i: Int)

object CardScrollPanePosition extends Enumeration {
  val LEFT, CENTER, RIGHT = Value
}

object SelectingMode extends Enumeration {
  val AttackingCard, DefendingCard = Value
}

object TargetingMode extends Enumeration {
  val None, EnemyCard, EnemyTeam, EnemyAll, OwnCard, OwnTeam, OwnAll, All = Value
}

object TableTarget extends Enumeration {
  val MyHealth, EnemyHealth, MyCard, EnemyCard = Value
}

object PlayersMode extends Enumeration {
  val OneVsOne, TwoVsTwo = Value
}

object TableState extends Enumeration {
  val StartRolling,
  Using,
  WaitingForEnemyUse,
  CounterUsing,
  WaitingForEnemyCounterUse,
  UseAndCounterUseEvaluating,
  Attacking,
  WaitingForEnemyAttack,
  Defending,
  WaitingForEnemyDefend,
  AttackAndDefendEvaluating,
  FortuneRolling = Value
}

case class PositionWithAngle(point: Point2D, angle: Double, aligment: Symbol)

object TableInteractionSource extends Enumeration {
  val Ally, Enemy = Value
}

case class TargetInteraction(pane: jfxsl.Pane, targetPane: Node, line: Shape, source: TableInteractionSource.Value, target: TableTarget.Value, interaction: NITSession.GameMessages)

class TableController extends jfxf.Initializable with StrokeTextDecorator with FormController {
  private var handler: ActorRef = _

  private var cardsOnOwnTable: List[CardHolder] = List()
  private var allyCardsOnOwnTable: List[CardHolder] = List()
  private var cardsOnOwnGraveyard: List[CardHolder] = List()
  private var cardsOnEnemyTable: List[CardHolder] = List()
  private var cardsOnEnemyGraveyard: List[CardHolder] = List()

  private var cardsInHand: List[CardHolder] = List()

  private var possibleCardToUse: List[NITSession.AttackerUsesCard] = List()
  private var possibleCardToCounterUse: List[NITSession.DefenderUsesCard] = List()
  private var possibleCardToAttack: List[NITSession.AttackerAttacks] = List()
  private var possibleCardToDefense: List[NITSession.DefenderDefends] = List()

  private var paneCardPairs: List[Pair[jfxsl.Pane, CardHolder]] = List()
  private var ownTablePaneCardPairs: List[Pair[jfxsl.Pane, CardHolder]] = List()
  private var enemyTablePaneCardPairs: List[Pair[jfxsl.Pane, CardHolder]] = List()

  private var allyTargetInteractions: List[TargetInteraction] = List()

  private var subRoundController: SubRoundBuilder = _

  private var cardsOnTableController: CardsOnTableController = _

  private var cardsOnGraveyardController: CardsOnGraveyardController = _

  @jfxf.FXML
  var allyTargetInteractionsLayer: jfxsl.AnchorPane = _

  private var enemyTargetInteractions: List[TargetInteraction] = List()

  @jfxf.FXML
  var enemyTargetInteractionsLayer: jfxsl.AnchorPane = _

  private var sentRequest: List[TableInterAction] = List()

  private var startingMyHp = 0
  private var startingEnemyHp = 0

  private var players: List[PlayerSeesPlayer] = List()
  private var attackersDefenders: List[AttackerDefenderPair] = List()

  private var connectingLinesOnRoot: List[LinePanePair] = List()

  private var xOffset: Double = 0
  private var yOffset: Double = 0

  private var cardDraggingXOffset: Double = 0
  private var cardDraggingYOffset: Double = 0
  private var cardDraggingRotateOffset: Double = 0

  private var doingHandAnimation = false
  private var movedBy: List[Pair[Node, PositionWithAngle]] = List()
  private var myHandIsOpened = false
  private var focusedCard: Option[Pair[Node, Point2D]] = None

  private var playerMode: PlayersMode.Value = PlayersMode.OneVsOne
  private var tableState: TableState.Value = TableState.FortuneRolling

  private var targeting: List[TargetingMode.Value] = List()

  private var neutralDistributionDialog: Option[NeutralDistributionDialog] = None

  private val cardsOnTableAttackHighlightedOff = "-fx-background-color: white; -fx-border-color: black; -fx-border-width: 2px; -fx-border-radius: 5px;"
  private val cardsOnTableAttackHighlightedOn = "-fx-background-color: #D67F22; -fx-border-color: black; -fx-border-width: 2px; -fx-border-radius: 5px;"

  private val cardShadowOnTableTemplate = new Pane {
    prefHeight = 114
    prefWidth = 84
    translateY = 9
    style = "-fx-background-color: FFC485; -fx-border-color:  D67F22; -fx-border-width: 2px; -fx-border-radius: 5px;"
  }

  private val cardInHandTemplate = new Pane {
    prefHeight = 200
    prefWidth = 140
    layoutX = 53
    layoutY = 65
    style = "-fx-background-color: white; -fx-border-color: black; -fx-border-radius: 5; -fx-border-width: 2"
  }

  private val enemyColor = "D64960"
  private val allyColor = "148C38"

  val aEnemy = new Image("/assets/attack_enemy_cursor.png");
  private val attackCursor = new ImageCursor(aEnemy, aEnemy.getWidth / 2, aEnemy.getHeight / 2)

  val dAlly = new Image("/assets/defend_ally_cursor.png");
  private val defendCursor = new ImageCursor(dAlly, dAlly.getWidth / 2, dAlly.getHeight / 2)

  val uEnemy = new Image("/assets/use_enemy_cursor.png");
  private val useOnEnemyCursor = new ImageCursor(uEnemy, uEnemy.getWidth / 2, uEnemy.getHeight / 2)

  val uAlly = new Image("/assets/use_ally_cursor.png");
  private val useOnAllyCursor = new ImageCursor(uAlly, uAlly.getWidth / 2, uAlly.getHeight / 2)

  val allyLineEnd = new Image("/assets/allyUse.gif");
  val enemyLineEnd = new Image("/assets/enemyUse.gif");

  val allyLineTemplate = new Line()
  allyLineTemplate.setFill(Color.web("#148C38"))
  allyLineTemplate.setStroke(Color.web("#148C38"))
  allyLineTemplate.getStrokeDashArray().addAll(10d, 5d);

  val enemyLineTemplate = new Line()
  enemyLineTemplate.setFill(Color.web("#D64960"))
  enemyLineTemplate.setStroke(Color.web("#D64960"))
  enemyLineTemplate.getStrokeDashArray().addAll(10d, 5d);

  //Chat controlls

  //chatInputBoxTextField
  //chatSendButtonLabel
  //chatMessagesVBox
  //chatSwitchToTeamChatLabel
  //switchToAllChatLabel
  //chatMessageTemplatePane

  var chatController: ChatController = _

  var endGameController: Option[GameEndController] = None

  @jfxf.FXML
  var matchEndsResultScreen: jfxsl.Pane = _

  @jfxf.FXML
  var matchEndsState: jfxsc.Label = _

  @jfxf.FXML
  var matchEndsLoader: ImageView = _

  @jfxf.FXML
  var matchEndsEssencesGainValue: jfxsc.Label = _

  @jfxf.FXML
  var matchEndsMmrGainValue: jfxsc.Label = _

  @jfxf.FXML
  var matchEndsOkButton: jfxsc.Label = _

  @jfxf.FXML
  var chatInputBoxTextField: jfxsc.TextField = _

  @jfxf.FXML
  var chatSendButtonLabel: jfxsc.Label = _

  @jfxf.FXML
  var chatMessagesScrollPale: ScrollPane = _

  @jfxf.FXML
  var chatMessagesVBox: jfxsl.VBox = _

  @jfxf.FXML
  var chatSwitchToTeamChatLabel: jfxsc.Label = _

  @jfxf.FXML
  var switchToAllChatLabel: jfxsc.Label = _

  @jfxf.FXML
  var chatAllyMessageTemplatePane: jfxsl.Pane = _

  @jfxf.FXML
  var chatEnemyMessageTemplatePane: jfxsl.Pane = _

  @jfxf.FXML
  var graveyardOwnCardsScrollPane: ScrollPane = _

  @jfxf.FXML
  var graveyardEnemyCardsScrollPane: ScrollPane = _

  @jfxf.FXML
  var graveyardOwnCardsHBoxPane: jfxsl.HBox = _

  @jfxf.FXML
  var graveyardEnemyCardsHBoxPane: jfxsl.HBox = _

  @jfxf.FXML
  var graveyardOwnCardsTableLeftArrowImageView: ImageView = _

  @jfxf.FXML
  var graveyardOwnCardsTableRightArrowImageView: ImageView = _

  @jfxf.FXML
  var graveyardEnemyCardsTableRightArrowImageView: ImageView = _

  @jfxf.FXML
  var graveyardEnemyCardsTableLeftArrowImageView: ImageView = _

  @jfxf.FXML
  var subRoundPane: jfxsl.Pane = _

  @jfxf.FXML
  var cardOnTableTemplate: jfxsl.Pane = _

  @jfxf.FXML
  var allySelectedCardOnTableTemplate: jfxsl.Pane = _

  @jfxf.FXML
  var enemySelectedCardOnTableTemplate: jfxsl.Pane = _

  @jfxf.FXML
  var normalCardTemplate: jfxsl.Pane = _

  @jfxf.FXML
  var detailsCardTemplate: jfxsl.Pane = _

  @jfxf.FXML
  var cardDetailsPane: jfxsl.Pane = _

  @jfxf.FXML
  var loaderPane: jfxsl.Pane = _

  @jfxf.FXML
  var enemyHpDropPane: jfxsl.Pane = _

  @jfxf.FXML
  var ownHpDropPane: jfxsl.Pane = _

  @jfxf.FXML
  var enemyCardTableHelpPane: jfxsl.Pane = _

  @jfxf.FXML
  var ownCardTableHelpPane: jfxsl.Pane = _

  @jfxf.FXML
  var enemyHpTextValue: Text = _

  @jfxf.FXML
  var myHpTextValue: Text = _

  @jfxf.FXML
  var myHpProgressBar: ProgressBar = _

  @jfxf.FXML
  var enemyHpProgressBar: ProgressBar = _

  /* ======================================= Players      ======================================= */

  @jfxf.FXML
  var enemy2DeckImageView: ImageView = _

  @jfxf.FXML
  var enemy2GraveyardImageView: ImageView = _

  @jfxf.FXML
  var enemy2HandImageView: ImageView = _

  @jfxf.FXML
  var enemy2FireImageView: ImageView = _

  @jfxf.FXML
  var enemy2WaterImageView: ImageView = _

  @jfxf.FXML
  var enemy2EarthImageView: ImageView = _

  @jfxf.FXML
  var enemy2AirImageView: ImageView = _

  @jfxf.FXML
  var enemy2NameLabel: Text = _

  @jfxf.FXML
  var enemy2FireLabel: Text = _

  @jfxf.FXML
  var enemy2WaterLabel: Text = _

  @jfxf.FXML
  var enemy2EarthLabel: Text = _

  @jfxf.FXML
  var enemy2AirLabel: Text = _

  @jfxf.FXML
  var enemy2GraveyardSizeLabel: Text = _

  @jfxf.FXML
  var enemy2HandSizeLabel: Text = _

  @jfxf.FXML
  var enemy2DeckSizeLabel: Text = _

  @jfxf.FXML
  var enemy1NameLabel: Text = _

  @jfxf.FXML
  var enemy1FireLabel: Text = _

  @jfxf.FXML
  var enemy1WaterLabel: Text = _

  @jfxf.FXML
  var enemy1EarthLabel: Text = _

  @jfxf.FXML
  var enemy1AirLabel: Text = _

  @jfxf.FXML
  var enemy1GraveyardSizeLabel: Text = _

  @jfxf.FXML
  var enemy1HandSizeLabel: Text = _

  @jfxf.FXML
  var enemy1DeckSizeLabel: Text = _

  @jfxf.FXML
  var allyDeckImageView: ImageView = _

  @jfxf.FXML
  var allyGraveyardImageView: ImageView = _

  @jfxf.FXML
  var allyHandImageView: ImageView = _

  @jfxf.FXML
  var allyFireImageView: ImageView = _

  @jfxf.FXML
  var allyWaterImageView: ImageView = _

  @jfxf.FXML
  var allyEarthImageView: ImageView = _

  @jfxf.FXML
  var allyAirImageView: ImageView = _

  @jfxf.FXML
  var allyNameLabel: Text = _

  @jfxf.FXML
  var allyFireLabel: Text = _

  @jfxf.FXML
  var allyWaterLabel: Text = _

  @jfxf.FXML
  var allyEarthLabel: Text = _

  @jfxf.FXML
  var allyAirLabel: Text = _

  @jfxf.FXML
  var allyGraveyardSizeLabel: Text = _

  @jfxf.FXML
  var allyHandSizeLabel: Text = _

  @jfxf.FXML
  var allyDeckSizeLabel: Text = _

  @jfxf.FXML
  var myNameLabel: Text = _

  @jfxf.FXML
  var myFireLabel: Text = _

  @jfxf.FXML
  var myWaterLabel: Text = _

  @jfxf.FXML
  var myEarthLabel: Text = _

  @jfxf.FXML
  var myAirLabel: Text = _

  @jfxf.FXML
  var myGraveyardSizeLabel: Text = _

  @jfxf.FXML
  var myHandSizeLabel: Text = _

  @jfxf.FXML
  var myDeckSizeLabel: Text = _

  /* ======================================= Fortune roll ======================================= */

  val attackerImage = new Image("/assets/sword.png");
  val defenderImage = new Image("/assets/armor.png");

  private var lastRollPosition: NITSession.PlayerRollPosition = _

  @jfxf.FXML
  var fortuneRollPane: jfxsl.Pane = _


  @jfxf.FXML
  var fortuneRollLabel: Label = _

  @jfxf.FXML
  var fortuneRollResult: Label = _

  @jfxf.FXML
  var myRollResultLabel: Text = _

  @jfxf.FXML
  var enemyRollResultLabel: Text = _


  @jfxf.FXML
  var rollPositionImageView: ImageView = _

  @jfxf.FXML
  var enemyRollPositionImageView: ImageView = _


  @jfxf.FXML
  var fortuneWheel: jfxsl.Pane = _
  var fortuneWheelNumberTexts: List[Text] = List()

  //OneVsOne mode

  @jfxf.FXML
  var oneVsOneMyNameLabel: Label = _

  @jfxf.FXML
  var oneVsOneEnemyNameLabel: Label = _


  //TwoVsTwo mode

  @jfxf.FXML
  var twoVsTwoMyNameLabel: Label = _

  @jfxf.FXML
  var twoVsTwoAllyNameLabel: Label = _

  @jfxf.FXML
  var twoVsTwoEnemy1NameLabel: Label = _

  @jfxf.FXML
  var twoVsTwoEnemy2NameLabel: Label = _

  @jfxf.FXML
  var twoVsTwoMyRollLabel: Text = _

  @jfxf.FXML
  var twoVsTwoAllyRollLabel: Text = _

  @jfxf.FXML
  var twoVsTwoEnemy1RollLabel: Text = _

  @jfxf.FXML
  var twoVsTwoEnemy2RollLabel: Text = _

  @jfxf.FXML
  var myRollBackgroundShape: Circle = _

  @jfxf.FXML
  var allyRollBackgroundShape: Circle = _

  @jfxf.FXML
  var enemy1RollBackgroundShape: Circle = _

  @jfxf.FXML
  var enemy2RollBackgroundShape: Circle = _

  @jfxf.FXML
  var fortuneOkButton: Label = _

  @jfxf.FXML
  var waitingPaneRoll: jfxsl.Pane = _

  @jfxf.FXML
  var waitingPaneRollNumberLabel: Label = _

  @jfxf.FXML
  var waitingPaneRollPositionImageView: ImageView = _

  @jfxf.FXML
  def onFortuneRollOkClicked(event: jfxe.Event): Unit = {
    val t = new TranslateTransition(Duration.millis(500))
    t.setNode(fortuneRollPane)
    t.setByY(fortuneRollPane.getHeight * -1)
    t.play()
  }


  /* ======================================= */

  @jfxf.FXML
  var root: jfxsl.Pane = _

  @jfxf.FXML
  var tablePane: jfxsl.Pane = _

  @jfxf.FXML
  var menuPane: jfxsl.Pane = _


  @jfxf.FXML
  var graveyardPane: jfxsl.Pane = _

  @jfxf.FXML
  var communicationPane: jfxsl.Pane = _

  @jfxf.FXML
  var myHand: jfxsl.Pane = _

  @jfxf.FXML
  var myHandDismissPane: jfxsl.Pane = _

  @jfxf.FXML
  var ownCardScrollPane: ScrollPane = _

  @jfxf.FXML
  var ownCardHBoxPane: HBox = _

  @jfxf.FXML
  var ownTableLeftArrowImageView: ImageView = _

  @jfxf.FXML
  var ownTableRightArrowImageView: ImageView = _

  @jfxf.FXML
  def onOwnTableLeftArrowImageViewClicked(event: jfxe.Event): Unit = {
    ownTableScrollPanePosition match {
      case CardScrollPanePosition.RIGHT =>
        ownTableScrollPanePosition = CardScrollPanePosition.CENTER
      case CardScrollPanePosition.CENTER =>
        ownTableScrollPanePosition = CardScrollPanePosition.LEFT
      case t =>
    }

    refreshOwnTableScrollPane
  }


  @jfxf.FXML
  def onOwnTableRightArrowImageViewClicked(event: jfxe.Event): Unit = {
    ownTableScrollPanePosition match {
      case CardScrollPanePosition.LEFT =>
        ownTableScrollPanePosition = CardScrollPanePosition.CENTER
      case CardScrollPanePosition.CENTER =>
        ownTableScrollPanePosition = CardScrollPanePosition.RIGHT
      case t =>
    }

    refreshOwnTableScrollPane
  }

  private var ownTableScrollPanePosition = CardScrollPanePosition.CENTER

  @jfxf.FXML
  var enemyCardScrollPane: ScrollPane = _

  @jfxf.FXML
  var enemyCardHBoxPane: HBox = _

  @jfxf.FXML
  var enemyTableLeftArrowImageView: ImageView = _

  @jfxf.FXML
  def onEnemyTableLeftArrowImageViewClicked(event: jfxe.Event): Unit = {
    enemyTableScrollPanePosition match {
      case CardScrollPanePosition.RIGHT =>
        enemyTableScrollPanePosition = CardScrollPanePosition.CENTER
      case CardScrollPanePosition.CENTER =>
        enemyTableScrollPanePosition = CardScrollPanePosition.LEFT
      case t =>
    }

    refreshEnemyTableScrollPane
  }

  @jfxf.FXML
  def onEnemyTableRightArrowImageViewClicked(event: jfxe.Event): Unit = {
    enemyTableScrollPanePosition match {
      case CardScrollPanePosition.LEFT =>
        enemyTableScrollPanePosition = CardScrollPanePosition.CENTER
      case CardScrollPanePosition.CENTER =>
        enemyTableScrollPanePosition = CardScrollPanePosition.RIGHT
      case t =>
    }

    refreshEnemyTableScrollPane
  }


  @jfxf.FXML
  var enemyTableRightArrowImageView: ImageView = _

  private var enemyTableScrollPanePosition = CardScrollPanePosition.CENTER

  @jfxf.FXML
  def mousePressedHandler(event: jfxe.Event): Unit = {
    //    print(event)
    xOffset = event.asInstanceOf[MouseEvent].getSceneX();
    yOffset = event.asInstanceOf[MouseEvent].getSceneY();
  }

  @jfxf.FXML
  def mouseDraggedHandler(event: jfxe.Event): Unit = {
    print(event)

    root.getScene.getWindow.setX(event.asInstanceOf[MouseEvent].getScreenX() - xOffset);
    root.getScene.getWindow.setY(event.asInstanceOf[MouseEvent].getScreenY() - yOffset);
  }

  @jfxf.FXML
  def onGraveyardMouseClicked(event: jfxe.Event): Unit = {
    Platform.runLater(new Runnable {
      def run(): Unit = {
        val tt = new TranslateTransition(Duration.millis(200), graveyardPane);
        tt.setToX(850.0)
        tt.play()
      }
    })
  }

  @jfxf.FXML
  def onCommunicationMouseClicked(event: jfxe.Event): Unit = {
    Platform.runLater(new Runnable {
      def run(): Unit = {
        val tt = new TranslateTransition(Duration.millis(200), communicationPane);
        tt.setToX(-850.0)
        tt.play()
        chatController.onMoveToScene
      }
    })
  }


  @jfxf.FXML
  def onBackToTableClicked(event: jfxe.Event): Unit = {
    Platform.runLater(new Runnable {
      def run(): Unit = {
        val tt = new TranslateTransition(Duration.millis(200), graveyardPane);
        tt.setToX(-850.0)
        tt.play()
      }
    })
  }

  @jfxf.FXML
  def onBackToTableClickedFromCommunication(event: jfxe.Event): Unit = {
    Platform.runLater(new Runnable {
      def run(): Unit = {
        val tt = new TranslateTransition(Duration.millis(200), communicationPane);
        tt.setToX(850.0)
        tt.play()
        chatController.onMoveFromScene
      }
    })
  }


  @jfxf.FXML
  def onMenuTextClicked(event: jfxe.Event): Unit = {
    Platform.runLater(new Runnable {
      def run(): Unit = {
        menuPane.setVisible(true)
        menuPane.setMouseTransparent(false)

        val ft = new FadeTransition(Duration.millis(200), menuPane);
        ft.setFromValue(0.0);
        ft.setToValue(1.0);
        ft.play()

        val effect = new GaussianBlur()
        effect.radius = 5
        tablePane.setEffect(effect)
      }
    })
  }

  @jfxf.FXML
  def onMenuMouseEntered(event: jfxe.Event): Unit = {
    val text = event.getTarget.asInstanceOf[Text]
    onStrokedTextSelected(text)
  }

  @jfxf.FXML
  def onMenuMouseExited(event: jfxe.Event): Unit = {
    val text = event.getTarget.asInstanceOf[Text]
    onStrokedTextIdle(text)
  }

  @jfxf.FXML
  def onEndTurnClicked(event: jfxe.Event): Unit = {
  }

  @jfxf.FXML
  def onEndTurnMouseEntered(event: jfxe.Event): Unit = {
    val text = event.getTarget.asInstanceOf[Text]
    onStrokedTextSelected(text)
  }

  @jfxf.FXML
  def onEndTurnMouseExited(event: jfxe.Event): Unit = {
    val text = event.getTarget.asInstanceOf[Text]
    onStrokedTextIdle(text)
  }

  @jfxf.FXML
  def onMenuSurrenderAndExitClicked(event: jfxe.Event): Unit = {
    handler ! CloseFormInteraction
  }

  @jfxf.FXML
  def onMenuSurrenderAndExitMouseEntered(event: jfxe.Event): Unit = {
    val text = event.getTarget.asInstanceOf[Text]
    onStrokedTextSelected(text)
  }

  @jfxf.FXML
  def onMenuSurrenderAndExitMouseExited(event: jfxe.Event): Unit = {
    val text = event.getTarget.asInstanceOf[Text]
    onStrokedTextIdle(text)
  }

  @jfxf.FXML
  def onMenuSimulateDisconnectMouseEntered(event: jfxe.Event): Unit = {
    val text = event.getTarget.asInstanceOf[Text]
    onStrokedTextSelected(text)
  }

  @jfxf.FXML
  def onMenuSimulateDisconnectMouseExited(event: jfxe.Event): Unit = {
    val text = event.getTarget.asInstanceOf[Text]
    onStrokedTextIdle(text)
  }

  @jfxf.FXML
  def onMenuSimulateDisconnectMouseClicked(event: jfxe.Event): Unit = {
  }

  @jfxf.FXML
  def onMenuBackClicked(event: jfxe.Event): Unit = {
    tablePane.setEffect(null)
    menuPane.setVisible(false)
    menuPane.setMouseTransparent(true)
  }


  @jfxf.FXML
  def onMenuBackMouseEntered(event: jfxe.Event): Unit = {
    val text = event.getTarget.asInstanceOf[Text]
    onStrokedTextSelected(text)
  }

  @jfxf.FXML
  def OnshowMyHandMouseEnter(event: jfxe.Event): Unit = {
    showHands
  }

  @jfxf.FXML
  def OnshowMyHandMouseExited(event: jfxe.Event): Unit = {

  }

  @jfxf.FXML
  def onMenuBackMouseExited(event: jfxe.Event): Unit = {
    val text = event.getTarget.asInstanceOf[Text]
    onStrokedTextIdle(text)
  }

  @jfxf.FXML
  def onMyHandDismissMouseClicked(event: jfxe.Event): Unit = {
    println("clickeed, hand: " + myHandIsOpened + ", animation: " + doingHandAnimation)
    hideHands
  }

  // ===================================== Neutral distribution ==================================
  @jfxf.FXML
  var neutralDistributionPane: jfxsl.Pane = _

  @jfxf.FXML
  var neutralDistributionAlphaPane: jfxsl.Pane = _

  @jfxf.FXML
  var neutralDistributionCardPane: jfxsl.Pane = _

  @jfxf.FXML
  var neutralDistributionTotalNeededElementsLabel: jfxsc.Label = _

  @jfxf.FXML
  var neutralDistributionTotalSetElementsLabel: jfxsc.Label = _


  @jfxf.FXML
  var neutralDistributionTotalFireElementsLabel: jfxsc.Label = _


  @jfxf.FXML
  var neutralDistributionTotalWaterElementsLabel: jfxsc.Label = _


  @jfxf.FXML
  var neutralDistributionTotalEarthElementsLabel: jfxsc.Label = _

  @jfxf.FXML
  var neutralDistributionTotalAirElementsLabel: jfxsc.Label = _

  @jfxf.FXML
  var neutralDistributionActualAirElementsLabel: jfxsc.Label = _

  @jfxf.FXML
  var neutralDistributioActualEarthElementsLabel: jfxsc.Label = _

  @jfxf.FXML
  var neutralDistributionActualWaterElementsLabel: jfxsc.Label = _

  @jfxf.FXML
  var neutralDistributionActualFireElementsLabel: jfxsc.Label = _

  @jfxf.FXML
  def neutralDistributionOnCancelClicked(event: jfxe.Event): Unit = neutralDistributionOnCancelClickedDelegate()

  private var neutralDistributionOnCancelClickedDelegate: () => Unit = _


  @jfxf.FXML
  def neutralDistributionOnOkClicked(event: jfxe.Event): Unit = neutralDistributionOnOkClickedDelegate

  private var neutralDistributionOnOkClickedDelegate: () => Unit = _

  @jfxf.FXML
  def neutralDistributionOnFirePlusClicked(event: jfxe.Event): Unit = neutralDistributionOnFirePlusClickedDelegate()

  private var neutralDistributionOnFirePlusClickedDelegate: () => Unit = _

  @jfxf.FXML
  def neutralDistributionOnFireMinusClicked(event: jfxe.Event): Unit = neutralDistributionOnFireMinusClickedDelegate()

  private var neutralDistributionOnFireMinusClickedDelegate: () => Unit = _

  @jfxf.FXML
  def neutralDistributionOnWaterPlusClicked(event: jfxe.Event): Unit = neutralDistributionOnWaterPlusClickedDelegate()

  private var neutralDistributionOnWaterPlusClickedDelegate: () => Unit = _

  @jfxf.FXML
  def neutralDistributionOnWaterMinusClicked(event: jfxe.Event): Unit = neutralDistributionOnWaterMinusClickedDelegate()

  private var neutralDistributionOnWaterMinusClickedDelegate: () => Unit = _

  @jfxf.FXML
  def neutralDistributionOnEarthPlusClicked(event: jfxe.Event): Unit = neutralDistributionOnEarthPlusClickedDelegate()

  private var neutralDistributionOnEarthPlusClickedDelegate: () => Unit = _

  @jfxf.FXML
  def neutralDistributionOnEarthMinusClicked(event: jfxe.Event): Unit = neutralDistributionOnEarthMinusClickedDelegate()

  private var neutralDistributionOnEarthMinusClickedDelegate: () => Unit = _

  @jfxf.FXML
  def neutralDistributionOnAirPlusClicked(event: jfxe.Event): Unit = neutralDistributionOnAirPlusClickedDelegate()

  private var neutralDistributionOnAirPlusClickedDelegate: () => Unit = _

  @jfxf.FXML
  def neutralDistributionOnAirMinusClicked(event: jfxe.Event): Unit = neutralDistributionOnAirMinusClickedDelegate()

  private var neutralDistributionOnAirMinusClickedDelegate: () => Unit = _

  def initialize(url: URL, rb: ResourceBundle): Unit = {

  }

  def postInitialize(handler: ActorRef, properties: Properties) = {
    this.handler = handler


    cardsOnTableController = new CardsOnTableController(
      this.root.getScene,
      this.root,
      this.detailsCardTemplate,
      this.fortuneOkButton,
      this.cardOnTableTemplate,
      this.allySelectedCardOnTableTemplate,
      this.enemySelectedCardOnTableTemplate,
      this.cardShadowOnTableTemplate,
      this.ownCardHBoxPane,
      this.enemyCardHBoxPane,
      this.ownCardScrollPane,
      this.enemyCardScrollPane,
      this.cardDetailsPane,
      this.ownHpDropPane,
      this.myHpProgressBar,
      this.enemyHpDropPane,
      this.enemyHpProgressBar,
      this.ownCardTableHelpPane,
      this.enemyCardTableHelpPane,
      this.attackCursor,
      this.defendCursor
    )

    cardsOnGraveyardController = new CardsOnGraveyardController(
      this.root.getScene,
      this.cardOnTableTemplate,
      this.detailsCardTemplate,
      this.fortuneOkButton,
      this.cardDetailsPane,
      this.graveyardOwnCardsScrollPane,
      this.graveyardOwnCardsHBoxPane,
      this.graveyardOwnCardsTableLeftArrowImageView,
      this.graveyardOwnCardsTableRightArrowImageView,
      this.graveyardEnemyCardsScrollPane,
      this.graveyardEnemyCardsHBoxPane,
      this.graveyardEnemyCardsTableLeftArrowImageView,
      this.graveyardEnemyCardsTableRightArrowImageView
    )

    chatController = new ChatController(
      root,
      chatInputBoxTextField,
      chatSendButtonLabel,
      chatMessagesScrollPale,
      chatMessagesVBox,
      chatSwitchToTeamChatLabel,
      switchToAllChatLabel,
      chatAllyMessageTemplatePane,
      chatEnemyMessageTemplatePane,
      (teamMessage: String) => {
        handler ! SendMessageInteraction(teamMessage, false)
      },
      (allMessage: String) => {
        handler ! SendMessageInteraction(allMessage, true)
      }
    )

    this.ownCardHBoxPane.widthProperty().onChange({
      refreshOwnTableScrollPane
      recreateEnemyTargetInteractionLines
      recreateOwnTargetInteractionLines
    })

    this.enemyCardHBoxPane.widthProperty().onChange({
      refreshEnemyTableScrollPane
      recreateEnemyTargetInteractionLines
      recreateOwnTargetInteractionLines
    })

    myHand.getChildren.clear()

    recreateHands
    recreateEnemyTable
    recreateOwnTable

    ownCardScrollPane.hvalueProperty().onChange({
      recreateOwnTargetInteractionLines
      recreateEnemyTargetInteractionLines

      val limitMax = ownCardScrollPane.getHmax - (ownCardScrollPane.getHmax / 100) * ((ownCardHBoxPane.getPadding.getRight / ownCardHBoxPane.getWidth) * 100)
      if (ownCardScrollPane.getHvalue >= limitMax) {
        ownTableRightArrowImageView.setVisible(false)
      } else {
        ownTableRightArrowImageView.setVisible(true)
      }

      val limitMin = ownCardScrollPane.getHmin + (ownCardScrollPane.getHmax / 100) * ((ownCardHBoxPane.getPadding.getLeft / ownCardHBoxPane.getWidth) * 100)
      if (ownCardScrollPane.getHvalue <= limitMin) {
        ownTableLeftArrowImageView.setVisible(false)
      } else {
        ownTableLeftArrowImageView.setVisible(true)
      }

    })

    enemyCardScrollPane.hvalueProperty().onChange({
      recreateOwnTargetInteractionLines
      recreateEnemyTargetInteractionLines

      val limitMax = enemyCardScrollPane.getHmax - (enemyCardScrollPane.getHmax / 100) * ((enemyCardHBoxPane.getPadding.getRight / enemyCardHBoxPane.getWidth) * 100)
      if (enemyCardScrollPane.getHvalue >= limitMax) {
        enemyTableRightArrowImageView.setVisible(false)
      } else {
        enemyTableRightArrowImageView.setVisible(true)
      }

      val limitMin = enemyCardScrollPane.getHmin + (enemyCardScrollPane.getHmax / 100) * ((enemyCardHBoxPane.getPadding.getLeft / enemyCardHBoxPane.getWidth) * 100)
      if (enemyCardScrollPane.getHvalue <= limitMin) {
        enemyTableLeftArrowImageView.setVisible(false)
      } else {
        enemyTableLeftArrowImageView.setVisible(true)
      }

    })


    enemyCardHBoxPane.widthProperty().onChange({
      refreshEnemyTableScrollPane
      recreateEnemyTargetInteractionLines
      recreateOwnTargetInteractionLines
    })

    enemyCardHBoxPane.heightProperty.onChange({
      refreshEnemyTableScrollPane
      recreateEnemyTargetInteractionLines
      recreateOwnTargetInteractionLines
    })



    resetFortuneRoll
    //    goToAttacking
  }

  def setTableState(state: TableState.Value): Unit = {
    tableState = state
  }

  def actualizeCardsInHand(list: List[CardHolder]): Unit = {
    cardsInHand = list
    hideHands
    recreateHands
  }

  def actualizeOwnTable(list: List[CardHolder]): Unit = {
    cardsOnOwnTable = list
  }

  def actualizeAllyTable(list: List[CardHolder]): Unit = {
    allyCardsOnOwnTable = list
  }

  def actualizeEnemyTable(list: List[CardHolder]): Unit = {
    cardsOnEnemyTable = list
  }

  def actualizeOwnGraveyard(list: List[CardHolder]): Unit = {
    cardsOnOwnGraveyard = list
  }

  def actualizeEnemyGraveyard(list: List[CardHolder]): Unit = {
    cardsOnEnemyGraveyard = list
  }


  def actualizePlayerVision(playerVision: PlayerVision): Unit = {
    this.cardsOnTableController.disableActualMode
    players = playerVision.players

    playerVision.players.find(p => p.relationship == PlayerRelationship.Me).map(me => {
      myFireLabel setText me.elements.fire.toString
      myWaterLabel setText me.elements.water.toString
      myEarthLabel setText me.elements.earth.toString
      myAirLabel setText me.elements.air.toString
      myDeckSizeLabel setText me.deckSize.toString
      myHandSizeLabel setText me.handsSize.toString
      myGraveyardSizeLabel setText me.graveyardSize.toString
      myNameLabel.setText(me.player)
      //   cardsInHand = me.hand.toList
    })

    playerVision.players.find(p => p.relationship == PlayerRelationship.Ally).map(ally => {
      allyFireLabel setText ally.elements.fire.toString
      allyWaterLabel setText ally.elements.water.toString
      allyEarthLabel setText ally.elements.earth.toString
      allyAirLabel setText ally.elements.air.toString
      allyDeckSizeLabel setText ally.deckSize.toString
      allyHandSizeLabel setText ally.handsSize.toString
      allyGraveyardSizeLabel setText ally.graveyardSize.toString
      allyNameLabel.setText(ally.player)
    }).orElse({
      allyFireLabel setVisible (false)
      allyWaterLabel setVisible (false)
      allyEarthLabel setVisible (false)
      allyAirLabel setVisible (false)
      allyDeckSizeLabel setVisible (false)
      allyHandSizeLabel setVisible (false)
      allyGraveyardSizeLabel setVisible (false)

      allyFireImageView setVisible (false)
      allyWaterImageView setVisible (false)
      allyAirImageView setVisible (false)
      allyEarthImageView setVisible (false)
      allyDeckImageView setVisible (false)
      allyHandImageView setVisible (false)
      allyGraveyardImageView setVisible (false)
      allyNameLabel.setVisible(false)
      None
    })

    playerVision.players.find(p => p.relationship == PlayerRelationship.Enemy1).map(enemy1 => {
      enemy1FireLabel setText enemy1.elements.fire.toString
      enemy1WaterLabel setText enemy1.elements.water.toString
      enemy1EarthLabel setText enemy1.elements.earth.toString
      enemy1AirLabel setText enemy1.elements.air.toString
      enemy1DeckSizeLabel setText enemy1.deckSize.toString
      enemy1HandSizeLabel setText enemy1.handsSize.toString
      enemy1GraveyardSizeLabel setText enemy1.graveyardSize.toString
      enemy1NameLabel.setText(enemy1.player)
    })

    playerVision.players.find(p => p.relationship == PlayerRelationship.Enemy2).map(enemy2 => {
      enemy2FireLabel setText enemy2.elements.fire.toString
      enemy2WaterLabel setText enemy2.elements.water.toString
      enemy2EarthLabel setText enemy2.elements.earth.toString
      enemy2AirLabel setText enemy2.elements.air.toString
      enemy2DeckSizeLabel setText enemy2.deckSize.toString
      enemy2HandSizeLabel setText enemy2.handsSize.toString
      enemy2GraveyardSizeLabel setText enemy2.graveyardSize.toString
      enemy2NameLabel.setText(enemy2.player)
    }).orElse({
      enemy2FireLabel setVisible (false)
      enemy2WaterLabel setVisible (false)
      enemy2EarthLabel setVisible (false)
      enemy2AirLabel setVisible (false)
      enemy2DeckSizeLabel setVisible (false)
      enemy2HandSizeLabel setVisible (false)
      enemy2GraveyardSizeLabel setVisible (false)

      enemy2FireImageView setVisible (false)
      enemy2WaterImageView setVisible false
      enemy2EarthImageView setVisible false
      enemy2AirImageView setVisible false
      enemy2DeckImageView setVisible false
      enemy2HandImageView setVisible false
      enemy2GraveyardImageView setVisible false

      enemy2NameLabel.setVisible(false)
      None
    })

    //cardsOnEnemyTable = enemyTeamTable
    //cardsOnOwnTable = myTeamTable

    if (startingEnemyHp == 0) startingEnemyHp = playerVision.enemyTeamHp
    if (startingMyHp == 0) startingMyHp = playerVision.myTeamHp

    myHpTextValue.setText(playerVision.myTeamHp.toString)
    enemyHpTextValue.setText(playerVision.enemyTeamHp.toString)

    myHpProgressBar.setProgress(playerVision.myTeamHp / startingMyHp)
    enemyHpProgressBar.setProgress(playerVision.enemyTeamHp / startingEnemyHp)


    if (subRoundController != null) subRoundController.forceEnd
    subRoundController = new SubRoundBuilder()
    val time = new DateTime(DateTimeZone.UTC)
    val shifted = time.getMillis - playerVision.timeAnchor
    val limit = playerVision.timeLimit - shifted

    subRoundController(subRoundPane, playerVision.tableState, () => {
      playerVision.tableState match {
        case TableState.Using =>
          handler ! AttackerUsesSubRoundEndInteraction
        case TableState.CounterUsing =>
          handler ! DefenderCounterUsesSubRoundEndInteraction
        case TableState.Attacking =>
          handler ! AttackerAttacksSubRoundEndInteraction
        case TableState.Defending =>
          handler ! DefenderDefendsSubRoundEndInteraction
        case st =>
      }
    }, limit)

    val enemyTable = recreateEnemyTable
    val ownTable = recreateOwnTable

    allyTargetInteractions = List()
    allyTargetInteractionsLayer.getChildren.clear()

    enemyTargetInteractions = List()
    enemyTargetInteractionsLayer.getChildren.clear()

    val interactionController = new PlayerInteractionsController(allyLineTemplate, enemyLineTemplate)
    interactionController(
      players.map(p => {
        (p.using ::: p.counterUsing ::: p.attacking ::: p.defending).map(interaction => interaction)
      }).flatten,
      playerVision.players,
      ownTable,
      enemyTable,
      myHpProgressBar,
      enemyHpProgressBar
    ).map(ti => {
      ti.source match {
        case TableInteractionSource.Ally =>
          allyTargetInteractions = ti :: allyTargetInteractions
        case TableInteractionSource.Enemy =>
          enemyTargetInteractions = ti :: enemyTargetInteractions
      }
    })

    //  cardsOnOwnTable = List(CardHolder.EMPTY, CardHolder.EMPTY)

    // val ownTable = recreateOwnTable
    //   allyTargetInteractions = TargetInteraction(ownTable.head._1, ownTable.last._1, new Polyline(), TableInteractionSource.Ally, TableTarget.MyCard, null) :: allyTargetInteractions

    recreateEnemyTargetInteractionLines
    recreateOwnTargetInteractionLines

    //showNeutralDistribution(CardHolder.EMPTY.copy(price = CardHolder.EMPTY.price.copy(neutral = 2)))

    this.cardsOnGraveyardController.repaintGraveyard(
      this.cardsOnOwnGraveyard,
      this.cardsOnEnemyGraveyard
    )


    playerVision.tableState match {
      case TableState.StartRolling =>
      case TableState.FortuneRolling =>
      case TableState.Using =>
      case TableState.CounterUsing =>
      case TableState.Attacking =>
        if (possibleCardToAttack.size > 0) {
          cardsOnTableController.setAttackMode(
            possibleCardToAttack.map(pa => pa.card),
            (ch) => {
              possibleCardToAttack.find(pa => pa.card == ch).map(pa => {
                handler ! AttackedWithCardInteraction(pa.withPosition, ch.id, NeutralDistribution.ZERO, (par: AnyRef) => {
                  par match {
                    case attacks: List[NITSession.AttackerAttacks] =>
                      possibleCardToAttack = attacks
                    case t =>
                  }
                },
                  () => {

                  })
              })
            },
            () => {}
          )
        }
      case TableState.Defending =>
        playerVision.players.find(p => p.relationship == PlayerRelationship.Me).map(me => {
          val defendingOnTable = possibleCardToDefense.filter(p => p.withPosition.isInstanceOf[OnTableCardPosition] && !me.defending.exists(d => d.withPosition =? p.withPosition))
          if (defendingOnTable.size > 0) {
            cardsOnTableController.setDefenseModeWithCardsOnTable(
              defendingOnTable.map(dot => dot.againts match {
                case t: CardIsTarget =>
                  Some(CardPositionToCardHolder(dot.withPosition, players) -> CardPositionToCardHolder(t.position, players))
                case t => None
              }).filter(ch => ch.isDefined).map(ch => ch.get),
              (ch1, ch2) => {
                CardHolderToCardPosition(ch1, players).map(pos => {
                  CardHolderToCardPosition(ch2, players).map(tPos => {
                    handler ! DefendWithCardInteraction(
                      pos,
                      ch1.id,
                      CardIsTarget(tPos),
                      NeutralDistribution.ZERO, (par: AnyRef) => {
                        par match {
                          case defends: List[NITSession.DefenderDefends] =>
                            possibleCardToDefense = defends
                          case t =>
                        }
                      },
                      () => {

                      }
                    )
                  })
                })
              }
            )
          }
        })
      case t =>
    }

    if (playerVision.matchEnded) {
      showMatchEnds(playerVision.win)
    }
  }

  def resetPossibilities = {
    possibleCardToUse = List()
    possibleCardToCounterUse = List()
    possibleCardToAttack = List()
    possibleCardToDefense = List()
  }

  def resetFortuneRoll: Unit = {

    fortuneOkButton.setVisible(false)

    myRollResultLabel.setText("")
    enemyRollResultLabel.setText("")
    fortuneWheelNumberTexts = fortuneWheel
      .getChildren.filter(ch => {
      ch.isInstanceOf[Text]
    }).map(ch => {
      ch.asInstanceOf[Text].setText("")
      ch.asInstanceOf[Text]
    }).toList

    playerMode match {
      case PlayersMode.OneVsOne =>
        myRollBackgroundShape.setVisible(false)
        twoVsTwoMyNameLabel.setVisible(false)
        twoVsTwoMyRollLabel.setVisible(false)

        allyRollBackgroundShape.setVisible(false)
        twoVsTwoAllyNameLabel.setVisible(false)
        twoVsTwoAllyRollLabel.setVisible(false)

        enemy1RollBackgroundShape.setVisible(false)
        twoVsTwoEnemy1NameLabel.setVisible(false)
        twoVsTwoEnemy1RollLabel.setVisible(false)

        enemy2RollBackgroundShape.setVisible(false)
        twoVsTwoEnemy2NameLabel.setVisible(false)
        twoVsTwoEnemy2RollLabel.setVisible(false)

        players.find(p => p.relationship == PlayerRelationship.Enemy1).map(f => {
          oneVsOneEnemyNameLabel.setText(f.player)
        })

        players.find(p => p.relationship == PlayerRelationship.Me).map(f => {
          oneVsOneMyNameLabel.setText(f.player)
        })

      case PlayersMode.TwoVsTwo =>
        twoVsTwoMyRollLabel.setText("")
        twoVsTwoAllyRollLabel.setText("")
        twoVsTwoEnemy1RollLabel.setText("")
        twoVsTwoEnemy2RollLabel.setText("")

        oneVsOneEnemyNameLabel.setVisible(false)
        oneVsOneMyNameLabel.setVisible(false)

        players.find(p => p.relationship == PlayerRelationship.Me).map(f => {
          twoVsTwoMyNameLabel.setText(f.player)
        })

        players.find(p => p.relationship == PlayerRelationship.Ally).map(f => {
          twoVsTwoAllyNameLabel.setText(f.player)
        })

        players.find(p => p.relationship == PlayerRelationship.Enemy1).map(f => {
          twoVsTwoEnemy1NameLabel.setText(f.player)
        })

        players.find(p => p.relationship == PlayerRelationship.Enemy2).map(f => {
          twoVsTwoEnemy2NameLabel.setText(f.player)
        })

    }
  }

  def fortuneRoll(position: NITSession.PlayerRollPosition): Unit = {
    lastRollPosition = position
    val rNumber = 1 + Random.nextInt(5)
    showFortuneAwaits(position, rNumber)
    Thread.sleep(1000)
    handler ! RollInteraction(position, rNumber)
  }

  def showFortuneAwaits(position: NITSession.PlayerRollPosition, number: Int): Unit = {
    waitingPaneRoll.setVisible(true)
    waitingPaneRollNumberLabel.setText(number.toString)
    waitingPaneRollPositionImageView.setImage(position match {
      case NITSession.PLAYER_ROLL_POSITION_ATTACKER =>
        attackerImage
      case NITSession.PLAYER_ROLL_POSITION_DEFENDER =>
        defenderImage
    })
  }

  def fortuneWheel(rollResult: PlayerRollResult, playerName: String): Unit = {
    waitingPaneRoll.setVisible(false)

    val won = rollResult.getWinnerPlayersList.asScala.find(p => p.getPlayer == playerName).isDefined
    val allPlayers = (rollResult.getLoserPlayersList.asScala ++ rollResult.getWinnerPlayersList.asScala).toList
    var attackerNumber = 0
    var defenderNumber = 0

    allPlayers.map(p => if (p.getRollPositionAttacker) {
      attackerNumber += p.getRoll
    } else {
      defenderNumber += p.getRoll
    })

    playerMode match {
      case PlayersMode.OneVsOne =>
      case PlayersMode.TwoVsTwo =>
        players.find(p => p.relationship == PlayerRelationship.Me).map(found => {
          twoVsTwoMyRollLabel.setText(allPlayers.find(p => p.getPlayer == found.player).map(f => f.getRoll.toString).getOrElse(""))
          //      twoVsTwoMyNameLabel.setText(found.player)
        })

        players.find(p => p.relationship == PlayerRelationship.Ally).map(found => {
          //      twoVsTwoAllyNameLabel.setText(found.player)
          twoVsTwoAllyRollLabel.setText(allPlayers.find(p => p.getPlayer == found.player).map(f => f.getRoll.toString).getOrElse(""))
        })

        players.find(p => p.relationship == PlayerRelationship.Enemy1).map(found => {
          //  twoVsTwoEnemy1NameLabel.setText(found.player)
          twoVsTwoEnemy1RollLabel.setText(allPlayers.find(p => p.getPlayer == found.player).map(f => f.getRoll.toString).getOrElse(""))

        })

        players.find(p => p.relationship == PlayerRelationship.Enemy2).map(found => {
          //     twoVsTwoEnemy1NameLabel.setText(found.player)
          twoVsTwoEnemy1RollLabel.setText(allPlayers.find(p => p.getPlayer == found.player).map(f => f.getRoll.toString).getOrElse(""))
        })
    }

    lastRollPosition match {
      case NITSession.PLAYER_ROLL_POSITION_DEFENDER =>
        enemyRollResultLabel.setText(attackerNumber.toString)
        myRollResultLabel.setText(defenderNumber.toString)

        rollPositionImageView.setImage(defenderImage)
        enemyRollPositionImageView.setImage(attackerImage)

        val start = if (defenderNumber.get + 4 > 6) {
          defenderNumber.get + 4 - 6
        } else {
          defenderNumber.get + 4
        }

        var offset = 0
        fortuneWheelNumberTexts.map(t => {
          t.setText(
            {
              if ((start - offset) <= 0) {
                (start - offset) + 6
              } else {
                (start - offset)
              }
            }.toString)
          offset += 1
        })

        if (won) {
          tableState match {
            case TableState.StartRolling =>
              fortuneRollResult.setText("You have won!. You are lucky and turn first!")
            case TableState.FortuneRolling =>
              fortuneRollResult.setText("You have won!. You are lucky!")
            case t =>
          }
        } else {
          tableState match {
            case TableState.StartRolling =>
              fortuneRollResult.setText("You have lost!. You are unlucky and turn last!")
            case TableState.FortuneRolling =>
              fortuneRollResult.setText("You have lost!. You are unlucky!")
            case t =>
          }
        }

        fortuneOkButton.setVisible(true)


      case NITSession.PLAYER_ROLL_POSITION_ATTACKER =>

        myRollResultLabel.setText(attackerNumber.toString)
        enemyRollResultLabel.setText(defenderNumber.toString)

        rollPositionImageView.setImage(attackerImage)
        enemyRollPositionImageView.setImage(defenderImage)

        val start = if (defenderNumber.get + 1 > 6) {
          1
        } else {
          defenderNumber.get + 1
        }

        var offset = 0
        fortuneWheelNumberTexts.map(t => {
          t.setText(
            {
              if ((start - offset) <= 0) {
                (start - offset) + 6
              } else {
                (start - offset)
              }
            }.toString)
          offset += 1
        })

        if (won) {
          tableState match {
            case TableState.StartRolling =>
              fortuneRollResult.setText("You have won!. You are lucky and turn first!")
            case TableState.FortuneRolling =>
              fortuneRollResult.setText("You have won!. You are lucky!")
            case t =>
          }
        } else {
          tableState match {
            case TableState.StartRolling =>
              fortuneRollResult.setText("You have lost!. You are unlucky and turn last!")
            case TableState.FortuneRolling =>
              fortuneRollResult.setText("You have lost!. You are unlucky!")
            case t =>
          }
        }

        fortuneOkButton.setVisible(true)
    }
  }

  def recreateHands = {
    myHand.getChildren.clear()
    paneCardPairs = List()
    cardsInHand.map(c => {
      /*  val pane = new Pane()
        pane.setPrefHeight(cardInHandTemplate.getPrefHeight)
        pane.setPrefWidth(cardInHandTemplate.getPrefWidth)

        pane.setLayoutX(cardInHandTemplate.getLayoutX)
        pane.setLayoutY(cardInHandTemplate.getLayoutY)

        pane.setStyle(cardInHandTemplate.getStyle);*/
      val normalCardPane = NormalCardBuilder(c, (card: CardHolder) => {
        //  cardDetailsPane.getChildren.clear()
        cardDetailsPane.getChildren.map(ch => paneCardPairs = paneCardPairs.filter(pc => pc._1 != ch))
        cardDetailsPane.getChildren.setAll(cardDetailsPane.getChildren.head)
        val newPane = DetailedCardBuilder(c, detailsCardTemplate)
        val vBox = detailsCardTemplate.getChildren.toList.find(n => n.isInstanceOf[VBox])
        val boxCorrection: Double = vBox.map(vb => (vb.getLayoutBounds.getWidth / 2)).getOrElse(0.0)

        newPane.setLayoutX((cardDetailsPane.getWidth / 2) - (detailsCardTemplate.getLayoutBounds.getWidth / 2) - boxCorrection)
        newPane.setLayoutY((cardDetailsPane.getHeight / 2) - (detailsCardTemplate.getLayoutBounds.getHeight / 2))

        val button = new Label()
        button.setText(fortuneOkButton.getText)
        button.setFont(fortuneOkButton.getFont)
        button.setTextFill(Color.WHITE)
        button.setPrefHeight(fortuneOkButton.getPrefHeight)
        button.setPrefWidth(fortuneOkButton.getPrefWidth)
        // button.setStyle(fortuneOkButton.getStyle)
        button.setLayoutX(fortuneOkButton.getLayoutX)
        button.setLayoutY(fortuneOkButton.getLayoutY)
        button.setCursor(Cursor.HAND)

        button.setOnMouseClicked(new EventHandler[MouseEvent] {
          override def handle(p1: MouseEvent): Unit = {
            cardDetailsPane.setVisible(false)
          }
        })

        cardDetailsPane.getChildren.addAll(newPane, button)
        cardDetailsPane.setVisible(true)

      }, normalCardTemplate)

      normalCardPane.setStyle(cardsOnTableAttackHighlightedOff)
      normalCardPane.setLayoutX(cardInHandTemplate.getLayoutX)
      normalCardPane.setLayoutY(cardInHandTemplate.getLayoutY)
      val pressHandler = new Pane()
      pressHandler.setPrefWidth(normalCardTemplate.getPrefWidth)
      pressHandler.setPrefHeight(normalCardTemplate.getPrefHeight / 2)
      normalCardPane.asInstanceOf[jfxsl.Pane].getChildren.add(pressHandler)

      myHand.getChildren.add(normalCardPane)
      paneCardPairs = normalCardPane.asInstanceOf[jfxsl.Pane] -> c :: paneCardPairs
    })

  }

  def recreateOwnTable = {
    ownTablePaneCardPairs = this.cardsOnTableController.paintOwnCards(
      cardsOnOwnTable, allyCardsOnOwnTable
    )
    ownTablePaneCardPairs.map(p => {
      p._1.boundsInParentProperty().onChange({
        recreateOwnTargetInteractionLines
      })
      p
    })
  }


  def recreateEnemyTable = {

    enemyTablePaneCardPairs = this.cardsOnTableController.paintEnemyCards(
      cardsOnEnemyTable
    )
    enemyTablePaneCardPairs.map(p => {
      p._1.boundsInParentProperty().onChange({
        recreateEnemyTargetInteractionLines
      })
      p
    })
  }

  def recreateEnemyTargetInteractionLines: Unit = {
    enemyTargetInteractionsLayer.getChildren.clear()
    enemyTargetInteractions.map(t => {
      t.target match {
        case TableTarget.MyCard =>
          t.line match {
            case line: Line =>
              val startPoint = t.pane.localToScene(0.0, 0.0)
              val layerPoint = enemyTargetInteractionsLayer.localToScene(0.0, 0.0)
              println("lp: " + layerPoint)
              val start = new Point2D(
                startPoint.x - layerPoint.x + (t.pane.getLayoutBounds.getWidth / 2),
                startPoint.y - layerPoint.y + t.pane.getLayoutBounds.getHeight
              )

              val endPoint = t.targetPane.localToScene(0.0, 0.0)
              val end = new Point2D(
                endPoint.x - layerPoint.x + (t.targetPane.getLayoutBounds.getWidth / 2),
                endPoint.y - layerPoint.y
              )

              line.setStartX(start.x)
              line.setStartY(start.y)
              line.setEndX(end.x)
              line.setEndY(end.y)

              val ending = new ImageView(enemyLineEnd)
              ending.setLayoutX(end.x - (enemyLineEnd.getWidth / 2))
              ending.setLayoutY(end.y - (enemyLineEnd).getHeight / 2)

              val starting = new Arc()
              starting.setStroke(Color.web("#D64960"))
              starting.setFill(Color.web("#D64960"))
              starting.setStartAngle(180.0)
              starting.setLength(180)
              starting.setRadiusX(6)
              starting.setRadiusY(6)
              starting.setCenterX(start.x)
              starting.setCenterY(start.y)

              enemyTargetInteractionsLayer.getChildren.add(ending)
              enemyTargetInteractionsLayer.getChildren.add(t.line)
              enemyTargetInteractionsLayer.getChildren.add(starting)
          }

        case TableTarget.MyHealth =>
          t.line match {
            case line: Line =>
              val startPoint = t.pane.localToScene(0.0, 0.0)
              println(startPoint)
              val layerPoint = enemyTargetInteractionsLayer.localToScene(0.0, 0.0)
              val start = new Point2D(
                startPoint.x - layerPoint.x + (t.pane.getLayoutBounds.getWidth / 2),
                startPoint.y - layerPoint.y + t.pane.getLayoutBounds.getHeight
              )

              val endPoint = t.targetPane.localToScene(0.0, 0.0)
              val end = new Point2D(
                layerPoint.x + endPoint.x /*- (t.targetPane.getLayoutBounds.getWidth / 2)*/ ,
                endPoint.y - layerPoint.y - t.targetPane.getLayoutBounds.getHeight
              )

              line.setStartX(start.x)
              line.setStartY(start.y)
              line.setEndX(end.x)
              line.setEndY(end.y)

              val ending = new ImageView(enemyLineEnd)
              ending.setLayoutX(end.x - (enemyLineEnd.getWidth / 2))
              ending.setLayoutY(end.y - (enemyLineEnd).getHeight / 2)

              val starting = new Arc()
              starting.setStroke(Color.web("#D64960"))
              starting.setFill(Color.web("#D64960"))
              starting.setStartAngle(180.0)
              starting.setLength(180)
              starting.setRadiusX(6)
              starting.setRadiusY(6)
              starting.setCenterX(start.x)
              starting.setCenterY(start.y)

              enemyTargetInteractionsLayer.getChildren.add(ending)
              enemyTargetInteractionsLayer.getChildren.add(t.line)
              enemyTargetInteractionsLayer.getChildren.add(starting)
          }

        case TableTarget.EnemyCard =>
          t.line match {
            case line: Polyline =>
              val startPoint = t.pane.localToScene(0.0, 0.0)
              val layerPoint = enemyTargetInteractionsLayer.localToScene(0.0, 0.0)
              val start = new Point2D(
                startPoint.x - layerPoint.x + (t.pane.getLayoutBounds.getWidth / 2),
                startPoint.y - layerPoint.y
              )

              val endPoint = t.targetPane.localToScene(0.0, 0.0)
              val end = new Point2D(
                endPoint.x - layerPoint.x + (t.targetPane.getLayoutBounds.getWidth / 2),
                endPoint.y - layerPoint.y
              )

              /*
              line.setStartX(start.x)
              line.setStartY(start.y)
              line.setEndX(end.x)
              line.setEndY(end.y)
*/
              line.getPoints.clear()
              line.getPoints.setAll(
                start.x, start.y,
                start.x, start.y - 10,
                end.x, end.y - 10,
                end.x, end.y
              )
              line.setFill(Color.TRANSPARENT)
              val ending = new ImageView(enemyLineEnd)
              ending.setLayoutX(end.x - (enemyLineEnd.getWidth / 2))
              ending.setLayoutY(end.y - (enemyLineEnd).getHeight / 2)

              val starting = new Arc()
              starting.setStroke(Color.web("#D64960"))
              starting.setFill(Color.web("#D64960"))
              starting.setStartAngle(0.0)
              starting.setLength(180)
              starting.setRadiusX(6)
              starting.setRadiusY(6)
              starting.setCenterX(start.x)
              starting.setCenterY(start.y)

              enemyTargetInteractionsLayer.getChildren.add(ending)
              enemyTargetInteractionsLayer.getChildren.add(t.line)
              enemyTargetInteractionsLayer.getChildren.add(starting)
          }

        case TableTarget.EnemyHealth =>
          t.line match {
            case line: Line =>
              println("layout" + t.pane.getLayoutBounds())
              val startPoint = t.pane.localToScene(0.0, 0.0)
              val layerPoint = enemyTargetInteractionsLayer.localToScene(0.0, 0.0)

              println("e sp: " + startPoint)
              println("e lp: " + layerPoint)

              val start = new Point2D(
                startPoint.x - layerPoint.x + (t.pane.getLayoutBounds.getWidth / 2),
                startPoint.y - layerPoint.y
              )

              val endPoint = t.targetPane.localToScene(0.0, 0.0)
              val end = new Point2D(
                layerPoint.x + endPoint.x /*+ (t.targetPane.getLayoutBounds.getWidth / 2)*/ ,
                endPoint.y - layerPoint.y + t.targetPane.getBoundsInLocal.getHeight + 6
              )

              line.setStartX(start.x)
              line.setStartY(start.y)
              line.setEndX(end.x)
              line.setEndY(end.y)

              val ending = new ImageView(enemyLineEnd)
              ending.setLayoutX(end.x - (enemyLineEnd.getWidth / 2))
              ending.setLayoutY(end.y - (enemyLineEnd).getHeight / 2)

              val starting = new Arc()
              starting.setStroke(Color.web("#D64960"))
              starting.setFill(Color.web("#D64960"))
              starting.setStartAngle(0.0)
              starting.setLength(180)
              starting.setRadiusX(6)
              starting.setRadiusY(6)
              starting.setCenterX(start.x)
              starting.setCenterY(start.y)

              enemyTargetInteractionsLayer.getChildren.add(ending)
              enemyTargetInteractionsLayer.getChildren.add(t.line)
              enemyTargetInteractionsLayer.getChildren.add(starting)
          }
      }

    })

  }

  def recreateOwnTargetInteractionLines: Unit = {
    allyTargetInteractionsLayer.getChildren.clear()
    allyTargetInteractions.map(t => {
      t.target match {
        case TableTarget.EnemyCard =>
          t.line match {
            case line: Line =>
              val startPoint = t.pane.localToScene(0.0, 0.0)
              val layerPoint = allyTargetInteractionsLayer.localToScene(0.0, 0.0)
              val start = new Point2D(
                startPoint.x - layerPoint.x + (t.pane.getLayoutBounds.getWidth / 2),
                startPoint.y - layerPoint.y - 2
              )

              val endPoint = t.targetPane.localToScene(0.0, 0.0)
              val end = new Point2D(
                endPoint.x - layerPoint.x + (t.targetPane.getLayoutBounds.getWidth / 2),
                endPoint.y - layerPoint.y + t.targetPane.getLayoutBounds.getHeight + 2
              )

              line.setStartX(start.x)
              line.setStartY(start.y)
              line.setEndX(end.x)
              line.setEndY(end.y)

              val ending = new ImageView(allyLineEnd)
              ending.setLayoutX(end.x - (allyLineEnd.getWidth / 2))
              ending.setLayoutY(end.y - (allyLineEnd).getHeight / 2)

              val starting = new Arc()
              starting.setStroke(Color.web("#148C38"))
              starting.setFill(Color.web("#148C38"))
              starting.setStartAngle(0.0)
              starting.setLength(180)
              starting.setRadiusX(6)
              starting.setRadiusY(6)
              starting.setCenterX(start.x)
              starting.setCenterY(start.y + 2)

              allyTargetInteractionsLayer.getChildren.add(ending)
              allyTargetInteractionsLayer.getChildren.add(t.line)
              allyTargetInteractionsLayer.getChildren.add(starting)
          }

        case TableTarget.EnemyHealth =>
          t.line match {
            case line: Line =>
              val startPoint = t.pane.localToScene(0.0, 0.0)
              val layerPoint = allyTargetInteractionsLayer.localToScene(0.0, 0.0)
              val start = new Point2D(
                startPoint.x - layerPoint.x + (t.pane.getLayoutBounds.getWidth / 2),
                startPoint.y - layerPoint.y - 2
              )

              val endPoint = t.targetPane.localToScene(0.0, 0.0)
              val end = new Point2D(
                layerPoint.x + endPoint.x /*- (t.targetPane.getLayoutBounds.getWidth / 2)*/ ,
                endPoint.y - layerPoint.y + t.targetPane.getLayoutBounds.getHeight + 6
              )

              line.setStartX(start.x)
              line.setStartY(start.y)
              line.setEndX(end.x)
              line.setEndY(end.y)

              val ending = new ImageView(allyLineEnd)
              ending.setLayoutX(end.x - (allyLineEnd.getWidth / 2))
              ending.setLayoutY(end.y - (allyLineEnd).getHeight / 2)

              val starting = new Arc()
              starting.setStroke(Color.web("#148C38"))
              starting.setFill(Color.web("#148C38"))
              starting.setStartAngle(0.0)
              starting.setLength(180)
              starting.setRadiusX(6)
              starting.setRadiusY(6)
              starting.setCenterX(start.x)
              starting.setCenterY(start.y + 2)

              allyTargetInteractionsLayer.getChildren.add(ending)
              allyTargetInteractionsLayer.getChildren.add(t.line)
              allyTargetInteractionsLayer.getChildren.add(starting)
          }

        case TableTarget.MyCard =>
          t.line match {
            case line: Polyline =>
              //    val startPointRemake = t.pane.getBoundsInParent()
              val startPoint = t.pane.localToScene(0, 0) //t.pane.localToScene(0.0, 0.0)
            val layerPoint = allyTargetInteractionsLayer.localToScene(0.0, 0.0)
              val start = new Point2D(
                startPoint.x - layerPoint.x + (t.pane.getLayoutBounds.getWidth / 2),
                startPoint.y - layerPoint.y + t.pane.getLayoutBounds.getHeight
              )

              val endPoint = t.targetPane.localToScene(0.0, 0.0)
              val end = new Point2D(
                endPoint.x - layerPoint.x + (t.targetPane.getLayoutBounds.getWidth / 2),
                endPoint.y - layerPoint.y + t.targetPane.getLayoutBounds.getHeight
              )

              /*
              line.setStartX(start.x)
              line.setStartY(start.y)
              line.setEndX(end.x)
              line.setEndY(end.y)
*/
              line.getPoints.clear()
              line.getPoints.setAll(
                start.x, start.y,
                start.x, start.y + 10,
                end.x, end.y + 10,
                end.x, end.y
              )

              println("Spane: " + t.pane + "| Epane: " + t.targetPane)
              println("Start point: " + startPoint + "| Endpoint: " + endPoint)
              println("Start: " + start + "| End: " + end)

              line.setFill(Color.TRANSPARENT)
              val ending = new ImageView(allyLineEnd)
              ending.setLayoutX(end.x - (allyLineEnd.getWidth / 2))
              ending.setLayoutY(end.y - (allyLineEnd).getHeight / 2)

              val starting = new Arc()
              starting.setStroke(Color.web("#148C38"))
              starting.setFill(Color.web("#148C38"))
              starting.setStartAngle(180.0)
              starting.setLength(180)
              starting.setRadiusX(6)
              starting.setRadiusY(6)
              starting.setCenterX(start.x)
              starting.setCenterY(start.y)

              allyTargetInteractionsLayer.getChildren.add(ending)
              allyTargetInteractionsLayer.getChildren.add(t.line)
              allyTargetInteractionsLayer.getChildren.add(starting)
          }

        case TableTarget.MyHealth =>
          t.line match {
            case line: Line =>
              val startPoint = t.pane.localToScene(0.0, 0.0)
              val layerPoint = allyTargetInteractionsLayer.localToScene(0.0, 0.0)
              val start = new Point2D(
                startPoint.x - layerPoint.x + (t.pane.getLayoutBounds.getWidth / 2),
                startPoint.y - layerPoint.y + t.pane.getLayoutBounds.getHeight
              )


              println("a sp: " + startPoint)
              println("a lp: " + layerPoint)

              val endPoint = t.targetPane.localToScene(0.0, 0.0)
              val end = new Point2D(
                layerPoint.x + endPoint.x /*+ (t.targetPane.getLayoutBounds.getWidth / 2)*/ ,
                endPoint.y - layerPoint.y - t.targetPane.getLayoutBounds.getHeight
              )

              line.setStartX(start.x)
              line.setStartY(start.y)
              line.setEndX(end.x)
              line.setEndY(end.y)

              val ending = new ImageView(allyLineEnd)
              ending.setLayoutX(end.x - (allyLineEnd.getWidth / 2))
              ending.setLayoutY(end.y - (allyLineEnd).getHeight / 2)

              val starting = new Arc()
              starting.setStroke(Color.web("#148C38"))
              starting.setFill(Color.web("#148C38"))
              starting.setStartAngle(180.0)
              starting.setLength(180)
              starting.setRadiusX(6)
              starting.setRadiusY(6)
              starting.setCenterX(start.x)
              starting.setCenterY(start.y)

              allyTargetInteractionsLayer.getChildren.add(ending)
              allyTargetInteractionsLayer.getChildren.add(t.line)
              allyTargetInteractionsLayer.getChildren.add(starting)
          }
      }

    })


  }

  def refreshOwnTableScrollPane = {
    recreateOwnTargetInteractionLines
    ownTableScrollPanePosition match {
      case CardScrollPanePosition.CENTER =>
        scrollOwnTableToCenter
        ownTableLeftArrowImageView.setVisible(true)
        ownTableRightArrowImageView.setVisible(true)
      case CardScrollPanePosition.LEFT =>
        ownTableLeftArrowImageView.setVisible(false)
        ownTableRightArrowImageView.setVisible(true)
        scrollOwnTableToLeft
      case CardScrollPanePosition.RIGHT =>
        ownTableRightArrowImageView.setVisible(false)
        ownTableLeftArrowImageView.setVisible(true)
        scrollOwnTableToRight
    }
  }

  def refreshEnemyTableScrollPane = {
    enemyTableScrollPanePosition match {
      case CardScrollPanePosition.CENTER =>
        scrollEnemyTableToCenter
        enemyTableLeftArrowImageView.setVisible(true)
        enemyTableRightArrowImageView.setVisible(true)
      case CardScrollPanePosition.LEFT =>
        scrollEnemyTableToLeft
        enemyTableLeftArrowImageView.setVisible(false)
        enemyTableRightArrowImageView.setVisible(true)
      case CardScrollPanePosition.RIGHT =>
        scrollEnemyTableToRight
        enemyTableLeftArrowImageView.setVisible(true)
        enemyTableRightArrowImageView.setVisible(false)
    }
  }

  def scrollOwnTableToLeft: Unit = {
    ownCardScrollPane.setHvalue(ownCardScrollPane.getHmin)
  }

  def scrollOwnTableToRight: Unit = {
    ownCardScrollPane.setHvalue(ownCardScrollPane.getHmax)
  }

  def scrollOwnTableToCenter: Unit = {
    ownCardScrollPane.setHvalue(ownCardScrollPane.getHmax / 2)
  }

  def scrollEnemyTableToLeft: Unit = {
    enemyCardScrollPane.setHvalue(enemyCardScrollPane.getHmin)
  }

  def scrollEnemyTableToRight: Unit = {
    enemyCardScrollPane.setHvalue(enemyCardScrollPane.getHmax)
  }

  def scrollEnemyTableToCenter: Unit = {
    enemyCardScrollPane.setHvalue(enemyCardScrollPane.getHmax / 2)
  }

  def showHands = {
    val howMuchVisible = root.getHeight - myHand.getLayoutY
    val by = myHand.getHeight - howMuchVisible
    if (!doingHandAnimation && !myHandIsOpened && pendingRequest_?) {
      doingHandAnimation = true
      myHandDismissPane.setMouseTransparent(false)

      Platform.runLater(new Runnable {
        def run(): Unit = {

          val initialPackagePosition = new Point() //dodelat spravne moveTo, potom prepocitat body podle vysunuteho pane, dodelat rotace.

          val parallelMoveTransition = new ParallelTransition()
          val cardMoveParallelTransition = new ParallelTransition()
          val center = new Point((myHand.getBoundsInLocal.getWidth / 2.0).toInt, (myHand.getBoundsInLocal.getHeight / 2.0).toInt)
          println(center)
          val doubleDiv = cardsInHand.size / 2
          val pivot: Double = cardsInHand.size % 2
          var leftModifier = doubleDiv
          var rightModifier = 1

          val xModifier = 30
          val yModifier = 10
          val rotateModifier = 5

          var thereWasChange = false

          for (i <- 0 to myHand.getChildren.size - 1) {
            val ch = myHand.getChildren.get(i)
            if ((pivot != 0.0 && i < doubleDiv) || (pivot == 0.0 && i < doubleDiv)) {
              val change = (pivot != 0.0 && i + 1 > doubleDiv) || (pivot == 0.0 && i + 1 >= doubleDiv) || (pivot != 0.0 && i == doubleDiv)
              if (change) thereWasChange = true
              val tempXModifier = if (change) {
                xModifier / 2
              } else {
                xModifier
              }

              val tempRotateModifer = if (change) {
                rotateModifier / 2
              } else {
                rotateModifier
              }

              val x = center.x - (ch.getBoundsInLocal.getWidth / 2.0) - (tempXModifier * leftModifier)
              val y = center.y - (ch.getBoundsInLocal.getHeight / 2.0) + (yModifier * leftModifier)

              val ct = new TranslateTransition()
              val by = new Point2D(
                center.x - ch.getLayoutX - (ch.getBoundsInLocal.getWidth / 2) - tempXModifier * leftModifier,
                ch.getLayoutY * -1 + (yModifier * leftModifier)
              )

              ct.setByX(by.x)
              ct.setByY(by.y)
              val byAngle = tempRotateModifer * leftModifier

              ct.setNode(ch)

              movedBy = (ch -> PositionWithAngle(by, byAngle, 'left)) :: movedBy

              val rt = new RotateTransition()
              rt.setByAngle(tempRotateModifer * leftModifier * -1)
              rt.setNode(ch)

              cardMoveParallelTransition.getChildren.add(ct)
              cardMoveParallelTransition.getChildren.add(rt)
              leftModifier = leftModifier - 1
            } else if ((pivot != 0.0 && i > doubleDiv) || (pivot == 0.0 && i >= doubleDiv)) {
              if (thereWasChange) thereWasChange = false

              val tempXModifier = if (thereWasChange) {
                xModifier / 2
              } else {
                xModifier
              }

              val tempRotateModifer = if (thereWasChange) {
                rotateModifier / 2
              } else {
                rotateModifier
              }

              val ct = new TranslateTransition()
              val by = new Point2D(
                center.x - ch.getLayoutX - (ch.getBoundsInLocal.getWidth / 2) + tempXModifier * rightModifier,
                ch.getLayoutY * -1 + (yModifier * rightModifier)
              )
              val byAngle = tempRotateModifer * rightModifier

              ct.setByX(by.x)
              ct.setByY(by.y)
              ct.setNode(ch)

              movedBy = (ch -> PositionWithAngle(by, byAngle, 'right)) :: movedBy

              val rt = new RotateTransition()
              rt.setByAngle(byAngle)
              rt.setNode(ch)
              //        /    println(y)
              cardMoveParallelTransition.getChildren.add(ct)
              cardMoveParallelTransition.getChildren.add(rt)
              rightModifier = rightModifier + 1
            } else if (pivot != 0.0 && i == doubleDiv) {
              val ct = new TranslateTransition()
              val by = new Point2D(
                center.x - ch.getLayoutX - (ch.getBoundsInLocal.getWidth / 2),
                ch.getLayoutY * -1
              )
              ct.setByX(by.x)
              ct.setByY(by.y)
              val byAngle = 0
              ct.setNode(ch)

              movedBy = (ch -> PositionWithAngle(by, byAngle, 'middle)) :: movedBy

              cardMoveParallelTransition.getChildren.add(ct)
            }
          }

          cardMoveParallelTransition.setOnFinished(new EventHandler[ActionEvent] {
            override def handle(event: ActionEvent): Unit = {
              doingHandAnimation = false
              myHandIsOpened = true

              myHand.getChildren.map(pane => {
                val card = paneCardPairs.find(c => c._1 == pane).map(c => c._2).getOrElse(CardHolder.EMPTY)


                if (!possibleCardToUse.exists(c => c.card == card) &&
                  !possibleCardToCounterUse.exists(c => c.card == card) &&
                  !possibleCardToDefense.exists(c => c.defenderCard == card)) {
                  pane.setOpacity(0.8)
                }

                pane.setOnMouseEntered(new EventHandler[MouseEvent]() {
                  def handle(mouseEvent: MouseEvent): Unit = {
                    val node: Node = mouseEvent.getTarget.asInstanceOf[Node]

                    //  myHand.getChildren.remove(node)
                    //myHand.getChildren.add(node)
                    if (myHandIsOpened) {
                      movedBy.find(f => f._1 == node).map(f => {
                        val alpha = f._2.angle
                        val beta = 90 - alpha
                        val x = Math.cos(Math.toRadians(beta)) * 100
                        val y = Math.cos(Math.toRadians(alpha)) * 100


                        f._2.aligment match {
                          case 'left =>
                            node.setLayoutX(node.getLayoutX - x)
                            focusedCard = Some(node -> new Point2D(x, y))

                          case 'right =>
                            node.setLayoutX(node.getLayoutX + x)
                            focusedCard = Some(node -> new Point2D(-1 * x, y))
                          case t =>
                            focusedCard = Some(node -> new Point2D(0, y))
                        }

                        node.setLayoutY(node.getLayoutY - y)
                      })
                    }
                  }
                })

                pane.setOnMouseExited(new EventHandler[MouseEvent]() {
                  def handle(mouseEvent: MouseEvent): Unit = {
                    if (myHandIsOpened) {
                      focusedCard.map(c => {
                        val node = mouseEvent.getTarget.asInstanceOf[Node]
                        if (c._1 == node) {
                          c._1.setLayoutX(c._1.getLayoutX + c._2.x)
                          c._1.setLayoutY(c._1.getLayoutY + c._2.y)
                        }
                      })
                    }
                  }
                })

                pane.asInstanceOf[jfxsl.Pane].getChildren.last.setOnMousePressed(new EventHandler[MouseEvent]() {
                  def handle(mouseEvent: MouseEvent): Unit = {
                    if (
                      (
                        tableState == TableState.Using ||
                          tableState == TableState.CounterUsing ||
                          tableState == TableState.Defending
                        ) &&
                        (
                          possibleCardToUse.exists(c => c.card == card) ||
                            possibleCardToCounterUse.exists(c => c.card == card) ||
                            possibleCardToDefense.exists(c => c.defenderCard == card)
                          )
                    ) {
                      cardDraggingXOffset = mouseEvent.getSceneX
                      cardDraggingYOffset = mouseEvent.getSceneY
                      cardDraggingRotateOffset = pane.getRotate

                      val newPane = new Pane()
                      newPane.setPrefSize(pane.getBoundsInLocal.getWidth, pane.getBoundsInLocal.getHeight)
                      newPane.setStyle(pane.getStyle)
                      newPane.setTranslateX(mouseEvent.getSceneX - pane.getBoundsInLocal.getWidth / 2)
                      newPane.setTranslateY(mouseEvent.getSceneY - 10)
                      val child = pane.asInstanceOf[jfxsl.Pane].getChildren.toList
                      newPane.getChildren.addAll(ChildrenDuplicator(child.filter(f => f != child.last)).asJava)

                      root.getChildren.add(newPane)

                      val bounds = ownCardScrollPane.getBoundsInParent()
                      val dragHandler = new EventHandler[MouseEvent]() {
                        def handle(event: MouseEvent) {
                          newPane.setTranslateX(event.getSceneX - pane.getBoundsInLocal.getWidth / 2)
                          newPane.setTranslateY(event.getSceneY - 10)

                          if (bounds.intersects(newPane.getBoundsInParent)) {
                            cardsOnTableController.showCardUseDropShadow
                          } else {
                            cardsOnTableController.hideCardUseDropShadow
                          }
                        }
                      }

                      val releaseHandler = new EventHandler[MouseEvent]() {
                        def handle(event: MouseEvent) {
                          // root.getChildren.remove(newPane)
                          hideHands
                          root.removeEventHandler(MouseEvent.MOUSE_DRAGGED, dragHandler)
                          root.removeEventHandler(MouseEvent.MOUSE_RELEASED, this)

                          if (bounds.intersects(newPane.getBoundsInParent)) {
                            root.getChildren.remove(newPane)
                            //cardsOnTableController.dropMyCardOnTable(card)
                            //handler ! UsedCardInteraction()
                            cardsOnTableController.hideCardUseDropShadow
                            myHand.setMouseTransparent(true)

                            tableState match {
                              case TableState.Using =>
                                cardsOnTableController.setUseMode(
                                  players,
                                  Some(card),
                                  List(),
                                  card.cardTargetingType,
                                  possibleCardToUse
                                    .filter(f => CardPositionToCardHolder(f.onPosition, players) == card)
                                    .map(p => p.target.map(t => CardTargetToCardHolder(t, players)).getOrElse(None))
                                    .filter(f => f.isDefined).map(f => f.get),
                                  (card: CardHolder, target: Option[CardTarget], sourcePane: Option[jfxsl.Pane], targetNode: Option[Node], line: Option[Line], targetType: Option[TableTarget.Value]) => {
                                    myHand.setMouseTransparent(false)
                                    targetType match {
                                      case None =>
                                        if (card.price.neutral > 0) {
                                          showNeutralDistribution(card, (fire: Int, water: Int, earth: Int, air: Int) => {
                                            CardHolderToCardPosition(card, players).map(p => {
                                              recreateOwnTargetInteractionLines
                                              handler ! UsedCardInteraction(p, card.id, target, NeutralDistribution(fire, water, earth, air), {
                                                case possibleUsages: mutable.WrappedArray[List[NITSession.AttackerUsesCard]] =>
                                                  possibleCardToUse = possibleUsages.head
                                                case o =>
                                              },
                                              () => {
                                              }
                                              )
                                            })
                                          }, () => {
                                          })
                                        } else {
                                          CardHolderToCardPosition(card, players).map(p => {
                                            handler ! UsedCardInteraction(p, card.id, target, NeutralDistribution(0, 0, 0, 0), {
                                              case possibleUsages: mutable.WrappedArray[List[NITSession.AttackerUsesCard]] =>
                                                possibleCardToUse = possibleUsages.head
                                              case o =>
                                            },
                                            () => {
                                            }
                                            )
                                          })
                                        }

                                      case Some(t) =>

                                        if (card.price.neutral > 0) {
                                          showNeutralDistribution(card, (fire: Int, water: Int, earth: Int, air: Int) => {
                                            CardHolderToCardPosition(card, players).map(p => {
                                              handler ! UsedCardInteraction(p, card.id, target, NeutralDistribution(fire, water, earth, air), {
                                                case possibleUsages: mutable.WrappedArray[List[NITSession.AttackerUsesCard]] =>
                                                  possibleCardToUse = possibleUsages.head
                                                case o =>
                                              },
                                              () => {
                                              }
                                              )
                                            })
                                          }, () => {
                                          })
                                        } else {
                                          CardHolderToCardPosition(card, players).map(p => {
                                            handler ! UsedCardInteraction(p, card.id, target, NeutralDistribution(0, 0, 0, 0), {
                                              case possibleUsages: mutable.WrappedArray[List[NITSession.AttackerUsesCard]] =>
                                                possibleCardToUse = possibleUsages.head
                                              case o =>
                                            }
                                            ,
                                            () => {
                                            }
                                            )
                                          })
                                        }
                                    }
                                  },
                                  () => {
                                    myHand.setMouseTransparent(true)
                                  }
                                )

                              case TableState.CounterUsing =>
                                cardsOnTableController.setUseMode(
                                  players,
                                  Some(card),
                                  List(),
                                  card.cardTargetingType,
                                  possibleCardToCounterUse
                                    .filter(f => CardPositionToCardHolder(f.onPosition, players) == card)
                                    .map(p => p.target.map(t => CardTargetToCardHolder(t, players)).getOrElse(None))
                                    .filter(f => f.isDefined).map(f => f.get),
                                  (card: CardHolder, target: Option[CardTarget], sourcePane: Option[jfxsl.Pane], targetNode: Option[Node], line: Option[Line], targetType: Option[TableTarget.Value]) => {
                                    myHand.setMouseTransparent(false)
                                    targetType match {
                                      case None =>
                                        if (card.price.neutral > 0) {
                                          showNeutralDistribution(card, (fire: Int, water: Int, earth: Int, air: Int) => {
                                            CardHolderToCardPosition(card, players).map(p => {
                                              recreateOwnTargetInteractionLines
                                              handler ! CounterUsedCardInteraction(p, card.id, target, NeutralDistribution(fire, water, earth, air), {
                                                case possibleUsages: mutable.WrappedArray[List[NITSession.DefenderUsesCard]] =>
                                                  possibleCardToCounterUse = possibleUsages.head
                                                case o =>
                                              },
                                              () => {
                                              }
                                              )
                                            })
                                          }, () => {
                                          })
                                        } else {
                                          CardHolderToCardPosition(card, players).map(p => {
                                            handler ! UsedCardInteraction(p, card.id, target, NeutralDistribution(0, 0, 0, 0), {
                                              case possibleUsages: mutable.WrappedArray[List[NITSession.DefenderUsesCard]] =>
                                                possibleCardToCounterUse = possibleUsages.head
                                              case o =>
                                            },
                                            () => {
                                            }
                                            )
                                          })
                                        }

                                      case Some(t) =>
                                        if (card.price.neutral > 0) {
                                          showNeutralDistribution(card, (fire: Int, water: Int, earth: Int, air: Int) => {
                                            CardHolderToCardPosition(card, players).map(p => {
                                              handler ! UsedCardInteraction(p, card.id, target, NeutralDistribution(fire, water, earth, air), {
                                                case possibleUsages: mutable.WrappedArray[List[NITSession.DefenderUsesCard]] =>
                                                  possibleCardToCounterUse = possibleUsages.head
                                                case o =>
                                              },
                                              () => {
                                              }
                                              )
                                            })
                                          }, () => {
                                          })
                                        } else {
                                          CardHolderToCardPosition(card, players).map(p => {
                                            handler ! UsedCardInteraction(p, card.id, target, NeutralDistribution(0, 0, 0, 0), {
                                              case possibleUsages: mutable.WrappedArray[List[NITSession.DefenderUsesCard]] =>
                                                possibleCardToCounterUse = possibleUsages.head
                                              case o =>
                                            },
                                            () => {
                                            }
                                            )
                                          })
                                        }
                                    }
                                  },
                                  () => {
                                    myHand.setMouseTransparent(true)
                                  }
                                )

                              case TableState.Defending =>
                                //   val possibleUseCardsForDefense = possibleCardToDefense.filter(pcd => pcd.withPosition.isInstanceOf[InHandCardPosition])
                                cardsOnTableController.setUseMode(
                                  players,
                                  Some(card),
                                  List(),
                                  card.cardTargetingType,
                                  possibleCardToDefense
                                    .filter(f => CardPositionToCardHolder(f.withPosition, players) == card)
                                    .map(p => CardTargetToCardHolder(p.againts, players))
                                    .filter(f => f.isDefined)
                                    .map(f => f.get),
                                  (card: CardHolder, target: Option[CardTarget], sourcePane: Option[jfxsl.Pane], targetNode: Option[Node], line: Option[Line], targetType: Option[TableTarget.Value]) => {
                                    myHand.setMouseTransparent(false)
                                    targetType match {
                                      case None =>
                                        if (card.price.neutral > 0) {
                                          showNeutralDistribution(card, (fire: Int, water: Int, earth: Int, air: Int) => {
                                            CardHolderToCardPosition(card, players).map(p => {
                                              recreateOwnTargetInteractionLines
                                              handler ! CounterUsedCardInteraction(p, card.id, target, NeutralDistribution(fire, water, earth, air), {
                                                case possibleUsages: mutable.WrappedArray[List[NITSession.DefenderDefends]] =>
                                                  possibleCardToDefense = possibleUsages.head
                                                case o =>
                                              },
                                              () => {
                                              }
                                              )
                                            })
                                          }, () => {
                                          })
                                        } else {
                                          CardHolderToCardPosition(card, players).map(p => {
                                            handler ! UsedCardInteraction(p, card.id, target, NeutralDistribution(0, 0, 0, 0), {
                                              case possibleUsages: mutable.WrappedArray[List[NITSession.DefenderDefends]] =>
                                                possibleCardToDefense = possibleUsages.head
                                              case o =>
                                            },
                                            () => {
                                            }
                                            )
                                          })
                                        }

                                      case Some(t) =>
                                        if (card.price.neutral > 0) {
                                          showNeutralDistribution(card, (fire: Int, water: Int, earth: Int, air: Int) => {
                                            CardHolderToCardPosition(card, players).map(p => {
                                              handler ! UsedCardInteraction(p, card.id, target, NeutralDistribution(fire, water, earth, air), {
                                                case possibleUsages: mutable.WrappedArray[List[NITSession.DefenderDefends]] =>
                                                  possibleCardToDefense = possibleUsages.head
                                                case o =>
                                              },
                                              () => {
                                              }
                                              )
                                            })
                                          }, () => {
                                          })
                                        } else {
                                          CardHolderToCardPosition(card, players).map(p => {
                                            handler ! UsedCardInteraction(p, card.id, target, NeutralDistribution(0, 0, 0, 0), {
                                              case possibleUsages: mutable.WrappedArray[List[NITSession.DefenderDefends]] =>
                                                possibleCardToDefense = possibleUsages.head
                                              case o =>
                                            },
                                            () => {
                                            }
                                            )
                                          })
                                        }
                                    }
                                  },
                                  () => {
                                    myHand.setMouseTransparent(true)
                                  }
                                )

                              case t =>
                            }


                          } else {
                            val t = new TranslateTransition(Duration.millis(500))
                            t.setByX(myHand.getLayoutX + 53 - newPane.getTranslateX)
                            t.setByY(myHand.getLayoutY + 63 - newPane.getTranslateY)
                            t.setNode(newPane)
                            t.setOnFinished(new EventHandler[ActionEvent] {
                              override def handle(p1: ActionEvent): Unit = {
                                root.getChildren.remove(newPane)
                              }
                            })
                            t.play
                          }

                          //  t.play()
                        }
                      }

                      root.addEventHandler(MouseEvent.MOUSE_DRAGGED, dragHandler)
                      root.addEventHandler(MouseEvent.MOUSE_RELEASED, releaseHandler)

                    }
                  }
                })


              })

            }
          })

          cardMoveParallelTransition.play()
        }
      })
    }
  }

  def hideHands = {
    if (myHandIsOpened && !doingHandAnimation) {
      Platform.runLater(new Runnable {
        def run(): Unit = {

          myHandDismissPane.setMouseTransparent(true)
          focusedCard = None

          println("doing close animation")
          doingHandAnimation = true
          myHand.getChildren.map(pane => {
            pane.setOnMouseEntered(null)
            pane.setOnMouseExited(null)
          })

          val p = new ParallelTransition()
          movedBy.map(m => {
            val tt = new animation.TranslateTransition(Duration.millis(200))
            tt.setNode(m._1)
            tt.setByX(m._2.point.x * -1)
            tt.setByY(m._2.point.y * -1)

            val r = new RotateTransition(Duration.millis(100))
            r.setToAngle(0)
            r.setNode(m._1)
            p.getChildren.add(tt)
            p.getChildren.add(r)
          })

          p.setOnFinished(new EventHandler[ActionEvent] {
            override def handle(event: ActionEvent): Unit = {
              doingHandAnimation = false
              myHandIsOpened = false
              movedBy = List()

            }
          })

          p.play()
        }
      })
    }
  }

  def addElements(msg: ElementsGenerated) = {
    players.find(p => p.player == msg.getPlayer).map(found => {
      found.relationship match {
        case PlayerRelationship.Enemy1 =>
          enemy1FireLabel setText (enemy1FireLabel.getText.toInt + msg.getCharm.getFire).toString
          enemy1WaterLabel setText (enemy1WaterLabel.getText.toInt + msg.getCharm.getWater).toString
          enemy1EarthLabel setText (enemy1EarthLabel.getText.toInt + msg.getCharm.getEarth).toString
          enemy1AirLabel setText (enemy1AirLabel.getText.toInt + msg.getCharm.getAir).toString

        case PlayerRelationship.Enemy2 =>
          enemy2FireLabel setText (enemy2FireLabel.getText.toInt + msg.getCharm.getFire).toString
          enemy2WaterLabel setText (enemy2WaterLabel.getText.toInt + msg.getCharm.getWater).toString
          enemy2EarthLabel setText (enemy2EarthLabel.getText.toInt + msg.getCharm.getEarth).toString
          enemy2AirLabel setText (enemy2AirLabel.getText.toInt + msg.getCharm.getAir).toString

        case PlayerRelationship.Ally =>
          allyFireLabel setText (allyFireLabel.getText.toInt + msg.getCharm.getFire).toString
          allyWaterLabel setText (allyWaterLabel.getText.toInt + msg.getCharm.getWater).toString
          allyEarthLabel setText (allyEarthLabel.getText.toInt + msg.getCharm.getEarth).toString
          allyAirLabel setText (allyAirLabel.getText.toInt + msg.getCharm.getAir).toString

        case PlayerRelationship.Me =>
          myFireLabel setText (myFireLabel.getText.toInt + msg.getCharm.getFire).toString
          myWaterLabel setText (myWaterLabel.getText.toInt + msg.getCharm.getWater).toString
          myEarthLabel setText (myEarthLabel.getText.toInt + msg.getCharm.getEarth).toString
          myAirLabel setText (myAirLabel.getText.toInt + msg.getCharm.getAir).toString
      }
    })
  }

  def addCards(msg: PlayerDrawCards, cards: List[CardHolder]) = {
    msg.getCardsList.asScala.map(cMSG => {
      players.find(p => p.player == cMSG.getPosition.getPlayer).map(found => {
        found.relationship match {
          case PlayerRelationship.Enemy1 =>
            enemy1HandSizeLabel setText (enemy1HandSizeLabel.getText.toInt + 1).toString
            enemy1DeckSizeLabel setText (enemy1DeckSizeLabel.getText.toInt - 1).toString

          case PlayerRelationship.Enemy2 =>
            enemy2HandSizeLabel setText (enemy2HandSizeLabel.getText.toInt + 1).toString
            enemy2DeckSizeLabel setText (enemy2DeckSizeLabel.getText.toInt - 1).toString

          case PlayerRelationship.Ally =>
            allyHandSizeLabel setText (allyHandSizeLabel.getText.toInt + 1).toString
            allyDeckSizeLabel setText (allyDeckSizeLabel.getText.toInt - 1).toString

          case PlayerRelationship.Me =>
            myHandSizeLabel setText (myHandSizeLabel.getText.toInt + 1).toString
            myDeckSizeLabel setText (myDeckSizeLabel.getText.toInt - 1).toString
            cardsInHand = cards.filter(c => c.id == cMSG.getId) ::: cardsInHand
            recreateHands
        }
      })
    })
  }

  def showNeutralDistribution(
                               card: CardHolder,
                               okAction: (Int, Int, Int, Int) => Unit, //fire, water, earth, air
                               cancelAction: () => Unit
                               ): Unit = {
    players.find(p => p.relationship == PlayerRelationship.Me).map(me => {
      neutralDistributionDialog = Some(
        new NeutralDistributionDialog(
          neutralDistributionTotalNeededElementsLabel,
          neutralDistributionTotalFireElementsLabel,
          neutralDistributionTotalWaterElementsLabel,
          neutralDistributionTotalEarthElementsLabel,
          neutralDistributionTotalAirElementsLabel,
          neutralDistributionActualFireElementsLabel,
          neutralDistributionActualWaterElementsLabel,
          neutralDistributioActualEarthElementsLabel,
          neutralDistributionActualAirElementsLabel,
          neutralDistributionTotalSetElementsLabel,
          neutralDistributionPane,
          neutralDistributionAlphaPane,
          neutralDistributionCardPane,
          card.price.neutral.toInt,
          me.elements,
          (fire: Int, water: Int, earth: Int, air: Int) => {

          },
          () => {

          }
        )
      )

      neutralDistributionDialog.map(d => {
        neutralDistributionOnOkClickedDelegate = d.okInterAction
        neutralDistributionOnCancelClickedDelegate = d.cancelInterAction
        neutralDistributionOnFirePlusClickedDelegate = d.firePlusInterAction
        neutralDistributionOnFireMinusClickedDelegate = d.fireMinusInterAction
        neutralDistributionOnWaterPlusClickedDelegate = d.waterPlusInterAction
        neutralDistributionOnWaterMinusClickedDelegate = d.waterMinusInterAction
        neutralDistributionOnEarthPlusClickedDelegate = d.earthPlusInterAction
        neutralDistributionOnEarthMinusClickedDelegate = d.earthMinusInterAction
        neutralDistributionOnAirPlusClickedDelegate = d.airPlusInterAction
        neutralDistributionOnAirMinusClickedDelegate = d.airMinusInterAction
      })
    })
  }


  def goToStartRolling(rollPosition: NITSession.PlayerRollPosition) = {
    tableState = TableState.StartRolling
    resetFortuneRoll
    fortuneRoll(rollPosition)
    fortuneRollPane.setLayoutY(fortuneRollPane.getHeight)
    possibleCardToUse = List()
    possibleCardToCounterUse = List()
    possibleCardToAttack = List()
    possibleCardToDefense = List()
  }

  def goToFortuneRolling(rollPosition: NITSession.PlayerRollPosition) = {
    tableState = TableState.FortuneRolling
    resetFortuneRoll
    fortuneRoll(rollPosition)
    fortuneRollLabel.setText("FORTUNE ROLL")
    val t = new TranslateTransition(Duration.millis(500))
    t.setNode(fortuneRollPane)
    t.setByY(fortuneRollPane.getHeight)
    t.play()
    possibleCardToUse = List()
    possibleCardToCounterUse = List()
    possibleCardToAttack = List()
    possibleCardToDefense = List()
  }

  def goToAttacking: Unit = {
    tableState = TableState.Attacking
  }

  def hide: Unit = {
    root.getScene().getWindow.asInstanceOf[Stage].hide()
  }

  def close = {
    root.getScene().getWindow.asInstanceOf[Stage].close()
  }


  def paintTableChanges(change: NITSession.TableStateChange) = {

  }

  def recreatePossibleUses(usages: List[NITSession.AttackerUsesCard]): Unit = {
    possibleCardToUse = usages
    recreateHands
  }

  def recreatePossibleCounterUses(counterUsages: List[NITSession.DefenderUsesCard]): Unit = {
    possibleCardToCounterUse = counterUsages
    recreateHands
  }

  def recreatePossibleAttacks(attacks: List[NITSession.AttackerAttacks]): Unit = {
    possibleCardToAttack = attacks
    recreateHands
  }

  def recreatePossibleDefends(defends: List[NITSession.DefenderDefends]): Unit = {
    possibleCardToDefense = defends
    recreateHands
  }

  def addMessage(message: ChatMessage): Unit = {
    this.chatController.addMessage(message)
  }

  def showMatchEnds(win: Boolean): Unit = {
    endGameController = Some(new GameEndController(
      Color.web(allyColor),
      Color.web(enemyColor),
      matchEndsResultScreen,
      matchEndsState,
      matchEndsLoader,
      matchEndsEssencesGainValue,
      matchEndsMmrGainValue,
      matchEndsOkButton,
      () => {
        handler ! CloseFormInteraction
      },
      win
    ))
  }

  def showGains(mmr: Int, essences: Int): Unit = {
    endGameController
      .map(c => c.setResults(essences, mmr))
  }

  private def pendingRequest_? = {
    sentRequest.isEmpty
  }
}
