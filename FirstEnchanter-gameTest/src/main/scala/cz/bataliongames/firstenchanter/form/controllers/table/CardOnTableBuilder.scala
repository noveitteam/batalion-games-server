package cz.bataliongames.firstenchanter.form.controllers.table

import javafx.event.EventHandler
import javafx.scene.control.Label
import javafx.scene.image.ImageView
import javafx.scene.input.MouseEvent
import javafx.scene.layout.Pane

import cz.bataliongames.firstenchanter.form.controllers.table.duplicator
import cz.bataliongames.firstenchanter.form.controllers.table.duplicator.{PaneDuplicator, ImageViewDuplicator, RectangleDuplicator, LabelDuplicator}
import cz.noveit.games.cardgame.engine.card.CardHolder

import javafx.scene.shape.Rectangle

/**
 * Created by arnostkuchar on 03.10.14.
 */
object CardOnTableBuilder {
  def apply(cardHolder: CardHolder, onClickDetailsHandler: (CardHolder) => Unit, cardOnTableTemplate: Pane) = {

    /*
    * cardOnTableCardName
    *
    * cardOnTableCardAttackRectangle
    * cardOnTableCardAttackIcon
    * cardOnTableCardAttackValue
    *
    * cardOnTableCardDefenseValue
    * cardOnTableCardDefenseRectangle
    * cardOnTableCardDefenseIcon
    *
    * cardOnTableCardDetailsContainer
    * cardOnTableCardDetailsLabel
    * */

    val headerLabel = cardOnTableTemplate.lookup("#cardOnTableCardName").asInstanceOf[Label]

    val attackRectangle = cardOnTableTemplate.lookup("#cardOnTableCardAttackRectangle").asInstanceOf[Rectangle]
    val attackValueLabel = cardOnTableTemplate.lookup("#cardOnTableCardAttackValue").asInstanceOf[Label]
    val attackIcon = cardOnTableTemplate.lookup("#cardOnTableCardAttackIcon").asInstanceOf[ImageView]

    val defenseValueLabel = cardOnTableTemplate.lookup("#cardOnTableCardDefenseValue").asInstanceOf[Label]
    val defenseRectangle = cardOnTableTemplate.lookup("#cardOnTableCardDefenseRectangle").asInstanceOf[Rectangle]
    val defenseIcon = cardOnTableTemplate.lookup("#cardOnTableCardDefenseIcon").asInstanceOf[ImageView]

    //val detailsContainer = cardOnTableTemplate.lookup("#cardOnTableCardDetailsContainer").asInstanceOf[Pane]
    val detailsLabel = cardOnTableTemplate.lookup("#cardOnTableCardDetailsLabel").asInstanceOf[Label]

    val newPane = new Pane
    val detailsLabelCopy  = LabelDuplicator(detailsLabel)
   // detailsContainerCopy.getChildren.add(LabelDuplicator(detailsLabel))

    newPane.getChildren.addAll(
      LabelDuplicator(headerLabel, cardHolder.name),
      RectangleDuplicator(attackRectangle),
      LabelDuplicator(attackValueLabel, cardHolder.attack.toString),
      ImageViewDuplicator(attackIcon),
      RectangleDuplicator(defenseRectangle),
      LabelDuplicator(defenseValueLabel, cardHolder.defense.toString),
      ImageViewDuplicator(defenseIcon),
      detailsLabelCopy
    )

    detailsLabelCopy.setOnMouseClicked(new EventHandler[MouseEvent] {
      override def handle(p1: MouseEvent): Unit = {
        onClickDetailsHandler(cardHolder)
      }
    })

    newPane.setPrefWidth(cardOnTableTemplate.getPrefWidth)
    newPane.setPrefHeight(cardOnTableTemplate.getPrefHeight)
    newPane.setStyle(cardOnTableTemplate.getStyle)
    newPane
  }
}
