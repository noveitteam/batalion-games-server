package cz.bataliongames.firstenchanter.form.controllers.table

import javafx.animation.{KeyFrame, Timeline}
import javafx.event.{ActionEvent, EventHandler}
import javafx.scene.paint.Color
import javafx.scene.{Cursor, Scene}
import javafx.scene.control.{Label, ScrollPane}
import javafx.scene.image.ImageView
import javafx.scene.input.MouseEvent
import javafx.scene.layout.{VBox, HBox, Pane}
import javafx.util.Duration

import cz.noveit.games.cardgame.engine.card.CardHolder
import scala.collection.JavaConverters._

/**
 * Created by arnostkuchar on 27.10.14.
 */
class CardsOnGraveyardController(
                                  val scene: Scene,
                                  val cardOnTableTemplate: Pane,
                                  val detailsCardTemplate: Pane,
                                  val detailsButtonTemplate: Label,
                                  val detailsPane: Pane,
                                  val graveyardOwnCardsScrollPane: ScrollPane,
                                  val graveyardOwnCardsHBoxPane: HBox,
                                  val graveyardOwnCardsTableLeftArrowImage: ImageView,
                                  val graveyardOwnCardsTableRightArrowImage: ImageView,
                                  val graveyardEnemyCardsScrollPane: ScrollPane,
                                  val graveyardEnemyCardsHBoxPane: HBox,
                                  val graveyardEnemyCardsTableLeftArrowImage: ImageView,
                                  val graveyardEnemyCardsTableRightArrowImage: ImageView
                                  ) {

  graveyardEnemyCardsTableLeftArrowImage.addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler[MouseEvent] {
    def handle(e: MouseEvent): Unit = {
      val timeline = new Timeline()
      timeline.setCycleCount(100)
      //timeline.setAutoReverse(false)
      timeline.getKeyFrames().addAll(
        new KeyFrame(
          Duration.seconds(0),
          new EventHandler[ActionEvent]() {
            @Override def handle(actionEvent: ActionEvent): Unit = {
              graveyardEnemyCardsScrollPane.setHvalue(graveyardEnemyCardsScrollPane.getHvalue - 0.01)
            }
          }
        ),
        new KeyFrame(Duration.millis(10))
      )
      timeline.play();

      scene.addEventHandler(MouseEvent.MOUSE_RELEASED, new EventHandler[MouseEvent] {
        def handle(se: MouseEvent): Unit = {
          scene.removeEventHandler(MouseEvent.MOUSE_RELEASED, this)
          timeline.stop()
        }
      })
    }
  })

  graveyardEnemyCardsTableRightArrowImage.addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler[MouseEvent] {
    def handle(e: MouseEvent): Unit = {
      val timeline = new Timeline()
      timeline.setCycleCount(100)
      //timeline.setAutoReverse(false)
      timeline.getKeyFrames().addAll(
        new KeyFrame(
          Duration.seconds(0),
          new EventHandler[ActionEvent]() {
            @Override def handle(actionEvent: ActionEvent): Unit = {
              graveyardEnemyCardsScrollPane.setHvalue(graveyardEnemyCardsScrollPane.getHvalue +  0.01)
            }
          }
        ),
        new KeyFrame(Duration.millis(10))
      )
      timeline.play();
      scene.addEventHandler(MouseEvent.MOUSE_RELEASED, new EventHandler[MouseEvent] {
        def handle(se: MouseEvent): Unit = {
          scene.removeEventHandler(MouseEvent.MOUSE_RELEASED, this)
          timeline.stop()
        }
      })
    }
  })

  graveyardOwnCardsTableLeftArrowImage.addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler[MouseEvent] {
    def handle(e: MouseEvent): Unit = {
      val timeline = new Timeline()
      timeline.setCycleCount(100)
      //timeline.setAutoReverse(false)
      timeline.getKeyFrames().addAll(
        new KeyFrame(
          Duration.seconds(0),
          new EventHandler[ActionEvent]() {
            @Override def handle(actionEvent: ActionEvent): Unit = {
              graveyardOwnCardsScrollPane.setHvalue(graveyardOwnCardsScrollPane.getHvalue -  0.01)
            }
          }
        ),
        new KeyFrame(Duration.millis(10))
      )

      timeline.play()
      scene.addEventHandler(MouseEvent.MOUSE_RELEASED, new EventHandler[MouseEvent] {
        def handle(se: MouseEvent): Unit = {
          scene.removeEventHandler(MouseEvent.MOUSE_RELEASED, this)
          timeline.stop()
        }
      })
    }
  })

  graveyardOwnCardsTableRightArrowImage.addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler[MouseEvent] {
    def handle(e: MouseEvent): Unit = {
      scene.addEventHandler(MouseEvent.MOUSE_RELEASED, new EventHandler[MouseEvent] {
        val timeline = new Timeline()
        timeline.setCycleCount(100)
        //timeline.setAutoReverse(false)
        timeline.getKeyFrames().addAll(
          new KeyFrame(
            Duration.seconds(0),
            new EventHandler[ActionEvent]() {
              @Override def handle(actionEvent: ActionEvent): Unit = {
                graveyardOwnCardsScrollPane.setHvalue(graveyardOwnCardsScrollPane.getHvalue +  0.01)
              }
            }
          ),
          new KeyFrame(Duration.millis(10))
        )

  timeline.play()
        def handle(se: MouseEvent): Unit = {
          scene.removeEventHandler(MouseEvent.MOUSE_RELEASED, this)
          timeline.stop()
        }
      })
    }
  })

  def repaintGraveyard(ally: List[CardHolder], enemy: List[CardHolder]): Unit = {
    this.graveyardEnemyCardsHBoxPane.getChildren.clear()
    this.graveyardOwnCardsHBoxPane.getChildren.clear()

    ally.map(card => {
      val pane = CardOnTableBuilder(card, (card: CardHolder) => {
        detailsPane.getChildren.setAll(detailsPane.getChildren.asScala.head)
        val newDetailsPane = DetailedCardBuilder(card, detailsCardTemplate)
        val vBox = detailsCardTemplate.getChildren.asScala.toList.find(n => n.isInstanceOf[VBox])
        val boxCorrection: Double = vBox.map(vb => (vb.getLayoutBounds.getWidth / 2)).getOrElse(0.0)

        newDetailsPane.setLayoutX((detailsPane.getWidth / 2) - (detailsCardTemplate.getLayoutBounds.getWidth / 2) - boxCorrection)
        newDetailsPane.setLayoutY((detailsPane.getHeight / 2) - (detailsCardTemplate.getLayoutBounds.getHeight / 2))

        val button = new Label()
        button.setText(detailsButtonTemplate.getText)
        button.setFont(detailsButtonTemplate.getFont)
        button.setTextFill(Color.WHITE)
        button.setPrefHeight(detailsButtonTemplate.getPrefHeight)
        button.setPrefWidth(detailsButtonTemplate.getPrefWidth)
        // button.setStyle(fortuneOkButton.getStyle)
        button.setLayoutX(detailsButtonTemplate.getLayoutX)
        button.setLayoutY(detailsButtonTemplate.getLayoutY)
        button.setCursor(Cursor.HAND)

        button.setOnMouseClicked(new EventHandler[MouseEvent] {
          override def handle(p1: MouseEvent): Unit = {
            detailsPane.setVisible(false)
          }
        })

        detailsPane.getChildren.addAll(newDetailsPane, button)
        detailsPane.setVisible(true)
      }, cardOnTableTemplate)

      graveyardOwnCardsHBoxPane.getChildren.add(pane)
    })

    enemy.map(card => {
      val pane = CardOnTableBuilder(card, (card: CardHolder) => {
        detailsPane.getChildren.setAll(detailsPane.getChildren.asScala.head)
        val newDetailsPane = DetailedCardBuilder(card, detailsCardTemplate)
        val vBox = detailsCardTemplate.getChildren.asScala.toList.find(n => n.isInstanceOf[VBox])
        val boxCorrection: Double = vBox.map(vb => (vb.getLayoutBounds.getWidth / 2)).getOrElse(0.0)

        newDetailsPane.setLayoutX((detailsPane.getWidth / 2) - (detailsCardTemplate.getLayoutBounds.getWidth / 2) - boxCorrection)
        newDetailsPane.setLayoutY((detailsPane.getHeight / 2) - (detailsCardTemplate.getLayoutBounds.getHeight / 2))

        val button = new Label()
        button.setText(detailsButtonTemplate.getText)
        button.setFont(detailsButtonTemplate.getFont)
        button.setTextFill(Color.WHITE)
        button.setPrefHeight(detailsButtonTemplate.getPrefHeight)
        button.setPrefWidth(detailsButtonTemplate.getPrefWidth)
        // button.setStyle(fortuneOkButton.getStyle)
        button.setLayoutX(detailsButtonTemplate.getLayoutX)
        button.setLayoutY(detailsButtonTemplate.getLayoutY)
        button.setCursor(Cursor.HAND)

        button.setOnMouseClicked(new EventHandler[MouseEvent] {
          override def handle(p1: MouseEvent): Unit = {
            detailsPane.setVisible(false)
          }
        })

        detailsPane.getChildren.addAll(newDetailsPane, button)
        detailsPane.setVisible(true)
      }, cardOnTableTemplate)

      graveyardEnemyCardsHBoxPane.getChildren.add(pane)
    })
  }

}
