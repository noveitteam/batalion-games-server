package cz.bataliongames.firstenchanter.form.controllers.table

import java.awt.MouseInfo
import javafx.event.EventHandler
import javafx.scene.shape.Line
import javafx.scene.{Scene, Node, Cursor}
import javafx.scene.control.{ProgressBar, ScrollPane, Button, Label}
import javafx.scene.input.{ScrollEvent, MouseEvent}
import javafx.scene.layout.{VBox, Pane, HBox}
import javafx.scene.paint.Color

import scalafx.Includes._
import cz.bataliongames.firstenchanter.form.actors._
import cz.bataliongames.firstenchanter.form.controllers.{TableTarget, TargetingMode}
import cz.bataliongames.firstenchanter.form.controllers.table.duplicator.GetLocalDimensionsOfElementInCenteredHBox
import cz.noveit.games.cardgame.adapters.CardTargetingType
import cz.noveit.games.cardgame.engine.card.{PlayerTeamIsTarget, CardIsTarget, CardTarget, CardHolder}
import collection.JavaConverters._
import scalafx.scene.input.MouseButton

/**
 * Created by arnostkuchar on 16.10.14.
 */
class CardsOnTableController(
                              val scene: Scene,
                              val rootPane: Pane,
                              val detailsCardTemplate: Pane,
                              val detailsButtonTemplate: Label,
                              val cardOnTableTemplate: Pane,
                              val allySelectedCardOnTableTemplate: Pane,
                              val enemySelectedCardOnTableTemplate: Pane,
                              val cardOnTableShadowTemplate: Pane,
                              val ownTableContainer: HBox,
                              val enemyTableContainer: HBox,
                              val ownTableScrollPane: ScrollPane,
                              val enemyTableScrollPane: ScrollPane,
                              val cardDetailsPane: Pane,
                              val ownHpDropPane: Pane,
                              val ownHpProgressBard: ProgressBar,
                              val enemyHpDropPane: Pane,
                              val enemyHpProgressBar: ProgressBar,
                              val ownCardTableHelperPane: Pane,
                              val enemyCardTableHelperPane: Pane,
                              val attackCursor: Cursor,
                              val defendCursor: Cursor
                              ) {

  var myCards: List[Pair[Pane, CardHolder]] = List()
  var allyCards: List[Pair[Pane, CardHolder]] = List()
  var enemyCards: List[Pair[Pane, CardHolder]] = List()
  private var shadowExists: Option[Pane] = None
  private var actualModeDisablers: List[() => Unit] = List()

  def paintOwnCards(mine: List[CardHolder], ally: List[CardHolder]): List[Pair[Pane, CardHolder]] = {
    ownTableContainer.getChildren.clear()

    myCards = mine.map(card => {
      val pane = CardOnTableBuilder(card, (card: CardHolder) => {
        cardDetailsPane.getChildren.setAll(cardDetailsPane.getChildren.asScala.head)
        val detailsPane = DetailedCardBuilder(card, detailsCardTemplate)
        val vBox = detailsCardTemplate.getChildren.asScala.toList.find(n => n.isInstanceOf[VBox])
        val boxCorrection: Double = vBox.map(vb => (vb.getLayoutBounds.getWidth / 2)).getOrElse(0.0)

        detailsPane.setLayoutX((cardDetailsPane.getWidth / 2) - (detailsCardTemplate.getLayoutBounds.getWidth / 2) - boxCorrection)
        detailsPane.setLayoutY((cardDetailsPane.getHeight / 2) - (detailsCardTemplate.getLayoutBounds.getHeight / 2))

        val button = new Label()
        button.setText(detailsButtonTemplate.getText)
        button.setFont(detailsButtonTemplate.getFont)
        button.setTextFill(Color.WHITE)
        button.setPrefHeight(detailsButtonTemplate.getPrefHeight)
        button.setPrefWidth(detailsButtonTemplate.getPrefWidth)
        // button.setStyle(fortuneOkButton.getStyle)
        button.setLayoutX(detailsButtonTemplate.getLayoutX)
        button.setLayoutY(detailsButtonTemplate.getLayoutY)
        button.setCursor(Cursor.HAND)

        button.setOnMouseClicked(new EventHandler[MouseEvent] {
          override def handle(p1: MouseEvent): Unit = {
            cardDetailsPane.setVisible(false)
          }
        })

        cardDetailsPane.getChildren.addAll(detailsPane, button)
        cardDetailsPane.setVisible(true)
      }, cardOnTableTemplate)

      ownTableContainer.getChildren.add(pane)
      pane -> card
    })

    allyCards = ally.map(card => {
      val pane = CardOnTableBuilder(card, (card: CardHolder) => {
        cardDetailsPane.getChildren.setAll(cardDetailsPane.getChildren.asScala.head)
        val detailsPane = DetailedCardBuilder(card, detailsCardTemplate)
        val vBox = detailsCardTemplate.getChildren.asScala.toList.find(n => n.isInstanceOf[VBox])
        val boxCorrection: Double = vBox.map(vb => (vb.getLayoutBounds.getWidth / 2)).getOrElse(0.0)

        detailsPane.setLayoutX((cardDetailsPane.getWidth / 2) - (detailsCardTemplate.getLayoutBounds.getWidth / 2) - boxCorrection)
        detailsPane.setLayoutY((cardDetailsPane.getHeight / 2) - (detailsCardTemplate.getLayoutBounds.getHeight / 2))

        val button = new Label()
        button.setText(detailsButtonTemplate.getText)
        button.setFont(detailsButtonTemplate.getFont)
        button.setTextFill(Color.WHITE)
        button.setPrefHeight(detailsButtonTemplate.getPrefHeight)
        button.setPrefWidth(detailsButtonTemplate.getPrefWidth)
        // button.setStyle(fortuneOkButton.getStyle)
        button.setLayoutX(detailsButtonTemplate.getLayoutX)
        button.setLayoutY(detailsButtonTemplate.getLayoutY)
        button.setCursor(Cursor.HAND)

        button.setOnMouseClicked(new EventHandler[MouseEvent] {
          override def handle(p1: MouseEvent): Unit = {
            cardDetailsPane.setVisible(false)
          }
        })

        cardDetailsPane.getChildren.addAll(detailsPane, button)
        cardDetailsPane.setVisible(true)
      }, cardOnTableTemplate)

      ownTableContainer.getChildren.add(pane)
      pane -> card
    })

    /*
      pane.setOnMouseEntered(new EventHandler[MouseEvent] {
        override def handle(p1: MouseEvent): Unit = {
          if (tableState == TableState.Attacking && targeting.isEmpty) {
            val pane = p1.getTarget.asInstanceOf[jfxsl.Pane]
            pane.setStyle(cardsOnTableAttackHighlightedOn)
          }
        }
      })

      pane.setOnMouseExited(new EventHandler[MouseEvent] {
        override def handle(p1: MouseEvent): Unit = {
          if (tableState == TableState.Attacking && targeting.isEmpty) {
            val pane = p1.getTarget.asInstanceOf[jfxsl.Pane]
            pane.setStyle(cardsOnTableAttackHighlightedOff)
          }
        }
      })

      pane.setOnMouseClicked(new EventHandler[MouseEvent] {
        override def handle(p1: MouseEvent): Unit = {
          if (tableState == TableState.Attacking) {
            targeting = List(TargetingMode.EnemyTeam)
            root.setCursor(attackCursor)
            attackDropPane.setVisible(true)
            attackDropPane.setOnMouseClicked(new EventHandler[MouseEvent] {
              override def handle(p1: MouseEvent): Unit = {
                root.setCursor(Cursor.DEFAULT)
                attackDropPane.setOnMouseClicked(null)
                attackDropPane.setVisible(false)
                pane.setStyle(cardsOnTableAttackHighlightedOff)
                targeting = List()

                val start = pane.localToScene(pane.getLayoutBounds.getMinX, pane.getLayoutBounds.getMinY)
                val attack = new ImageView(aEnemy)
                attack.setLayoutX(pane.getWidth / 2 - aEnemy.getWidth / 2)
                attack.setLayoutY(pane.getHeight / 2 - aEnemy.getHeight / 2)

                pane.getChildren.add(attack)
              }
            })

          }
        }
      })

  */
    myCards ::: allyCards
  }

  def paintEnemyCards(enemy: List[CardHolder]): List[Pair[Pane, CardHolder]] = {
    println("Painting " + enemy.size + "  enemy cards.")
    enemyTableContainer.getChildren.clear()
    enemyCards = enemy.map(c => {
      val pane = CardOnTableBuilder(c, (card: CardHolder) => {
        cardDetailsPane.getChildren.setAll(cardDetailsPane.getChildren.asScala.head)
        val detailsPane = DetailedCardBuilder(card, detailsCardTemplate)
        val vBox = detailsCardTemplate.getChildren.asScala.toList.find(n => n.isInstanceOf[VBox])
        val boxCorrection: Double = vBox.map(vb => (vb.getLayoutBounds.getWidth / 2)).getOrElse(0.0)

        detailsPane.setLayoutX((cardDetailsPane.getWidth / 2) - (detailsCardTemplate.getLayoutBounds.getWidth / 2) - boxCorrection)
        detailsPane.setLayoutY((cardDetailsPane.getHeight / 2) - (detailsCardTemplate.getLayoutBounds.getHeight / 2))

        val button = new Label()
        button.setText(detailsButtonTemplate.getText)
        button.setFont(detailsButtonTemplate.getFont)
        button.setTextFill(Color.WHITE)
        button.setPrefHeight(detailsButtonTemplate.getPrefHeight)
        button.setPrefWidth(detailsButtonTemplate.getPrefWidth)
        // button.setStyle(fortuneOkButton.getStyle)
        button.setLayoutX(detailsButtonTemplate.getLayoutX)
        button.setLayoutY(detailsButtonTemplate.getLayoutY)
        button.setCursor(Cursor.HAND)

        button.setOnMouseClicked(new EventHandler[MouseEvent] {
          override def handle(p1: MouseEvent): Unit = {
            cardDetailsPane.setVisible(false)
          }
        })

        cardDetailsPane.getChildren.addAll(detailsPane, button)
        cardDetailsPane.setVisible(true)
      }, cardOnTableTemplate)


      enemyTableContainer.getChildren.add(pane)
      pane -> c
    })

    enemyCards
  }

  def showCardUseDropShadow: Unit = {

    val sp = new Pane()
    sp.setPrefWidth(cardOnTableShadowTemplate.getPrefWidth)
    sp.setPrefHeight(cardOnTableShadowTemplate.getPrefHeight)
    sp.setStyle(cardOnTableShadowTemplate.getStyle)

    if (ownTableContainer.getChildren.size() > 0 && myCards.size > 0 && shadowExists.isEmpty) {
      ownTableContainer.getChildren.add(
        ownTableContainer.getChildren.indexOf(myCards.last._1),
        sp
      )
      shadowExists = Some(sp)
    } else if (shadowExists.isEmpty) {
      ownTableContainer.getChildren.add(
        sp
      )
      shadowExists = Some(sp)
    }
  }

  def hideCardUseDropShadow: Unit = {
    if (shadowExists.isDefined) {
      shadowExists.map(se => {
        ownTableContainer.getChildren.remove(se)
      })

      shadowExists = None
    }
  }

  def dropMyCardOnTable(card: CardHolder): Unit = {
    val cardOnTable = CardOnTableBuilder(card, (CardHolder) => {
      cardDetailsPane.getChildren.setAll(cardDetailsPane.getChildren.asScala.head)
      val detailsPane = DetailedCardBuilder(card, detailsCardTemplate)
      val vBox = detailsCardTemplate.getChildren.asScala.toList.find(n => n.isInstanceOf[VBox])
      val boxCorrection: Double = vBox.map(vb => (vb.getLayoutBounds.getWidth / 2)).getOrElse(0.0)

      detailsPane.setLayoutX((cardDetailsPane.getWidth / 2) - (detailsCardTemplate.getLayoutBounds.getWidth / 2) - boxCorrection)
      detailsPane.setLayoutY((cardDetailsPane.getHeight / 2) - (detailsCardTemplate.getLayoutBounds.getHeight / 2))

      val button = new Label()
      button.setText(detailsButtonTemplate.getText)
      button.setFont(detailsButtonTemplate.getFont)
      button.setTextFill(Color.WHITE)
      button.setPrefHeight(detailsButtonTemplate.getPrefHeight)
      button.setPrefWidth(detailsButtonTemplate.getPrefWidth)
      button.setLayoutX(detailsButtonTemplate.getLayoutX)
      button.setLayoutY(detailsButtonTemplate.getLayoutY)
      button.setCursor(Cursor.HAND)

      button.setOnMouseClicked(new EventHandler[MouseEvent] {
        override def handle(p1: MouseEvent): Unit = {
          cardDetailsPane.setVisible(false)
        }
      })

      cardDetailsPane.getChildren.addAll(detailsPane, button)
      cardDetailsPane.setVisible(true)
    }, cardOnTableTemplate)

    if (ownTableContainer.getChildren.size() > 0 && myCards.size > 0) {
      ownTableContainer.getChildren.add(
        ownTableContainer.getChildren.indexOf(myCards.last._1),
        cardOnTable
      )
    } else {
      ownTableContainer.getChildren.add(
        cardOnTable
      )
    }

    myCards = cardOnTable -> card :: myCards
  }

  def removeMyCardFromTable(cardHolder: CardHolder): Unit = {
    myCards.find(c => c._2 == cardHolder).map(found => {
      myCards = myCards.filter(p => p != found)
      ownTableContainer.getChildren.remove(found._1)
    })
  }

  def setUseMode(
                  players: List[PlayerSeesPlayer], defaultCard: Option[CardHolder], useCardFilter: List[CardHolder],
                  targetingMode: CardTargetingType.Value, targetCardFilter: List[CardHolder] = List(),
                  onConfirm: (CardHolder, Option[CardTarget], Option[Pane], Option[Node], Option[Line], Option[TableTarget.Value]) => Unit, onCancel: () => Unit
                  ): Unit = {
    defaultCard.map(c => {
      val cardOnTable = CardOnTableBuilder(c, (CardHolder) => {
        cardDetailsPane.getChildren.setAll(cardDetailsPane.getChildren.asScala.head)
        val detailsPane = DetailedCardBuilder(c, detailsCardTemplate)
        val vBox = detailsCardTemplate.getChildren.asScala.toList.find(n => n.isInstanceOf[VBox])
        val boxCorrection: Double = vBox.map(vb => (vb.getLayoutBounds.getWidth / 2)).getOrElse(0.0)

        detailsPane.setLayoutX((cardDetailsPane.getWidth / 2) - (detailsCardTemplate.getLayoutBounds.getWidth / 2) - boxCorrection)
        detailsPane.setLayoutY((cardDetailsPane.getHeight / 2) - (detailsCardTemplate.getLayoutBounds.getHeight / 2))

        val button = new Label()
        button.setText(detailsButtonTemplate.getText)
        button.setFont(detailsButtonTemplate.getFont)
        button.setTextFill(Color.WHITE)
        button.setPrefHeight(detailsButtonTemplate.getPrefHeight)
        button.setPrefWidth(detailsButtonTemplate.getPrefWidth)
        button.setLayoutX(detailsButtonTemplate.getLayoutX)
        button.setLayoutY(detailsButtonTemplate.getLayoutY)
        button.setCursor(Cursor.HAND)

        button.setOnMouseClicked(new EventHandler[MouseEvent] {
          override def handle(p1: MouseEvent): Unit = {
            cardDetailsPane.setVisible(false)
          }
        })

        cardDetailsPane.getChildren.addAll(detailsPane, button)
        cardDetailsPane.setVisible(true)
      }, cardOnTableTemplate)

      if (ownTableContainer.getChildren.size() > 0 && myCards.size > 0) {
        ownTableContainer.getChildren.add(
          ownTableContainer.getChildren.indexOf(myCards.last._1),
          cardOnTable
        )
        ownTableContainer.requestLayout()
      } else {
        ownTableContainer.getChildren.add(
          cardOnTable
        )
        ownTableContainer.requestLayout()
      }

      targetingMode match {
        case CardTargetingType.None =>
          onConfirm(c, None, None, None, None, None)

        case t /* TargetingMode.EnemyCard */ =>

          var enemyCardPanes: List[Pair[Pane, CardHolder]] = List()
          var ownCardPanes: List[Pair[Pane, CardHolder]] = List()
          var allyPane: Option[Pane] = None
          var enemyPane: Option[Pane] = None

          t match {
            case CardTargetingType.AllyCards =>
              ownCardPanes = (myCards ::: allyCards).filter(f => targetCardFilter.contains(f._2)).map(p => p)
            case CardTargetingType.EnemyCards =>
              enemyCardPanes = enemyCards.filter(f => targetCardFilter.contains(f._2)).map(p => p)
            case CardTargetingType.AllCards =>
              ownCardPanes = (myCards ::: allyCards).filter(f => targetCardFilter.contains(f._2)).map(p => p)
              enemyCardPanes = enemyCards.filter(f => targetCardFilter.contains(f._2)).map(p => p)
            case CardTargetingType.AllAlly =>
              ownCardPanes = (myCards ::: allyCards).filter(f => targetCardFilter.contains(f._2)).map(p => p)
              allyPane = Some(ownHpDropPane)
            case CardTargetingType.AllEnemy =>
              enemyCardPanes = enemyCards.filter(f => targetCardFilter.contains(f._2)).map(p => p)
              enemyPane = Some(enemyHpDropPane)
            case CardTargetingType.All =>
              ownCardPanes = (myCards ::: allyCards).filter(f => targetCardFilter.contains(f._2)).map(p => p)
              enemyCardPanes = enemyCards.filter(f => targetCardFilter.contains(f._2)).map(p => p)

            case CardTargetingType.EnemyTeam =>
              enemyPane = Some(enemyHpDropPane)
            case CardTargetingType.MyTeam =>
              allyPane = Some(ownHpDropPane)
          }

          val line = new Line()
          val local = GetLocalDimensionsOfElementInCenteredHBox(cardOnTable, ownTableContainer)
          val globalCoordinates = cardOnTable.localToScene(local.getX, 0.0)

          line.setStartX(globalCoordinates.getX + (cardOnTableTemplate.getPrefWidth / 2))
          line.setStartY(globalCoordinates.getY + 5)
          line.setFill(Color.web("#D64960"))
          line.getStrokeDashArray().addAll(10d, 5d);
          line.setMouseTransparent(true)

          val enemyTableChange = this.enemyTableScrollPane.hvalueProperty.onChange({
            val loc = MouseInfo.getPointerInfo().getLocation()
            line.setEndX(loc.getX - scene.getWindow.getX)
            line.setEndY(loc.getY - scene.getWindow.getY)
            enemyCardTableHelperPane.setLayoutX(line.getEndX)
            enemyCardTableHelperPane.setLayoutY(line.getEndY)
          })

          val ownTableChange = this.ownTableScrollPane.hvalueProperty.onChange({
            val loc = MouseInfo.getPointerInfo().getLocation()
            line.setEndX(loc.getX - scene.getWindow.getX)
            line.setEndY(loc.getY - scene.getWindow.getY)
            enemyCardTableHelperPane.setLayoutX(line.getEndX)
            enemyCardTableHelperPane.setLayoutY(line.getEndY)
          })

          this.rootPane.getChildren.add(line)


          if (!enemyCards.isEmpty) {
            enemyCardTableHelperPane.setVisible(true)
            enemyCardTableHelperPane.getChildren.asScala.find(ch => ch.isInstanceOf[Label])
              .map(ch => ch.asInstanceOf[Label].setText("SELECT ENEMY CARD"))
          }

          if (!(myCards ::: allyCards).isEmpty) {
            ownCardTableHelperPane.setVisible(true)
            ownCardTableHelperPane.getChildren.asScala.find(ch => ch.isInstanceOf[Label])
              .map(ch => ch.asInstanceOf[Label].setText("SELECT OWN CARD"))
          }

          if (allyPane.isDefined) {
            ownCardTableHelperPane.setVisible(true)
          }

          if (enemyPane.isDefined) {
            ownCardTableHelperPane.setVisible(true)
          }

          val moveMouse = new EventHandler[MouseEvent] {
            def handle(e: MouseEvent): Unit = {
              line.setEndX(e.getSceneX)
              line.setEndY(e.getSceneY)
              enemyCardTableHelperPane.setLayoutX(line.getEndX)
              enemyCardTableHelperPane.setLayoutY(line.getEndY)
            }
          }

          scene.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler[MouseEvent] {
            override def handle(p1: MouseEvent): Unit = {
              if (p1.getButton == javafx.scene.input.MouseButton.SECONDARY) {
                println(p1.getButton)
                disableActualMode
                scene.removeEventFilter(MouseEvent.MOUSE_CLICKED, this)
              }
            }
          })

          scene.addEventHandler(MouseEvent.MOUSE_DRAGGED, moveMouse)
          scene.addEventHandler(MouseEvent.MOUSE_MOVED, moveMouse)

          actualModeDisablers = (
            () => {
              scene.removeEventHandler(MouseEvent.MOUSE_DRAGGED, moveMouse)
              scene.removeEventHandler(MouseEvent.MOUSE_MOVED, moveMouse)
              enemyCardTableHelperPane.setVisible(false)
              this.rootPane.getChildren.remove(line)
              enemyTableChange.cancel()
              ownTableChange.cancel()
            }
            ) :: actualModeDisablers

          enemyCardPanes.map(found => {
            val mouseEntered = new EventHandler[MouseEvent] {
              override def handle(p1: MouseEvent): Unit = {
                found._1.setStyle("-fx-background-color: white; -fx-border-radius: 5; -fx-border-width: 2; -fx-border-color: #D64960;")
              }
            }

            val mouseExited = new EventHandler[MouseEvent] {
              override def handle(p1: MouseEvent): Unit = {
                found._1.setStyle("-fx-background-color: white; -fx-border-radius: 5; -fx-border-width: 2; -fx-border-color: black;")
              }
            }

            val mouseClicked = new EventHandler[MouseEvent] {
              override def handle(p1: MouseEvent): Unit = {
                if (p1.getButton == javafx.scene.input.MouseButton.PRIMARY) {
                  disableActualMode
                  CardHolderToCardPosition(found._2, players).map(p => {

                    val line2 = new Line()
                    line2.setFill(Color.web("#D64960"))
                    line2.getStrokeDashArray().addAll(10d, 5d);

                    onConfirm(c, Some(CardIsTarget(p)), Some(cardOnTable), Some(found._1), Some(line2), Some(TableTarget.EnemyCard))
                  })
                }
              }
            }

            actualModeDisablers = (
              () => {
                found._1.removeEventHandler(MouseEvent.MOUSE_ENTERED, mouseEntered)
                found._1.removeEventHandler(MouseEvent.MOUSE_EXITED, mouseExited)
                found._1.removeEventHandler(MouseEvent.MOUSE_CLICKED, mouseClicked)
              }
              ) :: actualModeDisablers

            found._1.addEventHandler(MouseEvent.MOUSE_ENTERED, mouseEntered)
            found._1.addEventHandler(MouseEvent.MOUSE_EXITED, mouseExited)
            found._1.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseClicked)
          })

          ownCardPanes.map(found => {
            val mouseEntered = new EventHandler[MouseEvent] {
              override def handle(p1: MouseEvent): Unit = {
                found._1.setStyle("-fx-background-color: white; -fx-border-radius: 5; -fx-border-width: 2; -fx-border-color: #D64960;")
              }
            }

            val mouseExited = new EventHandler[MouseEvent] {
              override def handle(p1: MouseEvent): Unit = {
                found._1.setStyle("-fx-background-color: white; -fx-border-radius: 5; -fx-border-width: 2; -fx-border-color: black;")
              }
            }

            val mouseClicked = new EventHandler[MouseEvent] {
              override def handle(p1: MouseEvent): Unit = {
                if (p1.getButton == javafx.scene.input.MouseButton.PRIMARY) {
                  disableActualMode
                  CardHolderToCardPosition(found._2, players).map(p => {

                    val line2 = new Line()
                    line2.setFill(Color.web("#D64960"))
                    line2.getStrokeDashArray().addAll(10d, 5d);

                    onConfirm(c, Some(CardIsTarget(p)), Some(cardOnTable), Some(found._1), Some(line2), Some(TableTarget.MyCard))
                  })
                }
              }
            }

            actualModeDisablers = (
              () => {
                found._1.removeEventHandler(MouseEvent.MOUSE_ENTERED, mouseEntered)
                found._1.removeEventHandler(MouseEvent.MOUSE_EXITED, mouseExited)
                found._1.removeEventHandler(MouseEvent.MOUSE_CLICKED, mouseClicked)
              }
              ) :: actualModeDisablers

            found._1.addEventHandler(MouseEvent.MOUSE_ENTERED, mouseEntered)
            found._1.addEventHandler(MouseEvent.MOUSE_EXITED, mouseExited)
            found._1.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseClicked)
          })

          allyPane.map(pane => {
            val mouseClicked = new EventHandler[MouseEvent] {
              override def handle(p1: MouseEvent): Unit = {
                if (p1.getButton == javafx.scene.input.MouseButton.PRIMARY) {
                  val line2 = new Line()
                  line2.setFill(Color.web("#D64960"))
                  line2.getStrokeDashArray().addAll(10d, 5d);

                  disableActualMode
                  onConfirm(
                    c,
                    Some(PlayerTeamIsTarget(
                      players.filter(p => p.relationship == PlayerRelationship.Me || p.relationship == PlayerRelationship.Ally).map(p => p.player)
                    )),
                    Some(cardOnTable),
                    Some(ownHpProgressBard),
                    Some(line2),
                    Some(TableTarget.MyHealth)
                  )
                }
              }
            }
            pane.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseClicked)
            ownHpDropPane.setVisible(true)
            actualModeDisablers = (
              () => {
                pane.removeEventHandler(MouseEvent.MOUSE_CLICKED, mouseClicked)
                ownHpDropPane.setVisible(false)
              }
              ) :: actualModeDisablers
          })

          enemyPane.map(pane => {
            val mouseClicked = new EventHandler[MouseEvent] {
              override def handle(p1: MouseEvent): Unit = {
                if (p1.getButton == javafx.scene.input.MouseButton.PRIMARY) {
                  val line2 = new Line()
                  line2.setFill(Color.web("#D64960"))
                  line2.getStrokeDashArray().addAll(10d, 5d);

                  disableActualMode
                  onConfirm(c,
                    Some(PlayerTeamIsTarget(
                    players.filter(p => p.relationship == PlayerRelationship.Enemy1 || p.relationship == PlayerRelationship.Enemy2).map(p => p.player)
                  )), Some(cardOnTable), Some(enemyHpProgressBar), Some(line2), Some(TableTarget.EnemyHealth))
                }
              }
            }

            pane.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseClicked)
            enemyHpDropPane.setVisible(true)
            actualModeDisablers = (
              () => {
                pane.removeEventHandler(MouseEvent.MOUSE_CLICKED, mouseClicked)
                enemyHpDropPane.setVisible(false)
              }
              ) :: actualModeDisablers
          })
      }


    })

  }

  def setAttackMode(cards: List[CardHolder], onConfirm: (CardHolder) => Unit, onCancel: () => Unit) = {
    rootPane.setCursor(attackCursor)
    enemyTableScrollPane.setCursor(attackCursor)
    ownTableScrollPane.setCursor(attackCursor)
    (myCards ::: allyCards).filter(f => cards.contains(f._2)).map(f => {

      val hoverHandler = new EventHandler[MouseEvent] {
        def handle(e: MouseEvent) = {
          println("enter")
          f._1.setStyle(allySelectedCardOnTableTemplate.getStyle)
        }
      }

      val leftHandler = new EventHandler[MouseEvent] {
        def handle(e: MouseEvent) = {
          println("left")
          f._1.setStyle(cardOnTableTemplate.getStyle)
        }
      }

      val clickHandler = new EventHandler[MouseEvent] {
        def handle(e: MouseEvent): Unit = {
          if (e.getButton == javafx.scene.input.MouseButton.PRIMARY) {
            disableActualMode
            onConfirm(f._2)
          }
        }
      }

      f._1.addEventHandler(MouseEvent.MOUSE_CLICKED, clickHandler)
      f._1.addEventHandler(MouseEvent.MOUSE_ENTERED, hoverHandler)
      f._1.addEventHandler(MouseEvent.MOUSE_EXITED, leftHandler)


      actualModeDisablers = (() => {
        f._1.removeEventHandler(MouseEvent.MOUSE_CLICKED, clickHandler)
        f._1.removeEventHandler(MouseEvent.MOUSE_ENTERED, hoverHandler)
        f._1.removeEventHandler(MouseEvent.MOUSE_EXITED, leftHandler)
      }) :: actualModeDisablers

    })

    actualModeDisablers = (() => {
      rootPane.setCursor(Cursor.DEFAULT)
      enemyTableScrollPane.setCursor(Cursor.DEFAULT)
      ownTableScrollPane.setCursor(Cursor.DEFAULT)
    }) :: actualModeDisablers

  }


  def setDefenseModeWithCardInHand (
                      defaultCard: Option[CardHolder], defendCardFilter: List[CardHolder],
                      targetingMode: TargetingMode.Value, targetCardFilter: List[CardHolder] = List(),
                      onConfirm: () => Unit, onCancel: () => Unit
                      ) {

  }

  def setDefenseModeWithCardsOnTable(possibleDefends: List[Pair[CardHolder, CardHolder]], defending: (CardHolder, CardHolder) => Unit) = {

    myCards.filter(mc => possibleDefends.exists(f => f._1 == mc._2)).map(foundOwnPair => {
      val targets = possibleDefends.filter(f => f._1 == foundOwnPair._2).map(t => t._2)
      val targetPanes = enemyCards.filter(pair => targets.exists(t => pair._2 == t)).map(p => p._1)

      val cardClickedHandler = new EventHandler[MouseEvent]() {
        def handle(e: MouseEvent): Unit = {

          val line = new Line()
          val local = GetLocalDimensionsOfElementInCenteredHBox(foundOwnPair._1, ownTableContainer)
          val globalCoordinates = foundOwnPair._1.localToScene(0.0, 0.0)

          line.setStartX(globalCoordinates.getX + (cardOnTableTemplate.getPrefWidth / 2))
          line.setStartY(globalCoordinates.getY + 5)
          line.setStroke(Color.web("#D64960"))
          line.getStrokeDashArray().addAll(10d, 5d);
          line.setMouseTransparent(true)

          val enemyTableChange = enemyTableScrollPane.hvalueProperty.onChange({
            val loc = MouseInfo.getPointerInfo().getLocation()
            line.setEndX(loc.getX - scene.getWindow.getX)
            line.setEndY(loc.getY - scene.getWindow.getY)
            enemyCardTableHelperPane.setLayoutX(line.getEndX)
            enemyCardTableHelperPane.setLayoutY(line.getEndY)
          })

          val ownTableChange = ownTableScrollPane.hvalueProperty.onChange({
            val loc = MouseInfo.getPointerInfo().getLocation()
            line.setEndX(loc.getX - scene.getWindow.getX)
            line.setEndY(loc.getY - scene.getWindow.getY)
            enemyCardTableHelperPane.setLayoutX(line.getEndX)
            enemyCardTableHelperPane.setLayoutY(line.getEndY)
          })

          rootPane.getChildren.add(line)

          val moveMouse = new EventHandler[MouseEvent] {
            def handle(e: MouseEvent): Unit = {
              line.setEndX(e.getSceneX)
              line.setEndY(e.getSceneY)
              enemyCardTableHelperPane.setLayoutX(line.getEndX)
              enemyCardTableHelperPane.setLayoutY(line.getEndY)
            }
          }

          actualModeDisablers = (() => {
            scene.removeEventHandler(MouseEvent.MOUSE_CLICKED, this)
            scene.removeEventHandler(MouseEvent.MOUSE_MOVED, moveMouse)
            rootPane.getChildren.remove(line)
            enemyTableChange.cancel()
            ownTableChange.cancel()
          }) :: actualModeDisablers

          scene.addEventHandler(MouseEvent.MOUSE_MOVED, moveMouse)
          scene.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler[MouseEvent] {
            override def handle(p1: MouseEvent): Unit = {
              if (p1.getButton == javafx.scene.input.MouseButton.SECONDARY) {
                scene.removeEventHandler(MouseEvent.MOUSE_CLICKED, this)
                scene.removeEventHandler(MouseEvent.MOUSE_MOVED, moveMouse)
                rootPane.getChildren.remove(line)
                enemyTableChange.cancel()
                ownTableChange.cancel()
              }
            }
          })

          targets.map(t => {
            enemyCards.find(pair => pair._2 == t).map(tPane => {

              val enemyClickedHandler = new EventHandler[MouseEvent]() {
                def handle(e: MouseEvent): Unit = {
                  defending(foundOwnPair._2, t)
                  disableActualMode
                }
              }

              val enemyCardHoverHandler = new EventHandler[MouseEvent] {
                def handle(e: MouseEvent) = {
                  tPane._1.setStyle(enemySelectedCardOnTableTemplate.getStyle)
                }
              }

              val enemyCardLeftHandler = new EventHandler[MouseEvent] {
                def handle(e: MouseEvent) = {
                  tPane._1.setStyle(cardOnTableTemplate.getStyle)
                }
              }

              tPane._1.addEventHandler(MouseEvent.MOUSE_CLICKED, enemyClickedHandler)
              tPane._1.addEventHandler(MouseEvent.MOUSE_ENTERED, enemyCardHoverHandler)
              tPane._1.addEventHandler(MouseEvent.MOUSE_EXITED, enemyCardLeftHandler)

              actualModeDisablers = (() => {
                tPane._1.removeEventHandler(MouseEvent.MOUSE_CLICKED, enemyClickedHandler)
                tPane._1.removeEventHandler(MouseEvent.MOUSE_ENTERED, enemyCardHoverHandler)
                tPane._1.removeEventHandler(MouseEvent.MOUSE_EXITED, enemyCardLeftHandler)
              }) :: actualModeDisablers

              scene.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler[MouseEvent] {
                override def handle(p1: MouseEvent): Unit = {
                  if (p1.getButton == javafx.scene.input.MouseButton.SECONDARY) {
                    tPane._1.removeEventHandler(MouseEvent.MOUSE_CLICKED, enemyClickedHandler)
                    tPane._1.removeEventHandler(MouseEvent.MOUSE_ENTERED, enemyCardHoverHandler)
                    tPane._1.removeEventHandler(MouseEvent.MOUSE_EXITED, enemyCardLeftHandler)
                    scene.removeEventHandler(MouseEvent.MOUSE_CLICKED, this)
                  }
                }
              })

            })
          })
        }
      }

      val ownCardHoverHandler = new EventHandler[MouseEvent] {
        def handle(e: MouseEvent) = {
          foundOwnPair._1.setStyle(allySelectedCardOnTableTemplate.getStyle)
        }
      }

      val ownCardLeftHandler = new EventHandler[MouseEvent] {
        def handle(e: MouseEvent) = {
          foundOwnPair._1.setStyle(cardOnTableTemplate.getStyle)
        }
      }

      foundOwnPair._1.addEventHandler(MouseEvent.MOUSE_CLICKED, cardClickedHandler)
      foundOwnPair._1.addEventHandler(MouseEvent.MOUSE_ENTERED, ownCardHoverHandler)
      foundOwnPair._1.addEventHandler(MouseEvent.MOUSE_EXITED, ownCardLeftHandler)

      actualModeDisablers = (() => {
        foundOwnPair._1.removeEventHandler(MouseEvent.MOUSE_CLICKED, cardClickedHandler)
        foundOwnPair._1.removeEventHandler(MouseEvent.MOUSE_ENTERED, ownCardHoverHandler)
        foundOwnPair._1.removeEventHandler(MouseEvent.MOUSE_EXITED, ownCardLeftHandler)
      }) :: actualModeDisablers
    })
  }

  def disableActualMode: Unit = {
    if (!this.actualModeDisablers.isEmpty) {
      this.actualModeDisablers.map(d => d())
      this.actualModeDisablers = List()
    }
  }
}
