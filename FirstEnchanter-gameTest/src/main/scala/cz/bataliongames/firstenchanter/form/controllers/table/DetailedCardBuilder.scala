package cz.bataliongames.firstenchanter.form.controllers.table

import javafx.scene.control.{TextArea, Label}
import javafx.scene.image.ImageView
import javafx.scene.layout.{VBox, Pane}
import javafx.scene.paint.Color
import javafx.scene.shape.Rectangle

import cz.bataliongames.firstenchanter.form.controllers.table.duplicator
import cz.bataliongames.firstenchanter.form.controllers.table.duplicator._
import cz.noveit.games.cardgame.engine.card.CardHolder

/**
 * Created by arnostkuchar on 06.10.14.
 */
object DetailedCardBuilder {
  def apply(cardHolder: CardHolder, detailedCardTemplate: Pane) = {

    /*
    * detailedCardGameName
    *
    * detailedCardGameAttackRectangle
    * detailedCardGameAttackIcon
    * detailedCardGameAttackValue
    * detailedCardGameAttackLabel
    *
    * detailedCardGameDefenseRectangle
    * detailedCardGameDefenseIcon
    * detailedCardGameDefenseValue
    * detailedCardGameDefenseLabel
    *
    * detailedCardFireIcon
    * detailedCardFireLabel
    * detailedCardFireValue
    *
    * detailedCardWaterIcon
    * detailedCardWaterLabel
    * detailedCardWaterValue
    *
    * detailedCardEarthIcon
    * detailedCardEarthLabel
    * detailedCardEarthValue
    *
    * detailedCardAirIcon
    * detailedCardAirLabel
    * detailedCardAirValue
    *
    * detailedCardNeutralLabel
    * detailedCardNeutralValue
    *
    * detailedCardDescription
    *
    * */

    val detailedCardGameName = detailedCardTemplate.lookup("#detailedCardGameName").asInstanceOf[Label]

    val detailedCardGameAttackRectangle = detailedCardTemplate.lookup("#detailedCardGameAttackRectangle").asInstanceOf[Rectangle]
    val detailedCardGameAttackIcon = detailedCardTemplate.lookup("#detailedCardGameAttackIcon").asInstanceOf[ImageView]
    val detailedCardGameAttackValue = detailedCardTemplate.lookup("#detailedCardGameAttackValue").asInstanceOf[Label]
    val detailedCardGameAttackLabel = detailedCardTemplate.lookup("#detailedCardGameAttackLabel").asInstanceOf[Label]

    val detailedCardGameDefenseRectangle = detailedCardTemplate.lookup("#detailedCardGameDefenseRectangle").asInstanceOf[Rectangle]
    val detailedCardGameDefenseIcon = detailedCardTemplate.lookup("#detailedCardGameDefenseIcon").asInstanceOf[ImageView]
    val detailedCardGameDefenseValue = detailedCardTemplate.lookup("#detailedCardGameDefenseValue").asInstanceOf[Label]
    val detailedCardGameDefenseLabel = detailedCardTemplate.lookup("#detailedCardGameDefenseLabel").asInstanceOf[Label]

    val detailedCardFireIcon = detailedCardTemplate.lookup("#detailedCardFireIcon").asInstanceOf[ImageView]
    val detailedCardFireLabel = detailedCardTemplate.lookup("#detailedCardFireLabel").asInstanceOf[Label]
    val detailedCardFireValue = detailedCardTemplate.lookup("#detailedCardFireValue").asInstanceOf[Label]

    val detailedCardWaterIcon = detailedCardTemplate.lookup("#detailedCardWaterIcon").asInstanceOf[ImageView]
    val detailedCardWaterLabel = detailedCardTemplate.lookup("#detailedCardWaterLabel").asInstanceOf[Label]
    val detailedCardWaterValue = detailedCardTemplate.lookup("#detailedCardWaterValue").asInstanceOf[Label]

    val detailedCardEarthIcon = detailedCardTemplate.lookup("#detailedCardEarthIcon").asInstanceOf[ImageView]
    val detailedCardEarthLabel = detailedCardTemplate.lookup("#detailedCardEarthLabel").asInstanceOf[Label]
    val detailedCardEarthValue = detailedCardTemplate.lookup("#detailedCardEarthValue").asInstanceOf[Label]

    val detailedCardAirIcon = detailedCardTemplate.lookup("#detailedCardAirIcon").asInstanceOf[ImageView]
    val detailedCardAirLabel = detailedCardTemplate.lookup("#detailedCardAirLabel").asInstanceOf[Label]
    val detailedCardAirValue = detailedCardTemplate.lookup("#detailedCardAirValue").asInstanceOf[Label]

    val detailedCardNeutralLabel = detailedCardTemplate.lookup("#detailedCardNeutralLabel").asInstanceOf[Label]
    val detailedCardNeutralValue = detailedCardTemplate.lookup("#detailedCardNeutralValue").asInstanceOf[Label]

    val detailedCardDescription = detailedCardTemplate.lookup("#detailedCardDescription").asInstanceOf[TextArea]
    val detailedCardTags = detailedCardTemplate.lookup("#detailedCardGameTags").asInstanceOf[VBox]


    val newPane = new Pane
    newPane.setStyle(detailedCardTemplate.getStyle)
    newPane.setPrefHeight(detailedCardTemplate.getPrefHeight)
    newPane.setPrefWidth(detailedCardTemplate.getPrefWidth)

    val tags = VBoxDuplicator(detailedCardTags)

    cardHolder.tags.map(tag => {
      val label = new Label()
      label.setTextFill(Color.WHITE)
      label.setText(tag)
      tags.getChildren.add(label)
    })

    newPane.getChildren.addAll(
      LabelDuplicator(detailedCardGameName, cardHolder.name),
      RectangleDuplicator(detailedCardGameAttackRectangle),
      ImageViewDuplicator(detailedCardGameAttackIcon),
      LabelDuplicator(detailedCardGameAttackValue, cardHolder.defense.toString),
      LabelDuplicator(detailedCardGameAttackLabel),
      RectangleDuplicator(detailedCardGameDefenseRectangle),
      ImageViewDuplicator(detailedCardGameDefenseIcon),
      LabelDuplicator(detailedCardGameDefenseValue, cardHolder.defense.toString),
      LabelDuplicator(detailedCardGameDefenseLabel),
      ImageViewDuplicator(detailedCardGameDefenseIcon),
      ImageViewDuplicator(detailedCardFireIcon),
      LabelDuplicator(detailedCardFireValue, cardHolder.price.fire.toString),
      LabelDuplicator(detailedCardFireLabel),
      ImageViewDuplicator(detailedCardWaterIcon),
      LabelDuplicator(detailedCardWaterValue, cardHolder.price.water.toString),
      LabelDuplicator(detailedCardWaterLabel),
      ImageViewDuplicator(detailedCardEarthIcon),
      LabelDuplicator(detailedCardEarthValue, cardHolder.price.earth.toString),
      LabelDuplicator(detailedCardEarthLabel),
      ImageViewDuplicator(detailedCardAirIcon),
      LabelDuplicator(detailedCardAirValue, cardHolder.price.air.toString),
      LabelDuplicator(detailedCardAirLabel),
      LabelDuplicator(detailedCardNeutralValue, cardHolder.price.neutral.toString),
      LabelDuplicator(detailedCardNeutralLabel),
      TextAreaDuplicator(detailedCardDescription),
      tags
    )

    newPane
  }
}
