package cz.bataliongames.firstenchanter.form.controllers.table

import javafx.event.EventHandler
import javafx.scene.control.{Button, Label}
import javafx.scene.image.ImageView
import javafx.scene.input.MouseEvent
import javafx.scene.layout.Pane
import javafx.scene.paint.Paint
import scala.collection.JavaConverters._


/**
 * Created by arnostkuchar on 14.11.14.
 */
class GameEndController(
                         val allyColor: Paint,
                         val enemyColor: Paint,
                         val matchEndsResultScreen: Pane,
                         val matchEndsState: Label,
                         val matchEndsLoader: ImageView,
                         val matchEndsEssencesGainValue: Label,
                         val matchEndsMmrGainValue: Label,
                         val matchEndsOkButton: Label,
                         val okHandler: () => Unit,
                         win: Boolean
                         ) {


  matchEndsResultScreen.getChildren.asScala.map(ch => {
    if (ch != matchEndsState && ch != matchEndsLoader) {
      ch.setVisible(false)
    }
  })

  matchEndsResultScreen.setLayoutY(0)

  if (win) {
    matchEndsState.setText("YOU HAVE WON")
    //matchEndsState.setTextFill(allyColor)
  } else {
    matchEndsState.setText("YOU HAVE LOST")
  //  matchEndsState.setTextFill(enemyColor)
  }

  matchEndsOkButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler[MouseEvent] {
    def handle(e: MouseEvent) = {
      okHandler()
    }
  })

  def setResults(essencesGain: Int, mmrGain: Int): Unit = {
    matchEndsResultScreen.getChildren.asScala.map(ch => {
      ch.setVisible(true)
    })

    if (mmrGain < 0) {
      matchEndsMmrGainValue.setTextFill(enemyColor)
    }

    matchEndsEssencesGainValue.setText(essencesGain.toString)
    matchEndsMmrGainValue.setText(mmrGain.toString)

    matchEndsLoader.setVisible(false)
  }
}
