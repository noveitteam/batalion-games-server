package cz.bataliongames.firstenchanter.form.controllers.table

import javafx.scene.control.Label
import javafx.scene.layout.Pane

import cz.noveit.games.cardgame.engine.player.CharmOfElements

/**
 * Created by arnostkuchar on 03.10.14.
 */


class NeutralDistributionDialog(
                                 val needElementsNumberLabel: Label,
                                 val fireNumberLabel: Label,
                                 val waterNumberLabel: Label,
                                 val earthNumberLabel: Label,
                                 val airNumberLabel: Label,
                                 val actualFireNumberLabel: Label,
                                 val actualWaterNumberLabel: Label,
                                 val actualEarthNumberLabel: Label,
                                 val actualAirNumberLabel: Label,
                                 val totalNumberLabel: Label,
                                 val rootPane: Pane,
                                 val alphaPane: Pane,
                                 val cardPane: Pane,
                                 val need: Int,
                                 val limits: CharmOfElements,
                                 okAction: (Int, Int, Int, Int) => Unit, //fire, water, earth, air
                                 cancelAction: () => Unit
                                 ) {

  rootPane.setVisible(true)
  needElementsNumberLabel.setText(need.toString)
  actualAirNumberLabel.setText(limits.air.toString)
  actualEarthNumberLabel.setText(limits.earth.toString)
  actualFireNumberLabel.setText(limits.fire.toString)
  actualWaterNumberLabel.setText(limits.water.toString)

  def cancelInterAction() = {
    rootPane.setVisible(false)
    cancelAction()
  }

  def okInterAction() = {
    if (checkForOk) {
      rootPane.setVisible(false)
      okAction(
        fireNumberLabel.getText.toInt,
        waterNumberLabel.getText.toInt,
        earthNumberLabel.getText.toInt,
        airNumberLabel.getText.toInt
      )
    }
  }

  def firePlusInterAction() = {
    val actualValue = fireNumberLabel.getText.toInt
    val needElements = needElementsNumberLabel.getText.toInt

    if (actualValue < limits.fire && totalElements < needElements) {
      fireNumberLabel.setText((actualValue + 1).toString)
      actualFireNumberLabel.setText((actualFireNumberLabel.getText.toInt - 1).toString)
    }
  }

  def fireMinusInterAction() = {
    val actualValue = fireNumberLabel.getText.toInt

    if (actualValue > 0) {
      fireNumberLabel.setText((actualValue - 1).toString)
      actualFireNumberLabel.setText((actualFireNumberLabel.getText.toInt + 1).toString)
    }
  }

  def waterPlusInterAction() = {
    val actualValue = waterNumberLabel.getText.toInt
    val needElements = needElementsNumberLabel.getText.toInt

    if (actualValue < limits.water && totalElements < needElements) {
      waterNumberLabel.setText((actualValue + 1).toString)
      actualWaterNumberLabel.setText((actualWaterNumberLabel.getText.toInt - 1).toString)
    }
  }

  def waterMinusInterAction() = {
    val actualValue = waterNumberLabel.getText.toInt

    if (actualValue > 0) {
      waterNumberLabel.setText((actualValue - 1).toString)
      actualWaterNumberLabel.setText((actualWaterNumberLabel.getText.toInt + 1).toString)
    }
  }

  def earthPlusInterAction() = {
    val actualValue = earthNumberLabel.getText.toInt
    val needElements = needElementsNumberLabel.getText.toInt

    if (actualValue < limits.earth && totalElements < needElements) {
      earthNumberLabel.setText((actualValue + 1).toString)
      actualEarthNumberLabel.setText((actualEarthNumberLabel.getText.toInt - 1).toString)
    }
  }

  def earthMinusInterAction() = {
    val actualValue = earthNumberLabel.getText.toInt

    if (actualValue > 0) {
      earthNumberLabel.setText((actualValue - 1).toString)
      actualEarthNumberLabel.setText((actualEarthNumberLabel.getText.toInt + 1).toString)
    }
  }

  def airPlusInterAction() = {
    val actualValue = airNumberLabel.getText.toInt
    val needElements = needElementsNumberLabel.getText.toInt

    if (actualValue < limits.air && totalElements < needElements) {
      airNumberLabel.setText((actualValue + 1).toString)
      actualAirNumberLabel.setText((actualAirNumberLabel.getText.toInt - 1).toString)
    }
  }

  def airMinusInterAction() = {
    val actualValue = airNumberLabel.getText.toInt

    if (actualValue > 0) {
      airNumberLabel.setText((actualValue - 1).toString)
      actualAirNumberLabel.setText((actualAirNumberLabel.getText.toInt + 1).toString)
    }
  }

  private def totalElements = {
    fireNumberLabel.getText.toInt + airNumberLabel.getText.toInt + earthNumberLabel.getText.toInt + waterNumberLabel.getText.toInt
  }

  private def checkForOk = {
    totalElements == need
  }


}
