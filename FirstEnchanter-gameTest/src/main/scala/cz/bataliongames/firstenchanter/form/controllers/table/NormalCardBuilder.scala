package cz.bataliongames.firstenchanter.form.controllers.table

import javafx.event.EventHandler
import javafx.scene.control.{TextArea, Label}
import javafx.scene.image.ImageView
import javafx.scene.input.MouseEvent
import javafx.scene.layout.Pane
import javafx.scene.shape.Rectangle

import cz.bataliongames.firstenchanter.form.controllers.table.duplicator._
import cz.noveit.games.cardgame.engine.card.CardHolder


/**
 * Created by arnostkuchar on 06.10.14.
 */
object NormalCardBuilder {
  def apply(cardHolder: CardHolder, onClickDetailsHandler: (CardHolder) => Unit, normalCardTemplate: Pane) = {

    /*
    * normalCardName
    *
    * normalCardAttackRectangle
    * normalCardAttackIcon
    * normalCardAttackValue
    *
    * normalCardDefenseRectangle
    * normalCardDefenseValue
    * normalCardDefenseIcon
    *
    * normalCardFire
    * normalCardEarth
    * normalCardWater
    * normalCardAir
    * normalCardNeutral
    *
    * normalCardDescription
    *
    * normalCardDetailsContainer
    * normalCardDetailsLabel
    * */

    val normalCardName = normalCardTemplate.lookup("#normalCardName").asInstanceOf[Label]

    val normalCardAttackRectangle = normalCardTemplate.lookup("#normalCardAttackRectangle").asInstanceOf[Rectangle]
    val normalCardAttackValue = normalCardTemplate.lookup("#normalCardAttackValue").asInstanceOf[Label]
    val normalCardAttackIcon = normalCardTemplate.lookup("#normalCardAttackIcon").asInstanceOf[ImageView]

    val normalCardDefenseRectangle = normalCardTemplate.lookup("#normalCardDefenseRectangle").asInstanceOf[Rectangle]
    val normalCardDefenseValue = normalCardTemplate.lookup("#normalCardDefenseValue").asInstanceOf[Label]
    val normalCardDefenseIcon = normalCardTemplate.lookup("#normalCardDefenseIcon").asInstanceOf[ImageView]

    val normalCardFire = normalCardTemplate.lookup("#normalCardFire").asInstanceOf[Label]
    val normalCardEarth = normalCardTemplate.lookup("#normalCardEarth").asInstanceOf[Label]
    val normalCardWater = normalCardTemplate.lookup("#normalCardWater").asInstanceOf[Label]
    val normalCardAir = normalCardTemplate.lookup("#normalCardAir").asInstanceOf[Label]
    val normalCardNeutral = normalCardTemplate.lookup("#normalCardNeutral").asInstanceOf[Label]

    val normalCardDescription = normalCardTemplate.lookup("#normalCardDescription").asInstanceOf[TextArea]
    val normalCardDetails = normalCardTemplate.lookup("#normalCardDetails").asInstanceOf[Label]

    //val normalCardDetailsContainer = normalCardTemplate.lookup("#normalCardDetailsContainer").asInstanceOf[Pane]
 //


    val newPane = new Pane
  //  val normalCardDetailsContainerCopy = PaneDuplicator(normalCardDetailsContainer)
  //  normalCardDetailsContainerCopy.getChildren.add(LabelDuplicator(normalCardDetailsLabel))
val duplicated = LabelDuplicator(normalCardDetails)
    newPane.getChildren.addAll(
      LabelDuplicator(normalCardName, cardHolder.name),
      RectangleDuplicator(normalCardAttackRectangle),
      LabelDuplicator(normalCardAttackValue, cardHolder.attack.toString),
      ImageViewDuplicator(normalCardAttackIcon),
      RectangleDuplicator(normalCardDefenseRectangle),
      LabelDuplicator(normalCardDefenseValue, cardHolder.defense.toString),
      ImageViewDuplicator(normalCardDefenseIcon),
      LabelDuplicator(normalCardFire, cardHolder.price.fire.toString),
      LabelDuplicator(normalCardWater, cardHolder.price.water.toString),
      LabelDuplicator(normalCardEarth, cardHolder.price.earth.toString),
      LabelDuplicator(normalCardAir, cardHolder.price.air.toString),
      LabelDuplicator(normalCardNeutral, cardHolder.price.neutral.toString),
      TextAreaDuplicator(normalCardDescription, cardHolder.localizedDescription.getOrElse("")),
      duplicated
    )

    duplicated.setOnMouseClicked(new EventHandler[MouseEvent] {
      override def handle(p1: MouseEvent): Unit = {
        onClickDetailsHandler(cardHolder)
      }
    })

    newPane
  }
}
