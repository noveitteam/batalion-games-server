package cz.bataliongames.firstenchanter.form.controllers.table

import javafx.scene.layout.Pane
import javafx.scene.shape.{Polyline, Line}

import cz.bataliongames.firstenchanter.form.actors.{CardPositionToCardHolder, PlayerRelationship, PlayerSeesPlayer}
import cz.bataliongames.firstenchanter.form.controllers.{TableInteractionSource, TableTarget, TargetInteraction}
import cz.noveit.games.cardgame.engine.card.{CardIsTarget, PlayerTeamIsTarget, PlayerIsTarget, CardHolder}

import cz.noveit.games.cardgame.engine.game.{session => NITSession}
import cz.noveit.games.cardgame.{engine => CEngine}
import scalafx.scene.control.ProgressBar

/**
 * Created by arnostkuchar on 30.10.14.
 */
class PlayerInteractionsController(
                                    val allyLineTemplate: Line,
                                    val enemyLineTemplate: Line
                                    ) {
  /*
  def apply(
             usages: List[AttackerUsesCard],
             players: List[PlayerSeesPlayer],
             ownCardPairs: List[Pair[Pane, CardHolder]],
             enemyCardPairs: List[Pair[Pane, CardHolder]],
             ownHpProgressBar: ProgressBar,
             enemyHpProgressBar: ProgressBar): List[TargetInteraction] = {
    usages.map(us => {
      if(isPlayerAlly(players, us.onPosition.player)) {
        ownCardPairs.find(p => p._2 == us.card).map(panePair => {
          us.target map {
            case t: PlayerIsTarget =>
              if(isPlayerAlly(players, t.playerName)){
                val newLine = new Line()
                newLine.setFill(allyLineTemplate.getFill)
                newLine.getStrokeDashArray.addAll(allyLineTemplate.getStrokeDashArray)
                TargetInteraction(panePair._1, ownHpProgressBar, newLine, TableTarget.MyHealth)
              } else {
                val newLine = new Line()
                newLine.setFill(allyLineTemplate.getFill)
                newLine.getStrokeDashArray.addAll(allyLineTemplate.getStrokeDashArray)
                TargetInteraction(panePair._1, enemyHpProgressBar, newLine, TableTarget.EnemyHealth)
              }
            case t: PlayerTeamIsTarget =>
              if(isPlayerAlly(players, t.team.head)){
                val newLine = new Line()
                newLine.setFill(allyLineTemplate.getFill)
                newLine.getStrokeDashArray.addAll(allyLineTemplate.getStrokeDashArray)
                TargetInteraction(panePair._1, ownHpProgressBar, newLine, TableTarget.MyHealth)
              } else {
                val newLine = new Line()
                newLine.setFill(allyLineTemplate.getFill)
                newLine.getStrokeDashArray.addAll(allyLineTemplate.getStrokeDashArray)
                TargetInteraction(panePair._1, enemyHpProgressBar, newLine, TableTarget.EnemyHealth)
              }
            case t: CardIsTarget =>
              if(isPlayerAlly(players, t.position.player)){
                ownCardPairs.find(pair => pair._2 == CardPositionToCardHolder(t.position, players)).map(panePairTarget => {
                  val newLine = new Line()
                  newLine.setFill(allyLineTemplate.getFill)
                  newLine.getStrokeDashArray.addAll(allyLineTemplate.getStrokeDashArray)
                  TargetInteraction(panePair._1, panePairTarget._1, newLine, TableTarget.MyCard)
                }).get
              } else {
                ownCardPairs.find(pair => pair._2 == CardPositionToCardHolder(t.position, players)).map(panePairTarget => {
                  val newLine = new Line()
                  newLine.setFill(allyLineTemplate.getFill)
                  newLine.getStrokeDashArray.addAll(allyLineTemplate.getStrokeDashArray)
                  TargetInteraction(panePair._1, panePairTarget._1, newLine, TableTarget.EnemyCard)
                }).get
              }
          }
        }).flatten
      } else {
        enemyCardPairs.find(p => p._2 == us.card).map(panePair => {
          us.target map {
            case t: PlayerIsTarget =>
              if(isPlayerAlly(players, t.playerName)){
                val newLine = new Line()
                newLine.setFill(enemyLineTemplate.getFill)
                newLine.getStrokeDashArray.addAll(enemyLineTemplate.getStrokeDashArray)
                TargetInteraction(panePair._1, ownHpProgressBar, newLine, TableTarget.MyHealth)
              } else {
                val newLine = new Line()
                newLine.setFill(enemyLineTemplate.getFill)
                newLine.getStrokeDashArray.addAll(enemyLineTemplate.getStrokeDashArray)
                TargetInteraction(panePair._1, enemyHpProgressBar, newLine, TableTarget.EnemyHealth)
              }
            case t: PlayerTeamIsTarget =>
              if(isPlayerAlly(players, t.team.head)){
                val newLine = new Line()
                newLine.setFill(enemyLineTemplate.getFill)
                newLine.getStrokeDashArray.addAll(enemyLineTemplate.getStrokeDashArray)
                TargetInteraction(panePair._1, ownHpProgressBar, newLine, TableTarget.MyHealth)
              } else {
                val newLine = new Line()
                newLine.setFill(enemyLineTemplate.getFill)
                newLine.getStrokeDashArray.addAll(enemyLineTemplate.getStrokeDashArray)
                TargetInteraction(panePair._1, enemyHpProgressBar, newLine, TableTarget.EnemyHealth)
              }
            case t: CardIsTarget =>
              if(isPlayerAlly(players, t.position.player)){
                ownCardPairs.find(pair => pair._2 == CardPositionToCardHolder(t.position, players)).map(panePairTarget => {
                  val newLine = new Line()
                  newLine.setFill(enemyLineTemplate.getFill)
                  newLine.getStrokeDashArray.addAll(enemyLineTemplate.getStrokeDashArray)
                  TargetInteraction(panePair._1, panePairTarget._1, newLine, TableTarget.MyCard)
                }).get
              } else {
                ownCardPairs.find(pair => pair._2 == CardPositionToCardHolder(t.position, players)).map(panePairTarget => {
                  val newLine = new Line()
                  newLine.setFill(enemyLineTemplate.getFill)
                  newLine.getStrokeDashArray.addAll(enemyLineTemplate.getStrokeDashArray)
                  TargetInteraction(panePair._1, panePairTarget._1, newLine, TableTarget.EnemyCard)
                }).get
              }
          }
        }).flatten
      }
    }).filter(f => f.isDefined).map(f => f.get).toList
  }*/
  /*
    def apply(
               counterUsages: List[DefenderUsesCard],
               players: List[PlayerSeesPlayer],
               ownCardPairs: List[Pair[Pane, CardHolder]],
               enemyCardPairs: List[Pair[Pane, CardHolder]],
               ownHpProgressBar: ProgressBar,
               enemyHpProgressBar: ProgressBar): List[TargetInteraction] = {
  List()

    }*/
  /*
    def apply(
               usages: List[AttackerAttacks],
               players: List[PlayerSeesPlayer],
               ownCardPairs: List[Pair[Pane, CardHolder]],
               enemyCardPairs: List[Pair[Pane, CardHolder]],
               ownHpProgressBar: ProgressBar,
               enemyHpProgressBar: ProgressBar): List[TargetInteraction] = {
      List()

    }*/
  /*
    def apply(
               usages: List[DefenderDefends],
               players: List[PlayerSeesPlayer],
               ownCardPairs: List[Pair[Pane, CardHolder]],
               enemyCardPairs: List[Pair[Pane, CardHolder]],
               ownHpProgressBar: ProgressBar,
               enemyHpProgressBar: ProgressBar): List[TargetInteraction] = {
      List()

    }*/


  def apply(
             interactions: List[NITSession.GameMessages],
             players: List[PlayerSeesPlayer],
             ownCardPairs: List[Pair[Pane, CardHolder]],
             enemyCardPairs: List[Pair[Pane, CardHolder]],
             ownHpProgressBar: ProgressBar,
             enemyHpProgressBar: ProgressBar
             ): List[TargetInteraction] = {
    try {
      interactions.map(int =>
        int match {
          case i: NITSession.AttackerUsesCard =>
            players.find(p => p.player == i.onPosition.player).map(found => {
              found.relationship match {
                case PlayerRelationship.Me | PlayerRelationship.Ally =>
                  i.target.map {
                    case t: CEngine.card.CardIsTarget =>
                      players.find(p => p.player == t.position.player).map(playerForCard => {
                        playerForCard.relationship match {
                          case PlayerRelationship.Me | PlayerRelationship.Ally =>
                            val sourcePane = ownCardPairs.find(p => p._2 == i.card).map(p => p._1).get
                            val targetPane = ownCardPairs.find(p => p._2 == CardPositionToCardHolder(t.position, players)).map(p => p._1).get
                            val line = new Polyline()
                            line.setFill(allyLineTemplate.getFill)
                            line.setStroke(allyLineTemplate.getFill)
                            line.getStrokeDashArray().addAll(10d, 5d)
                            TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Ally, TableTarget.MyCard, i)

                          case PlayerRelationship.Enemy1 | PlayerRelationship.Enemy2 =>
                            val sourcePane = ownCardPairs.find(p => p._2 == i.card).map(p => p._1).get
                            val targetPane = enemyCardPairs.find(p => p._2 == CardPositionToCardHolder(t.position, players)).map(p => p._1).get
                            val line = new Line()
                            line.setFill(allyLineTemplate.getFill)
                            line.setStroke(allyLineTemplate.getFill)
                            line.getStrokeDashArray().addAll(10d, 5d)
                            TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Ally, TableTarget.EnemyCard, i)
                        }
                      }).get

                    case t: PlayerTeamIsTarget =>
                      players.find(p => p.player == t.team.head).map(playerForCard => {
                        playerForCard.relationship match {
                          case PlayerRelationship.Me | PlayerRelationship.Ally =>
                            val sourcePane = ownCardPairs.find(p => p._2 == i.card).map(p => p._1).get
                            val targetPane = ownHpProgressBar

                            val line = new Line()
                            line.setFill(allyLineTemplate.getFill)
                            line.setStroke(allyLineTemplate.getFill)
                            line.getStrokeDashArray().addAll(10d, 5d)
                            TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Ally, TableTarget.MyHealth, i)

                          case PlayerRelationship.Enemy1 | PlayerRelationship.Enemy2 =>
                            val sourcePane = ownCardPairs.find(p => p._2 == i.card).map(p => p._1).get
                            val targetPane = enemyHpProgressBar

                            val line = new Line()
                            line.setFill(allyLineTemplate.getFill)
                            line.setStroke(allyLineTemplate.getFill)
                            line.getStrokeDashArray().addAll(10d, 5d)
                            TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Ally, TableTarget.EnemyHealth, i)
                        }
                      }).get

                    case t: PlayerIsTarget =>
                      players.find(p => p.player == t.playerName).map(playerForCard => {
                        playerForCard.relationship match {
                          case PlayerRelationship.Me | PlayerRelationship.Ally =>
                            val sourcePane = ownCardPairs.find(p => p._2 == i.card).map(p => p._1).get
                            val targetPane = ownHpProgressBar
                            val line = new Line()
                            line.setFill(allyLineTemplate.getFill)
                            line.setStroke(allyLineTemplate.getFill)
                            line.getStrokeDashArray().addAll(10d, 5d)
                            TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Ally, TableTarget.MyHealth, i)

                          case PlayerRelationship.Enemy1 | PlayerRelationship.Enemy2 =>
                            val sourcePane = ownCardPairs.find(p => p._2 == i.card).map(p => p._1).get
                            val targetPane = enemyHpProgressBar

                            val line = new Line()
                            line.setFill(allyLineTemplate.getFill)
                            line.setStroke(allyLineTemplate.getFill)
                            line.getStrokeDashArray().addAll(10d, 5d)
                            TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Ally, TableTarget.EnemyHealth, i)
                        }
                      }).get
                  }

                case PlayerRelationship.Enemy1 | PlayerRelationship.Enemy2 =>
                  i.target.map {
                    case t: CEngine.card.CardIsTarget =>
                      players.find(p => p.player == t.position.player).map(playerForCard => {
                        playerForCard.relationship match {
                          case PlayerRelationship.Enemy1 | PlayerRelationship.Enemy2 =>
                            val sourcePane = enemyCardPairs.find(p => p._2 == i.card).map(p => p._1).get
                            val targetPane = enemyCardPairs.find(p => p._2 ==  CardPositionToCardHolder(t.position, players)).map(p => p._1).get

                            val line = new Polyline()
                            line.setFill(enemyLineTemplate.getFill)
                            line.setStroke(enemyLineTemplate.getStroke)
                            line.getStrokeDashArray().addAll(10d, 5d)
                            TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Enemy, TableTarget.EnemyCard, i)

                          case PlayerRelationship.Me | PlayerRelationship.Ally =>
                            val sourcePane = enemyCardPairs.find(p => p._2 == i.card).map(p => p._1).get
                            val targetPane = ownCardPairs.find(p => p._2 == CardPositionToCardHolder(t.position, players)).map(p => p._1).get

                            val line = new Line()
                            line.setFill(enemyLineTemplate.getFill)
                            line.setStroke(enemyLineTemplate.getStroke)
                            line.getStrokeDashArray().addAll(10d, 5d)
                            TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Enemy, TableTarget.MyCard, i)
                        }
                      }).get

                    case t: PlayerTeamIsTarget =>
                      players.find(p => p.player == t.team.head).map(playerForCard => {
                        playerForCard.relationship match {
                          case PlayerRelationship.Me | PlayerRelationship.Ally =>
                            val sourcePane = enemyCardPairs.find(p => p._2 == i.card).map(p => p._1).get
                            val targetPane = ownHpProgressBar

                            val line = new Line()
                            line.setFill(enemyLineTemplate.getFill)
                            line.setStroke(enemyLineTemplate.getStroke)
                            line.getStrokeDashArray().addAll(10d, 5d)
                            TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Enemy, TableTarget.MyHealth, i)

                          case PlayerRelationship.Enemy1 | PlayerRelationship.Enemy2 =>
                            val sourcePane = enemyCardPairs.find(p => p._2 == i.card).map(p => p._1).get
                            val targetPane = enemyHpProgressBar

                            val line = new Line()
                            line.setFill(enemyLineTemplate.getFill)
                            line.setStroke(enemyLineTemplate.getStroke)
                            line.getStrokeDashArray().addAll(10d, 5d)
                            TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Enemy, TableTarget.EnemyHealth, i)
                        }
                      }).get

                    case t: PlayerIsTarget =>
                      players.find(p => p.player == t.playerName).map(playerForCard => {
                        playerForCard.relationship match {
                          case PlayerRelationship.Me | PlayerRelationship.Ally =>
                            val sourcePane = enemyCardPairs.find(p => p._2 == i.card).map(p => p._1).get
                            val targetPane = ownHpProgressBar

                            val line = new Line()
                            line.setFill(enemyLineTemplate.getFill)
                            line.setStroke(enemyLineTemplate.getStroke)
                            line.getStrokeDashArray().addAll(10d, 5d)
                            TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Enemy, TableTarget.MyHealth, i)

                          case PlayerRelationship.Enemy1 | PlayerRelationship.Enemy2 =>
                            val sourcePane = enemyCardPairs.find(p => p._2 == i.card).map(p => p._1).get
                            val targetPane = enemyHpProgressBar

                            val line = new Line()
                            line.setFill(enemyLineTemplate.getFill)
                            line.setStroke(enemyLineTemplate.getStroke)
                            line.getStrokeDashArray().addAll(10d, 5d)
                            TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Enemy, TableTarget.EnemyHealth, i)
                        }
                      }).get
                  }
              }
            }).get

          case i: NITSession.DefenderUsesCard =>
            players.find(p => p.player == i.onPosition.player).map(found => {
              found.relationship match {
                case PlayerRelationship.Me | PlayerRelationship.Ally =>
                  i.target.map {
                    case t: CEngine.card.CardIsTarget =>
                      players.find(p => p.player == t.position.player).map(playerForCard => {
                        playerForCard.relationship match {
                          case PlayerRelationship.Me | PlayerRelationship.Ally =>
                            val sourcePane = ownCardPairs.find(p => p._2 == i.card).map(p => p._1).get
                            val targetPane = ownCardPairs.find(p => p._2 == CardPositionToCardHolder(t.position, players)).map(p => p._1).get
                            val line = new Polyline()
                            line.setFill(allyLineTemplate.getFill)
                            line.setStroke(allyLineTemplate.getFill)
                            line.getStrokeDashArray().addAll(10d, 5d)
                            TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Ally, TableTarget.MyCard, i)

                          case PlayerRelationship.Enemy1 | PlayerRelationship.Enemy2 =>
                            val sourcePane = ownCardPairs.find(p => p._2 == i.card).map(p => p._1).get
                            val targetPane = enemyCardPairs.find(p => p._2 == CardPositionToCardHolder(t.position, players)).map(p => p._1).get
                            val line = new Line()
                            line.setFill(allyLineTemplate.getFill)
                            line.setStroke(allyLineTemplate.getFill)
                            line.getStrokeDashArray().addAll(10d, 5d)
                            TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Ally, TableTarget.EnemyCard, i)
                        }
                      }).get

                    case t: PlayerTeamIsTarget =>
                      players.find(p => p.player == t.team.head).map(playerForCard => {
                        playerForCard.relationship match {
                          case PlayerRelationship.Me | PlayerRelationship.Ally =>
                            val sourcePane = ownCardPairs.find(p => p._2 == i.card).map(p => p._1).get
                            val targetPane = ownHpProgressBar

                            val line = new Line()
                            line.setFill(allyLineTemplate.getFill)
                            line.setStroke(allyLineTemplate.getFill)
                            line.getStrokeDashArray().addAll(10d, 5d)
                            TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Ally, TableTarget.MyHealth, i)

                          case PlayerRelationship.Enemy1 | PlayerRelationship.Enemy2 =>
                            val sourcePane = ownCardPairs.find(p => p._2 == i.card).map(p => p._1).get
                            val targetPane = enemyHpProgressBar

                            val line = new Line()
                            line.setFill(allyLineTemplate.getFill)
                            line.setStroke(allyLineTemplate.getFill)
                            line.getStrokeDashArray().addAll(10d, 5d)
                            TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Ally, TableTarget.EnemyHealth, i)
                        }
                      }).get

                    case t: PlayerIsTarget =>
                      players.find(p => p.player == t.playerName).map(playerForCard => {
                        playerForCard.relationship match {
                          case PlayerRelationship.Me | PlayerRelationship.Ally =>
                            val sourcePane = ownCardPairs.find(p => p._2 == i.card).map(p => p._1).get
                            val targetPane = ownHpProgressBar

                            val line = new Line()
                            line.setFill(allyLineTemplate.getFill)
                            line.setStroke(allyLineTemplate.getFill)
                            line.getStrokeDashArray().addAll(10d, 5d)
                            TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Ally, TableTarget.MyHealth, i)

                          case PlayerRelationship.Enemy1 | PlayerRelationship.Enemy2 =>
                            val sourcePane = ownCardPairs.find(p => p._2 == i.card).map(p => p._1).get
                            val targetPane = enemyHpProgressBar

                            val line = new Line()
                            line.setFill(allyLineTemplate.getFill)
                            line.setStroke(allyLineTemplate.getFill)
                            line.getStrokeDashArray().addAll(10d, 5d)
                            TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Ally, TableTarget.EnemyHealth, i)
                        }
                      }).get
                  }

                case PlayerRelationship.Enemy1 | PlayerRelationship.Enemy2 =>

                  i.target.map {
                    case t: CEngine.card.CardIsTarget =>
                      players.find(p => p.player == t.position.player).map(playerForCard => {
                        playerForCard.relationship match {
                          case PlayerRelationship.Enemy1 | PlayerRelationship.Enemy2 =>
                            val sourcePane = enemyCardPairs.find(p => p._2 == i.card).map(p => p._1).get
                            val targetPane = enemyCardPairs.find(p => p._2 ==  CardPositionToCardHolder(t.position, players)).map(p => p._1).get

                            val line = new Polyline()
                            line.setFill(enemyLineTemplate.getFill)
                            line.setStroke(enemyLineTemplate.getStroke)
                            line.getStrokeDashArray().addAll(10d, 5d)
                            TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Enemy, TableTarget.EnemyCard, i)

                          case PlayerRelationship.Me | PlayerRelationship.Ally =>
                            val sourcePane = enemyCardPairs.find(p => p._2 == i.card).map(p => p._1).get
                            val targetPane = ownCardPairs.find(p => p._2 ==  CardPositionToCardHolder(t.position, players)).map(p => p._1).get

                            val line = new Line()
                            line.setFill(enemyLineTemplate.getFill)
                            line.setStroke(enemyLineTemplate.getStroke)
                            line.getStrokeDashArray().addAll(10d, 5d)
                            TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Enemy, TableTarget.MyCard, i)
                        }
                      }).get

                    case t: PlayerTeamIsTarget =>
                      players.find(p => p.player == t.team.head).map(playerForCard => {
                        playerForCard.relationship match {
                          case PlayerRelationship.Me | PlayerRelationship.Ally =>
                            val sourcePane = enemyCardPairs.find(p => p._2 == i.card).map(p => p._1).get
                            val targetPane = ownHpProgressBar

                            val line = new Line()
                            line.setFill(enemyLineTemplate.getFill)
                            line.setStroke(enemyLineTemplate.getStroke)
                            line.getStrokeDashArray().addAll(10d, 5d)
                            TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Enemy, TableTarget.MyHealth, i)

                          case PlayerRelationship.Enemy1 | PlayerRelationship.Enemy2 =>
                            val sourcePane = enemyCardPairs.find(p => p._2 == i.card).map(p => p._1).get
                            val targetPane = enemyHpProgressBar

                            val line = new Line()
                            line.setFill(enemyLineTemplate.getFill)
                            line.setStroke(enemyLineTemplate.getStroke)
                            line.getStrokeDashArray().addAll(10d, 5d)
                            TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Enemy, TableTarget.EnemyHealth, i)
                        }
                      }).get

                    case t: PlayerIsTarget =>
                      players.find(p => p.player == t.playerName).map(playerForCard => {
                        playerForCard.relationship match {
                          case PlayerRelationship.Me | PlayerRelationship.Ally =>
                            val sourcePane = enemyCardPairs.find(p => p._2 == i.card).map(p => p._1).get
                            val targetPane = ownHpProgressBar

                            val line = new Line()
                            line.setFill(enemyLineTemplate.getFill)
                            line.setStroke(enemyLineTemplate.getStroke)
                            line.getStrokeDashArray().addAll(10d, 5d)
                            TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Enemy, TableTarget.MyHealth, i)

                          case PlayerRelationship.Enemy1 | PlayerRelationship.Enemy2 =>
                            val sourcePane = enemyCardPairs.find(p => p._2 == i.card).map(p => p._1).get
                            val targetPane = enemyHpProgressBar

                            val line = new Line()
                            line.setFill(enemyLineTemplate.getFill)
                            line.setStroke(enemyLineTemplate.getStroke)
                            line.getStrokeDashArray().addAll(10d, 5d)
                            TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Enemy, TableTarget.EnemyHealth, i)
                        }
                      }).get
                  }
              }
            }).get

          case i: NITSession.AttackerAttacks =>
            Some(players.find(p => p.player == i.withPosition.player).map(found => {
              found.relationship match {
                case PlayerRelationship.Me | PlayerRelationship.Ally =>
                  val sourcePane = ownCardPairs.find(p => p._2 == i.card).map(p => p._1).get
                  val targetPane = enemyHpProgressBar

                  val line = new Line()
                  line.setFill(allyLineTemplate.getFill)
                  line.setStroke(allyLineTemplate.getFill)
                  line.getStrokeDashArray().addAll(10d, 5d)
                  TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Ally, TableTarget.EnemyHealth, i)

                case PlayerRelationship.Enemy1 | PlayerRelationship.Enemy2 =>
                  val sourcePane = enemyCardPairs.find(p => p._2 == i.card).map(p => p._1).get
                  val targetPane = ownHpProgressBar

                  val line = new Line()
                  line.setFill(enemyLineTemplate.getFill)
                  line.setStroke(enemyLineTemplate.getStroke)
                  line.getStrokeDashArray().addAll(10d, 5d)
                  TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Enemy, TableTarget.MyHealth, i)
              }
            }).get)


          case i: NITSession.DefenderDefends =>
            Some(players.find(p => p.player == i.withPosition.player).map(found => {
              found.relationship match {
                case PlayerRelationship.Me | PlayerRelationship.Ally =>
                  i.againts match {
                    case t: CEngine.card.CardIsTarget =>
                      players.find(p => p.player == t.position.player).map(playerForCard => {
                        playerForCard.relationship match {
                          case PlayerRelationship.Me | PlayerRelationship.Ally =>
                            val sourcePane = ownCardPairs.find(p => p._2 == i.defenderCard).map(p => p._1).get
                            val targetPane = ownCardPairs.find(p => p._2 ==  CardPositionToCardHolder(t.position, players)).map(p => p._1).get

                            val line = new Polyline()
                            line.setFill(allyLineTemplate.getFill)
                            line.setStroke(allyLineTemplate.getFill)
                            line.getStrokeDashArray().addAll(10d, 5d)
                            TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Ally, TableTarget.MyCard, i)

                          case PlayerRelationship.Enemy1 | PlayerRelationship.Enemy2 =>
                            val sourcePane = ownCardPairs.find(p => p._2 == i.defenderCard).map(p => p._1).get
                            val targetPane = enemyCardPairs.find(p => p._2 ==  CardPositionToCardHolder(t.position, players)).map(p => p._1).get

                            val line = new Line()
                            line.setFill(allyLineTemplate.getFill)
                            line.setStroke(allyLineTemplate.getFill)
                            line.getStrokeDashArray().addAll(10d, 5d)
                            TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Ally, TableTarget.EnemyCard, i)
                        }
                      }).get

                    case t: PlayerTeamIsTarget =>
                      players.find(p => p.player == t.team.head).map(playerForCard => {
                        playerForCard.relationship match {
                          case PlayerRelationship.Me | PlayerRelationship.Ally =>
                            val sourcePane = ownCardPairs.find(p => p._2 == i.defenderCard).map(p => p._1).get
                            val targetPane = ownHpProgressBar

                            val line = new Line()
                            line.setFill(allyLineTemplate.getFill)
                            line.setStroke(allyLineTemplate.getFill)
                            line.getStrokeDashArray().addAll(10d, 5d)
                            TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Ally, TableTarget.MyHealth, i)

                          case PlayerRelationship.Enemy1 | PlayerRelationship.Enemy2 =>
                            val sourcePane = ownCardPairs.find(p => p._2 == i.defenderCard).map(p => p._1).get
                            val targetPane = enemyHpProgressBar

                            val line = new Line()
                            line.setFill(allyLineTemplate.getFill)
                            line.setStroke(allyLineTemplate.getFill)
                            line.getStrokeDashArray().addAll(10d, 5d)
                            TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Ally, TableTarget.EnemyHealth, i)
                        }
                      }).get

                    case t: PlayerIsTarget =>
                      players.find(p => p.player == t.playerName).map(playerForCard => {
                        playerForCard.relationship match {
                          case PlayerRelationship.Me | PlayerRelationship.Ally =>
                            val sourcePane = ownCardPairs.find(p => p._2 == i.defenderCard).map(p => p._1).get
                            val targetPane = ownHpProgressBar

                            val line = new Line()
                            line.setFill(allyLineTemplate.getFill)
                            line.setStroke(allyLineTemplate.getFill)
                            line.getStrokeDashArray().addAll(10d, 5d)
                            TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Ally, TableTarget.MyHealth, i)

                          case PlayerRelationship.Enemy1 | PlayerRelationship.Enemy2 =>
                            val sourcePane = ownCardPairs.find(p => p._2 == i.defenderCard).map(p => p._1).get
                            val targetPane = enemyHpProgressBar

                            val line = new Line()
                            line.setFill(allyLineTemplate.getFill)
                            line.setStroke(allyLineTemplate.getFill)
                            line.getStrokeDashArray().addAll(10d, 5d)
                            TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Ally, TableTarget.EnemyHealth, i)
                        }
                      }).get
                  }

                case PlayerRelationship.Enemy1 | PlayerRelationship.Enemy2 =>
                  i.againts match {
                    case t: CEngine.card.CardIsTarget =>
                      players.find(p => p.player == t.position.player).map(playerForCard => {
                        playerForCard.relationship match {
                          case PlayerRelationship.Enemy1 | PlayerRelationship.Enemy2 =>
                            val sourcePane = enemyCardPairs.find(p => p._2 == i.defenderCard).map(p => p._1).get
                            val targetPane = enemyCardPairs.find(p => p._2 ==  CardPositionToCardHolder(t.position, players)).map(p => p._1).get

                            val line = new Polyline()
                            line.setFill(enemyLineTemplate.getFill)
                            line.setStroke(enemyLineTemplate.getStroke)
                            line.getStrokeDashArray().addAll(10d, 5d)
                            TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Enemy, TableTarget.EnemyCard, i)

                          case PlayerRelationship.Me | PlayerRelationship.Ally =>
                            val sourcePane = enemyCardPairs.find(p => p._2 == i.defenderCard).map(p => p._1).get
                            val targetPane = ownCardPairs.find(p => p._2 ==  CardPositionToCardHolder(t.position, players)).map(p => p._1).get

                            val line = new Line()
                            line.setFill(enemyLineTemplate.getFill)
                            line.setStroke(enemyLineTemplate.getStroke)
                            line.getStrokeDashArray().addAll(10d, 5d)
                            TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Enemy, TableTarget.MyCard, i)
                        }
                      }).get

                    case t: PlayerTeamIsTarget =>
                      players.find(p => p.player == t.team.head).map(playerForCard => {
                        playerForCard.relationship match {
                          case PlayerRelationship.Me | PlayerRelationship.Ally =>
                            val sourcePane = enemyCardPairs.find(p => p._2 == i.defenderCard).map(p => p._1).get
                            val targetPane = ownHpProgressBar

                            val line = new Line()
                            line.setFill(enemyLineTemplate.getFill)
                            line.setStroke(enemyLineTemplate.getStroke)
                            line.getStrokeDashArray().addAll(10d, 5d)
                            TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Enemy, TableTarget.MyHealth, i)

                          case PlayerRelationship.Enemy1 | PlayerRelationship.Enemy2 =>
                            val sourcePane = enemyCardPairs.find(p => p._2 == i.defenderCard).map(p => p._1).get
                            val targetPane = enemyHpProgressBar

                            val line = new Line()
                            line.setFill(enemyLineTemplate.getFill)
                            line.setStroke(enemyLineTemplate.getStroke)
                            line.getStrokeDashArray().addAll(10d, 5d)
                            TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Enemy, TableTarget.EnemyHealth, i)
                        }
                      }).get

                    case t: PlayerIsTarget =>
                      players.find(p => p.player == t.playerName).map(playerForCard => {
                        playerForCard.relationship match {
                          case PlayerRelationship.Me | PlayerRelationship.Ally =>
                            val sourcePane = enemyCardPairs.find(p => p._2 == i.defenderCard).map(p => p._1).get
                            val targetPane = ownHpProgressBar

                            val line = new Line()
                            line.setFill(enemyLineTemplate.getFill)
                            line.setStroke(enemyLineTemplate.getStroke)
                            line.getStrokeDashArray().addAll(10d, 5d)
                            TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Enemy, TableTarget.MyHealth, i)

                          case PlayerRelationship.Enemy1 | PlayerRelationship.Enemy2 =>
                            val sourcePane = enemyCardPairs.find(p => p._2 == i.defenderCard).map(p => p._1).get
                            val targetPane = enemyHpProgressBar

                            val line = new Line()
                            line.setFill(enemyLineTemplate.getFill)
                            line.setStroke(enemyLineTemplate.getStroke)
                            line.getStrokeDashArray().addAll(10d, 5d)
                            TargetInteraction(sourcePane, targetPane, line, TableInteractionSource.Enemy, TableTarget.EnemyHealth, i)
                        }
                      }).get
                  }
              }
            }).get)
        }).filter(f => f.isDefined).map(f => f.get).toList
    } catch {
      case t: Throwable =>
        println(t)
        List()
    }
  }

  /*
    def isPlayerAlly(players: List[PlayerSeesPlayer], player: String): Boolean = {
      players.find(p => p.player == player).map(pl => pl.relationship match {
        case PlayerRelationship.Me | PlayerRelationship.Ally =>
          true
        case PlayerRelationship.Enemy1 | PlayerRelationship.Enemy2 =>
          false
      }).get
    }*/
}
