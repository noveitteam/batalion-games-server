package cz.bataliongames.firstenchanter.form.controllers.table

import javafx.animation.{KeyValue, KeyFrame, Timeline}
import javafx.event.EventHandler
import javafx.scene.input.MouseEvent
import javafx.scene.layout.Pane
import javafx.scene.paint.Color
import javafx.scene.shape.Circle
import javafx.scene.text.Text
import javafx.util.Duration

import cz.bataliongames.firstenchanter.form.controllers.TableState
import cz.noveit.games.cardgame.engine.game.session.PlayerRollPosition

import javafx.event.ActionEvent

/**
 * Created by arnostkuchar on 14.10.14.
 */
class SubRoundBuilder {
  /*
  * subRoundLabel
  * phase1Circle
  * phase2Circle
  * phase3Circle
  * phase4Circle
  * subRoundTimerLabel
  * subRoundTimerLabel
  * */

  private var phase1Circle: Circle = _
  private var phase2Circle: Circle = _
  private var phase3Circle: Circle = _
  private var phase4Circle: Circle = _

  private var subRoundLabel: Text = _
  private var subRoundTimerLabel: Text = _
  private var subRoundEnd: Text = _

  private val enemyColor = Color.web("D64960")
  private val allyColor = Color.web("148C38")

  private var timeline: Timeline = _

  def apply(subRoundPane: Pane, state: TableState.Value, endSubRound: () => Unit, initialTimer: Long): Unit = {
    phase1Circle = subRoundPane.lookup("#phase1Circle").asInstanceOf[Circle]
    phase2Circle = subRoundPane.lookup("#phase2Circle").asInstanceOf[Circle]
    phase3Circle = subRoundPane.lookup("#phase3Circle").asInstanceOf[Circle]
    phase4Circle = subRoundPane.lookup("#phase4Circle").asInstanceOf[Circle]

    subRoundLabel = subRoundPane.lookup("#subRoundLabel").asInstanceOf[Text]
    subRoundTimerLabel = subRoundPane.lookup("#subRoundTimerLabel").asInstanceOf[Text]
    subRoundTimerLabel.setText(timeToString(initialTimer.toInt / 1000))

    subRoundEnd = subRoundPane.lookup("#subRoundEnd").asInstanceOf[Text]

    subRoundEnd.setOnMouseClicked(new EventHandler[MouseEvent] {
      override def handle(p1: MouseEvent): Unit = {
        if(timeline != null) timeline.stop()
        endSubRound()
      }
    })

    subRoundPane.setVisible(true)
    state match {
      case TableState.Using =>
        subRoundLabel.setFill(allyColor)
        subRoundLabel.setText("Use your cards!")

        phase1Circle.setFill(allyColor)
        phase2Circle.setFill(enemyColor)
        phase3Circle.setFill(allyColor)
        phase4Circle.setFill(enemyColor)

        subRoundEnd.setVisible(true)

      case TableState.WaitingForEnemyUse =>
        subRoundLabel.setFill(enemyColor)
        subRoundLabel.setText("Waiting for enemy cards!")

        phase1Circle.setFill(enemyColor)
        phase2Circle.setFill(allyColor)
        phase3Circle.setFill(enemyColor)
        phase4Circle.setFill(allyColor)

        subRoundEnd.setVisible(false)

      case TableState.CounterUsing =>
        subRoundLabel.setFill(allyColor)
        subRoundLabel.setText("Use your cards!")

        phase1Circle.setFill(enemyColor)
        phase2Circle.setFill(allyColor)
        phase3Circle.setFill(enemyColor)
        phase4Circle.setFill(allyColor)

        subRoundEnd.setVisible(true)

      case TableState.WaitingForEnemyCounterUse =>
        subRoundLabel.setFill(enemyColor)
        subRoundLabel.setText("Waiting for enemy cards!")

        phase1Circle.setFill(allyColor)
        phase2Circle.setFill(enemyColor)
        phase3Circle.setFill(allyColor)
        phase4Circle.setFill(enemyColor)

        subRoundEnd.setVisible(false)

      case TableState.Attacking =>
        subRoundLabel.setFill(allyColor)
        subRoundLabel.setText("Choose cards to attack with!")

        phase1Circle.setFill(allyColor)
        phase2Circle.setFill(enemyColor)
        phase3Circle.setFill(allyColor)
        phase4Circle.setFill(enemyColor)

        subRoundEnd.setVisible(true)

      case TableState.WaitingForEnemyAttack =>
        subRoundLabel.setFill(enemyColor)
        subRoundLabel.setText("Waiting for enemy attacks!")

        phase1Circle.setFill(enemyColor)
        phase2Circle.setFill(allyColor)
        phase3Circle.setFill(enemyColor)
        phase4Circle.setFill(allyColor)

        subRoundEnd.setVisible(false)
      case TableState.Defending =>
        subRoundLabel.setFill(allyColor)
        subRoundLabel.setText("Choose cards to defend against!")

        phase1Circle.setFill(enemyColor)
        phase2Circle.setFill(allyColor)
        phase3Circle.setFill(enemyColor)
        phase4Circle.setFill(allyColor)

        subRoundEnd.setVisible(true)

      case TableState.WaitingForEnemyDefend =>
        subRoundLabel.setFill(enemyColor)
        subRoundLabel.setText("Waiting for enemy defends!")

        phase1Circle.setFill(allyColor)
        phase2Circle.setFill(enemyColor)
        phase3Circle.setFill(allyColor)
        phase4Circle.setFill(enemyColor)

        subRoundEnd.setVisible(false)
      case state =>
        subRoundPane.setVisible(false)
    }

    timeline = new Timeline()
    timeline.setCycleCount((initialTimer / 1000).toInt)
   //timeline.setAutoReverse(false)
    timeline.getKeyFrames().addAll(
      new KeyFrame(
        Duration.seconds(0),
        new EventHandler[ActionEvent]() {
          @Override def handle(actionEvent: ActionEvent): Unit = {
            subRoundTimerLabel.setText(timeToString(stringToTime(subRoundTimerLabel.getText) - 1))
          }
        }
      ),
      new KeyFrame(Duration.seconds(1))
    )
    timeline.setOnFinished(new EventHandler[ActionEvent] {
      override def handle(p1: ActionEvent): Unit = {
        subRoundEnd.setVisible(false)
        endSubRound()
      }
    })
    timeline.play();
  }

  def forceEnd: Unit = {
    if(timeline != null) timeline.stop()
  }

  private def timeToString(numberOfSeconds: Int): String = {
    if(numberOfSeconds > 0) {
      "%02d:%02d".format(numberOfSeconds / 60, numberOfSeconds % 60)
    } else {
      "00:00"
    }
  }

  private def stringToTime(string: String): Int = {
    val splitted = string.split(":").map(s => s.toInt)
    if (splitted.size == 2) {
      splitted(0) * 60 + splitted(1)
    } else {
      -1
    }
  }
}
