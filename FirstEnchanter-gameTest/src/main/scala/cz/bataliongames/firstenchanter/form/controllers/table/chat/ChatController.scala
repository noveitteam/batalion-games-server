package cz.bataliongames.firstenchanter.form.controllers.table.chat

import javafx.event.EventHandler
import javafx.scene.control.{ScrollPane, Label, TextField}
import javafx.scene.input.{KeyCode, KeyEvent, MouseEvent}
import javafx.scene.layout.{Pane, VBox}

import cz.bataliongames.firstenchanter.form.actors.PlayerRelationship
import cz.noveit.proto.serialization.MessageEvent
import scalafx.Includes._

/**
 * Created by arnostkuchar on 03.11.14.
 */

object ChatMode extends Enumeration {
  val Team, All = Value
}

class ChatController(
                      val root: Pane,
                      val chatInputBoxTextField: TextField,
                      val chatSendButtonLabel: Label,
                      val chatMessagesScrollPale: ScrollPane,
                      val chatMessagesVBox: VBox,
                      val chatSwitchToTeamChatLabel: Label,
                      val switchToAllChatLabel: Label,
                      val chatAllyMessageTemplatePane: Pane,
                      val chatEnemyMessageTemplatePane: Pane,
                      val sentToTeamMessageHandler: (String) => Unit,
                      val sentToAllMessageHandler: (String) => Unit
                      ) {

  private var messages: List[ChatMessage] = List()
  private var mode = ChatMode.Team
  private var enterEventHandler: Option[EventHandler[KeyEvent]] = None

  chatMessagesVBox.heightProperty().onChange({
    chatMessagesScrollPale.setVvalue(chatMessagesScrollPale.getVmax)
  })

  chatSwitchToTeamChatLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler[MouseEvent] {
    override def handle(p1: MouseEvent): Unit = {
      if (mode != ChatMode.Team) {
        mode = ChatMode.Team
        chatSendButtonLabel.setText("Send to team")
        switchToAllChatLabel.setText(switchToAllChatLabel.getText.replace(">", "").replace("<", ""))
        chatSwitchToTeamChatLabel.setText(">" + chatSwitchToTeamChatLabel.getText + "<")
        chatMessagesScrollPale.setVvalue(1)
        repaintMessages
      }
    }
  })

  switchToAllChatLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler[MouseEvent] {
    override def handle(p1: MouseEvent): Unit = {
      if (mode != ChatMode.All) {
        mode = ChatMode.All
        chatSendButtonLabel.setText("Send to all")
        chatSwitchToTeamChatLabel.setText(chatSwitchToTeamChatLabel.getText.replace(">", "").replace("<", ""))
        switchToAllChatLabel.setText(">" + switchToAllChatLabel.getText + "<")
        chatMessagesScrollPale.setVvalue(1)
        repaintMessages
      }
    }
  })

  chatSendButtonLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler[MouseEvent] {
    override def handle(p1: MouseEvent): Unit = {
      mode match {
        case ChatMode.Team => if (chatInputBoxTextField.getText().trim != "") {
          sentToTeamMessageHandler(chatInputBoxTextField.getText())
          chatInputBoxTextField.setText("")
        }
        case ChatMode.All => if (chatInputBoxTextField.getText().trim != "") {
          sentToAllMessageHandler(chatInputBoxTextField.getText())
          chatInputBoxTextField.setText("")
        }
      }
    }
  })

  def onMoveToScene = {
    val eh = new EventHandler[KeyEvent] {
      override def handle(p1: KeyEvent): Unit = {
        if(p1.getCode.equals(KeyCode.ENTER)) {
          mode match {
            case ChatMode.Team => if (chatInputBoxTextField.getText().trim != "") {
              sentToTeamMessageHandler(chatInputBoxTextField.getText())
              chatInputBoxTextField.setText("")
            }
            case ChatMode.All => if (chatInputBoxTextField.getText().trim != "") {
              sentToAllMessageHandler(chatInputBoxTextField.getText())
              chatInputBoxTextField.setText("")
            }
          }
        }
      }
    }

    enterEventHandler = Some(eh)
    chatInputBoxTextField.addEventHandler(KeyEvent.KEY_PRESSED, eh)
  }

  def onMoveFromScene = {
    enterEventHandler.map(eh => {
      chatInputBoxTextField.removeEventHandler(KeyEvent.KEY_PRESSED, eh)
    })
  }

  def clearMessages = {
    chatMessagesVBox.getChildren.clear()
    messages = List()
  }


  def addMessage(message: ChatMessage): Unit = {
    if (messages.size == 0 | {
      try {
        message.time >= messages.last.time
      } catch {
        case t: Throwable => false
      }
    }) {
      messages = message :: messages
      if (mode == message.chatMode) {
        chatMessagesVBox.getChildren.add(ChatMessageCreator(chatAllyMessageTemplatePane, chatEnemyMessageTemplatePane, message))
      }

    } else {
      messages = message :: messages
      messages = messages.sortBy(s => s.time)
      repaintMessages
    }

  }

  def repaintMessages: Unit = {
    chatMessagesVBox.getChildren.clear()
    messages.filter(m => m.chatMode == mode).sortBy(s => s.time).map(message => {
      chatMessagesVBox.getChildren.add(ChatMessageCreator(chatAllyMessageTemplatePane, chatEnemyMessageTemplatePane, message))
    })
  }
}
