package cz.bataliongames.firstenchanter.form.controllers.table.chat

import javafx.scene.control.{TextArea, Label}
import javafx.scene.layout.Pane

import cz.bataliongames.firstenchanter.form.actors.PlayerRelationship
import cz.bataliongames.firstenchanter.form.controllers.table.duplicator.{TextAreaDuplicator, LabelDuplicator, PaneDuplicator}
import org.joda.time.DateTime

/**
 * Created by arnostkuchar on 03.11.14.
 */

case class ChatMessage(nick: String, time: Long, message: String, relationship: PlayerRelationship.Value, predefined: Boolean = false, chatMode: ChatMode.Value)
object ChatMessageCreator {
  def apply(chatAllyMessageTemplate: Pane, chatEnemyMessageTemplate: Pane, message: ChatMessage): Pane = {
    message.relationship match {
      case PlayerRelationship.Enemy1 | PlayerRelationship.Enemy2 =>
        val newChatMessagePane = PaneDuplicator(chatEnemyMessageTemplate)

        val nickLabel: Label = LabelDuplicator(chatEnemyMessageTemplate.getChildren.get(0).asInstanceOf[Label])
        val timeLabel: Label = LabelDuplicator(chatEnemyMessageTemplate.getChildren.get(1).asInstanceOf[Label])
        val messageTextArea: TextArea = TextAreaDuplicator(chatEnemyMessageTemplate.getChildren.get(2).asInstanceOf[TextArea])
        val jodaTime = new DateTime(message.time)
        nickLabel.setText(message.nick)
        timeLabel.setText(jodaTime.toString("HH:mm:ss"))
        messageTextArea.setText(message.message)

        newChatMessagePane.getChildren.addAll(nickLabel, timeLabel, messageTextArea)
        newChatMessagePane

      case PlayerRelationship.Ally | PlayerRelationship.Me =>
        val newChatMessagePane = PaneDuplicator(chatAllyMessageTemplate)

        val nickLabel: Label = LabelDuplicator(chatAllyMessageTemplate.getChildren.get(0).asInstanceOf[Label])
        val timeLabel: Label = LabelDuplicator(chatAllyMessageTemplate.getChildren.get(1).asInstanceOf[Label])
        val messageTextArea: TextArea = TextAreaDuplicator(chatAllyMessageTemplate.getChildren.get(2).asInstanceOf[TextArea])
        val jodaTime = new DateTime(message.time)
        nickLabel.setText(message.nick)
        timeLabel.setText(jodaTime.toString("HH:mm:ss"))
        messageTextArea.setText(message.message)

        newChatMessagePane.getChildren.addAll(nickLabel, timeLabel, messageTextArea)
        newChatMessagePane
    }
  }
}
