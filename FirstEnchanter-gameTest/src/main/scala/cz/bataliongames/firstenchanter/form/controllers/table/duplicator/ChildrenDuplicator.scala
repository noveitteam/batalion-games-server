package cz.bataliongames.firstenchanter.form.controllers.table.duplicator

import javafx.scene.Node
import javafx.scene.control.{TextArea, Label}
import javafx.scene.image.ImageView
import javafx.scene.shape.Rectangle

/**
 * Created by arnostkuchar on 07.10.14.
 */
object ChildrenDuplicator {
  def apply(nodes: List[Node]): List[Node] = {
    var toReturn: List[Node] = List()
    nodes.map(n => n match {
      case l: Label =>  toReturn =  LabelDuplicator(l) :: toReturn
      case iv: ImageView => toReturn = ImageViewDuplicator(iv) :: toReturn
      case ta: TextArea => toReturn = TextAreaDuplicator(ta) :: toReturn
      case r: Rectangle => toReturn = RectangleDuplicator(r) :: toReturn
      case t => t
    })
    toReturn
  }
}
