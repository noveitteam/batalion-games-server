package cz.bataliongames.firstenchanter.form.controllers.table.duplicator

import javafx.geometry.Point2D
import javafx.scene.Node
import javafx.scene.layout.{Pane, HBox}
import scala.collection.JavaConverters._
/**
 * Created by arnostkuchar on 17.10.14.
 */
object GetLocalDimensionsOfElementInCenteredHBox {
  def apply(element: Pane, hBox: HBox) = {
    val children = hBox.getChildren.asScala
    val width = children.head.getBoundsInLocal.getWidth
    val index = hBox.getChildren.indexOf(element)
    if(index >= 0) {
      println("children counts: " + children.size)
     val contentWidth = children.map(ch => {
       print("Width: " + ch.getBoundsInLocal.getWidth)
       width
     }).sum + (index + 1) * hBox.getSpacing + hBox.getPadding.getLeft
      println("Content width: " + contentWidth)
      println("Layout width: " + hBox.getBoundsInLocal.getWidth)
     val offset = (hBox.getBoundsInLocal.getWidth - contentWidth) / 2
      println("Offset: " + offset)
     val elementPosition = offset + children.slice(0, index).map(sl => width).sum + hBox.getPadding.getLeft + index * hBox.getSpacing

     new Point2D(elementPosition, hBox.getPadding.getTop)
    } else {
      new Point2D(0, 0)
    }
  }
}
