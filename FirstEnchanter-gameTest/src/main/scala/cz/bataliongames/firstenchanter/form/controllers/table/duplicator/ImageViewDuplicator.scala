package cz.bataliongames.firstenchanter.form.controllers.table.duplicator

import javafx.scene.image.ImageView

/**
 * Created by arnostkuchar on 06.10.14.
 */
object ImageViewDuplicator {
  def apply(node: ImageView): ImageView = {
    val newNode = new ImageView()
    newNode.setImage(node.getImage)
    newNode.setLayoutX(node.getLayoutX)
    newNode.setLayoutY(node.getLayoutY)
    newNode.setFitWidth(node.getFitWidth)
    newNode.setFitHeight(node.getFitHeight)
    node.setPreserveRatio(node.isPreserveRatio)
    node.setSmooth(node.isSmooth)
    newNode
  }
}
