package cz.bataliongames.firstenchanter.form.controllers.table.duplicator

import javafx.scene.control.Label

/**
 * Created by arnostkuchar on 06.10.14.
 */
object LabelDuplicator {
  def apply(node: Label): Label = {
    val newNode = new Label()
    newNode.setFont(node.getFont)
    newNode.setLayoutX(node.getLayoutX)
    newNode.setLayoutY(node.getLayoutY)
    newNode.setPrefHeight(node.getPrefHeight)
    newNode.setPrefWidth(node.getPrefWidth)
    newNode.setText(node.getText)
    newNode.setAlignment(node.getAlignment)
    newNode.setTextFill(node.getTextFill)
    newNode
  }

  def apply(node: Label, text: String):Label = {
    val newNode = LabelDuplicator(node)
    newNode.setText(text)
    newNode
  }
}
