package cz.bataliongames.firstenchanter.form.controllers.table.duplicator

import javafx.scene.layout.Pane

/**
 * Created by arnostkuchar on 06.10.14.
 */
object PaneDuplicator {
  def apply(node: Pane): Pane = {
    val newNode = new Pane()
    newNode.setPrefHeight(node.getPrefHeight)
    newNode.setPrefHeight(node.getPrefHeight)
    newNode.setLayoutX(node.getLayoutX)
    newNode.setLayoutY(node.getLayoutY)
    newNode
  }
}
