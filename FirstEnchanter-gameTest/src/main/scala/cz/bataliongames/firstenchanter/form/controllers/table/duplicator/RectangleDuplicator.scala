package cz.bataliongames.firstenchanter.form.controllers.table.duplicator

import javafx.scene.shape.Rectangle

/**
 * Created by arnostkuchar on 06.10.14.
 */
object RectangleDuplicator {
  def apply(node: Rectangle): Rectangle = {
    val newNode = new Rectangle()
    newNode.setWidth(node.getWidth)
    newNode.setHeight(node.getHeight)
    newNode.setLayoutX(node.getLayoutX)
    newNode.setLayoutY(node.getLayoutY)
    newNode.setFill(node.getFill)
    newNode
  }
}
