package cz.bataliongames.firstenchanter.form.controllers.table.duplicator

import javafx.scene.control.TextArea

/**
 * Created by arnostkuchar on 06.10.14.
 */
object TextAreaDuplicator {
  def apply(node: TextArea): TextArea = {
    val newNode = new TextArea()
    newNode.setPrefHeight(node.getPrefHeight)
    newNode.setPrefWidth(node.getPrefWidth)
    newNode.setWrapText(node.isWrapText)
    newNode.setLayoutX(node.getLayoutX)
    newNode.setLayoutY(node.getLayoutY)
    newNode.setText(node.getText)
    newNode.setStyle(node.getStyle)
    newNode
  }

  def apply(node: TextArea, text: String): TextArea = {
    val newNode = TextAreaDuplicator(node)
    newNode.setText(text)
    newNode
  }
}
