package cz.bataliongames.firstenchanter.form.controllers.table.duplicator

import javafx.scene.layout.VBox

/**
 * Created by arnostkuchar on 07.10.14.
 */
object VBoxDuplicator {
  def apply(node: VBox): VBox = {
    val newNode = new VBox()
    newNode.setPrefHeight(node.getPrefHeight)
    newNode.setPrefHeight(node.getPrefHeight)
    newNode.setLayoutX(node.getLayoutX)
    newNode.setLayoutY(node.getLayoutY)
    newNode.setAlignment(node.getAlignment)
    newNode
  }
}