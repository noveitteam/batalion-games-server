package cz.bataliongames.firstenchanter.form.decorators

import javafx.scene.paint.{Color, Paint}
import javafx.scene.text.Text

import scalafx.scene.text.{FontWeight, Font}

/**
 * Created by Wlsek on 4.9.2014.
 */
trait StrokeTextDecorator {

  var originalTextStroke: Paint = _

  def onStrokedTextIdle(text: Text) = {
    val originalWidth = text.prefWidth(-1)
    val originalHeight = text.prefHeight(-1)

    text.setFont(Font.font(text.getFont.getFamily,FontWeight.BOLD, text.getFont.getSize - 2))

    val newWidth = text.prefWidth(-1)
    val newHeight = text.prefHeight(-1)

    val widthOffset = (originalWidth - newWidth) / 2
    val heightOffset = (originalHeight - newHeight ) / 2

    text.setX(text.getX + widthOffset)
    text.setY(text.getY - heightOffset)

    text.setStroke(originalTextStroke)
  }

  def onStrokedTextSelected(text: Text) = {
    val originalWidth = text.prefWidth(-1)
    val originalHeight = text.prefHeight(-1)
    originalTextStroke = text.getStroke

    text.setFont(Font.font(text.getFont.getFamily,FontWeight.BOLD, text.getFont.getSize + 2))
    val newWidth = text.prefWidth(-1)
    val newHeight = text.prefHeight(-1)

    val widthOffset = (newWidth - originalWidth) / 2
    val heightOffset = (newHeight - originalHeight) / 2

    text.setX(text.getX - widthOffset)
    text.setY(text.getY + heightOffset)
    text.setStroke(Color.web("#ED9358"))
  }
}
