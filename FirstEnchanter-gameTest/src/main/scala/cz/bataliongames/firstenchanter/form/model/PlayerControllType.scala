package cz.bataliongames.firstenchanter.form.model

import cz.noveit.proto.firstenchanter.FirstEnchanterLightWeightServerLobby.CreateRoom.ControllType

/**
 * Created by Wlsek on 3.9.2014.
 */


object PlayerControllType extends Enumeration {
  val Human, Bot = Value

  def withProto(t: ControllType): PlayerControllType.Value = {
    t match {
      case ControllType.ControllTypeBot => PlayerControllType.Bot
      case ControllType.ControllTypeHuman => PlayerControllType.Human
    }
  }

  def toProto(t: PlayerControllType.Value): ControllType = {
    t match {
      case PlayerControllType.Bot => ControllType.ControllTypeBot
      case PlayerControllType.Human => ControllType.ControllTypeHuman
    }
  }

}
