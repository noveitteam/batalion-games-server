package cz.bataliongames.firstenchanter.form.services

import java.io._
import java.net.URI
import java.util.Properties

import akka.actor.{Actor, ActorLogging}
import com.typesafe.config.{ConfigFactory, Config}


/**
 * Created by Wlsek on 3.9.2014.
 */

case class LoadConfigForForm(formId: String)

trait LoadConfigurationResult

case class LoadedConfiguration(formId: String, config: Properties) extends LoadConfigurationResult

case class NoConfiguration(formId: String) extends LoadConfigurationResult

case class SaveConfigForForm(formId: String, newConfig: Properties)

class ConfigDispatcher extends Actor with ActorLogging {

  val fileUri = "forms"
  new File(fileUri).mkdirs()

  def receive = {
    case LoadConfigForForm(id) =>
      try {
        log.info("Loading info")
        //val config = ConfigFactory.parseFile(new File(fileUri + "/" + id + ".conf"))
        val properties = new Properties()
        properties.load(new FileInputStream(fileUri + "/" + id + ".properties"))
        if (properties.size() > 0) {
          sender ! LoadedConfiguration(id, properties)
        } else {
          sender ! NoConfiguration(id)
        }
      }
      catch {
        case e: FileNotFoundException =>
          log.error("Config file missing, creating new")
          new File(fileUri + "/" + id + ".properties").createNewFile()
          self.tell(LoadConfigForForm(id), sender)

        case t: Throwable =>
          log.error("Error {} while loading properties for id: {}", t.getLocalizedMessage, id)
          sender ! NoConfiguration(id)
      }

    case SaveConfigForForm(id, newConfig) => try {
      println("Saving")
      val file = new File(fileUri + "/" + id + ".properties")
      file.delete()
      file.createNewFile()
      val out = new FileOutputStream(fileUri + "/" + id + ".properties");
      newConfig.store(out, "Form " + id)
      out.close()
    } catch {
      case t: Throwable =>
        log.error("Error {} while saving properties for id: {}", t.getLocalizedMessage, id)
    }

    case t => throw new NotImplementedError("Unknown message: " + t)
  }


}
