package cz.bataliongames.firstenchanter.form.services

/**
 * Created by Wlsek on 3.9.2014.
 */
object FormServices extends Enumeration{
  val ConfigDispatcher, ApplicationController, WebSocketClient = Value
}
