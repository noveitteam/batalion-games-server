package cz.bataliongames.firstenchanter.form.viewers

import java.io.IOException
import java.net.URL
import javafx.embed.swing.JFXPanel
import javafx.{fxml => jfxf}
import javafx.{scene => jfxs}
import akka.actor.{ActorSystem, ActorRef}
import cz.bataliongames.firstenchanter.form.controllers.PlayerSelectionController

import scalafx.application.JFXApp
import scalafx.application.JFXApp.PrimaryStage
import scalafx.scene.Scene
import scalafx.Includes._
import scalafx.stage.{StageStyle, Stage}

/**
 * Created by Wlsek on 3.9.2014.
 */
object PlayerSelectionViewFactory {
  def apply(fd: ActorRef, cd: ActorRef, actorSystem: ActorSystem): Unit = {
    val resource = getClass.getResource("/fxml/PlayerSelection.fxml")
    if (resource == null) {
      throw new IOException("Cannot load resource: PlayerSelection.fxml")
    }

    val loader = new jfxf.FXMLLoader(resource)
    val playerSelectionContainer: jfxs.Parent = loader.load().asInstanceOf[jfxs.Parent]

    val stage = new Stage {
      scene = new Scene(playerSelectionContainer)
    }

    stage.initStyle(StageStyle.UNDECORATED)
    stage.showAndWait()

    // fd ! ChangePresentedState(stage, application)
  }
}
