package cz.bataliongames.firstenchanter.websocketclient

import java.net.URI

import akka.actor.{Actor, ActorRef}
import cz.noveit.games.cardgame.logging.NoActorLogging
import cz.noveit.proto.Messages.MessagesPackage.Message
import cz.noveit.proto.serialization.MessageDeserializer
import io.backchat.hookup._
import org.bouncycastle.util.encoders.Base64

/**
 * Created by arnostkuchar on 15.09.14.
 */

case class ConnectToServerUri(uri: URI)

case class CannotConnectToServerUri(uri: URI)

case class ConnectedToServerUri(uri: URI, context: WebSocketClientContext)

case class ClientContextDestroyed(context: WebSocketClientContext)

case class ClientContextCreated(context: WebSocketClientContext)

class WebSocketClient extends Actor {
  def receive = {
    case ConnectToServerUri(uri) =>
      new WebSocketClientContext(new HookupClientConfig(uri), sender)
  }
}

class WebSocketClientContext(override val settings: HookupClientConfig, responder: ActorRef) extends HookupClient with MessageDeserializer with NoActorLogging {

  override def receive = {
    case Disconnected(_) ⇒
      responder ! ClientContextDestroyed(this)

    case TextMessage(message) ⇒
      val deserialized = deserialize(Message.parseFrom(Base64.decode(message)))
      if (deserialized.isDefined) {
        responder ! deserialized.get
      } else {
        error("Cannot parse message: " + message)
      } //.map(e => registry ! MessageEvent(e.message, e.module, e.replyHash, Some(this)))

  }

  connect() onSuccess {
    case Success ⇒
      responder ! ConnectedToServerUri(settings.getUri, this)
    case Cancelled =>
      responder ! CannotConnectToServerUri(settings.getUri)
    case _ ⇒
  }
}

/*

new DefaultHookupClient(HookupClientConfig(new URI("ws://localhost:8080/thesocket"))) {

import io.backchat.hookup.DefaultHookupClient

import io.backchat.hookup.DefaultHookupClient

def receive = {
case Disconnected(_) ⇒
println("The websocket to " + uri.toASCIIString + " disconnected.")
case TextMessage(message) ⇒ {
println("RECV: " + message)
send("ECHO: " + message)
}
}

connect() onSuccess {
case Success ⇒
println("The websocket is connected to:"+this.uri.toASCIIString+".")
system.scheduler.schedule(0 seconds, 1 second) {
send("message " + messageCounter.incrementAndGet().toString)
}
case _ ⇒
}
}*/