package cz.bataliongames.firstenchanter

import akka.actor.{Props, ActorSystem, ActorContext}
import com.mongodb.ServerAddress
import cz.bataliongames.firstenchanter.deck.CardsAndDeckDatabaseQueryActor
import cz.bataliongames.firstenchanter.dispatcher.LightWeightDispatcher
import cz.bataliongames.firstenchanter.lobby.LobbyController
import cz.bataliongames.firstenchanter.player.PlayerController
import cz.noveit.connector.websockets.{WebSocketsServer, StartWebSocketServer, StopWebSocketServer}
import cz.noveit.database.{DatabaseControllerSettings, DatabaseController}
import cz.noveit.games.cardgame.WorldServices
import cz.noveit.games.cardgame.engine.ConversationController
import cz.noveit.games.cardgame.engine.bots.{BotConversationService, BotPlayerFactory}
import cz.noveit.games.cardgame.engine.game.{MatchResultController, GameEndController}
import cz.noveit.games.cardgame.engine.game.helpers.CardSwappingRequirements
import cz.noveit.games.cardgame.engine.game.session._
import cz.noveit.games.cardgame.engine.handlers.ServiceHandlersModules

import scala.collection.immutable.HashMap
import scala.concurrent.duration.FiniteDuration

import scala.collection.immutable.HashMap
import scala.concurrent.duration.FiniteDuration
import scala.concurrent.duration._

/**
 * Created by arnostkuchar on 12.09.14.
 */
object LightWeightServerStart {
  def apply(as: ActorSystem): Unit = {
    val databaseParameters: HashMap[String, String] = HashMap(
      "databaseName" -> "FirstEnchanter",
      "avatarVersionCollection" -> "avatarsVersion",
      "deckIconVersionCollection" -> "deckIconsVersion",
      "cardSkinVersionCollection" -> "cardSkinsVersion",
      "userCollection" -> "users",
      "cardsCollection" -> "cards",
      "decksCollection" -> "decks",
      "botStatisticCollection" -> "botStatistic",
      "botDetailsCollection" -> "botDetails",
      "conversationsCollection"-> "conversationsCollection",
      "conversationsMembershipCollection" -> "conversationsMembership",
      "conversationsInvitationCollection" -> "conversationsInvitation",
      "conversationsBansCollection" -> "conversationsBans"
    )

    val ms = MatchSetup(HashMap[State, FiniteDuration](
      PLAYER_STARTS_ROLL -> 60.seconds,
      ATTACKER_TEAM_PLAY -> 60.seconds,
      DEFENDER_TEAM_COUNTER_PLAY -> 60.seconds,
      ATTACKER_TEAM_ATTACK -> 60.seconds,
      DEFENDER_TEAM_DEFENDS -> 60.seconds,
      FORTUNE_ROLL -> 60.seconds,
      EVALUATION_AND_ANIMATION -> 60.seconds
    ), 2, 8, 8, "1", 20, CardSwappingRequirements(2, 1))

    val databaseController = as.actorOf(
      Props(
        classOf[DatabaseController],
        DatabaseControllerSettings(List(new ServerAddress("127.0.0.1")))
      ),
      WorldServices.databaseService
    )

    val cardDatabaseAdapterProps = Props(classOf[CardsAndDeckDatabaseQueryActor], databaseParameters)

    as.actorOf(Props[MatchResultController], WorldServices.matchResultController)

    val config = as.settings.config.getConfig("world")
    val ws = as.actorOf(Props(classOf[WebSocketsServer], config), "webSocketServer")
    val pc = as.actorOf(Props(classOf[PlayerController]), "playerController")
    val botFactory = as.actorOf(Props(classOf[BotPlayerFactory], databaseParameters, ms, cardDatabaseAdapterProps))

    val dsp = as.actorOf(Props(classOf[LightWeightDispatcher], HashMap(
      "FirstEnchanterLightWeightServerLobby" -> as.actorOf(Props(classOf[LobbyController], botFactory, databaseParameters, ms, cardDatabaseAdapterProps)),
      ServiceHandlersModules.cardGameMatch -> pc,
      ServiceHandlersModules.cardGamePlayer -> pc,
      ServiceHandlersModules.cardGameConversation -> pc
    )))

   as.actorOf(Props(classOf[ConversationController], dsp, databaseParameters), WorldServices.conversationService)
   as.actorOf(Props[BotConversationService], WorldServices.botConversationService)

    ws ! StartWebSocketServer("FirstEnchanter-LightWeight-server", dsp)
  }
}
