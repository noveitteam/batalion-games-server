package cz.bataliongames.firstenchanter

import akka.actor.ActorSystem
import cz.noveit.connector.websockets.StopWebSocketServer

/**
 * Created by arnostkuchar on 12.09.14.
 */
object LightWeightServerStop {
  def apply(as: ActorSystem) = {
    val ws = as.actorSelection("/user/webSocketServer")
    ws ! StopWebSocketServer
    as.shutdown()
  }
}
