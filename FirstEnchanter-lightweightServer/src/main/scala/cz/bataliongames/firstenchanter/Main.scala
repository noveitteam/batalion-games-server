package cz.bataliongames.firstenchanter

import akka.actor.{Props, ActorSystem}
import cz.noveit.connector.websockets.{StopWebSocketServer, StartWebSocketServer, WebSocketsServer}

/**
 * Created by arnostkuchar on 11.09.14.
 */

object Main extends App {
  override def main(args: Array[String]): Unit = {
    val as = ActorSystem("FirstEnchanter-LightWeight-server")
    LightWeightServerStart(as)
    readLine()
    LightWeightServerStop(as)
  }
}
