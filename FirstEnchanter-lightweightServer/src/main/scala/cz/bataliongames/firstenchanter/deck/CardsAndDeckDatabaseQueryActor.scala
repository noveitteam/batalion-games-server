package cz.bataliongames.firstenchanter.deck

import akka.actor.{ActorLogging, ActorRef, Actor}
import akka.util.Timeout
import com.mongodb.casbah.Imports._
import cz.noveit.database.{ProcessTaskOnCollection, DatabaseResult, ProcessTaskOnCollectionAndReplyToSender, DatabaseCommand}
import cz.noveit.games.cardgame.WorldServices
import cz.noveit.games.cardgame.adapters._
import org.bson.types.ObjectId

import scala.collection.immutable.HashMap
import scala.concurrent.duration._
import akka.pattern.ask
import scala.concurrent.Await

/**
 * Created by arnostkuchar on 19.09.14.
 */
class CardsAndDeckDatabaseQueryActor(databaseParams: HashMap[String, String]) extends Actor with ActorLogging {
  val dbController = context.actorSelection("/user/" + WorldServices.databaseService)
  val databaseName = databaseParams("databaseName")
  val deckCollection = databaseParams("decksCollection")
  val cardCollection = databaseParams("cardsCollection")

  def receive = {
    case dc: DatabaseCommand =>
      dc.command match {
        case GetDeckAndCardsForUserWithGivenDeckId(player, activeDeckId) =>
          val replyTo = sender
          try {
            implicit val timeout = Timeout(15 seconds)
            val future = dbController ? ProcessTaskOnCollectionAndReplyToSender((c: MongoCollection, s: ActorRef) => {
              s ! c.findOne(MongoDBObject("_id" -> new ObjectId(activeDeckId))).map(d => Some(Deck(
                activeDeckId,
                d.getAs[String]("name").getOrElse(""),
                d.getAs[Int]("iconId").getOrElse(-1),
                d.getAs[BasicDBObject]("elementsRating").map(obj => ElementsRating(
                  obj.getAs[Double]("water").getOrElse(-1.0).toFloat,
                  obj.getAs[Double]("fire").getOrElse(-1.0).toFloat,
                  obj.getAs[Double]("earth").getOrElse(-1.0).toFloat,
                  obj.getAs[Double]("air").getOrElse(-1.0).toFloat,
                  obj.getAs[Double]("neutral").getOrElse(-1.0).toFloat
                )).getOrElse(ElementsRating(-1, -1, -1, -1, -1)),
                d.getAs[MongoDBList]("cards").getOrElse(MongoDBList(0)).toList.collect {
                  case s: BasicDBObject => s
                }.map(o => CountableCardWithSkin(o.getAs[String]("cardId").getOrElse(""), o.getAs[Int]("count").getOrElse(-1), None)).toList,
                player
              ))
              ).getOrElse(None)

            }, databaseName, deckCollection)

            val deck = Await.result(future, timeout.duration).asInstanceOf[Option[Deck]]
            if (deck.isEmpty) {
              replyTo ! DatabaseResult(DeckAndCardsForUserWithGivenDeckId(None, List(), player, activeDeckId), dc.replyHash)
            } else {
              deck.map(deck => {
                dbController ! ProcessTaskOnCollection((c: MongoCollection) => {
                  replyTo ! DatabaseResult(
                    DeckAndCardsForUserWithGivenDeckId(
                      Some(deck),
                      c.find(MongoDBObject("_id" -> MongoDBObject("$in" -> deck.cards.map(cws => cws.cardId).toArray))).map(found =>
                        Card(
                          found.getAs[String]("_id").getOrElse(""),
                          found.getAs[String]("name").getOrElse(""),
                          found.getAs[Int]("rarity").getOrElse(-1),
                          found.getAs[Int]("attack").getOrElse(-1),
                          found.getAs[Int]("defense").getOrElse(-1),
                          found.getAs[String]("version").getOrElse(""),
                          found.getAs[Int]("imageId").getOrElse(-1),
                          found.getAs[MongoDBList]("price").getOrElse(MongoDBList()).toList.collect {
                            case o: BasicDBObject => o
                          }
                            .map(p => CardPrice(p.getAs[Int]("element").get, p.getAs[Int]("price").get)).toList,
                          found.getAs[MongoDBList]("tags").getOrElse(MongoDBList()).toList.collect {
                            case s: String => s.toLowerCase
                          },
                          found.getAs[MongoDBList]("description").getOrElse(MongoDBList()).toList.collect {
                            case o: BasicDBObject => o
                          }
                            .map(p => Description(p.getAs[String]("locale").get, p.getAs[String]("description").get)).toList,
                          CardTargetingType.withName(found.getAs[String]("targetingType").getOrElse("None"))
                        )
                      ).toList,
                      player,
                      activeDeckId
                    ), dc.replyHash)

                }, databaseName, cardCollection)

              })
            }
          } catch {
            case t: Throwable =>
              log.error("Error while getting deck and cards for user {} and deck id {}.", player, activeDeckId)
              replyTo ! DatabaseResult(DeckAndCardsForUserWithGivenDeckId(None, List(), player, activeDeckId), dc.replyHash)
          }
      }
  }
}
