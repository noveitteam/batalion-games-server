package cz.bataliongames.firstenchanter.deck

import akka.actor.Actor
import cz.noveit.database.ProcessTaskOnCollection
import cz.noveit.games.cardgame.WorldServices

import scala.collection.immutable.HashMap
import com.mongodb.casbah.Imports._

/**
 * Created by arnostkuchar on 18.09.14.
 */

case object GetAllStartingDeckIds

case class AllStartingDeckIds(ids: List[String])

class DeckQueryActor(params: HashMap[String, String]) extends Actor {
  val dbController = context.actorSelection("/user/" + WorldServices.databaseService)

  def receive = {
    case GetAllStartingDeckIds =>
      val receiver = sender
      val dqa = self

      dbController ! ProcessTaskOnCollection(
        (c: MongoCollection) => {
          receiver ! AllStartingDeckIds(c.find(MongoDBObject("startingDeck" -> true)).map(found => found.getAs[ObjectId]("_id").map(id => id.toString).getOrElse("")).toList)
          context.stop(dqa)
        }, params("databaseName"), params("decksCollection"))
  }
}
