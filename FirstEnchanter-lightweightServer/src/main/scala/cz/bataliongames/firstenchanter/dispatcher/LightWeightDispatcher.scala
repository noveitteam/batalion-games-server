package cz.bataliongames.firstenchanter.dispatcher

import akka.actor.{Actor, ActorLogging, ActorRef}
import cz.noveit.games.cardgame.engine.{CrossHandlerMessage, ServiceModuleMessage}
import cz.noveit.games.cluster.dispatch.{DispatchServiceBroadcast, DispatchMessageBroadcast, ClusterMessageEvent}
import cz.noveit.proto.serialization.MessageEvent
import cz.noveit.service.ContextDestroy

import scala.collection.immutable.HashMap

/**
 * Created by arnostkuchar on 12.09.14.
 */
class LightWeightDispatcher(val handlers: HashMap[String, ActorRef]) extends Actor with ActorLogging {
  def receive = {
    case msg: DispatchMessageBroadcast =>
      handlers(msg.e.moduleName) ! msg

    case msg: DispatchServiceBroadcast =>
      context.actorSelection("/user/" + msg.e.service) ! msg.e

    case e: MessageEvent =>
       handlers(e.module).tell(e, sender)

    case e: ContextDestroy =>
       handlers.map(h => h._2.tell(e, sender))
  }
}
