package cz.bataliongames.firstenchanter.lobby

import akka.actor.{Props, ActorRef, Actor, ActorLogging}
import akka.util.Timeout
import cz.bataliongames.firstenchanter.deck.{AllStartingDeckIds, GetAllStartingDeckIds, DeckQueryActor}
import cz.bataliongames.firstenchanter.player.PlayerHandler
import cz.noveit.connector.websockets.WebSocketsContext
import cz.noveit.games.cardgame.WorldServices
import cz.noveit.games.cardgame.adapters.User
import cz.noveit.games.cardgame.engine.bots.{BotPlayerHolder, GetBot}
import cz.noveit.games.cardgame.engine.game.session._
import cz.noveit.games.cardgame.engine.game._
import cz.noveit.games.cardgame.engine.player.{PlayerPlaysWithActiveDeck, PlayerIsTryingToFindGame}
import cz.noveit.proto.firstenchanter.FirstEnchanterLightWeightServerLobby.CreateRoom.ControllType
import cz.noveit.proto.firstenchanter.FirstEnchanterLightWeightServerLobby._
import cz.noveit.proto.serialization.{MessageSerializer, MessageEvent}
import cz.noveit.service.ContextDestroy
import org.bouncycastle.util.encoders.Base64
import collection.JavaConverters._
import scala.collection.immutable.HashMap
import scala.concurrent.Await

import scala.concurrent.duration._
import akka.pattern.ask

import scala.util.Random

/**
 * Created by arnostkuchar on 11.09.14.
 */


case class PositionSetup(position: Symbol, exptected: ControllType)

case class Player(context: Option[WebSocketsContext], nickname: String)

case class RoomMember(team: Symbol, position: Symbol, player: Player, handler: Option[ActorRef], bot: Boolean = false)

case class Room(name: String, createdBy: Player, teams: List[Symbol], members: List[RoomMember], positionSetups: List[PositionSetup])

class LobbyController(botFactory: ActorRef, databaseParams: HashMap[String, String], matchSetup: MatchSetup, cardDatabaseProps: Props) extends Actor with ActorLogging with MessageSerializer {
  val module = "FirstEnchanterLightWeightServerLobby"
  private var rooms: List[Room] = List()
  private var listeners: List[WebSocketsContext] = List()

  val teamsForPosition = HashMap(
    'position1 -> 'team1,
    'position2 -> 'team2,
    'position3 -> 'team1,
    'position4 -> 'team2
  )

  def receive = {
    case e: MessageEvent =>
      e.message match {
        case msg: GetRoomList =>
          if (listeners.find(l => l == e.context.get.asInstanceOf[WebSocketsContext]).isEmpty) {
            listeners = e.context.get.asInstanceOf[WebSocketsContext] :: listeners
          }

          e.context.map(c => c.asInstanceOf[WebSocketsContext].send(
            new String(Base64.encode(serializeToMessage(MessageEvent(
              RoomList.newBuilder().addAllRooms(rooms.map(r => r.name).asJava).build(),
              module,
              e.replyHash,
              e.context
            )).toByteArray), "UTF-8")
          ))

        case msg: CreateRoom =>
          rooms.find(r => r.createdBy.context.get == e.context.get).map(r => {
            r.members.map(p => {
              p.player.context.map(c => c.send(
                new String(Base64.encode(serializeToMessage(MessageEvent(
                  RemovedRoom.newBuilder().setName(r.name).build,
                  module,
                  e.replyHash,
                  e.context
                )).toByteArray), "UTF-8"
                )))
            })

            rooms = rooms.filter(room => room.name == r.name)
          })

          if (rooms.find(r => r.createdBy.nickname == msg.getName).isDefined) {
            e.context.map(c => c.asInstanceOf[WebSocketsContext].send(
              new String(Base64.encode(serializeToMessage(MessageEvent(
                CreateRoomFailed.newBuilder().build(),
                module,
                e.replyHash,
                e.context
              )).toByteArray), "UTF-8"
              )))
          } else {
            e.context.map(c => {
              var positions: List[PositionSetup] = List()
              if (msg.hasPosition1) {
                positions = PositionSetup('position1, msg.getPosition1) :: positions
              }

              if (msg.hasPosition2) {
                positions = PositionSetup('position2, msg.getPosition2) :: positions
              }


              if (msg.hasPosition3) {
                positions = PositionSetup('position3, msg.getPosition3) :: positions
              }


              if (msg.hasPosition4) {
                positions = PositionSetup('position4, msg.getPosition4) :: positions
              }

              val botPositions = positions.filter(p => p.exptected == ControllType.ControllTypeBot)
              var bots: List[BotPlayerHolder] = List()

              var numberOfTries = 0


              implicit val timeout = Timeout(5 seconds)
              while (bots.size < botPositions.size && numberOfTries < 100) {
                val future = botFactory ? GetBot(1500)
                val botPlayer = Await.result(future, 5.seconds).asInstanceOf[BotPlayerHolder]
                if (bots.find(b => b.name == botPlayer.name).isEmpty) {
                  bots = botPlayer :: bots
                }
                numberOfTries += 1
              }

              val room = Room(msg.getName, Player(Some(c.asInstanceOf[WebSocketsContext]), msg.getName), List('team1, 'team2),
                bots.zipWithIndex.map(b => {
                  val position = botPositions(b._2).position
                  RoomMember(teamsForPosition(position), position, Player(None, b._1.name), Some(b._1.handler), true)
                })
                , positions)
              rooms = room :: rooms

              c.asInstanceOf[WebSocketsContext].send(
                new String(Base64.encode(serializeToMessage(MessageEvent(
                  CreateRoomSuccess.newBuilder().addAllTeams(room.teams.map(t => t.toString().drop(1)).asJava).build(),
                  module,
                  e.replyHash,
                  e.context
                )).toByteArray), "UTF-8"
                ))

              val allRooms = RoomList.newBuilder().addAllRooms(rooms.map(r => r.name).asJava).build()
              listeners.map(l => {
                l.send(
                  new String(Base64.encode(serializeToMessage(MessageEvent(
                    allRooms,
                    module,
                    e.replyHash,
                    e.context
                  )).toByteArray), "UTF-8")
                )
              })
            }
            )
          }

        case msg: JoinRoom =>
          val room = rooms.find(r => r.name == msg.getRoomName)

          if (room.isEmpty) {
            e.context.map(c => {
              c.asInstanceOf[WebSocketsContext].send(
                new String(Base64.encode(serializeToMessage(MessageEvent(
                  JoinRoomFailed.newBuilder().build(),
                  module,
                  e.replyHash,
                  e.context
                )).toByteArray), "UTF-8"
                ))
            })
          } else {
            room.map(r => {
              val positionsForHuman = r.positionSetups.filter(s => s.exptected == ControllType.ControllTypeHuman && r.members.find(m => m.position == s.position).isEmpty)
              if (positionsForHuman.isEmpty) {
                e.context.map(c => {
                  c.asInstanceOf[WebSocketsContext].send(
                    new String(Base64.encode(serializeToMessage(MessageEvent(
                      JoinRoomFailed.newBuilder().build(),
                      module,
                      e.replyHash,
                      e.context
                    )).toByteArray), "UTF-8"
                    ))
                })
              } else {
                rooms = r
                  .copy(members = RoomMember(
                  teamsForPosition(positionsForHuman.head.position),
                  positionsForHuman.head.position,
                  Player
                    (Some(e.context.get.asInstanceOf[WebSocketsContext]),
                        msg.getPlayerName
                      ), None) :: r.members) :: rooms.filter(room => room.name != r.name)

                val newRoom = rooms.find(rf => rf.name == r.name).get

                val readyToStart = positionsForHuman.size - 1 == 0

                val possibleTeams = r.positionSetups.size match {
                  case 2 =>
                    var teams: List[Symbol] = List()
                    if (newRoom.members.filter(m => m.team == 'team1).size == 0) teams = 'team1 :: teams
                    if (newRoom.members.filter(m => m.team == 'team2).size == 0) teams = 'team2 :: teams
                    teams
                  case 4 =>
                    var teams: List[Symbol] = List()
                    if (newRoom.members.filter(m => m.team == 'team1).size <= 1) teams = 'team1 :: teams
                    if (newRoom.members.filter(m => m.team == 'team2).size <= 1) teams = 'team2 :: teams
                    teams
                }

                e.context.map(c => {
                  c.asInstanceOf[WebSocketsContext].send(
                    new String(Base64.encode(serializeToMessage(MessageEvent({
                      val builder = JoinRoomSuccessful.newBuilder()

                      newRoom.positionSetups.find(p => p.position == 'position1).map(found => builder.setRoomPosition1(found.exptected))
                      newRoom.positionSetups.find(p => p.position == 'position2).map(found => builder.setRoomPosition2(found.exptected))
                      newRoom.positionSetups.find(p => p.position == 'position3).map(found => builder.setRoomPosition3(found.exptected))
                      newRoom.positionSetups.find(p => p.position == 'position4).map(found => builder.setRoomPosition4(found.exptected))

                      builder.setJoinedTeam(teamsForPosition(positionsForHuman.head.position).toString().drop(1))
                      builder.setReadyToStart(readyToStart)
                      builder.setPosition(positionsForHuman.head.position.toString.drop(1))
                      builder.addAllTeams(teamsForPosition.values.map(s => s.toString().drop(1)).asJava)
                      builder.setRoomName(msg.getRoomName)

                      builder.addAllPossibleToChangeTeam(possibleTeams.map(t => t.toString().drop(1)).asJava)
                      builder.setIsCreator(r.createdBy.context.get == e.context.get)

                      builder.build()
                    },
                    module,
                    e.replyHash,
                    e.context
                    )).toByteArray), "UTF-8"
                    ))

                  r.members.filter(f => if (f.player.context.isDefined) {
                    f.player.context.get != e.context.get
                  } else {
                    true
                  }).map(pj => {

                    c.asInstanceOf[WebSocketsContext].send(
                      new String(Base64.encode(serializeToMessage(MessageEvent(
                        PlayerJoinedRoom
                          .newBuilder()
                          .setJoinedTeam(teamsForPosition(pj.position).toString().drop(1))
                          .setReadyToStart(readyToStart)
                          .setPosition(pj.position.toString.drop(1))
                          .setNickName(pj.player.nickname)
                          .addAllPossibleToChangeTeam(possibleTeams.map(t => t.toString().drop(1)).asJava)
                          .build(),
                        module,
                        e.replyHash,
                        e.context
                      )).toByteArray), "UTF-8"
                      ))
                  })
                })

                listeners.filter(l => l != e.context.get).map(l => {
                  l.send(
                    new String(Base64.encode(serializeToMessage(MessageEvent(
                      PlayerJoinedRoom
                        .newBuilder()
                        .setJoinedTeam(teamsForPosition(positionsForHuman.head.position).toString().drop(1))
                        .setReadyToStart(readyToStart)
                        .setPosition(positionsForHuman.head.position.toString.drop(1))
                        .addAllPossibleToChangeTeam(possibleTeams.map(t => t.toString().drop(1)).asJava)
                        .setNickName(msg.getPlayerName)
                        .build(),
                      module,
                      e.replyHash,
                      e.context
                    )).toByteArray), "UTF-8")
                  )
                })
              }
            })
          }

        case msg: RemoveRoom =>
          val room = rooms.find(r => r.createdBy.context.get == e.context.get)
          if (room.isEmpty) {
            e.context.map(c => c.asInstanceOf[WebSocketsContext].send(
              new String(Base64.encode(serializeToMessage(MessageEvent(
                RemoveRoomFailed.newBuilder().build,
                module,
                e.replyHash,
                e.context
              )).toByteArray), "UTF-8"
              )))
          } else {
            if (room.get.createdBy.context.get != e.context.get) {
              e.context.map(c => c.asInstanceOf[WebSocketsContext].send(
                new String(Base64.encode(serializeToMessage(MessageEvent(
                  RemoveRoomFailed.newBuilder().build,
                  module,
                  e.replyHash,
                  e.context
                )).toByteArray), "UTF-8"
                )))
            } else {
              room.map(r => {
                r.members.map(p => {
                  p.player.context.map(c => c.send(
                    new String(Base64.encode(serializeToMessage(MessageEvent(
                      RemovedRoom.newBuilder().setName(r.name).build,
                      module,
                      e.replyHash,
                      e.context
                    )).toByteArray), "UTF-8"
                    )))
                })

                rooms = rooms.filter(room => room.name == r.name)
              })
            }
          }

        case msg: LeaveRoom =>
          val room = rooms.find(r => r.members.exists(m => m.player.context.exists(c => c == e.context.get)))
          if (room.isEmpty) {
            e.context.map(c => c.asInstanceOf[WebSocketsContext].send(
              new String(Base64.encode(serializeToMessage(MessageEvent(
                LeftRoomFailed.newBuilder().build,
                module,
                e.replyHash,
                e.context
              )).toByteArray), "UTF-8"
              )))
          } else {
            if (room.get.createdBy.context.get == e.context.get) {
              room.map(r => {
                r.members.map(p => {
                  p.player.context.map(c => c.send(
                    new String(Base64.encode(serializeToMessage(MessageEvent(
                      RemovedRoom.newBuilder().setName(r.name).build,
                      module,
                      e.replyHash,
                      e.context
                    )).toByteArray), "UTF-8"
                    )))
                })

                rooms = rooms.filterNot(room => room.name == r.name)
              })
            } else {
              room.map(r => {
                rooms = r.copy(members = r.members.filterNot(m => if (m.player.context.isDefined) {
                  m.player.context.get == e.context.get
                } else {
                  false
                })) :: rooms.filterNot(room => room.name == r.name)
                val newRoom = rooms.find(rf => rf.name == r.name).get
                val positionsForHuman = r.positionSetups.filter(s => s.exptected == ControllType.ControllTypeHuman && r.members.find(m => m.position == s.position).isEmpty)

                val readyToStart = positionsForHuman.size - 1 == 0

                val possibleTeams = r.positionSetups.size match {
                  case 2 =>
                    var teams: List[Symbol] = List()
                    if (newRoom.members.filter(m => m.team == 'team1).size == 0) teams = 'team1 :: teams
                    if (newRoom.members.filter(m => m.team == 'team2).size == 0) teams = 'team2 :: teams
                    teams
                  case 4 =>
                    var teams: List[Symbol] = List()
                    if (newRoom.members.filter(m => m.team == 'team1).size <= 1) teams = 'team1 :: teams
                    if (newRoom.members.filter(m => m.team == 'team2).size <= 1) teams = 'team2 :: teams
                    teams
                }

                val player = r.members.find(p => if (p.player.context.isDefined) {
                  p.player.context.get == e.context.get
                } else {
                  false
                }).get
                (player :: r.members).map(p => {
                  p.player.context.map(c => c.send(
                    new String(Base64.encode(serializeToMessage(MessageEvent(
                      PlayerLeftRoom
                        .newBuilder()
                        .setName(player.player.nickname)
                        .setReadyToStart(readyToStart)
                        .addAllPossibleToChangeTeam(possibleTeams.map(pt => pt.toString().drop(1)).asJava)
                        .build,
                      module,
                      e.replyHash,
                      e.context
                    )).toByteArray), "UTF-8"
                    )))
                })


              })
            }
          }

        case msg: PlayerChangeTeamInRoom =>
          rooms.find(r => r.members.exists(m => if (m.player.context.isDefined) {
            m.player.context.get == e.context.get
          } else {
            false
          })).map(room => {
            val possitions = room.positionSetups.size match {
              case 2 =>
                val forbiddenPositions = room.members.filter(m => m.team == Symbol(msg.getTeam)).map(m => m.position)
                teamsForPosition.filter(f => f._1 == 'position1 || f._1 == 'position2).filter(tfp => tfp._2 == Symbol(msg.getTeam) && !forbiddenPositions.contains(tfp._1))
              case 4 =>
                val forbiddenPositions = room.members.filter(m => m.team == Symbol(msg.getTeam)).map(m => m.position)
                teamsForPosition.filter(tfp => tfp._2 == Symbol(msg.getTeam) && !forbiddenPositions.contains(tfp._1))
            }

            room.members.find(m => if (m.player.context.isDefined) {
              m.player.context.get == e.context.get
            } else {
              false
            }).map(member => {
              if (possitions.headOption.isDefined) {
                rooms = room.copy(
                  members = member.copy(team = Symbol(msg.getTeam), position = possitions.head._1) :: room.members.filterNot(m => m == member)
                ) :: rooms.filterNot(r => r == room)

                room.members.map(m => {
                  if (m.player.context.isDefined) m.player.context.get.send(
                    new String(Base64.encode(serializeToMessage(MessageEvent(
                      PlayerChangedTeamInRoom
                        .newBuilder()
                        .setPosition(possitions.head._1.toString().drop(1))
                        .setPlayer(member.player.nickname)
                        .build,
                      module,
                      e.replyHash,
                      e.context
                    )).toByteArray), "UTF-8"
                    )
                  )
                })

              }
            })


          })


        case msg: GameStart => try {
          val room = rooms.find(r => r.createdBy.context.get == e.context.get).get

          context actorOf(Props(
            classOf[MatchResultController]
          ), WorldServices.matchResultController)

          val teams = teamsForPosition.values.toList
          val attackerTeam = teams(Random.nextInt(teams.size))
          val defenderTeam = teams.find(t => t != attackerTeam).get

          val playerPositions =
            room.members.filter(m => m.team == attackerTeam).map(a => StartingRollPosition(a.player.nickname, PLAYER_ROLL_POSITION_ATTACKER)) :::
              room.members.filter(m => m.team == defenderTeam).map(a => StartingRollPosition(a.player.nickname, PLAYER_ROLL_POSITION_DEFENDER))

          val players = room.members.map(m =>
            PlayerIsTryingToFindGame(m.player.nickname, m.team.toString(), 0, 1500, 0, 0,
              if (m.handler.isDefined) {
                m.handler.get
              } else {
                context.actorOf(Props(classOf[PlayerHandler], m.player.context.get, User(m.player.nickname, "", "", 0L, None, None), databaseParams, cardDatabaseProps))
              },
              m.bot
            )
          )


          implicit val timeout = Timeout(5 seconds)
          val asking = context.actorOf(Props(classOf[DeckQueryActor], databaseParams)) ? GetAllStartingDeckIds
          val decks = Await.result(asking, 10.seconds).asInstanceOf[AllStartingDeckIds].ids

          val playerDecks = players.map(p => PlayerPlaysWithActiveDeck(p.player, decks(Random.nextInt(decks.size))))

          val startSetup = MatchQuerySetup(players, None, MatchGameModePractice, {
           if(room.members.exists(m => m.player.context.isEmpty)){
              MatchGameEnemyModePVE
            } else {
              MatchGameEnemyModePVP
            }
          }, {
            room.members.size match {
              case 2 => MatchTeamModeOneVsOne
              case 4 => MatchTeamModeTwoVsTwo
            }
          }, self)


          context.actorOf(Props(classOf[Match], startSetup, matchSetup, playerPositions, playerDecks))
          room.members.map(m =>
            m.player.context.map(ctx => {
              ctx.asInstanceOf[WebSocketsContext].send(
                new String(Base64.encode(serializeToMessage(MessageEvent(
                  GameStarted
                    .newBuilder()
                    .build,
                  module,
                  e.replyHash,
                  e.context
                )).toByteArray), "UTF-8"
                )
              )
            })
          )

          rooms = rooms.filter(r => r != room)
        } catch {
          case t: Throwable =>
            e.context.map(ctx => {
              ctx.asInstanceOf[WebSocketsContext].send(
                new String(Base64.encode(serializeToMessage(MessageEvent(
                  GameStartFailed
                    .newBuilder()
                    .build,
                  module,
                  e.replyHash,
                  e.context
                )).toByteArray), "UTF-8"
                )
              )
            })
        }


      }

    case ctx: ContextDestroy =>
  }
}
