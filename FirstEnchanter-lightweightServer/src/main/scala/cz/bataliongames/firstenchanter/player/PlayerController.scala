package cz.bataliongames.firstenchanter.player

import akka.actor.{ActorRef, Actor, ActorLogging}
import cz.noveit.connector.websockets.WebSocketsContext
import cz.noveit.games.cluster.dispatch.DispatchMessageBroadcast
import cz.noveit.proto.serialization.MessageEvent

/**
 * Created by arnostkuchar on 01.10.14.
 */

case class RegisterPlayer(webContext: WebSocketsContext, playerHandler: ActorRef)
case class DeregisterPlayer(webContext: WebSocketsContext)

class PlayerController extends Actor with ActorLogging {

  private var players: List[Pair[WebSocketsContext, ActorRef]] = List()

  def receive = {
    case RegisterPlayer(wCTX, ar) =>
      players = wCTX -> ar :: players
    case DeregisterPlayer(wCTX) =>
      players = players.filter(p => p._1 != wCTX)
    case msg: DispatchMessageBroadcast =>
      players.map(p => p._2 ! msg.e)

    case e: MessageEvent => e.context.map(ctx => {
      players.find(p => p._1 == ctx).map(found => {
        found._2 ! e
      })
    })
  }
}
