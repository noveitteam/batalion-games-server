package cz.bataliongames.firstenchanter.player

import akka.actor.{ActorLogging, ActorRef, Props, Actor}
import cz.noveit.connector.websockets.WebSocketsContext
import cz.noveit.games.cardgame.adapters.User
import cz.noveit.games.cardgame.engine.{BroadcastMessageBetweenPlayersWithServiceModule, CrossHandlerMessage, ServiceModuleMessage}
import cz.noveit.games.cardgame.engine.handlers._
import cz.noveit.games.cluster.dispatch.ClusterMessageEvent
import cz.noveit.proto.serialization.MessageEvent

import scala.collection.immutable.HashMap

/**
 * Created by arnostkuchar on 18.09.14.
 */
class PlayerHandler(webContext: WebSocketsContext, player: User, databaseParams: HashMap[String, String], cardDatabaseAdapterProps: Props) extends Actor with ActorLogging{

  val matchHandler = context.actorOf(
    Props(classOf[MatchHandlerActor], ServiceHandlersModules.cardGameMatch, PlayerServiceParameters(
      webContext,
      self,
      player,
      databaseParams
    ))
  )

  val conversationHandler =  context.actorOf(
    Props(classOf[ConversationHandlerActor], ServiceHandlersModules.cardGameConversation, PlayerServiceParameters(
      webContext,
      self,
      player,
      databaseParams
    ))
  )

  val playerController = context.actorSelection("/user/playerController")
  playerController ! RegisterPlayer(webContext, self)

  val cardDatabaseAdapter = context.actorOf(cardDatabaseAdapterProps)

  def receive = {
    case msg: ClusterMessageEvent =>
      msg.message match {
        case e: BroadcastMessageBetweenPlayersWithServiceModule =>
          e.serviceHandlerModule match {
            case ServiceHandlersModules.cardGameConversation =>
              conversationHandler ! ServiceModuleMessage(e, e.serviceHandlerModule, msg.replyHash)
            case t =>
          }
        case t =>
      }

    case msg: ServiceModuleMessage =>
     msg.module match {
      case ServiceHandlersModules.cardGameMatch =>
        matchHandler.tell(msg, sender)

      case ServiceHandlersModules.cardGameCards =>
        msg.message match {
          case GetCardDatabaseAdapter =>
            sender ! CardDatabaseAdapterFound(cardDatabaseAdapter)
        }
      case t =>
         log.error("Unknown message ")
    }

    case e: MessageEvent =>
      e.module match {
        case ServiceHandlersModules.cardGameMatch => matchHandler ! e
        case ServiceHandlersModules.cardGameConversation   => conversationHandler ! e
        case t =>
      }

    case t =>
      log.error("Unknown message {}", t)
  }


}
