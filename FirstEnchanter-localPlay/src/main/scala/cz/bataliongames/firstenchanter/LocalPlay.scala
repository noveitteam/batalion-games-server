package cz.bataliongames.firstenchanter

import javafx.embed.swing.JFXPanel

import akka.actor.{Props, ActorSystem}
import cz.bataliongames.firstenchanter.form.services.{ConfigDispatcher, FormServices}
import cz.bataliongames.firstenchanter.websocketclient.WebSocketClient
import cz.noveit.games.cardgame.engine.bots.BotPlayerFactory
import cz.noveit.games.cardgame.engine.game.helpers.CardSwappingRequirements
import cz.noveit.games.cardgame.engine.game.session.MatchSetup

import scala.collection.immutable.HashMap
import scala.concurrent.duration.FiniteDuration
import scala.concurrent.duration._

/**
 * Created by arnostkuchar on 17.09.14.
 */
object LocalPlay extends App {
  override def main(args: Array[String]) = {

    val as = ActorSystem("FirstEnchanter-LocalPlay")
    LightWeightServerStart(as)
    Thread.sleep(2000)
    as.actorOf( Props[ConfigDispatcher], FormServices.ConfigDispatcher.toString)
    as.actorOf(Props[WebSocketClient], FormServices.WebSocketClient.toString)
    val controller = as.actorOf(Props[ApplicationController],  FormServices.ApplicationController.toString)

    controller! StartApplicationInstance
  //  controller! StartApplicationInstance
    //controller! StartApplicationInstance
    new JFXPanel()
/*
   implicit val timeout = Timeout(15 seconds)

      val future = botFactory ? GetBot(1500)
      val botPlayer = Await.result(future, timeout.duration).asInstanceOf[BotPlayerHolder]

      var future2 = botFactory ? GetBot(1500)

      debug("Bots created, polishing them.")

      val botPlayer2 = {
        var tBot = ""
        var tBotHolder: BotPlayerHolder = null
        while (tBot == "" || tBot == botPlayer.name) {
          log.debug("Name for bot2 taken or inappropriate, choosing another.")
          future2 = botFactory ? GetBot(1500)
          tBotHolder = Await.result(future2, timeout.duration).asInstanceOf[BotPlayerHolder]
          tBot = tBotHolder.name
        }

        tBotHolder
      }

      debug("Creating startup setup.")

      val playerPositions = StartingRollPosition(botPlayer.name, PLAYER_ROLL_POSITION_DEFENDER) ::
        StartingRollPosition(botPlayer2.name, PLAYER_ROLL_POSITION_ATTACKER) ::
        Nil

      val bots = PlayerIsTryingToFindGame(botPlayer.name, "botTeam1", botPlayer.avatarId, 1500, 0, botPlayer.challengeId, botPlayer.handler) ::
        PlayerIsTryingToFindGame(botPlayer2.name, "botTeam2", botPlayer2.avatarId, 1500, 0, botPlayer2.challengeId, botPlayer2.handler) ::
        Nil

      val decs = PlayerPlaysWithActiveDeck(botPlayer.name, botPlayer.deckId) ::
        PlayerPlaysWithActiveDeck(botPlayer2.name, botPlayer2.deckId) ::
        Nil

      //Match creation
      log.debug("Creating match")
      val matchCreator = actorSystem.actorOf(Props[MatchCreator])
      val startSetup = MatchQuerySetup(bots, None, MatchGameModePractice, MatchGameEnemyModePVE, MatchTeamModeOneVsOne, matchCreator)

      actorSystem.actorOf(Props(classOf[Match], startSetup, ms, playerPositions, decs))
 */
  }
}
