package cz.noveit.games.print

import javax.imageio.ImageIO
import java.net.URL
import java.io.File
import java.awt.{Color, Graphics2D}
import java.awt.font.{TextLayout, LineBreakMeasurer}
import java.text.AttributedString

/**
 * Created by Wlsek on 20.6.14.
 */
object Config {
  val mongoServer = "127.0.0.1"
  val mongoPort = 27011
  val cardDatabase = "FirstEnchanter"
  val cardsCollection = "cards"
  val decksCollection = "decks"
}

object Main extends App {
 // PrintableCardsController.printDeck("Fire deck")
  PrintableCardsController.printAll



}
