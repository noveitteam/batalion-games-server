package cz.noveit.games.print

import com.mongodb.casbah.Imports._
import java.io.{FileOutputStream, File}
import javax.imageio.ImageIO
import java.text.AttributedString
import java.awt.font.LineBreakMeasurer
import java.awt.{Font, RenderingHints, Color}
import org.apache.pdfbox.pdmodel.{PDPage, PDDocument}
import org.apache.pdfbox.pdmodel.graphics.xobject.{PDPixelMap, PDJpeg}
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream
import java.awt.image.BufferedImage

/**
 * Created by Wlsek on 20.6.14.
 */
object PrintableCardsController {
  def getCards(cards: List[String]): List[Card] = {

    val collection = MongoConnection(Config.mongoServer)(Config.cardDatabase)(Config.cardsCollection)

   collection.find(MongoDBObject("_id" -> MongoDBObject("$in" -> cards.toArray))).map(found => {
      Card(
        found.getAs[String]("_id").getOrElse(""),
        found.getAs[String]("name").getOrElse(""),
        found.getAs[Int]("rarity").getOrElse(-1),
        found.getAs[Int]("attack").getOrElse(-1),
        found.getAs[Int]("defense").getOrElse(-1),
        found.getAs[String]("version").getOrElse(""),
        found.getAs[Int]("imageId").getOrElse(-1),
        found.getAs[MongoDBList]("price").getOrElse(MongoDBList()).collect {
          case o: BasicDBObject => o
        }
          .map(p => CardPrice(p.getAs[Int]("element").get, p.getAs[Int]("price").get)).toList,
        found.getAs[MongoDBList]("tags").getOrElse(MongoDBList()).toList.collect {
          case s: String => s
        },
        found.getAs[MongoDBList]("description").getOrElse(MongoDBList()).collect {
          case o: BasicDBObject => o
        }
          .map(p => Description(p.getAs[String]("locale").get, p.getAs[String]("description").get)).toList
      )
    }).toList
  }

  def printAll {

    val collection = MongoConnection(Config.mongoServer)(Config.cardDatabase)(Config.cardsCollection)

    val foundCards = collection.find(MongoDBObject()).map(found => {
      Card(
        found.getAs[String]("_id").getOrElse(""),
        found.getAs[String]("name").getOrElse(""),
        found.getAs[Int]("rarity").getOrElse(-1),
        found.getAs[Int]("attack").getOrElse(-1),
        found.getAs[Int]("defense").getOrElse(-1),
        found.getAs[String]("version").getOrElse(""),
        found.getAs[Int]("imageId").getOrElse(-1),
        found.getAs[MongoDBList]("price").getOrElse(MongoDBList()).collect {
          case o: BasicDBObject => o
        }
          .map(p => CardPrice(p.getAs[Int]("element").get, p.getAs[Int]("price").get)).toList,
        found.getAs[MongoDBList]("tags").getOrElse(MongoDBList()).toList.collect {
          case s: String => s
        },
        found.getAs[MongoDBList]("description").getOrElse(MongoDBList()).collect {
          case o: BasicDBObject => o
        }
          .map(p => Description(p.getAs[String]("locale").get, p.getAs[String]("description").get)).toList
      )
    }).toList

    makePdfWithCards("all", foundCards)
  }

  def printDeck(deck: String) {

    var printableCards: List[Card] = List()

    val collection = MongoConnection(Config.mongoServer)(Config.cardDatabase)(Config.decksCollection)
    collection.findOne(MongoDBObject("name" -> deck)).map(d => {
      val deck = Deck(
        d.getAs[ObjectId]("_id").map(o => o.toString).getOrElse(""),
        d.getAs[String]("name").getOrElse(""),
        d.getAs[Int]("iconId").getOrElse(-1),
        d.getAs[BasicDBObject]("elementsRating").map(obj => ElementsRating(
          obj.getAs[Double]("water").getOrElse(-1.0).toFloat,
          obj.getAs[Double]("fire").getOrElse(-1.0).toFloat,
          obj.getAs[Double]("earth").getOrElse(-1.0).toFloat,
          obj.getAs[Double]("air").getOrElse(-1.0).toFloat
        )).getOrElse(ElementsRating(-1, -1, -1, -1)),
        d.getAs[MongoDBList]("cards").getOrElse(MongoDBList(0)).toList.collect {
          case s: BasicDBObject => s
        }.map(o => CountableCardWithSkin(o.getAs[String]("cardId").getOrElse(""), o.getAs[Int]("count").getOrElse(-1), o.getAs[Int]("skinId"))).toList,
        d.getAs[String]("userName").getOrElse("")
      )

      getCards( deck.cards.map(c => c.cardId)).map(c => {
          deck.cards.find(cc => cc.cardId == c.id).map(f => {
            for(i <- 1 to f.count){
              printableCards = c :: printableCards
            }
          })
      })
    })
    makePdfWithCards(deck, printableCards.sortBy(_.name))

  }

  def makePdfWithCards(name: String, cards: List[Card]) {
    if(cards.size > 0) {
      val pageCount = Math.ceil(cards.size / 9.0).toInt
      val doc = new PDDocument()

      var page: Option[PDPage] = None

      var x = 0
      var y = 0

      for(i <- 0 to cards.size - 1) {
        val image2 = new PDPixelMap(doc, cards(i).toGraphics);

        if(i % 9 == 0) {
          page.map(p => doc.addPage(p))
          page = Some(new PDPage(PDPage.PAGE_SIZE_A4))
          x = 0
          y = 0
        } else {
          if(i % 3 == 0) {
            x = 0
            y += image2.getHeight
          }

        }

        page.map(p => {
          val content = new PDPageContentStream(doc, p, true, false);
          content.drawImage(image2, x, y);
          content.close();
          x += image2.getWidth
        })

      }

      page.map(p => doc.addPage(p))


      doc.save(new FileOutputStream("./test.pdf"));
       doc.close()
    }
  }
}


case class Card(
                 id: String,
                 name: String,
                 rarity: Int,
                 attack: Int,
                 defense: Int,
                 version: String,
                 imageId: Int,
                 price: List[CardPrice],
                 tags: List[String],
                 description: List[Description],
                 initializationType: Int = 0
                 ) {
  def toGraphics: BufferedImage = {
    val image = ImageIO.read(new File("card_small.png"))
    val g = image.createGraphics();

    val qualityHints = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    qualityHints.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
    qualityHints.put(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
    g.setRenderingHints(qualityHints);
    g.setFont(new Font("Times New Roman", Font.PLAIN, 14))
    //Set name
    g.setFont(g.getFont().deriveFont(18f))
    g.setColor(Color.black)
    g.drawString(name, 12, 21)

    //Set attack
    g.setFont(g.getFont().deriveFont(18f))
    g.setColor(Color.black)
    if(attack > 0) g.drawString(attack.toString + "A", 12, 43)

    //Set defense
    g.setFont(g.getFont().deriveFont(18f))
    g.setColor(Color.black)
    if(defense > 0) g.drawString(defense.toString + "D", 38, 43)

    //Set cost
    g.setFont(g.getFont().deriveFont(18f))
    g.setColor(Color.black)
    g.drawString(price.foldLeft(new StringBuilder) {
      (sb, s) => sb append {
        if (s.price > 0) {
          s.price.toString + {
            s.element match {
              case 0 => "W "
              case 1 => "A "
              case 2 => "F "
              case 3 => "E "
              case 4 => "N "
            }
          }
        } else {
          ""
        }

      }
    }.toString, 64, 43)

    //Set tags
    g.setFont(g.getFont().deriveFont(10f))
    g.setColor(Color.black)
    //   g.drawString(tags.foldLeft(new StringBuilder){ (sb, s) => sb append s }.toString, 15,186)

    // Set description
    val descriptionParagraph = description.find(d => d.locale == "en-US").get.description

    val linebreaker = new LineBreakMeasurer(new AttributedString(descriptionParagraph).getIterator(), g.getFontRenderContext());

    var descriptionY = 196f;
    while (linebreaker.getPosition() < descriptionParagraph.length()) {
      val tl = linebreaker.nextLayout(156f);

      descriptionY += tl.getAscent();
      if (descriptionY < (263f - tl.getLeading))
        tl.draw(g, 12, descriptionY);
      descriptionY += tl.getDescent() + tl.getLeading();
    }
    g.dispose()
    //ImageIO.write(image, "png", new File(name + ".png"));
    image
  }
}

case class CardPrice(element: Int, price: Int)

case class Description(locale: String, description: String)


case class ElementsRating(water: Float, fire: Float, earth: Float, air: Float)

case class Deck(id: String, name: String, icon: Int, elementsRatio: ElementsRating, cards: List[CountableCardWithSkin], ownerUserName: String)

case class CountableCardWithSkin(cardId: String, count: Int, skinId: Option[Int])
