package cz.bataliongames.firstenchanter.cards

import akka.actor.{Actor, ActorLogging}
import cz.noveit.games.cardgame.engine.card._
import cz.noveit.games.cardgame.engine.card.helpers._
import cz.noveit.games.cardgame.engine.card.AttackWithCard
import cz.noveit.games.cardgame.engine.card.TableStateAfterParametrizedDefense
import cz.noveit.games.cardgame.engine.card.CounterUseCard
import cz.noveit.games.cardgame.engine.card.TableStateAfterParametrizedAttack
import cz.noveit.games.cardgame.engine.card.EvaluateUseCard
import cz.noveit.games.cardgame.engine.card.EvaluateDefendWithCard
import cz.noveit.games.cardgame.engine.card.AskForPossibilities
import cz.noveit.games.cardgame.engine.card.EvaluateAttackWithCard
import cz.noveit.games.cardgame.engine.card.TableStateAfterParametrizedCounterUsage
import cz.noveit.games.cardgame.engine.card.EvaluateCounterUseCard
import cz.noveit.games.cardgame.engine.card.TableStateAfterParametrizedUsage
import cz.noveit.games.cardgame.engine.card.EvaluateUsage
import cz.noveit.games.cardgame.engine.card.EvaluateAttack
import cz.noveit.games.cardgame.engine.card.UseCard
import cz.noveit.games.cardgame.engine.card.DefendWithCard
import cz.noveit.games.cardgame.engine.card.helpers.possibilities.CreepUnit

/**
 * Created by Wlsek on 25.6.14.
 */
class AirArcher(val cardHolder: CardHolder, val effectRegistry: CardStateEffectsRegistry) extends CardActor {
  def evaluateUse(msg: EvaluateUseCard): EvaluateUsage = {
    LeftCardOnTable(msg)
  }

  def evaluateCounterUse(msg: EvaluateCounterUseCard): EvaluateUsage = {
    EvaluateUsage(msg.ts.copy(counterUsedCards = msg.ts.counterUsedCards.tail), msg.changesStack)
  }

  def evaluateAttackWithCard(msg: EvaluateAttackWithCard): EvaluateAttack = {
    AttackOnPlayerPool(msg, effectRegistry)
  }

  def evaluateDefendWithCard(msg: EvaluateDefendWithCard): EvaluateAttack = {
    DefendWithCardAgainstCard(msg, effectRegistry)
  }

  def useCard(msg: UseCard): TableStateAfterParametrizedUsage = {
    ShowCardOnTable(msg)
  }

  def counterUseCard(msg: CounterUseCard): TableStateAfterParametrizedCounterUsage = {
    TableStateAfterParametrizedCounterUsage(msg, msg.ts, None)
  }

  def attackWithCard(msg: AttackWithCard): TableStateAfterParametrizedAttack = {
    ShowAttackingCardOnTable(msg)
  }

  def defendWithCard(msg: DefendWithCard): TableStateAfterParametrizedDefense = {
    ShowDefendingCardOnTable(msg)
  }

  def askForPossibilities(msg: AskForPossibilities): AskForPossibilities = {
   CreepUnit(msg, cardHolder, effectRegistry)
  }

}