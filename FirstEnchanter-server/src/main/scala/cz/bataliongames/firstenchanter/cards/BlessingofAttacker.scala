package cz.bataliongames.firstenchanter.cards

import akka.actor.{ActorLogging}
import akka.util.Timeout
import com.google.protobuf.Message
import cz.noveit.games.cardgame.engine.card._
import cz.noveit.games.cardgame.engine.card.helpers.{ConsumeCardAfterUse, ShowCardOnTable}
import cz.noveit.games.cardgame.engine.card.CounterUseCard
import cz.noveit.games.cardgame.engine.card.TableStateAfterParametrizedAttack
import cz.noveit.games.cardgame.engine.card.EvaluateAttackWithCard
import cz.noveit.games.cardgame.engine.card.UseCard
import cz.noveit.games.cardgame.engine.card.AttackWithCard
import cz.noveit.games.cardgame.engine.card.TableStateAfterParametrizedDefense
import cz.noveit.games.cardgame.engine.card.EvaluateUseCard
import cz.noveit.games.cardgame.engine.card.EvaluateDefendWithCard
import cz.noveit.games.cardgame.engine.card.AskForPossibilities
import cz.noveit.games.cardgame.engine.card.TableStateAfterParametrizedCounterUsage
import cz.noveit.games.cardgame.engine.card.EvaluateCounterUseCard
import cz.noveit.games.cardgame.engine.card.TableStateAfterParametrizedUsage
import cz.noveit.games.cardgame.engine.card.EvaluateUsage
import cz.noveit.games.cardgame.engine.card.EvaluateAttack
import cz.noveit.games.cardgame.engine.card.DefendWithCard
import cz.noveit.games.cardgame.engine.card.helpers.possibilities.{UseCostAndTargets}
import cz.noveit.games.cardgame.engine.game.session.CardChangeAttack

import akka.pattern.ask
import akka.util.Timeout
import cz.noveit.games.cardgame.engine.handlers.helpers.serialization.CardPositionToProto
import scala.concurrent.duration._
import scala.concurrent.Await


/**
 * Created by Wlsek on 25.6.14.
 */
class BlessingofAttacker(cardHolder: CardHolder, val effectRegistry: CardStateEffectsRegistry) extends CardActor with ActorLogging {

  def evaluateUse(msg: EvaluateUseCard): EvaluateUsage = {
    msg.usage.target.map {
      case target: CardRefIsTarget =>
        val generate = msg.ts.duplicateAlias(msg.usage.onPosition)

        val newTableState = effectRegistry.addEffectToCard(target.position, new IncreaseAttackEffect {
          val source = Some(generate._2)
          val increaseAttackValue = 2

          override def isEffectSerializable = true

          override def serializeEffect(s: CardPosition, target: CardTarget): Message = {
            target match {
              case t: CardIsTarget =>
                cz.noveit.proto.firstenchanter.FirstEnchanterMatch.ChangeAttackEffect
                  .newBuilder()
                  .setTargetedCard(CardPositionToProto(t.position))
                  .setSourceCard(CardPositionToProto(s))
                  .setBy(2)
                  .build()

              case t => throw new NotImplementedError("Serialization not implemented")
            }

          }
        }, generate._1)

        ConsumeCardAfterUse(msg.copy(ts = newTableState, changesStack = CardChangeAttack(newTableState.positionForAlias(target.position), 2) :: msg.changesStack))
      case t =>
        log.error("Blessing of attacker: unknown target!")
        ConsumeCardAfterUse(msg)
    }.getOrElse({
      ConsumeCardAfterUse(msg)
    })
  }

  def evaluateCounterUse(msg: EvaluateCounterUseCard): EvaluateUsage = {
    EvaluateUsage(msg.ts.copy(counterUsedCards = msg.ts.counterUsedCards.tail), msg.changesStack)
  }

  def evaluateAttackWithCard(msg: EvaluateAttackWithCard): EvaluateAttack = {
    EvaluateAttack(msg.ts.copy(attackedCards = msg.ts.attackedCards.tail), msg.changesStack)
  }

  def evaluateDefendWithCard(msg: EvaluateDefendWithCard): EvaluateAttack = {
    EvaluateAttack(msg.ts.copy(defendedCards = msg.ts.defendedCards.tail), msg.changesStack)
  }

  def useCard(msg: UseCard): TableStateAfterParametrizedUsage = {
    ShowCardOnTable(msg)
  }

  def counterUseCard(msg: CounterUseCard): TableStateAfterParametrizedCounterUsage = {
    TableStateAfterParametrizedCounterUsage(msg, msg.ts, None)
  }

  def attackWithCard(msg: AttackWithCard): TableStateAfterParametrizedAttack = {
    TableStateAfterParametrizedAttack(msg, msg.ts, None)
  }

  def defendWithCard(msg: DefendWithCard): TableStateAfterParametrizedDefense = {
    TableStateAfterParametrizedDefense(msg, msg.ts, None)
  }

  def askForPossibilities(msg: AskForPossibilities): AskForPossibilities = {
    msg.ask match {
      case a: AskPossibleUsageForPlayer =>
        val position = msg.ask.tableState.positionForAlias(msg.leftToAsk.head)
        /*
                implicit val timeout = Timeout(15 seconds)
                val tableStateInFuture = sender() ? PreEvaluate(PreEvaluateAction.PreEvaluateActionUse, msg.ask.tableState, List(), new CardStateEffectsRegistry(effectRegistry.effects))

                val state = Await.result(tableStateInFuture, 15.seconds).asInstanceOf[PreEvaluatedState]

                var newTableState = a.tableState
        */

        val players = msg.ask.tableState
          .getPlayerTeam(position.player)
        var newTable = msg.ask.tableState
        val targets = msg.ask.tableState.players
          .filter(p => players contains (p.nickname))
          .map(p => p.onTable.filter(c => c.tags contains (Tags.creep)).zipWithIndex.map(z => {
          val createdRef = newTable.createNewAliasForPosition(OnTableCardPosition(z._2, p.nickname))
          newTable = createdRef._1
          CardRefIsTarget(createdRef._2)
        }).toList).flatten

        val targetsInUsage = newTable.usedCards.filter(us => {
          val position = newTable.positionForAlias(us.usage.onPosition)
          players.contains(position.player) && us.usage.cardHolder.tags.contains(Tags.creep)
        }).map(us => CardRefIsTarget(us.usage.onPosition))

        UseCostAndTargets(msg.copy(ask = a.copy(tableState = newTable)), cardHolder, targets ::: targetsInUsage, effectRegistry)

      case a =>
        UseCostAndTargets(msg, cardHolder, List(), effectRegistry)

    }
  }
}