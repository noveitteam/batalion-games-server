package cz.bataliongames.firstenchanter.cards

import com.google.protobuf.Message
import cz.noveit.games.cardgame.engine.card._
import cz.noveit.games.cardgame.engine.card.EvaluateAttackWithCard
import cz.noveit.games.cardgame.engine.card.EvaluateCounterUseCard
import cz.noveit.games.cardgame.engine.card.EvaluateUseCard
import cz.noveit.games.cardgame.engine.card.helpers.{ShowCardOnTable, ConsumeCardAfterUse}
import cz.noveit.games.cardgame.engine.card.helpers.possibilities.{UseCostAndTargets}
import cz.noveit.games.cardgame.engine.game.session.CardChangeDefense

import akka.pattern.ask
import akka.util.Timeout
import cz.noveit.games.cardgame.engine.handlers.helpers.serialization.CardPositionToProto
import scala.concurrent.duration._
import scala.concurrent.Await


/**
 * Created by Wlsek on 25.6.14.
 */
class BlessingofDefender(cardHolder: CardHolder, val effectRegistry: CardStateEffectsRegistry) extends CardActor {

  def evaluateUse(msg: EvaluateUseCard): EvaluateUsage = {
    msg.usage.target.map {
      case target: CardRefIsTarget =>
        val generate = msg.ts.duplicateAlias(msg.usage.onPosition)

        val newTableState = effectRegistry.addEffectToCard(target.position, new IncreaseDefenseCardEffect {
          override val source = Some(generate._2)
          override val increaseDefenseValue: Int = 2
          override def isEffectSerializable = true
          override def serializeEffect(s: CardPosition, target: CardTarget): Message = {
            target match {
              case t: CardIsTarget =>
                cz.noveit.proto.firstenchanter.FirstEnchanterMatch.ChangeDefenseEffect
                  .newBuilder()
                  .setTargetedCard(CardPositionToProto(t.position))
                  .setSourceCard(CardPositionToProto(s))
                  .setBy(2)
                  .build()

              case t => throw new NotImplementedError("Serialization not implemented")
            }

          }
        }, generate._1)

        ConsumeCardAfterUse(msg.copy(ts = newTableState, changesStack = CardChangeDefense(newTableState.positionForAlias(target.position), 2) :: msg.changesStack))
      case t =>
        log.error("Blessing of attacker: unknown target!")
        ConsumeCardAfterUse(msg)
    }.getOrElse({
      ConsumeCardAfterUse(msg)
    })
  }

  def evaluateCounterUse(msg: EvaluateCounterUseCard): EvaluateUsage = {
    EvaluateUsage(msg.ts.copy(counterUsedCards = msg.ts.counterUsedCards.tail), msg.changesStack)
  }

  def evaluateAttackWithCard(msg: EvaluateAttackWithCard): EvaluateAttack = {
    EvaluateAttack(msg.ts.copy(attackedCards = msg.ts.attackedCards.tail), msg.changesStack)
  }

  def evaluateDefendWithCard(msg: EvaluateDefendWithCard): EvaluateAttack = {
    EvaluateAttack(msg.ts.copy(defendedCards = msg.ts.defendedCards.tail), msg.changesStack)
  }

  def useCard(msg: UseCard): TableStateAfterParametrizedUsage = {
    ShowCardOnTable(msg)
  }

  def counterUseCard(msg: CounterUseCard): TableStateAfterParametrizedCounterUsage = {
    TableStateAfterParametrizedCounterUsage(msg, msg.ts, None)
  }

  def attackWithCard(msg: AttackWithCard): TableStateAfterParametrizedAttack = {
    TableStateAfterParametrizedAttack(msg, msg.ts, None)
  }

  def defendWithCard(msg: DefendWithCard): TableStateAfterParametrizedDefense = {
    TableStateAfterParametrizedDefense(msg, msg.ts, None)
  }

  def askForPossibilities(msg: AskForPossibilities): AskForPossibilities = {
    msg.ask match {
      case a: AskPossibleUsageForPlayer =>
        val position = msg.ask.tableState.positionForAlias(msg.leftToAsk.head)
        val players = msg.ask.tableState
          .getPlayerTeam(position.player)
        var newTable = msg.ask.tableState

        val targets = msg.ask.tableState.players
          .filter(p => players contains (p.nickname))
          .map(p => p.onTable.filter(c => c.tags contains (Tags.creep)).zipWithIndex.map(z => {
          val createdRef = newTable.createNewAliasForPosition(OnTableCardPosition(z._2, p.nickname))
          newTable = createdRef._1
          CardRefIsTarget(createdRef._2)
        }).toList).flatten

        val targetsInUsage = newTable.usedCards.filter(us => {
          val position = newTable.positionForAlias(us.usage.onPosition)
          players.contains(position.player) && us.usage.cardHolder.tags.contains(Tags.creep)
        }).map(us => CardRefIsTarget(us.usage.onPosition))

        UseCostAndTargets(msg.copy(ask = a.copy(tableState = newTable)), cardHolder, targets ::: targetsInUsage, effectRegistry)
      case a =>
        UseCostAndTargets(msg, cardHolder, List(), effectRegistry)

    }
  }
}