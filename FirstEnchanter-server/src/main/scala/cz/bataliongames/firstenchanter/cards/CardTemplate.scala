package cz.bataliongames.firstenchanter.cards

import akka.actor.{Actor, ActorLogging}
import cz.noveit.games.cardgame.engine.card._
import cz.noveit.games.cardgame.engine.card.EvaluateAttackWithCard
import cz.noveit.games.cardgame.engine.card.EvaluateCounterUseCard
import cz.noveit.games.cardgame.engine.card.EvaluateUseCard

/**
 * Created by Wlsek on 25.6.14.
 */
class CardTemplate(cardHolder: CardHolder, val effectRegistry: CardStateEffectsRegistry) extends CardActor {
  def evaluateUse(msg: EvaluateUseCard): EvaluateUsage = ???

  def evaluateCounterUse(msg: EvaluateCounterUseCard): EvaluateUsage = ???

  def evaluateAttackWithCard(msg: EvaluateAttackWithCard): EvaluateAttack = ???

  def evaluateDefendWithCard(msg: EvaluateDefendWithCard): EvaluateAttack = ???

  def useCard(msg: UseCard): TableStateAfterParametrizedUsage = ???

  def counterUseCard(msg: CounterUseCard): TableStateAfterParametrizedCounterUsage = ???

  def attackWithCard(msg: AttackWithCard): TableStateAfterParametrizedAttack = ???

  def defendWithCard(msg: DefendWithCard): TableStateAfterParametrizedDefense = ???

  def askForPossibilities(msg: AskForPossibilities): AskForPossibilities = ???
}
