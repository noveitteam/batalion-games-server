package cz.bataliongames.firstenchanter.cards

import akka.actor.{Actor, ActorLogging}
import cz.noveit.games.cardgame.engine.card._
import cz.noveit.games.cardgame.engine.card.EvaluateAttackWithCard
import cz.noveit.games.cardgame.engine.card.EvaluateCounterUseCard
import cz.noveit.games.cardgame.engine.card.EvaluateUseCard
import cz.noveit.games.cardgame.engine.card.helpers.{ShowCardOnTable, ConsumeCardAfterUse}
import cz.noveit.games.cardgame.engine.card.helpers.possibilities.{UseCostAndTargets, UseCostOnly}
import cz.noveit.games.cardgame.engine.game.session.{CardConsumed, CardStripEquipment, PLAYER_ROLL_POSITION_DEFENDER, PLAYER_ROLL_POSITION_ATTACKER}
import cz.noveit.games.cardgame.engine.player.PlayerPlaysMatch
import scala.util.Random

import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import scala.concurrent.Await

/**
 * Created by Wlsek on 25.6.14.
 */
class Disarm(cardHolder: CardHolder, val effectRegistry: CardStateEffectsRegistry) extends CardActor {

  def evaluateUse(msg: EvaluateUseCard): EvaluateUsage = {
    val pos = msg.ts.positionForAlias(msg.usage.onPosition)
    if (effectRegistry.filterEffectsByPlayerName(pos.player, msg.ts).exists(e => e.effect.source.exists(s => {
      val c = msg.ts.cardForPositionRef(s)
      c.exists(card => card.tags contains(Tags.shield))
    }))) {
      msg.usage.target.map(t => {
        val position = if (msg.ts.playersTurn.contains(pos.player)) {
          PLAYER_ROLL_POSITION_ATTACKER
        } else {
          PLAYER_ROLL_POSITION_DEFENDER
        }

        val newMsg = t match {
          case target: CardRefIsTarget =>
            val removeChange = effectRegistry.removeEffectsForCardWithTags(target.position, List(Tags.weapon), msg.ts)
            msg.copy(
              ts = removeChange._2,
              changesStack =
                removeChange._1
                  .map(e => CardStripEquipment(msg.ts.positionForAlias(target.position), List(Tags.weapon))) ::: msg.changesStack
            )

          case target: PlayerIsTarget =>
            msg.ts.players.find(p => p.nickname == target.playerName).map(p => {
              val id = (new Random()).nextInt(p.hands.size)
              msg.copy(
                ts = msg.ts.copy(players = PlayerPlaysMatch(
                  p.nickname,
                  p.deck,
                  p.hands.filterNot(c => c == p.hands(id)),
                  p.hands(id) +: p.graveyard,
                  p.onTable,
                  p.charmRatio,
                  p.charm,
                  p.handler,
                  p.connected
                ) :: msg.ts.players.filterNot(p => p.nickname == target.playerName)),
                changesStack = CardConsumed(InHandCardPosition(id, p.nickname), cardHolder) :: msg.changesStack
              )
            }).getOrElse({
              msg
            })
          case t =>
            log.error("Disarm: Unknown target!")
            msg
        }

        ConsumeCardAfterUse(newMsg.copy(ts = if(position != newMsg.ts.fortuneIsOnSide) {
          val newTs = effectRegistry.effects.find(e => {
            e.effect.source.exists(s => {
              val card = newMsg.ts.cardForPositionRef(s)
              card.exists(c => c.tags.contains(Tags.shield))
            })
          }).map(e => effectRegistry.removeEffectForCard(e.effect, newMsg.ts))
          newTs.getOrElse(newMsg.ts)
        } else {
          newMsg.ts
        }))
      }).getOrElse({
        ConsumeCardAfterUse(msg)
      })
    } else {
      EvaluateUsage(msg.ts.copy(usedCards = msg.ts.usedCards.tail), msg.changesStack)
    }
  }

  def evaluateCounterUse(msg: EvaluateCounterUseCard): EvaluateUsage = {
    EvaluateUsage(msg.ts.copy(counterUsedCards = msg.ts.counterUsedCards.tail), msg.changesStack)
  }

  def evaluateAttackWithCard(msg: EvaluateAttackWithCard): EvaluateAttack = {
    EvaluateAttack(msg.ts.copy(attackedCards = msg.ts.attackedCards.tail), msg.changesStack)
  }

  def evaluateDefendWithCard(msg: EvaluateDefendWithCard): EvaluateAttack = {
    EvaluateAttack(msg.ts.copy(defendedCards = msg.ts.defendedCards.tail), msg.changesStack)
  }

  def useCard(msg: UseCard): TableStateAfterParametrizedUsage = {
    ShowCardOnTable(msg)
  }

  def counterUseCard(msg: CounterUseCard): TableStateAfterParametrizedCounterUsage = {
    TableStateAfterParametrizedCounterUsage(msg, msg.ts, None)
  }

  def attackWithCard(msg: AttackWithCard): TableStateAfterParametrizedAttack = {
    TableStateAfterParametrizedAttack(msg, msg.ts, None)
  }

  def defendWithCard(msg: DefendWithCard): TableStateAfterParametrizedDefense = {
    TableStateAfterParametrizedDefense(msg, msg.ts, None)
  }

  def askForPossibilities(msg: AskForPossibilities): AskForPossibilities = {
    msg.ask match {
      case a: AskPossibleUsageForPlayer =>

        val position = msg.ask.tableState.positionForAlias(msg.leftToAsk.head)
        val enemies = msg.ask.tableState.getEnemyTeam(position.player)

        implicit val timeout = Timeout(15 seconds)
        val tableStateInFuture = sender() ? PreEvaluate(PreEvaluateAction.PreEvaluateActionUse, msg.ask.tableState, List(), new CardStateEffectsRegistry(effectRegistry.effects))
        val state = Await.result(tableStateInFuture, 15.seconds).asInstanceOf[PreEvaluatedState]

        var newTableState = msg.ask.tableState

        val targets = state.effectRegistry.effects.filter(e => {
          val position = e.effectIsOn match {
            case t: CardRefIsTarget =>
              Some(state.tableState.positionForAlias(t.position))
            case t: CardIsTarget =>
              Some(t.position)
            case t => None
          }

          position.exists( p => {
            val c = state.tableState.cardForPosition(p)
            c.exists(card => card.tags contains(Tags.weapon)) &&
            enemies.contains(p.player)
          })
        }).map(e => {
          e.effectIsOn match {
            case t: CardRefIsTarget =>
              Some(state.tableState.cardForPositionRef(t.position))
            case t: CardIsTarget =>
              Some(state.tableState.cardForPosition(t.position))
            case t => None
          }
        }).flatten.filter(c => c.isDefined).map(c => c.get).map(c => {
          a.tableState.positionForCardHolder(c).map(p => CardIsTarget(p))
        }).filter(c => c.isDefined).map(c => c.get)

        UseCostAndTargets(msg.copy(
          ask = a.copy(tableState = newTableState)
        ), cardHolder, targets, effectRegistry)
      case t =>
        UseCostAndTargets(msg, cardHolder, List(), effectRegistry)
    }
  }

}