package cz.bataliongames.firstenchanter.cards

import akka.actor.{Actor, ActorLogging}
import com.google.protobuf.Message
import cz.noveit.games.cardgame.engine.card._
import cz.noveit.games.cardgame.engine.card.EvaluateAttackWithCard
import cz.noveit.games.cardgame.engine.card.EvaluateCounterUseCard
import cz.noveit.games.cardgame.engine.card.EvaluateUseCard
import cz.noveit.games.cardgame.engine.card.helpers.{ShowCardOnTable, LeftCardOnTable}
import cz.noveit.games.cardgame.engine.card.helpers.possibilities.TotemUnit

/**
 * Created by Wlsek on 25.6.14.
 */
class EarthTotem(cardHolder: CardHolder,  val effectRegistry: CardStateEffectsRegistry) extends CardActor {

  def evaluateUse(msg: EvaluateUseCard): EvaluateUsage = {
    val position = msg.ts.positionForAlias(msg.usage.onPosition)
    val ref = msg.ts.createNewAliasForPosition(position)
    val team = msg.ts.getPlayerTeam(position.player)

    effectRegistry.addEffectToTeam(team, new SpellAbsorbCardEffect {
      override val source = Some(ref._2)

      val numberOfUsages = 1
      val teamMembers = msg.ts.getPlayerTeam(position.player)

      override def isEffectSerializable: Boolean = false
      override def serializeEffect(source: CardPosition, target: CardTarget): Message = throw new NotImplementedError("Not serializable")
    })

    LeftCardOnTable(msg.copy(ts = ref._1))
  }

  def evaluateCounterUse(msg: EvaluateCounterUseCard): EvaluateUsage = {
    EvaluateUsage(msg.ts.copy(counterUsedCards = msg.ts.counterUsedCards.tail), msg.changesStack)
  }

  def evaluateAttackWithCard(msg: EvaluateAttackWithCard): EvaluateAttack = {
    EvaluateAttack(msg.ts.copy(attackedCards = msg.ts.attackedCards.tail), msg.changesStack)
  }

  def evaluateDefendWithCard(msg: EvaluateDefendWithCard): EvaluateAttack = {
    EvaluateAttack(msg.ts.copy(defendedCards = msg.ts.defendedCards.tail), msg.changesStack)
  }

  def useCard(msg: UseCard): TableStateAfterParametrizedUsage = {
    ShowCardOnTable(msg)
  }

  def counterUseCard(msg: CounterUseCard): TableStateAfterParametrizedCounterUsage = {
    TableStateAfterParametrizedCounterUsage(msg, msg.ts, None)
  }

  def attackWithCard(msg: AttackWithCard): TableStateAfterParametrizedAttack = {
    TableStateAfterParametrizedAttack(msg, msg.ts, None)
  }

  def defendWithCard(msg: DefendWithCard): TableStateAfterParametrizedDefense = {
    TableStateAfterParametrizedDefense(msg, msg.ts, None)
  }

  def askForPossibilities(msg: AskForPossibilities): AskForPossibilities = {
    TotemUnit(msg, cardHolder, effectRegistry)
  }

}