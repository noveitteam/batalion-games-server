package cz.bataliongames.firstenchanter.cards

import akka.actor.{Actor, ActorLogging}
import cz.noveit.games.cardgame.engine.card._
import cz.noveit.games.cardgame.engine.card.EvaluateAttackWithCard
import cz.noveit.games.cardgame.engine.card.EvaluateCounterUseCard
import cz.noveit.games.cardgame.engine.card.EvaluateUseCard
import cz.noveit.games.cardgame.engine.card.helpers.{ShowCardOnTable, ConsumeCardAfterUse}
import cz.noveit.games.cardgame.engine.card.helpers.possibilities.{UseCostAndTargets, UseCostOnly}
import cz.noveit.games.cardgame.engine.card.helpers.transformation.{DealDamageToCard, DealDamageToPlayerTeam, DealDamage}
import cz.noveit.games.cardgame.engine.game.TableState
import cz.noveit.games.cardgame.engine.game.session.TableStateChange

/**
 * Created by Wlsek on 25.6.14.
 */
class Earthfall(cardHolder: CardHolder, val effectRegistry: CardStateEffectsRegistry) extends CardActor {

  def evaluateUse(msg: EvaluateUseCard): EvaluateUsage = {
    val position = msg.ts.positionForAlias(msg.usage.onPosition)
    msg.usage.target.map(t => {

      val team = t match {
        case target: PlayerTeamIsTarget =>
          target.team

        case target: CardRefIsTarget =>
          val position = msg.ts.positionForAlias(target.position)
          msg.ts.getPlayerTeam(position.player)

        case t =>
          log.error("{} unknown target! ", cardHolder.id)
          List()
      }

      ConsumeCardAfterUse(
        team.map(p => effectRegistry.filterEffectsByPlayerName(p, msg.ts))
          .flatten
          .find(e =>
          e.effect.isInstanceOf[SpellAbsorbCardEffect] &&
            (e.effect.element == CardEffectElement.CardEffectElementEarth || e.effect.element == CardEffectElement.CardEffectElementNeutral)
          ).map(e => {

          msg.copy(
            ts = if (e.effect.source.exists(s => {
              val card = msg.ts.cardForPositionRef(s)
              card.exists(c => c.tags contains (Tags.totem))
            })) {
              msg.ts.moveCardFromTableToGraveyard(e.effect.source.get)
            } else {
              msg.ts
            }
          )
        }).getOrElse({
          var attack = cardHolder.attack
          effectRegistry
            .filterEffectsByPlayerName(position.player, msg.ts)
            .filter(
              e => e.effect.isInstanceOf[SpellBoostCardEffect] &&
                (e.effect.element == CardEffectElement.CardEffectElementEarth || e.effect.element == CardEffectElement.CardEffectElementNeutral)
            )
            .map(e => {
            val effect = e.effect.asInstanceOf[SpellBoostCardEffect]
            attack += effect.spellBoost
          })

          val tsAfterDmg: Pair[TableState, List[TableStateChange]] = t match {
            case target: PlayerTeamIsTarget =>
              DealDamage(DealDamageToPlayerTeam(msg.usage.onPosition, target.team.head, attack), msg.ts, effectRegistry)

            case target: CardRefIsTarget =>
              DealDamage(DealDamageToCard(msg.usage.onPosition, target.position, attack), msg.ts, effectRegistry)

            case t =>
              log.error("{} unknown target! ", cardHolder.id)
              msg.ts -> List()
          }

          msg.copy(ts = tsAfterDmg._1, changesStack = tsAfterDmg._2 ::: msg.changesStack)
        }))
    }).getOrElse({
      ConsumeCardAfterUse(msg)
    })
  }

  def evaluateCounterUse(msg: EvaluateCounterUseCard): EvaluateUsage = {
    EvaluateUsage(msg.ts.copy(counterUsedCards = msg.ts.counterUsedCards.tail), msg.changesStack)
  }

  def evaluateAttackWithCard(msg: EvaluateAttackWithCard): EvaluateAttack = {
    EvaluateAttack(msg.ts.copy(attackedCards = msg.ts.attackedCards.tail), msg.changesStack)
  }

  def evaluateDefendWithCard(msg: EvaluateDefendWithCard): EvaluateAttack = {
    EvaluateAttack(msg.ts.copy(defendedCards = msg.ts.defendedCards.tail), msg.changesStack)
  }

  def useCard(msg: UseCard): TableStateAfterParametrizedUsage = {
    ShowCardOnTable(msg)
  }

  def counterUseCard(msg: CounterUseCard): TableStateAfterParametrizedCounterUsage = {
    TableStateAfterParametrizedCounterUsage(msg, msg.ts, None)
  }

  def attackWithCard(msg: AttackWithCard): TableStateAfterParametrizedAttack = {
    TableStateAfterParametrizedAttack(msg, msg.ts, None)
  }

  def defendWithCard(msg: DefendWithCard): TableStateAfterParametrizedDefense = {
    TableStateAfterParametrizedDefense(msg, msg.ts, None)
  }

  def askForPossibilities(msg: AskForPossibilities): AskForPossibilities = {

    msg.ask match {
      case a: AskPossibleUsageForPlayer =>

        val position = msg.ask.tableState.positionForAlias(msg.leftToAsk.head)
        val enemies =  msg.ask.tableState.getEnemyTeam(position.player)
        var newTableState = msg.ask.tableState
        val targets = msg.ask.tableState.players
          .filter(p => enemies contains(p.nickname))
          .map(p => p.onTable.zipWithIndex.map(z => {

          val generated = newTableState.createNewAliasForPosition(OnTableCardPosition(z._2, p.nickname), CardPositionRefLifeSpan.CardPositionRefLifeSpanOneRound)
          newTableState = generated._1
          CardRefIsTarget(generated._2)
        }).toList).flatten

        UseCostAndTargets(msg.copy(ask = a.copy(tableState = newTableState)), cardHolder, PlayerTeamIsTarget(enemies) :: targets, effectRegistry)
      case a =>
        UseCostAndTargets(msg, cardHolder, List(), effectRegistry)

    }
  }

}