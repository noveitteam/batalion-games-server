package cz.bataliongames.firstenchanter.cards

import akka.actor.{Actor, ActorLogging}
import cz.noveit.games.cardgame.engine.card._
import cz.noveit.games.cardgame.engine.card.EvaluateAttackWithCard
import cz.noveit.games.cardgame.engine.card.EvaluateCounterUseCard
import cz.noveit.games.cardgame.engine.card.EvaluateUseCard
import cz.noveit.games.cardgame.engine.card.helpers.{ConsumeCardAfterUse, ShowCardOnTable}
import cz.noveit.games.cardgame.engine.card.helpers.possibilities.{UseCostAndTargets}
import cz.noveit.games.cardgame.engine.card.helpers.generator.{GenerateCards, GenerateElements}
import cz.noveit.games.cardgame.engine.game.session.{CardGenerated, ElementsGenerated}

/**
 * Created by Wlsek on 25.6.14.
 */
class ElementalSynchronization(cardHolder: CardHolder, val effectRegistry: CardStateEffectsRegistry) extends CardActor {

  def evaluateUse(msg: EvaluateUseCard): EvaluateUsage = {
    val position = msg.ts.positionForAlias(msg.usage.onPosition)
    if (effectRegistry.filterEffectsByPlayerName(position.player, msg.ts).exists(e => e.effect.isInstanceOf[ElementalPresence])) {
      msg.ts.players.find(p => p.nickname == position.player).map(p => {

        val generatedElements = GenerateElements(p.charm.invertByLimit(msg.ts.limits.maxElements), 4)
        val generatedCards = GenerateCards(p.deck, 4)

        ConsumeCardAfterUse(msg.copy(
          ts = msg.ts.copy(
            players = p.copy(
              charm = p.charm + generatedElements
            ) :: msg.ts.players.filter(pl => pl != p)
          )
            .transformDeckForPlayer(p.nickname, generatedCards._1)
            .addCardsToHandForPlayer(p.nickname, generatedCards._2),
          changesStack = ElementsGenerated(generatedElements, p.nickname) ::
            generatedCards._2.view.zipWithIndex.map(gc => CardGenerated(InHandCardPosition(gc._2, p.nickname), gc._1)).toList :::
            msg.changesStack
        ))
      }).getOrElse({
        EvaluateUsage(msg.ts, msg.changesStack)
      })
    } else {
      EvaluateUsage(msg.ts, msg.changesStack)
    }
  }
  def evaluateCounterUse(msg: EvaluateCounterUseCard): EvaluateUsage = {
    EvaluateUsage(msg.ts.copy(counterUsedCards = msg.ts.counterUsedCards.tail), msg.changesStack)
  }

  def evaluateAttackWithCard(msg: EvaluateAttackWithCard): EvaluateAttack = {
    EvaluateAttack(msg.ts.copy(attackedCards = msg.ts.attackedCards.tail), msg.changesStack)
  }

  def evaluateDefendWithCard(msg: EvaluateDefendWithCard): EvaluateAttack = {
    EvaluateAttack(msg.ts.copy(defendedCards = msg.ts.defendedCards.tail), msg.changesStack)
  }

  def useCard(msg: UseCard): TableStateAfterParametrizedUsage = {
    ShowCardOnTable(msg)
  }

  def counterUseCard(msg: CounterUseCard): TableStateAfterParametrizedCounterUsage = {
    TableStateAfterParametrizedCounterUsage(msg, msg.ts, None)
  }

  def attackWithCard(msg: AttackWithCard): TableStateAfterParametrizedAttack = {
    TableStateAfterParametrizedAttack(msg, msg.ts, None)
  }

  def defendWithCard(msg: DefendWithCard): TableStateAfterParametrizedDefense = {
    TableStateAfterParametrizedDefense(msg, msg.ts, None)
  }

  def askForPossibilities(msg: AskForPossibilities): AskForPossibilities = {
    val targets = msg.ask.players.map(p => effectRegistry
      .filterEffectsByPlayerName(p, msg.ask.tableState))
      .flatten
      .find(e => e.isInstanceOf[ElementalPresence])
      .map(tep => PlayerTeamIsTarget(msg.ask.players)).toList

    UseCostAndTargets(msg, cardHolder, targets, effectRegistry)
  }

}