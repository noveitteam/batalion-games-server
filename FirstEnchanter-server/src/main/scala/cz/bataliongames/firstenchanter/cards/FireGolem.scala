package cz.bataliongames.firstenchanter.cards

import akka.actor.{Actor, ActorLogging}
import com.google.protobuf.Message
import cz.noveit.games.cardgame.engine.card._
import cz.noveit.games.cardgame.engine.card.helpers._
import cz.noveit.games.cardgame.engine.card.CounterUseCard
import cz.noveit.games.cardgame.engine.card.TableStateAfterParametrizedAttack
import cz.noveit.games.cardgame.engine.card.EvaluateAttackWithCard
import cz.noveit.games.cardgame.engine.card.UseCard
import cz.noveit.games.cardgame.engine.card.AttackWithCard
import cz.noveit.games.cardgame.engine.card.TableStateAfterParametrizedDefense
import cz.noveit.games.cardgame.engine.card.EvaluateUseCard
import cz.noveit.games.cardgame.engine.card.EvaluateDefendWithCard
import cz.noveit.games.cardgame.engine.card.AskForPossibilities
import cz.noveit.games.cardgame.engine.card.TableStateAfterParametrizedCounterUsage
import cz.noveit.games.cardgame.engine.card.EvaluateCounterUseCard
import cz.noveit.games.cardgame.engine.card.TableStateAfterParametrizedUsage
import cz.noveit.games.cardgame.engine.card.EvaluateUsage
import cz.noveit.games.cardgame.engine.card.EvaluateAttack
import cz.noveit.games.cardgame.engine.card.DefendWithCard
import cz.noveit.games.cardgame.engine.card.helpers.possibilities.CreepUnit

/**
 * Created by Wlsek on 25.6.14.
 */
class FireGolem(val cardHolder: CardHolder, val effectRegistry: CardStateEffectsRegistry) extends CardActor {
  def evaluateUse(msg: EvaluateUseCard): EvaluateUsage = {
    val position = msg.ts.positionForAlias(msg.usage.onPosition)
    val ref = msg.ts.createNewAliasForPosition(position)
    val team = msg.ts.getPlayerTeam(position.player)

    effectRegistry.addEffectToTeam(team, new ElementalPresence {
      val source: Option[CardPositionRef] = Some(ref._2)
      override val element = CardEffectElement.CardEffectElementFire
      val teamMembers: List[String] = msg.ts.getPlayerTeam(position.player)

      override def isEffectSerializable: Boolean = false
      override def serializeEffect(source: CardPosition, target: CardTarget): Message = throw new NotImplementedError("Not serializable")
    })

    LeftCardOnTable(msg.copy(ts =  ref._1))
  }

  def evaluateCounterUse(msg: EvaluateCounterUseCard): EvaluateUsage = {
    EvaluateUsage(msg.ts.copy(counterUsedCards = msg.ts.counterUsedCards.tail), msg.changesStack)
  }

  def evaluateAttackWithCard(msg: EvaluateAttackWithCard): EvaluateAttack = {
    AttackOnPlayerPool(msg, effectRegistry)
  }

  def evaluateDefendWithCard(msg: EvaluateDefendWithCard): EvaluateAttack = {
    DefendWithCardAgainstCard(msg, effectRegistry)
  }

  def useCard(msg: UseCard): TableStateAfterParametrizedUsage = {
    ShowCardOnTable(msg)
  }

  def counterUseCard(msg: CounterUseCard): TableStateAfterParametrizedCounterUsage = {
    TableStateAfterParametrizedCounterUsage(msg, msg.ts, None)
  }

  def attackWithCard(msg: AttackWithCard): TableStateAfterParametrizedAttack = {
    ShowAttackingCardOnTable(msg)
  }

  def defendWithCard(msg: DefendWithCard): TableStateAfterParametrizedDefense = {
    ShowDefendingCardOnTable(msg)
  }

  def askForPossibilities(msg: AskForPossibilities): AskForPossibilities = {
    CreepUnit(msg, cardHolder, effectRegistry)
  }

}