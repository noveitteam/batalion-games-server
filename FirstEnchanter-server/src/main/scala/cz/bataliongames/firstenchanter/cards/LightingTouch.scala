package cz.bataliongames.firstenchanter.cards

import akka.actor.{Actor, ActorLogging}
import cz.noveit.games.cardgame.engine.card._
import cz.noveit.games.cardgame.engine.card.EvaluateAttackWithCard
import cz.noveit.games.cardgame.engine.card.EvaluateCounterUseCard
import cz.noveit.games.cardgame.engine.card.EvaluateUseCard
import cz.noveit.games.cardgame.engine.card.helpers.{ShowCardOnTable, ConsumeCardAfterUse}
import cz.noveit.games.cardgame.engine.card.helpers.possibilities.{UseCostAndTargets, UseCostOnly}
import cz.noveit.games.cardgame.engine.game.session.PlayerPoolReceivedHeal

/**
 * Created by Wlsek on 25.6.14.
 */
class LightingTouch(cardHolder: CardHolder, val effectRegistry: CardStateEffectsRegistry) extends CardActor {

  def evaluateUse(msg: EvaluateUseCard): EvaluateUsage = {
    val position = msg.ts.positionForAlias(msg.usage.onPosition)
    (msg.usage.target map {
      case t: PlayerTeamIsTarget =>
        var healed = cardHolder.attack
        effectRegistry
          .filterEffectsByPlayerName(position.player, msg.ts)
          .filter(
            e => e.effect.isInstanceOf[SpellBoostCardEffect] &&
              (e.effect.element == CardEffectElement.CardEffectElementAir || e.effect.element == CardEffectElement.CardEffectElementNeutral)
          )
          .map(e => {
          val effect = e.effect.asInstanceOf[SpellBoostCardEffect]
          healed += effect.spellBoost
        })

        ConsumeCardAfterUse(
          msg.copy(ts = msg.ts.copy(teamHp = msg.ts.teamHp.find(team => team.members.contains(position.player))
            .map(f => List(f.copy(hp = f.hp + healed))).getOrElse(List()) ::: msg.ts.teamHp.filterNot(f => f.members.contains(position.player))
          ),
            changesStack = PlayerPoolReceivedHeal(msg.ts.getPlayerTeam(position.player), healed, position) :: msg.changesStack)
        )
      case t =>
        log.error("CalmingTouch: unknown target!")
        ConsumeCardAfterUse(msg)
    }).getOrElse({
      ConsumeCardAfterUse(msg)
    })
  }


  def evaluateCounterUse(msg: EvaluateCounterUseCard): EvaluateUsage = {
    EvaluateUsage(msg.ts.copy(counterUsedCards = msg.ts.counterUsedCards.tail), msg.changesStack)
  }

  def evaluateAttackWithCard(msg: EvaluateAttackWithCard): EvaluateAttack = {
    EvaluateAttack(msg.ts.copy(attackedCards = msg.ts.attackedCards.tail), msg.changesStack)
  }

  def evaluateDefendWithCard(msg: EvaluateDefendWithCard): EvaluateAttack = {
    EvaluateAttack(msg.ts.copy(defendedCards = msg.ts.defendedCards.tail), msg.changesStack)
  }
  def useCard(msg: UseCard): TableStateAfterParametrizedUsage = {
    ShowCardOnTable(msg)
  }

  def counterUseCard(msg: CounterUseCard): TableStateAfterParametrizedCounterUsage = {
    TableStateAfterParametrizedCounterUsage(msg, msg.ts, None)
  }

  def attackWithCard(msg: AttackWithCard): TableStateAfterParametrizedAttack = {
    TableStateAfterParametrizedAttack(msg, msg.ts, None)
  }

  def defendWithCard(msg: DefendWithCard): TableStateAfterParametrizedDefense = {
    TableStateAfterParametrizedDefense(msg, msg.ts, None)
  }

  def askForPossibilities(msg: AskForPossibilities): AskForPossibilities = {
    val position = msg.ask.tableState.positionForAlias(msg.leftToAsk.head)
    UseCostAndTargets(msg, cardHolder, List(PlayerTeamIsTarget(msg.ask.tableState.getPlayerTeam(position.player))), effectRegistry)
  }

}