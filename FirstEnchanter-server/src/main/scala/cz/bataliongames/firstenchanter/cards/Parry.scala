package cz.bataliongames.firstenchanter.cards

import akka.actor.{Actor, ActorLogging}
import cz.noveit.games.cardgame.engine.card._
import cz.noveit.games.cardgame.engine.card.EvaluateAttackWithCard
import cz.noveit.games.cardgame.engine.card.EvaluateCounterUseCard
import cz.noveit.games.cardgame.engine.card.EvaluateUseCard
import cz.noveit.games.cardgame.engine.card.helpers.ShowDefendingCardOnTable
import cz.noveit.games.cardgame.engine.card.helpers.possibilities.DefensiveCounterUseCostAndTargets

import akka.pattern.ask
import akka.util.Timeout
import cz.noveit.games.cardgame.engine.game.session.{CardAttackCancel, CardConsumed}
import scala.concurrent.duration._
import scala.concurrent.Await
/**
 * Created by Wlsek on 25.6.14.
 */
class Parry(cardHolder: CardHolder, val effectRegistry: CardStateEffectsRegistry) extends CardActor {
  def evaluateUse(msg: EvaluateUseCard): EvaluateUsage = {
    EvaluateUsage(msg.ts.copy(usedCards = msg.ts.usedCards.tail), msg.changesStack)
  }

  def evaluateCounterUse(msg: EvaluateCounterUseCard): EvaluateUsage = {
    EvaluateUsage(msg.ts.copy(usedCards = msg.ts.usedCards.tail), msg.changesStack)
  }

  def evaluateAttackWithCard(msg: EvaluateAttackWithCard): EvaluateAttack = {
    EvaluateAttack(msg.ts.copy(attackedCards = msg.ts.attackedCards.tail), msg.changesStack)
  }

  def evaluateDefendWithCard(msg: EvaluateDefendWithCard): EvaluateAttack = {
    val position = msg.ts.positionForAlias(msg.defend.defendWith)

    msg.ts.players.find(p => p.nickname == position.player).map(player => {
      if (effectRegistry.filterEffectsByPlayerName(position.player, msg.ts).exists(e => e.effect.source.exists(s => {
        val c = msg.ts.cardForPositionRef(s)
        c.exists(card => card.tags contains(Tags.oneHandSword))
      }))) {
        msg.defend.against match {
          case t: CardRefIsTarget =>
            msg.ts.attackedCards.find(ac => msg.ts.positionForAlias(ac.attack.onPosition) =? msg.ts.positionForAlias(t.position)).map(f => {
              EvaluateAttack(
                msg.ts.moveCardFromHandsToGraveyard(msg.defend.defendWith)
                .removeAttackedCard(t.position)
                .removeDefendedCard(msg.defend.defendWith),
                changesStack = CardConsumed(position, cardHolder) :: CardAttackCancel(msg.ts.positionForAlias(t.position), position) :: msg.changesStack
              )
            }).getOrElse({
              EvaluateAttack(msg.ts.copy(defendedCards = msg.ts.defendedCards.tail), msg.changesStack)
            })
          case t =>
            log.error("{} unknown target! ", cardHolder.id)
            EvaluateAttack(msg.ts.copy(defendedCards = msg.ts.defendedCards.tail), msg.changesStack)
        }
      } else {
        EvaluateAttack(msg.ts.copy(defendedCards = msg.ts.defendedCards.tail), msg.changesStack)
      }
    }).getOrElse({
      EvaluateAttack(msg.ts.copy(defendedCards = msg.ts.defendedCards.tail), msg.changesStack)
    })
  }

  def useCard(msg: UseCard): TableStateAfterParametrizedUsage = {
    TableStateAfterParametrizedUsage(msg, msg.ts, None)
  }

  def counterUseCard(msg: CounterUseCard): TableStateAfterParametrizedCounterUsage = {
    TableStateAfterParametrizedCounterUsage(msg, msg.ts, None)
  }

  def attackWithCard(msg: AttackWithCard): TableStateAfterParametrizedAttack = {
    TableStateAfterParametrizedAttack(msg, msg.ts, None)
  }

  def defendWithCard(msg: DefendWithCard): TableStateAfterParametrizedDefense = {
    ShowDefendingCardOnTable(msg)
  }

  def askForPossibilities(msg: AskForPossibilities): AskForPossibilities = {
    msg.ask match {
      case a: AskPossibleDefendingPositionsForPlayer =>

        val position = msg.ask.tableState.positionForAlias(msg.leftToAsk.head)

        implicit val timeout = Timeout(15 seconds)
        val tableStateInFuture = sender() ? PreEvaluate(PreEvaluateAction.PreEvaluateActionUse, msg.ask.tableState, List(), new CardStateEffectsRegistry(effectRegistry.effects))
        val state = Await.result(tableStateInFuture, 15.seconds).asInstanceOf[PreEvaluatedState]

        if (state.effectRegistry.filterEffectsByPlayerName(position.player, state.tableState).exists(e => e.effect.source.exists(s => {
          val c = state.tableState.cardForPositionRef(s)
          c.exists(card => card.tags contains(Tags.oneHandSword))
        }))) {
          val enemies = state.tableState.getEnemyTeam(position.player)
          val targets = state.tableState.attackedCards
            .find(ac => enemies.contains(state.tableState.positionForAlias(ac.attack.onPosition).player) && ac.attack.card.tags.contains(Tags.creep))
            .map(c => CardRefIsTarget(c.attack.onPosition)).toList

          DefensiveCounterUseCostAndTargets(msg, cardHolder, targets, effectRegistry)
        } else {
          msg.copy(leftToAsk = msg.leftToAsk.tail)
        }
      case a =>
        DefensiveCounterUseCostAndTargets(msg, cardHolder, List(), effectRegistry)
    }
  }
}
