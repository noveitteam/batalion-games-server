package cz.bataliongames.firstenchanter.cards

import akka.actor.{Actor, ActorLogging}
import cz.noveit.games.cardgame.engine.card._
import cz.noveit.games.cardgame.engine.card.EvaluateAttackWithCard
import cz.noveit.games.cardgame.engine.card.EvaluateCounterUseCard
import cz.noveit.games.cardgame.engine.card.EvaluateUseCard
import cz.noveit.games.cardgame.engine.card.helpers.ShowCardOnTable
import cz.noveit.games.cardgame.engine.card.helpers.possibilities.UseCostOnly
import cz.noveit.games.cardgame.engine.game.session.{CardConsumed, CardDestroyerBy}

/**
 * Created by Wlsek on 25.6.14.
 */
class RageofAir(cardHolder: CardHolder, val effectRegistry: CardStateEffectsRegistry) extends CardActor {
  def evaluateUse(msg: EvaluateUseCard): EvaluateUsage = {
    val position = msg.ts.positionForAlias(msg.usage.onPosition)
    var changes: List[CardDestroyerBy] = List()

    var newTableState = msg.ts
    msg.ts.players.map(player => {
       player.hands.zipWithIndex.map(zipped => OnTableCardPosition(zipped._2, player.nickname)).map(pos => {
         newTableState = newTableState.moveCardFromTableToGraveyard(position)
         changes = CardDestroyerBy(pos, position) :: changes
       })
    })

    EvaluateUsage(newTableState,  CardConsumed(position, cardHolder) :: changes ::: msg.changesStack)
  }

  def evaluateCounterUse(msg: EvaluateCounterUseCard): EvaluateUsage = {
    EvaluateUsage(msg.ts.copy(counterUsedCards = msg.ts.counterUsedCards.tail), msg.changesStack)
  }

  def evaluateAttackWithCard(msg: EvaluateAttackWithCard): EvaluateAttack = {
    EvaluateAttack(msg.ts.copy(attackedCards = msg.ts.attackedCards.tail), msg.changesStack)
  }

  def evaluateDefendWithCard(msg: EvaluateDefendWithCard): EvaluateAttack = {
    EvaluateAttack(msg.ts.copy(defendedCards = msg.ts.defendedCards.tail), msg.changesStack)
  }

  def useCard(msg: UseCard): TableStateAfterParametrizedUsage = {
    ShowCardOnTable(msg)
  }

  def counterUseCard(msg: CounterUseCard): TableStateAfterParametrizedCounterUsage = {
    TableStateAfterParametrizedCounterUsage(msg, msg.ts, None)
  }

  def attackWithCard(msg: AttackWithCard): TableStateAfterParametrizedAttack = {
    TableStateAfterParametrizedAttack(msg, msg.ts, None)
  }

  def defendWithCard(msg: DefendWithCard): TableStateAfterParametrizedDefense = {
    TableStateAfterParametrizedDefense(msg, msg.ts, None)
  }

  def askForPossibilities(msg: AskForPossibilities): AskForPossibilities = {
    UseCostOnly(msg, cardHolder, effectRegistry)
  }
}
