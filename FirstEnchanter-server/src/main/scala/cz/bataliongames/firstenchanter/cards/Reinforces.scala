package cz.bataliongames.firstenchanter.cards

import akka.actor.{Actor, ActorLogging}
import cz.noveit.games.cardgame.engine.card._
import cz.noveit.games.cardgame.engine.card.EvaluateAttackWithCard
import cz.noveit.games.cardgame.engine.card.EvaluateCounterUseCard
import cz.noveit.games.cardgame.engine.card.EvaluateUseCard
import cz.noveit.games.cardgame.engine.card.helpers.{ConsumeCardAfterUse, ShowCardOnTable}
import cz.noveit.games.cardgame.engine.card.helpers.possibilities.UseCostOnly
import cz.noveit.games.cardgame.engine.card.helpers.generator.GenerateCards


import akka.pattern.ask
import akka.util.Timeout
import cz.noveit.games.cardgame.engine.game.session.CardGenerated
import scala.concurrent.duration._
import scala.concurrent.Await

/**
 * Created by Wlsek on 25.6.14.
 */
class Reinforces(cardHolder: CardHolder, val effectRegistry: CardStateEffectsRegistry) extends CardActor {
  def evaluateUse(msg: EvaluateUseCard): EvaluateUsage = {
    val position = msg.ts.positionForAlias(msg.usage.onPosition)
    msg.ts.players.find(p => p.nickname == position.player).map(player => {
      val generated = GenerateCards(player.deck, 4)
      val newHands = generated._2 ++ player.hands.filterNot(c => c == player.hands(position.positionId))

      ConsumeCardAfterUse(
        msg.copy(
          ts = msg.ts.copy(
            players =
              player.copy(
                deck = generated._1,
                hands = newHands
              ) :: msg.ts.players.filterNot(p => p.nickname == player.nickname)
          ),
          changesStack = generated._2.map(g => CardGenerated(InHandCardPosition(newHands.indexOf(g), player.nickname), g)).toList ::: msg.changesStack
        )
      )

    }).getOrElse({
      EvaluateUsage(msg.ts.removeUsedCardCard(msg.usage.onPosition), msg.changesStack)
    })
  }

  def evaluateCounterUse(msg: EvaluateCounterUseCard): EvaluateUsage = {
    EvaluateUsage(msg.ts.copy(counterUsedCards = msg.ts.counterUsedCards.tail), msg.changesStack)
  }

  def evaluateAttackWithCard(msg: EvaluateAttackWithCard): EvaluateAttack = {
    EvaluateAttack(msg.ts.copy(attackedCards = msg.ts.attackedCards.tail), msg.changesStack)
  }

  def evaluateDefendWithCard(msg: EvaluateDefendWithCard): EvaluateAttack = {
    EvaluateAttack(msg.ts.copy(defendedCards = msg.ts.defendedCards.tail), msg.changesStack)
  }

  def useCard(msg: UseCard): TableStateAfterParametrizedUsage = {
    ShowCardOnTable(msg)
  }

  def counterUseCard(msg: CounterUseCard): TableStateAfterParametrizedCounterUsage = {
    TableStateAfterParametrizedCounterUsage(msg, msg.ts, None)
  }

  def attackWithCard(msg: AttackWithCard): TableStateAfterParametrizedAttack = {
    TableStateAfterParametrizedAttack(msg, msg.ts, None)
  }

  def defendWithCard(msg: DefendWithCard): TableStateAfterParametrizedDefense = {
    TableStateAfterParametrizedDefense(msg, msg.ts, None)
  }

  def askForPossibilities(msg: AskForPossibilities): AskForPossibilities = {
    val position = msg.ask.tableState.positionForAlias(msg.leftToAsk.head)

    implicit val timeout = Timeout(15 seconds)
    val tableStateInFuture = sender() ? PreEvaluate(PreEvaluateAction.PreEvaluateActionUse, msg.ask.tableState, List(), new CardStateEffectsRegistry(effectRegistry.effects))
    val state = Await.result(tableStateInFuture, 15.seconds).asInstanceOf[PreEvaluatedState]

    msg.ask.tableState.players.find(p => p.nickname == position.player).map(player => {
      if(player.hands.size < msg.ask.tableState.limits.maxCardsInHand) {
        UseCostOnly(msg, cardHolder,  effectRegistry)
      } else {
        msg.copy(leftToAsk = msg.leftToAsk.tail)
      }
    }).getOrElse({
      msg.copy(leftToAsk = msg.leftToAsk.tail)
    })
  }
}
