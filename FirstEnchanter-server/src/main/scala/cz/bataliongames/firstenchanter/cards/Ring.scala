package cz.bataliongames.firstenchanter.cards

import akka.actor.{Actor, ActorLogging}
import com.google.protobuf.Message
import cz.noveit.games.cardgame.engine.card._
import cz.noveit.games.cardgame.engine.card.EvaluateAttackWithCard
import cz.noveit.games.cardgame.engine.card.EvaluateCounterUseCard
import cz.noveit.games.cardgame.engine.card.EvaluateUseCard
import cz.noveit.games.cardgame.engine.card.helpers.{ShowCardOnTable, ConsumeCardAfterUse}
import cz.noveit.games.cardgame.engine.card.helpers.possibilities.UseCostAndTargets

import akka.pattern.ask
import akka.util.Timeout
import cz.noveit.games.cardgame.engine.game.session.{CardBuffed, AddedEquipmentToCard}
import cz.noveit.games.cardgame.engine.handlers.helpers.serialization.CardPositionToProto
import scala.concurrent.duration._
import scala.concurrent.Await


/**
 * Created by Wlsek on 25.6.14.
 */
class Ring(cardHolder: CardHolder, val effectRegistry: CardStateEffectsRegistry) extends CardActor {

  def evaluateUse(msg: EvaluateUseCard): EvaluateUsage = {
    msg.usage.target.map {
      case cis: CardRefIsTarget =>
        msg.ts.cardForPositionRef(cis.position).map(c => {
          if (!effectRegistry.existsEffect(cis.position, List(Tags.ring), msg.ts)) {
            var tempTableState = msg.ts

            val generatedForEffect1 = tempTableState.duplicateAlias(msg.usage.onPosition)
            tempTableState = generatedForEffect1._1

            val generatedForEffect2 = tempTableState.duplicateAlias(msg.usage.onPosition)
            tempTableState = generatedForEffect2._1

            tempTableState = effectRegistry.addEffectToCard(cis.position, new SpellBoostCardEffect {
              override val source = Some(generatedForEffect1._2)
              override val spellBoost = cardHolder.attack
              override def isEffectSerializable: Boolean = false
              override def serializeEffect(source: CardPosition, target: CardTarget): Message = throw new NotImplementedError("Not serializable")
            }, tempTableState)

            tempTableState = effectRegistry.addEffectToCard(cis.position, new IncreaseAttackEffect {
              override val source = Some(generatedForEffect2._2)
              override val increaseAttackValue: Int = cardHolder.attack
              override def isEffectSerializable: Boolean = true
              override def serializeEffect(s: CardPosition, target: CardTarget): Message = {
                target match {
                  case t: CardIsTarget =>
                    cz.noveit.proto.firstenchanter.FirstEnchanterMatch.ChangeAttackEffect
                      .newBuilder()
                      .setTargetedCard(CardPositionToProto(t.position))
                      .setSourceCard(CardPositionToProto(s))
                      .setBy(2)
                      .build()

                  case t => throw new NotImplementedError("Serialization not implemented")
                }

              }
            }, tempTableState)

            val position = tempTableState.positionForAlias(cis.position)
            ConsumeCardAfterUse(msg.copy(
              ts = tempTableState,
              changesStack =
                AddedEquipmentToCard(position, List(Tags.equip, Tags.ring)) :: CardBuffed(position) :: msg.changesStack
            ))
          } else {
            EvaluateUsage(msg.ts.copy(usedCards = msg.ts.usedCards.tail), msg.changesStack)
          }
        }).getOrElse({
          EvaluateUsage(msg.ts.copy(usedCards = msg.ts.usedCards.tail), msg.changesStack)
        })
      case t =>
        log.error("Ring: Target error!")
        EvaluateUsage(msg.ts.copy(usedCards = msg.ts.usedCards.tail), msg.changesStack)
    }.getOrElse({
      EvaluateUsage(msg.ts.copy(usedCards = msg.ts.usedCards.tail), msg.changesStack)
    })
  }

  def evaluateCounterUse(msg: EvaluateCounterUseCard): EvaluateUsage = {
    EvaluateUsage(msg.ts.copy(counterUsedCards = msg.ts.counterUsedCards.tail), msg.changesStack)
  }

  def evaluateAttackWithCard(msg: EvaluateAttackWithCard): EvaluateAttack = {
    EvaluateAttack(msg.ts.copy(attackedCards = msg.ts.attackedCards.tail), msg.changesStack)
  }

  def evaluateDefendWithCard(msg: EvaluateDefendWithCard): EvaluateAttack = {
    EvaluateAttack(msg.ts.copy(defendedCards = msg.ts.defendedCards.tail), msg.changesStack)
  }

  def useCard(msg: UseCard): TableStateAfterParametrizedUsage = {
    ShowCardOnTable(msg)
  }

  def counterUseCard(msg: CounterUseCard): TableStateAfterParametrizedCounterUsage = {
    TableStateAfterParametrizedCounterUsage(msg, msg.ts, None)
  }

  def attackWithCard(msg: AttackWithCard): TableStateAfterParametrizedAttack = {
    TableStateAfterParametrizedAttack(msg, msg.ts, None)
  }

  def defendWithCard(msg: DefendWithCard): TableStateAfterParametrizedDefense = {
    TableStateAfterParametrizedDefense(msg, msg.ts, None)
  }

  def askForPossibilities(msg: AskForPossibilities): AskForPossibilities = {
    msg.ask match {
      case a: AskPossibleUsageForPlayer =>
        val position = msg.ask.tableState.positionForAlias(msg.leftToAsk.head)
        implicit val timeout = Timeout(15 seconds)
        val tableStateInFuture = sender() ? PreEvaluate(PreEvaluateAction.PreEvaluateActionUse, msg.ask.tableState, List(), new CardStateEffectsRegistry(effectRegistry.effects))
        val state = Await.result(tableStateInFuture, 15.seconds).asInstanceOf[PreEvaluatedState]

        val cardsWithRing = state.effectRegistry.filterEffectsByTags(List(Tags.ring), state.tableState)
          .map(effect => effect.effectIsOn match {
          case t: CardRefIsTarget =>
            Some(state.tableState.positionForAlias(t.position))
          case t: CardIsTarget =>
            Some(t.position)
          case t =>
            None
        }).filter(f => f.exists(e => e.isInstanceOf[OnTableCardPosition])).map(f => f.get)

        val targets = state.tableState.players.filter(p => state.tableState.getPlayerTeam(position.player) contains (p.nickname)).map(player => {
          player.onTable.filter(c => c.tags.contains(Tags.mage)).zipWithIndex.filter(zipped =>
            cardsWithRing.find(pr => pr =? OnTableCardPosition(zipped._2, player.nickname)).isEmpty
          ).map(zipped => zipped._1)
        }).flatten.map(c => {
          a.tableState.positionForCardHolder(c).map(p =>  CardIsTarget(p))
        }).filter(c => c.isDefined).map(c => c.get)

        UseCostAndTargets(msg, cardHolder, targets, effectRegistry)
      case a =>
        UseCostAndTargets(msg, cardHolder, List(), effectRegistry)
    }
  }
}
