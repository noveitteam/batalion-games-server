package cz.bataliongames.firstenchanter.cards

import akka.actor.{Actor, ActorLogging}
import cz.noveit.games.cardgame.engine.card._
import cz.noveit.games.cardgame.engine.card.helpers.possibilities.{DefensiveCounterUseCostAndTargets, CounterUseCostOnly}
import cz.noveit.games.cardgame.engine.card.helpers.{ShowDefendingCardOnTable, ShowCounterUsingCardOnTable, ConsumeCardAfterCounterUse, ConsumeCardAfterUse}
import cz.noveit.games.cardgame.engine.card.helpers.generator.GenerateCards
import cz.noveit.games.cardgame.engine.card.CounterUseCard
import cz.noveit.games.cardgame.engine.card.TableStateAfterParametrizedAttack
import cz.noveit.games.cardgame.engine.card.EvaluateAttackWithCard
import cz.noveit.games.cardgame.engine.card.UseCard
import cz.noveit.games.cardgame.engine.card.AttackWithCard
import cz.noveit.games.cardgame.engine.card.TableStateAfterParametrizedDefense
import cz.noveit.games.cardgame.engine.card.EvaluateUseCard
import cz.noveit.games.cardgame.engine.card.EvaluateDefendWithCard
import cz.noveit.games.cardgame.engine.card.InHandCardPosition
import cz.noveit.games.cardgame.engine.card.EvaluateAttack
import cz.noveit.games.cardgame.engine.card.DefendWithCard
import cz.noveit.games.cardgame.engine.card.AskForPossibilities
import cz.noveit.games.cardgame.engine.card.TableStateAfterParametrizedCounterUsage
import cz.noveit.games.cardgame.engine.card.EvaluateCounterUseCard
import cz.noveit.games.cardgame.engine.card.TableStateAfterParametrizedUsage
import cz.noveit.games.cardgame.engine.card.EvaluateUsage

import akka.pattern.ask
import akka.util.Timeout
import cz.noveit.games.cardgame.engine.game.session.{CardGenerated, CardDestroyerBy}
import scala.concurrent.duration._
import scala.concurrent.Await

/**
 * Created by Wlsek on 25.6.14.
 */

class Sacrifice(cardHolder: CardHolder, val effectRegistry: CardStateEffectsRegistry) extends CardActor {

  def evaluateUse(msg: EvaluateUseCard): EvaluateUsage = {
    EvaluateUsage(msg.ts.copy(usedCards = msg.ts.usedCards.tail), msg.changesStack)
  }

  def evaluateCounterUse(msg: EvaluateCounterUseCard): EvaluateUsage = {
    EvaluateUsage(msg.ts.copy(usedCards = msg.ts.usedCards.tail), msg.changesStack)
  }

  def evaluateAttackWithCard(msg: EvaluateAttackWithCard): EvaluateAttack = {
    EvaluateAttack(msg.ts.copy(attackedCards = msg.ts.attackedCards.tail), msg.changesStack)
  }

  def evaluateDefendWithCard(msg: EvaluateDefendWithCard): EvaluateAttack = {
    val defendPosition = msg.ts.positionForAlias(msg.defend.defendWith)
    val player = msg.ts.players.find(p => p.nickname == defendPosition.player)
    if (player.isDefined) {
      if (player.get.charm.affordAble_?(cardHolder.price)) {
        msg.defend.against match {
          case t: PlayerTeamIsTarget =>
            var newTableState = msg.ts
            var changes = msg.changesStack

            msg.ts.players.filter(p => t.team.contains(p.nickname)).map(enemy => {
              val attackedCardsForEnemy = msg.ts.attackedCards.filter(ac => msg.ts.positionForAlias(ac.attack.onPosition).player == enemy.nickname)

              val generated = GenerateCards(enemy.deck, attackedCardsForEnemy.size)
              var cIndex = enemy.hands.size - 1

              msg.ts.attackedCards.map(ac => {
                changes = CardDestroyerBy(msg.ts.positionForAlias(ac.attack.onPosition), defendPosition) :: generated._2.map(c => {
                  cIndex += 1
                  CardGenerated(InHandCardPosition(cIndex, enemy.nickname), c)
                }).toList ::: changes
              })

              msg.ts.attackedCards.map(ac => {
                newTableState = newTableState
                  .moveCardFromTableToGraveyard(ac.attack.onPosition)
                  .removeAttackedCard(ac.attack.onPosition)
              })

              newTableState
                .transformDeckForPlayer(enemy.nickname, generated._1)
                .addCardsToHandForPlayer(enemy.nickname, generated._2)
            })

            EvaluateAttack(newTableState.removeDefendedCard(msg.defend.defendWith), changes)


          case t =>
            log.debug("{} Unknown target! {}", cardHolder.id, t)
            EvaluateAttack(msg.ts.copy(defendedCards = msg.ts.defendedCards.tail), msg.changesStack)
        }
      } else {
        EvaluateAttack(msg.ts.copy(defendedCards = msg.ts.defendedCards.tail), msg.changesStack)
      }
    } else {
      EvaluateAttack(msg.ts.copy(defendedCards = msg.ts.defendedCards.tail), msg.changesStack)
    }
  }

  def useCard(msg: UseCard): TableStateAfterParametrizedUsage = {
    TableStateAfterParametrizedUsage(msg, msg.ts, None)
  }

  def counterUseCard(msg: CounterUseCard): TableStateAfterParametrizedCounterUsage = {
    TableStateAfterParametrizedCounterUsage(msg, msg.ts, None)
  }

  def attackWithCard(msg: AttackWithCard): TableStateAfterParametrizedAttack = {
    TableStateAfterParametrizedAttack(msg, msg.ts, None)
  }

  def defendWithCard(msg: DefendWithCard): TableStateAfterParametrizedDefense = {
    ShowDefendingCardOnTable(msg)
  }

  def askForPossibilities(msg: AskForPossibilities): AskForPossibilities = {

    val position = msg.ask.tableState.positionForAlias(msg.leftToAsk.head)

    implicit val timeout = Timeout(15 seconds)
    val tableStateInFuture = sender() ? PreEvaluate(PreEvaluateAction.PreEvaluateActionUse, msg.ask.tableState, List(), new CardStateEffectsRegistry(effectRegistry.effects))

    val state = Await.result(tableStateInFuture, 15.seconds).asInstanceOf[PreEvaluatedState]
    val enemies = msg.ask.tableState.getEnemyTeam(msg.ask.tableState.positionForAlias(msg.leftToAsk.head).player)

    val targets = msg
      .ask
      .tableState
      .attackedCards.find(p => enemies contains (state.tableState.positionForAlias(p.attack.onPosition).player))
      .map(pos => PlayerTeamIsTarget(enemies)).toList

    DefensiveCounterUseCostAndTargets(msg, cardHolder, targets, effectRegistry)
  }
}
