package cz.bataliongames.firstenchanter.cards

import akka.actor.{Actor, ActorLogging}
import com.google.protobuf.Message
import cz.noveit.games.cardgame.engine.card._
import cz.noveit.games.cardgame.engine.card.EvaluateAttackWithCard
import cz.noveit.games.cardgame.engine.card.EvaluateCounterUseCard
import cz.noveit.games.cardgame.engine.card.EvaluateUseCard
import cz.noveit.games.cardgame.engine.card.helpers.{ShowCardOnTable, ConsumeCardAfterUse}
import cz.noveit.games.cardgame.engine.card.helpers.possibilities.UseCostAndTargets


import akka.pattern.ask
import akka.util.Timeout
import cz.noveit.games.cardgame.engine.game.session.{CardChangeDefense, CardBuffed, AddedEquipmentToCard}
import cz.noveit.games.cardgame.engine.handlers.helpers.serialization.CardPositionToProto
import scala.concurrent.duration._
import scala.concurrent.Await

/**
 * Created by Wlsek on 25.6.14.
 */
class   Shield(cardHolder: CardHolder, val effectRegistry: CardStateEffectsRegistry) extends CardActor {

  def evaluateUse(msg: EvaluateUseCard): EvaluateUsage = {
    msg.usage.target.map {
      case cis: CardRefIsTarget =>
        msg.ts.cardForPositionRef(cis.position).map(c => {
          if (
            !effectRegistry.existsEffect(cis.position, List(Tags.shield), msg.ts)
          ) {

            val generated = msg.ts.duplicateAlias(cis.position)

           val newTs = effectRegistry.addEffectToCard(cis.position, new IncreaseDefenseCardEffect {
              val source = Some(generated._2)
              val increaseDefenseValue: Int = cardHolder.defense
              override def isEffectSerializable = true
              override def serializeEffect(s: CardPosition, target: CardTarget): Message = {
                target match {
                  case t: CardIsTarget =>
                    cz.noveit.proto.firstenchanter.FirstEnchanterMatch.ChangeDefenseEffect
                      .newBuilder()
                      .setTargetedCard(CardPositionToProto(t.position))
                      .setSourceCard(CardPositionToProto(s))
                      .setBy(2)
                      .build()

                  case t => throw new NotImplementedError("Serialization not implemented")
                }

              }
            }, generated._1)


            val consumed =  ConsumeCardAfterUse(msg.copy(
              ts = newTs,
              changesStack =
                AddedEquipmentToCard(newTs.positionForAlias(generated._2),  List(Tags.equip, Tags.shield)) ::
                  CardBuffed(newTs.positionForAlias(generated._2)) ::
                  CardChangeDefense(newTs.positionForAlias(generated._2), cardHolder.defense) ::
                  msg.changesStack
            ))
            log.debug("Returning consumed after evaluating shield usage.")
            consumed
          } else {
            log.debug("Cannot evaluate usage, because this card cant be used on target.")
            EvaluateUsage(msg.ts.copy(usedCards = msg.ts.usedCards.tail), msg.changesStack)
          }
        }).getOrElse({
          log.debug("Cannot find position for shield card ref.")
          EvaluateUsage(msg.ts.copy(usedCards = msg.ts.usedCards.tail), msg.changesStack)
        })
      case t =>
        log.error("{} unknown target!", cardHolder.id)
        EvaluateUsage(msg.ts.copy(usedCards = msg.ts.usedCards.tail), msg.changesStack)
    }.getOrElse({
      EvaluateUsage(msg.ts.copy(usedCards = msg.ts.usedCards.tail), msg.changesStack)
    })
  }

  def evaluateCounterUse(msg: EvaluateCounterUseCard): EvaluateUsage = {
    EvaluateUsage(msg.ts.copy(counterUsedCards = msg.ts.counterUsedCards.tail), msg.changesStack)
  }

  def evaluateAttackWithCard(msg: EvaluateAttackWithCard): EvaluateAttack = {
    EvaluateAttack(msg.ts.copy(attackedCards = msg.ts.attackedCards.tail), msg.changesStack)
  }

  def evaluateDefendWithCard(msg: EvaluateDefendWithCard): EvaluateAttack = {
    EvaluateAttack(msg.ts.copy(defendedCards = msg.ts.defendedCards.tail), msg.changesStack)
  }


  def useCard(msg: UseCard): TableStateAfterParametrizedUsage = {
    ShowCardOnTable(msg)
  }

  def counterUseCard(msg: CounterUseCard): TableStateAfterParametrizedCounterUsage = {
    TableStateAfterParametrizedCounterUsage(msg, msg.ts, None)
  }

  def attackWithCard(msg: AttackWithCard): TableStateAfterParametrizedAttack = {
    TableStateAfterParametrizedAttack(msg, msg.ts, None)
  }

  def defendWithCard(msg: DefendWithCard): TableStateAfterParametrizedDefense = {
    TableStateAfterParametrizedDefense(msg, msg.ts, None)
  }

  def askForPossibilities(msg: AskForPossibilities): AskForPossibilities = {
    msg.ask match {
      case a: AskPossibleUsageForPlayer =>
        val position = msg.ask.tableState.positionForAlias(msg.leftToAsk.head)
        implicit val timeout = Timeout(15 seconds)
        val tableStateInFuture = sender() ? PreEvaluate(PreEvaluateAction.PreEvaluateActionUse, msg.ask.tableState, List(), new CardStateEffectsRegistry(effectRegistry.effects))
        val state = Await.result(tableStateInFuture, 15.seconds).asInstanceOf[PreEvaluatedState]

        val cardsWithRing = state.effectRegistry.filterEffectsByTags(List(Tags.shield), state.tableState)
          .map(effect => effect.effectIsOn match {
          case t: CardRefIsTarget =>
            Some(state.tableState.positionForAlias(t.position))
          case t: CardIsTarget =>
            Some(t.position)
          case t =>
            None
        }).filter(f => f.exists(e => e.isInstanceOf[OnTableCardPosition])).map(f => f.get)

        val targets = state.tableState.players.filter(p => state.tableState.getPlayerTeam(position.player) contains (p.nickname)).map(player => {
          player.onTable.filter(c => c.tags.contains(Tags.soldier)).zipWithIndex.filter(zipped =>
            cardsWithRing.find(pr => pr =? OnTableCardPosition(zipped._2, player.nickname)).isEmpty
          ).map(zipped => zipped._1)
        }).flatten.map(c => {
          a.tableState.positionForCardHolder(c).map(p => CardIsTarget(p))
        }).filter(c => c.isDefined).map(c => c.get)

        UseCostAndTargets(msg, cardHolder, targets, effectRegistry)
      case a =>
        UseCostAndTargets(msg, cardHolder, List(), effectRegistry)
    }
  }
}
