package cz.bataliongames.firstenchanter.cards

import akka.actor.{Actor, ActorLogging}
import cz.noveit.games.cardgame.engine.card._
import cz.noveit.games.cardgame.engine.card.EvaluateAttackWithCard
import cz.noveit.games.cardgame.engine.card.EvaluateCounterUseCard
import cz.noveit.games.cardgame.engine.card.EvaluateUseCard
import cz.noveit.games.cardgame.engine.card.helpers.{ConsumeCardAfterCounterUse, ShowCounterUsingCardOnTable}
import cz.noveit.games.cardgame.engine.card.helpers.possibilities.CounterUseCostAndTargets
import cz.noveit.games.cardgame.engine.card.helpers.transformation.{DealDamageToPlayerTeam, DealDamage}


import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import scala.concurrent.Await

/**
 * Created by Wlsek on 25.6.14.
 */
class Spellreact(cardHolder: CardHolder, val effectRegistry: CardStateEffectsRegistry) extends CardActor {
  def evaluateUse(msg: EvaluateUseCard): EvaluateUsage = {
    EvaluateUsage(msg.ts.copy(usedCards = msg.ts.usedCards.tail), msg.changesStack)
  }

  def evaluateCounterUse(msg: EvaluateCounterUseCard): EvaluateUsage = {

    msg.ts.usedCards.find(c => {
      c.usage.cardHolder.tags.contains(Tags.harmful) && c.usage.cardHolder.tags.contains(Tags.spell)
    }).map(c => {
      var attack = c.usage.cardHolder.attack
      val position = msg.ts.positionForAlias(c.usage.onPosition)
      val team = msg.ts.getPlayerTeam(position.player)

      effectRegistry
        .filterEffectsByPlayerName(position.player, msg.ts)
        .filter(
          e => e.effect.isInstanceOf[SpellBoostCardEffect] &&
            (e.effect.element == CardEffectElement.CardEffectElementEarth || e.effect.element == CardEffectElement.CardEffectElementNeutral)
        )
        .map(e => {
        val effect = e.effect.asInstanceOf[SpellBoostCardEffect]
        attack += effect.spellBoost
      })

      val damageDealt = DealDamage(DealDamageToPlayerTeam(c.usage.onPosition, position.player, attack), msg.ts, effectRegistry)
      ConsumeCardAfterCounterUse(msg.copy(ts = damageDealt._1, changesStack = damageDealt._2 ::: msg.changesStack))
    }).getOrElse({
      EvaluateUsage(msg.ts.copy(counterUsedCards = msg.ts.counterUsedCards.tail), msg.changesStack)
    })
  }

  def evaluateAttackWithCard(msg: EvaluateAttackWithCard): EvaluateAttack = {
    EvaluateAttack(msg.ts.copy(attackedCards = msg.ts.attackedCards.tail), msg.changesStack)
  }

  def evaluateDefendWithCard(msg: EvaluateDefendWithCard): EvaluateAttack = {
    EvaluateAttack(msg.ts.copy(defendedCards = msg.ts.defendedCards.tail), msg.changesStack)
  }

  def useCard(msg: UseCard): TableStateAfterParametrizedUsage = {
    TableStateAfterParametrizedUsage(msg, msg.ts, None)
  }

  def counterUseCard(msg: CounterUseCard): TableStateAfterParametrizedCounterUsage = {
    ShowCounterUsingCardOnTable(msg)
  }

  def attackWithCard(msg: AttackWithCard): TableStateAfterParametrizedAttack = {
    TableStateAfterParametrizedAttack(msg, msg.ts, None)
  }

  def defendWithCard(msg: DefendWithCard): TableStateAfterParametrizedDefense = {
    TableStateAfterParametrizedDefense(msg, msg.ts, None)
  }

  def askForPossibilities(msg: AskForPossibilities): AskForPossibilities = {
    msg.ask match {
      case a: AskPossibleCounterUsageForPlayer =>

        val position = msg.ask.tableState.positionForAlias(msg.leftToAsk.head)


        implicit val timeout = Timeout(15 seconds)
        val tableStateInFuture = sender() ? PreEvaluate(PreEvaluateAction.PreEvaluateActionCounterUse, msg.ask.tableState, List(), new CardStateEffectsRegistry(effectRegistry.effects))

        val state = Await.result(tableStateInFuture, 15.seconds).asInstanceOf[PreEvaluatedState]
        val enemies = msg.ask.tableState.getEnemyTeam(msg.ask.tableState.positionForAlias(msg.leftToAsk.head).player)
        val targets = state.tableState
          .usedCards
          .filter(c =>
          c.usage.cardHolder.tags.contains(Tags.harmful) &&
            c.usage.cardHolder.tags.contains(Tags.spell) &&
            enemies.contains(state.tableState.positionForAlias(c.usage.onPosition)))
          .map(f => CardRefIsTarget(f.usage.onPosition))

        CounterUseCostAndTargets(msg, cardHolder, targets, effectRegistry)
      case a =>
        CounterUseCostAndTargets(msg, cardHolder, List(), effectRegistry)
    }
  }
}
