package cz.bataliongames.firstenchanter.cards

import akka.actor.{Actor, ActorLogging}
import cz.noveit.games.cardgame.engine.card._
import cz.noveit.games.cardgame.engine.card.helpers.possibilities.DefensiveCounterUseCostAndTargets
import cz.noveit.games.cardgame.engine.card.helpers.ShowDefendingCardOnTable
import cz.noveit.games.cardgame.engine.card.helpers.transformation.DealDamage
import cz.noveit.games.cardgame.engine.card.CounterUseCard
import cz.noveit.games.cardgame.engine.card.TableStateAfterParametrizedAttack
import cz.noveit.games.cardgame.engine.card.EvaluateAttackWithCard
import cz.noveit.games.cardgame.engine.card.helpers.transformation.DealDamageToPlayerTeam
import cz.noveit.games.cardgame.engine.card.UseCard
import cz.noveit.games.cardgame.engine.card.AttackWithCard
import cz.noveit.games.cardgame.engine.card.TableStateAfterParametrizedDefense
import cz.noveit.games.cardgame.engine.card.EvaluateUseCard
import cz.noveit.games.cardgame.engine.card.EvaluateDefendWithCard
import cz.noveit.games.cardgame.engine.card.EvaluateAttack
import cz.noveit.games.cardgame.engine.card.DefendWithCard
import cz.noveit.games.cardgame.engine.card.AskForPossibilities
import cz.noveit.games.cardgame.engine.card.TableStateAfterParametrizedCounterUsage
import cz.noveit.games.cardgame.engine.card.EvaluateCounterUseCard
import cz.noveit.games.cardgame.engine.card.TableStateAfterParametrizedUsage
import cz.noveit.games.cardgame.engine.card.EvaluateUsage


import akka.pattern.ask
import akka.util.Timeout
import cz.noveit.games.cardgame.engine.game.session.CardDestroyerBy
import scala.concurrent.duration._
import scala.concurrent.Await

/**
 * Created by Wlsek on 25.6.14.
 */
class Traitor(cardHolder: CardHolder, val effectRegistry: CardStateEffectsRegistry) extends CardActor {
  def evaluateUse(msg: EvaluateUseCard): EvaluateUsage = {
    EvaluateUsage(msg.ts.copy(usedCards = msg.ts.usedCards.tail), msg.changesStack)
  }

  def evaluateCounterUse(msg: EvaluateCounterUseCard): EvaluateUsage = {
    EvaluateUsage(msg.ts.copy(counterUsedCards = msg.ts.counterUsedCards.tail), msg.changesStack)
  }

  def evaluateAttackWithCard(msg: EvaluateAttackWithCard): EvaluateAttack = {
    EvaluateAttack(msg.ts.copy(attackedCards = msg.ts.attackedCards.tail), msg.changesStack)
  }

  def evaluateDefendWithCard(msg: EvaluateDefendWithCard): EvaluateAttack = {
    try {
      msg.defend.against match {
        case t: CardRefIsTarget =>
          val defendWith = msg.ts.positionForAlias(msg.defend.defendWith)
          val target = msg.ts.positionForAlias(t.position)


          msg.ts.attackedCards.find(c => msg.ts.positionForAlias(c.attack.onPosition) =? target).map(ac => {
            var attack = ac.attack.card.attack

            effectRegistry
              .filterEffectsByPlayerName(target.player, msg.ts)
              .filter(
                e => (e.effect.isInstanceOf[IncreaseAttackEffect] || e.effect.isInstanceOf[DecreaseAttackCardEffect]) &&
                  (e.effectIsOn match {
                  case t: CardRefIsTarget =>
                    target =? msg.ts.positionForAlias(t.position)
                  case t: CardIsTarget =>
                    target =? t.position
                  case t => true
                })
              )
              .map(e => {
              e.effect match {
                case e: IncreaseAttackEffect => attack += e.increaseAttackValue
                case e: DecreaseAttackCardEffect => attack -= e.decreaseAttackValue
              }
            })

            if (attack < 0) {
              attack = 0
            }

            val damageDealt = DealDamage(DealDamageToPlayerTeam(t.position, target.player, attack), msg.ts, effectRegistry)
            EvaluateAttack(
              damageDealt._1
                .moveCardFromTableToGraveyard(t.position)
                .removeAttackedCard(t.position)
                .removeDefendedCard(msg.defend.defendWith)
              , CardDestroyerBy(target, defendWith) :: damageDealt._2 ::: msg.changesStack)

          }).getOrElse(
              EvaluateAttack(msg.ts.copy(defendedCards = msg.ts.defendedCards.tail), msg.changesStack)
            )

        case t =>
          log.debug("{} Unknown target! {}", cardHolder.id, t)
          EvaluateAttack(msg.ts.copy(defendedCards = msg.ts.defendedCards.tail), msg.changesStack)
      }
    } catch {
      case t: Throwable =>
        log.debug("Exception while defending with {}", cardHolder.id)
        EvaluateAttack(msg.ts.copy(defendedCards = msg.ts.defendedCards.tail), msg.changesStack)
    }
  }

  def useCard(msg: UseCard): TableStateAfterParametrizedUsage = {
    val position = msg.ts.positionForAlias(msg.usage.onPosition)
    TableStateAfterParametrizedUsage(msg, msg.ts.copy(usedCards = msg.ts.usedCards.filterNot(p => msg.ts.positionForAlias(p.usage.onPosition) =? position)), None)
  }

  def counterUseCard(msg: CounterUseCard): TableStateAfterParametrizedCounterUsage = {
    val position = msg.ts.positionForAlias(msg.usage.onPosition)
    TableStateAfterParametrizedCounterUsage(msg, msg.ts.copy(usedCards = msg.ts.usedCards.filterNot(p => msg.ts.positionForAlias(p.usage.onPosition) =? position)), None)
  }

  def attackWithCard(msg: AttackWithCard): TableStateAfterParametrizedAttack = {
    val position = msg.ts.positionForAlias(msg.attack.onPosition)
    TableStateAfterParametrizedAttack(msg, msg.ts.copy(attackedCards = msg.ts.attackedCards.filterNot(p => msg.ts.positionForAlias(p.attack.onPosition) =? position)), None)
  }

  def defendWithCard(msg: DefendWithCard): TableStateAfterParametrizedDefense = {
    ShowDefendingCardOnTable(msg)
  }

  def askForPossibilities(msg: AskForPossibilities): AskForPossibilities = {


    msg.ask match {
      case a: AskPossibleDefendingPositionsForPlayer =>

        val position = msg.ask.tableState.positionForAlias(msg.leftToAsk.head)

        implicit val timeout = Timeout(15 seconds)
        val tableStateInFuture = sender() ? PreEvaluate(PreEvaluateAction.PreEvaluateActionUse, msg.ask.tableState, List(), new CardStateEffectsRegistry(effectRegistry.effects))

        val state = Await.result(tableStateInFuture, 15.seconds).asInstanceOf[PreEvaluatedState]
        val enemies = msg.ask.tableState.getEnemyTeam(position.player)
        val targets = msg.ask.tableState.attackedCards
          .find(ac => {
          val pos = msg.ask.tableState.positionForAlias(ac.attack.onPosition)
          enemies.contains(pos.player) && ac.attack.card.tags.contains(Tags.creep)
        }).map(c => CardRefIsTarget(c.attack.onPosition)).toList

        DefensiveCounterUseCostAndTargets(msg, cardHolder, targets, effectRegistry)
      case a =>
        DefensiveCounterUseCostAndTargets(msg, cardHolder, List(), effectRegistry)

    }

    /*
        val enemies = msg.ask.tableState.getEnemyTeam(msg.leftToAsk.head.player)
        val targets = msg.ask.tableState.attackedCards.find(ac => enemies.contains(ac.withPosition) && ac.card.tags.contains(Tags.creep)).map(c => CardIsTarget(c.withPosition)).toList

        DefensiveCounterUseCostAndTargets(msg, cardHolder, targets, effectRegistry)*/
  }
}
