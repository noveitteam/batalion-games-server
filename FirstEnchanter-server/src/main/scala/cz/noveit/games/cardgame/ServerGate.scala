package cz.noveit.games.firstenchanter

import akka.actor.{ActorLogging, ActorRef, Props, ActorSystem}
import cz.noveit.connector.websockets.{StopWebSocketServer, StartWebSocketServer, WebSocketsServer}
import cz.noveit.service.{ServiceRegistration, ProtoServiceRegistry, RegisterServiceMessage, Service}
import cz.noveit.games.cardgame.authorization.AuthorizationController
import cz.noveit.database.DatabaseController
import cz.noveit.connector.DispatchController
import com.typesafe.config._
import java.lang.{Double, Long}
import cz.noveit.connector.websockets.StartWebSocketServer
import java.{util, lang}
import java.util.Map.Entry
import cz.noveit.games.cardgame.World
import java.io.File


/**
 * Created with IntelliJ IDEA.
 * User: Wlsek
 * Date: 20.5.13
 * Time: 15:59
 */

object ServerGate extends App with ServiceRegistration  {

  override def main(args: Array[String]): Unit = {
    val config = ConfigFactory.parseFile(new File("application.conf"))
    val actorSystem = ActorSystem("FirstEnchanterCluster", config)
    try {
      val world = new World()(actorSystem, config.getConfig("world"))
      Console.readLine()
      world.destroy
    } catch {
      case t: Throwable => println("Error while running server: " + t.toString)
    } finally {
      actorSystem.shutdown()
    }
  }


}
