package cz.noveit.games.cardgame

import akka.actor.{ActorRef, Props, ActorSystem}
import cz.noveit.connector.websockets.{StopWebSocketServer, StartWebSocketServer, WebSocketsServer}
import cz.noveit.service.ProtoServiceRegistry
import cz.noveit.database.{DatabaseControllerSettings, DatabaseController}
import cz.noveit.games.cardgame.authorization.{AddPublicMessage, AuthorizationController}
import cz.noveit.games.cluster.{ClusterListener}
import com.typesafe.config.Config
import cz.noveit.games.cluster.balancer.ClusterBalancer
import cz.noveit.games.cluster.dispatch.ClusterDispatcher
import cz.noveit.games.cluster.registry.ClusterRegistry
import cz.noveit.games.cardgame.engine.{ConversationController, PublicController, PlayerHandlerFactoryImpl, PlayerController}
import cz.noveit.games.cardgame.engine.handlers.{ServiceHandlersModuleFactoryImpl, ServiceHandlersModuleFactory}
import cz.noveit.smtp.{SmtpConfig, SmtpService}
import cz.noveit.token.TokenService
import cz.noveit.games.cardgame.engine.game.{MatchResultController, MatchController}
import scala.collection.immutable.HashMap
import cz.noveit.games.cardgame.engine.resources.{DeckIconLoader, CardSkinLoader, AvatarLoader}
import com.mongodb.ServerAddress
import akka.actor
import cz.noveit.connector.DispatchController
import cz.noveit.games.cardgame.engine.conversation.ConversationCleanController
import cz.noveit.games.cardgame.engine.unlockable._
import cz.noveit.games.cardgame.authorization.AddPublicMessage
import cz.noveit.connector.websockets.StartWebSocketServer
import scala.Some
import cz.noveit.smtp.SmtpConfig
import cz.noveit.database.DatabaseControllerSettings
import cz.noveit.games.cardgame.engine.code.CodeRedemptionController

/**
 * Created by Wlsek on 28.1.14.
 */

object WorldServices {
  val conversationService= "conversationController"
  val conversationCleanService = "conversationCleanController"
  val SMTPService = "smtpService"
  val databaseService = "databaseController"
  val tokenService = "tokenService"
  val matchService = "matchController"
  val unlockAbleService = "unlockAbleService"
  val codeRedemptionService = "codeRedemptionService"

  val clusterListenerService = "clusterListener"
  val clusterRegistryService = "clusterRegistry"

  val matchResultController = "matchResultController"
  val botConversationService = "botConversationService"
}

class World {
  private var webSocketsServer: Option[ActorRef] = None
  private var avatarLoader: Option[ActorRef] = None
  private var actorSystem: Option[ActorSystem] =  None

  def apply(actorSystem: ActorSystem, config: Config): World = {
    build(actorSystem, config)
    this
  }

  def build(actorSystem: ActorSystem, config: Config) = {
    this.actorSystem = Some(actorSystem)
    val smtpc = SmtpConfig(
      config.getConfig("smtp").getBoolean("tls"),
      config.getConfig("smtp").getBoolean("ssl"),
      config.getConfig("smtp").getInt("port"),
      config.getConfig("smtp").getString("address"),
      config.getConfig("smtp").getString("username"),
      config.getConfig("smtp").getString("password")
    )

    val mailService = actorSystem actorOf(Props(classOf[SmtpService], smtpc), WorldServices.SMTPService)

    val databaseParameters: HashMap[String, String] = HashMap(
      "databaseName" -> "FirstEnchanter",
      "avatarVersionCollection" -> "avatarsVersion",
      "deckIconVersionCollection" -> "deckIconsVersion",
      "cardSkinVersionCollection" -> "cardSkinsVersion",
      "userCollection" -> "users",
      "userUnlockedCollection" -> "usersUnlocked",
      "cardsCollection" -> "cards",
      "decksCollection" -> "decks",
      "userHasCardsCollection" -> "userHasCards",
      "friendshipsCollection" -> "friendships",
      "statCollection" -> "stats",
      "matchCollection" -> "matches",
      "tokensCollection" -> "tokens",
      "newsCollection" -> "news",
      "scoreBoardCollection" -> "scoreBoards",
      "transactionsCollection" -> "transactions",
      "conversationsCollection" -> "conversations",
      "conversationsMembershipCollection" -> "conversationMemberships",
      "conversationsInvitationCollection" -> "conversationInvitations",
      "conversationsBansCollection" -> "conversationBans",
      "botStatisticCollection" -> "botStatistic",
      "botDetailsCollection" -> "botDetails",
      "storeItemsCollection" -> "storeItems",
      "unlockAblesCollection" -> "unlockAbles",
      "unlockAblesHistoryCollection" -> "unlockAblesHistory",
      "codesCollection" -> "codes",
      "codesRedeemedCollection" -> "codesRedeemed"
    )
    val databaseController = actorSystem actorOf(Props(classOf[DatabaseController], DatabaseControllerSettings(List(new ServerAddress("127.0.0.1")))), WorldServices.databaseService)

    val tokenService = actorSystem actorOf(
      Props(
        classOf[TokenService],
        databaseController,
        databaseParameters("databaseName"),
        databaseParameters("tokensCollection"),
        (60 * 60 * 1000).toLong
      ), WorldServices.tokenService
      )



    val aLoader = actorSystem actorOf((Props(classOf[AvatarLoader], databaseParameters)),  "avatarLoader")
    avatarLoader = Some(aLoader)

    val csLoader = actorSystem actorOf((Props(classOf[CardSkinLoader], databaseParameters)),  "cardSkinLoader")
    val diLoader = actorSystem actorOf((Props(classOf[DeckIconLoader], databaseParameters)),  "deckIconLoader")

    val clusterRegistry = actorSystem actorOf(Props[ClusterRegistry], WorldServices.clusterRegistryService)
    val clusterDispatcher = actorSystem actorOf (Props[ClusterDispatcher])
    val clusterBalancer = actorSystem actorOf (Props(classOf[ClusterBalancer], clusterDispatcher))
    val clusterListener = actorSystem actorOf(Props(classOf[ClusterListener], clusterBalancer), WorldServices.clusterListenerService)

    val matchController = actorSystem actorOf(Props(classOf[MatchController], clusterDispatcher, databaseParameters), WorldServices.matchService)


    val wss = actorSystem actorOf Props(classOf[WebSocketsServer], config)
    val messageRegistry = actorSystem actorOf Props[ProtoServiceRegistry]

    val playerController = actorSystem actorOf Props(
      classOf[PlayerController],
      clusterDispatcher,
      clusterRegistry,
      new ServiceHandlersModuleFactoryImpl(),
      new PlayerHandlerFactoryImpl(),
      databaseParameters
    )

    val publicController = actorSystem actorOf Props(
      classOf[PublicController],
      clusterDispatcher,
      clusterRegistry,
      aLoader,
      databaseParameters
    )

    actorSystem actorOf (Props(
      classOf[ConversationController],
      clusterDispatcher,
      databaseParameters
    ), WorldServices.conversationService)


    actorSystem actorOf (Props(
      classOf[ConversationCleanController],
      clusterDispatcher,
      databaseParameters
    ), WorldServices.conversationCleanService)

     actorSystem actorOf(Props(
       classOf[UnlockAbleController],
       databaseController,
       databaseParameters,
       Seq (
        UnlockAbleType.UnlockAbleTypeAvatar   -> classOf[UnlockAvatarRequest],
        UnlockAbleType.UnlockAbleTypeCardSkin -> classOf[UnlockCardSkinRequest],
        UnlockAbleType.UnlockAbleTypeDeckIcon -> classOf[UnlockDeckIconRequest],
        UnlockAbleType.UnlockAbleTypeDeck     -> classOf[UnlockCardsWithDeckRequest],
        UnlockAbleType.UnlockAbleTypeBooster  -> classOf[UnlockCardsWithBoosterRequest]
       )
     ), WorldServices.unlockAbleService)

    actorSystem actorOf (Props(
      classOf[CodeRedemptionController],
      databaseController,
      databaseParameters
    ), WorldServices.codeRedemptionService)


    actorSystem actorOf (Props(
      classOf[MatchResultController]
    ), WorldServices.matchResultController)


    val dispatcher = actorSystem actorOf Props(classOf[DispatchController], messageRegistry)

    val ac = actorSystem actorOf Props(classOf[AuthorizationController], dispatcher, publicController, playerController,  clusterDispatcher, databaseParameters)


    ac ! AddPublicMessage("GetAvatarVersions")
    ac ! AddPublicMessage("UpdateAvatar")

    wss ! StartWebSocketServer("First enchanter connector", ac)
    webSocketsServer = Some(wss)
  }

  def destroy = {
    webSocketsServer.map(ws => ws ! StopWebSocketServer)
    // avatarLoader.map(al => al ! "Stop")

   // Thread.sleep(5000)
  }
}
