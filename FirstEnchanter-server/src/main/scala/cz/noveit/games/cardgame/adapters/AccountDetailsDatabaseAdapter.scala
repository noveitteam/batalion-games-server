package cz.noveit.games.cardgame.adapters

import akka.actor.{ActorSelection, ActorRef, ActorLogging, Actor}
import cz.noveit.database._
import cz.noveit.database.ProcessTaskOnCollection
import com.mongodb.casbah.Imports._

import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import scala.concurrent.Await
/**
 * Created by Wlsek on 19.2.14.
 */



trait AccountDetailsDatabaseMessages extends DatabaseAdapterMessage

case class QueryAccountState(userName: String) extends DatabaseCommandMessage with AccountDetailsDatabaseMessages

case class QueriedAccountState(emailConfirmed: Boolean, selectedStartingDeck: Boolean, inGameMoney: Int) extends DatabaseCommandResultMessage with AccountDetailsDatabaseMessages

case class GetUnlockedAvatarsForUser(userName: String, device: Int) extends DatabaseCommandMessage with AccountDetailsDatabaseMessages

case class UnlockedAvatars(ids: List[Int]) extends DatabaseCommandResultMessage with AccountDetailsDatabaseMessages

case class ChangeAvatarForUser(userName: String, standard: Boolean, id: Int) extends DatabaseCommandMessage with AccountDetailsDatabaseMessages

case object ChangeAvatarForUserFailed extends DatabaseCommandResultMessage with AccountDetailsDatabaseMessages

case object ChangeAvatarForUserSuccessful extends DatabaseCommandResultMessage with AccountDetailsDatabaseMessages

class AccountDetailsDatabaseAdapter(
                                     val databaseController: ActorSelection,
                                     databaseName: String,
                                     userCollection: String,
                                     userUnlockedCollection: String,
                                     avatarVersionsCollection: String,
                                     transactionsCollection: String
                                     ) extends Actor with ActorLogging {
  def receive = {
    case dc: DatabaseCommand =>
      dc.command match {

        case QueryAccountState(ac) => {
          val replyTo = sender

          implicit val timeout = Timeout(5 seconds)

          val future = databaseController ? ProcessTaskOnCollectionAndReplyToSender((c: MongoCollection, s: ActorRef) => {
               val found = c.findOne(MongoDBObject("userName" -> ac))
            s !  found.get.getAs[Int]("inGameAccountBalance").getOrElse(0)
          }, databaseName, transactionsCollection)

          val balance = Await.result(future, timeout.duration).asInstanceOf[Int]

          databaseController ! ProcessTaskOnCollection(
            (c: MongoCollection) => {
              c.findOne(MongoDBObject("userName" -> ac)).map(r => {
                  r.getAs[Boolean]("emailConfirmed").map(ec => {
                  r.getAs[Boolean]("startingDeckSelected").map(sd => {
                    replyTo ! DatabaseResult(QueriedAccountState(ec, sd, balance), dc.replyHash)
                  })
                })
              })
            }, databaseName, userCollection
          )
        }

        case GetUnlockedAvatarsForUser(un, device) =>

          implicit val timeout = Timeout(5 seconds)

          val future = databaseController ? ProcessTaskOnCollectionAndReplyToSender((c: MongoCollection, s: ActorRef) => {
            s ! c.find(MongoDBObject("device" -> device)).map(found => {
              Pair(found.getAs[Int]("id").getOrElse(-1), found.getAs[Boolean]("unlockable").getOrElse(false))
            }).toList
          }, databaseName, avatarVersionsCollection)

          val avatars = Await.result(future, timeout.duration).asInstanceOf[List[Pair[Int, Boolean]]]

          val replyTo = sender
          databaseController ! ProcessTaskOnCollection(
            (c: MongoCollection) => {
              c.findOne(MongoDBObject("user" -> un)).map(found => {
                var content: List[Int] = List()
                val unlocked = found.getAs[MongoDBList]("unlockedAvatars").getOrElse(MongoDBList()).toList.collect {case s: Int => s}
                avatars.map(av => {
                  if(av._2) {
                    if(unlocked.contains(av._1))  content = av._1 :: content
                  } else {
                    content = av._1 :: content
                  }
                })

                replyTo ! DatabaseResult(UnlockedAvatars(content), dc.replyHash)
              })
            }, databaseName, userUnlockedCollection
          )

        case ChangeAvatarForUser(un, std, id) =>
          val replyTo = sender
          databaseController ! ProcessTaskOnCollection(
            (c: MongoCollection) => {
              try {

                if(std){
                  c.update(MongoDBObject("userName" -> un), MongoDBObject("$set" -> MongoDBObject("avatar" -> id)))
                  replyTo ! DatabaseResult(ChangeAvatarForUserSuccessful, dc.replyHash)
                } else {
                  implicit val timeout = Timeout(30 seconds)

                  val future = databaseController ? ProcessTaskOnCollectionAndReplyToSender((c: MongoCollection, s: ActorRef) => {
                    s ! c.find(MongoDBObject("user" -> un, "unlockedAvatars" -> id)).isEmpty
                  }, databaseName, userUnlockedCollection)


                  val empty = Await.result(future, timeout.duration).asInstanceOf[Boolean]

                  if(empty){
                    replyTo ! DatabaseResult(ChangeAvatarForUserFailed, dc.replyHash)
                  }  else {
                    c.update(MongoDBObject("userName" -> un), MongoDBObject("$set" -> MongoDBObject("avatar" -> id)))
                    replyTo ! DatabaseResult(ChangeAvatarForUserSuccessful, dc.replyHash)
                  }
                }

              }  catch {
                case t: Throwable =>
                  replyTo ! DatabaseResult(ChangeAvatarForUserFailed, dc.replyHash)
              }

              replyTo ! DatabaseResult(UnlockedAvatars(c.findOne(MongoDBObject("user" -> un)).map(found => {
                found.getAs[MongoDBList]("unlockedAvatars").getOrElse(MongoDBList()).toList.collect {case s: Int => s}
              }).getOrElse(List())), dc.replyHash)
            }, databaseName, userCollection
          )
      }

    case t => throw new UnsupportedOperationException(t.toString)
  }
}
