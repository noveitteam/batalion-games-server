package cz.noveit.games.cardgame.adapters

import akka.actor._
import cz.noveit.database._
import com.mongodb.casbah.Imports._
import com.mongodb.MongoException.DuplicateKey
import cz.noveit.database.ProcessTaskOnCollection
import cz.noveit.database.DatabaseResult
import cz.noveit.database.DatabaseCommand
import cz.noveit.games.cardgame.authorization.AuthorizedContextPair

/**
 * Created by Wlsek on 6.1.14.
 */


sealed trait AuthorizationDatabaseMessages extends DatabaseAdapterMessage

case class UserQuery(userName: String, password: String, aPair: AuthorizedContextPair) extends DatabaseCommandMessage with AuthorizationDatabaseMessages

case class SetUserLastLogged(uid: String, lastLogged: Long) extends DatabaseCommandMessage with AuthorizationDatabaseMessages

case class UserQueryResultAccept(user: User, aPair: AuthorizedContextPair) extends DatabaseCommandResultMessage with AuthorizationDatabaseMessages

case class UserQueryResultFail(aPair: AuthorizedContextPair) extends DatabaseCommandResultMessage with AuthorizationDatabaseMessages

case class User(nickname: String, uid: String, email: String, lastLogged: Long, avatar: Option[Int] = None, password: Option[String] = None)

case class AddUser(user: User, aPair: AuthorizedContextPair) extends DatabaseCommandMessage with AuthorizationDatabaseMessages

case class RegistrationFailedUserExists(aPair: AuthorizedContextPair) extends DatabaseCommandResultMessage with AuthorizationDatabaseMessages

case class RegistrationFailedUnknown(aPair: AuthorizedContextPair) extends DatabaseCommandResultMessage with AuthorizationDatabaseMessages

case class RegistrationSuccessful(aPair: AuthorizedContextPair, email: String) extends DatabaseCommandResultMessage with AuthorizationDatabaseMessages

case class EmailConfirmed(userName: String) extends DatabaseCommandMessage with AuthorizationDatabaseMessages

class AuthorizationDatabaseAdapter(val databaseController: ActorSelection, databaseName: String, userCollection: String) extends Actor with ActorLogging {

  def receive = {
    case dc: DatabaseCommand =>
      dc.command match {
        case m: UserQuery =>
          val replyTo = sender
          databaseController ! ProcessTaskOnCollection(
            (c: MongoCollection) => {
              c.findOne(MongoDBObject("userName" -> m.userName, "password" -> m.password)).map(found => {
                replyTo ! DatabaseResult(
                  UserQueryResultAccept(
                    User(found.getAs[String]("userName").get.toString, found.getAs[ObjectId]("_id").get.toString, found.getAs[String]("email").get.toString, found.getAs[Long]("lastLogged").getOrElse(0), found.getAs[Int]("avatar")),
                    m.aPair
                  ),
                  dc.replyHash
                )
              }).getOrElse(
                  replyTo ! DatabaseResult(
                    UserQueryResultFail(
                      m.aPair
                    ),
                    dc.replyHash
                  )
                )
            }, databaseName, userCollection
          )

        case m: SetUserLastLogged =>
          databaseController ! ProcessTaskOnCollection(
            (c: MongoCollection) => {
              c.update(MongoDBObject("_id" -> new ObjectId(m.uid)), MongoDBObject("$set" -> MongoDBObject("lastLogged" -> m.lastLogged)))
            }, databaseName, userCollection
          )
        /*
case GetWalletState(id) =>
 val replyTo = sender
 databaseController ! ProcessTaskOnCollection(
   (c: MongoCollection) => {
     c.findOne(MongoDBObject("_id" -> new ObjectId(id))).map(found => {
       replyTo ! DatabaseResult(
         WalletState(found.getAs[Long]("inGameMoney").getOrElse(-1), found.getAs[Long]("microTransactionMoney").getOrElse(-1)),
         dc.replyHash
       )
     })
   }, "CardGame", "Users"
 )
      */
        case AddUser(user, pair) =>
          val replyTo = sender
          databaseController ! ProcessTaskOnCollection(
            (c: MongoCollection) => {
              try {
                c.save(MongoDBObject(
                  "userName" -> user.nickname,
                  "email" -> user.email,
                  "avatar" -> user.avatar.map(a => a).getOrElse(-1),
                  "password" -> user.password.map(p => p).getOrElse(""),
                  "startingDeckSelected" -> false,
                  "emailConfirmed" -> false,
                  "inGameMoney" -> 0L,
                "microTransactionMoney" -> 0L
                ))
                replyTo ! DatabaseResult(RegistrationSuccessful(pair, user.email), dc.replyHash)
              } catch {
                case msg: DuplicateKey =>
                  replyTo ! DatabaseResult(RegistrationFailedUserExists(pair), dc.replyHash)
                case t: Throwable =>
                  replyTo ! DatabaseResult(RegistrationFailedUnknown(pair), dc.replyHash)
              }
            }, databaseName, userCollection
          )

        case EmailConfirmed(un) =>
          databaseController ! ProcessTaskOnCollection(
            (c: MongoCollection) => {
              c.update(MongoDBObject("userName" -> un), MongoDBObject("$set" -> MongoDBObject("emailConfirmed" -> true)))
            }, databaseName, userCollection
          )

      }
    case t => throw new UnsupportedOperationException(t.toString)
  }
}
