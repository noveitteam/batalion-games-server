package cz.noveit.games.cardgame.adapters

import cz.noveit.database._
import akka.actor.{ActorRef, ActorLogging, Actor, ActorSelection}
import com.mongodb.casbah.Imports._
import cz.noveit.database.DatabaseCommand
import scala.util.Random


import akka.util.Timeout
import scala.concurrent.duration._
import akka.pattern.ask
import scala.concurrent.Await

/**
 * Created by Wlsek on 28.5.14.
 */


sealed trait BotDatabaseMessages extends DatabaseAdapterMessage

/*
*
* botDetailsCollection: {
*   "botName" : "name of bot",
*   "avatarId" : 0,
*   "challengeId": 0
* }
*
* "botStatisticCollection : {
*   "rating" : 1520,
*   "win" : 1,
*   "lost" : 1,
*   "numberOfGames" : 1
* }
*
* */

case class GetBotFromDatabase(rating: Int) extends BotDatabaseMessages with DatabaseCommandMessage

case class BotFromDatabase(botName: String, avatarId: Int, challengeId: Int, deckId: String) extends DatabaseCommandResultMessage

case class IncreaseStatistic(rating: Int, win: Int, lost: Int, numberOfGames: Int) extends DatabaseCommandMessage


class BotDatabaseAdapter(val databaseController: ActorSelection, databaseName: String, botDetailsCollection: String, botStatisticCollection: String, decksCollection: String) extends Actor with ActorLogging {

  def receive = {
    case dc: DatabaseCommand =>
      dc.command match {
        case GetBotFromDatabase(rating) =>
          val replyTo = sender

          databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
            val bots = c.find(MongoDBObject()).map(f => BotFromDatabase(
              f.getAs[String]("botName").getOrElse(""),
              f.getAs[Int]("avatarId").getOrElse(-1),
              f.getAs[Int]("challengeId").getOrElse(-1),
              ""
            )).toList

            val bot = bots(Random.nextInt(bots.size))

            implicit val timeout = Timeout(15 seconds)

            val future = databaseController ? ProcessTaskOnCollectionAndReplyToSender((c: MongoCollection, s: ActorRef) => {
              s ! c.find(MongoDBObject("user" -> bot.botName)).map(f =>  f.getAs[ObjectId]("_id").map(o => o.toString).getOrElse("")).toList
            }, databaseName, decksCollection)

            val possibleDecks = Await.result(future, timeout.duration).asInstanceOf[List[String]]
            replyTo ! DatabaseResult(bot.copy(deckId =  possibleDecks(Random.nextInt(possibleDecks.size))))

          }, databaseName, botDetailsCollection)

        case IncreaseStatistic(rating, win, lost, numberOfGames) =>
          databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
            c.update(
              MongoDBObject("rating" -> rating),
              MongoDBObject(
                "$inc" -> MongoDBObject("win" -> win),
                "$inc" -> MongoDBObject("lost" -> lost),
                "$inc" -> MongoDBObject("numberOfGames" -> lost)
              ),
              upsert = true
            )
          }, databaseName, botDetailsCollection)
        case t => throw new UnsupportedOperationException(t.toString)
      }
    case t => throw new UnsupportedOperationException(t.toString)
  }
}
