package cz.noveit.games.cardgame.adapters

import akka.actor.{ActorSelection, ActorLogging, Actor, ActorRef}
import cz.noveit.database._
import cz.noveit.database.DatabaseCommand
import com.mongodb.casbah.Imports._

import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import scala.concurrent.Await
import cz.noveit.games.cardgame.engine.handlers.{AskOwnershipResult, AskOwnershipOfDeckForPlayer}
import java.util
import scala.collection.JavaConverters._

/**
 * Created by Wlsek on 17.2.14.
 */


sealed trait CardDatabaseMessages extends DatabaseAdapterMessage

case class ElementsRating(water: Float, fire: Float, earth: Float, air: Float, neutral: Float) {
  def total = water + fire + earth + air + neutral
}


object Deck {
  val Empty = Deck(
    "",
    "",
    -1,
    ElementsRating(0, 0, 0, 0, 0),
    List(),
    ""
  )
}

case class Deck(id: String, name: String, icon: Int, elementsRatio: ElementsRating, cards: List[CountableCardWithSkin], ownerUserName: String)

case class DeckLight(id: String, name: String, iconId: Int)

case class CountableCardWithSkin(cardId: String, count: Int, skinId: Option[Int])

object CardTargetingType extends Enumeration {
  val None, MyTeam, EnemyTeam, AllPlayers, AllyCards, EnemyCards, AllCards, All, AllEnemy, AllAlly = Value
}

case class Card(
                 id: String,
                 name: String,
                 rarity: Int,
                 attack: Int,
                 defense: Int,
                 version: String,
                 imageId: Int,
                 price: List[CardPrice],
                 tags: List[String],
                 description: List[Description],
                 cardTargetingType: CardTargetingType.Value = CardTargetingType.None
                 ) extends DatabaseCommandResultMessage with CardDatabaseMessages

case class CardPrice(element: Int, price: Int)

case class Description(locale: String, description: String)

case class GetCard(cardId: String, user: String) extends DatabaseCommandMessage with CardDatabaseMessages

case object GetStarterDecksList extends DatabaseCommandMessage with CardDatabaseMessages

case class StarterDecks(decks: List[DeckLight]) extends DatabaseCommandResultMessage with CardDatabaseMessages

case class GetDeckLightList(userName: String) extends DatabaseCommandMessage with CardDatabaseMessages

case class DeckLightList(decks: List[DeckLight]) extends DatabaseCommandResultMessage with CardDatabaseMessages

case class SaveNewDeck(deck: Deck) extends DatabaseCommandMessage with CardDatabaseMessages

case object SaveNewDeckFailed extends DatabaseCommandResultMessage with CardDatabaseMessages

case object SaveNewDeckSuccessful extends DatabaseCommandResultMessage with CardDatabaseMessages

case class SaveEditedDeck(deck: Deck) extends DatabaseCommandMessage with CardDatabaseMessages

case object SaveEditedDeckFailed extends DatabaseCommandResultMessage with CardDatabaseMessages

case object SaveEditedDeckSuccessful extends DatabaseCommandResultMessage with CardDatabaseMessages

case class DeleteDeck(id: String, user: String) extends DatabaseCommandMessage with CardDatabaseMessages

case object DeleteDeckFailed extends DatabaseCommandResultMessage with CardDatabaseMessages

case object DeleteDeckSuccessful extends DatabaseCommandResultMessage with CardDatabaseMessages

case object GetModuleVersions extends DatabaseCommandMessage with CardDatabaseMessages

case class CardVersionPair(cardId: String, version: String)

case class DeckIconVersionPair(iconId: Int, version: String)

case class DeckIconUnlockablePair(iconId: Int, unlockable: Boolean)

case class CardSkinVersionPair(skinId: Int, version: String)

case class ModuleVersion(cardsVersion: List[CardVersionPair], deckIconsVersion: List[DeckIconVersionPair], cardSkinsVersion: List[CardSkinVersionPair]) extends DatabaseCommandResultMessage with CardDatabaseMessages

case class GetUpdatedCard(id: String) extends DatabaseCommandMessage with CardDatabaseMessages

case class ReturnUpdatedCard(card: Card) extends DatabaseCommandResultMessage with CardDatabaseMessages

case class SetStartingDeck(deckId: String, user: String) extends DatabaseCommandMessage with CardDatabaseMessages

case object SetStartingDeckFailed extends DatabaseCommandResultMessage with CardDatabaseMessages

case object SetStartingDeckSuccessful extends DatabaseCommandResultMessage with CardDatabaseMessages

case class GetDeckAndCardsIdForUserWithGivenDeckId(user: String, deckId: String) extends DatabaseCommandMessage with CardDatabaseMessages

case class DeckAndCardsIdForUserWithGivenDeckId(deck: Option[Deck], user: String, deckId: String) extends DatabaseCommandResultMessage with CardDatabaseMessages

case class GetDeckAndCardsForUserWithGivenDeckId(user: String, deckId: String) extends DatabaseCommandMessage with CardDatabaseMessages

case class DeckAndCardsForUserWithGivenDeckId(deck: Option[Deck], cards: List[Card], user: String, deckId: String) extends DatabaseCommandResultMessage with CardDatabaseMessages

case class GetAvailableCardSkinsFromDatabase(userName: String) extends DatabaseCommandMessage with CardDatabaseMessages

case class AvailableCardSkinsFromDatabase(skinIds: List[Int]) extends DatabaseCommandResultMessage with CardDatabaseMessages

case class GetAvailableDeckIconsFromDatabase(userName: String) extends DatabaseCommandMessage with CardDatabaseMessages

case class AvailableDeckIconsFromDatabase(deckIconIds: List[Int]) extends DatabaseCommandResultMessage with CardDatabaseMessages

/*
*
	WATER = 0;
	FIRE = 1;
	AIR = 2;
	EARTH = 3;
	*/
case class GetAvailableCardsByFiltering(
                                         rarity: List[Int],
                                         tags: List[String],
                                         elements: List[Int],
                                         nameRegexp: String,
                                         tagRegExp: String,
                                         skip: Int,
                                         limit: Int, costLimit: Int, user: String) extends DatabaseCommandMessage with CardDatabaseMessages

case class FilteredAvailableCards(cards: List[String]) extends DatabaseCommandResultMessage with CardDatabaseMessages


class CardDatabaseAdapter(
                           val databaseController: ActorSelection,
                           databaseName: String,
                           cardCollection: String,
                           deckCollection: String,
                           deckIconsVersionCollection: String,
                           cardSkinsVersionCollection: String,
                           userHasCardsCollection: String,
                           userUnlockedCollection: String,
                           userCollection: String) extends Actor with ActorLogging {
  def receive = {
    case dc: DatabaseCommand =>
      dc.command match {
        case GetStarterDecksList =>
          val replyTo = sender
          databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
            replyTo ! DatabaseResult(StarterDecks((c.find(MongoDBObject("startingDeck" -> true)).map(d => DeckLight(
              d.getAs[ObjectId]("_id").map(s => s.toString).getOrElse(""),
              d.getAs[String]("name").getOrElse(""),
              d.getAs[Int]("iconId").getOrElse(-1)
            )).toList)), dc.replyHash)
          }, databaseName, deckCollection)

        case GetModuleVersions => {
          val replyTo = sender

          implicit val timeout = Timeout(15 seconds)

          var future = databaseController ? ProcessTaskOnCollectionAndReplyToSender((c: MongoCollection, s: ActorRef) => {
            s ! c.find(MongoDBObject(), MongoDBObject("_id" -> 1, "version" -> 1)).map(found => {
              CardVersionPair(found.getAs[String]("_id").getOrElse(""), found.getAs[String]("version").getOrElse(""))
            }).toList
          }, databaseName, cardCollection)

          val cards = Await.result(future, timeout.duration).asInstanceOf[List[CardVersionPair]]

          future = databaseController ? ProcessTaskOnCollectionAndReplyToSender((c: MongoCollection, s: ActorRef) => {
            s ! c.find(MongoDBObject()).map(found => {
              DeckIconVersionPair(found.getAs[Int]("id").getOrElse(-1), found.getAs[String]("version").getOrElse(""))
            }).toList
          }, databaseName, deckIconsVersionCollection)

          val decks = Await.result(future, timeout.duration).asInstanceOf[List[DeckIconVersionPair]]

          future = databaseController ? ProcessTaskOnCollectionAndReplyToSender((c: MongoCollection, s: ActorRef) => {
            s ! c.find(MongoDBObject()).map(found => {
              CardSkinVersionPair(found.getAs[Int]("id").getOrElse(-1), found.getAs[String]("version").getOrElse(""))
            }).toList
          }, databaseName, cardSkinsVersionCollection)

          val skins = Await.result(future, timeout.duration).asInstanceOf[List[CardSkinVersionPair]]

          replyTo ! DatabaseResult(ModuleVersion(cards, decks, skins), dc.replyHash)
        }

        case GetUpdatedCard(id) => {
          val replyTo = sender
          databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
            try {
              c.findOne(MongoDBObject("_id" -> id)).map(found => {
                replyTo ! DatabaseResult(ReturnUpdatedCard(Card(
                  id,
                  found.getAs[String]("name").getOrElse(""),
                  found.getAs[Int]("rarity").getOrElse(-1),
                  found.getAs[Int]("attack").getOrElse(-1),
                  found.getAs[Int]("defense").getOrElse(-1),
                  found.getAs[String]("version").getOrElse(""),
                  found.getAs[Int]("imageId").getOrElse(-1),
                  found.getAs[MongoDBList]("price").getOrElse(MongoDBList()).toList.collect {
                    case s: BasicDBObject => s
                  }.map(p => CardPrice(p.getAs[Int]("element").get, p.getAs[Int]("price").get)).toList,
                  found.getAs[MongoDBList]("tags").getOrElse(MongoDBList()).toList.collect {
                    case s: String => s.toLowerCase
                  },
                  found.getAs[MongoDBList]("description").getOrElse(MongoDBList()).toList.collect {
                    case s: BasicDBObject => s
                  }
                    .map(p => Description(p.getAs[String]("locale").get, p.getAs[String]("description").get)).toList,
                  CardTargetingType.withName(found.getAs[String]("targetingType").getOrElse("None"))
                )), dc.replyHash)
              })
            } catch {
              case t: Throwable =>
                log.error("Error while getting updated card: " + t.toString)
            }
          }, databaseName, cardCollection)
        }

        case GetDeckLightList(userName) =>
          val replyTo = sender
          databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
            replyTo ! DatabaseResult(
              DeckLightList(
                c.find(MongoDBObject("user" -> userName)).map(found => {
                  DeckLight(
                    found.getAs[ObjectId]("_id").map(id => id.toString).getOrElse(""),
                    found.getAs[String]("name").getOrElse(""),
                    found.getAs[Int]("iconId").getOrElse(-1)
                  )
                }).toList
              )
              , dc.replyHash)
          }, databaseName, deckCollection)

        case SaveNewDeck(deck) => {
          val replyTo = sender
          databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
            try {

              implicit val timeout = Timeout(15 seconds)

              var future = databaseController ? ProcessTaskOnCollectionAndReplyToSender((c: MongoCollection, s: ActorRef) => {
                c.findOne(MongoDBObject("userName" -> deck.ownerUserName)).map(found => {
                  s ! found.getAs[MongoDBList]("cards").getOrElse(MongoDBList()).toList.collect {
                    case s: String => s
                  }
                })
              }, databaseName, userHasCardsCollection)

              val cards = Await.result(future, timeout.duration).asInstanceOf[List[String]]

              future = databaseController ? ProcessTaskOnCollectionAndReplyToSender((c: MongoCollection, s: ActorRef) => {
                s ! c.find(MongoDBObject()).map(found => {
                  DeckIconUnlockablePair(found.getAs[Int]("id").getOrElse(-1), found.getAs[Boolean]("unlockable").getOrElse(true))
                }).toList
              }, databaseName, deckIconsVersionCollection)

              val decks = Await.result(future, timeout.duration).asInstanceOf[List[DeckIconUnlockablePair]]

              future = databaseController ? ProcessTaskOnCollectionAndReplyToSender((c: MongoCollection, s: ActorRef) => {
                c.findOne(MongoDBObject("userName" -> deck.ownerUserName), MongoDBObject("unlockedSkins" -> 1)).map(f => {
                  s ! f.getAs[MongoDBList]("unlockedSkins").getOrElse(MongoDBList()).toList.collect {
                    case s: Int => s
                  }
                })
              }, databaseName, userUnlockedCollection)
              //todo: tady se to hnusne sekne!!!!
              val skins = Await.result(future, timeout.duration).asInstanceOf[List[Int]]

              val testIcon: Boolean = decks.find(d => d.iconId == deck.id && d.unlockable == true).isEmpty

              val iconId = if (!testIcon) {
                try {
                  implicit val timeout = Timeout(15 seconds)

                  future = databaseController ? ProcessTaskOnCollectionAndReplyToSender((c: MongoCollection, s: ActorRef) => {
                    s ! c.findOne(MongoDBObject("userName" -> deck.ownerUserName, "unlockedDeckIcons" -> deck.icon)).isEmpty
                  }, databaseName, userUnlockedCollection)

                  if (Await.result(future, timeout.duration).asInstanceOf[Boolean]) {
                    0
                  } else {
                    deck.icon
                  }
                } catch {
                  case t: Throwable => 0
                }
              } else {
                deck.icon
              }

              c.save(
                MongoDBObject(
                  "user" -> deck.ownerUserName,
                  "name" -> deck.name,
                  "cards" -> deck.cards.filter(card => cards.contains(card.cardId)).map(card => {
                    val document = MongoDBObject.newBuilder
                    document += "cardId" -> card.cardId
                    document += "count" -> card.count
                    card.skinId.map(skin => "skinId" -> skin)
                    document.result()
                  }).toArray,
                  "elementsRating" -> MongoDBObject(
                    "water" -> deck.elementsRatio.water.toDouble,
                    "air" -> deck.elementsRatio.air.toDouble,
                    "earth" -> deck.elementsRatio.earth.toDouble,
                    "fire" -> deck.elementsRatio.fire.toDouble
                  ),
                  "iconId" -> iconId
                )
              )

              replyTo ! DatabaseResult(SaveNewDeckSuccessful, dc.replyHash)
            } catch {
              case t: Throwable =>
                error(t.toString)
                replyTo ! DatabaseResult(SaveNewDeckFailed, dc.replyHash)
            }
          }, databaseName, deckCollection)
        }

        case SaveEditedDeck(deck) => {
          val replyTo = sender
          databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
            try {

              implicit val timeout = Timeout(15 seconds)

              var future = databaseController ? ProcessTaskOnCollectionAndReplyToSender((c: MongoCollection, s: ActorRef) => {
                c.findOne(MongoDBObject("userName" -> deck.ownerUserName)).map(found => {
                  s ! found.getAs[MongoDBList]("cards").getOrElse(MongoDBList()).toList.collect {
                    case s: String => s
                  }
                })
              }, databaseName, userHasCardsCollection)

              val cards = Await.result(future, timeout.duration).asInstanceOf[List[String]]

              future = databaseController ? ProcessTaskOnCollectionAndReplyToSender((c: MongoCollection, s: ActorRef) => {
                s ! c.find(MongoDBObject()).map(found => {
                  DeckIconUnlockablePair(found.getAs[Int]("id").getOrElse(-1), found.getAs[Boolean]("unlockable").getOrElse(true))
                }).toList
              }, databaseName, deckIconsVersionCollection)

              val decks = Await.result(future, timeout.duration).asInstanceOf[List[DeckIconUnlockablePair]]

              future = databaseController ? ProcessTaskOnCollectionAndReplyToSender((c: MongoCollection, s: ActorRef) => {
                c.findOne(MongoDBObject("userName" -> deck.ownerUserName), MongoDBObject("unlockedSkins" -> 1)).map(f => {
                  s ! f.getAs[MongoDBList]("unlockedSkins").getOrElse(MongoDBList(0)).toList.collect {
                    case s: Int => s
                  }
                })
              }, databaseName, userUnlockedCollection)


              val skins = Await.result(future, timeout.duration).asInstanceOf[List[Int]]

              val testIcon: Boolean = decks.find(d => d.iconId == deck.id && d.unlockable == true).isEmpty

              val iconId = if (!testIcon) {
                try {
                  implicit val timeout = Timeout(15 seconds)

                  future = databaseController ? ProcessTaskOnCollectionAndReplyToSender((c: MongoCollection, s: ActorRef) => {
                    s ! c.find(MongoDBObject("user" -> deck.ownerUserName, "unlockedDeckIcons" -> deck.icon)).isEmpty
                  }, databaseName, userUnlockedCollection)

                  if (Await.result(future, timeout.duration).asInstanceOf[Boolean]) {
                    0
                  } else {
                    deck.icon
                  }
                } catch {
                  case t: Throwable => 0
                }
              } else {
                deck.icon
              }

              c.update(MongoDBObject("_id" -> new ObjectId(deck.id)), MongoDBObject("$set" ->
                MongoDBObject(
                  "user" -> deck.ownerUserName,
                  "name" -> deck.name,
                  "cards" -> deck.cards.filter(card => cards.contains(card.cardId)).map(card => {
                    val document = MongoDBObject.newBuilder
                    document += "cardId" -> card.cardId
                    document += "count" -> card.count
                    card.skinId.map(skin => "skinId" -> skin)
                    document.result()
                  }).toArray,
                  "elementsRating" -> MongoDBObject(
                    "water" -> deck.elementsRatio.water.toDouble,
                    "air" -> deck.elementsRatio.air.toDouble,
                    "earth" -> deck.elementsRatio.earth.toDouble,
                    "fire" -> deck.elementsRatio.fire.toDouble
                  ),
                  "iconId" -> iconId
                ))
              )

              replyTo ! DatabaseResult(SaveEditedDeckSuccessful, dc.replyHash)
            } catch {
              case t: Throwable =>
                error(t.toString)
                replyTo ! DatabaseResult(SaveEditedDeckFailed, dc.replyHash)
            }
          }, databaseName, deckCollection)
        }


        case DeleteDeck(deckId, username) => {
          val replyTo = sender
          databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
            try {
              c.remove(MongoDBObject("_id" -> new ObjectId(deckId), "user" -> username))
              replyTo ! DatabaseResult(DeleteDeckSuccessful, dc.replyHash)
            } catch {
              case t: Throwable =>
                replyTo ! DatabaseResult(DeleteDeckFailed, dc.replyHash)
            }
          }, databaseName, deckCollection)
        }

        case msg: GetAvailableCardsByFiltering => {
          val query = MongoDBObject.newBuilder

          if (!msg.elements.isEmpty) {
            query += "price.element" -> MongoDBObject("$in" -> msg.elements.toArray)
          }

          if (!msg.tags.isEmpty) {
            query += "tags" -> MongoDBObject("$in" -> msg.tags.toArray)
          }

          if (!msg.rarity.isEmpty) {
            query += "rarity" -> MongoDBObject("$in" -> msg.rarity.toArray)
          }

          query += "name" -> MongoDBObject("$regex" -> msg.nameRegexp, "$options" -> "i")
          query += "tags" -> MongoDBObject("$regex" -> msg.tagRegExp, "$options" -> "i")

          query += "price.price" -> MongoDBObject("$lte" -> msg.costLimit)
          val replyTo = sender

          implicit val timeout = Timeout(15 seconds)

          println(query.result())

          val future = databaseController ? ProcessTaskOnCollectionAndReplyToSender((c: MongoCollection, s: ActorRef) => {
            c.findOne(MongoDBObject("userName" -> msg.user)).map(found => {
              s ! found.getAs[MongoDBList]("cards").getOrElse(MongoDBList(0)).toList.collect {
                case l: String => l
              }
            })
          }, databaseName, userHasCardsCollection)

          val cards = Await.result(future, timeout.duration).asInstanceOf[List[String]]

          query += "_id" -> MongoDBObject("$in" -> cards.map(c => c).toArray)

          databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
            try {
              replyTo ! DatabaseResult(FilteredAvailableCards(
                c.find(query.result(), MongoDBObject("_id" -> 1)).limit(msg.limit).skip(msg.skip).map(found => {
                  found.getAs[String]("_id").getOrElse("")
                }).toList
              ), dc.replyHash)

            } catch {
              case t: Throwable =>
            }
          }, databaseName, cardCollection)
        }

        case SetStartingDeck(deckId, user) => {
          val sendTo = sender
          try {
            implicit val timeout = Timeout(15 seconds)

            var future = databaseController ? ProcessTaskOnCollectionAndReplyToSender((c: MongoCollection, s: ActorRef) => {
              c.findOne(MongoDBObject("userName" -> user)).map(found => {
                s ! found.getAs[Boolean]("startingDeckSelected").getOrElse(true)
              })
            }, databaseName, userCollection)

            val startingDeckSelected = Await.result(future, timeout.duration).asInstanceOf[Boolean]

            if (!startingDeckSelected) {
              future = databaseController ? ProcessTaskOnCollectionAndReplyToSender((c: MongoCollection, s: ActorRef) => {
                c.findOne(MongoDBObject("_id" -> new ObjectId(deckId))).map(d => {
                  s ! Deck(
                    deckId,
                    d.getAs[String]("name").getOrElse(""),
                    d.getAs[Int]("iconId").getOrElse(-1),
                    d.getAs[BasicDBObject]("elementsRating").map(obj => ElementsRating(
                      obj.getAs[Double]("water").getOrElse(-1.0).toFloat,
                      obj.getAs[Double]("fire").getOrElse(-1.0).toFloat,
                      obj.getAs[Double]("earth").getOrElse(-1.0).toFloat,
                      obj.getAs[Double]("air").getOrElse(-1.0).toFloat,
                      obj.getAs[Double]("neutral").getOrElse(-1.0).toFloat
                    )).getOrElse(ElementsRating(-1, -1, -1, -1, -1)),
                    d.getAs[MongoDBList]("cards").getOrElse(MongoDBList(0)).toList.collect {
                      case s: BasicDBObject => s
                    }.map(o => CountableCardWithSkin(o.getAs[String]("cardId").getOrElse(""), o.getAs[Int]("count").getOrElse(-1), None)).toList,
                    ""
                  )
                })
              }, databaseName, deckCollection)

              val deck = Await.result(future, timeout.duration).asInstanceOf[Deck]

              databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
                val saved = c.save(MongoDBObject(
                  "user" -> user,
                  "name" -> deck.name,
                  "cards" -> deck.cards.map(card => MongoDBObject("cardId" -> card.cardId, "count" -> card.count)).toArray,
                  "elementsRating" -> MongoDBObject(
                    "fire" -> deck.elementsRatio.fire.toDouble,
                    "earth" -> deck.elementsRatio.earth.toDouble,
                    "water" -> deck.elementsRatio.water.toDouble,
                    "air" -> deck.elementsRatio.air.toDouble
                  ),
                  "iconId" -> deck.icon
                ))

                databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
                  val updated = c.update(MongoDBObject("userName" -> user), MongoDBObject("$push" -> MongoDBObject("cards" -> MongoDBObject("$each" -> deck.cards.map(card => card.cardId).toArray))), concern = WriteConcern.Safe, upsert = true)
                  if (updated.getN == 1) {
                    databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
                      c.update(MongoDBObject("userName" -> user), MongoDBObject("$set" -> MongoDBObject("startingDeckSelected" -> true)))
                      sendTo ! DatabaseResult(SetStartingDeckSuccessful, dc.replyHash)
                    }, databaseName, userCollection)
                  } else {
                    sendTo ! DatabaseResult(SetStartingDeckFailed, dc.replyHash)
                  }
                }, databaseName, userHasCardsCollection)
              }, databaseName, deckCollection)
            }
          } catch {
            case t: Throwable => sendTo ! DatabaseResult(SetStartingDeckFailed, dc.replyHash)
          }
        }

        case GetDeckAndCardsIdForUserWithGivenDeckId(user, deckId) =>
          val replyTo = sender
          try {
            databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
              var result: Option[Deck] = None
              c.findOne(MongoDBObject("_id" -> new ObjectId(deckId), "user" -> user)).map(d => {
                result = Some(Deck(
                  deckId,
                  d.getAs[String]("name").getOrElse(""),
                  d.getAs[Int]("iconId").getOrElse(-1),
                  d.getAs[BasicDBObject]("elementsRating").map(obj => ElementsRating(
                    obj.getAs[Double]("water").getOrElse(-1.0).toFloat,
                    obj.getAs[Double]("fire").getOrElse(-1.0).toFloat,
                    obj.getAs[Double]("earth").getOrElse(-1.0).toFloat,
                    obj.getAs[Double]("air").getOrElse(-1.0).toFloat,
                    obj.getAs[Double]("neutral").getOrElse(-1.0).toFloat
                  )).getOrElse(ElementsRating(-1, -1, -1, -1, -1)),
                  d.getAs[MongoDBList]("cards").getOrElse(MongoDBList(0)).toList.collect {
                    case s: BasicDBObject => s
                  }.map(o => CountableCardWithSkin(o.getAs[String]("cardId").getOrElse(""), o.getAs[Int]("count").getOrElse(-1), o.getAs[Int]("skinId"))).toList,
                  user
                ))
              })
              replyTo ! DatabaseResult(DeckAndCardsIdForUserWithGivenDeckId(result, user, deckId), dc.replyHash)
            }, databaseName, deckCollection)
          } catch {
            case t: Throwable => replyTo ! DatabaseResult(DeckAndCardsIdForUserWithGivenDeckId(None, user, deckId), dc.replyHash)
          }

        case GetDeckAndCardsForUserWithGivenDeckId(user, deckId) =>
          val replyTo = sender
          try {
            implicit val timeout = Timeout(15 seconds)
            val future = databaseController ? ProcessTaskOnCollectionAndReplyToSender((c: MongoCollection, s: ActorRef) => {
              s ! c.findOne(MongoDBObject("_id" -> new ObjectId(deckId), "user" -> user)).map(d => Some(Deck(
                deckId,
                d.getAs[String]("name").getOrElse(""),
                d.getAs[Int]("iconId").getOrElse(-1),
                d.getAs[BasicDBObject]("elementsRating").map(obj => ElementsRating(
                  obj.getAs[Double]("water").getOrElse(-1.0).toFloat,
                  obj.getAs[Double]("fire").getOrElse(-1.0).toFloat,
                  obj.getAs[Double]("earth").getOrElse(-1.0).toFloat,
                  obj.getAs[Double]("air").getOrElse(-1.0).toFloat,
                  obj.getAs[Double]("neutral").getOrElse(-1.0).toFloat
                )).getOrElse(ElementsRating(-1, -1, -1, -1, -1)),
                d.getAs[MongoDBList]("cards").getOrElse(MongoDBList(0)).toList.collect {
                  case s: BasicDBObject => s
                }.map(o => CountableCardWithSkin(o.getAs[String]("cardId").getOrElse(""), o.getAs[Int]("count").getOrElse(-1), None)).toList,
                user
              ))
              ).getOrElse(None)

            }, databaseName, deckCollection)

            val deck = Await.result(future, timeout.duration).asInstanceOf[Option[Deck]]
            if (deck.isEmpty) {
              replyTo ! DatabaseResult(DeckAndCardsForUserWithGivenDeckId(None, List(), user, deckId), dc.replyHash)
            } else {
              deck.map(deck => {
                databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
                  replyTo ! DatabaseResult(
                    DeckAndCardsForUserWithGivenDeckId(
                      Some(deck),
                      c.find(MongoDBObject("_id" -> MongoDBObject("$in" -> deck.cards.map(cws => cws.cardId).toArray))).map(found =>
                        Card(
                          found.getAs[String]("_id").getOrElse(""),
                          found.getAs[String]("name").getOrElse(""),
                          found.getAs[Int]("rarity").getOrElse(-1),
                          found.getAs[Int]("attack").getOrElse(-1),
                          found.getAs[Int]("defense").getOrElse(-1),
                          found.getAs[String]("version").getOrElse(""),
                          found.getAs[Int]("imageId").getOrElse(-1),
                          found.getAs[MongoDBList]("price").getOrElse(MongoDBList()).toList.collect {
                            case o: BasicDBObject => o
                          }
                            .map(p => CardPrice(p.getAs[Int]("element").get, p.getAs[Int]("price").get)).toList,
                          found.getAs[MongoDBList]("tags").getOrElse(MongoDBList()).toList.collect {
                            case s: String => s.toLowerCase
                          },
                          found.getAs[MongoDBList]("description").getOrElse(MongoDBList()).toList.collect {
                            case o: BasicDBObject => o
                          }
                            .map(p => Description(p.getAs[String]("locale").get, p.getAs[String]("description").get)).toList,
                          CardTargetingType.withName(found.getAs[String]("targetingType").getOrElse("None"))
                        )
                      ).toList,
                      user,
                      deckId
                    ), dc.replyHash)

                }, databaseName, cardCollection)

              })
            }


          } catch {
            case t: Throwable => replyTo ! DatabaseResult(DeckAndCardsForUserWithGivenDeckId(None, List(), user, deckId), dc.replyHash)
          }

        case AskOwnershipOfDeckForPlayer(player, deckID) => {
          try {
            val replyTo = sender
            databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
              replyTo ! DatabaseResult(AskOwnershipResult(player, deckID, !c.find(MongoDBObject("user" -> player, "_id" -> new ObjectId(deckID))).isEmpty), dc.replyHash)
            }, databaseName, deckCollection)
          } catch {
            case t: Throwable =>
              log.error("Database error {}", t.toString)
          }
        }


        case GetCard(id, user) =>
          val replyTo = sender
          try {
            implicit val timeout = Timeout(15 seconds)
            val future = databaseController ? ProcessTaskOnCollectionAndReplyToSender((c: MongoCollection, s: ActorRef) => {
              c.find(MongoDBObject("user" -> user)).map(found => {
                s ! found.getAs[MongoDBList]("cards").getOrElse(MongoDBList()).toList.collect {
                  case s: String => s
                }
              })
            }, databaseName, userHasCardsCollection)
            val cards = Await.result(future, timeout.duration).asInstanceOf[List[String]]
            cards.find(card => card == id).map(ok => {
              databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
                c.find(MongoDBObject("_id" -> id)).map(found => {
                  replyTo ! DatabaseResult(Card(
                    id,
                    found.getAs[String]("name").getOrElse(""),
                    found.getAs[Int]("rarity").getOrElse(-1),
                    found.getAs[Int]("attack").getOrElse(-1),
                    found.getAs[Int]("defense").getOrElse(-1),
                    found.getAs[String]("version").getOrElse(""),
                    found.getAs[Int]("imageId").getOrElse(-1),
                    found.getAs[MongoDBList]("price").getOrElse(MongoDBList()).collect {
                      case o: BasicDBObject => o
                    }
                      .map(p => CardPrice(p.getAs[Int]("element").get, p.getAs[Int]("price").get)).toList,
                    found.getAs[MongoDBList]("tags").getOrElse(MongoDBList()).toList.collect {
                      case s: String => s
                    },
                    found.getAs[MongoDBList]("description").getOrElse(MongoDBList()).collect {
                      case o: BasicDBObject => o
                    }
                      .map(p => Description(p.getAs[String]("locale").get, p.getAs[String]("description").get)).toList,
                    CardTargetingType.withName(found.getAs[String]("targetingType").getOrElse("None"))
                  ), dc.replyHash)
                })
              }, databaseName, deckCollection)
            })
          } catch {
            case t: Throwable =>
              log.error("Error while getting card details.")
          }

        case GetAvailableCardSkinsFromDatabase(userName) =>
          try {

            implicit val timeout = Timeout(5 seconds)
            var future = databaseController ? ProcessTaskOnCollectionAndReplyToSender((c: MongoCollection, s: ActorRef) => {
              s ! c.find(MongoDBObject("unlockable" -> false), MongoDBObject("id" -> 1)).map(found => found.getAs[Int]("id").getOrElse(-1)).toList
            }, databaseName, cardSkinsVersionCollection)

            val defaultUnlocked = Await.result(future, timeout.duration).asInstanceOf[List[Int]]

            future = databaseController ? ProcessTaskOnCollectionAndReplyToSender((c: MongoCollection, s: ActorRef) => {
              c.findOne(MongoDBObject("userName" -> userName)).map(found =>
                s ! found.getAs[MongoDBList]("unlockedSkins").getOrElse(MongoDBList()).toList.collect {
                  case i: Int => i
                }
              )
            }, databaseName, userUnlockedCollection)

            val alreadyUnlocked = Await.result(future, timeout.duration).asInstanceOf[List[Int]]

            sender ! DatabaseResult(AvailableCardSkinsFromDatabase(defaultUnlocked ::: alreadyUnlocked), dc.replyHash)

          } catch {
            case t: Throwable =>
              log.error(t.toString)
              sender ! DatabaseResult(AvailableCardSkinsFromDatabase(List()), dc.replyHash)
          }

        case GetAvailableDeckIconsFromDatabase(userName) =>
          try {

            implicit val timeout = Timeout(5 seconds)
            var future = databaseController ? ProcessTaskOnCollectionAndReplyToSender((c: MongoCollection, s: ActorRef) => {
              s ! c.find(MongoDBObject("unlockable" -> false), MongoDBObject("id" -> 1)).map(found => found.getAs[Int]("id").getOrElse(-1)).toList
            }, databaseName, deckIconsVersionCollection)

            val defaultUnlocked = Await.result(future, timeout.duration).asInstanceOf[List[Int]]

            future = databaseController ? ProcessTaskOnCollectionAndReplyToSender((c: MongoCollection, s: ActorRef) => {
              c.findOne(MongoDBObject("userName" -> userName)).map(found =>
                s ! found.getAs[MongoDBList]("unlockedDeckIcons").getOrElse(MongoDBList()).toList.collect {
                  case i: Int => i
                }
              )
            }, databaseName, userUnlockedCollection)

            val alreadyUnlocked = Await.result(future, timeout.duration).asInstanceOf[List[Int]]

            sender ! DatabaseResult(AvailableDeckIconsFromDatabase(defaultUnlocked ::: alreadyUnlocked), dc.replyHash)

          } catch {
            case t: Throwable =>
              log.error(t.toString)
              sender ! DatabaseResult(AvailableDeckIconsFromDatabase(List()), dc.replyHash)
          }

      }


    case t => throw new UnsupportedOperationException(t.toString)
  }

}
