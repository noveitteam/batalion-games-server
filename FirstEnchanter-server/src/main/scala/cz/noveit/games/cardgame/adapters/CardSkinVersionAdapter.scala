package cz.noveit.games.cardgame.adapters

import akka.actor.{ActorLogging, Actor, ActorSelection}
import cz.noveit.database._
import com.mongodb.casbah.Imports._
import cz.noveit.database.DatabaseCommand

/**
 * Created by Wlsek on 4.4.14.
 */

trait CardSkinVersionMessages extends DatabaseAdapterMessage

case class GetCardSkinVersions(deviceType: Int) extends DatabaseCommandMessage with CardSkinVersionMessages

case class CardSkinVersion(id: Int, version: String, device: Int, unlockAble: Boolean)

case class CardVersionsReturned(versions: List[CardSkinVersion]) extends DatabaseCommandResultMessage with CardSkinVersionMessages

case class UpdateCardSkinVersion(version: CardSkinVersion) extends DatabaseCommandMessage with CardSkinVersionMessages
case class CreateCardSkinVersion(version: CardSkinVersion) extends DatabaseCommandMessage with CardSkinVersionMessages
case class RemoveCardSkinVersion(id: Int, device: Int) extends DatabaseCommandMessage with CardSkinVersionMessages

class CardSkinVersionAdapter  (
                                val databaseController: ActorSelection,
                                databaseName: String,
                                cardSkinVersionCollection: String) extends Actor with ActorLogging {

  def receive = {
    case dc: DatabaseCommand =>
      dc.command match {
        case GetCardSkinVersions(deviceType) =>
          val replyTo = sender
          databaseController ! ProcessTaskOnCollection(
            (c: MongoCollection) => {
              val result = DatabaseResult(CardVersionsReturned(c.find(MongoDBObject("device" -> deviceType)).map(found => CardSkinVersion(
                found.getAs[Int]("id").getOrElse(-1),
                found.getAs[String]("version").getOrElse(""),
                deviceType,
                found.getAs[Boolean]("unlockable").getOrElse(false)
              )).toList), dc.replyHash)

              println(result)
              replyTo ! result
            }, databaseName, cardSkinVersionCollection
          )

        case UpdateCardSkinVersion(cardSkinVersion) =>
          databaseController ! ProcessTaskOnCollection(
            (c: MongoCollection) => {
              c.update(
                MongoDBObject("device" -> cardSkinVersion.device, "id" -> cardSkinVersion.id),
                MongoDBObject("$set" -> MongoDBObject("version" -> cardSkinVersion.version, "unlockable" -> cardSkinVersion.unlockAble))
              )
            }, databaseName, cardSkinVersionCollection
          )

        case CreateCardSkinVersion(cardSkinVersion) =>
          databaseController ! ProcessTaskOnCollection(
            (c: MongoCollection) => {
              c.save(
                MongoDBObject(
                  "device" -> cardSkinVersion.device,
                  "id" -> cardSkinVersion.id,
                  "unlockable" -> cardSkinVersion.unlockAble,
                  "version" -> cardSkinVersion.version
                )
              )
            }, databaseName, cardSkinVersionCollection
          )

        case RemoveCardSkinVersion(id, device) =>
          databaseController ! ProcessTaskOnCollection(
            (c: MongoCollection) => {
              c.remove(
                MongoDBObject("device" -> device, "id" -> id)
              )
            }, databaseName, cardSkinVersionCollection
          )
      }
  }

}