package cz.noveit.games.cardgame.adapters

import akka.actor.{ActorRef, Actor, ActorLogging, ActorSelection}
import cz.noveit.database._
import cz.noveit.database.DatabaseCommand
import com.mongodb.casbah.Imports._

import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import scala.concurrent.Await
import cz.noveit.games.cardgame.engine.unlockable.UnlockAbleType

/**
 * Created by Wlsek on 19.6.14.
 */


sealed trait CodeRedemptionMessages extends DatabaseAdapterMessage

/*
*
* val codes = {
*   {
*     "code" : "xxxxxxxxxx",
*     unlockAbles: ["aa.caa", "bbb.bbb"]
*   }
* }


* val redeemedCodes = {
*  {
*     "code" : "xxxxxxxxxx",
*     unlockAbles: ["aa.caa", "bbb.bbb"]
*   }
* }
* */


case class DatabaseCodeRedemption(code: String, user: String) extends CodeRedemptionMessages with DatabaseCommandMessage

object DatabaseCodeRedemptionFailReason extends Enumeration {
  val DatabaseCodeRedemptionFailReasonAlreadyUnlocked, DatabaseCodeRedemptionFailReasonException = Value
}

case class DatabaseCodeRedemptionNotPossible(code: String, user: String, reason: DatabaseCodeRedemptionFailReason.Value) extends CodeRedemptionMessages with DatabaseCommandResultMessage

case class DatabaseCodeRedemptionPossible(code: String, user: String, unlockAbles: List[String]) extends CodeRedemptionMessages with DatabaseCommandResultMessage

case class DatabaseCodeRedemptionConfirm(code: String, user: String) extends CodeRedemptionMessages with DatabaseCommandMessage

case class DatabaseCodeRedemptionConfirmFail(code: String, user: String) extends CodeRedemptionMessages with DatabaseCommandResultMessage

case class DatabaseCodeRedemptionConfirmSuccess(code: String, user: String) extends CodeRedemptionMessages with DatabaseCommandResultMessage

class CodeRedemptionAdapter(
                             val databaseController: ActorRef,
                             databaseName: String,
                             codesCollection: String,
                             redeemedCodesCollection: String,
                             unlockAbleHistoryCollection: String
                             ) extends Actor with ActorLogging {
  def receive = {
    case dc: DatabaseCommand =>
      dc.command match {
        case DatabaseCodeRedemption(code, user) =>
          val replyTo = sender
          try {
            implicit val timeout = Timeout(15 seconds)
            var future = databaseController ? ProcessTaskOnCollectionAndReplyToSender((c: MongoCollection, s: ActorRef) => {
              val found = c.findOne(MongoDBObject("code" -> code))
              s ! {
                if (found.isEmpty) {
                  List()
                } else {
                  found.get.getAs[MongoDBList]("unlockAbles").getOrElse(MongoDBList()).collect {
                    case s: String => s
                  }.toList
                }
              }
            }, databaseName, codesCollection)

            val unlockAbles = Await.result(future, timeout.duration).asInstanceOf[List[String]]

            future = databaseController ? ProcessTaskOnCollectionAndReplyToSender((c: MongoCollection, s: ActorRef) => {
              s ! c.find(MongoDBObject(
                "unlockAbleId" -> MongoDBObject("$in" -> unlockAbles.toArray),
                "unlockAbleType" -> MongoDBObject("$ne" -> UnlockAbleType.UnlockAbleTypeBooster.toString),
                "userName" -> user
              )).map(f => f.getAs[String]("unlockAbleId").getOrElse("")).toList
            }, databaseName, unlockAbleHistoryCollection)

            val unlockedUnlockAbles = Await.result(future, timeout.duration).asInstanceOf[List[String]]

            val finalized = unlockAbles.filter(f => !unlockedUnlockAbles.contains(f))
            if (finalized.size > 0) {
              replyTo ! DatabaseResult(DatabaseCodeRedemptionPossible(code, user, finalized))
            } else {
              replyTo ! DatabaseResult(DatabaseCodeRedemptionNotPossible(code, user, DatabaseCodeRedemptionFailReason.DatabaseCodeRedemptionFailReasonAlreadyUnlocked))
            }

          } catch {
            case t: Throwable =>
              log.error("Error while testing code in database. " + code + " - " + t.toString)
              replyTo ! DatabaseResult(DatabaseCodeRedemptionNotPossible(code, user, DatabaseCodeRedemptionFailReason.DatabaseCodeRedemptionFailReasonException))
          }

        case DatabaseCodeRedemptionConfirm(code, user) =>
          val replyTo = sender
          try {
            databaseController ! ProcessTaskOnCollection((codes: MongoCollection) => {
              try {
                val codeFound = codes.findOne(MongoDBObject("code" -> code))
                val document: MongoDBObject = codeFound.get ++ ("user" -> user)
                databaseController ! ProcessTaskOnCollection((redeemedCodes: MongoCollection) => {
                  try {
                    redeemedCodes.save(document)
                    codes.remove(MongoDBObject("code" -> code))
                    replyTo ! DatabaseResult(DatabaseCodeRedemptionConfirmSuccess(code, user))
                  } catch {
                    case t: Throwable =>
                      log.error("Error while testing code in database. " + code + " - " + t.toString)
                      replyTo ! DatabaseResult(DatabaseCodeRedemptionConfirmFail(code, user))
                  }
                }, databaseName, redeemedCodesCollection)
              } catch {
                case t: Throwable =>
                  log.error("Error while testing code in database. " + code + " - " + t.toString)
                  replyTo ! DatabaseResult(DatabaseCodeRedemptionConfirmFail(code, user))
              }
            }, databaseName, codesCollection)
          } catch {
            case t: Throwable =>
              log.error("Error while testing code in database. " + code + " - " + t.toString)
              replyTo ! DatabaseResult(DatabaseCodeRedemptionConfirmFail(code, user))
          }
      }
  }
}