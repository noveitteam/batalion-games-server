package cz.noveit.games.cardgame.adapters

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSelection}
import cz.noveit.games.cardgame.engine.player.Player
import cz.noveit.games.cardgame.engine._
import cz.noveit.database._
import com.mongodb.casbah.MongoCollection
import com.mongodb.casbah.commons.Imports._
import cz.noveit.games.cardgame.engine.Conversation
import cz.noveit.database.ProcessTaskOnCollection
import cz.noveit.database.DatabaseCommand

/**
 * Created by Wlsek on 13.5.14.
 */


sealed trait ConversationMessage extends DatabaseAdapterMessage

case class SaveConversation(conversation: Conversation) extends ConversationMessage with DatabaseCommandMessage

case object SaveConversationSuccess extends ConversationMessage with DatabaseCommandResultMessage

case object SaveConversationFailedDuplicate extends ConversationMessage with DatabaseCommandResultMessage

case object SaveConversationFailedException extends ConversationMessage with DatabaseCommandResultMessage

case class DeleteConversation(conversationId: String) extends ConversationMessage with DatabaseCommandMessage

case class GetConversationsOlderThan(time: Long) extends ConversationMessage with DatabaseCommandMessage

case class ConversationsOlderThan(time: Long, conversations: List[Conversation]) extends ConversationMessage with DatabaseCommandResultMessage

case object GetConversationsWithoutMembers extends ConversationMessage with DatabaseCommandMessage

case class ConversationsWithoutMembers(conversations: List[Conversation]) extends ConversationMessage with DatabaseCommandResultMessage

case class AddMemberToConversation(conversationId: String, member: String) extends ConversationMessage with DatabaseCommandMessage

case object AddMemberToConversationSuccess extends ConversationMessage with DatabaseCommandResultMessage

case object AddMemberToConversationFailed extends ConversationMessage with DatabaseCommandResultMessage

case class RemoveMemberFromConversation(conversationId: String, member: String) extends ConversationMessage with DatabaseCommandMessage

case object RemoveMemberFromConversationSuccess extends ConversationMessage with DatabaseCommandResultMessage

case object RemoveMemberFromConversationFailed extends ConversationMessage with DatabaseCommandResultMessage

case class WriteMessageToConversation(message: Message, conversationId: String) extends ConversationMessage with DatabaseCommandMessage

case object WriteMessageToConversationSuccess extends ConversationMessage with DatabaseCommandResultMessage

case object WriteMessageToConversationFailed extends ConversationMessage with DatabaseCommandResultMessage

case class SaveConversationInvite(conversationId: String, byPlayer: String, whichPlayer: String) extends ConversationMessage with DatabaseCommandMessage

case object SaveConversationInviteSuccess extends ConversationMessage with DatabaseCommandResultMessage

case object SaveConversationInviteFailed extends ConversationMessage with DatabaseCommandResultMessage

case class ConversationDetails(conversation: String, queryType: ConversationQueryType) extends ConversationMessage with DatabaseCommandMessage

case class ConversationFound(conversation: Conversation) extends ConversationMessage with DatabaseCommandResultMessage

case object CannotFindConversation extends ConversationMessage with DatabaseCommandResultMessage

case class GetConversationInvitations(conversationId: String) extends ConversationMessage with DatabaseCommandMessage

case class ConversationInvitation(by: String, whom: String, created: Long)

case class ConversationInvitations(invitations: List[ConversationInvitation]) extends ConversationMessage with DatabaseCommandResultMessage

case class GetPlayerInvitations(playerName: String) extends ConversationMessage with DatabaseCommandMessage

case class PlayerInvitation(by: String, whom: String, created: Long, conversationId: String, conversationName: String)

case class PlayerInvitations(invitations: List[PlayerInvitation]) extends ConversationMessage with DatabaseCommandResultMessage

case class RemoveConversationInvitation(conversationId: String, player: String) extends ConversationMessage with DatabaseCommandMessage

case class GetPlayerConversations(playerName: String, conversationType: ConversationType.Value) extends ConversationMessage with DatabaseCommandMessage

case class PlayerConversations(conversations: List[Conversation]) extends ConversationMessage with DatabaseCommandResultMessage

case class GetMessageListForConversation(conversationId: String) extends ConversationMessage with DatabaseCommandMessage

case class MessageList(messages: List[Message]) extends ConversationMessage with DatabaseCommandResultMessage

case class CreateBanForConversation(conversationId: String, player: String) extends ConversationMessage with DatabaseCommandMessage

case object CreateBanForConversationFailed extends ConversationMessage with DatabaseCommandResultMessage

case object CreateBanForConversationSuccessful extends ConversationMessage with DatabaseCommandResultMessage

case class RemoveBanForConversation(conversationId: String, player: String) extends ConversationMessage with DatabaseCommandMessage

case object RemoveBanForConversationFailed extends ConversationMessage with DatabaseCommandResultMessage

case object RemoveBanForConversationSuccessful extends ConversationMessage with DatabaseCommandResultMessage

case class FindBansForConversation(conversationId: String) extends ConversationMessage with DatabaseCommandMessage

case class BansForConversation(bans: List[String]) extends ConversationMessage with DatabaseCommandResultMessage

case class QueryPlayerBanStateForConversation(conversationId: String, player: String) extends ConversationMessage with DatabaseCommandMessage

case object ConversationBanStateBanned extends ConversationMessage with DatabaseCommandResultMessage

case object ConversationBanStateNotBanned extends ConversationMessage with DatabaseCommandResultMessage

case class ConversationMembers(conversationId: String) extends ConversationMessage with DatabaseCommandMessage

case class ConversationWithMembersFound(conversationId: String, members: List[String]) extends ConversationMessage with DatabaseCommandResultMessage

case class ConversationWithMembersCannotFound(conversationId: String) extends ConversationMessage with DatabaseCommandResultMessage

case class ChooseConversationForMaintenance(period: Long) extends ConversationMessage with DatabaseCommandMessage
case class ConversationForMaintenance(conversation: Conversation)  extends ConversationMessage with DatabaseCommandResultMessage
case object NoConversationForMaintenance extends ConversationMessage with DatabaseCommandResultMessage


class ConversationControllerDatabaseAdapter(val databaseController: ActorSelection, databaseName: String, conversationsCollection: String, conversationsMembershipCollection: String, conversationsInvitationsCollection: String, conversationsBanCollection: String) extends Actor with ActorLogging {
  def receive = {
    case DatabaseCommand(command, hash) =>
      command match {
        case SaveConversation(conversation) =>
          val replyTo = sender
          databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
            if (conversation.public) {
              val result = c.findOne(MongoDBObject("name" -> conversation.name, "public" -> true))
              if (result.isEmpty) {
                try {
                  c.save(MongoDBObject(
                    "conversationId" -> conversation.id,
                    "name" -> conversation.name,
                    "createdBy" -> conversation.creator,
                    "creationType" -> conversation.creationType.toString,
                    "conversationType" -> conversation.conversationType.toString,
                    "public" -> conversation.public,
                    "created" -> conversation.creationTime,
                    "messages" -> MongoDBList() ,
                    "maintenance" -> conversation.creationTime,
                    "permanent" -> conversation.permanent
                  ))
                  replyTo ! DatabaseResult(SaveConversationSuccess, hash)
                } catch {
                  case t: Throwable =>
                    replyTo ! DatabaseResult(SaveConversationFailedException, hash)
                    log.error("ConversationController handler cast exception  while handling SaveConversation: " + t.toString)
                }
              } else {
                replyTo ! DatabaseResult(SaveConversationFailedDuplicate, hash)
              }
            } else {
              try {
                val result = c.findOne(MongoDBObject("conversationId" -> conversation.id))
                if (result.isEmpty) {
                c.save(MongoDBObject(
                  "conversationId" -> conversation.id,
                  "name" -> conversation.name,
                  "createdBy" -> conversation.creator,
                  "creationType" -> conversation.creationType.toString,
                  "conversationType" -> conversation.conversationType.toString,
                  "public" -> conversation.public,
                  "created" -> conversation.creationTime,
                  "messages" -> MongoDBList(),
                  "maintenance" -> conversation.creationTime,
                  "permanent" -> conversation.permanent
                ))
                replyTo ! DatabaseResult(SaveConversationSuccess, hash)
                } else {
                  replyTo ! DatabaseResult(SaveConversationFailedDuplicate, hash)
                }
              } catch {
                case t: Throwable =>
                  replyTo ! DatabaseResult(SaveConversationFailedException, hash)
                  log.error("ConversationController handler cast exception while handling SaveConversation: " + t.toString)
              }
            }
          }, databaseName, conversationsCollection)

        case DeleteConversation(conversation) =>
          try {
            databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
              c.update(MongoDBObject(), MongoDBObject("$pull" -> MongoDBObject("conversations" -> conversation)))
              databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
                c.remove(MongoDBObject("conversationId" -> conversation))
                databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
                  c.remove(MongoDBObject("conversationId" -> conversation))
                  databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
                    c.remove(MongoDBObject("conversationId" -> conversation))
                  }, databaseName, conversationsCollection)
                }, databaseName, conversationsInvitationsCollection)
              }, databaseName, conversationsBanCollection)
            }, databaseName, conversationsMembershipCollection)
          } catch {
            case t: Throwable =>
              log.error("ConversationController handler cast exception while handling DeleteConversation: " + t.toString)
          }

        case GetConversationsOlderThan(time) =>
          val replyTo = sender
          try {
            databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
              replyTo ! DatabaseResult(ConversationsOlderThan(time, c.find(MongoDBObject("created" -> MongoDBObject("$lt" -> time))).map(found => {
                Conversation(
                  found.getAs[String]("conversationId").getOrElse(""),
                  found.getAs[String]("name").getOrElse(""),
                  found.getAs[String]("createdBy").getOrElse(""),
                  ConversationAutomaticCreationType.withName(found.getAs[String]("creationType").getOrElse("")),
                  ConversationType.withName(found.getAs[String]("conversationType").getOrElse("")),
                  found.getAs[Boolean]("public").get,
                  true,
                  found.getAs[Long]("created").getOrElse(0L)
                )
              }).toList), hash)
            }, databaseName, conversationsCollection)
          } catch {
            case t: Throwable =>
              replyTo ! DatabaseResult(ConversationsOlderThan(time, List()), hash)
              log.error("ConversationController handler cast exception while handling GetConversationsOlderThan: " + t.toString)
          }

        case GetConversationsWithoutMembers =>
          val replyTo = sender
          try {
            databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
              replyTo ! DatabaseResult(ConversationsWithoutMembers(c.find(MongoDBObject("members" -> MongoDBObject("$size" -> 0))).map(found => {
                Conversation(
                  found.getAs[String]("conversationId").getOrElse(""),
                  found.getAs[String]("name").getOrElse(""),
                  found.getAs[String]("createdBy").getOrElse(""),
                  ConversationAutomaticCreationType.withName(found.getAs[String]("creationType").getOrElse("")),
                  ConversationType.withName(found.getAs[String]("conversationType").getOrElse("")),
                  found.getAs[Boolean]("public").get,
                  true,
                  found.getAs[Long]("created").getOrElse(0L)
                )
              }).toList), hash)
            }, databaseName, conversationsCollection)
          } catch {
            case t: Throwable =>
              replyTo ! DatabaseResult(ConversationsWithoutMembers(List()), hash)
              log.error("ConversationController handler cast exception while handling GetConversationsWithoutMembers: " + t.toString)
          }

        case AddMemberToConversation(conversation, member) =>
          val replyTo = sender
          try {
            databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
              c.update(MongoDBObject("userName" -> member), MongoDBObject("$push" -> MongoDBObject("conversations" -> conversation)), upsert = true)
              replyTo ! DatabaseResult(AddMemberToConversationSuccess, hash)
            }, databaseName, conversationsMembershipCollection)
          } catch {
            case t: Throwable =>
              replyTo ! DatabaseResult(AddMemberToConversationFailed, hash)
              log.error("ConversationController handler cast exception while handling AddMemberToConversationById: " + t.toString)
          }

        case RemoveMemberFromConversation(conversation, member) =>
          val replyTo = sender
          try {
            databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
              c.update(MongoDBObject("userName" -> member), MongoDBObject("$pull" -> MongoDBObject("conversations" -> conversation)))
              replyTo ! DatabaseResult(RemoveMemberFromConversationSuccess, hash)
            }, databaseName, conversationsMembershipCollection)
          } catch {
            case t: Throwable =>
              replyTo ! DatabaseResult(RemoveMemberFromConversationFailed, hash)
              log.error("ConversationController handler cast exception while handling RemoveMemberFromConversation: " + t.toString)
          }

        case WriteMessageToConversation(message, conversation) =>
          val replyTo = sender
          try {
            databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
              c.update(MongoDBObject("conversationId" -> conversation), MongoDBObject(
                "$push" -> MongoDBObject("messages" -> MongoDBObject(
                  "$each" -> MongoDBList(
                    MongoDBObject("message" -> message.content, "created" -> message.time, "userName" -> message.fromPlayer, "type" -> message.messageType.toString)
                  ),
                  "$sort" -> MongoDBObject("created" -> 1),
                  "$slice" -> -20
                ))
              ))
              replyTo ! DatabaseResult(WriteMessageToConversationSuccess, hash)
            }, databaseName, conversationsCollection)
          } catch {
            case t: Throwable =>
              replyTo ! DatabaseResult(WriteMessageToConversationFailed, hash)
              log.error("ConversationController handler cast exception while handling WriteMessageToConversationFailed: " + t.toString)
          }

        case SaveConversationInvite(conversation, byPlayer, whichPlayer) =>
          val replyTo = sender
          try {
            databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
              c.update(
                MongoDBObject("conversationId" -> conversation),
                MongoDBObject("$push" ->
                  MongoDBObject("invitations" -> MongoDBObject("created" -> System.currentTimeMillis(), "by" -> byPlayer, "whom" -> whichPlayer))
                ), upsert = true)
              replyTo ! DatabaseResult(SaveConversationInviteSuccess, hash)
            }, databaseName, conversationsInvitationsCollection)
          } catch {
            case t: Throwable =>
              replyTo ! DatabaseResult(SaveConversationInviteFailed, hash)
              log.error("ConversationController handler cast exception while handling SaveConversationInvite: " + t.toString)
          }

        case ConversationDetails(conversation, queryType) =>
          val replyTo = sender
          try {
            queryType match {
              case ConversationQueryTypeById =>
                databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
                  val cFound = c.findOne(MongoDBObject("conversationId" -> conversation)).map(found => {
                    Conversation(
                      conversation,
                      found.getAs[String]("name").getOrElse(""),
                      found.getAs[String]("createdBy").getOrElse(""),
                      ConversationAutomaticCreationType.withName(found.getAs[String]("creationType").getOrElse("")),
                      ConversationType.withName(found.getAs[String]("conversationType").getOrElse("")),
                      found.getAs[Boolean]("public").get,
                      true,
                      found.getAs[Long]("created").getOrElse(0L)
                    )
                  })
                  if (cFound.isEmpty) {
                    replyTo ! DatabaseResult(CannotFindConversation, hash)
                  } else {
                    replyTo ! DatabaseResult(ConversationFound(cFound.get), hash)
                  }
                }, databaseName, conversationsCollection)

              case ConversationQueryTypeByName =>
                databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
                  val cFound = c.findOne(MongoDBObject("name" -> conversation)).map(found => {
                    Conversation(
                      found.getAs[String]("conversationId").getOrElse(""),
                      conversation,
                      found.getAs[String]("createdBy").getOrElse(""),
                      ConversationAutomaticCreationType.withName(found.getAs[String]("creationType").getOrElse("")),
                      ConversationType.withName(found.getAs[String]("conversationType").getOrElse("")),
                      found.getAs[Boolean]("public").get,
                      true,
                      found.getAs[Long]("created").getOrElse(0L)
                    )
                  })

                  if (cFound.isEmpty) {
                    replyTo ! DatabaseResult(CannotFindConversation, hash)
                  } else {
                    replyTo ! DatabaseResult(ConversationFound(cFound.get), hash)
                  }
                }, databaseName, conversationsCollection)
            }
          } catch {
            case t: Throwable =>
              replyTo ! DatabaseResult(CannotFindConversation, hash)
              log.error("ConversationController handler cast exception while handling ConversationDetails: " + t.toString)
          }

        case ConversationMembers(cId) =>
          val replyTo = sender
          try {
            databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
              replyTo ! DatabaseResult(
                ConversationWithMembersFound(cId, c.find(MongoDBObject("conversations" -> cId)).map(found => {
                  found.getAs[String]("userName").getOrElse("")
                }).toList
                ))
            }, databaseName, conversationsMembershipCollection)
          } catch {
            case t: Throwable =>
              replyTo ! DatabaseResult(ConversationWithMembersFound(cId, List()))
          }

        case GetConversationInvitations(conversation) =>
          val replyTo = sender
          try {
            databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
              val cFound = c.findOne(MongoDBObject("conversationId" -> conversation))
              if (!cFound.isEmpty) {
                cFound.map(found => {
                  val invitations = found.getAs[MongoDBList]("invitations").getOrElse(MongoDBList()).toList collect {
                    case o: BasicDBObject => o
                  }

                  replyTo ! DatabaseResult(ConversationInvitations(
                    invitations.map(o => ConversationInvitation(
                      o.getAs[String]("by").getOrElse(""),
                      o.getAs[String]("whom").getOrElse(""),
                      o.getAs[Long]("created").getOrElse(0L)
                    )).toList
                  ))
                })
              } else {
                replyTo ! DatabaseResult(ConversationInvitations(List()))
              }
            }, databaseName, conversationsInvitationsCollection)
          } catch {
            case t: Throwable =>
              replyTo ! DatabaseResult(ConversationInvitations(List()))
              log.error("ConversationController handler cast exception while handling GetConversationInvitations: " + t.toString)
          }

        /*

        {
"conversationId" : "Actor[akka:\/\/FirstEnchanterCluster\/user\/$e\/$a\/$a\/$k\/$c#5730820]1400683474330",
"invitations" : [
  {"by" : "danecek",
  "whom" : "hawu2",
  "created" : NumberLong(0)}
]
}
         */
        case GetPlayerInvitations(player) =>
          val replyTo = sender
          try {
            databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
              val pInvitationsForPlayer = c.find(MongoDBObject("invitations.whom" -> player), MongoDBObject("invitations.$" -> 1, "conversationId" -> 1)).map(found => {
                val invitations = found.getAs[MongoDBList]("invitations").getOrElse(MongoDBList()).toList collect {
                  case o: BasicDBObject => o
                }

                val fObject = invitations(0)
                PlayerInvitation(
                  fObject.getAs[String]("by").getOrElse(""),
                  fObject.getAs[String]("whom").getOrElse(""),
                  fObject.getAs[Long]("created").getOrElse(0L),
                  found.getAs[String]("conversationId").getOrElse(""),
                  ""
                )
              })

              databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
                replyTo ! DatabaseResult(PlayerInvitations(pInvitationsForPlayer.map(pInv => {
                  pInv.copy(conversationName = c.findOne(MongoDBObject("conversationId" -> pInv.conversationId), MongoDBObject("name" -> 1)).map(found => {
                    found.getAs[String]("name").getOrElse("")
                  }).getOrElse("")
                  )
                }).toList), hash)
              }, databaseName, conversationsCollection)
            }, databaseName, conversationsInvitationsCollection)
          } catch {
            case t: Throwable =>
              replyTo ! DatabaseResult(PlayerInvitations(List()), hash)
              log.error("ConversationController handler cast exception while handling GetPlayerInvitations: " + t.toString)
          }

        case RemoveConversationInvitation(conversationId, player) =>
          val replyTo = sender
          try {
            databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
              c.update(
                MongoDBObject("conversationId" -> conversationId, "invitations.whom" -> player),
                MongoDBObject("$pull" -> MongoDBObject("invitations" -> MongoDBObject("whom" -> player)))
              )
            }, databaseName, conversationsInvitationsCollection)
          } catch {
            case t: Throwable =>
              log.error("ConversationController handler cast exception while handling GetConversationInvitations: " + t.toString)
          }

        case GetPlayerConversations(player, cType) =>
          val replyTo = sender
          try {
            databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
              val mFound = c.findOne(MongoDBObject("userName" -> player))

              if (!mFound.isEmpty) {
                mFound.map(found => {
                  val conversations = found.getAs[MongoDBList]("conversations").getOrElse(MongoDBList()).toList.collect {
                    case s: String => s
                  }

                  databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
                    replyTo ! DatabaseResult(PlayerConversations(conversations.map(conversationId => {
                      c.findOne({
                        if(cType == ConversationType.ConversationAll) {
                          MongoDBObject("conversationId" -> conversationId)
                        } else {
                          MongoDBObject("conversationId" -> conversationId, "conversationType" -> cType.toString)
                        }
                      }).map(found => {
                        Conversation(
                          found.getAs[String]("conversationId").getOrElse(""),
                          found.getAs[String]("name").getOrElse(""),
                          found.getAs[String]("createdBy").getOrElse(""),
                          ConversationAutomaticCreationType.withName(found.getAs[String]("creationType").getOrElse("")),
                          cType,
                          found.getAs[Boolean]("public").get,
                          true,
                          found.getAs[Long]("created").getOrElse(0L)
                        )
                      })
                    }).toList collect {
                      case s: Some[Conversation] => s.get
                    }
                    ), hash)

                  }, databaseName, conversationsCollection)
                })
              } else {
                replyTo ! DatabaseResult(PlayerConversations(List()), hash)
              }
            }, databaseName, conversationsMembershipCollection)
          } catch {
            case t: Throwable =>
              replyTo ! DatabaseResult(PlayerConversations(List()), hash)
              log.error("ConversationController handler cast exception while handling GetPlayerConversations: " + t.toString)
          }

        case GetMessageListForConversation(conversationId) =>
          val replyTo = sender
          try {
            databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
              val cFound = c.findOne(MongoDBObject("conversationId" -> conversationId), MongoDBObject("messages" -> 1))
              if (!cFound.isEmpty) {
                cFound.map(found => {
                  replyTo ! DatabaseResult(MessageList(found.getAs[MongoDBList]("messages").getOrElse(MongoDBList()).collect {
                    case o: BasicDBObject => o
                  }.map(o => {
                    Message(
                      o.getAs[Long]("created").getOrElse(0L),
                      o.getAs[String]("message").getOrElse(""),
                      o.getAs[String]("userName").getOrElse(""),
                      MessageType.withName(o.getAs[String]("type").getOrElse(""))
                    )
                  }).toList.reverse
                  ), hash)
                })
              } else {
                replyTo ! DatabaseResult(MessageList(List()), hash)
              }
            }, databaseName, conversationsCollection)
          } catch {
            case t: Throwable =>
              replyTo ! DatabaseResult(MessageList(List()), hash)
              log.error("ConversationController handler cast exception while handling GetMessageListForConversation: " + t.toString)
          }

        case CreateBanForConversation(conversationId, playerName) =>
          val replyTo = sender
          try {
            databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
              c.update(
                MongoDBObject("conversationId" -> conversationId),
                MongoDBObject("$push" -> MongoDBObject("bans" -> MongoDBObject("created" -> System.currentTimeMillis(), "userName" -> playerName))),
                upsert = true
              )
              replyTo ! DatabaseResult(CreateBanForConversationSuccessful, hash)
            }, databaseName, conversationsBanCollection)
          } catch {
            case t: Throwable =>
              replyTo ! DatabaseResult(CreateBanForConversationFailed, hash)
              log.error("ConversationController handler cast exception while handling CreateBanForConversation: " + t.toString)
          }

        case RemoveBanForConversation(conversationId, playerName) =>
          val replyTo = sender
          try {
            databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
              c.update(
                MongoDBObject("conversationId" -> conversationId),
                MongoDBObject("$pull" -> MongoDBObject("bans" -> MongoDBObject("userName" -> playerName)))
              )
              replyTo ! DatabaseResult(RemoveBanForConversationSuccessful, hash)
            }, databaseName, conversationsBanCollection)
          } catch {
            case t: Throwable =>
              replyTo ! DatabaseResult(RemoveBanForConversationFailed, hash)
              log.error("ConversationController handler cast exception while handling RemoveBanForConversation: " + t.toString)
          }

        case FindBansForConversation(conversationId) =>
          val replyTo = sender
          try {
            databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
              val cFound = c.findOne(MongoDBObject("conversationId" -> conversationId))
              if (!cFound.isEmpty) {
                cFound.map(found => {
                  replyTo ! DatabaseResult(BansForConversation(found.getAs[MongoDBList]("bans").getOrElse(MongoDBList()).collect {
                    case o: BasicDBObject => o
                  }.map(o => {
                    o.getAs[String]("userName")
                  }).collect {
                    case s: Some[String] => s.get
                  }.toList), hash)
                })
              } else {
                replyTo ! DatabaseResult(BansForConversation(List()), hash)
              }
            }, databaseName, conversationsBanCollection)
          } catch {
            case t: Throwable =>
              replyTo ! DatabaseResult(BansForConversation(List()), hash)
              log.error("ConversationController handler cast exception while handling FindBansForConversation: " + t.toString)
          }

        case QueryPlayerBanStateForConversation(conversationId, playerName) =>
          val replyTo = sender
          try {
            databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
              val cFound = c.findOne(MongoDBObject("conversationId" -> conversationId, "bans.userName" -> playerName))
              if (!cFound.isEmpty) {
                replyTo ! DatabaseResult(ConversationBanStateBanned, hash)
              } else {
                replyTo ! DatabaseResult(ConversationBanStateNotBanned, hash)
              }
            }, databaseName, conversationsBanCollection)
          } catch {
            case t: Throwable =>
              replyTo ! DatabaseResult(ConversationBanStateBanned, hash)
              log.error("ConversationController handler cast exception while handling QueryPlayerBanStateForConversation: " + t.toString)
          }

        case ChooseConversationForMaintenance(period) =>
          val replyTo = sender
          try {
            databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
              val lowerThan = System.currentTimeMillis() - period
              val found = c.findAndModify(MongoDBObject("maintenance" -> MongoDBObject("$lt" -> lowerThan)), MongoDBObject("$set" -> MongoDBObject("maintenance" -> lowerThan)))
              if(found.isDefined) {
                replyTo ! DatabaseResult(ConversationForMaintenance(Conversation(
                  found.get.getAs[String]("conversationId").getOrElse(""),
                  found.get.getAs[String]("name").getOrElse(""),
                  found.get.getAs[String]("createdBy").getOrElse(""),
                  ConversationAutomaticCreationType.withName(found.get.getAs[String]("creationType").getOrElse("")),
                  ConversationType.withName(found.get.getAs[String]("conversationType").getOrElse("")),
                  found.get.getAs[Boolean]("public").get,
                  found.get.getAs[Boolean]("permanent").get,
                  found.get.getAs[Long]("created").getOrElse(0L)
                )))
              } else {
                replyTo ! DatabaseResult(NoConversationForMaintenance)
              }
            }, databaseName, conversationsCollection)
          } catch {
            case t: Throwable =>
              replyTo ! DatabaseResult(NoConversationForMaintenance)
              log.error("ConversationController handler cast exception while handling ChooseConversationForMaintenance: " + t.toString)

          }

        case t: Throwable =>
          log.error("Unknown command received: " + t.toString)
      }
    case t: Throwable => log.error("Unknown message received: " + t.toString)
  }
}
