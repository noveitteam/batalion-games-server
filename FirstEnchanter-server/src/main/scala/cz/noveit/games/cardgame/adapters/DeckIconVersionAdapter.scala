package cz.noveit.games.cardgame.adapters

import cz.noveit.database._
import akka.actor.{ActorLogging, Actor, ActorSelection}
import cz.noveit.database.DatabaseCommand
import com.mongodb.casbah.Imports._

/**
 * Created by Wlsek on 4.4.14.
 */

trait DeckIconVersionMessages extends DatabaseAdapterMessage

case class GetDeckIconVersions(deviceType: Int) extends DatabaseCommandMessage with DeckIconVersionMessages

case class DeckIconVersion(id: Int, version: String, device: Int, unlockAble: Boolean)

case class DeckIconsReturned(versions: List[DeckIconVersion]) extends DatabaseCommandResultMessage with DeckIconVersionMessages

case class UpdateDeckIconVersion(version: DeckIconVersion) extends DatabaseCommandMessage with DeckIconVersionMessages
case class CreateDeckIconVersion(version: DeckIconVersion) extends DatabaseCommandMessage with DeckIconVersionMessages
case class RemoveDeckIconVersion(id: Int, device: Int) extends DatabaseCommandMessage with DeckIconVersionMessages

class DeckIconVersionAdapter  (
                                val databaseController: ActorSelection,
                                databaseName: String,
                                deckIconVersionCollection: String) extends Actor with ActorLogging {

  def receive = {
    case dc: DatabaseCommand =>
      dc.command match {
        case GetCardSkinVersions(deviceType) =>
          val replyTo = sender
          databaseController ! ProcessTaskOnCollection(
            (c: MongoCollection) => {
              val result = DatabaseResult(CardVersionsReturned(c.find(MongoDBObject("device" -> deviceType)).map(found => CardSkinVersion(
                found.getAs[Int]("id").getOrElse(-1),
                found.getAs[String]("version").getOrElse(""),
                deviceType,
                found.getAs[Boolean]("unlockable").getOrElse(false)
              )).toList), dc.replyHash)

              println(result)
              replyTo ! result
            }, databaseName, deckIconVersionCollection
          )

        case UpdateDeckIconVersion(deckIconVersion) =>
          databaseController ! ProcessTaskOnCollection(
            (c: MongoCollection) => {
              c.update(
                MongoDBObject("device" -> deckIconVersion.device, "id" -> deckIconVersion.id),
                MongoDBObject("$set" -> MongoDBObject("version" -> deckIconVersion.version, "unlockable" -> deckIconVersion.unlockAble))
              )
            }, databaseName, deckIconVersionCollection
          )

        case CreateDeckIconVersion(deckIconVersion) =>
          databaseController ! ProcessTaskOnCollection(
            (c: MongoCollection) => {
              c.save(
                MongoDBObject(
                  "device" -> deckIconVersion.device,
                  "id" -> deckIconVersion.id,
                  "unlockable" -> deckIconVersion.unlockAble,
                  "version" -> deckIconVersion.version
                )
              )
            }, databaseName, deckIconVersionCollection
          )

        case RemoveDeckIconVersion(id, device) =>
          databaseController ! ProcessTaskOnCollection(
            (c: MongoCollection) => {
              c.remove(
                MongoDBObject("device" -> device, "id" -> id)
              )
            }, databaseName, deckIconVersionCollection
          )
      }
  }

}