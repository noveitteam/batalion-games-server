package cz.noveit.games.cardgame.adapters

import akka.actor.{ActorSelection, Actor, ActorLogging}
import cz.noveit.database._
import com.mongodb.casbah.Imports._
import com.mongodb.casbah.commons.MongoDBObject
import org.bson.types.ObjectId
import cz.noveit.database.ProcessTaskOnCollection
import cz.noveit.database.DatabaseResult
import cz.noveit.database.DatabaseCommand

/**
 * Created by Wlsek on 11.2.14.
 */

case class News(title: String, description: String, dateTime: Long, newsId: String)

sealed trait NewsDatabaseMessages extends DatabaseAdapterMessage

case class GetNews(skip: Int, size: Int, localization: Option[String]) extends DatabaseCommandMessage with NewsDatabaseMessages

case class ListOfNews(news: List[News]) extends DatabaseCommandResultMessage with NewsDatabaseMessages

class NewsDatabaseAdapter(val databaseController: ActorSelection,
                          databaseName: String,
                          newsCollection: String) extends Actor with ActorLogging {

  def receive = {
    case dc: DatabaseCommand =>
      dc.command match {
        case m: GetNews =>
          val replyTo = sender

          databaseController ! ProcessTaskOnCollection(
            (c: MongoCollection) => {
              val result = m.localization.map(
                l => c.find(MongoDBObject("localization" -> l)).skip(m.skip).limit(m.size)
              ).getOrElse(
                  c.find(MongoDBObject("localization" -> "en-US")).skip(m.skip).limit(m.size)
              )

              val news =  result.map(
                row => News(
                  row.getAs[String]("title").get,
                  row.getAs[String]("description").get,
                  row.getAs[Long]("created").get,
                  row.getAs[ObjectId]("_id").get.toString())
              ).toList

              replyTo ! DatabaseResult(
                ListOfNews(
                   news
                ), dc.replyHash
              )

            }, databaseName, newsCollection
          )
      }
    case t => throw new UnsupportedOperationException(t.toString)
  }
}
