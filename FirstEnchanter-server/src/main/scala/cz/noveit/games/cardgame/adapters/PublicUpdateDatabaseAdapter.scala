package cz.noveit.games.cardgame.adapters

import akka.actor.{ActorSelection, Actor, ActorLogging, ActorRef}
import cz.noveit.database._
import cz.noveit.database.DatabaseCommand
import com.mongodb.casbah.Imports._

/**
 * Created by Wlsek on 21.3.14.
 */


trait PublicUpdateDatabaseMessages extends DatabaseAdapterMessage

case class GetAvatarVersions(deviceType: Int) extends DatabaseCommandMessage with PublicUpdateDatabaseMessages

case class AvatarVersion(id: Int, version: String, device: Int, unlockAble: Boolean)

case class AvatarVersionsReturned(versions: List[AvatarVersion]) extends DatabaseCommandResultMessage with PublicUpdateDatabaseMessages

case class UpdateVersionForAvatar(avatarVersion: AvatarVersion) extends DatabaseCommandMessage with PublicUpdateDatabaseMessages
case class CreateVersionForAvatar(avatarVersion: AvatarVersion) extends DatabaseCommandMessage with PublicUpdateDatabaseMessages
case class RemoveVersionForAvatar(id: Int, device: Int) extends DatabaseCommandMessage with PublicUpdateDatabaseMessages

class PublicUpdateDatabaseAdapter(val databaseController: ActorSelection, databaseName: String, avatarVersionCollection: String) extends Actor with ActorLogging {
  def receive = {
    case dc: DatabaseCommand =>
      dc.command match {
        case GetAvatarVersions(deviceType) =>
          val replyTo = sender
          databaseController ! ProcessTaskOnCollection(
            (c: MongoCollection) => {
              val result = DatabaseResult(AvatarVersionsReturned(c.find(MongoDBObject("device" -> deviceType)).map(found => AvatarVersion(
                found.getAs[Int]("id").getOrElse(-1),
                found.getAs[String]("version").getOrElse(""),
                deviceType,
                found.getAs[Boolean]("unlockable").getOrElse(false)
              )).toList), dc.replyHash)

              println(result)
              replyTo ! result
            }, databaseName, avatarVersionCollection
          )

        case UpdateVersionForAvatar(avatarVersion) =>
          databaseController ! ProcessTaskOnCollection(
            (c: MongoCollection) => {
              c.update(
                MongoDBObject("device" -> avatarVersion.device, "id" -> avatarVersion.id),
                MongoDBObject("$set" -> MongoDBObject("version" -> avatarVersion.version, "unlockable" -> avatarVersion.unlockAble))
              )
            }, databaseName, avatarVersionCollection
          )

        case CreateVersionForAvatar(avatarVersion) =>
          databaseController ! ProcessTaskOnCollection(
            (c: MongoCollection) => {
              c.save(
                MongoDBObject(
                  "device" -> avatarVersion.device,
                  "id" -> avatarVersion.id,
                  "unlockable" -> avatarVersion.unlockAble,
                  "version" -> avatarVersion.version
                )
              )
            }, databaseName, avatarVersionCollection
          )

        case RemoveVersionForAvatar(id, device) =>
          databaseController ! ProcessTaskOnCollection(
            (c: MongoCollection) => {
              c.remove(
                MongoDBObject("device" -> device, "id" -> id)
              )
            }, databaseName, avatarVersionCollection
          )
      }
  }
}
