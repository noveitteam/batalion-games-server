package cz.noveit.games.cardgame.adapters

import akka.actor.{ActorSelection, ActorLogging, Actor, ActorRef}
import cz.noveit.database._
import cz.noveit.database.DatabaseCommand
import com.mongodb.casbah.Imports._

/**
 * Created by Wlsek on 27.2.14.
 */


trait ScoreBoardDatabaseMessages extends DatabaseAdapterMessage

case class PlayerScore(user: String, rating: Int, score: Int, divisionName: String, division: Int, divisionId: String)

case class ScoreBoardDivision(name: String, divisionId: String, level: Int, members: List[PlayerScore], created: Long)

case class GetPlayerScore(user: String) extends DatabaseCommandMessage with ScoreBoardDatabaseMessages

case class PlayerScoreReturned(playerScore: Option[PlayerScore]) extends DatabaseCommandResultMessage with ScoreBoardDatabaseMessages

case class UpdatePlayerScore(playerScore: PlayerScore) extends DatabaseCommandMessage with ScoreBoardDatabaseMessages

case class ChangeDivision(player: String, newDivisionId: String) extends DatabaseCommandMessage with ScoreBoardDatabaseMessages

case class GetFreeDivision(maxSize: Int, divisionLevel: Int) extends DatabaseCommandMessage with ScoreBoardDatabaseMessages

case class FreeDivisionReturned(freeDivision: Option[ScoreBoardDivision]) extends DatabaseCommandResultMessage with ScoreBoardDatabaseMessages

case class GetDivisionForPlayer(user: String) extends DatabaseCommandMessage with ScoreBoardDatabaseMessages

case class DivisionForPlayerReturned(division: Option[ScoreBoardDivision]) extends DatabaseCommandResultMessage with ScoreBoardDatabaseMessages

case class CreateDivision(divisionName: String, divisionLevel: Int) extends DatabaseCommandMessage with ScoreBoardDatabaseMessages

case class DivisionCreated(division: Option[ScoreBoardDivision]) extends DatabaseCommandResultMessage with ScoreBoardDatabaseMessages

class ScoreBoardDatabaseAdapter(val databaseController: ActorSelection, databaseName: String, userCollection: String, scoreBoardCollection: String) extends Actor with ActorLogging {
  def receive = {
    case dc: DatabaseCommand =>
      dc.command match {
        case GetPlayerScore(player) =>
          repairInconsistency(player)
          val replyTo = sender
          databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
            try {
              var playerScore: Option[PlayerScore] = None
              c.findOne(MongoDBObject("players.user" -> player, "players.transaction" -> MongoDBObject("$exists" -> false))).foreach(found => {
                found.getAs[MongoDBList]("players").map(players => players.collect {
                  case o: MongoDBObject => o
                }.find(o => {
                  val objectOption = o.getAs[String]("user")
                  objectOption.map(user => user.equals(player)).getOrElse(false)
                }).map(f => playerScore = Some(PlayerScore(
                  f.getAs[String]("user").getOrElse(""),
                  f.getAs[Int]("rating").getOrElse(0),
                  f.getAs[Int]("score").getOrElse(0),
                  found.getAs[String]("name").getOrElse(""),
                  found.getAs[Int]("level").getOrElse(0),
                  found.getAs[ObjectId]("_id").map(id => id.toString).getOrElse("")
                )))
                )
              })

              replyTo ! DatabaseResult(PlayerScoreReturned(playerScore), dc.replyHash)
            } catch {
              case t: Throwable =>
                log.error("Exception thrown {}", t.toString)
                replyTo ! DatabaseResult(PlayerScoreReturned(None), dc.replyHash)
            }
          }, databaseName, scoreBoardCollection)

        case UpdatePlayerScore(playerScore) =>
          repairInconsistency(playerScore.user)
          databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
            try {
              c.update(MongoDBObject("players.user" -> playerScore.user), MongoDBObject("$set" -> MongoDBObject("players.$.rating" -> playerScore.rating, "players.$.score" -> playerScore.score)))
            } catch {
              case t: Throwable =>
                log.error("Exception thrown {}", t.toString)
            }
          }, databaseName, scoreBoardCollection)

        case ChangeDivision(player, divisionId) =>
          repairInconsistency(player)
          databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
            try {
              c.find(MongoDBObject("_id" -> new ObjectId(divisionId))).map(foundNewDivision => {
                c.update(MongoDBObject("players.user" -> player), MongoDBObject("$set" -> MongoDBObject("players.$.transaction" -> 1)))

                c.findOne(MongoDBObject("players.user" -> player)).map(foundPlayerDivision => {
                  foundPlayerDivision.getAs[Array[MongoDBObject]]("players").map(players => players.find(o => {
                    val objectOption = o.getAs[String]("user")
                    objectOption.map(user => user.equals(player)).getOrElse(false)
                  }).map(f => {
                    val playerScore = PlayerScore(
                      f.getAs[String]("user").getOrElse(""),
                      f.getAs[Int]("rating").getOrElse(0),
                      f.getAs[Int]("score").getOrElse(0),
                      foundPlayerDivision.getAs[String]("name").getOrElse(""),
                      foundPlayerDivision.getAs[Int]("level").getOrElse(0),
                      foundPlayerDivision.getAs[ObjectId]("_id").map(id => id.toString).getOrElse("")
                    )

                    foundNewDivision.getAs[ObjectId]("_id").map(id => {
                      c.update(
                        MongoDBObject("_id" -> id),
                        MongoDBObject("$push" -> MongoDBObject("players" -> MongoDBObject(
                          "user" -> playerScore.user,
                          "rating" -> playerScore.rating,
                          "score" -> playerScore.score
                        )), "$inc" -> MongoDBObject("size" -> 1)))
                    })

                    foundPlayerDivision.getAs[ObjectId]("_id").map(id => {
                      c.update(
                        MongoDBObject("_id" -> id, "players.user" -> player),
                        MongoDBObject("$pull" -> MongoDBObject("players" -> MongoDBObject("user" -> player)), "$inc" -> MongoDBObject("size" -> -1)))
                    })

                  })
                  )
                })
              })
            } catch {
              case t: Throwable =>
                log.error("Exception thrown {}", t.toString)
            }
          }, databaseName, scoreBoardCollection)

        case GetFreeDivision(maxSize, divisionLevel) =>
          val replyTo = sender
          databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
            try {
              replyTo ! DatabaseResult(FreeDivisionReturned(c.findOne(MongoDBObject("size" -> MongoDBObject("$lt" -> maxSize), "level" -> divisionLevel)).map(found => {
                ScoreBoardDivision(
                  found.getAs[String]("name").getOrElse(""),
                  found.getAs[ObjectId]("_id").map(id => id.toString).getOrElse(""),
                  found.getAs[Int]("level").getOrElse(0),
                  List(),
                  found.getAs[Long]("created").getOrElse(0)
                )
              })), dc.replyHash)
            } catch {
              case t: Throwable =>
                log.error("Exception thrown {}", t.toString)
                replyTo ! DatabaseResult(FreeDivisionReturned(None), dc.replyHash)
            }
          }, databaseName, scoreBoardCollection)

        case GetDivisionForPlayer(player) =>
          val replyTo = sender
          databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
            try {
              val result = c.findOne(MongoDBObject("players.user" -> player))
              val division = DivisionForPlayerReturned(
                if (!result.isEmpty) {
                  Some(ScoreBoardDivision(
                    result.get.getAs[String]("name").getOrElse(""),
                    result.get.getAs[ObjectId]("_id").map(id => id.toString).getOrElse(""),
                    result.get.getAs[Int]("level").getOrElse(0),
                    result.get.getAs[MongoDBList]("players").getOrElse(MongoDBList()).toList.collect {
                      case o: BasicDBObject => o
                    }.map(player => {
                      PlayerScore(
                        player.getAs[String]("user").getOrElse(""),
                        player.getAs[Int]("rating").getOrElse(0),
                        player.getAs[Int]("score").getOrElse(0),
                        result.get.getAs[String]("name").getOrElse(""),
                        result.get.getAs[Int]("level").getOrElse(0),
                        result.get.getAs[ObjectId]("_id").map(id => id.toString).getOrElse("")
                      )
                    }).toList,
                    result.get.getAs[Long]("created").getOrElse(0)
                  ))
                } else {
                  None
                }
              )

              replyTo ! DatabaseResult(division, dc.replyHash)
            } catch {
              case t: Throwable =>
                log.error("Exception thrown {}", t.toString)
                replyTo ! DatabaseResult(DivisionForPlayerReturned(None), dc.replyHash)
            }
          }, databaseName, scoreBoardCollection)

        case CreateDivision(divisionName, divisionLevel) =>
          databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
            try {
              c.save(MongoDBObject(
                "name" -> divisionName,
                "level" -> divisionLevel,
                "players" -> MongoDBList(),
                "size" -> 0
              ))
            } catch {
              case t: Throwable => log.error("Exception thrown {}", t.toString)
            }
          }, databaseName, scoreBoardCollection)

        case t => throw new UnsupportedOperationException(t.toString)
      }
  }


  def repairInconsistency(player: String) = {
    databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
      try {
        c.findOne(MongoDBObject("players.user" -> player, "players.transaction" -> MongoDBObject("$exists" -> true))).map(found => {
          val inconsistency = c.findOne(MongoDBObject("players.user" -> player, "players.transaction" -> MongoDBObject("$exists" -> false)))
          if (!inconsistency.isEmpty) {
            inconsistency.get.getAs[ObjectId]("_id").map(id => {
              c.update(MongoDBObject("_id" -> id), MongoDBObject("$pull" -> MongoDBObject("players" -> MongoDBObject("user" -> player))))
            })
          } else {
            found.getAs[ObjectId]("_id").map(id => {
              c.update(MongoDBObject("_id" -> id, "players.user" -> player), MongoDBObject("$unset" -> MongoDBObject("players.$.transaction" -> "")))
            })
          }
        })
      } catch {
        case t: Throwable => log.error("Error while repairing inconsistency: {}", t.toString)
      }
    }, databaseName, scoreBoardCollection)
  }
}
