package cz.noveit.games.cardgame.adapters

import akka.actor._
import cz.noveit.database._
import com.mongodb.casbah.Imports._
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import scala.concurrent.Await
import cz.noveit.database.DatabaseCommand
import cz.noveit.database.ProcessTaskOnCollection
import cz.noveit.database.GetCollection
import cz.noveit.database.DatabaseResult

/**
 * Created by Wlsek on 11.2.14.
 */


sealed trait SocialDatabaseMessages extends DatabaseAdapterMessage

case class GetFriendshipsForUser(user: String) extends DatabaseCommandMessage with SocialDatabaseMessages

case class Friendship(users: Pair[String, String], state: Int)

case class AllFriendShips(friendships: List[Friendship]) extends DatabaseCommandResultMessage with SocialDatabaseMessages

case class AddFriendRequest(fromUserName: String, toUserName: String) extends DatabaseCommandMessage with SocialDatabaseMessages

case class AddFriendRequestSuccessful(userName: String) extends DatabaseCommandResultMessage with SocialDatabaseMessages

case class AddFriendRequestFailedUserDoesntExists(userName: String) extends DatabaseCommandResultMessage with SocialDatabaseMessages

case class AddFriendRequestFailedAlreadyPresent(userName: String) extends DatabaseCommandResultMessage with SocialDatabaseMessages

case class AddFriendRequestFailed(userName: String) extends DatabaseCommandResultMessage with SocialDatabaseMessages

case class AcceptFriend(fromUserName: String, whichUserName: String) extends DatabaseCommandMessage with SocialDatabaseMessages
case class AcceptFriendDone(user: String)  extends DatabaseCommandResultMessage with SocialDatabaseMessages

case class RemoveFriend(fromUserName: String, whichUserName: String) extends DatabaseCommandMessage with SocialDatabaseMessages
case class RemoveFriendDone(user: String)  extends DatabaseCommandResultMessage with SocialDatabaseMessages

class SocialDatabaseAdapter(val databaseController: ActorSelection, databaseName: String, usersCollection: String, friendshipsCollection: String) extends Actor with ActorLogging {


  def receive = {
    case dc: DatabaseCommand =>
      dc.command match {
        case m: AddFriendRequest =>
          val replyTo = sender
          try {
            implicit val timeout = Timeout(5 seconds)
            var future = databaseController ? GetCollection(databaseName, usersCollection)
            val userCollection = Await.result(future, timeout.duration).asInstanceOf[MongoCollection]

            future = databaseController ? GetCollection(databaseName, friendshipsCollection)
            val friendshipCollection = Await.result(future, timeout.duration).asInstanceOf[MongoCollection]

            if (userCollection.findOne(MongoDBObject("userName" -> m.toUserName)).isEmpty) {
              replyTo ! DatabaseResult(AddFriendRequestFailedUserDoesntExists(m.toUserName), dc.replyHash)
            } else {

              if (friendshipCollection.findOne(MongoDBObject("users" -> Array(m.fromUserName, m.toUserName))).isEmpty) {
                friendshipCollection.save(MongoDBObject("users" -> Array(m.fromUserName, m.toUserName), "state" -> 0))
                replyTo ! DatabaseResult(AddFriendRequestSuccessful(m.toUserName), dc.replyHash)
              } else {
                replyTo ! DatabaseResult(AddFriendRequestFailedAlreadyPresent(m.toUserName), dc.replyHash)
              }
            }
          } catch {
            case t: Throwable => replyTo ! DatabaseResult(AddFriendRequestFailed(m.toUserName), dc.replyHash)
          }

        case GetFriendshipsForUser(user) =>
          val replyTo = sender
          databaseController ! ProcessTaskOnCollection(
            (c: MongoCollection) => {
              replyTo ! DatabaseResult(AllFriendShips(c.find(MongoDBObject("users" -> user)).map(found => {
                Friendship({
                  val friends =  found.getAs[MongoDBList]("users").getOrElse(MongoDBList(0)).toList.collect {case s: String => s}
                   Pair(
                      friends(0),
                      friends(1)
                   )
                }, found.getAs[Int]("state").getOrElse(0))
              }).toList), dc.replyHash)
            } , databaseName, friendshipsCollection)


        case AcceptFriend(fromUser, toUser) =>
          val replyTo = sender
          databaseController ! ProcessTaskOnCollection(
            (c: MongoCollection) => {
              c.update(MongoDBObject("users" -> Array(fromUser, toUser)), MongoDBObject("$set" -> MongoDBObject("state" -> 99)))
              replyTo ! DatabaseResult(AcceptFriendDone(toUser), dc.replyHash)
            }, databaseName, friendshipsCollection
          )

        case RemoveFriend(fromUser, toUser) =>
          val replyTo = sender
          databaseController ! ProcessTaskOnCollection(
            (c: MongoCollection) => {
              c.remove(MongoDBObject("users" -> Array(fromUser, toUser)))
              replyTo ! DatabaseResult(RemoveFriendDone(toUser), dc.replyHash)
            }, databaseName, friendshipsCollection
          )
      }

    case t => throw new UnsupportedOperationException(t.toString)
  }
}
