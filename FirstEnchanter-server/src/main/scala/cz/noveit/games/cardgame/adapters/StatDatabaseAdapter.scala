package cz.noveit.games.cardgame.adapters

import cz.noveit.database._
import akka.actor.{ActorSelection, ActorLogging, Actor, ActorRef}
import cz.noveit.database.DatabaseCommand
import com.mongodb.casbah.Imports._

import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import scala.concurrent.Await

/**
 * Created by Wlsek on 18.2.14.
 */

sealed trait StatDatabaseMessages extends DatabaseAdapterMessage

case class GetOverviewForUser(userName: String) extends StatDatabaseMessages with DatabaseCommandMessage

case class OverviewForUser(practiceStat: PracticeStatistics, rankedStat: RankedStatistics, matches: List[MatchResult], transactions: List[Transaction], rating: Int, rankedRank: String) extends StatDatabaseMessages with DatabaseCommandResultMessage

case class PracticeStatistics(oneVsOne: PracticeConcreteTeamStatistic, twoVsTwo: PracticeConcreteTeamStatistic)

case class PracticeConcreteTeamStatistic(pvp: StatisticTrinity, pve: StatisticTrinity)

case class RankedStatistics(oneVsOne: StatisticTrinity, twoVsTwo: StatisticTrinity)

case class StatisticTrinity(games: Int, wins: Int, loss: Int)

case class MatchResult(gameType: Int, enemyType: Int, teamType: Int, resultType: Int, duration: Long, started: Long, winners: List[String], losers: List[String], activeDeck: String)

case class Transaction(value: Double, currency: String, details: String, time: Long)

class StatDatabaseAdapter(val databaseController: ActorSelection, databaseName: String, statCollection: String, matchCollection: String, transactionCollection: String, scoreBoardCollection: String) extends Actor with ActorLogging {
  def receive = {
    case dc: DatabaseCommand =>
      dc.command match {
        case GetOverviewForUser(userName) =>
          val replyTo = sender
          databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
            c.findOne(MongoDBObject("userName" -> userName)).map(found => {
              implicit val timeout = Timeout(30 seconds)
              var future = databaseController ? ProcessTaskOnCollectionAndReplyToSender((c: MongoCollection, s: ActorRef) => {
                val found = c.findOne(MongoDBObject("userName" -> userName))
                if(found.isEmpty) {
                   s ! List()
                } else {
                   s ! found.get.getAs[MongoDBList]("transactions").getOrElse(MongoDBList()).collect {
                    case o: BasicDBObject => o
                  }.take(20).map(transactionFound => {
                    Transaction(
                      transactionFound.getAs[Double]("value").getOrElse(-1),
                      transactionFound.getAs[String]("currency").getOrElse(""),
                      transactionFound.getAs[String]("details").getOrElse(""),
                      transactionFound.getAs[Long]("time").getOrElse(0)
                    )
                   })
                }
              }, databaseName, transactionCollection)

              val transactions = Await.result(future, timeout.duration).asInstanceOf[List[Transaction]]

              future = databaseController ? ProcessTaskOnCollectionAndReplyToSender((c: MongoCollection, s: ActorRef) => {
                s ! c.find(MongoDBObject("$or" -> MongoDBList(MongoDBObject("winners.userName" -> userName), MongoDBObject("losers.userName" -> userName)))).limit(20).map(matchFound => {
                  var activeDeck = ""
                  MatchResult(
                    matchFound.getAs[Int]("gameType").getOrElse(-1),
                    matchFound.getAs[Int]("enemyType").getOrElse(-1),
                    matchFound.getAs[Int]("teamType").getOrElse(-1),
                    matchFound.getAs[Int]("resultType").getOrElse(-1),
                    matchFound.getAs[Long]("duration").getOrElse(-1),
                    matchFound.getAs[Long]("started").getOrElse(-1),
                    matchFound.getAs[MongoDBList]("winners").getOrElse(MongoDBList()).toList.collect {
                      case o: BasicDBObject => o
                    }
                      .map(o => {
                      val un = o.getAs[String]("userName").getOrElse("")
                      if (un.equals(userName)) activeDeck = o.getAs[String]("activeDeck").getOrElse("")
                      un
                    }),
                    matchFound.getAs[MongoDBList]("losers").getOrElse(MongoDBList()).toList.collect {
                      case o: BasicDBObject => o
                    }
                      .map(o => {
                      val un = o.getAs[String]("userName").getOrElse("")
                      if (un.equals(userName)) activeDeck = o.getAs[String]("activeDeck").getOrElse("")
                      un
                    }),
                    activeDeck
                  )
                }).toList
              }, databaseName, matchCollection)

              val matches = Await.result(future, timeout.duration).asInstanceOf[List[MatchResult]]

              future = databaseController ? ProcessTaskOnCollectionAndReplyToSender((c: MongoCollection, s: ActorRef) => {
                s ! c.findOne(MongoDBObject("players.user" -> userName)).map(scoreBoardFound => {
                  scoreBoardFound.getAs[String]("name").getOrElse("")
                }).map(s => s).getOrElse("")
              }, databaseName, scoreBoardCollection)

              val rank = Await.result(future, timeout.duration).asInstanceOf[String]

              var overview: Option[OverviewForUser] = None


              found.getAs[BasicDBObject]("practice").map(practice => {
                found.getAs[BasicDBObject]("ranked").map(ranked => {
                  practice.getAs[BasicDBObject]("1v1").map(practiceOneVsOne => {
                    practice.getAs[BasicDBObject]("2v2").map(practiceTwoVsTwo => {
                      ranked.getAs[BasicDBObject]("1v1").map(rankedOneVsOne => {
                        ranked.getAs[BasicDBObject]("2v2").map(rankedTwoVsTwo => {
                          practiceOneVsOne.getAs[BasicDBObject]("pve").map(practiceOneVsOnePve => {
                            practiceOneVsOne.getAs[BasicDBObject]("pvp").map(practiceOneVsOnePvp => {
                              practiceTwoVsTwo.getAs[BasicDBObject]("pve").map(practiceTwoVsTwoPve => {
                                practiceTwoVsTwo.getAs[BasicDBObject]("pvp").map(practiceTwoVsTwoPvp => {
                                  overview = Some(OverviewForUser(
                                    PracticeStatistics(
                                      PracticeConcreteTeamStatistic(
                                        convertObjectToStatisticTrinity(practiceOneVsOnePvp),
                                        convertObjectToStatisticTrinity(practiceOneVsOnePve)
                                      ),
                                      PracticeConcreteTeamStatistic(
                                        convertObjectToStatisticTrinity(practiceTwoVsTwoPvp),
                                        convertObjectToStatisticTrinity(practiceTwoVsTwoPve)
                                      )),
                                    RankedStatistics(
                                      convertObjectToStatisticTrinity(rankedOneVsOne),
                                      convertObjectToStatisticTrinity(rankedTwoVsTwo)
                                    ),
                                    matches, transactions, found.getAs[Int]("mmrating").getOrElse(0), rank))
                                })
                              })
                            })
                          })
                        })
                      })
                    })
                  })
                })
              })


              overview.map(ofu => replyTo ! DatabaseResult(ofu, dc.replyHash))
            })
          }, databaseName, statCollection)
      }

    case t => throw new UnsupportedOperationException(t.toString)
  }

  def convertObjectToStatisticTrinity(o: BasicDBObject): StatisticTrinity = {
    StatisticTrinity(o.getAs[Int]("games").getOrElse(-1), o.getAs[Int]("win").getOrElse(-1), o.getAs[Int]("loss").getOrElse(-1))
  }
}
