package cz.noveit.games.cardgame.adapters

import cz.noveit.database._
import akka.actor.{ActorRef, ActorLogging, Actor, ActorSelection}
import com.mongodb.casbah.Imports._
import cz.noveit.database.DatabaseCommand
import scala.util.Random
import cz.noveit.database

/**
 * Created by Wlsek on 28.5.14.
 */


sealed trait StoreDatabaseMessages extends DatabaseAdapterMessage

case object FindAllStoreItems extends DatabaseCommandMessage with StoreDatabaseMessages

object StoreItemType extends Enumeration {
  val StoreItemTypeBooster = Value
}

object TransactionType extends Enumeration {
  val TransactionTypeStore, TransactionTypeInGameMoney = Value
}

trait StoreItem {
  val storeId: String
  val unlockAbles: List[String]
  val storeItemType: StoreItemType.Value
  val inGameMoneyCost: Int
}

case class BoosterStoreItem(deckIconId: Int, val storeId: String, val unlockAbles: List[String], val storeItemType: StoreItemType.Value, val inGameMoneyCost: Int) extends StoreItem

case class AllStoreItems(boosters: List[StoreItem]) extends DatabaseCommandResultMessage with StoreDatabaseMessages

case class GetStoreItem(storeItemId: String) extends DatabaseCommandMessage with StoreDatabaseMessages

case class StoreItemFound(item: StoreItem) extends DatabaseCommandResultMessage with StoreDatabaseMessages

case object CannotFindStoreItem extends DatabaseCommandResultMessage with StoreDatabaseMessages

case class FindTransaction(transactionId: String) extends DatabaseCommandMessage with StoreDatabaseMessages

case class TransactionFound(transactionId: String, currency: String, details: String, time: Long, userName: String, value: Double, itemId: String, transactionType: TransactionType.Value) extends DatabaseCommandResultMessage with StoreDatabaseMessages

case object TransactionNotFound extends DatabaseCommandResultMessage with StoreDatabaseMessages

case class SaveTransaction(transactionId: String, currency: String, details: String, time: Long, userName: String, value: Double, itemId: String, transactionType: TransactionType.Value) extends DatabaseCommandMessage with StoreDatabaseMessages

case object SaveTransactionSuccessful extends DatabaseCommandResultMessage with StoreDatabaseMessages

case object SaveTransactionFailed extends DatabaseCommandResultMessage with StoreDatabaseMessages

case class CompleteTransactionDetails(currency: String, value: Double, itemId: String, transactionId: String) extends DatabaseCommandMessage with StoreDatabaseMessages

case object CompleteTransactionDetailsSuccessful extends DatabaseCommandResultMessage with StoreDatabaseMessages

case object CompleteTransactionDetailsFailed extends DatabaseCommandResultMessage with StoreDatabaseMessages

case class GetUnlockAblesForStoreItem(itemId: String) extends DatabaseCommandMessage with StoreDatabaseMessages

case class UnlockAblesForStoreItem(unlockAbles: List[String]) extends DatabaseCommandResultMessage with StoreDatabaseMessages

/*
*
* transactionCollection: {
*  userName : "",
*  inGameAccountBalance: NumberInt(0),
 *  transactions : [
*     {transactionId : "",
*     currency : "",
*     details : "",
*     time : NumberLong(0),
*     value : 0.0,
*     itemId: "" }
 *  ]
 *
 *
* }
*
* "storeItemsCollection : {
*   "storeItemType" : StoreItemTypeBooster,
*   "deckIconId" : NumberInt(0),
*   "inGameMoneyCost" : NumberInt(0),
*   _id: "",
*   "unlockAbles" : [
*
*   ]
* }
*
* */

class StoreDatabaseAdapters(val databaseController: ActorSelection, databaseName: String, transactionCollection: String, storeItemsCollection: String, val usersCollection: String) extends Actor with ActorLogging {

  def receive = {
    case dc: DatabaseCommand =>
      dc.command match {
        case FindAllStoreItems =>
          val replyTo = sender
          databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
            try {
              replyTo ! DatabaseResult(AllStoreItems(c.find(MongoDBObject()).map(found => {
                toStoreItem(found)
              }).toList))
            } catch {
              case t: Throwable =>
                log.error("Error while getting store items: " + t)
                replyTo ! DatabaseResult(AllStoreItems(List()))
            }
          }, databaseName, storeItemsCollection)


        case GetStoreItem(item) =>
          val replyTo = sender

          databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
            try {
              val found = c.findOne(MongoDBObject("_id" -> item))
              replyTo ! DatabaseResult(StoreItemFound(toStoreItem(found.get)))
            } catch {
              case t: Throwable =>
                log.error("Error while getting store items: " + t)
                replyTo ! DatabaseResult(CannotFindStoreItem)
            }
          }, databaseName, storeItemsCollection)


        case FindTransaction(transactionId) =>
          val replyTo = sender

          databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
            try {
              val found = c.findOne(MongoDBObject("transactions.transactionId" -> transactionId), MongoDBObject("userName" -> 1, "transactions.$" -> 1))
              if (found.isEmpty) {
                replyTo ! DatabaseResult(TransactionNotFound)
              } else {
                val transactionDocument = found.get.getAs[MongoDBList]("transactions").getOrElse(MongoDBList(())).collect {
                  case o: BasicDBObject => o
                }.head

                replyTo ! DatabaseResult(TransactionFound(
                  transactionDocument.getAs[String]("transactionId").getOrElse(""),
                  transactionDocument.getAs[String]("currency").getOrElse(""),
                  transactionDocument.getAs[String]("details").getOrElse(""),
                  transactionDocument.getAs[Long]("time").getOrElse(0L),
                  found.get.getAs[String]("userName").getOrElse(""),
                  transactionDocument.getAs[Double]("value").getOrElse(0),
                  transactionDocument.getAs[String]("itemId").getOrElse(""),
                  TransactionType.withName(transactionDocument.getAs[String]("transactionType").getOrElse(""))
                ))
              }
            } catch {
              case t: Throwable =>
                log.error("Error while getting store items: " + t)
                replyTo ! DatabaseResult(TransactionNotFound)
            }
          }, databaseName, transactionCollection)


        case SaveTransaction(transactionId, currency, details, time, userName, value, itemId, transactionType) =>
          val replyTo = sender

          databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
            try {
              val updateQuery = MongoDBObject.newBuilder
              updateQuery += "$push" -> MongoDBObject(
                "transactions" -> MongoDBObject(
                  "transactionId" -> transactionId,
                  "currency" -> currency,
                  "details" -> details,
                  "time" -> time,
                  "value" -> value,
                  "itemId" -> itemId,
                  "transactionType" -> transactionType.toString
                )
              )

              transactionType match {
                case TransactionType.TransactionTypeInGameMoney =>
                  updateQuery += "$inc" -> MongoDBObject("inGameAccountBalance" -> (value * (-1)))
                case t =>
              }

              c.update(MongoDBObject("userName" -> userName), updateQuery.result, upsert = true)
              replyTo ! DatabaseResult(SaveTransactionSuccessful)
            } catch {
              case t: Throwable =>
                log.error("Error while getting save transaction: " + t)
                replyTo ! DatabaseResult(SaveTransactionFailed)
            }
          }, databaseName, transactionCollection)


        case CompleteTransactionDetails(currency, value, itemId, transactionId) =>
          val replyTo = sender
          try {
            databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
              c.update(MongoDBObject("transactions.transactionId" -> transactionId, "transactions.itemId" -> itemId), MongoDBObject(
                "$set" -> MongoDBObject(
                  "transactions.$.currency" -> currency,
                  "transactions.$.value" -> value
                )
              ))
            }, databaseName, transactionCollection)
            replyTo ! DatabaseResult(CompleteTransactionDetailsSuccessful)
          } catch {
            case t: Throwable =>
              log.error("Error while update transaction: " + t)
              replyTo ! DatabaseResult(CompleteTransactionDetailsSuccessful)
          }

        case GetUnlockAblesForStoreItem(storeItem) =>
          val replyTo = sender
          databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
            try {
              replyTo ! DatabaseResult(UnlockAblesForStoreItem({
                val found = c.findOne(MongoDBObject("_id" -> storeItem))
                found.get.getAs[MongoDBList]("unlockAbles").getOrElse(MongoDBList()).collect {
                  case s: String => s
                }.toList
              }))
            } catch {
              case t: Throwable =>
                log.error("Error while getting unlockables: " + t)
                replyTo ! DatabaseResult(UnlockAblesForStoreItem(List()))
            }
          }, databaseName, storeItemsCollection)
      }
    case t => throw new UnsupportedOperationException(t.toString)
  }

  def toStoreItem(o: MongoDBObject): StoreItem = {
    StoreItemType.withName(o.getAs[String]("storeItemType").getOrElse("")) match {
      case StoreItemType.StoreItemTypeBooster => BoosterStoreItem(
        o.getAs[Int]("deckIconId").getOrElse(-1),
        o.getAs[String]("_id").getOrElse(""),
        o.getAs[MongoDBList]("unlockAbles").getOrElse(MongoDBList()).collect {
          case s: String => s
        }.toList,
        StoreItemType.StoreItemTypeBooster,
        o.getAs[Int]("inGameMoneyCost").getOrElse(0)
      ).asInstanceOf[StoreItem]
    }
  }
}
