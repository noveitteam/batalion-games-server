package cz.noveit.games.cardgame.adapters

import akka.actor.{ActorLogging, Actor, ActorSelection}
import cz.noveit.database._
import cz.noveit.games.cardgame.engine.unlockable.{UnlockedContent, UnlockAbleMethod, UnlockAbleType}
import cz.noveit.games.cardgame.engine.card._
import com.mongodb.casbah.MongoCollection
import com.mongodb.casbah.commons.Imports._
import cz.noveit.database.ProcessTaskOnCollection
import cz.noveit.database.DatabaseResult
import cz.noveit.database.DatabaseCommand

/**
 * Created by Wlsek on 18.6.14.
 */


/*
*
* val unlockAbles = {
    {
        "unlockAbleId" : "cz.blabla.blabla.bla",
        "unlockAbleType" : "UnlockAbleTypeCardSkin",
        "skinId"  : NumberInt(0)
    },
    {
        "unlockAbleId" : "cz.blabla.blabla.bla",
        "unlockAbleType" : "UnlockAbleTypeAvatar",
        "avatarId"  : NumberInt(0)
    },
    {
        "unlockAbleId" : "cz.blabla.blabla.bla",
        "unlockAbleType" : "UnlockAbleTypeDeckIcon",
        "deckIconId"  : NumberInt(0)
    },
    {
        "unlockAbleId" : "cz.blabla.blabla.bla",
        "unlockAbleType" : "UnlockAbleTypeBooster",
        "boosterRequiredSize" : NumberInt(10),
        "boosterLimitations" : [
          {
            "rarity" : NumberInt(2),
            "min" : NumberInt(0),
            "max" : NumberInt(10)
          }
        ],
        "boosterTable" : {
             "cardId": "",
             "chance": 0.2,
             "rarity" : "common"
        }
    },
    {
        "unlockAbleId" : "cz.blabla.blabla.bla",
        "unlockAbleType" : "UnlockAbleTypeDeck",
        "deckId" : ""
    },
}

                  //UnlockAbleMethodTransaction, UnlockAbleMethodRedemptionCode
val unlockAbleHistory = {
   {
    "time" : NumberLong(1000),
    "unlockAbleId" : "",
    "unlockAbleType" : "",
    "unlockAbleMethodType" : "UnlockAbleMethodTransaction",
    "transactionId"  : "" ,
    "user"  : ""
   },
      {
       "time" : NumberLong(1000),
       "unlockAbleId" : "",
       "unlockAbleType" : "",
       "unlockAbleMethodType" : "UnlockAbleMethodRedemptionCode",
       "redemptionCode"  : "185-555-566-488" ,
       "user"  : ""
      }
}
*
* */

trait UnlockAbleMessages extends DatabaseAdapterMessage

case class GetUnlockAble(unlockAbleId: String) extends UnlockAbleMessages with DatabaseCommandMessage

trait UnlockAble extends UnlockAbleMessages with DatabaseCommandResultMessage {
  val unlockAbleId: String
  val unlockAbleType: UnlockAbleType.Value
}

case class UnlockAbleAvatar(val unlockAbleId: String, avatarId: Int) extends UnlockAble {
  val unlockAbleType = UnlockAbleType.UnlockAbleTypeAvatar
}

case class UnlockAbleCardSkin(val unlockAbleId: String, skinId: Int) extends UnlockAble {
  val unlockAbleType = UnlockAbleType.UnlockAbleTypeCardSkin
}


case class UnlockAbleDeckIcon(val unlockAbleId: String, deckIcon: Int) extends UnlockAble {
  val unlockAbleType = UnlockAbleType.UnlockAbleTypeDeckIcon
}

case class BoosterLimitation(min: Int, max: Int, rarity: CardRarity)

case class BoosterTableRow(cardId: String, chance: Float, rarity: CardRarity)

case class UnlockAbleBooster(val unlockAbleId: String, requiredSize: Int, boosterLimitations: List[BoosterLimitation], boosterTable: List[BoosterTableRow]) extends UnlockAble {
  val unlockAbleType = UnlockAbleType.UnlockAbleTypeBooster
}

case class UnlockAbleDeck(val unlockAbleId: String, deckId: String) extends UnlockAble {
  val unlockAbleType = UnlockAbleType.UnlockAbleTypeDeck
}


case class CannotGetUnlockAble(unlockAbleId: String) extends UnlockAbleMessages with DatabaseCommandResultMessage

case class UnlockAbleAlreadyInHistory(unlockAbleId: String, unlockAbleType: UnlockAbleType.Value, unlockAbleMethod: UnlockAbleMethod.Value, time: Long, user: String)

case class GetUnlockAblesAlreadyInHistoryForUser(askingUnlockAbles: List[String], user: String) extends UnlockAbleMessages with DatabaseCommandMessage

case class UnlockAblesAlreadyInHistory(unlockAbles: List[UnlockAbleAlreadyInHistory]) extends UnlockAbleMessages with DatabaseCommandResultMessage

case object CannotGetUnlockablesInHistory extends UnlockAbleMessages with DatabaseCommandResultMessage

case class GetUnlockAbles(askingUnlockAbles: List[String]) extends UnlockAbleMessages with DatabaseCommandMessage

case class UnlockAbles(unlockAbles: List[UnlockAble]) extends UnlockAbleMessages with DatabaseCommandResultMessage

case object CannotGetUnlockAbles extends UnlockAbleMessages with DatabaseCommandResultMessage



/*
* Unlock avatar for user
* */

case class UnlockAvatar(avatarId: Int, user: String) extends UnlockAbleMessages with DatabaseCommandMessage

case class AvatarUnlocked(avatarId: Int, user: String) extends UnlockAbleMessages with DatabaseCommandResultMessage with UnlockedContent

case class CannotUnlockAvatar(avatarId: Int, user: String) extends UnlockAbleMessages with DatabaseCommandResultMessage

/*
* Unlock deck icon for user
* */

case class UnlockDeckIcon(deckIcon: Int, user: String) extends UnlockAbleMessages with DatabaseCommandMessage

case class DeckIconUnlocked(deckIcon: Int, user: String) extends UnlockAbleMessages with DatabaseCommandResultMessage with UnlockedContent

case class CannotUnlockDeckIcon(deckIcon: Int, user: String) extends UnlockAbleMessages with DatabaseCommandResultMessage

/*
* Unlock card skin for user
* */

case class UnlockCardSkin(cardSkin: Int, user: String) extends UnlockAbleMessages with DatabaseCommandMessage

case class CardSkinUnlocked(cardSkin: Int, user: String) extends UnlockAbleMessages with DatabaseCommandResultMessage with UnlockedContent

case class CannotUnlockCardSkin(cardSkin: Int, user: String) extends UnlockAbleMessages with DatabaseCommandResultMessage


/*
* Unlock cards for user (booster e.g.)
* */

case class GetOwnedCardsForUser(user: String) extends UnlockAbleMessages with DatabaseCommandMessage

case class OwnedCardsForUser(user: String, cards: List[String]) extends UnlockAbleMessages with DatabaseCommandResultMessage
case object CannotGetOwndedCardsForUser  extends UnlockAbleMessages with DatabaseCommandResultMessage
case class UnlockCards(cards: List[String], user: String) extends UnlockAbleMessages with DatabaseCommandMessage

case class CardsUnlocked(cards: List[String], user: String) extends UnlockAbleMessages with DatabaseCommandResultMessage with UnlockedContent

case class CannotUnlockCards(cards: List[String], user: String) extends UnlockAbleMessages with DatabaseCommandResultMessage


/*
* Unlock unlockable deck for user
* */

case class GetUnlockAbleDeckDetails(deckId: String) extends UnlockAbleMessages with DatabaseCommandMessage

case class UnlockAbleDeckDetails(deck: Deck) extends UnlockAbleMessages with DatabaseCommandResultMessage

case class CannotGetDeckDetails(deckId: String) extends UnlockAbleMessages with DatabaseCommandResultMessage

case class UnlockDeckForUser(deck: Deck, user: String) extends UnlockAbleMessages with DatabaseCommandMessage

case class UnlockAbleDeckForUserUnlocked(deck: Deck, user: String) extends UnlockAbleMessages with DatabaseCommandResultMessage with UnlockedContent

case class CannotUnlockUnlockAbleDeckForUser(deck: Deck, user: String) extends UnlockAbleMessages with DatabaseCommandResultMessage


case class WriteUnlockAbleLog(
                               unlockAbleId: String,
                               unlockAbleType: UnlockAbleType.Value,
                               unlockAbleMethodType: UnlockAbleMethod.Value,
                               time: Long,
                               transactionId: Option[String] = None,
                               redemptionCode: Option[String] = None,
                               user: String
                               ) extends UnlockAbleMessages with DatabaseCommandMessage


class UnlockAbleDatabaseAdapter(
                                 val databaseController: ActorSelection,
                                 databaseName: String,
                                 unlockAbleCollection: String,
                                 unlockAbleHistoryCollection: String,
                                 userHasCardsCollection: String,
                                 userUnlockedCollection: String,
                                 decksCollection: String
                                 ) extends Actor with ActorLogging {
  def receive = {

    case dc: DatabaseCommand =>
      dc.command match {
        case GetUnlockAble(unlockAbleId) => {
          val replyTo = sender

          databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
            try {
              val found = c.findOne(MongoDBObject("unlockAbleId" -> unlockAbleId))
              replyTo ! DatabaseResult(toUnlockAble(found.get))
            } catch {
              case t: Throwable =>
                log.error("Error: " + t + " while getting unlockAble: " + unlockAbleId)
                replyTo ! DatabaseResult(CannotGetUnlockAble(unlockAbleId))
            }
          }, databaseName, unlockAbleCollection)
        }

        case  GetUnlockAbles(unlockAbles) => {
          val replyTo = sender

          databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
            try {
              replyTo ! DatabaseResult(  UnlockAbles(
                c.find(MongoDBObject("unlockAbleId" -> MongoDBObject("$in" -> unlockAbles.toArray))).map(f => {
                  toUnlockAble(f)
                }).toList
               ))
            } catch {
              case t: Throwable =>
                replyTo ! DatabaseResult(CannotGetUnlockAbles)
            }
          }, databaseName, unlockAbleCollection)
        }
        case WriteUnlockAbleLog(unlockAbleId, unlockAbleType, unlockAbleMethod, time, transactionId, redemptionCode, user) =>
          databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
            try {
              val o = MongoDBObject.newBuilder
              o += "unlockAbleId" -> unlockAbleId
              o += "unlockAbleType" -> unlockAbleType.toString
              o += "unlockAbleMethod" -> unlockAbleMethod.toString
              o += "time" -> time
              o += "userName" -> user
              transactionId.map(tr => o += "transactionId" -> tr)
              redemptionCode.map(rc => o += "redemptionCode" -> rc)

              c.save(o.result())
            } catch {
              case t: Throwable =>
                log.error("Error while saving unlockAble log: " + unlockAbleId + " for user: " + user)
            }
          }, databaseName, unlockAbleHistoryCollection)


        case GetUnlockAblesAlreadyInHistoryForUser(unlockAbles, user) => {
          val replyTo = sender
          databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
            try {
              replyTo ! DatabaseResult(UnlockAblesAlreadyInHistory(c.find(MongoDBObject("unlockAbleId" -> MongoDBObject("$in" -> unlockAbles.toArray), "userName" -> user)).map(f => {
                UnlockAbleAlreadyInHistory(
                  f.getAs[String]("unlockAbleId").get,
                  UnlockAbleType.withName(f.getAs[String]("unlockAbleType").get),
                  UnlockAbleMethod.withName(f.getAs[String]("unlockAbleMethod").get),
                  f.getAs[Long]("time").get,
                  user
                )
              }).toList))
            } catch {
              case t: Throwable =>
                replyTo ! DatabaseResult(CannotGetUnlockablesInHistory)
            }
          }, databaseName, unlockAbleHistoryCollection)
        }

        case UnlockAvatar(avatarId, user) => {
          val replyTo = sender
          databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
            try {
              c.update(
                MongoDBObject("userName" -> user),
                MongoDBObject("$addToSet" -> MongoDBObject("unlockedAvatars" -> avatarId))
              )
              replyTo ! DatabaseResult(AvatarUnlocked(avatarId, user))
            } catch {
              case t: Throwable =>
                log.error("Error cannot unlock avatar: " + avatarId + " for user: " + user)
                replyTo ! DatabaseResult(CannotUnlockAvatar(avatarId, user))
            }
          }, databaseName, userUnlockedCollection)

        }

        case UnlockDeckIcon(deckIcon, user) => {
          val replyTo = sender
          databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
            try {
              c.update(
                MongoDBObject("userName" -> user),
                MongoDBObject("$addToSet" -> MongoDBObject("unlockedDeckIcons" -> deckIcon))
              )
              replyTo ! DatabaseResult(DeckIconUnlocked(deckIcon, user))
            } catch {
              case t: Throwable =>
                log.error("Error cannot unlock deck icon: " + deckIcon + " for user: " + user)
                replyTo ! DatabaseResult(CannotUnlockDeckIcon(deckIcon, user))
            }
          }, databaseName, userUnlockedCollection)

        }

        case UnlockCardSkin(cardSkin, user) => {
          val replyTo = sender
          databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
            try {
              c.update(
                MongoDBObject("userName" -> user),
                MongoDBObject("$addToSet" -> MongoDBObject("unlockedSkins" -> cardSkin)), upsert = true
              )
              replyTo ! DatabaseResult(CardSkinUnlocked(cardSkin, user))
            } catch {
              case t: Throwable =>
                log.error("Error cannot unlock card skin: " + cardSkin + " for user: " + user)
                replyTo ! DatabaseResult(CannotUnlockCardSkin(cardSkin, user))
            }
          }, databaseName, userUnlockedCollection)

        }

        case GetOwnedCardsForUser(user) =>
          val replyTo = sender
          databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
            try {
              val found = c.findOne(MongoDBObject("userName" -> user))
              replyTo ! DatabaseResult(OwnedCardsForUser(
                user,
                found.get.getAs[MongoDBList]("cards").getOrElse(MongoDBList()).collect {
                  case s: String => s
                }.toList
              ))

            } catch {
              case t: Throwable =>
                log.error("Error cannot get user cards: " + user)
                replyTo ! DatabaseResult(CannotGetOwndedCardsForUser)
            }
          }, databaseName, userHasCardsCollection)


        case UnlockCards(cards, user) =>
          val replyTo = sender
          databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
            try {
              c.update(MongoDBObject("userName" -> user), MongoDBObject("$pushAll" -> MongoDBObject("cards" -> cards)), upsert = true)
              replyTo ! DatabaseResult(CardsUnlocked(cards, user))
            } catch {
              case t: Throwable =>
                log.error("Error cannot get user cards: " + user)
                replyTo ! DatabaseResult(CannotUnlockCards(cards, user))
            }
          }, databaseName, userHasCardsCollection)


        case GetUnlockAbleDeckDetails(deckId) =>
          val replyTo = sender
          databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
            try {
              var result: Option[Deck] = None
              c.findOne(MongoDBObject("_id" -> new ObjectId(deckId))).map(d => {
                result = Some(Deck(
                  deckId,
                  d.getAs[String]("name").getOrElse(""),
                  d.getAs[Int]("iconId").getOrElse(-1),
                  d.getAs[BasicDBObject]("elementsRating").map(obj => ElementsRating(
                    obj.getAs[Double]("water").getOrElse(-1.0).toFloat,
                    obj.getAs[Double]("fire").getOrElse(-1.0).toFloat,
                    obj.getAs[Double]("earth").getOrElse(-1.0).toFloat,
                    obj.getAs[Double]("air").getOrElse(-1.0).toFloat,
                    obj.getAs[Double]("neutral").getOrElse(-1.0).toFloat
                  )).getOrElse(ElementsRating(-1, -1, -1, -1, -1)),
                  d.getAs[MongoDBList]("cards").getOrElse(MongoDBList(0)).toList.collect {
                    case s: BasicDBObject => s
                  }.map(o => CountableCardWithSkin(o.getAs[String]("cardId").getOrElse(""), o.getAs[Int]("count").getOrElse(-1), o.getAs[Int]("skinId"))).toList,
                  ""
                ))
              })

              if (result.isEmpty) {
                replyTo ! DatabaseResult(CannotGetDeckDetails(deckId))
              } else {
                replyTo ! DatabaseResult(UnlockAbleDeckDetails(result.get))
              }
            } catch {
              case t: Throwable =>
                replyTo ! DatabaseResult(CannotGetDeckDetails(deckId))
            }
          }, databaseName, decksCollection)


        case UnlockDeckForUser(deck, user) => {
          val replyTo = sender
          databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
            try {
              c.save(
                MongoDBObject(
                  "user" -> user,
                  "name" -> deck.name,
                  "cards" -> deck.cards.map(card => {
                    val document = MongoDBObject.newBuilder
                    document += "cardId" -> card.cardId
                    document += "count" -> card.count
                    card.skinId.map(skin => "skinId" -> skin)
                    document.result()
                  }).toArray,
                  "elementsRating" -> MongoDBObject(
                    "water" -> deck.elementsRatio.water.toDouble,
                    "air" -> deck.elementsRatio.air.toDouble,
                    "earth" -> deck.elementsRatio.earth.toDouble,
                    "fire" -> deck.elementsRatio.fire.toDouble
                  ),
                  "iconId" -> deck.icon
                )
              )

              databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
                try {
                  c.update(MongoDBObject("userName" -> user), MongoDBObject("$pushAll" -> MongoDBObject("cards" -> deck.cards.map(card => card.cardId).toArray)), upsert = true)
                  replyTo ! DatabaseResult(UnlockAbleDeckForUserUnlocked(deck, user))
                } catch {
                  case t: Throwable =>
                    replyTo ! DatabaseResult(CannotUnlockUnlockAbleDeckForUser(deck, user))
                }
              }, databaseName, userHasCardsCollection)
            } catch {
              case t: Throwable =>
                replyTo ! DatabaseResult(CannotUnlockUnlockAbleDeckForUser(deck, user))
            }
          }, databaseName, decksCollection)
        }


        case t => throw new UnsupportedOperationException(t.toString)
      }
  }

  def toUnlockAble(o: MongoDBObject): UnlockAble = {
    val unlockAbleType = UnlockAbleType.withName(o.getAs[String]("unlockAbleType").getOrElse(""))
    val unlockAbleId = o.getAs[String]("unlockAbleId").getOrElse("")

    unlockAbleType match {
      case UnlockAbleType.UnlockAbleTypeAvatar =>
        UnlockAbleAvatar(unlockAbleId, o.getAs[Int]("avatarId").get)

      case UnlockAbleType.UnlockAbleTypeDeckIcon =>
        UnlockAbleDeckIcon(unlockAbleId, o.getAs[Int]("deckIconId").get)

      case UnlockAbleType.UnlockAbleTypeCardSkin =>
        UnlockAbleCardSkin(unlockAbleId, o.getAs[Int]("skinId").get)

      case UnlockAbleType.UnlockAbleTypeBooster =>
        UnlockAbleBooster(
          unlockAbleId,
          o.getAs[Int]("boosterRequiredSize").getOrElse(0),
          o.getAs[MongoDBList]("boosterLimitations").getOrElse(MongoDBList()).collect {
            case o: BasicDBObject => {
              BoosterLimitation(
                o.getAs[Int]("min").getOrElse(0),
                o.getAs[Int]("max").getOrElse(0),
                o.getAs[Int]("rarity").getOrElse(1) match {
                  case 1 => Common
                  case 2 => Uncommon
                  case 3 => Rare
                  case 4 => Hero
                }
              )
            }
          }.toList,
          o.getAs[MongoDBList]("boosterTable").getOrElse(MongoDBList()).collect {
            case o: BasicDBObject => {
              BoosterTableRow(
                o.getAs[String]("cardId").getOrElse(""),
                o.getAs[Double]("chance").getOrElse(0.0).toFloat,
                o.getAs[Int]("rarity").getOrElse(1) match {
                  case 1 => Common
                  case 2 => Uncommon
                  case 3 => Rare
                  case 4 => Hero
                }
              )
            }
          }.toList
        )

      case UnlockAbleType.UnlockAbleTypeDeck =>
        UnlockAbleDeck(unlockAbleId, o.getAs[String]("deckId").get)

    }
  }
}
