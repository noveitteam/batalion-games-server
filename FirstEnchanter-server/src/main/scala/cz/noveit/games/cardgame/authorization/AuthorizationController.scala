package cz.noveit.games.cardgame.authorization

import scala.collection.immutable
import akka.actor._
import cz.noveit.proto.serialization.{MessageSerializer, MessageEvent, MessageEventContext}
import cz.noveit.connector.websockets.WebSocketsContext
import cz.noveit.games.cardgame.adapters._
import cz.noveit.games.cluster.dispatch._
import scala.concurrent.duration.Duration
import cz.noveit.token._
import cz.noveit.games.cluster.dispatch.DispatchMessageBroadcastWithoutMe
import cz.noveit.service.ContextDestroy
import cz.noveit.token.TokenInvalid
import cz.noveit.games.cardgame.adapters.RegistrationFailedUserExists
import cz.noveit.games.cluster.dispatch.ClusterMessageEvent
import cz.noveit.games.cardgame.adapters.AddUser
import cz.noveit.token.TokenValidate
import scala.Some
import cz.noveit.connector.SendMessageToContext
import cz.noveit.token.TokenContext
import cz.noveit.database.DatabaseCommand
import cz.noveit.games.cardgame.adapters.UserQueryResultFail
import cz.noveit.token.TokenValid
import cz.noveit.games.cardgame.adapters.RegistrationSuccessful
import cz.noveit.proto.serialization.MessageEvent
import cz.noveit.games.cluster.GameWorkerUserMetrics
import cz.noveit.games.cardgame.adapters.UserQueryResultAccept
import cz.noveit.games.cluster.registry.SubscribeActorForModuleName
import cz.noveit.games.cardgame.adapters.RegistrationFailedUnknown
import cz.noveit.database.DatabaseResult
import cz.noveit.games.cardgame.adapters.SetUserLastLogged
import cz.noveit.games.cardgame.adapters.UserQuery
import cz.noveit.smtp.template.EmailTemplate
import cz.noveit.smtp.EmailMessage

import cz.noveit.proto.firstenchanter.FirstEnchanterAuthorization._
import java.net.URLEncoder
import scala.collection.immutable.HashMap
import cz.noveit.games.cardgame.engine.CreatePlayerForContext

/**
 * Created by Wlsek on 23.1.14.
 */

case class  AddPublicMessage(message: String)

case class  RemovePublicMessage(message: String)

case class AuthorizedContextPair(userName: String, ctx: MessageEventContext)

class AuthorizationController(dispatcher: ActorRef, publicDispatcher: ActorRef, playerDispatcher: ActorRef, clusterDispatcher: ActorRef,  databaseParams: HashMap[String, String] = new HashMap()) extends Actor with ActorLogging {

  case class AuthorizationCancelInCluster(uid: String) extends ClusterMessage

  val databaseAdapter = context.actorOf(Props(classOf[AuthorizationDatabaseAdapter],context.actorSelection("/user/databaseController"), databaseParams("databaseName"), databaseParams("userCollection")))
  val clusterRegistry = context.system.actorSelection("/user/clusterRegistry")

  val smtpService = context.system.actorSelection("/user/smtpService")
  val tokenService = context.system.actorSelection("/user/tokenService")

  val moduleName = "FirstEnchanterAuthorization"

  clusterRegistry ! SubscribeActorForModuleName(self, moduleName)

  var publicMessages: List[String] = List()
  var authorizedContext: List[AuthorizedContextPair] = List()
  var contextsToBeAuthorized: List[AuthorizedContextPair] = List()

  override def receive = {
    case msg: ClusterMessageEvent =>
      msg.message match {
        case AuthorizationCancelInCluster(uid) =>
          authorizedContext.filter(pair => pair.userName.equals(uid)).map(pair => {
            if (pair.ctx.isInstanceOf[WebSocketsContext]) pair.ctx.asInstanceOf[WebSocketsContext].disconnect()
          })
      }

    case msg: AddPublicMessage =>
      publicMessages = msg.message :: publicMessages

    case msg: RemovePublicMessage =>
      publicMessages = publicMessages.filterNot(m => msg.message.equals(m))

    case msg: DatabaseResult =>
      msg.result match {
        case accept: UserQueryResultAccept =>
          contextsToBeAuthorized = contextsToBeAuthorized.filterNot(pair => pair == accept.aPair)
          authorizedContext = AuthorizedContextPair(accept.user.uid, accept.aPair.ctx) :: authorizedContext

          val authOk = AuthorizationSuccessful
            .newBuilder()
            .setUserDetails(
              AuthorizationSuccessful .User
                .newBuilder()
                .setNickName(accept.user.nickname)
                .setEmail(accept.user.email)
                .setLastLogged(accept.user.lastLogged)
                .setAvatarId(accept.user.avatar.getOrElse(-1))
                .build()
            ).build()


          playerDispatcher ! CreatePlayerForContext(accept.user, accept.aPair.ctx)
          dispatcher ! SendMessageToContext(MessageEvent(authOk, moduleName, msg.replyHash), accept.aPair.ctx)
          databaseAdapter ! DatabaseCommand(SetUserLastLogged(accept.user.uid, System.currentTimeMillis()), msg.replyHash)
          clusterDispatcher ! DispatchMessageBroadcastWithoutMe(ClusterMessageEvent(AuthorizationCancelInCluster(accept.user.uid), moduleName, ""))
          clusterDispatcher ! DispatchMessageBroadcastWithoutMe(ClusterMessageEvent(GameWorkerUserMetrics(authorizedContext.size), moduleName, ""))

        case fail: UserQueryResultFail =>
          contextsToBeAuthorized = contextsToBeAuthorized.filterNot(pair => pair == fail.aPair)

          val authFail = AuthorizationFailed
            .newBuilder()
            .setDetails("")
            .setReason(AuthorizationFailed.AuthorizationFailedReason.NotValidCredencials)
            .build()

          dispatcher ! SendMessageToContext(MessageEvent(authFail, moduleName, msg.replyHash), fail.aPair.ctx)

        case RegistrationFailedUnknown(aPair) =>
          val regFail = RegisterUserFailed.newBuilder().setReason(RegisterUserFailed.Reason.UNKNOWN).build()
          dispatcher ! SendMessageToContext(MessageEvent(regFail, moduleName, msg.replyHash), aPair.ctx)

        case RegistrationFailedUserExists(aPair) =>
          val regFail = RegisterUserFailed.newBuilder().setReason(RegisterUserFailed.Reason.USEREXISTS).build()
          dispatcher ! SendMessageToContext(MessageEvent(regFail, moduleName, msg.replyHash), aPair.ctx)

        case RegistrationSuccessful(aPair, email) =>                  // dispatcher: ActorRef, smtpService: ActorRef, module: String, replyHash: String, userName: String, email: String, ctx: MessageEventContext
          val actor = context.actorOf(Props(classOf[RegistrationEmailSendWorker], dispatcher, smtpService, moduleName, msg.replyHash, aPair.userName, email, aPair.ctx))
          tokenService ! GenerateToken(TokenContext(actor, aPair.userName), (24 * 60 * 60 * 1000))
      }

    case cd: ContextDestroy =>
      authorizedContext = authorizedContext.filterNot(c => c.ctx == cd.context)
      playerDispatcher  ! cd
      clusterDispatcher ! DispatchMessageBroadcastWithoutMe(ClusterMessageEvent(GameWorkerUserMetrics(authorizedContext.size), "metrics", ""))
      dispatcher        ! cd

    case msg: MessageEvent =>
      msg.message match {
        case m: Authorize =>
          authorizedContext.filter(pair => pair.userName.equals(m.getUserName)).map(pair => {
            if (pair.ctx.isInstanceOf[WebSocketsContext]) pair.ctx.asInstanceOf[WebSocketsContext].disconnect()
          })

          msg.context.map(ctx => {
            val pair = AuthorizedContextPair(m.getUserName, ctx)
            contextsToBeAuthorized = pair :: contextsToBeAuthorized
            databaseAdapter ! DatabaseCommand(UserQuery(m.getUserName, m.getPassword, pair), msg.replyHash)
          })

        case m: AuthorizationCancel =>
          msg.context.map(ctx => {
            authorizedContext = authorizedContext.filterNot(cx => cx.ctx == ctx)
            if (ctx.isInstanceOf[WebSocketsContext]) ctx.asInstanceOf[WebSocketsContext].disconnect()
          })

        case m: RegisterUser => {
          msg.context.map(ctx => {
            val pair = AuthorizedContextPair(m.getUserName, ctx)
            databaseAdapter ! DatabaseCommand(AddUser(cz.noveit.games.cardgame.adapters.User(m.getUserName, "", m.getEmail, 0L, Some(m.getAvatarId), Some(m.getPassword)), pair), msg.replyHash)
          })
        }

        case m: EmailConfirmation =>
          msg.context.map(ctx => authorizedContext.filter(cx => cx.ctx == ctx).map(pair => {
            val actor = context.actorOf(Props(classOf[TokenConfirmationWorker], dispatcher, databaseAdapter, msg, pair.userName))
            tokenService ! TokenValidate(TokenContext(actor, pair.userName), m.getKey)
          })
          )

        case m =>
          if (publicMessages.contains(m.getClass.getSimpleName)) {
            publicDispatcher ! msg
          } else {
            msg.context.map(ctx => if (!authorizedContext.filter(cx => cx.ctx == ctx).isEmpty) playerDispatcher ! msg)
          }
      }

    case t => throw new UnsupportedOperationException("Unknown message")
  }
}


class TokenConfirmationWorker(dispatcher: ActorRef, database: ActorRef, originalMessage: MessageEvent, userName: String) extends Actor with ActorLogging {

  context.setReceiveTimeout(Duration.create("30 seconds"))

  def receive = {
    case TokenValid(ctx) =>
      dispatcher ! SendMessageToContext(MessageEvent(EmailConfirmationSuccessful.newBuilder().build(), originalMessage.module, originalMessage.replyHash), originalMessage.context.get)
      database ! EmailConfirmed(userName)
      context.stop(self)

    case TokenInvalid(ctx) =>
      dispatcher ! SendMessageToContext(MessageEvent(EmailConfirmationFailed.newBuilder().build(), originalMessage.module, originalMessage.replyHash), originalMessage.context.get)
      context.stop(self)

    case ReceiveTimeout =>
      dispatcher ! SendMessageToContext(MessageEvent(EmailConfirmationFailed.newBuilder().build(), originalMessage.module, originalMessage.replyHash), originalMessage.context.get)
      context.stop(self)
  }
}

class RegistrationEmailSendWorker(dispatcher: ActorRef, smtpService: ActorSelection, module: String, replyHash: String, userName: String, email: String, ctx: MessageEventContext) extends Actor with ActorLogging {

  context.setReceiveTimeout(Duration.create("30 seconds"))

  def receive = {
    case GeneratedToken(ct, token) =>
      val regSuccess = RegisterUserSuccessful.newBuilder().build()
      dispatcher ! SendMessageToContext(MessageEvent(regSuccess, module, replyHash), ctx)

      val lines = scala.io.Source.fromFile("emailtemplates/registration.html").mkString
      val template = new EmailTemplate(lines)
      template("%USERNAME%" -> userName)
    //  template("%EMAILVALIDATIONLINKGAME%" -> URLEncoder.encode("\"app://cz.noveit.games.firstenchanter/registration?key=" + token.key + "\"" ))
      template("%EMAILVALIDATIONLINKBROWSER%" -> ("\"http://www.firstenchanter.com/registration?key=" + token.key + "\"" ))
      //println(template)
      smtpService ! EmailMessage("Welcome!", email, "registration@firstenchanter.com", "Thank you for registration, please enter: " + token + " for email confirmation.", template.toString)
      context.stop(self)

    case ReceiveTimeout =>
      val regFail = RegisterUserFailed.newBuilder().setReason(RegisterUserFailed.Reason.UNKNOWN).build()
      dispatcher ! SendMessageToContext(MessageEvent(regFail, module, replyHash), ctx)
      context.stop(self)
  }
}

