package cz.noveit.games.cardgame.engine

import akka.actor.{Props, ActorRef, ActorLogging, Actor}
import cz.noveit.games.cardgame.WorldServices
import scala.collection.immutable.HashMap
import cz.noveit.games.cardgame.adapters._
import cz.noveit.games.cluster.dispatch._
import cz.noveit.games.cardgame.engine.handlers.ServiceHandlersModules
import cz.noveit.games.cardgame.adapters.GetPlayerConversations
import cz.noveit.games.cardgame.adapters.BansForConversation
import cz.noveit.games.cardgame.adapters.PlayerInvitation
import cz.noveit.games.cardgame.adapters.MessageList
import scala.Some
import cz.noveit.games.cardgame.adapters.RemoveBanForConversation
import cz.noveit.games.cardgame.adapters.CreateBanForConversation
import cz.noveit.database.DatabaseCommand
import cz.noveit.games.cardgame.adapters.ConversationDetails
import cz.noveit.games.cardgame.adapters.GetMessageListForConversation
import cz.noveit.games.cardgame.adapters.SaveConversation
import cz.noveit.games.cardgame.adapters.RemoveMemberFromConversation
import cz.noveit.games.cluster.registry.SubscribeActorForModuleName
import cz.noveit.games.cardgame.adapters.ConversationInvitations
import cz.noveit.games.cardgame.adapters.ConversationFound
import cz.noveit.database.DatabaseResult
import cz.noveit.games.cardgame.adapters.SaveConversationInvite
import cz.noveit.games.cardgame.adapters.GetConversationInvitations
import cz.noveit.games.cardgame.adapters.AddMemberToConversation
import cz.noveit.games.cardgame.adapters.RemoveConversationInvitation
import cz.noveit.games.cardgame.adapters.PlayerInvitations
import cz.noveit.games.cardgame.adapters.GetPlayerInvitations
import cz.noveit.games.cardgame.adapters.PlayerConversations
import cz.noveit.games.cardgame.adapters.WriteMessageToConversation
import cz.noveit.games.cardgame.adapters.QueryPlayerBanStateForConversation
import cz.noveit.games.cardgame.adapters.FindBansForConversation
import sun.reflect.generics.reflectiveObjects.NotImplementedException

/**
 * Created by Wlsek on 13.5.14.
 */

object ConversationAutomaticCreationType extends Enumeration {
  val ConversationAutomaticCreationTypeBySystem, ConversationAutomaticCreationTypeByUser = Value
}

object ConversationType extends Enumeration {
  val ChatRoomConversationType, PrivateConversationType, GameAllyConversationType, GameConversationAll, ConversationAll = Value
}

object MessageType extends Enumeration {
  val NormalMessageType, PredefinedMessageType = Value
}

object ConversationCreationFailedType extends Enumeration {
  val ConversationCreationFailedAlreadyExists, ConversationCreationFailedUnknown = Value
}

trait ConversationQueryType

object ConversationQueryTypeById extends ConversationQueryType

object ConversationQueryTypeByName extends ConversationQueryType

case class Conversation(id: String, name: String, creator: String, creationType: ConversationAutomaticCreationType.Value, conversationType: ConversationType.Value, public: Boolean, permanent: Boolean, creationTime: Long)

case class GetConversationDetails(handler: ActorRef, conversationId: String)

case class Message(time: Long, content: String, fromPlayer: String, messageType: MessageType.Value)

case class CreateConversation(handler: ActorRef, conversation: Conversation)

case class ConversationCreated(conversation: Conversation)

case class ConversationCreationFailed(reason: ConversationCreationFailedType.Value, conversation: Conversation)

case class JoinConversationById(handler: ActorRef, conversationId: String, playerName: String)

case class JoinConversationByIdSuccessful(conversation: Conversation, member: String)

case class JoinConversationByIdFailed(conversationId: String)

case class JoinConversationByName(handler: ActorRef, conversationName: String, playerName: String)

case class JoinConversationByNameSuccessful(conversation: Conversation, member: String)

case class JoinConversationByNameFailed(conversationName: String)

case class JoinConversationAutomatically(handler: ActorRef, conversationId: String, playerName: String)

case class JoinConversationAutomaticallySuccessful(conversationId: String, member: String)

case class JoinConversationAutomaticallyFailed(conversationId: String)

case class LeaveConversation(conversationId: String, playerName: String)

case class LeaveConversationSuccessful(conversationId: String, member: String)

case class InvitePlayerToJoinConversation(conversationId: String, player: String, byPlayer: String)

case class PlayerConversationInvitation(conversation: Conversation, by: String, val playerName: String, replyHash: String = "", serviceHandlerModule: String = "") extends ClusterMessage with PlayerControllerMessageWithPlayerNameAndServiceHandlerModule

case class ConversationInviteDecline(conversationId: String, playerName: String)

case class GetListOfConversationsForPlayer(handler: ActorRef, playerName: String, conversationType: ConversationType.Value)

case class ListOfConversations(conversations: List[Conversation])

case class GetConversationHistory(handler: ActorRef, conversationId: String)

case class ConversationHistory(conversationId: String, messages: List[Message])

case class GetConversationInvitationsForPlayer(handler: ActorRef, player: String)

case class ConversationInvitationsForPlayer(invitations: List[PlayerInvitation])

trait ConversationMembershipChangeType

object ConversationMembershipChangeTypeJoin extends ConversationMembershipChangeType

object ConversationMembershipChangeTypeLeft extends ConversationMembershipChangeType

case class GetMembersOfConversation(h: ActorRef, conversationId: String)

case class ConversationWithMembers(conversationId: String, members: List[String])

case class ConversationMembershipChanged(conversationId: String, member: String, changeType: ConversationMembershipChangeType) extends ClusterMessage with BroadcastMessageBetweenPlayersWithServiceModule {
  val serviceHandlerModule = ServiceHandlersModules.cardGameConversation
}

case class SendMessageToPlayer(message: Message, playerName: String, friends: Boolean)

case class SendMessageToConversation(message: Message, conversationId: String)

case class ConversationReceivedMessage(conversationId: String, message: Message) extends ClusterMessage with BroadcastMessageBetweenPlayersWithServiceModule {
  val serviceHandlerModule = ServiceHandlersModules.cardGameConversation
}

case class KickPlayerFromConversation(conversationId: String, player: String)

case class BanPlayerFromConversation(conversationId: String, player: String)

case class UnBanPlayerFromConversation(conversationId: String, player: String)

case class GetListOfBansForConversation(handler: ActorRef, conversationId: String)

case class ListOfBannedPlayersForConversation(conversationId: String, players: List[String])

class ConversationController(val clusterDispatcher: ActorRef, databaseParams: HashMap[String, String]) extends Actor with ActorLogging {

  val databaseHandler = context.actorOf(
    Props(classOf[ConversationControllerDatabaseAdapter],
      context.actorSelection("/user/databaseController"),
      databaseParams("databaseName"),
      databaseParams("conversationsCollection"),
      databaseParams("conversationsMembershipCollection"),
      databaseParams("conversationsInvitationCollection"),
      databaseParams("conversationsBansCollection")
    ))

  def receive = {
    case CreateConversation(h, conversation) =>
      val conversationWithId = if(conversation.id == ""){
        val conversationId = h.toString() + System.currentTimeMillis().toString
        conversation.copy(id = conversationId)
      } else {
        conversation
      }
      context.actorOf(Props(classOf[ConversationCreationWorker], h, clusterDispatcher, databaseHandler, conversationWithId))

    case JoinConversationById(h, c, player) =>
      context.actorOf(Props(classOf[ConversationJoinWorker], h, self, databaseHandler, c, player, ConversationQueryTypeById))

    case JoinConversationByName(h, c, player) =>
      context.actorOf(Props(classOf[ConversationJoinWorker], h, self, databaseHandler, c, player, ConversationQueryTypeByName))

    case LeaveConversation(c, m) =>
      context.actorOf(Props(classOf[ConversationLeaveWorker], self, databaseHandler, c, m))

    case InvitePlayerToJoinConversation(conversation, player, byPlayer) =>
      context.actorOf(Props(classOf[ConversationInviteWorker], clusterDispatcher, databaseHandler, conversation, player, byPlayer))

    case ConversationInviteDecline(conversation, player) =>
      databaseHandler ! DatabaseCommand(RemoveConversationInvitation(conversation, player))

    case SendMessageToPlayer(message, player, friends) =>
      context.actorOf(Props(classOf[SendMessageToPlayerWorker], clusterDispatcher, databaseHandler, message, player, friends))

    case SendMessageToConversation(message, conversation) =>
      context.actorOf(Props(classOf[SendMessageToConversationWorker], clusterDispatcher, databaseHandler, message, conversation))

    case GetListOfConversationsForPlayer(h, player, convType) =>
      context.actorOf(Props(classOf[GetListOfConversationsForPlayerWorker], h, databaseHandler, player, convType))

    case GetConversationHistory(h, conversation) =>
      context.actorOf(Props(classOf[GetConversationHistoryWorker], h, databaseHandler, conversation))

    case GetConversationInvitationsForPlayer(h, player) =>
      context.actorOf(Props(classOf[ConversationInvitationsForPlayerWorker], h, databaseHandler, player))

    case KickPlayerFromConversation(conversation, player) =>
      context.actorOf(Props(classOf[KickPlayerFromConversationWorker], clusterDispatcher, databaseHandler, conversation, player))

    case BanPlayerFromConversation(conversation, player) =>
      context.actorOf(Props(classOf[BanPlayerFromConversationWorker], clusterDispatcher, databaseHandler, conversation, player))

    case UnBanPlayerFromConversation(conversation, player) =>
      context.actorOf(Props(classOf[UnBanPlayerFromConversationWorker], clusterDispatcher, databaseHandler, conversation, player))

    case GetListOfBansForConversation(h, conversation) =>
      context.actorOf(Props(classOf[GetListOfBansForConversationWorker], h, databaseHandler, conversation))

    case GetConversationDetails(h, conversationId) =>
      context.actorOf(Props(classOf[GetConversationDetailsWorker], h, databaseHandler, conversationId))

    case GetMembersOfConversation(h, conversationId) =>
      context.actorOf(Props(classOf[GetConversationMembersWorker], h, databaseHandler, conversationId))

    case JoinConversationAutomatically(handler, conversationId, player) =>
      context.actorOf(Props(classOf[ConversationJoinAutomaticallyWorker], handler, self, databaseHandler, conversationId, player))

    case JoinConversationByIdSuccessful(c, m) =>
      clusterDispatcher ! DispatchMessageBroadcast(ClusterMessageEvent(ConversationMembershipChanged(c.id, m, ConversationMembershipChangeTypeJoin), "FirstEnchanterPlayer", ""))

    case JoinConversationByNameSuccessful(c, m) =>
      clusterDispatcher ! DispatchMessageBroadcast(ClusterMessageEvent(ConversationMembershipChanged(c.id, m, ConversationMembershipChangeTypeJoin), "FirstEnchanterPlayer", ""))

    case JoinConversationAutomaticallySuccessful(c, m) =>
      clusterDispatcher ! DispatchMessageBroadcast(ClusterMessageEvent(ConversationMembershipChanged(c, m, ConversationMembershipChangeTypeJoin), "FirstEnchanterPlayer", ""))

    case LeaveConversationSuccessful(c, m) =>
      clusterDispatcher ! DispatchMessageBroadcast(ClusterMessageEvent(ConversationMembershipChanged(c, m, ConversationMembershipChangeTypeLeft), "FirstEnchanterPlayer", ""))


  }
}

class ConversationDetailsWorker(replyTo: ActorRef, databaseHandler: ActorRef, conversation: String, queryType: ConversationQueryType) extends Actor with ActorLogging {
  databaseHandler ! DatabaseCommand(ConversationDetails(conversation, queryType), "")

  def receive = {
    case DatabaseResult(result, hash) =>
      result match {
        case (ConversationFound(_) | CannotFindConversation) =>
          replyTo ! result
          context.stop(self)
      }
  }
}

class ConversationInvitationsWorker(replyTo: ActorRef, databaseHandler: ActorRef, conversationId: String) extends Actor with ActorLogging {
  databaseHandler ! DatabaseCommand(GetConversationInvitations(conversationId), "")

  def receive = {
    case DatabaseResult(result, hash) =>
      result match {
        case msg: ConversationInvitations =>
          replyTo ! msg
          context.stop(self)
      }
  }
}

class ConversationInvitationsForPlayerWorker(replyTo: ActorRef, databaseHandler: ActorRef, player: String) extends Actor with ActorLogging {
  databaseHandler ! DatabaseCommand(GetPlayerInvitations(player))

  def receive = {
    case DatabaseResult(result, hash) =>
      result match {
        case PlayerInvitations(invitations) =>
          replyTo ! ConversationInvitationsForPlayer(invitations)
          context.stop(self)
      }
  }
}

class ConversationCreationWorker(handler: ActorRef, dispatcher: ActorRef, databaseHandler: ActorRef, conversation: Conversation) extends Actor with ActorLogging {
  databaseHandler ! DatabaseCommand(SaveConversation(conversation), "")

  def receive = {
    case DatabaseResult(result, hash) =>
      result match {
        case SaveConversationSuccess =>
          handler ! ConversationCreated(conversation)
          databaseHandler ! DatabaseCommand(AddMemberToConversation(conversation.id, conversation.creator))

        case SaveConversationFailedDuplicate =>
          handler ! ConversationCreationFailed(ConversationCreationFailedType.ConversationCreationFailedAlreadyExists, conversation)
          context.stop(self)

        case SaveConversationFailedException =>
          handler ! ConversationCreationFailed(ConversationCreationFailedType.ConversationCreationFailedUnknown, conversation)
          context.stop(self)

        case AddMemberToConversationFailed =>
          handler ! ConversationCreationFailed(ConversationCreationFailedType.ConversationCreationFailedUnknown, conversation)
          context.stop(self)

        case AddMemberToConversationSuccess =>
          dispatcher ! DispatchMessageBroadcast(ClusterMessageEvent(ConversationMembershipChanged(conversation.id, conversation.creator, ConversationMembershipChangeTypeJoin), PlayerControllerModule.module))
          context.stop(self)

      }

    case t => log.error("Unknown message received: " + t.toString)
  }
}

class ConversationJoinWorker(handler: ActorRef, parent: ActorRef, databaseHandler: ActorRef, conversation: String, playerName: String, queryType: ConversationQueryType) extends Actor with ActorLogging {
  context.actorOf(Props(classOf[ConversationDetailsWorker], self, databaseHandler, conversation, queryType))
  var conversationDetails: Option[Conversation] = None

  def receive = {
    case ConversationFound(c) =>
      conversationDetails = Some(c)
      if (!c.public) {
        context.actorOf(Props(classOf[ConversationInvitationsWorker], self, databaseHandler, c.id))
      } else {
        databaseHandler ! DatabaseCommand(QueryPlayerBanStateForConversation(c.id, playerName))
      }

    case ConversationInvitations(invitations) => {
      if (invitations.find(p => p.whom.equals(playerName)).isDefined) {
        databaseHandler ! DatabaseCommand(RemoveConversationInvitation(conversationDetails.get.id, playerName))
        databaseHandler ! DatabaseCommand(AddMemberToConversation(conversationDetails.get.id, playerName))
      } else {
        joinFailed
        context.stop(self)
      }
    }

    case CannotFindConversation =>
      joinFailed
      context.stop(self)

    case DatabaseResult(result, hash) =>
      result match {
        case ConversationBanStateNotBanned =>
          databaseHandler ! DatabaseCommand(AddMemberToConversation(conversationDetails.get.id, playerName))

        case AddMemberToConversationSuccess =>
          joinSuccess
          context.stop(self)

        case AddMemberToConversationFailed =>
          joinFailed
          context.stop(self)

        case ConversationBanStateBanned =>
          joinFailed
          context.stop(self)
      }
  }

  def joinFailed = {
    queryType match {
      case ConversationQueryTypeById =>
        handler ! JoinConversationByIdFailed(conversation)

      case ConversationQueryTypeByName =>
        handler ! JoinConversationByNameFailed(conversation)
    }
  }

  def joinSuccess = {
    queryType match {
      case ConversationQueryTypeById =>
        parent ! JoinConversationByIdSuccessful(conversationDetails.get, playerName)
        handler ! JoinConversationByIdSuccessful(conversationDetails.get, playerName)

      case ConversationQueryTypeByName =>
        parent ! JoinConversationByIdSuccessful(conversationDetails.get, playerName)
        handler ! JoinConversationByNameSuccessful(conversationDetails.get, playerName)
    }
  }
}

class ConversationJoinAutomaticallyWorker(handler: ActorRef, parent: ActorRef, databaseHandler: ActorRef, conversationId: String, playerName: String) extends Actor with ActorLogging {
  databaseHandler ! DatabaseCommand(AddMemberToConversation(conversationId, playerName), "")

  def receive = {
    case DatabaseResult(result, hash) =>
      result match {
        case AddMemberToConversationSuccess =>
          joinSuccess
          context.stop(self)

        case AddMemberToConversationFailed =>
          joinFailed
          context.stop(self)
      }
  }

  def joinFailed = {
    handler ! JoinConversationAutomaticallyFailed
  }

  def joinSuccess = {
    parent ! JoinConversationAutomaticallySuccessful(conversationId, playerName)
    handler ! JoinConversationAutomaticallySuccessful(conversationId, playerName)
  }
}

class ConversationLeaveWorker(parent: ActorRef, databaseHandler: ActorRef, conversationId: String, playerName: String) extends Actor with ActorLogging {
  databaseHandler ! DatabaseCommand(RemoveMemberFromConversation(conversationId, playerName), "")

  def receive = {
    case DatabaseResult(result, hash) =>
      result match {
        case RemoveMemberFromConversationSuccess =>
          joinSuccess
          context.stop(self)

        case RemoveMemberFromConversationFailed =>
          context.stop(self)
      }
  }

  def joinSuccess = {
    parent ! LeaveConversationSuccessful(conversationId, playerName)
  }
}

class ConversationInviteWorker(dispatcher: ActorRef, databaseHandler: ActorRef, conversationId: String, playerName: String, byPlayer: String) extends Actor with ActorLogging {
  context.actorOf(Props(classOf[ConversationDetailsWorker], self, databaseHandler, conversationId, ConversationQueryTypeById))
  var conversationDetails: Option[Conversation] = None

  def receive = {
    case ConversationFound(c) =>
      conversationDetails = Some(c)
      context.actorOf(Props(classOf[ConversationInvitationsWorker], self, databaseHandler, c.id))

    case ConversationInvitations(invitations) =>
      if (invitations.find(i => i.whom.equals(playerName)).isEmpty) {
        databaseHandler ! DatabaseCommand(SaveConversationInvite(conversationId, byPlayer, playerName), "")
      } else {
        invitationSuccessful
        context.stop(self)
      }

    case CannotFindConversation =>
      context.stop(self)

    case DatabaseResult(result, hash) =>
      result match {
        case SaveConversationInviteSuccess =>
          invitationSuccessful
          context.stop(self)

        case SaveConversationInviteFailed =>
          context.stop(self)
      }
  }

  def invitationSuccessful = {
    dispatcher ! DispatchMessageBroadcast(ClusterMessageEvent(PlayerConversationInvitation(conversationDetails.get, byPlayer, playerName), PlayerControllerModule.module))
  }
}

class SendMessageToPlayerWorker(dispatcher: ActorRef, databaseHandler: ActorRef, message: Message, playerName: String, friends: Boolean) extends Actor with ActorLogging {

  val listToMakeId = List(playerName, message.fromPlayer).sorted

  val conversation = Conversation(
    listToMakeId.toString,
    "PM: " + playerName,
    message.fromPlayer,
    ConversationAutomaticCreationType.ConversationAutomaticCreationTypeByUser,
    ConversationType.PrivateConversationType,
    false,
    false,
    System.currentTimeMillis()
  )

  databaseHandler ! DatabaseCommand(SaveConversation(conversation))
  var noMembers = 0

  def receive = {
    case DatabaseResult(result, hash) =>
      result match {
        case SaveConversationSuccess =>
          databaseHandler ! DatabaseCommand(AddMemberToConversation(conversation.id, message.fromPlayer), message.fromPlayer)
          if (friends) {
            databaseHandler ! DatabaseCommand(AddMemberToConversation(conversation.id, playerName), playerName)
          } else {
            databaseHandler ! DatabaseCommand(SaveConversationInvite(conversation.id, message.fromPlayer, playerName))
          }

        case SaveConversationFailedDuplicate =>
          databaseHandler ! DatabaseCommand(WriteMessageToConversation(message, conversation.id))
          context.stop(self)

        case AddMemberToConversationSuccess =>
          dispatcher ! DispatchMessageBroadcast(ClusterMessageEvent(ConversationMembershipChanged(conversation.id, hash, ConversationMembershipChangeTypeJoin), PlayerControllerModule.module, ""))
          noMembers += 1
          testWriting

        case SaveConversationInviteSuccess =>
          dispatcher ! DispatchMessageBroadcast(ClusterMessageEvent(PlayerConversationInvitation(conversation, message.fromPlayer, playerName), PlayerControllerModule.module))
          noMembers += 1
          testWriting

        case WriteMessageToConversationSuccess =>
          dispatcher ! DispatchMessageBroadcast(ClusterMessageEvent(ConversationReceivedMessage(conversation.id, message), PlayerControllerModule.module))
          context.stop(self)

        case WriteMessageToConversationFailed =>
          context.stop(self)

        case SaveConversationFailedException =>
          context.stop(self)

        case AddMemberToConversationFailed =>
          context.stop(self)

        case SaveConversationInviteFailed =>
          context.stop(self)
      }
  }

  def testWriting = {
    if (noMembers == 2) {
      databaseHandler ! DatabaseCommand(WriteMessageToConversation(message, conversation.id))
    }
  }
}

class SendMessageToConversationWorker(dispatcher: ActorRef, databaseHandler: ActorRef, message: Message, conversationId: String) extends Actor with ActorLogging {
  databaseHandler ! DatabaseCommand(WriteMessageToConversation(message, conversationId))

  def receive = {
    case DatabaseResult(result, hash) =>
      result match {
        case WriteMessageToConversationFailed =>
          context.stop(self)

        case WriteMessageToConversationSuccess =>
          dispatcher ! DispatchMessageBroadcast(ClusterMessageEvent(ConversationReceivedMessage(conversationId, message), PlayerControllerModule.module))
          dispatcher ! DispatchServiceBroadcast(ClusterServiceEvent(ConversationReceivedMessage(conversationId, message), WorldServices.botConversationService))
          context.stop(self)
      }
  }
}

class GetListOfConversationsForPlayerWorker(handler: ActorRef, databaseHandler: ActorRef, player: String, conversationType: ConversationType.Value) extends Actor with ActorLogging {
  databaseHandler ! DatabaseCommand(GetPlayerConversations(player, conversationType))

  def receive = {
    case DatabaseResult(result, hash) =>
      result match {
        case PlayerConversations(conversations) =>
          handler ! ListOfConversations(conversations)
          context.stop(self)
      }
  }
}

class GetConversationHistoryWorker(handler: ActorRef, databaseHandler: ActorRef, conversationId: String) extends Actor with ActorLogging {
  databaseHandler ! DatabaseCommand(GetMessageListForConversation(conversationId))

  def receive = {
    case DatabaseResult(result, hash) =>
      result match {
        case MessageList(messages) =>
          handler ! ConversationHistory(conversationId, messages)
          context.stop(self)
      }
  }
}

class KickPlayerFromConversationWorker(dispatcher: ActorRef, databaseHandler: ActorRef, conversationId: String, player: String) extends Actor with ActorLogging {
  databaseHandler ! DatabaseCommand(RemoveMemberFromConversation(conversationId, player))

  def receive = {
    case DatabaseResult(result, hash) =>
      result match {
        case RemoveMemberFromConversationSuccess =>
          dispatcher ! DispatchMessageBroadcast(ClusterMessageEvent(ConversationMembershipChanged(conversationId, player, ConversationMembershipChangeTypeLeft), PlayerControllerModule.module))
          context.stop(self)
        case t =>
          context.stop(self)
      }
  }
}

class BanPlayerFromConversationWorker(dispatcher: ActorRef, databaseHandler: ActorRef, conversationId: String, player: String) extends Actor with ActorLogging {
  databaseHandler ! DatabaseCommand(RemoveMemberFromConversation(conversationId, player))

  def receive = {
    case DatabaseResult(result, hash) =>
      result match {
        case RemoveMemberFromConversationSuccess =>
          dispatcher ! DispatchMessageBroadcast(ClusterMessageEvent(ConversationMembershipChanged(conversationId, player, ConversationMembershipChangeTypeLeft), PlayerControllerModule.module))
          databaseHandler ! DatabaseCommand(CreateBanForConversation(conversationId, player))
        case t =>
          context.stop(self)
      }
  }
}

class UnBanPlayerFromConversationWorker(dispatcher: ActorRef, databaseHandler: ActorRef, conversationId: String, player: String) extends Actor with ActorLogging {
  databaseHandler ! DatabaseCommand(RemoveBanForConversation(conversationId, player))

  def receive = {
    case DatabaseResult(result, hash) =>
      result match {
        case t =>
          context.stop(self)
      }
  }
}

class GetListOfBansForConversationWorker(handler: ActorRef, databaseHandler: ActorRef, conversationId: String) extends Actor with ActorLogging {
  databaseHandler ! DatabaseCommand(FindBansForConversation(conversationId))

  def receive = {
    case DatabaseResult(result, hash) =>
      result match {
        case BansForConversation(bannedPlayers) =>
          handler ! ListOfBannedPlayersForConversation(conversationId, bannedPlayers)
          context.stop(self)
      }
  }
}

class GetConversationDetailsWorker(handler: ActorRef, databaseHandler: ActorRef, conversationId: String) extends Actor with ActorLogging {
  databaseHandler ! DatabaseCommand(ConversationDetails(conversationId, ConversationQueryTypeById))

  def receive = {
    case DatabaseResult(result, hash) =>
      result match {
        case m: ConversationFound =>
          handler ! result
          context.stop(self)

        case CannotFindConversation =>
          handler ! CannotFindConversation
          context.stop(self)
      }
  }
}

class GetConversationMembersWorker(handler: ActorRef, databaseHandler: ActorRef, conversationId: String) extends Actor with ActorLogging {
  databaseHandler ! DatabaseCommand(ConversationMembers(conversationId))

  def receive = {
    case DatabaseResult(result, hash) =>
      result match {
        case m: ConversationWithMembersFound =>
          handler ! ConversationWithMembers(conversationId, m.members)
          context.stop(self)
      }
  }
}