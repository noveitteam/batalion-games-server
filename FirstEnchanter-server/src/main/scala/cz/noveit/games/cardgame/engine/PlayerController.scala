package cz.noveit.games.cardgame.engine

import akka.actor._
import cz.noveit.proto.serialization.{MessageEvent, MessageEventContext}
import cz.noveit.games.cardgame.engine.handlers._
import cz.noveit.games.cluster.dispatch._
import cz.noveit.service.ContextDestroy
import cz.noveit.games.cardgame.adapters.User
import cz.noveit.proto.serialization.MessageEvent
import cz.noveit.games.cardgame.engine.handlers.PlayerServiceParameters
import scala.reflect.ClassTag
import cz.noveit.service.ContextDestroy
import cz.noveit.games.cardgame.engine.handlers.RefreshFriendsForUsers

import cz.noveit.games.cardgame.adapters.User
import cz.noveit.proto.serialization.MessageEvent
import cz.noveit.games.cardgame.engine.handlers.PlayerServiceParameters
import cz.noveit.games.cluster.registry.SubscribeActorForModuleName
import cz.noveit.service.ContextDestroy
import cz.noveit.games.cluster.dispatch.ClusterMessageEvent
import cz.noveit.games.cardgame.engine.handlers.RefreshFriendsForUsers
import cz.noveit.games.cardgame.adapters.User
import cz.noveit.proto.serialization.MessageEvent
import cz.noveit.games.cardgame.engine.handlers.PlayerServiceParameters
import cz.noveit.games.cluster.registry.SubscribeActorForModuleName
import scala.collection.immutable.HashMap

/**
 * Created by Wlsek on 10.2.14.
 */

case class AuthorizedContextPair(user: User, ctx: MessageEventContext, handler: ActorRef)

case class CreatePlayerForContext(user: User, context: MessageEventContext)

case class PlayerOnline(user: User, handler: ActorRef) extends ClusterMessage with CrossHandlerMessage with BroadcastMessageBetweenPlayersWithServiceModuleWithIgnoringPlayer {
  val playerName = user.nickname
  val serviceHandlerModule = ServiceHandlersModules.cardGameSocial
}

case class PlayerOffline(user: String, handler: ActorRef) extends ClusterMessage with CrossHandlerMessage with BroadcastMessageBetweenPlayersWithServiceModuleWithIgnoringPlayer {
  val playerName = user
  val serviceHandlerModule = ServiceHandlersModules.cardGameSocial
}

case class RequestPlayersStatus(playerUserName: String, replyTo: ActorRef) extends ClusterMessage with PlayerControllerMessage

case object GetNumberOfPlayers

trait PlayerControllerMessage

trait PlayerControllerMessageWithPlayerNameAndServiceHandlerModule extends CrossHandlerMessage {
  val playerName: String
  val serviceHandlerModule: String
}

trait BroadcastMessageBetweenPlayersWithServiceModule extends CrossHandlerMessage {
  val serviceHandlerModule: String
}

trait BroadcastMessageBetweenPlayersWithServiceModuleWithIgnoringPlayer extends CrossHandlerMessage {
  val playerName: String
  val serviceHandlerModule: String
}

object PlayerControllerModule {
  val module = "FirstEnchanterPlayer"
}

class PlayerController(val dispatcher: ActorRef, val clusterRegistry: ActorRef, serviceModuleFactory: ServiceHandlersModuleFactory, playerHandlerFactory: PlayerHandlerFactory, databaseParameters: HashMap[String, String]) extends Actor with ActorLogging {

  val moduleName = PlayerControllerModule.module
  val playersWorker = context.actorOf(Props(classOf[PlayerControllerPlayersWorker], self, serviceModuleFactory, playerHandlerFactory, databaseParameters))

  clusterRegistry ! SubscribeActorForModuleName(self, moduleName)

  def receive = {
    case msg: CreatePlayerForContext =>
      playersWorker ! msg

    case msg: ContextDestroy =>
      playersWorker ! msg

    case msg: ClusterMessageEvent =>
      playersWorker ! msg

    case msg: MessageEvent =>
      playersWorker ! msg

    case GetNumberOfPlayers =>
      playersWorker.tell(GetNumberOfPlayers, sender)

    case PlayerPairCreated(pair) =>
      dispatcher ! DispatchMessageBroadcast(ClusterMessageEvent(PlayerOnline(pair.user, pair.handler), moduleName, ""))

    case PlayerPairDestroyed(pair) =>
      dispatcher ! DispatchMessageBroadcast(ClusterMessageEvent(PlayerOffline(pair.user.nickname, self), moduleName, ""))
      pair.handler ! Kill

    case msg: RequestPlayersStatus =>
      dispatcher ! DispatchMessageBroadcast(ClusterMessageEvent(msg, moduleName, ""))

    case msg: RefreshFriendsForUsers =>
      context.actorOf(Props(classOf[DispatchRefreshFriendshipsWorker], dispatcher, moduleName))  ! msg

    case msg => throw new UnsupportedOperationException("unknown message")
  }
}

case class PlayerPairCreated(pair: AuthorizedContextPair)
case class PlayerPairDestroyed(pair: AuthorizedContextPair)

class PlayerControllerPlayersWorker(
                                     val parent: ActorRef,
                                     serviceModuleFactory: ServiceHandlersModuleFactory,
                                     playerHandlerFactory: PlayerHandlerFactory,
                                     databaseParameters: HashMap[String, String]
                                     ) extends Actor with ActorLogging {

  var players: List[AuthorizedContextPair] = List()
  def receive = {
    case msg: CreatePlayerForContext =>
      if (players.find(p => p.ctx.equals(msg.context)).isEmpty) {
        val actor = playerHandlerFactory.playerHandler(context, msg.user, msg.context, self, serviceModuleFactory, databaseParameters)
        val pair = AuthorizedContextPair(msg.user, msg.context, actor)
        players = pair :: players
        parent ! PlayerPairCreated(pair)
      }

    case msg: ContextDestroy =>
      players = players.filterNot(p => {
        val equals = p.ctx.equals(msg.context)
        if(equals) parent ! PlayerPairDestroyed(p)
        equals
      })

    case GetNumberOfPlayers =>
      sender ! players.size

    case msg: ClusterMessageEvent =>
      context.actorOf(Props(classOf[PlayerControllerClusterMessageEventWorker], players)) ! msg

    case msg: MessageEvent =>
      context.actorOf(Props(classOf[PlayerControllerMessageEventWorker], players)) ! msg
  }
}

case class PlayerControllerClusterMessageEventWorker(players: List[AuthorizedContextPair]) extends Actor with ActorLogging {
  def receive = {
    case ClusterMessageEvent(msg, module, replyHash) =>
      msg match {
        case RequestPlayersStatus(p, replyTo) =>
          players.find(pl => pl.user.nickname.equals(p)).map(pl => replyTo ! PlayerOnline(pl.user, pl.handler))
          context.stop(self)
        case msg: PlayerControllerMessageWithPlayerNameAndServiceHandlerModule =>
          players.filter(player => player.user.nickname.equals(msg.playerName)).map(found => found.handler ! ServiceModuleMessage(msg, msg.serviceHandlerModule, replyHash))
          context.stop(self)
        case msg: BroadcastMessageBetweenPlayersWithServiceModule =>
          players.map(p => p.handler ! ServiceModuleMessage(msg, msg.serviceHandlerModule, replyHash))
          context.stop(self)
        case msg: BroadcastMessageBetweenPlayersWithServiceModuleWithIgnoringPlayer =>
          players.filterNot(user => msg.playerName.equals(user.user.nickname)).map(p => p.handler ! ServiceModuleMessage(msg, msg.serviceHandlerModule))
          context.stop(self)
      }
  }
}

class PlayerControllerMessageEventWorker(players: List[AuthorizedContextPair]) extends Actor with ActorLogging {
  def receive = {
    case msg: MessageEvent =>
      msg.context.map(c => players.find(p => p.ctx.equals(c)).map(f => f.handler ! msg))
      context.stop(self)
  }
}

class DispatchRefreshFriendshipsWorker(dispatcher: ActorRef, moduleName: String) extends Actor with ActorLogging {
  def receive = {
    case RefreshFriendsForUsers(users) =>
      users.map(u => dispatcher ! DispatchMessageBroadcast(ClusterMessageEvent(RefreshFriendships(u), moduleName, "")))
      context.stop(self)
  }
}

trait CrossHandlerMessage

case class ServiceModuleMessage(message: CrossHandlerMessage, module: String, replyHash: String = "")

case class GetServiceModuleHandler(module: String)

class PlayerHandler(user: User, ctx: MessageEventContext, playerController: ActorRef, serviceModule: ServiceHandlersModuleFactory, databaseParameters: HashMap[String, String]) extends Actor with ActorLogging {

  val handlers = serviceModule.module(context, PlayerServiceParameters(ctx, self, user, databaseParameters)).handlers

  def receive = {
    case msg: MessageEvent =>
      handlers.find(h => h.module.equals(msg.module)).map(f => f.handler ! msg)

    case msg: PlayerOnline =>
      handlers.find(h => h.module.equals(ServiceHandlersModules.cardGameSocial)).map(h => h.handler ! msg)

    case msg: PlayerOffline =>
      handlers.find(h => h.module.equals(ServiceHandlersModules.cardGameSocial)).map(h => h.handler ! msg)

    case msg: PlayerControllerMessage =>
      playerController ! msg

    case msg: ServiceModuleMessage =>
      handlers.find(h => h.module.equals(msg.module)).map(h => h.handler.tell(msg, sender))

    case msg: GetServiceModuleHandler =>
      handlers.find(h => h.module equals msg.module).map(h => sender ! h.handler)
  }

  override def postStop() = {
    handlers.map(h => h.handler ! Kill)
  }
}

trait PlayerHandlerFactory {
  def playerHandler(actorContext: ActorContext, user: User, messageEventContext: MessageEventContext, parent: ActorRef, serviceHandlersFactory: ServiceHandlersModuleFactory, databaseParameters: HashMap[String, String]): ActorRef
}

class PlayerHandlerFactoryImpl extends PlayerHandlerFactory {
  def playerHandler(actorContext: ActorContext, user: User, messageEventContext: MessageEventContext, parent: ActorRef, serviceHandlersFactory: ServiceHandlersModuleFactory, databaseParameters: HashMap[String, String]): ActorRef = {
    actorContext.actorOf(Props(classOf[PlayerHandler], user, messageEventContext, parent, serviceHandlersFactory, databaseParameters))
  }
}

