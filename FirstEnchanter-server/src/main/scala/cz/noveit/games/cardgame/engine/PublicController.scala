package cz.noveit.games.cardgame.engine

import akka.actor.{Props, ActorLogging, Actor, ActorRef}
import cz.noveit.games.cardgame.engine.handlers.ServiceHandlersModuleFactory
import scala.collection.immutable.HashMap
import cz.noveit.games.cluster.registry.SubscribeActorForModuleName
import cz.noveit.proto.serialization.MessageEvent
import cz.noveit.games.cardgame.engine.handlers.public.{PublicUpdateHandler, PublicServiceHandlersModules}
import cz.noveit.database.DatabaseController

/**
 * Created by Wlsek on 21.3.14.
 */
class PublicController (
                         val dispatcher: ActorRef,
                         val clusterRegistry: ActorRef,
                         avatarLoader: ActorRef,
                         databaseParameters: HashMap[String, String]
                         ) extends Actor with ActorLogging {

  val moduleName = "CardGamePublic"
  clusterRegistry ! SubscribeActorForModuleName(self, moduleName)

  def receive = {
    case e: MessageEvent =>
         e.module match {
           case PublicServiceHandlersModules.publicUpdate =>
              context.actorOf(Props(classOf[PublicUpdateHandler], e.module, context.actorSelection("/user/databaseController"), avatarLoader , databaseParameters)) ! e
         }
    case t =>
      log.error("Unknown message " + t.toString)
  }
}
