package cz.noveit.games.cardgame.engine.arena

/**
 * Created by Wlsek on 7.2.14.
 */

case class Arena (arenaId: String, tag: String, description: String)
