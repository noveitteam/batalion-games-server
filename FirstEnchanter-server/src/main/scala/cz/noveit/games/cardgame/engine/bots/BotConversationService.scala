package cz.noveit.games.cardgame.engine.bots

import akka.actor.{Props, ActorSelection, ActorRef, Actor}
import com.google.code.chatterbotapi.{ChatterBotSession, ChatterBotType, ChatterBotFactory}
import cz.noveit.games.cardgame.WorldServices
import cz.noveit.games.cardgame.engine._
import cz.noveit.games.cluster.dispatch.ClusterServiceEvent

/**
 * Created by arnostkuchar on 05.11.14.
 */


case class BotJoinsConversation(conversationId: String, botName: String)
case class BotLeftConversation(conversationId: String, botName: String)
case class BotJoinedConversation(id: String, name: String, handler: ActorRef)

class BotConversationService extends Actor {
  var botsAndConversations: List[BotJoinedConversation] = List()

  def receive = {
    case BotJoinsConversation(id, name) =>
      if(!botsAndConversations.exists(bc => bc.id == id && bc.name == name)) {
        val factory = new ChatterBotFactory();
        val bot = factory.create(ChatterBotType.CLEVERBOT);
        val botSession = bot.createSession()

        val conversationService = context actorSelection("/user/" +  WorldServices.conversationService)
        botsAndConversations = BotJoinedConversation(id, name, context.actorOf(Props(classOf[BotInConversationHandler], conversationService, name, id, botSession))) :: botsAndConversations
      }

    case BotLeftConversation(id, name) =>
      botsAndConversations.find(bc => bc.id == id && bc.name == name).map(found => {
        context.stop(found.handler)
        botsAndConversations = botsAndConversations.filter(f => f.id == id && f.name == name)
      })

    case ClusterServiceEvent(ConversationReceivedMessage(id, msg), service) =>
      botsAndConversations.filter(f => f.id == id).map(found => {
        if(msg.fromPlayer != found.name && msg.messageType != MessageType.PredefinedMessageType) {
          found.handler ! msg
        }
      })

  }
}

class BotInConversationHandler(conversationService: ActorSelection, myName: String, conversationId: String, botSession: ChatterBotSession) extends Actor {
  def receive = {
    case msg: Message =>
      conversationService ! SendMessageToConversation(
        Message(System.currentTimeMillis(),
          botSession.think(msg.content),
          myName,
          MessageType.NormalMessageType
        ), conversationId
      )
  }
}
