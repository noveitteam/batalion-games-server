package cz.noveit.games.cardgame.engine.bots

import akka.actor._
import cz.noveit.games.cardgame.WorldServices
import cz.noveit.games.cardgame.engine.game.helpers.{CardSwappingRequirements, CardSwapping}
import cz.noveit.games.cardgame.engine.game.session._
import scala.util.Random
import scala.collection.immutable.HashMap
import cz.noveit.games.cardgame.adapters._
import scala.concurrent.duration._
import cz.noveit.games.cardgame.engine.handlers.{CardDatabaseAdapterFound, PlayerInMatchHandlerFound, GetPlayerInMatchHandler, GetCardDatabaseAdapter}
import cz.noveit.games.cardgame.engine.game._
import cz.noveit.games.cardgame.engine.card._
import cz.noveit.games.cardgame.engine.{game, ServiceModuleMessage}
import cz.noveit.games.cardgame.engine.handlers.CardDatabaseAdapterFound
import cz.noveit.games.cardgame.adapters.GetBotFromDatabase
import cz.noveit.database.DatabaseCommand
import cz.noveit.database.DatabaseResult
import cz.noveit.games.cardgame.engine.handlers.GetPlayerInMatchHandler
import cz.noveit.games.cardgame.engine.player.CharmOfElements
import cz.noveit.games.cardgame.engine.card.EvaluateAttackDone
import cz.noveit.games.cardgame.engine.handlers.PlayerInMatchHandlerFound
import cz.noveit.games.cardgame.adapters.BotFromDatabase
import cz.noveit.games.cardgame.engine.card.EvaluateUsageDone

/**
 * Created by Wlsek on 28.5.14.
 */


case class BotPlayerHolder(handler: ActorRef, name: String, avatarId: Int, challengeId: Int, deckId: String)

case class BotSetup(cardSwappingReq: CardSwappingRequirements)

object BotPlayerRelationship extends Enumeration {
  val BotPlayerRelationshipMe, BotPlayerRelationshipFriend, BotPlayerRelationshipEnemy = Value
}

case class CardWithEquipment(card: CardPosition, equipementTag: List[String])

case class BotSeesPlayer(player: String,
                         hand: Array[CardHolder],
                         onTable: Array[CardHolder],
                         graveyard: Array[CardHolder],
                         using: List[AttackerUsesCard],
                         counterUsing: List[DefenderUsesCard],
                         attacking: List[AttackerAttacks],
                         defending: List[DefenderDefends],
                         elements: CharmOfElements,
                         relationship: BotPlayerRelationship.Value,
                         fortuneIsSmiling: Boolean
                          )

case class BotVision(players: List[BotSeesPlayer], myTeamHp: Int = 0, enemyTeamHp: Int = 0, cardsWithEquipment: List[CardWithEquipment], roundsPassed: Int = 1)

case class BotData(playerName: String, botFactory: ActorRef, matchHandler: ActorRef, matchSetup: MatchSetup, vision: BotVision, botSetup: BotSetup) {
  def me = {
    vision.players.find(p => p.relationship == BotPlayerRelationship.BotPlayerRelationshipMe).get
  }
}

class BotPlayer(initialData: BotData) extends Actor with ActorLogging with FSM[State, BotData] with CardSwapping {
  startWith(INITIALIZING_MATCH, initialData)

  val random = new Random()
  var strategist: Option[ActorRef] = None
  val strategy = new SimpleBotStrategy

  val cardSwappingRequirements = initialData.botSetup.cardSwappingReq

  when(INITIALIZING_MATCH) {
    case Event(ServiceModuleMessage(StateAsk(state, ts, timeInState, timeAnchor), module, hsh), data: BotData) => {
      log.debug("[Bot {} responds] > Hello, I am in {} state and going to {}.", initialData.playerName, INITIALIZING_MATCH, state)
      sender ! StateAck(state, data.playerName)
      state match {
        case PLAYER_STARTS_ROLL =>
          goto(state) using (data.copy(matchHandler = sender))
        case other =>
          desynchronized
          goto(state) using (data.copy(matchHandler = sender))
      }
    }
  }

  when(PLAYER_STARTS_ROLL) {
    case Event(ServiceModuleMessage(StateAsk(state, ts, timeInState, timeAnchor), module, hsh), data) => {
      log.debug("[Bot {} responds] > Hello, I am in {} state and going to {}.", initialData.playerName, ATTACKER_TEAM_PLAY, state)
      sender ! StateAck(state, data.playerName)
      state match {
        case ATTACKER_TEAM_PLAY =>
          goto(state)
        case other =>
          desynchronized
          goto(state)
      }
    }

    case Event(ServiceModuleMessage(StateAsk(state, ts, timeInState, timeAnchor), module, hsh), data) => {
      log.debug("[Bot {} responds] > Hello, I am in {} state and going to {}.", initialData.playerName, PLAYER_STARTS_ROLL, state)
      sender ! StateAck(state, data.playerName)
      state match {
        case ATTACKER_TEAM_PLAY =>
          goto(state) using data.copy(vision = data.vision.copy(roundsPassed = data.vision.roundsPassed + 1))
        case other =>
          desynchronized
          goto(state)
      }
    }

    case Event(ServiceModuleMessage(AskPlayerRoll(position), module, hsh), data) => {
      val roll = random.nextInt(6) + 1
      log.debug("[Bot {} responds] > Had to roll, and rolled {}", initialData.playerName, roll)
      sender ! PlayerRoll(data.playerName, roll, position)
      stay()
    }


    case Event(ServiceModuleMessage(PlayerDrawCards(player, cards), module, hsh), data) => {
      if (log.isDebugEnabled) {
        log.debug("[Bot {} responds] > Received information about drawing card {} for: {}", initialData.playerName, cards.map(c => c.card.id), player)
      }

      stay using data.copy(vision = data.vision.copy(players = data.vision.players.find(p => p.player == player).map(p => {
        p.copy(
          hand = p.hand ++ cards.map(c => c.card)
        )
      }).getOrElse(BotSeesPlayer(player, cards.map(c => c.card).toArray, Array(), Array(), List(), List(), List(), List(), CharmOfElements(0, 0, 0, 0), if (player == initialData.playerName) {
        BotPlayerRelationship.BotPlayerRelationshipMe
      } else {
        BotPlayerRelationship.BotPlayerRelationshipEnemy
      }, false)) :: data.vision.players.filterNot(p => p.player == player)))
    }

    case Event(ServiceModuleMessage(PlayerGetsElements(player, elements), module, hsh), data) => {
      log.debug("[Bot {} responds] > Received information about getting elements {} for: {}", initialData.playerName, elements, player)
      stay using data.copy(vision = data.vision.copy(players = data.vision.players.find(p => p.player == player).map(p => {
        p.copy(
          elements = p.elements + elements
        )
      }).getOrElse(BotSeesPlayer(player, Array(), Array(), Array(), List(), List(), List(), List(), elements, if (player == initialData.playerName) {
        BotPlayerRelationship.BotPlayerRelationshipMe
      } else {
        BotPlayerRelationship.BotPlayerRelationshipEnemy
      }, false)) :: data.vision.players.filterNot(p => p.player == player)))
    }

    case Event(ServiceModuleMessage(PlayerRollResult(winning, loosing), module, hsh), data) => {
      stay using data.copy(
        vision = data.vision.copy(
          players = winning.map(w => {
            data.vision.players.find(p => p.player == w.user).map(f => f.copy(
              relationship = {
                if (initialData.playerName == f.player) {
                  BotPlayerRelationship.BotPlayerRelationshipMe
                } else if (winning.contains(initialData.playerName)) {
                  BotPlayerRelationship.BotPlayerRelationshipFriend
                } else {
                  BotPlayerRelationship.BotPlayerRelationshipEnemy
                }
              },
              fortuneIsSmiling = true
            )).getOrElse(
                BotSeesPlayer(
                w.user, Array(), Array(), Array(), List(), List(), List(), List(), CharmOfElements(0, 0, 0, 0), {
                  if (initialData.playerName == w.user) {
                    BotPlayerRelationship.BotPlayerRelationshipMe
                  } else if (winning.contains(initialData.playerName)) {
                    BotPlayerRelationship.BotPlayerRelationshipFriend
                  } else {
                    BotPlayerRelationship.BotPlayerRelationshipEnemy
                  }
                },
                true
                )
              )
          }) :::
            loosing.map(l => {
              data.vision.players.find(p => p.player == l.user).map(f => f.copy(
                relationship = {
                  if (initialData.playerName == f.player) {
                    BotPlayerRelationship.BotPlayerRelationshipMe
                  } else if (loosing.contains(initialData.playerName)) {
                    BotPlayerRelationship.BotPlayerRelationshipFriend
                  } else {
                    BotPlayerRelationship.BotPlayerRelationshipEnemy
                  }
                },
                fortuneIsSmiling = false
              )).getOrElse(
                  BotSeesPlayer(l.user, Array(), Array(), Array(), List(), List(), List(), List(), CharmOfElements(0, 0, 0, 0), {
                    if (initialData.playerName == l.user) {
                      BotPlayerRelationship.BotPlayerRelationshipMe
                    } else if (loosing.contains(initialData.playerName)) {
                      BotPlayerRelationship.BotPlayerRelationshipFriend
                    } else {
                      BotPlayerRelationship.BotPlayerRelationshipEnemy
                    }
                  },
                  false
                  )
                )
            })
        )
      )
    }
  }

  when(FORTUNE_ROLL) {
    case Event(ServiceModuleMessage(StateAsk(state, ts, timeInState, timeAnchor), module, hsh), data) => {
      log.debug("[Bot {} responds] > Hello, I am in {} state and going to {}.", initialData.playerName, FORTUNE_ROLL, state)
      sender ! StateAck(state, data.playerName)
      state match {
        case ATTACKER_TEAM_PLAY =>
          goto(state)
        case other =>
          desynchronized
          goto(state)
      }
    }

    case Event(ServiceModuleMessage(AskPlayerRoll(position), module, hsh), data) => {
      val roll = random.nextInt(6) + 1
      log.debug("[Bot {} responds] > Had to roll, and rolled {}", initialData.playerName, roll)
      sender ! PlayerRoll(data.playerName, roll, position)
      stay()
    }

    case Event(ServiceModuleMessage(PlayerDrawCards(player, cards), module, hsh), data) => {
      if (log.isDebugEnabled) {
        log.debug("[Bot {} responds] > Received information about drawing card {} for: {}", initialData.playerName, cards.map(c => c.card.id), player)
      }

      stay using data.copy(vision = data.vision.copy(players = data.vision.players.find(p => p.player == player).map(p => {
        p.copy(
          hand = p.hand ++ cards.map(c => c.card)
        )
      }).toList ::: data.vision.players.filterNot(p => p.player == player)))

    }

    case Event(ServiceModuleMessage(PlayerGetsElements(player, elements), module, hsh), data) => {
      log.debug("[Bot {} responds] > Received information about getting elements {} for: {}", initialData.playerName, elements, player)
      stay using data.copy(vision = data.vision.copy(players = data.vision.players.find(p => p.player == player).map(p => {
        p.copy(
          elements = p.elements + elements
        )
      }).toList ::: data.vision.players.filterNot(p => p.player == player)))
    }

    case Event(ServiceModuleMessage(PlayerRollResult(winning, loosing), module, hsh), data) => {
      stay using data.copy(
        vision = data.vision.copy(
          players = data.vision.players.map(p => p.copy(
            fortuneIsSmiling = winning.map(w => w.user).contains(p.player)
          ))
        )
      )
    }
  }

  when(ATTACKER_TEAM_PLAY) {
    case Event(ServiceModuleMessage(StateAsk(state, ts, timeInState, timeAnchor), module, hsh), data) => {
      log.debug("[Bot {} responds] > Hello, I am in {} state and going to {}.", initialData.playerName, ATTACKER_TEAM_PLAY, state)
      sender ! StateAck(state, data.playerName)
      state match {
        case DEFENDER_TEAM_COUNTER_PLAY =>
          goto(state)
        case other =>
          desynchronized
          goto(state)
      }
    }

    case Event(ServiceModuleMessage(AskAttackerToUseCard(possibleUsages), module, hsh), data) => {
      log.debug("[Bot {} responds] > Had to use cards like attacker", initialData.playerName)
      val me = data.me
      if (possibleUsages.size == 0 && me.hand.size == data.matchSetup.maxCardsInHand) {

        if (howManyCardSwappingPossible_?(me.hand.size, me.elements) > 1) {

          strategist.map(s => {
            context.stop(s)
          })

          strategist = Some(
            context.actorOf(Props(classOf[StrategistWorkerForCardSwapping],
              self, initialData.playerName,
              me.hand.zipWithIndex.map(zipped => CardSwap(zipped._1, InHandCardPosition(zipped._2, me.player))).toList,
              data, strategy))
          )
        }
      } else {
        strategist.map(s => {
          context.stop(s)
        })

        strategist = Some(
          context.actorOf(Props(classOf[StrategistWorker], self, initialData.playerName, possibleUsages, data, strategy))
        )
      }
      stay()
    }

    case Event(ServiceModuleMessage(AttackerUsedCardAccepted(position, target, neutralDistribution, possibleUsages, card, changes), module, hsh), data) => {
      log.debug("[Bot {} responds] > received actualized usages.", initialData.playerName)

      strategist.map(s => {
        context.stop(s)
      })

      strategist = Some(
        context.actorOf(Props(classOf[StrategistWorker], self, initialData.playerName, possibleUsages, data, strategy))
      )

      stay using reactToChanges(changes, data).copy(vision = data.vision.copy(players = data.vision.players.find(p => p.player == position.player).map(p => {
        p.copy(
          using = AttackerUsesCard(position, target, card) :: p.using
        )
      }).toList ::: data.vision.players.filterNot(p => p.player == position.player)))
    }

    case Event(ActionsChosen(action), data) => {
      strategist.map(str => {
        if (str == sender) {
          log.debug("[Bot {} responds] > Received possible actions for usage as attacker.", initialData.playerName)
          if (action.isDefined) {
            action.get match {
              case a: AttackerUsesCard =>
                data.matchHandler ! a
            }
          } else {
            data.matchHandler ! AttackerEndsTurn(data.playerName)
          }
        } else {
          log.debug("[Bot {} responds] > Received possible actions for usage as attacker, but from unknown strategist, ignoring them.", initialData.playerName)
        }
      })
      stay()
    }

    case Event(ServiceModuleMessage(AskDefenderToWait, module, hsh), data) => {
      log.debug("[Bot {} responds] > Had to wait like defender", initialData.playerName)
      stay()
    }

    case Event(ServiceModuleMessage(AttackerUsesCard(position, target, card, neutralDistribution, changes), module, hsh), data) => {
      log.debug("[Bot {} responds] > Attacker used card.", initialData.playerName)
      stay using reactToChanges(changes, data).copy(vision = data.vision.copy(players = data.vision.players.find(p => p.player == position.player).map(p => {
        p.copy(
          using = AttackerUsesCard(position, target, card, neutralDistribution) :: p.using
        )
      }).toList ::: data.vision.players.filterNot(p => p.player == position.player)))
      stay()
    }

    case Event(ServiceModuleMessage(AttackerEndsTurn(player), module, hsh), data) => {
      log.debug("[Bot {} responds] > Attacker ended turn.", initialData.playerName)
      stay()
    }
  }

  when(DEFENDER_TEAM_COUNTER_PLAY) {
    case Event(ServiceModuleMessage(StateAsk(state, ts, timeInState, timeAnchor), module, hsh), data) => {
      log.debug("[Bot responds] > Hello, I am in {} state and going to {}.", DEFENDER_TEAM_COUNTER_PLAY, state)
      sender ! StateAck(state, data.playerName)
      state match {
        case EVALUATION_AND_ANIMATION =>
          goto(state)
        case other =>
          desynchronized
          goto(state)
      }
    }

    case Event(ServiceModuleMessage(AskDefenderToUseCard(possibleUsages), module, hsh), data) => {
      log.debug("[Bot {} responds] > Had to use cards like defender", initialData.playerName)
      strategist.map(s => {
        context.stop(s)
      })

      strategist = Some(
        context.actorOf(Props(classOf[StrategistWorker], self, initialData.playerName, possibleUsages, data, strategy))
      )

      stay()
    }

    case Event(ServiceModuleMessage(DefenderUsedCardAccepted(position, target, neutralDistribution, possibleUsages, card, changes), module, hsh), data) => {
      log.debug("[Bot {} responds] > received actualized usages.", initialData.playerName)

      strategist.map(s => {
        context.stop(s)
      })

      strategist = Some(
        context.actorOf(Props(classOf[StrategistWorker], self, initialData.playerName, possibleUsages, data, strategy))
      )

      stay using reactToChanges(changes, data).copy(vision = data.vision.copy(players = data.vision.players.find(p => p.player == position.player).map(p => {
        p.copy(
          counterUsing = DefenderUsesCard(position, target, card) :: p.counterUsing
        )
      }).toList ::: data.vision.players.filterNot(p => p.player == position.player)))
    }

    case Event(ActionsChosen(action), data) => {
      strategist.map(str => {
        if (str == sender) {
          log.debug("[Bot {} responds] > Received possible actions for usage as defender.", initialData.playerName)
          if (action.isDefined) {
            action.get match {
              case a: DefenderUsesCard =>
                data.matchHandler ! a
            }
            data.matchHandler ! (data.playerName)
          } else {
            data.matchHandler ! DefenderEndsTurn(data.playerName)
          }
        } else {
          log.debug("[Bot {} responds] > Received possible actions for usage as defender, but from unknown strategist, ignoring them.", initialData.playerName)
        }
      })
      stay()
    }


    case Event(ServiceModuleMessage(AskAttackerToWait, module, hsh), data) => {
      log.debug("[Bot {} responds] > Had to wait like attacker", initialData.playerName)
      stay()
    }

    case Event(ServiceModuleMessage(DefenderUsesCard(position, target, card, neutralDistribution, changes), module, hsh), data) => {
      log.debug("[Bot {} responds] > Defender used card.", initialData.playerName)
      stay using reactToChanges(changes, data).copy(vision = data.vision.copy(players = data.vision.players.find(p => p.player == position.player).map(p => {
        p.copy(
          counterUsing = DefenderUsesCard(position, target, card, neutralDistribution) :: p.counterUsing
        )
      }).toList ::: data.vision.players.filterNot(p => p.player == position.player)))
      stay()
    }

    case Event(ServiceModuleMessage(DefenderEndsTurn(player), module, hsh), data) => {
      log.debug("[Bot {} responds] > Defender ends turn.", initialData.playerName)
      stay()
    }

  }

  when(ATTACKER_TEAM_ATTACK) {
    case Event(ServiceModuleMessage(StateAsk(state, ts, timeInState, timeAnchor), module, hsh), data) => {
      log.debug("[Bot {} responds] > Hello, I am in {} state and going to {}.", initialData.playerName, ATTACKER_TEAM_ATTACK, state)
      sender ! StateAck(state, data.playerName)
      state match {
        case DEFENDER_TEAM_DEFENDS =>
          goto(state)
        case other =>
          desynchronized
          goto(state)
      }
    }

    case Event(ServiceModuleMessage(AskAttackerToAttack(possibleAttacks), module, hsh), data) => {
      log.debug("[Bot {} responds] > Had to attacks with cards", initialData.playerName)
      strategist.map(s => {
        context.stop(s)
      })

      strategist = Some(
        context.actorOf(Props(classOf[StrategistWorker], self, initialData.playerName, possibleAttacks, data, strategy))
      )

      stay()
    }


    case Event(ServiceModuleMessage(AttackAccepted(position, possibleUsages, card, changes), module, hsh), data) => {
      log.debug("[Bot {} responds] > received actualized usages.", initialData.playerName)

      strategist.map(s => {
        context.stop(s)
      })

      strategist = Some(
        context.actorOf(Props(classOf[StrategistWorker], self, initialData.playerName, possibleUsages, data, strategy))
      )

      stay using reactToChanges(changes, data).copy(vision = data.vision.copy(players = data.vision.players.find(p => p.player == position.player).map(p => {
        p.copy(
          attacking = AttackerAttacks(position, card) :: p.attacking
        )
      }).toList ::: data.vision.players.filterNot(p => p.player == position.player)))
    }

    case Event(ActionsChosen(action), data) => {
      strategist.map(str => {
        if (str == sender) {
          log.debug("[Bot {} responds] > Received possible actions for attack.", initialData.playerName)
          if (action.isDefined) {
            action.get match {
              case a: AttackerAttacks =>
                data.matchHandler ! a
            }
          } else {
            data.matchHandler ! AttackerEndsTurn(data.playerName)
          }
        } else {
          log.debug("[Bot {} responds] > Received possible actions for attack, but from unknown strategist, ignoring them.", initialData.playerName)
        }
      })
      stay()
    }


    case Event(ServiceModuleMessage(AskDefenderToWaitForAttack, module, hsh), data) => {
      log.debug("[Bot {} responds] > have to wait for attacker.", initialData.playerName)
      stay()
    }

    case Event(ServiceModuleMessage(AttackerEndsTurn(player), module, hsh), data) => {
      log.debug("[Bot {} responds] > attacker ends turn.", initialData.playerName)
      stay()
    }

    case Event(ServiceModuleMessage(AttackerAttacks(position, card, changes), module, hsh), data) => {
      stay using reactToChanges(changes, data).copy(vision = data.vision.copy(players = data.vision.players.find(p => p.player == position.player).map(p => {
        p.copy(
          attacking = AttackerAttacks(position, card) :: p.attacking
        )
      }).toList ::: data.vision.players.filterNot(p => p.player == position.player)))
      log.debug("[Bot {} responds] > attacker attacks.", initialData.playerName)
      stay()
    }
  }

  when(DEFENDER_TEAM_DEFENDS) {
    case Event(ServiceModuleMessage(StateAsk(state, ts, timeInState, timeAnchor), module, hsh), data) => {
      log.debug("[Bot responds] > Hello, I am in {} state and going to {}.", DEFENDER_TEAM_DEFENDS, state)
      sender ! StateAck(state, data.playerName)
      state match {
        case EVALUATION_AND_ANIMATION =>
          goto(state)
        case other =>
          desynchronized
          goto(state)
      }
    }

    case Event(ServiceModuleMessage(AskDefenderToDefend(possibleDefends), module, hsh), data) => {
      log.debug("[Bot {} responds] > Had to defends with cards", initialData.playerName)
      strategist.map(s => {
        context.stop(s)
      })

      strategist = Some(
        context.actorOf(Props(classOf[StrategistWorker], self, initialData.playerName, possibleDefends, data, strategy))
      )

      stay()
    }

    case Event(ServiceModuleMessage(DefendingAccepted(position, target, possibleDefendings, defenderCard, neutralDistribution, changes), module, hsh), data) => {
      log.debug("[Bot {} responds] > received actualized defends.", initialData.playerName)

      strategist.map(s => {
        context.stop(s)
      })

      strategist = Some(
        context.actorOf(Props(classOf[StrategistWorker], self, initialData.playerName, possibleDefendings, data, strategy))
      )

      stay using reactToChanges(changes, data).copy(vision = data.vision.copy(players = data.vision.players.find(p => p.player == position.player).map(p => {
        p.copy(
          defending = DefenderDefends(position, target, defenderCard) :: p.defending
        )
      }).toList ::: data.vision.players.filterNot(p => p.player == position.player)))
    }

    case Event(ActionsChosen(action), data) => {
      strategist.map(str => {
        if (str == sender) {
          log.debug("[Bot {} responds] > Received possible actions for defending as defender.", initialData.playerName)
          if (action.isDefined) {
            action.get match {
              case a: DefenderDefends =>
                data.matchHandler ! a
            }
          } else {
            data.matchHandler ! DefenderEndsTurn(data.playerName)
          }
        } else {
          log.debug("[Bot {} responds] > Received possible actions for defending as defender, but from unknown strategist, ignoring them.", initialData.playerName)
        }
      })
      stay()
    }


    case Event(ServiceModuleMessage(AskAttackerToWaitForDefend, module, hsh), data) => {
      log.debug("[Bot {} responds] > Received AskAttackerToWaitForDefend, waiting then.", initialData.playerName)
      stay()
    }

    case Event(ServiceModuleMessage(DefenderDefends(withPosition, againstPosition, defenderCard, neutralDistribution, changes), module, hsh), data) => {
      log.debug("[Bot {} responds] > Defender defends with {} against {}.", initialData.playerName, withPosition, againstPosition)
      stay using reactToChanges(changes, data).copy(vision = data.vision.copy(players = data.vision.players.find(p => p.player == withPosition.player).map(p => {
        p.copy(
          defending = DefenderDefends(withPosition, againstPosition, defenderCard) :: p.defending
        )
      }).toList ::: data.vision.players.filterNot(p => p.player == withPosition.player)))
      stay()
    }

    case Event(ServiceModuleMessage(DefenderEndsTurn(player), module, hsh), data) => {
      log.debug("[Bot {} responds] > Defender {} ends turn.", initialData.playerName, player)
      stay()
    }
  }

  when(EVALUATION_AND_ANIMATION) {
    case Event(ServiceModuleMessage(StateAsk(state, ts, timeInState, timeAnchor), module, hsh), data) => {
      log.debug("[Bot responds] > Hello, I am in {} state and going to {}.", EVALUATION_AND_ANIMATION, state)
      sender ! StateAck(state, data.playerName)
      state match {
        case (FORTUNE_ROLL | ATTACKER_TEAM_ATTACK) =>
          goto(state)
        case other =>
          desynchronized
          goto(state)
      }
    }

    case Event(ServiceModuleMessage(EvaluateUsageDone(ts, changes), module, hsh), data) => {
      stay using (reactToChanges(changes, data))
    }

    case Event(ServiceModuleMessage(EvaluateAttackDone(ts, changes), module, hsh), data) => {
      sender ! AnimationDone(data.playerName)
      val newData = reactToChanges(changes, data)
      stay using (newData.copy(
        vision = newData.vision.copy(
          players = newData.vision.players.map(p => {
            p.copy(
              using = List(),
              attacking = List(),
              defending = List(),
              counterUsing = List()
            )
          })
        )
      ))
    }
  }


  def reactToChanges(changes: List[TableStateChange], data: BotData) = {
    log.debug("[Bot {}] Received {} changes.", initialData.playerName, changes.size)
    var newData = data
    changes.map {
      case ch: StayOnTable =>
        try {
        newData = newData.copy(
          vision = newData.vision.copy(
            players = data.vision.players.find(p => p.player == ch.position.player).map(p => {
              p.relationship match {
                case (BotPlayerRelationship.BotPlayerRelationshipFriend | BotPlayerRelationship.BotPlayerRelationshipMe) =>

                    p.copy(
                      onTable = p.hand(ch.position.positionId) +: p.onTable,
                      hand = p.hand.filterNot(c => c == p.hand(ch.position.positionId)),
                      using = p.using.filterNot(c => c.onPosition =? ch.position),
                      counterUsing = p.counterUsing.filterNot(c => c.onPosition =? ch.position)
                    )

                case BotPlayerRelationship.BotPlayerRelationshipEnemy =>

                    p.copy(
                      onTable = ch.card +: p.onTable,
                      hand = p.hand.filterNot(c => c == p.hand(ch.position.positionId)),
                      using = p.using.filterNot(c => c.onPosition =? ch.position),
                      counterUsing = p.counterUsing.filterNot(c => c.onPosition =? ch.position)
                    )

              }
            }).toList ::: data.vision.players.filterNot(p => p.player == ch.position.player)
          )
        )
            }catch {
        case t: Throwable =>
          log.error("Error {} while reacting to change {} and in state {}", t.getStackTraceString, ch.toXml, data.toString)
      }

      case ch: PlayerPoolReceivedDamage =>
        try {
          if (ch.players contains (newData.playerName)) {
            newData = newData.copy(
              vision = newData.vision.copy(
                myTeamHp = newData.vision.myTeamHp - ch.value
              ))
          } else {
            newData = newData.copy(
              vision = newData.vision.copy(
                enemyTeamHp = newData.vision.enemyTeamHp - ch.value
              ))
          }

        } catch {
          case t: Throwable =>
            log.error("Error {} while reacting to change {} and in state {}", t.getStackTraceString, ch.toXml, data.toString)
        }

      case ch: PlayerPoolReceivedHeal =>
        try {
          if (ch.players contains (data.playerName)) {
            newData = data.copy(
              vision = data.vision.copy(
                myTeamHp = data.vision.myTeamHp + ch.value
              ))
          } else {
            newData = data.copy(
              vision = data.vision.copy(
                enemyTeamHp = data.vision.enemyTeamHp + ch.value
              ))
          }
        } catch {
          case t: Throwable =>
            log.error("Error {} while reacting to change {} and in state {}", t.getStackTraceString, ch.toXml, data.toString)
        }

      case ch: PlayersLost =>
        try {
          log.debug("Game ends, ending myself.")
          context.stop(self)
        } catch {
          case t: Throwable =>
            log.error("Error {} while reacting to change {} and in state {}", t.getStackTraceString, ch.toXml, data.toString)
        }

      case ch: CardStripEquipment =>
        try {
          newData = newData.copy(
            vision = newData.vision.copy(
              cardsWithEquipment = newData.vision.cardsWithEquipment.filterNot(cwe => {
                var contains = false
                ch.equipmentTags.map(tag => {
                  if(cwe.equipementTag contains(tag)) {
                    contains = true
                  }
                })
                contains
              })
            )
          )
        } catch {
          case t: Throwable =>
            log.error("Error {} while reacting to change {} and in state {}", t.getStackTraceString, ch.toXml, data.toString)
        }

      case ch: AddedEquipmentToCard =>
        try {
          newData = newData.copy(
            vision = newData.vision.copy(
              cardsWithEquipment = CardWithEquipment(ch.card, ch.equipmentTags) :: newData.vision.cardsWithEquipment
            )
          )
        } catch {
          case t: Throwable =>
            log.error("Error {} while reacting to change {} and in state {}", t.getStackTraceString, ch.toXml, data.toString)
        }

      case ch: CardChangeAttack =>
        try {
          newData = newData.copy(
            vision = newData.vision.copy(
              players = data.vision.players.find(p => p.player == ch.card.player).map(p => {
                ch.card match {
                  case pos: InHandCardPosition => p.copy(
                    hand = p.hand(pos.positionId).copy(
                      attack = p.hand(pos.positionId).attack + ch.changeBy
                    ) +: p.hand.filterNot(f => f == p.hand(pos.positionId))
                  )

                  case pos: OnTableCardPosition => p.copy(
                    hand = p.onTable(pos.positionId).copy(
                      attack = p.onTable(pos.positionId).attack + ch.changeBy
                    ) +: p.onTable.filterNot(f => f == p.onTable(pos.positionId))
                  )
                }
              }).toList ::: data.vision.players.filterNot(p => p.player == ch.card.player)
            )
          )
        } catch {
          case t: Throwable =>
            log.error("Error {} while reacting to change {} and in state {}", t.getStackTraceString, ch.toXml, data.toString)
        }

        case ch: CardChangeDefense =>
        try {
          newData = newData.copy(
            vision = newData.vision.copy(
              players = data.vision.players.find(p => p.player == ch.card.player).map(p => {
                ch.card match {
                  case pos: InHandCardPosition => p.copy(
                    hand = p.hand(pos.positionId).copy(
                      defense = p.hand(pos.positionId).defense + ch.changeBy
                    ) +: p.hand.filterNot(f => f == p.hand(pos.positionId))
                  )

                  case pos: OnTableCardPosition => p.copy(
                    onTable = p.onTable(pos.positionId).copy(
                      defense = p.onTable(pos.positionId).defense + ch.changeBy
                    ) +: p.onTable.filterNot(f => f == p.onTable(pos.positionId))
                  )
                }
              }).toList ::: data.vision.players.filterNot(p => p.player == ch.card.player)
            )
          )
        } catch {
          case t: Throwable =>
            log.error("Error {} while reacting to change {} and in state {}", t.getStackTraceString, ch.toXml, data.toString)
        }

      case ch: CardBuffed =>

      case ch: CardConsumed =>
        try {
          newData = newData.copy(
            vision = newData.vision.copy(
              players = data.vision.players.find(p => p.player == ch.card.player).map(p => {
                p.relationship match {
                  case (BotPlayerRelationship.BotPlayerRelationshipFriend | BotPlayerRelationship.BotPlayerRelationshipMe) =>
                    ch.card match {
                      case pos: InHandCardPosition => p.copy(
                        hand = p.hand.filterNot(f => f == p.hand(pos.positionId)),
                        graveyard = p.hand(pos.positionId) +: p.graveyard,
                        attacking = p.attacking.filterNot(c => c.withPosition =? ch.card),
                        using = p.using.filterNot(c => c.onPosition =? ch.card),
                        counterUsing = p.counterUsing.filterNot(c => c.onPosition =? ch.card),
                        defending = p.defending.filterNot(c => c.withPosition =? ch.card)
                      )
                      case pos: OnTableCardPosition => p.copy(
                        onTable = p.onTable.filterNot(f => f == p.onTable(pos.positionId)),
                        graveyard = p.onTable(pos.positionId) +: p.graveyard,
                        attacking = p.attacking.filterNot(c => c.withPosition =? ch.card),
                        using = p.using.filterNot(c => c.onPosition =? ch.card),
                        counterUsing = p.counterUsing.filterNot(c => c.onPosition =? ch.card),
                        defending = p.defending.filterNot(c => c.withPosition =? ch.card)
                      )
                    }

                  case BotPlayerRelationship.BotPlayerRelationshipEnemy =>
                    ch.card match {
                      case pos: InHandCardPosition => p.copy(
                        graveyard = ch.cardHolder +: p.graveyard,
                        attacking = p.attacking.filterNot(c => c.withPosition =? ch.card),
                        using = p.using.filterNot(c => c.onPosition =? ch.card),
                        counterUsing = p.counterUsing.filterNot(c => c.onPosition =? ch.card),
                        defending = p.defending.filterNot(c => c.withPosition =? ch.card)
                      )
                      case pos: OnTableCardPosition => p.copy(
                        graveyard = ch.cardHolder +: p.graveyard,
                        attacking = p.attacking.filterNot(c => c.withPosition =? ch.card),
                        using = p.using.filterNot(c => c.onPosition =? ch.card),
                        counterUsing = p.counterUsing.filterNot(c => c.onPosition =? ch.card),
                        defending = p.defending.filterNot(c => c.withPosition =? ch.card)
                      )
                    }
                }


              }).toList ::: data.vision.players.filterNot(p => p.player == ch.card.player)

            )
          )

        } catch {
          case t: Throwable =>
            log.error("Error {} while reacting to change {} and in state {}", t.getStackTraceString, ch.toXml, data.toString)
        }

      case ch: CardDestroyerBy =>
        try {
          newData = newData.copy(
            vision = newData.vision.copy(
              players = data.vision.players.find(p => p.player == ch.defender.player).map(p => {
                ch.defender match {
                  case pos: InHandCardPosition => p.copy(
                    hand = p.hand.filterNot(f => f == p.hand(pos.positionId)),
                    graveyard = p.hand(pos.positionId) +: p.graveyard,
                    attacking = p.attacking.filterNot(c => c.withPosition =? ch.defender),
                    using = p.using.filterNot(c => c.onPosition =? ch.defender),
                    counterUsing = p.counterUsing.filterNot(c => c.onPosition =? ch.defender),
                    defending = p.defending.filterNot(c => c.withPosition =? ch.defender)
                  )
                  case pos: OnTableCardPosition => p.copy(
                    onTable = p.onTable.filterNot(f => f == p.onTable(pos.positionId)),
                    graveyard = p.onTable(pos.positionId) +: p.graveyard,
                    attacking = p.attacking.filterNot(c => c.withPosition =? ch.defender),
                    using = p.using.filterNot(c => c.onPosition =? ch.defender),
                    counterUsing = p.counterUsing.filterNot(c => c.onPosition =? ch.defender),
                    defending = p.defending.filterNot(c => c.withPosition =? ch.defender)
                  )
                }
              }).toList :::
                data.vision.players.find(p => p.player == ch.attacker.player).map(p => {
                  p.copy(
                    attacking = p.attacking.filterNot(c => c.withPosition =? ch.attacker),
                    using = p.using.filterNot(c => c.onPosition =? ch.attacker),
                    counterUsing = p.counterUsing.filterNot(c => c.onPosition =? ch.attacker),
                    defending = p.defending.filterNot(c => c.withPosition =? ch.attacker)
                  )
                }).toList ::: data.vision.players.filterNot(p => p.player == ch.attacker.player || p.player == ch.defender.player)
            )
          )

        } catch {
          case t: Throwable =>
            log.error("Error {} while reacting to change {} and in state {}", t.getStackTraceString, ch.toXml, data.toString)
        }
      case ch: CardDefendendAgainst =>
        try {
          newData = newData.copy(
            vision = newData.vision.copy(
              players = data.vision.players.find(p => p.player == ch.defender.player).map(p => {
                p.copy(
                  defending = p.defending.filterNot(d => d.withPosition =? (ch.defender))
                )
              }).toList :::
                data.vision.players.find(p => p.player == ch.attacker.player).map(p => {
                  p.copy(
                    attacking = p.attacking.filterNot(a => a.withPosition =? (ch.attacker))
                  )
                }).toList ::: data.vision.players.filterNot(p => p.player == ch.defender.player || p.player == ch.attacker.player)
            )
          )
        } catch {
          case t: Throwable =>
            log.error("Error {} while reacting to change {} and in state {}", t.getStackTraceString, ch.toXml, data.toString)
        }


      case ch: CardAttackCancel =>
        try {
          newData = newData.copy(
            vision = newData.vision.copy(
              players = data.vision.players.find(p => p.player == ch.canceledAttack.player).map(p => {
                p.copy(
                  attacking = p.attacking.filterNot(a => a.withPosition =? (ch.canceledAttack))
                )
              }).toList ::: data.vision.players.filterNot(p => p.player == ch.canceledAttack.player)
            )
          )

        } catch {
          case t: Throwable =>
            log.error("Error {} while reacting to change {} and in state {}", t.getStackTraceString, ch.toXml, data.toString)
        }

      case ch: CardGenerated =>
        try {
          newData = newData.copy(
            vision = newData.vision.copy(
              players = data.vision.players.find(p => p.player == ch.cardPosition.player).map(p => {
                ch.cardPosition match {
                  case pos: InHandCardPosition => p.copy(
                    hand = ch.cardHolder +: p.hand
                  )
                  case pos: OnTableCardPosition => p.copy(
                    onTable = ch.cardHolder +: p.onTable
                  )
                }
              }).toList ::: data.vision.players.filterNot(p => p.player == ch.cardPosition.player)
            )
          )
        } catch {
          case t: Throwable =>
            log.error("Error {} while reacting to change {} and in state {}", t.getStackTraceString, ch.toXml, data.toString)
        }

      case ch: ElementsGenerated =>
        try {
          newData = newData.copy(
            vision = newData.vision.copy(
              players = data.vision.players.find(p => p.player == ch.player).map(p => {
                p.copy(
                  elements = ch.charm + p.elements
                )
              }).toList ::: data.vision.players.filterNot(p => p.player == ch.player)
            )
          )
        } catch {
          case t: Throwable =>
            log.error("Error {} while reacting to change {} and in state {}", t.getStackTraceString, ch.toXml, data.toString)
        }

      case ch: ElementsAbsorbed =>
        try {
          newData = newData.copy(
            vision = newData.vision.copy(
              players = data.vision.players.find(p => p.player == ch.player).map(p => {
                p.copy(
                  elements = ch.charm - p.elements
                )
              }).toList ::: data.vision.players.filterNot(p => p.player == ch.player)
            )
          )
        } catch {
          case t: Throwable =>
            log.error("Error {} while reacting to change {} and in state {}", t.getStackTraceString, ch.toXml, data.toString)
        }

      case ch: ElementsTransformed =>
        try {
          newData = newData.copy(
            vision = newData.vision.copy(
              players = data.vision.players.find(p => p.player == ch.from).map(p => {
                p.copy(
                  elements = ch.charm - p.elements
                )
              }).toList ::: data.vision.players.find(p => p.player == ch.to).map(p => {
                p.copy(
                  elements = ch.charm + p.elements
                )
              }).toList ::: data.vision.players.filterNot(p => p.player == ch.from || p.player == ch.to)
            )
          )
        } catch {
          case t: Throwable =>
            log.error("Error {} while reacting to change {} and in state {}", t.getStackTraceString, ch.toXml, data.toString)
        }
    }

    sender ! AnimationDone(data.playerName)
    newData
  }

  def desynchronized = {

  }

  whenUnhandled {
    case Event(msg: ServiceModuleMessage, data) =>
      msg.message match {
        case GetCardDatabaseAdapter =>
          initialData.botFactory.tell(msg.message, sender)

        case GetPlayerInMatchHandler(mh) =>
          sender ! PlayerInMatchHandlerFound(self)

        case CannotComposePlayer(player) =>
          log.info("Killing bot {} because cannot start the match.", initialData.playerName)
      }
      stay

    case Event(e, s) =>
      log.warning("received unhandled request {} in state {}/{}", e, stateName, s)
      stay
  }

  onTransition {
    case x -> y =>
      if (log.isDebugEnabled) {
        log.debug("Bot {} go from {} to {} with data {}.", initialData.playerName, x, y, nextStateData)
      }
  }
}


case class GetBot(rating: Int)

class BotPlayerFactory(databaseParams: HashMap[String, String], matchSetup: MatchSetup, cardDatabaseProps: Props) extends Actor with ActorLogging {

  val databaseHandler = context.actorOf(
    Props(classOf[BotDatabaseAdapter],
      context.actorSelection("/user/databaseController"),
      databaseParams("databaseName"),
      databaseParams("botDetailsCollection"),
      databaseParams("botStatisticCollection"),
      databaseParams("decksCollection")
    ))

  def receive = {
    case GetBot(rating) =>
      val actor = context actorOf (Props(classOf[BotProxy], sender, self, context, matchSetup))
      databaseHandler.tell(DatabaseCommand(GetBotFromDatabase(rating)), actor)

    case GetCardDatabaseAdapter =>
      sender ! CardDatabaseAdapterFound(context actorOf cardDatabaseProps)

      /*
      sender ! CardDatabaseAdapterFound(context actorOf Props(classOf[CardDatabaseAdapter],
        context.actorSelection("/user/databaseController"),
        databaseParams("databaseName"),
        databaseParams("cardsCollection"),
        databaseParams("decksCollection"),
        databaseParams("deckIconVersionCollection"),
        databaseParams("cardSkinVersionCollection"),
        databaseParams("userHasCardsCollection"),
        databaseParams("userUnlockedCollection"),
        databaseParams("userCollection")
      ))
*/
  }

}

class BotProxy(replyTo: ActorRef, factory: ActorRef, parentContext: ActorContext, matchSetup: MatchSetup) extends Actor with ActorLogging {
  context.setReceiveTimeout(15 seconds)

  def receive = {
    case DatabaseResult(result, hash) => {
      result match {
        case BotFromDatabase(name, avatarId, challengeId, deckId) =>
          val actor = parentContext.actorOf(Props(classOf[BotPlayer], BotData(name, factory, replyTo, matchSetup, BotVision(List(), matchSetup.teamHp, matchSetup.teamHp, List()), BotSetup(matchSetup.cardSwappingReq))))

          replyTo ! BotPlayerHolder(actor, name, avatarId, challengeId, deckId)
          context.stop(self)
      }
    }

    case ReceiveTimeout =>
      context.stop(self)

    case t =>
      log.error("Unknown message: " + t.toString)
      context.stop(self)
  }
}
