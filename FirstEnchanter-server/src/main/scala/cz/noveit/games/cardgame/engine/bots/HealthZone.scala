package cz.noveit.games.cardgame.engine.bots

/**
 * Created by Wlsek on 22.7.14.
 */
object HealthZone extends Enumeration {
   val GreenHealthZone, YellowHealthZone, RedHealthZone = Value
}
