package cz.noveit.games.cardgame.engine.bots

import cz.bataliongames.firstenchanter.cards._
import cz.noveit.games.cardgame.engine.card.{CardIsTarget, PlayerIsTarget}
import cz.noveit.games.cardgame.engine.game.session.{DefenderUsesCard, DefenderDefends, AttackerUsesCard, AttackerAttacks}

/**
 * Created by Wlsek on 22.7.14.
 */
class SimpleBotStrategy extends Strategy {
  val cardPrioritization = HashMapWithSequence(
    ((action: BotAction, botData: BotData) => {
      action match {
        case a: AttackerUsesCard =>
          ActionWithEvaluation(a, a.card.price.total.toInt, 10)

        case a: AttackerAttacks =>
          ActionWithEvaluation(a, 0, 10 + a.card.attack)

        case a: DefenderDefends =>
          ActionWithEvaluation(a, 0, 10 + a.defenderCard.defense)

        case a: CardSwap =>
          ActionWithEvaluation(a, 0, 0)
      }
    }) -> (>>[FireArcher] :: >>[WaterArcher] :: >>[EarthArcher] :: >>[AirArcher] :: Nil) ::
      ((action: BotAction, botData: BotData) => {
        action match {
          case a: AttackerUsesCard =>
            ActionWithEvaluation(a, a.card.price.total.toInt, 10)

          case a: AttackerAttacks =>
            ActionWithEvaluation(a, 0, 10 + a.card.attack)

          case a: DefenderDefends =>
            ActionWithEvaluation(a, 0, 10 + a.defenderCard.defense)

          case a: CardSwap =>
            ActionWithEvaluation(a, 0, 0)
        }
      }) -> (>>[FireSoldier] :: >>[WaterSoldier] :: >>[EarthSoldier] :: >>[AirSoldier] :: Nil) ::
      ((action: BotAction, botData: BotData) => {
        action match {
          case a: AttackerUsesCard =>
            ActionWithEvaluation(a, a.card.price.total.toInt, 10)

          case a: AttackerAttacks =>
            ActionWithEvaluation(a, 0, 10 + a.card.attack)

          case a: DefenderDefends =>
            ActionWithEvaluation(a, 0, 10 + a.defenderCard.defense)

          case a: CardSwap =>
            ActionWithEvaluation(a, 0, 0)
        }
      }) -> (>>[FireMage] :: >>[WaterMage] :: >>[EarthMage] :: >>[AirMage] :: Nil) ::
      ((action: BotAction, botData: BotData) => {
        action match {
          case a: AttackerUsesCard =>
            ActionWithEvaluation(a, a.card.price.total.toInt, 10)

          case a: AttackerAttacks =>
            ActionWithEvaluation(a, 0, 10 + a.card.attack)

          case a: DefenderDefends =>
            ActionWithEvaluation(a, 0, 10 + a.defenderCard.defense)

          case a: CardSwap =>
            ActionWithEvaluation(a, 0, 0)
        }
      }) -> (>>[FireGolem] :: >>[WaterGolem] :: >>[EarthGolem] :: >>[AirGolem] :: Nil) ::
      ((action: BotAction, botData: BotData) => {
        action match {
          case a: AttackerUsesCard =>
            myHealthZone(botData) match {
              case HealthZone.GreenHealthZone => ActionWithEvaluation(a, a.card.price.total.toInt, 9)
              case HealthZone.YellowHealthZone => ActionWithEvaluation(a, a.card.price.total.toInt, 11)
              case HealthZone.RedHealthZone => ActionWithEvaluation(a, a.card.price.total.toInt, 200)
            }

          case a: CardSwap =>
            ActionWithEvaluation(a, 0, 0)
        }
      }) -> (>>[SearingTouch] :: >>[CalmingTouch] :: >>[HardTouch] :: >>[LightingTouch] :: Nil) ::
      ((action: BotAction, botData: BotData) => {
        action match {
          case a: AttackerUsesCard =>
            enemyHealthZone(botData) match {
              case HealthZone.GreenHealthZone => ActionWithEvaluation(a, a.card.price.total.toInt, 9)
              case HealthZone.YellowHealthZone => ActionWithEvaluation(a, a.card.price.total.toInt, 11)
              case HealthZone.RedHealthZone => ActionWithEvaluation(a, a.card.price.total.toInt, 200)
            }

          case a: CardSwap =>
            ActionWithEvaluation(a, 0, 0)
        }
      }) -> (>>[Fireball] :: >>[WaterBall] :: >>[Earthfall] :: >>[LightingBolt] :: Nil) ::
      ((action: BotAction, botData: BotData) => {
        action match {
          case a: AttackerUsesCard =>
            myHealthZone(botData) match {
              case HealthZone.GreenHealthZone => ActionWithEvaluation(a, a.card.price.total.toInt, 9)
              case HealthZone.YellowHealthZone => ActionWithEvaluation(a, a.card.price.total.toInt, 11)
              case HealthZone.RedHealthZone => ActionWithEvaluation(a, a.card.price.total.toInt, 18)
            }

          case a: CardSwap =>
            ActionWithEvaluation(a, 0, 0)
        }
      }) -> (>>[FireTotem] :: >>[WaterTotem] :: >>[EarthTotem] :: >>[AirTotem] :: Nil) ::
      ((action: BotAction, data: BotData) => {
        implicit val botData = data
        action match {
          case a: AttackerUsesCard =>
            myHealthZone(botData) match {
              case HealthZone.GreenHealthZone =>
                val ratio = (me :: friends).map(f => f.onTable.size).sum / enemies.map(e => e.onTable.size).sum
                if (ratio >= 0.75) {
                  ActionWithEvaluation(a, a.card.price.total.toInt, 12)
                } else {
                  ActionWithEvaluation(a, a.card.price.total.toInt, 9)
                }

              case HealthZone.YellowHealthZone =>
                val ratio = (me :: friends).map(f => f.onTable.size).sum / enemies.map(e => e.onTable.size).sum
                if (ratio >= 0.5) {
                  ActionWithEvaluation(a, a.card.price.total.toInt, 12)
                } else {
                  ActionWithEvaluation(a, a.card.price.total.toInt, 9)
                }

              case HealthZone.RedHealthZone => ActionWithEvaluation(a, a.card.price.total.toInt, -1)
            }

          case a: CardSwap =>
            ActionWithEvaluation(a, 0, 0)
        }
      }) -> (>>[RageofFire] :: >>[RageofWater] :: >>[RageofEarth] :: >>[RageofAir] :: Nil) ::
      ((action: BotAction, data: BotData) => {
        action match {
          case a: DefenderDefends =>
            ActionWithEvaluation(a, a.defenderCard.price.total.toInt, 12)
          case a: CardSwap =>
            ActionWithEvaluation(a, 0, 0)
        }
      }) -> >>[Traitor] ::
      ((action: BotAction, data: BotData) => {
        implicit val botData = data
        action match {
          case a: DefenderDefends =>
            val rating = enemies.map(e => e.attacking.size).sum / enemies.map(e => e.onTable.size).sum
            if (rating >= 0.5) {
              ActionWithEvaluation(a, a.defenderCard.price.total.toInt, 14)
            } else if (rating >= 0.25) {
              ActionWithEvaluation(a, a.defenderCard.price.total.toInt, 9)
            } else {
              ActionWithEvaluation(a, a.defenderCard.price.total.toInt, -1)
            }

          case a: CardSwap =>
            ActionWithEvaluation(a, 0, 0)
        }
      }) -> >>[Sacrifice] ::
      ((action: BotAction, data: BotData) => {
        action match {
          case a: DefenderDefends =>
            ActionWithEvaluation(a, a.defenderCard.price.total.toInt, 10)
          case a: CardSwap =>
            ActionWithEvaluation(a, 0, 10)
        }
      }) -> >>[Parry] ::
      ((action: BotAction, data: BotData) => {
        implicit val botData = data
        action match {
          case a: AttackerUsesCard =>
            val rating = me.hand.size / data.matchSetup.maxCardsInHand
            if (rating >= 0.5) {
              ActionWithEvaluation(a, a.card.price.total.toInt, 20)
            } else {
              ActionWithEvaluation(a, a.card.price.total.toInt, -1)
            }
          case a: CardSwap =>
            ActionWithEvaluation(a, 0, 0)
        }
      }) -> >>[Reinforces] ::
      ((action: BotAction, data: BotData) => {
        implicit val botData = data
        action match {
          case a: AttackerUsesCard =>
            val rating = me.hand.size / data.matchSetup.maxCardsInHand
            if (rating >= 0.5) {
              ActionWithEvaluation(a, a.card.price.total.toInt, 22)
            } else {
              ActionWithEvaluation(a, a.card.price.total.toInt, -1)
            }
          case a: CardSwap =>
            ActionWithEvaluation(a, 0, 0)
        }
      }) -> >>[ElementalSynchronization] ::
      ((action: BotAction, data: BotData) => {
        implicit val botData = data
        action match {
          case a: AttackerUsesCard =>
            ActionWithEvaluation(a, a.card.price.total.toInt, 9)
          case a: CardSwap =>
            ActionWithEvaluation(a, 0, 0)
        }
      }) -> (>>[BlessingofAttacker] :: >>[BlessingofDefender] :: Nil) ::
      ((action: BotAction, data: BotData) => {
        implicit val botData = data
        action match {
          case a: AttackerUsesCard =>
            ActionWithEvaluation(a, a.card.price.total.toInt, 12)
          case a: CardSwap =>
            ActionWithEvaluation(a, 0, 10)
        }
      }) -> (>>[Shield] :: >>[Ring] :: >>[OneHandSword] :: Nil) ::
      ((action: BotAction, data: BotData) => {
        implicit val botData = data
        action match {
          case a: AttackerUsesCard =>
            ActionWithEvaluation(a, a.card.price.total.toInt, 13)
          case a: CardSwap =>
            ActionWithEvaluation(a, 0, 9)
        }
      }) -> >>[Enchantedshield] ::
      ((action: BotAction, data: BotData) => {
        implicit val botData = data
        action match {
          case a: AttackerUsesCard =>
            a.target map {
              case t: PlayerIsTarget =>
                enemies.find(e => e.player == t.playerName).map(f => if(f.hand.size > 0) {
                  ActionWithEvaluation(a, a.card.price.total.toInt, 13)
                } else {
                  ActionWithEvaluation(a, a.card.price.total.toInt, -1)
                }).getOrElse({
                  ActionWithEvaluation(a, a.card.price.total.toInt, -1)
                })
              case t: CardIsTarget =>
                ActionWithEvaluation(a, a.card.price.total.toInt, 10)
            } getOrElse(ActionWithEvaluation(a, a.card.price.total.toInt, 10))

          case a: CardSwap =>
            ActionWithEvaluation(a, 0, 10)
        }
      }) -> >>[Disarm] ::
      ((action: BotAction, data: BotData) => {
        action match {
          case a: DefenderUsesCard =>
            ActionWithEvaluation(a, a.card.price.total.toInt, 10)
          case a: CardSwap =>
            ActionWithEvaluation(a, 0, 8)
        }
      }) -> >>[Spellreact] ::
      Nil
  )
}
