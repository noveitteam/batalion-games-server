package cz.noveit.games.cardgame.engine.bots

import akka.actor._
import cz.noveit.games.cardgame.engine.game._
import cz.noveit.games.cardgame.engine.game.helpers.CardSwappingRequirements
import cz.noveit.games.cardgame.engine.game.session._
import cz.noveit.games.cardgame.engine.player.{CharmOfElements, NeutralDistribution}
import scala.util.Random
import java.util
import scala.collection.immutable.HashMap
import cz.noveit.games.cardgame.engine.card.{CardPosition, CardHolder}
import scala.reflect.ClassTag

/**
 * Created by Wlsek on 18.7.14.
 */

trait BotAction

case class ActionWithEvaluation(action: BotAction, cost: Int, priority: Int)

case class CardSwap(ch: CardHolder, po: CardPosition) extends BotAction

object HashMapWithSequence {
  def apply(sequence: List[Pair[(BotAction, BotData) => ActionWithEvaluation, Any]]) = {
    var mapBuilder = new HashMap[String, (BotAction, BotData) => ActionWithEvaluation]
    sequence.map(p => {
      p._2 match {
        case s: String =>
          mapBuilder += s -> p._1
        case seq: List[String] =>
          seq.map(s => {
            mapBuilder += s -> p._1
          })
        case t =>

      }

    })
    mapBuilder
  }
}

trait Strategy {

  val cardPrioritization: HashMap[String, (BotAction, BotData) => ActionWithEvaluation]

  def prioritizeCard(cardHolder: CardHolder, action: BotAction, data: BotData): ActionWithEvaluation = {
    try {
      cardPrioritization(cardHolder.id)(action, data)
    } catch {
      case t: Throwable =>
        ActionWithEvaluation(action, 0, -1)
    }
  }

  def evaluate(actions: List[BotAction], ts: BotData): List[ActionWithEvaluation] = {
    implicit def botData = ts
    actions.map {
      case a: AttackerUsesCard =>
        prioritizeCard(a.card, a, ts)
      case a: DefenderUsesCard =>
        prioritizeCard(a.card, a, ts)
      case a: AttackerAttacks =>
        prioritizeCard(a.card, a, ts)
      case a: DefenderDefends =>
        prioritizeCard(a.defenderCard, a, ts)
      case a: CardSwap =>
         prioritizeCard(a.ch, a, ts)
    }.toList.sorted(Ordering.by((_: ActionWithEvaluation).priority).reverse)
  }

  def myHealthZone(botData: BotData): HealthZone.Value = {
    val result: Double = botData.vision.myTeamHp.toDouble / botData.matchSetup.teamHp.toDouble
    val divergence: Double = Random.nextInt(3) / 10.0
    var toChoose: List[HealthZone.Value] = List()

    if (result >= 0 && result < 0.33 + divergence) {
      toChoose = HealthZone.RedHealthZone :: toChoose
    }

    if (result > 0.33 - divergence && result < 0.66 + divergence) {
      toChoose = HealthZone.YellowHealthZone :: toChoose
    }

    if (result > 0.66 - divergence && result <= 1) {
      toChoose = HealthZone.GreenHealthZone :: toChoose
    }

    toChoose(Random.nextInt(toChoose.size))
  }

  def enemyHealthZone(botData: BotData) = {
    val result: Double = botData.vision.enemyTeamHp.toDouble / botData.matchSetup.teamHp.toDouble
    val divergence: Double = Random.nextInt(3) / 10.0
    var toChoose: List[HealthZone.Value] = List()

    if (result >= 0 && result < 0.33 + divergence) {
      toChoose = HealthZone.RedHealthZone :: toChoose
    }

    if (result > 0.33 - divergence && result < 0.66 + divergence) {
      toChoose = HealthZone.YellowHealthZone :: toChoose
    }

    if (result > 0.66 - divergence && result <= 1) {
      toChoose = HealthZone.GreenHealthZone :: toChoose
    }

    toChoose(Random.nextInt(toChoose.size))
  }

  def >>[T](implicit tag: ClassTag[T]) = {
    tag.toString()
  }

  def me(implicit botData: BotData): BotSeesPlayer = {
    botData.vision.players.find(p => p.relationship == BotPlayerRelationship.BotPlayerRelationshipMe).get
  }

  def friends(implicit  botData: BotData): List[BotSeesPlayer] = {
    botData.vision.players.filter(p => p.relationship == BotPlayerRelationship.BotPlayerRelationshipFriend)
  }

  def enemies(implicit  botData: BotData): List[BotSeesPlayer] = {
    botData.vision.players.filter(p => p.relationship == BotPlayerRelationship.BotPlayerRelationshipEnemy)
  }
}


case class EvaluateStrategy(actions: List[BotAction], data: BotData)

case class StrategyEvaluated(evaluation: List[ActionWithEvaluation])

class StrategyEvaluateWorker(player: String, strategy: Strategy) extends Actor with ActorLogging {
  def receive = {
    case EvaluateStrategy(actions, data) =>
      sender ! StrategyEvaluated(strategy.evaluate(actions, data))
  }

  override def postStop = {
    log.debug("Killing StrategyEvaluateWorker for {} bot.", player)
  }
}

case class ChooseActionsWithEvaluation(actions: List[ActionWithEvaluation], data: BotData)

case class ActionsWithEvaluationChosen(action: Option[ActionWithEvaluation])

class ChooseActionWorker(player: String) extends Actor with ActorLogging {
  def receive = {
    case ChooseActionsWithEvaluation(actions, data) =>

      sender ! ActionsWithEvaluationChosen(actions.filter(a => a.priority > 0).headOption)
  }

  override def postStop = {
    log.debug("Killing ChooseActionWorker for {} bot.", player)
  }
}

case class ActionsChosen(action: Option[BotAction])

class StrategistWorker(botHandler: ActorRef, player: String, actions: List[BotAction], data: BotData, strategy: Strategy) extends Actor with ActorLogging {
  log.debug("Created BotActionsWorker for {} bot.", player)
  context.actorOf(Props(classOf[StrategyEvaluateWorker], player, strategy)) ! EvaluateStrategy(actions, data)

  def receive = {
    case StrategyEvaluated(evaluation) =>
      context.actorOf(Props(classOf[ChooseActionWorker], player)) ! ChooseActionsWithEvaluation(evaluation, data)

    case ActionsWithEvaluationChosen(a) =>
      botHandler ! ActionsChosen(a.map(ae => ae.action))
      context.stop(self)
  }
}


case class SwapChosen(swap: Option[AttackerSwapsCards])

class StrategistWorkerForCardSwapping(botHandler: ActorRef, player: String, actions: List[BotAction], data: BotData, strategy: Strategy, cardSwappingSetup: CardSwappingRequirements) extends Actor with ActorLogging {
  log.debug("Created BotActionsWorker for {} bot.", player)
  context.actorOf(Props(classOf[StrategyEvaluateWorker], player, strategy)) ! EvaluateStrategy(actions, data)

  def receive = {
    case StrategyEvaluated(evaluation) =>
      val selected = evaluation.take(cardSwappingSetup.numberOfCards)
      if(selected.size == cardSwappingSetup.numberOfCards) {
        var charm = data.me.elements
        var distribution = NeutralDistribution(0,0,0,0)
        for(i <- 1 to cardSwappingSetup.numberOfElements) {
          val taken = takeElement(charm, distribution)
          charm = taken._1
          distribution = taken._2
        }
        botHandler ! SwapChosen(Some(AttackerSwapsCards(selected.map(t => t.action.asInstanceOf[CardSwap].po), distribution)))
      } else {
        botHandler ! SwapChosen(None)
      }
  }

  def takeElement(charm: CharmOfElements, neutralDistribution: NeutralDistribution): Pair[CharmOfElements, NeutralDistribution] = {
    val charmList =  charm.toList
    val distributionList = neutralDistribution.toList
    val first = charmList.sorted(Ordering by ((_:Pair[Symbol, Int])._2)).reverse.head
    charm.fromList(first.copy(_2 = first._2 - 1) :: charmList.filter(p => p._1 != first._1)) ->
    neutralDistribution.fromList(distributionList.find(p => p._1 == first._1)
      .map(found => found.copy(_2 = found._2 + 1)).toList ::: distributionList.filter(p => p._1 != first._1))
  }

}