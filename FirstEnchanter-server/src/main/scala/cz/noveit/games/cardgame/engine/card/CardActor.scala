package cz.noveit.games.cardgame.engine.card

import akka.actor.{Actor, ActorLogging}

/**
 * Created by Wlsek on 3.7.14.
 */
trait CardActor extends Actor with ActorLogging {

  def receive = {
    case msg: EvaluateUseCard =>
      val eu = evaluateUse(msg)
      sender ! eu

    case msg: EvaluateCounterUseCard =>
      val cu = evaluateCounterUse(msg)
      sender ! cu

    case msg: EvaluateAttackWithCard =>
      val awc = evaluateAttackWithCard(msg)
      sender ! awc

    case msg: EvaluateDefendWithCard =>
      val dwc = evaluateDefendWithCard(msg)
      sender ! dwc

    case msg: UseCard =>
      val uc = useCard(msg)
      sender ! uc

    case msg: CounterUseCard =>
      val cuc = counterUseCard(msg)
      sender ! cuc

    case msg: AttackWithCard =>
      val awc = attackWithCard(msg)
      sender ! awc

    case msg: DefendWithCard =>
      val dwc = defendWithCard(msg)
      sender ! dwc

    case msg: AskForPossibilities =>
      val asf = askForPossibilities(msg)
      sender ! asf

    case t => throw new NotImplementedError("Unknown message: " + t.toString)
  }

  def evaluateUse(msg: EvaluateUseCard): EvaluateUsage

  def evaluateCounterUse(msg: EvaluateCounterUseCard): EvaluateUsage

  def evaluateAttackWithCard(msg: EvaluateAttackWithCard): EvaluateAttack

  def evaluateDefendWithCard(msg: EvaluateDefendWithCard): EvaluateAttack

  def useCard(msg: UseCard): TableStateAfterParametrizedUsage

  def counterUseCard(msg: CounterUseCard): TableStateAfterParametrizedCounterUsage

  def attackWithCard(msg: AttackWithCard): TableStateAfterParametrizedAttack

  def defendWithCard(msg: DefendWithCard): TableStateAfterParametrizedDefense

  def askForPossibilities(msg: AskForPossibilities): AskForPossibilities
}
