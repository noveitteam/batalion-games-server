package cz.noveit.games.cardgame.engine.card

import akka.actor.{ActorRef, Props, Actor, ActorLogging}
import cz.noveit.games.cardgame.engine.game.TableState
import cz.noveit.games.cardgame.engine.game.session.{TableStateChange, AttackerSwapsCards, AttackerSwappedCards}
import cz.noveit.games.cardgame.engine.player.{NeutralDistribution, CharmOfElements}
import cz.noveit.games.cardgame.engine.CrossHandlerMessage
import scala.collection.JavaConverters._
import cz.noveit.games.cardgame.recording.XmlRecording

/**
 * Created by Wlsek on 25.6.14.
 */

object CardPositionRefLifeSpan extends Enumeration {
  val CardPositionRefLifeSpanOneRound, CardPositionRefLifeSpanExplicit = Value
}

class CardPositionRef(val referenceLifeSpan: CardPositionRefLifeSpan.Value) {

}

trait CardPosition extends Product {
  val positionId: Int
  val player: String

  def =?(query: CardPosition): Boolean
}

case class InHandCardPosition(val positionId: Int, val player: String) extends CardPosition {
  def =?(query: CardPosition): Boolean = {
    query.player == this.player && query.positionId == this.positionId && query.isInstanceOf[InHandCardPosition]
  }

}

case class OnTableCardPosition(val positionId: Int, val player: String) extends CardPosition {
  def =?(query: CardPosition): Boolean = {
    query.player == this.player && query.positionId == this.positionId && query.isInstanceOf[OnTableCardPosition]
  }
}

case class OnGraveyardPosition(val positionId: Int, val player: String) extends CardPosition {
  def =?(query: CardPosition): Boolean = {
    query.player == this.player && query.positionId == this.positionId && query.isInstanceOf[OnGraveyardPosition]
  }
}

trait ActionWithCard {
  val card: CardHolder
  val onPosition: CardPosition
}

trait PossibilityAsk {
  val players: List[String]
  val tableState: TableState
}

case class ParametrizedUsage(
                              target: Option[CardTarget],
                              onPosition: CardPositionRef,
                              cardHolder: CardHolder,
                              neutralDistribution: NeutralDistribution = NeutralDistribution(0, 0, 0, 0)
                              )

case class UseCard(usage: ParametrizedUsage, ts: TableState)

case class TableStateAfterParametrizedUsage(originalRequest: UseCard, newTableState: TableState, change: Option[ParametrizedUsage], changes: List[TableStateChange] = List())

case class CannotUseCard(originalRequest: UseCard)

case class EvaluateUseCard(usage: ParametrizedUsage, ts: TableState, changesStack: List[TableStateChange])

trait AutomaticPossibility

case object AutomaticPossibilitySubRound extends AutomaticPossibility

case class AutomaticPossibilityAutomaticBehaviorAfterUse(original: UseCard, changes: List[TableStateChange] = List()) extends AutomaticPossibility

case class AutomaticPossibilityAutomaticBehaviorAfterCardSwap(original: AttackerSwapsCards, changes: List[TableStateChange]) extends AutomaticPossibility

case class AutomaticPossibilityAutomaticBehaviorAfterCounterUse(original: CounterUseCard, changes: List[TableStateChange] = List()) extends AutomaticPossibility

case class AutomaticPossibilityAutomaticBehaviorAfterAttack(original: AttackWithCard, changes: List[TableStateChange] = List()) extends AutomaticPossibility

case class AutomaticPossibilityAutomaticBehaviorAfterDefends(original: DefendWithCard, changes: List[TableStateChange] = List()) extends AutomaticPossibility

case object AutomaticPossibilityPlayerRequest extends AutomaticPossibility

case class AskPossibleUsageForPlayer(players: List[String], tableState: TableState, automatic: AutomaticPossibility) extends PossibilityAsk

case class PossibleUsages(players: List[String], usages: List[ParametrizedUsage], automatic: AutomaticPossibility, tableState: TableState)

case class CounterUseCard(usage: ParametrizedUsage, ts: TableState)

case class TableStateAfterParametrizedCounterUsage(originalRequest: CounterUseCard, newTableState: TableState, change: Option[ParametrizedUsage], changes: List[TableStateChange] = List())

case class CannotCounterUseCard(originalRequest: CounterUseCard)

case class EvaluateCounterUseCard(usage: ParametrizedUsage, ts: TableState, changesStack: List[TableStateChange])

case class AskPossibleCounterUsageForPlayer(players: List[String], tableState: TableState, automatic: AutomaticPossibility) extends PossibilityAsk

case class PossibleCounterUsages(players: List[String], counterUsages: List[ParametrizedUsage], automatic: AutomaticPossibility, tableState: TableState)

case class ParametrizedAttack(
                               onPosition: CardPositionRef,
                               card: CardHolder
                               )

case class AttackWithCard(attack: ParametrizedAttack, ts: TableState)

case class TableStateAfterParametrizedAttack(originalRequest: AttackWithCard, newTableState: TableState, change: Option[ParametrizedAttack], changes: List[TableStateChange] = List())

case class CannotAttackWithCard(originalRequest: AttackWithCard)

case class EvaluateAttackWithCard(attack: ParametrizedAttack, ts: TableState, changesStack: List[TableStateChange])

case class AskPossibleAttackPostionsForPlayer(players: List[String], tableState: TableState, automatic: AutomaticPossibility) extends PossibilityAsk

case class PossibleAttacksPositions(players: List[String], possibleAttacks: List[ParametrizedAttack], automatic: AutomaticPossibility, tableState: TableState)

case class ParametrizedDefense(
                                defendWith: CardPositionRef,
                                against: CardTarget,
                                card: CardHolder,
                                neutralDistribution: NeutralDistribution = NeutralDistribution(0, 0, 0, 0)
                                )

case class DefendWithCard(defend: ParametrizedDefense, ts: TableState)

case class TableStateAfterParametrizedDefense(originalRequest: DefendWithCard, newTableState: TableState, change: Option[ParametrizedDefense], changes: List[TableStateChange] = List())

case class CannotDefendWithCard(originalRequest: DefendWithCard)

case class EvaluateDefendWithCard(defend: ParametrizedDefense, ts: TableState, changesStack: List[TableStateChange])

case class AskPossibleDefendingPositionsForPlayer(players: List[String], tableState: TableState, automatic: AutomaticPossibility) extends PossibilityAsk

case class PossibleDefendPositions(players: List[String], possibleDefending: List[ParametrizedDefense], automatic: AutomaticPossibility, tableState: TableState)

case class EvaluateUsage(ts: TableState, changesStack: List[TableStateChange])

case class EvaluateUsageDone(ts: TableState, changesStack: List[TableStateChange]) extends CrossHandlerMessage

case class EvaluateAttack(ts: TableState, changesStack: List[TableStateChange])

case class EvaluateAttackDone(ts: TableState, changesStack: List[TableStateChange]) extends CrossHandlerMessage

trait Possibility

case class CardUsagePossibility(position: CardPositionRef, target: Option[CardTarget], card: CardHolder) extends Possibility

case class CardCounterUsagePossibility(position: CardPositionRef, target: Option[CardTarget], card: CardHolder) extends Possibility

case class CardAttackPossibility(position: CardPositionRef, card: CardHolder) extends Possibility

case class CardDefensePossibility(position: CardPositionRef, against: CardTarget, defenderCard: CardHolder) extends Possibility

case class AskForPossibilities(ask: PossibilityAsk, leftToAsk: List[CardPositionRef], posibilities: List[Possibility])

class CardController(matchHandler: ActorRef) extends Actor with ActorLogging {

  val effectRegistry = new CardStateEffectsRegistry(List())

  def receive = {
    case msg: EvaluateUsage =>
      if (log.isDebugEnabled) {
        if (msg.changesStack.size > 0) {
          log.debug("There are changes!")
        }
      }

      try {
        val counter = msg.ts.counterUsedCards.headOption
        if (counter.isDefined) {

          val position = msg.ts.positionForAlias(counter.get.usage.onPosition)
          val cardHolder = msg.ts.cardForPosition(position)

          if (cardHolder.isDefined) {
            log.debug("[Card controller] evaluating counter usage of {}", cardHolder.get.id)
            if (log.isDebugEnabled) {
              msg.ts.usedCards.map(uc => log.debug("CounterUse position: {} - Card: {}", cardHolder))
            }

            createCardHandler(cardHolder.get) ! EvaluateCounterUseCard(
              counter.get.usage,
              msg.ts,
              msg.changesStack
            )
          } else {
            log.error("Cannot find card for position")
            matchHandler ! EvaluateUsageDone(msg.ts, msg.changesStack)
          }
        } else {
          val use = msg.ts.usedCards.headOption
          if (use.isDefined) {
            val position = msg.ts.positionForAlias(use.get.usage.onPosition)
            val cardHolder = msg.ts.cardForPosition(position)

            if (cardHolder.isDefined) {
              log.debug("[Card controller] evaluating usage of {}", cardHolder.get.id)
              if (log.isDebugEnabled) {
                msg.ts.usedCards.map(uc => log.debug("Use position: {} - Card: {}", position, cardHolder))
              }
              createCardHandler(cardHolder.get) ! EvaluateUseCard(
                use.get.usage,
                msg.ts,
                msg.changesStack
              )
            } else {
              log.error("Cannot find card for position")
              matchHandler ! EvaluateUsageDone(msg.ts, msg.changesStack)
            }
          } else {
            log.info("No usages  to evaluate.")
            if (log.isDebugEnabled) {
              msg.changesStack.map(ch => log.debug("Change: {}", ch))
            }
              matchHandler ! EvaluateUsageDone(msg.ts, msg.changesStack)
          }
        }
      } catch {
        case t: Throwable =>
          log.error("Error while evaluate usage. {}", t.toString)
          matchHandler ! EvaluateUsageDone(msg.ts, msg.changesStack)
      }

    case msg: EvaluateAttack =>
      try {
        val defend = msg.ts.defendedCards.headOption
        if (defend.isDefined) {

          val position = msg.ts.positionForAlias(defend.get.defend.defendWith)
          val cardHolder = msg.ts.cardForPosition(position)

          if (cardHolder.isDefined) {
            log.debug("[Card controller] evaluating defense of {} against {}", cardHolder.get.id, defend.get.defend.against)
            createCardHandler(cardHolder.get) ! EvaluateDefendWithCard(
              defend.get.defend,
              msg.ts,
              msg.changesStack
            )
          } else {
            log.error("Cannot find card for position")
            matchHandler ! EvaluateAttackDone(msg.ts, msg.changesStack)
          }
        } else {
          val attack = msg.ts.attackedCards.headOption
          if (attack.isDefined) {
            val position = msg.ts.positionForAlias(attack.get.attack.onPosition)
            val cardHolder = msg.ts.cardForPosition(position)

            if (cardHolder.isDefined) {
              log.debug("[Card controller] evaluating attack of {}", cardHolder.get.id)
              createCardHandler(cardHolder.get) ! EvaluateAttackWithCard(
                attack.get.attack,
                msg.ts,
                msg.changesStack
              )
            } else {
              log.error("Cannot find card for position")
              matchHandler ! EvaluateAttackDone(msg.ts, msg.changesStack)
            }
          } else {
            log.info("No attacks  to evaluate.")
            matchHandler ! EvaluateAttackDone(msg.ts, msg.changesStack)
          }
        }
      } catch {
        case t: Throwable =>
          log.error("Error while evaluate usage.")
          matchHandler ! EvaluateUsageDone(msg.ts, msg.changesStack)
      }

    case msg: UseCard =>
      try {
        createCardHandler(msg.usage.cardHolder).tell(msg, matchHandler)
      } catch {
        case t: Throwable =>
          log.error("Error while generating class for card: {}", msg.usage.cardHolder.id)
      }

    case msg: CounterUseCard =>
      try {
        createCardHandler(msg.usage.cardHolder).tell(msg, matchHandler)
      } catch {
        case t: Throwable =>
          log.error("Error while generating class for card: {}", msg.usage.cardHolder.id)
      }

    case msg: AttackWithCard =>
      try {
        createCardHandler(msg.attack.card).tell(msg, matchHandler)
      } catch {
        case t: Throwable =>
          log.error("Error while generating class for card: {}", msg.attack.card.id)
      }

    case msg: DefendWithCard =>
      try {
        createCardHandler(msg.defend.card).tell(msg, matchHandler)
      } catch {
        case t: Throwable =>
          log.error("Error while generating class for card: {}", msg.defend.card.id)
      }

    case msg: AskPossibleUsageForPlayer =>
      log.debug("Get AskPossibleUsageForPlayer from match controller.")

      val positionToTransform = msg.players.map(player => {
        msg.tableState.players.find(p => p.nickname == player).map(f => {
          (f.onTable.zipWithIndex.map(z => OnTableCardPosition(z._2, f.nickname)) ++
            f.hands.zipWithIndex.map(z => InHandCardPosition(z._2, f.nickname))).toList
        }).getOrElse(List())
      }).flatten.filter(pos => msg.tableState.usedCards.find(c => {
        val position = msg.tableState.positionForAlias(c.usage.onPosition)
        position =? pos
      }).isEmpty)

      var newTableState = msg.tableState
      val leftToAsk = positionToTransform.map(pt => {
        val alias = newTableState.createNewAliasForPosition(pt, CardPositionRefLifeSpan.CardPositionRefLifeSpanOneRound)
        newTableState = alias._1
        alias._2
      })

      self ! AskForPossibilities(
        msg.copy(tableState = newTableState),
        leftToAsk,
        List()
      )


    case msg: AskPossibleCounterUsageForPlayer =>
      log.debug("Get AskPossibleCounterUsageForPlayer from match controller.")

      val positionToTransform = msg.players.map(player => {
        msg.tableState.players.find(p => p.nickname == player).map(f => {
          (
            f.onTable.zipWithIndex.map(z => OnTableCardPosition(z._2, f.nickname)) ++
              f.hands.zipWithIndex.map(z => InHandCardPosition(z._2, f.nickname))
            ).toList
        }).getOrElse(List())
      }).flatten.filter(pos => msg.tableState.counterUsedCards.find(c => {
        val position = msg.tableState.positionForAlias(c.usage.onPosition)
        position =? pos
      }).isEmpty)

      var newTableState = msg.tableState
      val leftToAsk = positionToTransform.map(pt => {
        val alias = newTableState.createNewAliasForPosition(pt, CardPositionRefLifeSpan.CardPositionRefLifeSpanOneRound)
        newTableState = alias._1
        alias._2
      })

      self ! AskForPossibilities(
        msg.copy(tableState = newTableState),
        leftToAsk,
        List()
      )


    case msg: AskPossibleAttackPostionsForPlayer =>
      log.debug("Get AskPossibleAttackPostionsForPlayer from match controller.")

      val positionToTransform = msg.players.map(player => {
        msg.tableState.players.find(p => p.nickname == player).map(f => {
          f.onTable.zipWithIndex.map(z => OnTableCardPosition(z._2, f.nickname)).toList
        }).getOrElse(List())
      }).flatten.filter(pos => msg.tableState.attackedCards.find(c => {
        val position = msg.tableState.positionForAlias(c.attack.onPosition)
        position =? pos
      }).isEmpty)

      var newTableState = msg.tableState
      val leftToAsk = positionToTransform.map(pt => {
        val alias = newTableState.createNewAliasForPosition(pt, CardPositionRefLifeSpan.CardPositionRefLifeSpanOneRound)
        newTableState = alias._1
        alias._2
      })

      self ! AskForPossibilities(
        msg.copy(tableState = newTableState),
        leftToAsk,
        List()
      )

    case msg: AskPossibleDefendingPositionsForPlayer =>
      log.debug("Get AskPossibleDefendingPositionsForPlayer from match controller.")

      val positionToTransform = msg.players.map(player => {
        msg.tableState.players.find(p => p.nickname == player).map(f => {
          f.onTable.zipWithIndex.map(z => OnTableCardPosition(z._2, f.nickname)).toList
        }).getOrElse(List())
      }).flatten.filter(pos => msg.tableState.defendedCards.find(c => {
        val position = msg.tableState.positionForAlias(c.defend.defendWith)
        position =? pos
      }).isEmpty)

      var newTableState = msg.tableState
      val leftToAsk = positionToTransform.map(pt => {
        val alias = newTableState.createNewAliasForPosition(pt, CardPositionRefLifeSpan.CardPositionRefLifeSpanOneRound)
        newTableState = alias._1
        alias._2
      })

      self ! AskForPossibilities(
        msg.copy(tableState = newTableState),
        leftToAsk,
        List()
      )

    case msg: AskForPossibilities =>
      log.debug("Asking for possibilities. {} left to ask.", msg.leftToAsk.size)
      try {
        if (msg.leftToAsk.size > 0) {
          msg.ask.tableState.cardForPositionRef(msg.leftToAsk.head).map(c => {
            createCardHandler(c) ! msg
          })

        } else {
          msg.ask match {
            case a: AskPossibleUsageForPlayer =>
              log.debug("Sending PossibleUsages to match controller.")
              matchHandler ! PossibleUsages(a.players, msg.posibilities.map(p => ParametrizedUsage(
                p.asInstanceOf[CardUsagePossibility].target,
                p.asInstanceOf[CardUsagePossibility].position,
                p.asInstanceOf[CardUsagePossibility].card
              )), a.automatic, msg.ask.tableState)

            case a: AskPossibleCounterUsageForPlayer =>
              log.debug("Sending PossibleCounterUsages to match controller.")
              matchHandler ! PossibleCounterUsages(a.players, msg.posibilities.map(p => ParametrizedUsage(
                p.asInstanceOf[CardCounterUsagePossibility].target,
                p.asInstanceOf[CardCounterUsagePossibility].position,
                p.asInstanceOf[CardCounterUsagePossibility].card
              )), a.automatic, msg.ask.tableState)

            case a: AskPossibleAttackPostionsForPlayer =>
              log.debug("Sending PossibleAttacksPositions to match controller.")
              matchHandler ! PossibleAttacksPositions(a.players, msg.posibilities.map(p => ParametrizedAttack(
                p.asInstanceOf[CardAttackPossibility].position,
                p.asInstanceOf[CardAttackPossibility].card
              )), a.automatic, msg.ask.tableState)

            case a: AskPossibleDefendingPositionsForPlayer =>
              log.debug("Sending PossibleDefendPositions to match controller.")
              matchHandler ! PossibleDefendPositions(a.players, msg.posibilities.map(p => ParametrizedDefense(
                p.asInstanceOf[CardDefensePossibility].position,
                p.asInstanceOf[CardDefensePossibility].against,
                p.asInstanceOf[CardDefensePossibility].defenderCard
              )), a.automatic, msg.ask.tableState)
          }
        }
      } catch {
        case t: Throwable =>
          log.error("Error while generating class for card. MSG: {}, Error: {} ", msg, t.toString)
      }

    case msg: PreEvaluate =>
      context.actorOf(Props(classOf[ActionPreEvaluation], msg, sender))

    case t =>
      log.error("Unknown message! " + t.toString)
  }

  def createCardHandler(card: CardHolder): ActorRef = {
    context.actorOf(Props(Class.forName(card.id), card, effectRegistry))
  }
}

object PreEvaluateAction extends Enumeration {
  val PreEvaluateActionUse, PreEvaluateActionCounterUse, PreEvaluateActionAttack, PreEvaluateActionDefense = Value;
}

case class PreEvaluate(action: PreEvaluateAction.Value, tableState: TableState, changesStack: List[TableStateChange], effectRegistry: CardStateEffectsRegistry)

case class PreEvaluatedState(action: PreEvaluateAction.Value, tableState: TableState, changesStack: List[TableStateChange], effectRegistry: CardStateEffectsRegistry)

class ActionPreEvaluation(pre: PreEvaluate, originalSender: ActorRef) extends Actor with ActorLogging {

  pre.action match {
    case PreEvaluateAction.PreEvaluateActionUse | PreEvaluateAction.PreEvaluateActionCounterUse =>
      self ! EvaluateUsage(pre.tableState, pre.changesStack)
    case PreEvaluateAction.PreEvaluateActionAttack | PreEvaluateAction.PreEvaluateActionDefense =>
      self ! EvaluateAttack(pre.tableState, pre.changesStack)
  }

  def receive = {
    case msg: EvaluateUsage =>
      pre.action match {
        case PreEvaluateAction.PreEvaluateActionUse =>
          if (!msg.ts.usedCards.isEmpty) {
            doPreEvaluate(pre.copy(
              action = PreEvaluateAction.PreEvaluateActionUse,
              tableState = msg.ts,
              changesStack = msg.changesStack
            ))
          } else {
            originalSender ! PreEvaluatedState(pre.action, msg.ts, msg.changesStack, pre.effectRegistry)
            context.stop(self)
          }
        case PreEvaluateAction.PreEvaluateActionCounterUse =>
          if (!msg.ts.counterUsedCards.isEmpty) {
            doPreEvaluate(pre.copy(
              action = PreEvaluateAction.PreEvaluateActionCounterUse,
              tableState = msg.ts,
              changesStack = msg.changesStack
            ))
          } else {
            originalSender ! PreEvaluatedState(pre.action, msg.ts, msg.changesStack, pre.effectRegistry)
            context.stop(self)
          }
      }


    case msg: EvaluateAttack =>
      pre.action match {
        case PreEvaluateAction.PreEvaluateActionDefense =>
          if (!msg.ts.defendedCards.isEmpty) {
            doPreEvaluate(pre.copy(
              action = PreEvaluateAction.PreEvaluateActionDefense,
              tableState = msg.ts,
              changesStack = msg.changesStack
            ))
          } else {
            originalSender ! PreEvaluatedState(pre.action, msg.ts, msg.changesStack, pre.effectRegistry)
            context.stop(self)
          }
        case PreEvaluateAction.PreEvaluateActionAttack =>
          if (!msg.ts.attackedCards.isEmpty) {
            doPreEvaluate(pre.copy(
              action = PreEvaluateAction.PreEvaluateActionAttack,
              tableState = msg.ts,
              changesStack = msg.changesStack
            ))
          } else {
            originalSender ! PreEvaluatedState(pre.action, msg.ts, msg.changesStack, pre.effectRegistry)
            context.stop(self)
          }
      }
  }


  def doPreEvaluate(preEvaluate: PreEvaluate) = {
    preEvaluate.action match {
      case PreEvaluateAction.PreEvaluateActionUse =>
        preEvaluate.tableState.cardForPositionRef(preEvaluate.tableState.usedCards.head.usage.onPosition).map(card => {
          context.actorOf(Props(Class.forName(
            card.id
          ), card, preEvaluate.effectRegistry)) ! EvaluateUseCard(
            preEvaluate.tableState.usedCards.head.usage, preEvaluate.tableState, preEvaluate.changesStack
          )
        })

      case PreEvaluateAction.PreEvaluateActionCounterUse =>
        preEvaluate.tableState.cardForPositionRef(preEvaluate.tableState.counterUsedCards.head.usage.onPosition).map(card => {
          context.actorOf(Props(Class.forName(
            card.id
          ), card, preEvaluate.effectRegistry)) ! EvaluateCounterUseCard(
            preEvaluate.tableState.counterUsedCards.head.usage, preEvaluate.tableState, preEvaluate.changesStack
          )
        })

      case PreEvaluateAction.PreEvaluateActionDefense =>
        preEvaluate.tableState.cardForPositionRef(preEvaluate.tableState.defendedCards.head.defend.defendWith).map(card => {
          context.actorOf(Props(Class.forName(
            card.id
          ), card, preEvaluate.effectRegistry)) ! EvaluateDefendWithCard(
            preEvaluate.tableState.defendedCards.head.defend, preEvaluate.tableState, preEvaluate.changesStack
          )
        })

      case PreEvaluateAction.PreEvaluateActionAttack =>
       preEvaluate.tableState.cardForPositionRef(preEvaluate.tableState.attackedCards.head.attack.onPosition).map(card => {
          context.actorOf(Props(Class.forName(
            card.id
          ), card, preEvaluate.effectRegistry)) ! EvaluateAttackWithCard(
            preEvaluate.tableState.attackedCards.head.attack, preEvaluate.tableState, preEvaluate.changesStack
          )
        })
    }
  }

}
