package cz.noveit.games.cardgame.engine.card

import cz.noveit.games.cardgame.adapters.{CardTargetingType, Deck, ElementsRating, Card}
import cz.noveit.proto.firstenchanter.FirstEnchanterCards.CardRarity

/**
 * Created by Wlsek on 6.3.14.
 */

object CardHolderBuilder {
  def apply(card: Card, deck: Deck) = {

    var price = ElementsRating(0, 0, 0, 0, 0)
    card.price.map(e => e.element match {
      case 0 => price.copy(water = price.water + e.price)
      case 1 => price.copy(fire = price.fire + e.price)
      case 2 => price.copy(air = price.air + e.price)
      case 3 => price.copy(earth = price.earth + e.price)
      case 4 => price.copy(neutral = price.neutral + e.price)
    })

    CardHolder(
      card.id,
      card.name,
      card.rarity match {
        case 0 => Common
        case 1 => Uncommon
        case 2 => Rare
        case 3 => Hero
      },
      card.version,
      card.imageId,
      price,
      card.attack,
      card.defense,
      card.defense,
      card.tags,
      deck.cards.find(c => c.cardId == card.id).map(f => f.skinId).getOrElse(None),
      card.cardTargetingType,
      card.description.find(des => des.locale == "en-US").map(found => found.description)
    )
  }

  def apply(card: Card, skinId: Int) = {

    var price = ElementsRating(0, 0, 0, 0, 0)
    card.price.map(e => e.element match {
      case 0 => price.copy(water = price.water + e.price)
      case 1 => price.copy(fire = price.fire + e.price)
      case 2 => price.copy(air = price.air + e.price)
      case 3 => price.copy(earth = price.earth + e.price)
      case 4 => price.copy(neutral = price.neutral + e.price)
    })

    CardHolder(
      card.id,
      card.name,
      card.rarity match {
        case 0 => Common
        case 1 => Uncommon
        case 2 => Rare
        case 3 => Hero
      },
      card.version,
      card.imageId,
      price,
      card.attack,
      card.defense,
      card.defense,
      card.tags,
     Some(skinId),
      card.cardTargetingType
    )
  }
}

object CardHolder {
  val EMPTY = CardHolder(
    "",
    "",
    Common,
    "",
    -1,
    ElementsRating(0,0,0,0,0),
    0,
    0,
    0,
    List(),
    None,
    CardTargetingType.None
  )
}

case class CardHolder(id: String,
                      name: String,
                      rarity: CardRarity,
                      version: String,
                      imageId: Int,
                      price: ElementsRating,
                      attack: Int,
                      defense: Int,
                      actualHp: Int,
                      tags: List[String],
                      skinId: Option[Int],
                      cardTargetingType: CardTargetingType.Value,
                      localizedDescription: Option[String] = None
                       )

trait CardRarity

case object Common extends CardRarity

case object Uncommon extends CardRarity

case object Rare extends CardRarity

case object Hero extends CardRarity