package cz.noveit.games.cardgame.engine.card

import cz.noveit.games.cardgame.engine.game.TableState

/**
 * Created by Wlsek on 1.7.14.
 */
trait CardPositionToCardHolder {
  def toCardHolder(position: CardPosition, tableState: TableState): Option[CardHolder] = {
    position match {
      case p: InHandCardPosition =>
        tableState.players.find(pl => pl.nickname == position.player).map(f => {
          f.hands(p.positionId)
        }).map(c => {
          c
        })
      case p: OnTableCardPosition =>
        tableState.players.find(pl => pl.nickname == position.player).map(f => f.onTable(position.positionId)).map(c => {
          c
        })
    }
  }

  def toCardHolderIgnoringPreMovedCards(position: CardPosition, tableState: TableState): Option[CardHolder] = {
    position match {
      case p: InHandCardPosition =>
        try {
          tableState.players.find(pl => pl.nickname == position.player).map(f => {

            val positionsToFilter = tableState.usedCards.map(u => tableState.positionForAlias(u.usage.onPosition))
              .filter(u => u.player == f.nickname && u.isInstanceOf[InHandCardPosition]) :::
              tableState.counterUsedCards.map(u => tableState.positionForAlias(u.usage.onPosition))
                .filter(u => u.player == f.nickname && u.isInstanceOf[InHandCardPosition]) :::
              tableState.defendedCards.map(u => tableState.positionForAlias(u.defend.defendWith))
                .filter(u => u.player == f.nickname && u.isInstanceOf[InHandCardPosition])

            f.hands.zipWithIndex.filter(i => {
              positionsToFilter.find(p => p.positionId == i._2).isEmpty
            }).map(i => i._1).toList(p.positionId)
          }).map(c => {
            c
          })
        } catch {
          case t: Throwable =>
            None
        }
      case p: OnTableCardPosition =>
        try {
          tableState.players.find(pl => pl.nickname == position.player).map(f => f.onTable(position.positionId)).map(c => {
            c
          })
        } catch {
          case t: Throwable =>
            None
        }
    }
  }

}
