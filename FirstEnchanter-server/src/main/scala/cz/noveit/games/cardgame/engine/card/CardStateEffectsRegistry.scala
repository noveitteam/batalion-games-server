package cz.noveit.games.cardgame.engine.card

import cz.noveit.games.cardgame.engine.game.TableState

import scala.reflect.ClassTag

/**
 * Created by Wlsek on 3.7.14.
 */

trait SerializableEffect {
  def isEffectSerializable: Boolean

  def serializeEffect(source: CardPosition, target: CardTarget): com.google.protobuf.Message
}

object CardEffectElement extends Enumeration {
  val CardEffectElementFire, CardEffectElementWater, CardEffectElementAir, CardEffectElementEarth, CardEffectElementNeutral = Value
}

/*

trait CardStateEffectSource

trait CardStateEffectSourceEquipment extends CardStateEffectSource

trait CardStateEffectSourceWeapon extends CardStateEffectSourceEquipment

trait CardStateEffectSourceShield extends CardStateEffectSourceEquipment

trait CardStateEffectSourceRing extends CardStateEffectSourceEquipment

trait CardStateEffectSourceTotem extends CardStateEffectSource {
  val totemPosition: CardPositionRef
}
*/

trait CardStateEffect extends SerializableEffect {
  val element: CardEffectElement.Value = CardEffectElement.CardEffectElementNeutral
  val source: Option[CardPositionRef]
}

/*
trait GlobalStateEffect extends CardStateEffect {}

trait TeamStateEffect extends CardStateEffect {
  val teamMembers: List[String]
}*/

trait AttackCardEffect extends CardStateEffect

trait DefenseCardEffect extends CardStateEffect

trait IncreaseAttackEffect extends AttackCardEffect {
  val increaseAttackValue: Int
}

trait DecreaseAttackCardEffect extends AttackCardEffect {
  val decreaseAttackValue: Int
}

trait IncreaseDefenseCardEffect extends DefenseCardEffect {
  val increaseDefenseValue: Int
}

trait DecreaseDefenseCardEffect extends DefenseCardEffect {
  val decreaseDefenseValue: Int
}

trait EnchantedCardEffect extends CardStateEffect {
  val enchantedValue: Int
}

trait SpellReflectCardEffect extends CardStateEffect {
  val numberOfUsages: Int
}

trait SpellAbsorbCardEffect extends CardStateEffect {
  val numberOfUsages: Int
}

trait SpellBoostCardEffect extends CardStateEffect {
  val spellBoost: Int
}

trait ElementalPresence extends CardStateEffect

case class CardStateEffectRow(effect: CardStateEffect, effectIsOn: CardTarget) {

  def isOnPosition(pos: CardPositionRef, ts: TableState): Boolean = {
    isOnPosition(ts.positionForAlias(pos), ts)
  }

  def isOnPosition(pos: CardPosition, ts: TableState): Boolean = {
    effectIsOn match {
      case t: CardIsTarget =>
        pos =? t.position
      case t: CardRefIsTarget =>
        pos =? ts.positionForAlias(t.position)
      case t: PlayerIsTarget => false
      case t: PlayerTeamIsTarget => false
      case MapIsTarget => false
    }
  }

  def isForPlayer(player: String, ts: TableState): Boolean = {
    effectIsOn match {
      case t: CardIsTarget =>
        t.position.player == player
      case t: CardRefIsTarget =>
        ts.positionForAlias(t.position).player == player
      case t: PlayerIsTarget => t.playerName == player
      case t: PlayerTeamIsTarget => t.team.contains(player)
      case MapIsTarget => true
    }
  }

}

class CardStateEffectsRegistry(val initialEffects: List[CardStateEffectRow]) {

  var effects: List[CardStateEffectRow] = initialEffects

  def addEffectToCard(card: CardPositionRef, effect: CardStateEffect, ts: TableState): TableState = {
    val pair = ts.createNewAliasForPosition(ts.positionForAlias(card))
    effects = CardStateEffectRow(effect, CardRefIsTarget(pair._2)) :: effects
    pair._1
  }

  def addEffectToPlayer(player: String, effect: CardStateEffect): Unit = {
    effects = CardStateEffectRow(effect, PlayerIsTarget(player)) :: effects
  }

  def addEffectToTeam(team: List[String], effect: CardStateEffect): Unit = {
    effects = CardStateEffectRow(effect, PlayerTeamIsTarget(team)) :: effects
  }

  def addEffectToMap(effect: CardStateEffect): Unit = {
    effects = CardStateEffectRow(effect, MapIsTarget) :: effects
  }

  def removeEffectsFromCard(card: CardPositionRef, ts: TableState): TableState = {
    val effect = effects.filter(f => f isOnPosition(card, ts))
    effects = effects.filterNot(f => effect contains (f))

    var newTableState = ts
    effect.map(e => {
      e.effectIsOn match {
        case t: CardRefIsTarget =>
          newTableState = newTableState.removePositionRef(t.position)

        case t => {}
      }

      e.effect.source.map(s => {
        newTableState = newTableState.removePositionRef(s)
      })
    })

    newTableState
  }

  def existsEffect(forCard: CardPositionRef, withTags: List[String], ts: TableState): Boolean = {
    effects.exists(e => {
      e.effect.source.exists(s => {
        val cardForSource = ts.cardForPositionRef(s)
        cardForSource.isDefined && cardForSource.map(cs => cs.tags containsSlice (withTags)).getOrElse(false)
      }) && e.isOnPosition(forCard, ts)
    })
  }

  def removeEffectsForCardWithTags(forCard: CardPositionRef, withTags: List[String], ts: TableState): Pair[List[CardStateEffect], TableState] = {
    val effect = effects.filter(e => {
      e.effect.source.exists(s => {
        val cardForSource = ts.cardForPositionRef(s)
        cardForSource.isDefined && cardForSource.map(cs => cs.tags containsSlice (withTags)).getOrElse(false)
      }) && e.isOnPosition(forCard, ts)
    })

    effects = effects.filterNot(f => effect contains f)

    var newTableState = ts
    effect.map(e => {
      e.effectIsOn match {
        case t: CardRefIsTarget =>
          newTableState = newTableState.removePositionRef(t.position)

        case t => {}
      }

      e.effect.source.map(s => {
        newTableState = newTableState.removePositionRef(s)
      })
    })

    effect.map(r => r.effect).toList -> newTableState
  }

  def removeEffectForCard(eff: CardStateEffect, ts: TableState): TableState = {
    val effect = effects.filter(f => f.effect == eff)
    effects = effects.filterNot(f => effect contains f)

    var newTableState = ts
    effect.map(e => {
      e.effectIsOn match {
        case t: CardRefIsTarget =>
          newTableState = newTableState.removePositionRef(t.position)

        case t => {}
      }
      e.effect.source.map(s => {
        newTableState = newTableState.removePositionRef(s)
      })
    })
    newTableState
  }

  def filterEffectsByPlayerName(player: String, ts: TableState): List[CardStateEffectRow] = {
    effects.filter(f => f.isForPlayer(player, ts))
  }

  def filterEffectsByTags(tags: List[String], ts: TableState): List[CardStateEffectRow] = {
    effects.filter(f => {
      f.effect.source.exists(s => {
        val card = ts.cardForPositionRef(s)
        card.exists(c => c.tags containsSlice (tags))
      })
    })
  }


}
