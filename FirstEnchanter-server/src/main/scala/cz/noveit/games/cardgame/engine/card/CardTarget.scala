package cz.noveit.games.cardgame.engine.card

/**
 * Created by Wlsek on 26.8.2014.
 */
trait CardTarget
case class PlayerTeamIsTarget(team: List[String]) extends CardTarget
case class PlayerIsTarget(playerName: String) extends CardTarget
case class CardIsTarget(position: CardPosition)  extends CardTarget
case object MapIsTarget extends CardTarget

case class CardRefIsTarget(position: CardPositionRef) extends CardTarget