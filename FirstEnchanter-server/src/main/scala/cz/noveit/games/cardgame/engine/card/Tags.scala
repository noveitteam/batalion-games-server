package cz.noveit.games.cardgame.engine.card

/**
 * Created by Wlsek on 10.7.14.
 */
object Tags {
  val soldier = "soldier"
  val archer = "archer"
  val mage = "mage"
  val totem = "totem"
  val harmful = "harmful"
  val spell = "spell"
  val creep = "creep"
  val equip = "equip"
  val weapon = "weapon"
  val shield = "shield"
  val ring = "ring"
  val oneHandSword = "onehandsword"

}
