package cz.noveit.games.cardgame.engine.card.helpers

import cz.noveit.games.cardgame.engine.card._
import cz.noveit.games.cardgame.engine.card.EvaluateAttackWithCard
import cz.noveit.games.cardgame.engine.card.EvaluateAttack
import cz.noveit.games.cardgame.engine.game.TeamHp
import cz.noveit.games.cardgame.engine.game.session.{PlayersLost, PlayerPoolReceivedDamage}

/**
 * Created by Wlsek on 3.7.14.
 */
object AttackOnPlayerPool {
  def apply(msg: EvaluateAttackWithCard, effectRegistry: CardStateEffectsRegistry): EvaluateAttack = {
  var attack = msg.attack.card.attack
  val position = msg.ts.positionForAlias(msg.attack.onPosition)

    effectRegistry
      .filterEffectsByPlayerName(position.player, msg.ts)
      .filter(
        e => (e.effect.isInstanceOf[IncreaseAttackEffect] || e.effect.isInstanceOf[DecreaseAttackCardEffect]) &&
          (e.effectIsOn match {
            case t: CardRefIsTarget =>
              position =? msg.ts.positionForAlias(t.position)
            case t: CardIsTarget =>
              position =? t.position
            case t => true
          })
      )
      .map(e => {
      e.effect match {
        case e: IncreaseAttackEffect => attack += e.increaseAttackValue
        case e: DecreaseAttackCardEffect => attack -= e.decreaseAttackValue
      }
    })

    if (attack < 0) {
      attack = 0
    }

   val evaluated = msg.ts.teamHp.find(hp => !hp.members.contains(position.player)).map(hp => {
      if ((hp.hp - attack) > 0) {
        EvaluateAttack(
          msg.ts.copy(teamHp = TeamHp(hp.members, hp.hp - attack) :: msg.ts.teamHp.filterNot(thp => thp == hp)).removeAttackedCard(msg.attack.onPosition),
          PlayerPoolReceivedDamage(hp.members, attack, position) :: msg.changesStack
        )
      } else {
        EvaluateAttack(
          msg.ts.copy(teamHp = TeamHp(hp.members, hp.hp - attack) :: msg.ts.teamHp.filterNot(thp => thp == hp)).removeAttackedCard(msg.attack.onPosition),
          PlayersLost(hp.members) :: PlayerPoolReceivedDamage(hp.members, attack, position) :: msg.changesStack
        )
      }
    })

   evaluated.map(e => e).getOrElse(
     EvaluateAttack(
       msg.ts.removeAttackedCard(msg.attack.onPosition),
       msg.changesStack
     )
   )
  }
}
