package cz.noveit.games.cardgame.engine.card.helpers

import cz.noveit.games.cardgame.engine.card._
import cz.noveit.games.cardgame.engine.card.EvaluateUsage
import cz.noveit.games.cardgame.engine.game.session.CardConsumed

/**
 * Created by Wlsek on 8.7.14.
 */
object ConsumeCardAfterCounterUse {
  def apply(msg: EvaluateCounterUseCard): EvaluateUsage = {


    //val cardsLeft = msg.ts.counterUsedCards.filter(c => c.onPosition.player == msg.usage.onPosition.player && !(c.onPosition =? msg.usage.onPosition))
    /*
        EvaluateUsage(
          msg.ts.players.find(p => p.nickname == msg.usage.onPosition.player).map(p => {
            msg.ts.copy(players = p.copy(
              hands = p.hands.filterNot(c => c == p.hands(msg.usage.onPosition.positionId)),
              graveyard = p.hands(msg.usage.onPosition.positionId) +: p.graveyard,
              //charm = p.charm -/ (msg.card.price, msg.usage.neutralDistribution)
            ) :: msg.ts.players.filterNot(p => p.nickname == msg.usage.onPosition.player),
              counterUsedCards = CounterUseCardReindexFinisher(ReindexCardPositions(CounterUseCardReindexPreparator(cardsLeft), msg.usage.onPosition)) :::
                msg.ts.counterUsedCards.filter(c => cardsLeft.find(cl => cl.onPosition =? c.onPosition).isEmpty)
            )
          }).getOrElse({
            msg.ts
          }),
          CardConsumed(OnTableCardPosition(msg.usage.onPosition.positionId, msg.usage.onPosition.player), msg.ts.cardForPosition(msg.usage.onPosition).get) :: msg.changesStack
        )*/
    val consumed = CardConsumed(msg.ts.positionForAlias(msg.usage.onPosition), msg.usage.cardHolder)
    EvaluateUsage(
      msg
        .ts
        .moveCardFromHandsToGraveyard(msg.usage.onPosition)
        .removeCounterUsedCard(msg.usage.onPosition),
      consumed :: msg.changesStack
    )
  }
}
