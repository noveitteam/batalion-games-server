package cz.noveit.games.cardgame.engine.card.helpers

import cz.noveit.games.cardgame.engine.card._
import cz.noveit.games.cardgame.engine.game.session.CardConsumed
import cz.noveit.games.cardgame.engine.player.PlayerPlaysMatch
import cz.noveit.games.cardgame.engine.player.PlayerPlaysMatch
import cz.noveit.games.cardgame.engine.card.EvaluateUseCard
import cz.noveit.games.cardgame.engine.card.OnTableCardPosition
import cz.noveit.games.cardgame.engine.card.EvaluateUsage

/**
 * Created by Wlsek on 8.7.14.
 */
object ConsumeCardAfterUse {
  def apply(msg: EvaluateUseCard): EvaluateUsage = {
    val position = msg.ts.positionForAlias(msg.usage.onPosition)
    EvaluateUsage(
      msg.ts.players.find(p => p.nickname == position.player).map(p => {
        msg.ts
          .moveCardFromHandsToGraveyard(msg.usage.onPosition)
          .chargePlayerForCard(p.nickname, msg.usage.cardHolder.price, msg.usage.neutralDistribution)
          .removeUsedCardCard(msg.usage.onPosition)
      }).getOrElse({
        msg.ts.copy(usedCards =  msg.ts.usedCards.tail)
      }),

      CardConsumed(position, msg.usage.cardHolder) :: msg.changesStack
    )
  }
}
