package cz.noveit.games.cardgame.engine.card.helpers

import cz.noveit.games.cardgame.engine.card._
import cz.noveit.games.cardgame.engine.card.EvaluateDefendWithCard
import cz.noveit.games.cardgame.engine.card.EvaluateAttack
import cz.noveit.games.cardgame.engine.card.helpers.transformation.TransferElementsFromPlayerToPlayer
import cz.noveit.games.cardgame.engine.card.helpers.generator.GenerateElements
import cz.noveit.games.cardgame.engine.game.session.{CardDefendendAgainst, CardDestroyerBy, ElementsTransformed}

/**
 * Created by Wlsek on 3.7.14.
 */
object DefendWithCardAgainstCard {

  def apply(msg: EvaluateDefendWithCard, effectRegistry: CardStateEffectsRegistry): EvaluateAttack = {
    var defense = msg.defend.card.defense
    var evaluate: Option[EvaluateAttack] = None
    val position = msg.ts.positionForAlias(msg.defend.defendWith)

    effectRegistry
      .filterEffectsByPlayerName(position.player, msg.ts)
      .filter(
        e => (e.effect.isInstanceOf[IncreaseDefenseCardEffect] || e.effect.isInstanceOf[DecreaseDefenseCardEffect]) &&
          (e.effectIsOn match {
            case t: CardRefIsTarget =>
              position =? msg.ts.positionForAlias(t.position)
            case t: CardIsTarget =>
              position =? t.position
            case t => true
          })
      )
      .map(e => {
      e.effect match {
        case e: IncreaseDefenseCardEffect => defense += e.increaseDefenseValue
        case e: DecreaseDefenseCardEffect => defense -= e.decreaseDefenseValue
      }
    })
    if (defense < 0) {
      defense = 0
    }

    msg.ts.players.find(p => p.nickname == position.player).map(player => {
      msg.ts.attackedCards.find(c =>
        msg.ts.positionForAlias(c.attack.onPosition) =? msg.ts.positionForAlias(msg.defend.against.asInstanceOf[CardRefIsTarget].position)
      ).map(attacker => {
        msg.ts.defendedCards.find(c =>
          msg.ts.positionForAlias(c.defend.defendWith) =? position
        ).map(defender => {
          val attackPosition = msg.ts.positionForAlias(attacker.attack.onPosition)
          val attackerCard = msg.ts.cardForPosition(attackPosition)
          attackerCard.map(card => {
            var attack = card.attack
            effectRegistry
              .filterEffectsByPlayerName(attackPosition.player, msg.ts)
              .filter(
                e => (e.effect.isInstanceOf[IncreaseAttackEffect] || e.effect.isInstanceOf[DecreaseAttackCardEffect]) &&
                  (e.effectIsOn match {
                    case t: CardRefIsTarget =>
                      attackPosition =? msg.ts.positionForAlias(t.position)
                    case t: CardIsTarget =>
                      attackPosition =? t.position
                    case t => true
                  })
              )
              .map(e => {
              e.effect match {
                case e: IncreaseAttackEffect => attack += e.increaseAttackValue
                case e: DecreaseAttackCardEffect => attack -= e.decreaseAttackValue
              }
            })
            if (attack < 0) {
              attack = 0
            }

            val chaining = effectRegistry.effects.find(e => e.isOnPosition(defender.defend.defendWith, msg.ts) && e.effect.isInstanceOf[EnchantedCardEffect]).map(e => {

              msg.ts.players.find(p => p.nickname == msg.ts.positionForAlias(defender.defend.against.asInstanceOf[CardRefIsTarget].position).player).map(ap => {
                val ammount = GenerateElements(ap.charm, e.effect.asInstanceOf[EnchantedCardEffect].enchantedValue)
                msg.copy(
                  ts = TransferElementsFromPlayerToPlayer(msg.ts, ammount, ap.nickname, position.player),
                  changesStack = ElementsTransformed(ammount, ap.nickname, position.player) :: msg.changesStack
                )
              }).getOrElse(msg)
            }).getOrElse(msg)


            if (attack >= defense) {
              /*
                            val cardsLeft = msg.ts.defendedCards
                              .filter(c => {
                              val pos = msg.ts.positionForAlias(c.defend.defendWith)
                                pos.player == position.player && !(pos =? position)
                            })

                            val defendedCards = DefendWithCardReindexFinisher(ReindexCardPositions(DefendWithCardReindexPreparator(cardsLeft), msg.defend.defendWith)) :::
                              msg.ts.defendedCards.filter(c => cardsLeft.find(cl => cl.withPosition =? c.withPosition).isEmpty)

                            evaluate = Some(EvaluateAttack(
                              chaining.ts.copy(
                                attackedCards = chaining.ts.attackedCards.filterNot(c => c.withPosition =? defender.againts.asInstanceOf[CardIsTarget].position),
                                defendedCards = defendedCards,
                                players = PlayerPlaysMatch(
                                  player.nickname,
                                  player.deck,
                                  player.hands,
                                  player.onTable(defender.withPosition.positionId) +: player.graveyard,
                                  player.onTable.filter(c => c == player.onTable(defender.withPosition.positionId)),
                                  player.charmRatio,
                                  player.charm,
                                  player.handler,
                                  player.connected
                                ) :: chaining.ts.players.filterNot(p => p.nickname == defender.withPosition.player)
                              ),
                              CardDestroyerBy(defender.withPosition, defender.againts.asInstanceOf[CardIsTarget].position) :: chaining.changesStack
                            ))

                            effectRegistry.removeEffectsForCard(defender.withPosition)
                            */
              effectRegistry.removeEffectsFromCard(defender.defend.defendWith, chaining.ts)
              evaluate = Some(EvaluateAttack(
                chaining.ts.moveCardFromTableToGraveyard(msg.defend.defendWith).removeDefendedCard(defender.defend.defendWith).removeAttackedCard(defender.defend.against.asInstanceOf[CardRefIsTarget].position),
                CardDestroyerBy(position, msg.ts.positionForAlias(defender.defend.against.asInstanceOf[CardRefIsTarget].position)) :: chaining.changesStack
              ))
            } else {
              evaluate = Some(EvaluateAttack(
                chaining.ts
                  .removeAttackedCard(defender.defend.against.asInstanceOf[CardRefIsTarget].position)
                  .removeDefendedCard(defender.defend.defendWith),
                CardDefendendAgainst(position, msg.ts.positionForAlias(defender.defend.against.asInstanceOf[CardRefIsTarget].position)) :: chaining.changesStack
              ))
            }
          })
        })
      })
    })

    evaluate.map(e => e)
      .getOrElse({
      EvaluateAttack(
        msg.ts
          .removeAttackedCard(msg.defend.against.asInstanceOf[CardRefIsTarget].position)
          .removeDefendedCard(msg.defend.defendWith)
        ,
        msg.changesStack
      )
    })
  }

}
