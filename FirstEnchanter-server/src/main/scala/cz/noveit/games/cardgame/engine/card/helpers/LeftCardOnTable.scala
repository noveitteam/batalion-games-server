package cz.noveit.games.cardgame.engine.card.helpers

import cz.noveit.games.cardgame.engine.card._
import cz.noveit.games.cardgame.engine.card.EvaluateUsage
import cz.noveit.games.cardgame.engine.game.session.StayOnTable
import cz.noveit.games.cardgame.logging.NoActorLogging

/**
 * Created by Wlsek on 3.7.14.
 */
object LeftCardOnTable extends NoActorLogging {
  def apply(msg: EvaluateUseCard): EvaluateUsage = {
    val tableState  = msg.ts
    val onPosition  = msg.ts.positionForAlias(msg.usage.onPosition)
    val changeStack = msg.changesStack

   val evaluatedUsage = EvaluateUsage(tableState.players.find(p => p.nickname == onPosition.player).map(p => {
      debug("Player found for evaulating." + p.toString)
      tableState
        .moveCardFromHandsToTable(msg.usage.onPosition)
        .removeUsedCardCard(msg.usage.onPosition)

    }).getOrElse({
      error("Player not found while evaluating usage.", onPosition.player)
      tableState
    }),
      StayOnTable(InHandCardPosition(onPosition.positionId, onPosition.player), tableState.cardForPosition(onPosition).get) :: changeStack
    )

    evaluatedUsage
  }
}
