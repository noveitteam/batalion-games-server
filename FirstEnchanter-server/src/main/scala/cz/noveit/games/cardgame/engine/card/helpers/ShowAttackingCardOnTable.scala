package cz.noveit.games.cardgame.engine.card.helpers

import cz.noveit.games.cardgame.engine.card._

/**
 * Created by Wlsek on 3.7.14.
 */
object ShowAttackingCardOnTable {
  def apply(msg: AttackWithCard): TableStateAfterParametrizedAttack = {
    TableStateAfterParametrizedAttack(
      msg,
      msg.ts.addAttackedCard(AttackWithCard(msg.attack, msg.ts)),
      Some(msg.attack)
    )
  }
}
