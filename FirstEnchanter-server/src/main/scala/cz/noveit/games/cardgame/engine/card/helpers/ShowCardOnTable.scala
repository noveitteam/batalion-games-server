package cz.noveit.games.cardgame.engine.card.helpers

import cz.noveit.games.cardgame.engine.card.{  TableStateAfterParametrizedUsage, UseCard}
import cz.noveit.games.cardgame.engine.game.session.ElementsAbsorbed
import cz.noveit.games.cardgame.engine.player.CharmOfElements

/**
 * Created by Wlsek on 3.7.14.
 */
object ShowCardOnTable {
  def apply(msg: UseCard): TableStateAfterParametrizedUsage = {
    val onPosition = msg.ts.positionForAlias(msg.usage.onPosition)

    TableStateAfterParametrizedUsage(
      msg,
      msg.ts
        .addUsedCard(UseCard(msg.usage, msg.ts))
        .chargePlayerForCard(onPosition.player, msg.usage.cardHolder.price, msg.usage.neutralDistribution),
      Some(msg.usage),
       List(ElementsAbsorbed(CharmOfElements(0,0,0,0) +/ (msg.usage.cardHolder.price, msg.usage.neutralDistribution), onPosition.player))
    )
  }
}
