package cz.noveit.games.cardgame.engine.card.helpers

import cz.noveit.games.cardgame.engine.card.{TableStateAfterParametrizedCounterUsage, CounterUseCard}
import cz.noveit.games.cardgame.engine.game.session.ElementsAbsorbed
import cz.noveit.games.cardgame.engine.player.CharmOfElements

/**
 * Created by Wlsek on 11.7.14.
 */
object ShowCounterUsingCardOnTable {
  def apply(msg: CounterUseCard): TableStateAfterParametrizedCounterUsage = {
    val onPosition = msg.ts.positionForAlias(msg.usage.onPosition)
    TableStateAfterParametrizedCounterUsage(
      msg,
      msg.ts
        .removeCounterUsedCard(msg.usage.onPosition)
        .chargePlayerForCard(onPosition.player, msg.usage.cardHolder.price, msg.usage.neutralDistribution),
      Some(msg.usage),
      List(ElementsAbsorbed(CharmOfElements(0,0,0,0) +/ (msg.usage.cardHolder.price, msg.usage.neutralDistribution), onPosition.player))
    )
  }
}
