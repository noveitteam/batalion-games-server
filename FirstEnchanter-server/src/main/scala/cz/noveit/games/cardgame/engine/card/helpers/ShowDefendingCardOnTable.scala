package cz.noveit.games.cardgame.engine.card.helpers

import cz.noveit.games.cardgame.engine.card._
import cz.noveit.games.cardgame.engine.game.session.ElementsAbsorbed
import cz.noveit.games.cardgame.engine.player.CharmOfElements

/**
 * Created by Wlsek on 3.7.14.
 */
object ShowDefendingCardOnTable {
  def apply(msg: DefendWithCard): TableStateAfterParametrizedDefense = {
    val onPosition = msg.ts.positionForAlias(msg.defend.defendWith)
    TableStateAfterParametrizedDefense(
      msg,
      msg.ts
        .addDefendedCard(DefendWithCard(msg.defend, msg.ts))
        .chargePlayerForCard(onPosition.player, msg.defend.card.price, msg.defend.neutralDistribution),
      Some(msg.defend)
      ,
      List(ElementsAbsorbed(CharmOfElements(0,0,0,0) +/ (msg.defend.card.price, msg.defend.neutralDistribution), onPosition.player))
    )
  }
}
