package cz.noveit.games.cardgame.engine.card.helpers.generator

import cz.noveit.games.cardgame.engine.card.CardHolder
import scala.util.Random

/**
 * Created by Wlsek on 9.7.14.
 */
object GenerateCards {
  def apply(pool: Array[CardHolder], limit: Int): Pair[Array[CardHolder], Array[CardHolder]] = {
    var temp = pool
    var taken: Array[CardHolder] = Array()
    var i = 0
    while (i < limit && temp.size > 0) {
      val take = temp(new Random().nextInt(temp.size))
      temp = temp.filter(p => p != take)
      taken = take +: taken
      i += 1
    }

    temp -> taken
  }
}
