package cz.noveit.games.cardgame.engine.card.helpers.generator

import cz.noveit.games.cardgame.engine.player.CharmOfElements
import scala.util.Random

/**
 * Created by Wlsek on 9.7.14.
 */

object GenerateElements {
  def apply(availablePool: CharmOfElements, generatorSize: Int): CharmOfElements = {
    var temp = availablePool
    var pool: List[CharmOfElements] = List()
    while (temp.fire > 0 || temp.air > 0 || temp.earth > 0 || temp.water > 0) {
      if (temp.fire > 0) {
        pool = CharmOfElements(1, 0, 0, 0) :: pool
        temp = CharmOfElements(temp.fire - 1, temp.water, temp.earth, temp.air)
      } else if (temp.water > 0) {
        pool = CharmOfElements(0, 1, 0, 0) :: pool
        temp = CharmOfElements(temp.fire, temp.water - 1, temp.earth, temp.air)
      } else if (temp.earth > 0) {
        pool = CharmOfElements(0, 0, 1, 0) :: pool
        temp = CharmOfElements(temp.fire, temp.water, temp.earth - 1, temp.air)
      } else if (temp.air > 0) {
        pool = CharmOfElements(0, 0, 0, 1) :: pool
        temp = CharmOfElements(temp.fire, temp.water, temp.earth, temp.air - 1)
      }
    }

    var result = CharmOfElements(0,0,0,0)

    var i = 0
    while(i < generatorSize && pool.size > 0) {
      val n = new Random().nextInt(pool.size)
      val value = pool(n)
      result = CharmOfElements(result.fire + value.fire, result.water + value.water, result.earth + value.earth, result.air + value.air)

      pool = pool.filter(e => e == value)
      i += 1
    }

    result
  }
}
