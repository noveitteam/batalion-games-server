package cz.noveit.games.cardgame.engine.card.helpers.possibilities

import cz.noveit.games.cardgame.engine.card._
import cz.noveit.games.cardgame.engine.card.AskPossibleUsageForPlayer
import cz.noveit.games.cardgame.engine.card.AskForPossibilities


/**
 * Created by Wlsek on 11.8.14.
 */

object CounterUseCostAndTargets {
  def apply(msg: AskForPossibilities, cardHolder: CardHolder, targets: List[CardTarget], effectsRegistry: CardStateEffectsRegistry): AskForPossibilities = if (msg.leftToAsk.size > 0) {
    val positionToAsk = msg.ask.tableState.positionForAlias(msg.leftToAsk.head)
    msg.ask.tableState.cardForPosition(positionToAsk).map(c => {
      msg.ask.tableState.players.find(p => p.nickname == positionToAsk.player).map(player => {
        if (c.id == cardHolder.id) {
          msg.ask match {
            case a: AskPossibleUsageForPlayer =>
              msg.copy(leftToAsk = msg.leftToAsk.tail)

            case a: AskPossibleCounterUsageForPlayer  =>
              msg.copy(
                leftToAsk = msg.leftToAsk.tail,
                posibilities = if (player.charm.affordAble_?(cardHolder.price)) {
                  player.hands.find(c => c.id == cardHolder.id).map(f => targets.map(t => CardCounterUsagePossibility(msg.leftToAsk.head, Some(t), cardHolder))).getOrElse(List()) ::: msg.posibilities
                } else {
                  msg.posibilities
                }
              )

            case a: AskPossibleAttackPostionsForPlayer =>
              msg.copy(leftToAsk = msg.leftToAsk.tail)

            case a: AskPossibleDefendingPositionsForPlayer =>
              msg.copy(leftToAsk = msg.leftToAsk.tail)

          }
        } else {
          msg
        }
      }).getOrElse({
        msg
      })
    }).getOrElse({
      msg
    })
  }
  else {
    msg
  }
}