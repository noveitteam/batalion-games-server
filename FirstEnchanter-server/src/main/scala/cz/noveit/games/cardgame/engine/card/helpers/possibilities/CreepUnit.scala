package cz.noveit.games.cardgame.engine.card.helpers.possibilities

import cz.noveit.games.cardgame.engine.card._
import cz.noveit.games.cardgame.engine.card.AskPossibleUsageForPlayer
import cz.noveit.games.cardgame.engine.card.AskForPossibilities
/**
 * Created by Wlsek on 4.7.14.
 */
object CreepUnit {
  def apply(msg: AskForPossibilities, cardHolder: CardHolder, effectsRegistry: CardStateEffectsRegistry): AskForPossibilities = if (msg.leftToAsk.size > 0) {
      val positionToAsk = msg.ask.tableState.positionForAlias(msg.leftToAsk.head)
      msg.ask.tableState.cardForPosition(positionToAsk).map(c => {
        msg.ask.tableState.players.find(p => p.nickname == positionToAsk.player).map(player => {
          if (c.id == cardHolder.id) {
            msg.ask match {
              case a: AskPossibleUsageForPlayer =>
                msg.copy(
                  leftToAsk = msg.leftToAsk.tail,
                  posibilities = if (player.charm.affordAble_?(cardHolder.price)) {
                    player.hands.find(c => c.id == cardHolder.id).map(f =>
                      CardUsagePossibility(msg.leftToAsk.head, None, cardHolder)
                    ).headOption.map(cusage => {
                      cusage :: msg.posibilities
                    }).getOrElse({
                      msg.posibilities
                    })
                  } else {
                    msg.posibilities
                  }
                )

              case a: AskPossibleCounterUsageForPlayer =>
                msg.copy(leftToAsk = msg.leftToAsk.tail)

              case a: AskPossibleAttackPostionsForPlayer =>
                msg.copy(leftToAsk = msg.leftToAsk.tail,
                  posibilities = player.onTable.find(cont => cont.id == cardHolder.id)
                    .map(in => CardAttackPossibility(msg.leftToAsk.head, cardHolder)).toList ::: msg.posibilities
                )

              case a: AskPossibleDefendingPositionsForPlayer =>
                msg.copy(
                  leftToAsk = msg.leftToAsk.tail,
                  posibilities = player.onTable.find(cont => cont.id == cardHolder.id).map(in =>
                      if(!msg.ask.tableState.defendedCards.exists(dc => msg.ask.tableState.positionForAlias(dc.defend.defendWith) =? positionToAsk) ) {
                        msg.ask.tableState.attackedCards
                          .filter(ac => !msg.ask.tableState.defendedCards.filter(dc => dc.defend.against.isInstanceOf[CardIsTarget] || dc.defend.against.isInstanceOf[CardRefIsTarget]).exists(
                           dc => (dc.defend.against match {
                             case t: CardRefIsTarget => msg.ask.tableState.positionForAlias(t.position)
                             case t: CardIsTarget => t.position
                           }) =? msg.ask.tableState.positionForAlias(ac.attack.onPosition)
                          )).map(ac => {
                          CardDefensePossibility(msg.leftToAsk.head, CardRefIsTarget(ac.attack.onPosition), cardHolder)
                        }).toList
                      } else {
                        List()
                      }
                  /*
                    msg.ask.tableState.attackedCards.filter(ac =>
                      msg.ask.tableState.defendedCards.filter(dc =>
                        dc.defend.against.isInstanceOf[CardRefIsTarget])
                        .map(dc => dc.defend.against.asInstanceOf[CardRefIsTarget].position)
                        .find(pos => msg.ask.tableState.positionForAlias(pos) =?  msg.ask.tableState.positionForAlias(ac.attack.onPosition)).isEmpty)
                    .map(ac => ).toList

                  */
                    ).getOrElse(List()) ::: msg.posibilities
                )
            }
          } else {
            msg
          }

      }).getOrElse({
        msg
      })
    }).getOrElse({
      msg
    })
  } else {
    msg
  }
}
