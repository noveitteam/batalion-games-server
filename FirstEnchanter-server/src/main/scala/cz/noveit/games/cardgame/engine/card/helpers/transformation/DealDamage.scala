package cz.noveit.games.cardgame.engine.card.helpers.transformation

import cz.noveit.games.cardgame.engine.card._
import cz.noveit.games.cardgame.engine.game.TeamHp
import cz.noveit.games.cardgame.engine.game.TableState
import cz.noveit.games.cardgame.engine.game.session._

/**
 * Created by Wlsek on 9.7.14.
 */

trait DealDamageParameter

case class DealDamageToCard(source: CardPositionRef, card: CardPositionRef, damage: Int) extends DealDamageParameter

case class DealDamageToPlayerTeam(source: CardPositionRef, player: String, damage: Int) extends DealDamageParameter

object DealDamage {
  def apply(ddt: DealDamageParameter, ts: TableState, effectRegistry: CardStateEffectsRegistry): Pair[TableState, List[TableStateChange]] = {
    ddt match {
      case dd: DealDamageToCard =>
        val dPosition = ts.positionForAlias(dd.card)
        val sPosition = ts.positionForAlias(dd.source)

        val card = ts.cardForPositionRef(dd.card)
        card.map(c => {
          var defense = c.defense
          effectRegistry
            .filterEffectsByPlayerName(dPosition.player, ts)
            .filter(
              e => (e.effect.isInstanceOf[IncreaseDefenseCardEffect] || e.effect.isInstanceOf[DecreaseDefenseCardEffect]) &&
                (e.effectIsOn match {
                  case t: CardRefIsTarget =>
                    dPosition =? ts.positionForAlias(t.position)
                  case t: CardIsTarget =>
                    dPosition =? t.position
                  case t => true
                })
            )
            .map(e => {
            e.effect match {
              case e: IncreaseDefenseCardEffect => defense += e.increaseDefenseValue
              case e: DecreaseDefenseCardEffect => defense -= e.decreaseDefenseValue
            }
          })

          ts.players.find(p => p.nickname == dPosition.player).map(player => {
            if (dd.damage >= defense) {
              effectRegistry.removeEffectsFromCard(dd.card, ts
                .moveCardFromTableToGraveyard(dd.card)) -> (CardDestroyerBy(dPosition, sPosition) :: Nil)
            } else {
              ts -> (CardDefendendAgainst(dPosition, sPosition) :: Nil)
            }
          }).getOrElse({
            ts -> List()
          })
        }).getOrElse({
          ts -> List()
        })


      case dd: DealDamageToPlayerTeam =>
        val sourcePosition = ts.positionForAlias(dd.source)
        ts.teamHp.find(hp => hp.members.contains(dd.player)).map(hp => {
          if ((hp.hp - dd.damage) > 0) {
            ts.copy(
              teamHp = TeamHp(hp.members, hp.hp - dd.damage) ::
                ts.teamHp.filterNot(thp => thp == hp)
            ) -> (PlayerPoolReceivedDamage(hp.members, dd.damage, sourcePosition) :: Nil)

          } else {
            ts.copy(
              teamHp = TeamHp(hp.members, hp.hp - dd.damage) ::
                ts.teamHp.filterNot(thp => thp == hp)
            ) -> (PlayersLost(hp.members) :: PlayerPoolReceivedDamage(hp.members, dd.damage, sourcePosition) :: Nil)
          }
        }).getOrElse({
          ts -> List()
        })
    }
  }
}
