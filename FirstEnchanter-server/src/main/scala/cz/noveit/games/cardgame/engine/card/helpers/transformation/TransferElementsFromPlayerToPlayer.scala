package cz.noveit.games.cardgame.engine.card.helpers.transformation

import cz.noveit.games.cardgame.engine.player.CharmOfElements
import cz.noveit.games.cardgame.engine.game.TableState

/**
 * Created by Wlsek on 10.7.14.
 */
object TransferElementsFromPlayerToPlayer {
  def apply(ts: TableState, ch: CharmOfElements, from: String, to: String) = {
    ts.players.find(p => p.nickname == from).map(f => {
      ts.players.find(p => p.nickname == to).map(t => {
        ts.copy(
          players =
            f.copy(
              charm = (f.charm - ch) nullValuesBellowZero
            ) :: t.copy(
              charm = (t.charm + ch) cutBy ts.limits.maxElements
            ) :: ts.players.filter(p => p.nickname != from && p.nickname != to)
        )
      }).getOrElse(ts)
    }).getOrElse(ts)
  }
}
