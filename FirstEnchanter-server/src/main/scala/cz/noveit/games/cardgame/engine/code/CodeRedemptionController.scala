package cz.noveit.games.cardgame.engine.code

import akka.actor._
import cz.noveit.games.cardgame.engine.unlockable._
import scala.collection.immutable.HashMap
import cz.noveit.games.cardgame.adapters._
import cz.noveit.games.cardgame.WorldServices
import cz.noveit.games.cardgame.engine.unlockable.CannotUnlockContent
import cz.noveit.games.cardgame.adapters.DatabaseCodeRedemptionNotPossible
import cz.noveit.games.cardgame.adapters.DatabaseCodeRedemptionPossible
import cz.noveit.games.cardgame.adapters.DatabaseCodeRedemption
import cz.noveit.games.cardgame.engine.unlockable.ContentUnlock
import cz.noveit.games.cardgame.engine.unlockable.ContentUnlocked
import cz.noveit.database.DatabaseResult
import cz.noveit.database.DatabaseCommand
import cz.noveit.games.cardgame.engine.unlockable.CannotUnlockContent
import cz.noveit.games.cardgame.engine.code.CannotRedeemCode
import cz.noveit.games.cardgame.adapters.DatabaseCodeRedemptionNotPossible
import cz.noveit.games.cardgame.adapters.DatabaseCodeRedemptionPossible
import cz.noveit.games.cardgame.adapters.DatabaseCodeRedemption
import cz.noveit.games.cardgame.engine.code.RedeemCode
import cz.noveit.games.cardgame.engine.unlockable.ContentUnlock
import cz.noveit.games.cardgame.engine.unlockable.ContentUnlocked
import cz.noveit.games.cardgame.engine.code.CodeRedeemed
import cz.noveit.database.DatabaseResult
import cz.noveit.database.DatabaseCommand


/**
 * Created by Wlsek on 19.6.14.
 */



object CannotRedeemReason extends Enumeration {
  val CannotRedeemReasonGeneral, CannotRedeemReasonAlreadyOwnsUnlockAble = Value;
}

case class RedeemCode(code: String, user: String)
case class CannotRedeemCode(code: String, reason: CannotRedeemReason.Value)
case class CodeRedeemed(code: String, unlockAbles: List[UnlockedContent])

class CodeRedemptionController(databaseController: ActorRef, databaseParameters: HashMap[String, String]) extends Actor with ActorLogging {
   def receive = {
     case RedeemCode(code, user) =>
        context.actorOf(Props(classOf[CodeRedemptionHandler],databaseController, databaseParameters, sender, code, user))
   }
}

class CodeRedemptionHandler(databaseController: ActorRef,
                            databaseParameters: HashMap[String, String],
                            replyTo: ActorRef,
                            code: String,
                            user: String) extends Actor with ActorLogging {

  val databaseHandler = context.actorOf(
    Props(
      classOf[CodeRedemptionAdapter],
      databaseController,
      databaseParameters("databaseName"),
      databaseParameters("codesCollection"),
      databaseParameters("codesRedeemedCollection"),
      databaseParameters("unlockAblesHistoryCollection")
    )
  )

  var unlockAbles: List[String] = List()
  var unlockedContent: List[UnlockedContent] = List()

  val unlockAbleService = context.actorSelection("/user/" + WorldServices.unlockAbleService)

  databaseHandler ! DatabaseCommand(DatabaseCodeRedemption(code, user))

  def receive = {
    case DatabaseResult(result, h) =>
      result match {
        case DatabaseCodeRedemptionPossible(c, u, ua) =>
          unlockAbles = ua
          unlockNextUnlockAble

        case DatabaseCodeRedemptionNotPossible(c, u, reason) =>
          replyTo ! CannotRedeemCode(c, reason match {
            case DatabaseCodeRedemptionFailReason.DatabaseCodeRedemptionFailReasonException =>
              CannotRedeemReason.CannotRedeemReasonGeneral
            case DatabaseCodeRedemptionFailReason.DatabaseCodeRedemptionFailReasonAlreadyUnlocked =>
              CannotRedeemReason.CannotRedeemReasonAlreadyOwnsUnlockAble
          })
          context.stop(self)

        case DatabaseCodeRedemptionConfirmSuccess(code, user) =>
          replyTo ! CodeRedeemed(code, unlockedContent)
          context.stop(self)

        case DatabaseCodeRedemptionConfirmFail(code, user) =>
          replyTo ! CannotRedeemCode(code, CannotRedeemReason.CannotRedeemReasonGeneral)
          context.stop(self)
      }

    case ContentUnlocked(id, content) =>
      unlockAbles = unlockAbles.filterNot(f => f == id)
      unlockedContent = content :: unlockedContent
      unlockNextUnlockAble

    case CannotUnlockContent(id) =>
      replyTo ! CannotRedeemCode(code, CannotRedeemReason.CannotRedeemReasonGeneral)
      context.stop(self)
  }


  def unlockNextUnlockAble  {
    if(unlockAbles.size > 0) {
      unlockAbleService ! ContentUnlock(unlockAbles.head, UnlockAbleMethod.UnlockAbleMethodRedemptionCode, user)
    } else {
    //  replyTo ! CodeRedeemed(code, unlockedContent)
      databaseHandler ! DatabaseCommand(DatabaseCodeRedemptionConfirm(code, user))
      //context.stop(self)
    }
  }
}
