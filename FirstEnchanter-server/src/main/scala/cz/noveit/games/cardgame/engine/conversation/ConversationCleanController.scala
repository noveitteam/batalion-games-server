package cz.noveit.games.cardgame.engine.conversation

import akka.actor._
import cz.noveit.games.cardgame.engine.ConversationType
import scala.collection.immutable.HashMap
import cz.noveit.games.cardgame.adapters.{ConversationForMaintenance, NoConversationForMaintenance, ChooseConversationForMaintenance, ConversationControllerDatabaseAdapter}
import cz.noveit.database.{DatabaseResult, DatabaseCommand}
import cz.noveit.games.cardgame.adapters.ChooseConversationForMaintenance
import cz.noveit.database.DatabaseResult
import cz.noveit.database.DatabaseCommand
import cz.noveit.games.cardgame.adapters.ConversationForMaintenance
import cz.noveit.games.utils.TimeOuter

/**
 * Created by Wlsek on 23.5.14.
 */


case object CleanDone

class ConversationCleanController(val clusterDispatcher: ActorRef, databaseParams: HashMap[String, String]) extends Actor with ActorLogging {

  val period: Long = 30 * 60 * 1000
  val maxPermRoomAge: Long = 150 * 24 * 60 * 60 * 1000
  val maxNormalRoomAge: Long = 7 * 24 * 60 * 60 * 1000

  val databaseHandler = context.actorOf(
    Props(classOf[ConversationControllerDatabaseAdapter],
      context.actorSelection("/user/databaseController"),
      databaseParams("databaseName"),
      databaseParams("conversationsCollection"),
      databaseParams("conversationsMembershipCollection"),
      databaseParams("conversationsInvitationCollection"),
      databaseParams("conversationsBansCollection")
    ))

  databaseHandler ! DatabaseCommand(ChooseConversationForMaintenance(period))

  var activeTimeOuter: Option[ActorRef] = None

  def receive = {
    case DatabaseResult(message, hash) =>
      message match {
        case ConversationForMaintenance(conversation) =>
          conversation.permanent match {
            case true =>
              context.actorOf(Props(classOf[ConversationPermaRoomsCleaner], clusterDispatcher, databaseHandler, conversation, maxPermRoomAge, self))
            case false =>
              context.actorOf(Props(classOf[ConversationNormalRoomsCleaner], clusterDispatcher, databaseHandler, conversation, maxNormalRoomAge, self))
          }
          activeTimeOuter.map(t => t ! Kill)
          activeTimeOuter = Some(context.actorOf(Props(classOf[TimeOuter], self, period)))

        case NoConversationForMaintenance =>
          activeTimeOuter.map(t => t ! Kill)
          activeTimeOuter = Some(context.actorOf(Props(classOf[TimeOuter], self, period)))
      }

    case CleanDone =>
      databaseHandler ! DatabaseCommand(ChooseConversationForMaintenance(period))

    case ReceiveTimeout =>
      activeTimeOuter = Some(context.actorOf(Props(classOf[TimeOuter], self, period)))
      databaseHandler ! DatabaseCommand(ChooseConversationForMaintenance(period))
  }


}
