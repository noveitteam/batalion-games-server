package cz.noveit.games.cardgame.engine.conversation

import cz.noveit.database.{DatabaseCommand, DatabaseResult}
import cz.noveit.games.cardgame.adapters._
import cz.noveit.games.cardgame.adapters.ConversationInvitation
import cz.noveit.games.cardgame.adapters.ConversationWithMembersFound
import cz.noveit.games.cardgame.adapters.ConversationInvitations
import cz.noveit.games.cardgame.adapters.GetConversationInvitations
import cz.noveit.database.DatabaseResult
import cz.noveit.database.DatabaseCommand
import cz.noveit.games.cardgame.engine._
import cz.noveit.games.cluster.dispatch.{ClusterMessageEvent, DispatchMessageBroadcast}
import akka.actor.{ActorLogging, Actor, ActorRef}
import cz.noveit.games.cardgame.adapters.ConversationInvitation
import cz.noveit.games.cardgame.adapters.GetMessageListForConversation
import cz.noveit.games.cardgame.adapters.ConversationWithMembersCannotFound
import cz.noveit.games.cluster.dispatch.DispatchMessageBroadcast
import cz.noveit.games.cluster.dispatch.ClusterMessageEvent
import cz.noveit.games.cardgame.adapters.ConversationMembers
import cz.noveit.games.cardgame.adapters.ConversationWithMembersFound
import cz.noveit.games.cardgame.adapters.DeleteConversation
import cz.noveit.games.cardgame.adapters.ConversationInvitations
import cz.noveit.games.cardgame.adapters.MessageList
import cz.noveit.games.cardgame.engine.ConversationMembershipChanged
import cz.noveit.games.cardgame.adapters.ConversationFound
import cz.noveit.games.cardgame.adapters.GetConversationInvitations
import cz.noveit.database.DatabaseResult
import cz.noveit.database.DatabaseCommand
import cz.noveit.games.cardgame.adapters.ConversationDetails

/**
 * Created by Wlsek on 23.5.14.
 */
class ConversationNormalRoomsCleaner (clusterDispatcher: ActorRef, databaseAdapter: ActorRef, conversation: Conversation, limitMillis: Long, parent: ActorRef) extends Actor with ActorLogging {

  databaseAdapter ! DatabaseCommand(ConversationMembers(conversation.id))
  var usersToAsk: List[String] = List()

  val olderThan = System.currentTimeMillis() - limitMillis

  def receive = {
    case DatabaseResult(r, h) => {
      r match {
        case ConversationWithMembersFound(cid, members) =>
          members.size match {
            case 0 =>
              delete
              parent ! CleanDone
            case t =>
              databaseAdapter ! DatabaseCommand(GetConversationInvitations(conversation.id))
              usersToAsk = members ::: usersToAsk
          }

        case ConversationInvitations(invitations) =>
          usersToAsk = invitations.collect {
            case i: ConversationInvitation => i.whom
          } ::: usersToAsk

          databaseAdapter ! DatabaseCommand(GetMessageListForConversation(conversation.id))

        case MessageList(messages) =>
          if (messages.size > 0) {
            if(messages.head.time < olderThan) delete
          } else {
            if(conversation.creationTime < olderThan) delete
          }
          parent ! CleanDone

        case ConversationWithMembersCannotFound(c) =>
          delete
          parent ! CleanDone
      }
    }
  }

  def delete {
    databaseAdapter ! DatabaseCommand(DeleteConversation(conversation.id))
    usersToAsk.map(u => {
      clusterDispatcher ! DispatchMessageBroadcast(ClusterMessageEvent(ConversationMembershipChanged(conversation.id, u, ConversationMembershipChangeTypeLeft), PlayerControllerModule.module))
    })
  }
}
