package cz.noveit.games.cardgame.engine.game

import cz.noveit.games.cardgame.engine.game.session.PLAYER_ROLL_POSITION_ATTACKER

/**
 * Created by Wlsek on 7.3.14.
 */
trait GameEndController {
  def endsGame_?(ts: TableState): Boolean = {
    val team1hp = ts.teamHp(0)
    val team2hp = ts.teamHp(1)

    if(team1hp.hp <= 0 || team2hp.hp <= 0) {
      true
    } else {
      val eliminated = eliminatedPlayers(ts)
      val attackersDefenders = ts.startingRollPositions.partition(p => p.position == PLAYER_ROLL_POSITION_ATTACKER)

      if(attackersDefenders._1.filterNot(p => eliminated.contains(p.user)).size > 0 && attackersDefenders._2.filterNot(p => eliminated.contains(p.user)).size > 0) {
        false
      } else {
        true
      }
    }
  }

  def eliminatedPlayers(ts: TableState): List[String] = {
     ts.players.filter(p => p.hands.size == 0 && p.deck.size == 0 && p.onTable.size == 0).map(p => p.nickname).toList
  }
}
