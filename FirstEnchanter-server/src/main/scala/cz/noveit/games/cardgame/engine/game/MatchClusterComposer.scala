package cz.noveit.games.cardgame.engine.game

import akka.actor._
import cz.noveit.games.cluster.dispatch.ClusterMessage
import scala.concurrent.duration._
import cz.noveit.games.cluster.dispatch.DispatchMessageBroadcast
import cz.noveit.games.cluster.dispatch.ClusterMessageEvent
import cz.noveit.games.cardgame.engine.player.TeamComposing
import cz.noveit.games.cluster.registry.SubscribeActorForModuleName
import java.util.Random
import cz.noveit.games.utils.TimeOuter
import cz.noveit.games.cardgame.engine.ServiceModuleMessage
import cz.noveit.games.cardgame.engine.handlers.ServiceHandlersModules

/**
 * Created by Wlsek on 5.3.14.
 */

sealed trait ComposerState

case object COMPOSER_ACCEPTING_SETUPS_AND_MERGING extends ComposerState

case object COMPOSER_CONSUMING_SETUP extends ComposerState

case object COMPOSER_WAITING_AFTER_ACK extends ComposerState

case object COMPOSER_READY_TO_MERGE extends ComposerState


sealed trait ComposerData

case class ComposerStateData(setup: MatchQuerySetup, hops: Int, originalSetups: List[MatchQuerySetup]) extends ComposerData

/* --------------------------- PHASE 1 Discover possibility for merge -------------------------------------- */

case class MergeComposerAck(setup: ComposerStateData, replyTo: ActorRef) extends ClusterMessage

case class MergeComposerAckReply(setup: ComposerStateData, replyTo: ActorRef)

/* -------------------------------------- PHASE 2 Actual merge --------------------------------------------- */

case class MergeComposerAccept(setups: ComposerStateData, replyTo: ActorRef)

case class MergeComposer(setups: ComposerStateData)

case object MergeReset


class MatchClusterComposer(startSetup: MatchQuerySetup, matchFinder: ActorRef) extends Actor with ActorLogging with FSM[ComposerState, ComposerData] {

  log.info("Init new consumer for {} - ", startSetup.players.map(p => p.player))
  startWith(COMPOSER_ACCEPTING_SETUPS_AND_MERGING, ComposerStateData(startSetup, 1, List(startSetup)))
  setTimer("newSearch", StateTimeout, ((new Random().nextInt(5) + 1) seconds), false)

  val identification = "[Consumer] |" + startSetup.players.map(p => p.player).toString + " |"

  when(COMPOSER_ACCEPTING_SETUPS_AND_MERGING /*, stateTimeout = 2 seconds(new Random().nextInt(10) + 5) seconds*/) {
    case Event(NewSetupDiscovered(setup, replyTo), ComposerStateData(actualSetup, hops, originalSetups)) => {
      log.info("{} New setup come, trying to add it.", identification)
      if (actualSetup :?? (setup, hops)) {
        replyTo ! GetSetup
        goto(COMPOSER_CONSUMING_SETUP)
      } else {
        stay()
      }
    }

    case Event(MergeComposerAck(state, replyTo), ComposerStateData(actualSetup, hops, originalSetups)) => {
      log.info("{} merging ack received, reply and changing state to COMPOSER_WAITING_AFTER_ACK.", identification)

      if (actualSetup :?? (state.setup, hops)) {
        replyTo ! MergeComposerAckReply(ComposerStateData(actualSetup, hops, originalSetups), self)
        goto(COMPOSER_WAITING_AFTER_ACK)
      } else {
        stay()
      }
    }

    case Event(MergeComposerAckReply(state, replyTo), ComposerStateData(actualSetup, hops, originalSetups)) => {
      log.info("{} merging ack reply received, requesting merge and changing state to COMPOSER_READY_TO_MERGE.", identification)

      if (actualSetup :?? (state.setup, hops)) {
        replyTo ! MergeComposerAccept(ComposerStateData(actualSetup, hops, originalSetups), self)
        goto(COMPOSER_READY_TO_MERGE)
      } else {
        stay()
      }
    }

    case Event(StateTimeout, ComposerStateData(actualSetup, hops, originalSetups)) => {
      log.info("{} afk status time outed, trying to merge.", identification)
      matchFinder ! MergeComposerAck(ComposerStateData(actualSetup, hops, originalSetups), self)
      stay() using (ComposerStateData(actualSetup, hops + 1, originalSetups))
    }
  }

  when(COMPOSER_CONSUMING_SETUP, stateTimeout = 3 seconds) {
    case Event(SendSetup(setup), ComposerStateData(actualSetup, hops, originalSetups)) => {
      log.info("{} consuming new setup.", identification)
      if (actualSetup :?? (setup, hops)) {
        goto(COMPOSER_ACCEPTING_SETUPS_AND_MERGING) using ( ComposerStateData(actualSetup :: setup , hops + 1, originalSetups = setup :: originalSetups))
      } else {
        setup.creator ! ServiceModuleMessage(RetryFindMatch(setup), ServiceHandlersModules.cardGameMatch)
        goto(COMPOSER_ACCEPTING_SETUPS_AND_MERGING) using (ComposerStateData(actualSetup, hops + 1, originalSetups))
      }
    }

    case Event(StateTimeout, ComposerStateData(actualSetup, hops, originalSetups)) => {
      log.error("Waiting for consuming setup time outed, lost connection to player actor?")
      goto(COMPOSER_ACCEPTING_SETUPS_AND_MERGING) using (ComposerStateData(actualSetup, hops + 1, originalSetups))
    }
  }

  when(COMPOSER_WAITING_AFTER_ACK, stateTimeout = 3 seconds) {
    case Event(MergeComposerAccept(state, replyTo), ComposerStateData(actualSetup, hops, originalSetups)) => {
      log.info("{} merging accept received", identification)
      if (actualSetup :?? (state.setup, hops)) {
        replyTo ! MergeComposer(ComposerStateData(actualSetup, hops, originalSetups))
        stop()
      } else {
        replyTo ! MergeReset
        goto(COMPOSER_ACCEPTING_SETUPS_AND_MERGING)
      }
    }

    case Event(StateTimeout, ComposerStateData(actualSetup, hops, originalSetups)) => {
      log.info("Trying to find composer to merge")
      matchFinder ! MergeComposerAck(ComposerStateData(actualSetup, hops, originalSetups), self)
      stay() using (ComposerStateData(actualSetup, hops + 1, originalSetups))
    }
  }

  when(COMPOSER_READY_TO_MERGE, stateTimeout = 3 seconds) {
    case Event(MergeComposer(state), ComposerStateData(actualSetup, hops, originalSetups)) => {
      log.info("{} merge received", identification)
      if (actualSetup :?? (state.setup, hops)) {
        goto(COMPOSER_ACCEPTING_SETUPS_AND_MERGING) using (ComposerStateData(actualSetup :: state.setup, hops + 1, state.originalSetups ::: originalSetups))
      } else {
        state.originalSetups.map(s => s.creator ! ServiceModuleMessage(RetryFindMatch(state.setup), ServiceHandlersModules.cardGameMatch))
        goto(COMPOSER_ACCEPTING_SETUPS_AND_MERGING) using (ComposerStateData(actualSetup, hops + 1, originalSetups))
      }
    }

    case Event(StateTimeout, ComposerStateData(actualSetup, hops, originalSetups)) => {
      log.error("Waiting for merging setups time outed, lost connection to consumer actor?")
      goto(COMPOSER_ACCEPTING_SETUPS_AND_MERGING) using (ComposerStateData(actualSetup, hops + 1, originalSetups))
    }
  }

  whenUnhandled {
    case Event(StopSearching(setup, reason), ComposerStateData(actualSetup, hops, originalSetups)) => {
      if (originalSetups.contains(setup)) {
        log.info("Stopping match cluster composer?")
        goto(COMPOSER_ACCEPTING_SETUPS_AND_MERGING) using (ComposerStateData(actualSetup -- setup, hops, originalSetups.filterNot(s => s == setup)))
      } else {
        stay()
      }
    }

    case Event(MergeReset, ComposerStateData(actualSetups, hops, originalSetups)) => {
      log.info("{} reseting.", identification)
      goto(COMPOSER_ACCEPTING_SETUPS_AND_MERGING) using (ComposerStateData(actualSetups, hops + 1, originalSetups))
    }

    case Event(MergeComposerAckReply(state, replyTo), ComposerStateData(actualSetups, hops, originalSetups)) => {
      log.info("{} merging ack reply received, but wrong state, sending reset.", identification)
      replyTo ! MergeReset
      stay
    }

    case Event(MergeComposer(state), actualState: ComposerStateData) => {
      log.error("Unhandled state come {}, sending to actors to retry find.", state)
      state.originalSetups.map(s => s.creator ! ServiceModuleMessage(RetryFindMatch(s), ServiceHandlersModules.cardGameMatch))
      stay()
    }

    case Event(SendSetup(setup), actualState: ComposerStateData) => {
      log.error("Unhandled send setup come {}, sending to actor to retry find.", setup)
      setup.creator ! ServiceModuleMessage(RetryFindMatch(setup), ServiceHandlersModules.cardGameMatch)
      stay()
    }

    case Event(e, s) => {
      log.error("Unhandled event {} in state {}.", e.getClass.getCanonicalName, stateName)
      stay()
    }
  }

  onTransition {
    case x -> COMPOSER_ACCEPTING_SETUPS_AND_MERGING =>
      if ((nextStateData.asInstanceOf[ComposerStateData]).setup.isComplete_?) {
        matchFinder ! MatchFound(nextStateData.asInstanceOf[ComposerStateData].setup)
        stop()
      } else {
        if (nextStateData.asInstanceOf[ComposerStateData].originalSetups.size == 0) {
          stop()
        } else {
          val number = (new Random().nextInt(5) + 1)
          println("-------- " + number + " seconds -----")
          setTimer("newSearch", StateTimeout, (number seconds), false)
        }
      }

    case x -> COMPOSER_CONSUMING_SETUP =>
      cancelTimer("newSearch")

    case x -> COMPOSER_READY_TO_MERGE =>
      cancelTimer("newSearch")

  }

  override def postStop {
    matchFinder ! RemoveMatchComposer(self)
  }
}