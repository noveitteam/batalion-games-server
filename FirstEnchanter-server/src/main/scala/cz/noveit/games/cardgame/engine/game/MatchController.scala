package cz.noveit.games.cardgame.engine.game

import akka.actor._
import cz.noveit.games.cardgame.engine.game.helpers.CardSwappingRequirements
import cz.noveit.games.cardgame.engine.game.session._
import cz.noveit.games.cluster.dispatch.ClusterMessage
import cz.noveit.games.cardgame.engine.player.{PlayerPlaysWithActiveDeck, PlayerIsTryingToFindGame}
import cz.noveit.games.utils.TimeOuter
import cz.noveit.games.cardgame.engine.{ServiceModuleMessage, CrossHandlerMessage}
import cz.noveit.games.cardgame.engine.handlers.ServiceHandlersModules
import scala.collection.immutable.HashMap
import scala.concurrent.duration.FiniteDuration
import scala.concurrent.duration._
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import scala.concurrent.Await
import cz.noveit.games.cardgame.engine.bots.{BotPlayerFactory, GetBot, BotPlayerHolder}


/**
 * Created by Wlsek on 24.2.14.
 */

trait MatchGameMode

case object MatchGameModePractice extends MatchGameMode

case object MatchGameModeRanked extends MatchGameMode

trait MatchGameEnemyMode

case object MatchGameEnemyModePVP extends MatchGameEnemyMode

case object MatchGameEnemyModePVE extends MatchGameEnemyMode

trait MatchTeamMode

case object MatchTeamModeOneVsOne extends MatchTeamMode

case object MatchTeamModeTwoVsTwo extends MatchTeamMode

case class MatchQuerySetup(players: List[PlayerIsTryingToFindGame], arenaId: Option[String], mode: MatchGameMode, enemyMode: MatchGameEnemyMode, teamMode: MatchTeamMode, creator: ActorRef) {
  def isBot(player: String): Boolean = {
    players.find(p => p.player == player).map(p => p.bot).getOrElse(false)
  }

  def toXml =
    <querySetup>
      <teams>
        {
        players.groupBy(_.teamToken).map(t => {
          <team token={t._1}>
            {
            t._2.map(p => {
              <player nickname={p.player} division={p.division.toString} rating={p.rating.toString} score={p.score.toString} avatar={p.avatarId.toString} />
            })
            }
          </team>
        })
        }
      </teams>
      <arena>
        {arenaId.getOrElse("")}
      </arena>
      <mode>
        {mode.toString}
      </mode>
      <enemyMode>
        {enemyMode.toString}
      </enemyMode>
      <teamMode>
        {teamMode.toString}
      </teamMode>
    </querySetup>

  def teams = {
    val t = players.groupBy(p => p.teamToken)
    var toReturn: Teams = Teams()
    for (i <- 1 to t.size) {
      i match {
        case 1 =>
          toReturn = toReturn.copy(a = t.head._2)
        case 2 =>
          toReturn = toReturn.copy(b = t.last._2)
      }
    }
    toReturn
  }

  def isMergeAble(setup: MatchQuerySetup) = :?(setup)

  def isMergeAbleWithDOF(setup: MatchQuerySetup, degreesOfFreedom: Int) = :??(setup, degreesOfFreedom)

  def :?(setup: MatchQuerySetup): Boolean = arenaId.exists(arena => setup.arenaId.exists(a => arena == a)) &&
    (mode == setup.mode) &&
    (enemyMode == setup.enemyMode) &&
    (teamMode == setup.teamMode) && {
    (!isComplete_?) && {
      setup.mode match {
        case MatchGameModePractice =>
          setup.teamMode match {
            case MatchTeamModeOneVsOne =>
              setup.enemyMode match {
                case MatchGameEnemyModePVE =>
                  false
                case MatchGameEnemyModePVP =>
                  teams.size ===(0, 1) && setup.teams.size ===(0, 1)
              }
            case MatchTeamModeTwoVsTwo =>
              setup.enemyMode match {
                case MatchGameEnemyModePVE =>
                  teams.size ===(0, 1) && setup.teams.size ===(0, 1)
                case MatchGameEnemyModePVP =>
                  (teams.size ===(0, 1) && setup.teams.size ===(0, 1)) ||
                    (teams.size ===(0, 1) && setup.teams.size ===(1, 1)) ||
                    (teams.size ===(1, 1) && setup.teams.size ===(0, 1)) ||
                    (teams.size ===(1, 1) && setup.teams.size ===(1, 1)) ||
                    (teams.size ===(1, 2) && setup.teams.size ===(0, 1)) ||
                    (teams.size ===(0, 1) && setup.teams.size ===(1, 2))
              }
          }

        case MatchGameModeRanked =>
          setup.teamMode match {
            case MatchTeamModeOneVsOne =>
              teams.size ===(0, 1) && setup.teams.size ===(0, 1)
            case MatchTeamModeTwoVsTwo =>
              teams.size ===(0, 2) && setup.teams.size ===(0, 2)
          }
      }
    }


  }

  def :??(setup: MatchQuerySetup, degreesOfFreedom: Int): Boolean = {
    :?(setup) && {
      val baseRating: Double = (teams.rating.a + teams.rating.b) / 2
      val max: Double = ((100 + degreesOfFreedom) / 100) * baseRating
      val min: Double = ((100 - degreesOfFreedom) / 100) * baseRating

      setup.teams.rating.a >= min && setup.teams.rating.a <= max && setup.teams.rating.b >= min && setup.teams.rating.b <= max
    }
  }


  def merge(setup: MatchQuerySetup) = ::(setup)

  def ::(setup: MatchQuerySetup): MatchQuerySetup = {
    var merged = this
    if (this :? setup) {
      if (setup.arenaId.isDefined) merged.copy(arenaId = setup.arenaId)
      setup.mode match {
        case MatchGameModePractice =>
          setup.teamMode match {
            case MatchTeamModeOneVsOne =>
              setup.enemyMode match {
                case MatchGameEnemyModePVE =>
                case MatchGameEnemyModePVP =>
                  merged = merged.copy(players = setup.players ::: players)
              }
            case MatchTeamModeTwoVsTwo =>
              setup.enemyMode match {
                case MatchGameEnemyModePVE =>
                  merged = merged.copy(players = setup.players.head.copy(teamToken = merged.players.head.teamToken) :: merged.players)

                case MatchGameEnemyModePVP =>
                  if (teams.size ===(0, 1) && setup.teams.size ===(0, 1)) {
                    merged = merged.copy(players = setup.players.head.copy(teamToken = merged.players.head.teamToken) :: merged.players)
                  } else if (teams.size ===(0, 1) && setup.teams.size ===(1, 1)) {
                    merged = merged.copy(players = merged.players.head.copy(teamToken = setup.players.head.teamToken) :: setup.players)
                  } else if (teams.size ===(1, 1) && setup.teams.size ===(0, 1)) {
                    merged = merged.copy(players = setup.players.head.copy(teamToken = merged.players.head.teamToken) :: merged.players)
                  } else if (teams.size ===(1, 1) && setup.teams.size ===(1, 1)) {
                    merged = merged.copy(players = setup.teams.a.head.copy(teamToken = merged.teams.a.head.teamToken) :: merged.players)
                    merged = merged.copy(players = setup.teams.b.head.copy(teamToken = merged.teams.b.head.teamToken) :: merged.players)
                  } else if (teams.size ==(1, 2) && setup.teams.size ==(0, 1)) {
                    merged = merged.copy(players = setup.teams.b.head.copy(teamToken = merged.teams.a.head.teamToken) :: merged.players)
                  } else if (teams.size ==(2, 1) && setup.teams.size ==(0, 1)) {
                    merged = merged.copy(players = setup.teams.b.head.copy(teamToken = merged.teams.b.head.teamToken) :: merged.players)
                  } else if (teams.size ==(1, 2) && setup.teams.size ==(1, 0)) {
                    merged = merged.copy(players = setup.teams.a.head.copy(teamToken = merged.teams.a.head.teamToken) :: merged.players)
                  } else if (teams.size ==(2, 1) && setup.teams.size ==(1, 0)) {
                    merged = merged.copy(players = setup.teams.a.head.copy(teamToken = merged.teams.b.head.teamToken) :: merged.players)
                  } else if (teams.size ===(0, 1) && setup.teams.size ===(1, 2)) {
                    merged = merged.copy(players = merged.teams.b.head.copy(teamToken = setup.teams.a.head.teamToken) :: setup.players)
                  } else if (teams.size ===(0, 1) && setup.teams.size ===(2, 1)) {
                    merged = merged.copy(players = merged.teams.b.head.copy(teamToken = setup.teams.b.head.teamToken) :: setup.players)
                  } else if (teams.size ===(1, 0) && setup.teams.size ===(1, 2)) {
                    merged = merged.copy(players = merged.teams.a.head.copy(teamToken = setup.teams.a.head.teamToken) :: setup.players)
                  } else if (teams.size ===(1, 0) && setup.teams.size ===(2, 1)) {
                    merged = merged.copy(players = merged.teams.a.head.copy(teamToken = setup.teams.b.head.teamToken) :: setup.players)
                  }
              }
          }

        case MatchGameModeRanked =>
          setup.teamMode match {
            case MatchTeamModeOneVsOne =>
              merged = merged.copy(players = merged.players ::: setup.players)
            case MatchTeamModeTwoVsTwo =>
              merged = merged.copy(players = merged.players ::: setup.players)
          }
      }
    }

    merged
  }

  def unMerge(setup: MatchQuerySetup) = --(setup)

  def --(setup: MatchQuerySetup): MatchQuerySetup = {
    this.copy(players = players.filterNot(p => setup.players.find(fp => fp.player == p.player).isDefined))
  }

  def isComplete_? = {
    mode match {
      case MatchGameModePractice =>
        teamMode match {
          case MatchTeamModeOneVsOne =>
            enemyMode match {
              case MatchGameEnemyModePVE =>
                teams.size ===(0, 1)
              case MatchGameEnemyModePVP =>
                teams.size ==(1, 1)
            }

          case MatchTeamModeTwoVsTwo =>
            enemyMode match {
              case MatchGameEnemyModePVE =>
                teams.size ===(0, 2)
              case MatchGameEnemyModePVP =>
                teams.size ==(2, 2)
            }
        }

      case MatchGameModeRanked =>
        teamMode match {
          case MatchTeamModeOneVsOne =>
            teams.size ==(1, 1)
          case MatchTeamModeTwoVsTwo =>
            teams.size ==(2, 2)
        }
    }
  }

}

case class Teams(a: List[PlayerIsTryingToFindGame] = List(), b: List[PlayerIsTryingToFindGame] = List()) {
  def size = TeamVector(a.size, b.size)

  def rating = TeamVector((0 /: a) {
    _ + _.rating
  } / a.size, (0 /: b) {
    _ + _.rating
  } / a.size)
}

case class TeamVector(a: Int, b: Int) {
  def ==(pair: (Int, Int)): Boolean = {
    (a == pair._1 && b == pair._2)
  }

  def ===(pair: (Int, Int)): Boolean = {
    ((a == pair._1 && b == pair._2) || (a == pair._2 && b == pair._1))
  }

  def >(): Boolean = {
    (a > b)
  }

  def <(): Boolean = {
    (a < b)
  }
}


case class FindMatch(setup: MatchQuerySetup)

case class RetryFindMatch(setup: MatchQuerySetup) extends CrossHandlerMessage

case class MatchFound(setup: MatchQuerySetup)


trait StopSearchingReason

case object StopSearchingReasonByCaptain extends StopSearchingReason

case object StopSearchingReasonPlayerOffline extends StopSearchingReason

case class StopSearching(setup: MatchQuerySetup, reason: StopSearchingReason) extends ClusterMessage

case class StoppedSearching(reason: StopSearchingReason) extends CrossHandlerMessage

class MatchController(clusterDispatcher: ActorRef, params: HashMap[String, String]) extends Actor with ActorLogging {

  val matchFinder = context.actorOf(Props(classOf[MatchFinder], clusterDispatcher, self))

  case class MatchPairs(setups: List[MatchQuerySetup], matchActor: ActorRef)

  var matches: List[MatchPairs] = List()

  val botFactory = context actorOf (Props(classOf[BotPlayerFactory], params))

  def receive = {
    case msg: FindMatch =>
      log.info("Message controller received find match for player {}", msg.setup.players.map(t => t.player))
      matchFinder ! msg

    case msg: MatchFound =>
      log.info("Match found for players {}", msg.setup.players.map(p => p.player))
      context.actorOf(Props(classOf[PlayerReadyQuery], msg.setup, self))

    case AllReady(setup, playersPlayDecks) =>
      log.info("All players are ready")

      val ms = MatchSetup(HashMap[State, FiniteDuration](
        PLAYER_STARTS_ROLL -> 20.seconds,
        ATTACKER_TEAM_PLAY -> 60.seconds,
        DEFENDER_TEAM_COUNTER_PLAY -> 60.seconds,
        ATTACKER_TEAM_ATTACK -> 60.seconds,
        DEFENDER_TEAM_DEFENDS -> 60.seconds,
        FORTUNE_ROLL -> 20.seconds,
        EVALUATION_AND_ANIMATION -> 60.seconds
      ), 2, 8, 8, "1", 20, CardSwappingRequirements(2,1))

      var playerPositions: List[StartingRollPosition] = List()


      setup.enemyMode match {
        case MatchGameEnemyModePVE =>

          var rating = 0
          setup.players.map(p => {
            playerPositions = StartingRollPosition(p.player, PLAYER_ROLL_POSITION_ATTACKER) :: playerPositions
            rating += p.rating
          })

          rating = rating / setup.players.size

          var bots: List[PlayerIsTryingToFindGame] = List()
          var decks: List[PlayerPlaysWithActiveDeck] = List()

          for (i <- 0 to setup.players.size) {
            implicit val timeout = Timeout(15 seconds)
            val future = botFactory ? GetBot(rating)
            val botPlayer = Await.result(future, timeout.duration).asInstanceOf[BotPlayerHolder]
            playerPositions = StartingRollPosition(botPlayer.name, PLAYER_ROLL_POSITION_DEFENDER) :: playerPositions
            bots = PlayerIsTryingToFindGame(botPlayer.name, "botTeam", botPlayer.avatarId, rating, 0, botPlayer.challengeId, botPlayer.handler) :: bots
            decks = PlayerPlaysWithActiveDeck(botPlayer.name, botPlayer.deckId) :: decks
          }

          val matchActor = context.actorOf(Props(classOf[Match], setup.copy(players = bots ::: setup.players), ms, playerPositions, playersPlayDecks ::: decks))

        case MatchGameEnemyModePVP =>
          if (setup.teams.rating >) {
            setup.teams.a.map(pl => playerPositions = StartingRollPosition(pl.player, PLAYER_ROLL_POSITION_ATTACKER) :: playerPositions)
          } else {
            setup.teams.b.map(pl => playerPositions = StartingRollPosition(pl.player, PLAYER_ROLL_POSITION_DEFENDER) :: playerPositions)
          }

          val matchActor = context.actorOf(Props(classOf[Match], setup, ms, playerPositions, playersPlayDecks))
      }


    case msg: StopSearching =>
      log.info("Stop searching come for player {}", msg.setup.players.map(t => t.player))
      matchFinder ! msg

    case t => throw new NotImplementedError("unknown message" + t.toString)
  }
}

case class AskReady(replyTo: ActorRef) extends CrossHandlerMessage

case class PlayerReady(playerName: String, activeDeckId: String)

case class PlayerDeclined(playerName: String)

case class PlayersNotReady(players: List[String])

case class AllReady(setup: MatchQuerySetup, playersPlayDecks: List[PlayerPlaysWithActiveDeck])

class PlayerReadyQuery(setup: MatchQuerySetup, controller: ActorRef) extends Actor with ActorLogging {

  val timeOuter = context.actorOf(Props(classOf[TimeOuter], self, 60000L))
  val playersNO = setup.players.size

  var readyPlayers: List[PlayerPlaysWithActiveDeck] = List()

  setup.players.map(p => {
    p.replyTo ! ServiceModuleMessage(AskReady(self), ServiceHandlersModules.cardGameMatch)
  })

  def receive = {
    case ReceiveTimeout =>
      setup.players.map(pl => pl.replyTo ! PlayersNotReady(setup.players.filter(pl => !readyPlayers.contains(pl)).map(p => p.player)))
      context.stop(self)

    case msg: PlayerDeclined =>
      setup.players.map(pl => pl.replyTo ! msg)
      context.stop(self)

    case PlayerReady(playerName, activeDeck) =>
      readyPlayers = PlayerPlaysWithActiveDeck(playerName, activeDeck) :: readyPlayers

      if (readyPlayers.size == playersNO) {
        timeOuter ! AllReady(setup, readyPlayers)
        controller ! AllReady(setup, readyPlayers)
        context.stop(self)
      }

    case t => throw new NotImplementedError("unknown message" + t.toString)
  }
}



