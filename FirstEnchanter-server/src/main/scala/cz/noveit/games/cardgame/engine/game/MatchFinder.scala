package cz.noveit.games.cardgame.engine.game

import akka.actor._
import cz.noveit.games.cluster.dispatch.ClusterMessage
import scala.concurrent.duration._
import cz.noveit.games.cluster.dispatch.DispatchMessageBroadcast
import cz.noveit.games.cluster.dispatch.ClusterMessageEvent
import cz.noveit.games.cardgame.engine.player.TeamComposing
import cz.noveit.games.cluster.registry.SubscribeActorForModuleName
import java.util.Random

/**
 * Created by Wlsek on 7.2.14.
 */


case class NewSetupDiscovered(setup: MatchQuerySetup, replyTo: ActorRef) extends ClusterMessage

case class CreateMatchComposer(setup: MatchQuerySetup)

case class RemoveMatchComposer(replyTo: ActorRef)

case class ReadyForMatch()

class MatchFinder(dispatcher: ActorRef, matchController: ActorRef) extends Actor with ActorLogging {

  val moduleName = "CardGameMatchController"

  val clusterRegistry = context.system.actorSelection("/user/clusterRegistry")
  clusterRegistry ! SubscribeActorForModuleName(self, moduleName)

  var matchQueryHandlers: List[ActorRef] = List()

  def receive = {
    case msg: ClusterMessageEvent =>
      msg.message match {
        case f: NewSetupDiscovered =>
          log.info("Discovered new setup, sending it to {} waiting setup handlers.", matchQueryHandlers.size)
          matchQueryHandlers.map(h => h ! f)

        case f: StopSearching =>
          log.info("Forwarding stop searching to setup handlers.")
          matchQueryHandlers.map(h => h ! f)

        case f: MergeComposerAck =>
          log.info("Searching for merge possibility")
          matchQueryHandlers.filter(h => h != f.replyTo).map(h => h ! f)
      }

    case msg: FindMatch =>
      log.info("Request to search match come, broadcasting it to cluster.")
      val actor = context.actorOf(Props(classOf[MatchClusterQueryHandler], msg.setup, self, 3))
      dispatcher ! DispatchMessageBroadcast(ClusterMessageEvent(NewSetupDiscovered(msg.setup, actor), moduleName, ""))

    case msg: MatchFound =>
      log.info("Found setup, sending request to controller to create match.")
      matchController ! msg

    case msg: CreateMatchComposer =>
      val actor = context.actorOf(Props(classOf[MatchClusterComposer], msg.setup, self))
      matchQueryHandlers = actor :: matchQueryHandlers

    case msg: RemoveMatchComposer =>
      matchQueryHandlers = matchQueryHandlers.filterNot(h => h == msg.replyTo)

    case msg: StopSearching =>
      dispatcher ! DispatchMessageBroadcast(ClusterMessageEvent(msg, moduleName, ""))

    case msg: MergeComposerAck =>
      dispatcher ! DispatchMessageBroadcast(ClusterMessageEvent(msg, moduleName, ""))

  }
}

case object GetSetup

case class SendSetup(setup: MatchQuerySetup)


class MatchClusterQueryHandler(
                                setup: MatchQuerySetup,
                                matchFinder: ActorRef,
                                retries: Int
                                ) extends Actor with ActorLogging {

  context.setReceiveTimeout(new Random().nextInt(5) + 1 seconds)

  def receive = {
    case GetSetup =>
      log.info("Setup consumer found.")
      sender ! SendSetup(setup)
      context.stop(self)

    case ReceiveTimeout =>
      log.info("Setup consumer not found, creating own")
      matchFinder ! CreateMatchComposer(setup)
      context.stop(self)
  }
}




/*
//dodelat vyhledavani pomoci zebricku, nebo proste aby to naslo velkel lvl kdyz uz nic jineho
class MatchClusterComposer(startSetup: MatchQuerySetup, matchFinder: ActorRef) extends Actor with ActorLogging {

val enemyMode = startSetup.enemyMode
val gameMode = startSetup.mode
val map = startSetup.arenaId
val requiredPlayers = startSetup.requiredPlayers

val teamSize = if (enemyMode == MatchGameEnemyModePVE) {
requiredPlayers
} else {
requiredPlayers / 2
}
val numberOfTeams = if (enemyMode == MatchGameEnemyModePVE) {
1
} else {
2
}

var teams: List[TeamComposing] = List(TeamComposing(startSetup.team))

var setups: List[MatchQuerySetup] = List(startSetup)

ready_?

def receive = {
case msg: NewSetupDiscovered =>
  if (canAccept(msg.setup)) {
    msg.replyTo ! GetSetup
  }

case msg: SendSetup =>
  if (canAccept(msg.setup)) {
    setups = msg.setup :: setups

    var tempTeams: List[TeamComposing] = List()

    var added = false
    teams.map(t => {
      if (msg.setup.team.size <= (teamSize - t.players.size) && !added) {
        tempTeams = TeamComposing(msg.setup.team ::: t.players) :: tempTeams
        added = true
      } else {
        tempTeams = t :: tempTeams
      }
    })

    if (!added) tempTeams = TeamComposing(msg.setup.team) :: tempTeams

    teams = tempTeams

  } else {
    msg.replyTo ! SetupDenied
  }
  ready_?

case msg: MergeComposerAck =>
  var acceptAble = true
  msg.setups.map(s => {
    s.team.map(p => {
      if (!setups.find(s => !s.team.find(pl => pl.player.equals(p.player)).isEmpty).isEmpty) acceptAble = false
    })

    if (!canAccept(s)) acceptAble = false
  })

  if (acceptAble) {
    msg.replyTo ! MergeComposerAccept(self)
  }

case msg: MergeComposer =>
  //todo: Accept merging and create new small object for merging

case msg: StopSearching =>
  setups = setups.filterNot(s => s == msg.setup)
  msg.setup.team.map(p => {
    var tempTeams: List[TeamComposing] = List()
    teams.map(t => {
      val filtered = t.players.filterNot(player => p == player)
      if (filtered.size > 0) tempTeams = TeamComposing(t.players.filterNot(player => p == player)) :: tempTeams
    })
    teams = tempTeams
  })

  if (teams.size == 0) {
    matchFinder ! RemoveMatchComposer(self)
    context.stop(self)
  }
}

def canAccept(newSetup: MatchQuerySetup): Boolean = {
var sum = 0
var count = 0

teams.map(t => t.players.map(p => {
  sum = sum + p.rating
  count = count + 1
}))

var average = sum / count
var minAverage = 0.0
var maxAverage = 0.0

var sizeCondition = false

if (gameMode == MatchGameModeRanked) {
  minAverage = average * 0.9
  maxAverage = average * 1.1

  if (teamSize == newSetup.team.size && (numberOfTeams - teams.size) > 0) sizeCondition = true
} else {
  minAverage = average * 0.5
  maxAverage = average * 1.5

  if ((numberOfTeams - teams.size) > 0) {
    sizeCondition = true
  } else {
    teams.map(t => if (newSetup.team.size <= (teamSize - t.players.size)) sizeCondition = true)
  }
}

sum = 0
count = 0

newSetup.team.map(p => {
  sum = sum + p.rating
  count = count + 1
})

average = sum / count

(average >= minAverage && average <= maxAverage && sizeCondition && map == newSetup.arenaId && gameMode == newSetup.mode && enemyMode == newSetup.enemyMode)
}

def ready_? = {
if (teams.size == numberOfTeams) {
  var ok = true
  teams.map(t => if (t.players.size != teamSize) ok = false)
  if (ok) ready
}
}

def ready = {
matchFinder ! RemoveMatchComposer(self)
matchFinder ! MatchFound(setups)
context.stop(self)
}
}

*/