package cz.noveit.games.cardgame.engine.game

import akka.actor.{Props, ActorRef, ActorLogging, Actor}
import cz.noveit.games.cardgame.engine.handlers.ServiceHandlersModules
import cz.noveit.games.cardgame.engine.{CrossHandlerMessage, ServiceModuleMessage}
import cz.noveit.games.cardgame.engine.game.session.MatchSetup

/**
 * Created by Wlsek on 6.8.14.
 */
case class MatchEnds(winners: List[String], losers: List[String], setup: MatchSetup, querySetup: MatchQuerySetup)

case class MatchReward(essences: Int, mmrChange: Int, win: Boolean) extends CrossHandlerMessage

case object Rewarded

class MatchResultController extends Actor with ActorLogging {
  log.debug("Started")

  def receive = {
    case me: MatchEnds =>
      val proxy = context.actorOf(Props(classOf[WaitForRecordingProxy], me.querySetup.players.size, sender))
      me.querySetup.players.map(f => {
        if (me.winners contains f.player) {
          f.replyTo ! ServiceModuleMessage(MatchReward(200, 10, true), ServiceHandlersModules.cardGameMatch)
          proxy ! Rewarded
        } else {
          f.replyTo ! ServiceModuleMessage(MatchReward(200, -10, false), ServiceHandlersModules.cardGameMatch)
          proxy ! Rewarded
        }
      })
  }
}

class WaitForRecordingProxy(waitForNumberOfReplies: Int, replyTo: ActorRef) extends Actor {
  var actual = waitForNumberOfReplies
  def receive = {
    case Rewarded =>
      actual -= 1
      if(actual <= 0) {
        replyTo ! Rewarded
      }
  }
}