package cz.noveit.games.cardgame.engine.game

import cz.noveit.games.cardgame.adapters.ElementsRating
import cz.noveit.games.cardgame.engine.game.helpers.AutomaticNeutralDistribution
import cz.noveit.games.cardgame.engine.game.session.{PLAYER_ROLL_POSITION_ATTACKER, StartingRollPosition, PlayerRollPosition}
import cz.noveit.games.cardgame.logging.NoActorLogging
import scala.collection.immutable.HashMap
import cz.noveit.games.cardgame.engine.card._
import cz.noveit.games.cardgame.recording.XmlRecording
import cz.noveit.games.cardgame.engine.player.{NeutralDistribution, PlayerPlaysMatch}
import cz.noveit.games.cardgame.engine.card.CounterUseCard
import cz.noveit.games.cardgame.engine.card.OnTableCardPosition
import cz.noveit.games.cardgame.engine.card.UseCard


case class TeamHp(members: List[String], hp: Int)

case class TableLimits(maxElements: Int, maxCardsInHand: Int)

case class TableState(
                       players: List[PlayerPlaysMatch],
                       playersTurn: List[String],
                       activeArenaId: String,
                       fortuneIsOnSide: PlayerRollPosition,
                       startingRollPositions: List[StartingRollPosition],
                       teamHp: List[TeamHp],
                       usedCards: List[UseCard],
                       counterUsedCards: List[CounterUseCard],
                       attackedCards: List[AttackWithCard],
                       defendedCards: List[DefendWithCard],
                       positionAliases: HashMap[CardPositionRef, CardPosition],
                       roundNumber: Int,
                       limits: TableLimits
                       ) extends XmlRecording with NoActorLogging {

  def createNewAliasForPosition(position: CardPosition): Pair[TableState, CardPositionRef] = {
    val ref = new CardPositionRef(CardPositionRefLifeSpan.CardPositionRefLifeSpanExplicit)
    this.copy(
      positionAliases = positionAliases + (ref -> position)
    ) -> ref
  }

  def createNewAliasForPosition(position: CardPosition, lifeSpan: CardPositionRefLifeSpan.Value): Pair[TableState, CardPositionRef] = {
    val ref = new CardPositionRef(lifeSpan)
    this.copy(
      positionAliases = positionAliases + (ref -> position)
    ) -> ref
  }

  def duplicateAlias(alias: CardPositionRef): Pair[TableState, CardPositionRef] = {
    val position = positionForAlias(alias)
    createNewAliasForPosition(position)
  }

  def duplicateAlias(alias: CardPositionRef, lifeSpan: CardPositionRefLifeSpan.Value): Pair[TableState, CardPositionRef] = {
    val position = positionForAlias(alias)
    createNewAliasForPosition(position, lifeSpan)
  }


  def positionForAlias(alias: CardPositionRef) = {
    try {
      positionAliases(alias)
    } catch {
      case t: Throwable =>
        error("Error while getting position for alias {}. \n Positions: {}", alias, positionAliases)
        throw t
    }
  }

  def removePositionRef(cardPosition: CardPositionRef): TableState = {
    this.copy(
      positionAliases = positionAliases.filter(f => f._1 != cardPosition)
    )
  }

  def removePosition(cardPosition: CardPosition): TableState = {
    var changedAliases: HashMap[CardPositionRef, CardPosition] = HashMap()
    cardPosition match {
      case p: InHandCardPosition =>
        positionAliases
          .filter(f => f._2.isInstanceOf[InHandCardPosition])
          .filter(f => f._2.asInstanceOf[InHandCardPosition].player == cardPosition.player)
          .map(left => {
          val position = left._2.asInstanceOf[InHandCardPosition]
          if (position.positionId > cardPosition.positionId) {
            changedAliases += left._1 -> InHandCardPosition(position.positionId - 1, position.player)
          }
        })

      case p: OnTableCardPosition =>
        positionAliases
          .filter(f => f._2.isInstanceOf[OnTableCardPosition])
          .filter(f => f._2.asInstanceOf[OnTableCardPosition].player == cardPosition.player)
          .map(left => {
          val position = left._2.asInstanceOf[OnTableCardPosition]
          if (position.positionId > cardPosition.positionId) {
            changedAliases += left._1 -> OnTableCardPosition(position.positionId - 1, position.player)
          }
        })

      case p: OnGraveyardPosition =>
        positionAliases
          .filter(f => f._2.isInstanceOf[OnGraveyardPosition])
          .filter(f => f._2.asInstanceOf[OnGraveyardPosition].player == cardPosition.player)
          .map(left => {
          val position = left._2.asInstanceOf[OnGraveyardPosition]
          if (position.positionId > cardPosition.positionId) {
            changedAliases += left._1 -> OnGraveyardPosition(position.positionId - 1, position.player)
          }
        })
    }

    this.copy(
      positionAliases = changedAliases ++: positionAliases.filter(filter => changedAliases.find(find => find._1 == filter._1).isEmpty)
    )

  }

  def addedPosition(cardPosition: CardPosition): TableState = {
    var changedAliases: HashMap[CardPositionRef, CardPosition] = HashMap()
    cardPosition match {
      case p: InHandCardPosition =>
        positionAliases
          .filter(f => f._2.isInstanceOf[InHandCardPosition])
          .filter(f => f._2.asInstanceOf[InHandCardPosition].player == cardPosition.player)
          .map(left => {
          val position = left._2.asInstanceOf[InHandCardPosition]
          if (position.positionId > cardPosition.positionId) {
            changedAliases += left._1 -> InHandCardPosition(position.positionId + 1, position.player)
          }
        })

      case p: OnTableCardPosition =>
        positionAliases
          .filter(f => f._2.isInstanceOf[OnTableCardPosition])
          .filter(f => f._2.asInstanceOf[OnTableCardPosition].player == cardPosition.player)
          .map(left => {
          val position = left._2.asInstanceOf[OnTableCardPosition]
          if (position.positionId > cardPosition.positionId) {
            changedAliases += left._1 -> OnTableCardPosition(position.positionId + 1, position.player)
          }
        })

      case p: OnGraveyardPosition =>
        positionAliases
          .filter(f => f._2.isInstanceOf[OnGraveyardPosition])
          .filter(f => f._2.asInstanceOf[OnGraveyardPosition].player == cardPosition.player)
          .map(left => {
          val position = left._2.asInstanceOf[OnGraveyardPosition]
          if (position.positionId > cardPosition.positionId) {
            changedAliases += left._1 -> OnGraveyardPosition(position.positionId + 1, position.player)
          }
        })
    }

    this.copy(
      positionAliases = changedAliases ++: positionAliases.filter(filter => changedAliases.find(find => find._1 == filter._1).isEmpty)
    )
  }

  def arePlayersOnSameTeam(player: String, withPlayer: String): Boolean = {
    val pair = startingRollPositions.partition(p => p.position == PLAYER_ROLL_POSITION_ATTACKER)
    if (pair._1.map(rp => rp.user).contains(player)) {
      pair._1.contains(withPlayer)
    } else {
      pair._2.contains(withPlayer)
    }
  }

  def getPlayerTeam(player: String): List[String] = {
    val pair = startingRollPositions.partition(p => p.position == PLAYER_ROLL_POSITION_ATTACKER)
    (
      if (pair._1.map(rp => rp.user).contains(player)) {
        pair._1
      } else {
        pair._2
      }
      ).collect {
      case p: StartingRollPosition => p.user
    }
  }

  def getEnemyTeam(player: String): List[String] = {
    val pair = startingRollPositions.partition(p => p.position == PLAYER_ROLL_POSITION_ATTACKER)
    (
      if (pair._1.map(rp => rp.user).contains(player)) {
        pair._2
      } else {
        pair._1
      }
      ).collect {
      case p: StartingRollPosition => p.user
    }
  }


  def cardForPosition(position: CardPosition): Option[CardHolder] = {
    try {
      players.find(p => p.nickname == position.player).map(f => {
        position match {
          case p: InHandCardPosition =>
            f.hands(p.positionId)
          case p: OnTableCardPosition =>
            f.onTable(p.positionId)
        }
      })
    } catch {
      case t: Throwable =>
        None
    }
  }

  def positionForCardHolder(card: CardHolder): Option[CardPosition] = {
    players.find(p => p.hands.contains(card) || p.onTable.contains(card) || p.graveyard.contains(card)).map(f => {
      if (f.hands.contains(card)) {
        Some(InHandCardPosition(f.hands.indexOf(card), f.nickname))
      } else  if (f.onTable.contains(card)) {
        Some(OnTableCardPosition(f.onTable.indexOf(card), f.nickname))
      } else  if (f.graveyard.contains(card)) {
        Some(OnGraveyardPosition(f.graveyard.indexOf(card), f.nickname))
      } else {
        None
      }
    }).getOrElse({
      None
    })
  }

  def cardForPositionRef(alias: CardPositionRef): Option[CardHolder] = {
    try {
      val position = this.positionForAlias(alias)
      players.find(p => p.nickname == position.player).map(f => {
        position match {
          case p: InHandCardPosition =>
            f.hands(p.positionId)
          case p: OnTableCardPosition =>
            f.onTable(p.positionId)
        }
      })
    } catch {
      case t: Throwable =>
        error("Error while getting position for alias: " + t.toString)
        None
    }
  }

  def moveCardFromHandsToTable(positionRef: CardPositionRef): TableState = {
    val position = positionForAlias(positionRef)
    val card = cardForPosition(position)

    val tableState = players.find(p => p.nickname == position.player).map(p => {
      val newPlayer = p.copy(
        hands = p.hands.filterNot(c => c == p.hands(position.positionId)),
        onTable = p.onTable :+ p.hands(position.positionId)
      )

      val newTableState = this.copy(
        players = newPlayer :: players.filterNot(player => player.nickname == newPlayer.nickname)
      )

      val reindexedAliases = CardPositionAliasesReindex(newTableState.positionAliases, position, OnTableCardPosition(newPlayer.onTable.size - 1, position.player))
        .asInstanceOf[HashMap[CardPositionRef, CardPosition]]

      newTableState.copy(
        positionAliases = reindexedAliases
      )
        .addedPosition(OnTableCardPosition(newPlayer.onTable.size - 1, position.player))
        .removePosition(position)
    })

    tableState.getOrElse({
      info("Error while creating new table state")
      this
    })
  }

  def moveCardFromHandsToGraveyard(positionRef: CardPositionRef): TableState = {
    val position = positionForAlias(positionRef)
    val card = cardForPosition(position)

    players.find(p => p.nickname == position.player).map(p => p.copy(
      hands = p.hands.filterNot(c => c == p.hands(position.positionId)),
      graveyard = p.graveyard :+ p.hands(position.positionId)
    )).map(newPlayer => {
      val newTableState = this.copy(
        players = newPlayer :: players.filterNot(player => player.nickname == newPlayer.nickname)
      ).removePosition(position).addedPosition(OnGraveyardPosition(newPlayer.graveyard.size - 1, position.player))

      newTableState.copy(
        positionAliases = newTableState.positionAliases + (positionRef -> OnGraveyardPosition(newPlayer.graveyard.size - 1, position.player))
      )
    }).getOrElse({
      this
    })
  }

  def moveCardFromTableToGraveyard(positionRef: CardPositionRef): TableState = {
    val position = positionForAlias(positionRef)
    val card = cardForPosition(position)

   val newTableState = players.find(p => p.nickname == position.player).map(p => p.copy(
      onTable = p.onTable.filterNot(c => c == p.onTable(position.positionId)),
      graveyard = p.graveyard :+ p.onTable(position.positionId)
    )).map(newPlayer => {
      val reindexedAliases = CardPositionAliasesReindex(this.positionAliases, position, OnGraveyardPosition(newPlayer.graveyard.size - 1, position.player))
        .asInstanceOf[HashMap[CardPositionRef, CardPosition]]

      this.copy(
        players = newPlayer :: players.filterNot(player => player.nickname == newPlayer.nickname),
        positionAliases = reindexedAliases
      ).removePosition(position).addedPosition(OnGraveyardPosition(newPlayer.graveyard.size - 1, position.player))

    })

    newTableState.getOrElse({
      info("Error while creating new table state")
      this
    })
  }

  def moveCardFromTableToHands(positionRef: CardPositionRef): TableState = {
    val position = positionForAlias(positionRef)
    val card = cardForPosition(position)

    players.find(p => p.nickname == position.player).map(p => p.copy(
      onTable = p.onTable.filterNot(c => c == p.onTable(position.positionId)),
      hands = p.hands :+ p.onTable(position.positionId)
    )).map(newPlayer => {
      val newTableState = this.copy(
        players = newPlayer :: players.filterNot(player => player.nickname == newPlayer.nickname)
      ).removePosition(position).addedPosition(InHandCardPosition(newPlayer.hands.size - 1, position.player))

      newTableState.copy(
        positionAliases = newTableState.positionAliases + (positionRef -> InHandCardPosition(newPlayer.hands.size - 1, position.player))
      )
    }).getOrElse({
      this
    })
  }

  def moveCardFromGraveyardToHands(positionRef: CardPositionRef): TableState = {
    val position = positionForAlias(positionRef)
    val card = cardForPosition(position)

    players.find(p => p.nickname == position.player).map(p => p.copy(
      graveyard = p.graveyard.filterNot(c => c == p.graveyard(position.positionId)),
      hands = p.hands :+ p.graveyard(position.positionId)
    )).map(newPlayer => {
      val newTableState = this.copy(
        players = newPlayer :: players.filterNot(player => player.nickname == newPlayer.nickname)
      ).removePosition(position).addedPosition(InHandCardPosition(newPlayer.hands.size - 1, position.player))

      newTableState.copy(
        positionAliases = newTableState.positionAliases + (positionRef -> InHandCardPosition(newPlayer.hands.size - 1, position.player))
      )
    }).getOrElse({
      this
    })
  }

  def moveCardFromGraveyardToTable(positionRef: CardPositionRef): TableState = {
    val position = positionForAlias(positionRef)
    val card = cardForPosition(position)

    players.find(p => p.nickname == position.player).map(p => p.copy(
      graveyard = p.graveyard.filterNot(c => c == p.graveyard(position.positionId)),
      onTable = p.onTable :+ p.graveyard(position.positionId)
    )).map(newPlayer => {
      val newTableState = this.copy(
        players = newPlayer :: players.filterNot(player => player.nickname == newPlayer.nickname)
      ).removePosition(position).addedPosition(OnTableCardPosition(newPlayer.onTable.size - 1, position.player))

      newTableState.copy(
        positionAliases = newTableState.positionAliases + (positionRef -> OnTableCardPosition(newPlayer.onTable.size - 1, position.player))
      )
    }).getOrElse({
      this
    })
  }

  def moveCardFromHandsToTable(position: CardPosition): TableState = {
    val card = cardForPosition(position)

    players.find(p => p.nickname == position.player).map(p => p.copy(
      hands = p.hands.filterNot(c => c == p.hands(position.positionId)),
      onTable = p.onTable :+ p.hands(position.positionId)
    )).map(newPlayer => {
      val newTableState = this.copy(
        players = newPlayer :: players.filterNot(player => player.nickname == newPlayer.nickname)
      ).removePosition(position).addedPosition(OnTableCardPosition(newPlayer.onTable.size - 1, position.player))

      newTableState
    }).getOrElse({
      this
    })
  }

  def moveCardFromHandsToGraveyard(position: CardPosition): TableState = {
    val card = cardForPosition(position)

    players.find(p => p.nickname == position.player).map(p => p.copy(
      hands = p.hands.filterNot(c => c == p.hands(position.positionId)),
      graveyard = p.graveyard :+ p.hands(position.positionId)
    )).map(newPlayer => {
      val newTableState = this.copy(
        players = newPlayer :: players.filterNot(player => player.nickname == newPlayer.nickname)
      ).removePosition(position).addedPosition(OnGraveyardPosition(newPlayer.graveyard.size - 1, position.player))

      newTableState
    }).getOrElse({
      this
    })
  }

  def moveCardFromTableToGraveyard(position: CardPosition): TableState = {
    val card = cardForPosition(position)

    players.find(p => p.nickname == position.player).map(p => p.copy(
      onTable = p.onTable.filterNot(c => c == p.onTable(position.positionId)),
      graveyard = p.graveyard :+ p.onTable(position.positionId)
    )).map(newPlayer => {
      val newTableState = this.copy(
        players = newPlayer :: players.filterNot(player => player.nickname == newPlayer.nickname)
      ).removePosition(position).addedPosition(OnGraveyardPosition(newPlayer.graveyard.size - 1, position.player))

      newTableState
    }).getOrElse({
      this
    })
  }

  def moveCardFromTableToHands(position: CardPosition): TableState = {
    val card = cardForPosition(position)

    players.find(p => p.nickname == position.player).map(p => p.copy(
      onTable = p.onTable.filterNot(c => c == p.onTable(position.positionId)),
      hands = p.hands :+ p.onTable(position.positionId)
    )).map(newPlayer => {
      val newTableState = this.copy(
        players = newPlayer :: players.filterNot(player => player.nickname == newPlayer.nickname)
      ).removePosition(position).addedPosition(InHandCardPosition(newPlayer.hands.size - 1, position.player))

      newTableState
    }).getOrElse({
      this
    })
  }

  def moveCardFromGraveyardToHands(position: CardPosition): TableState = {
    val card = cardForPosition(position)

    players.find(p => p.nickname == position.player).map(p => p.copy(
      graveyard = p.graveyard.filterNot(c => c == p.graveyard(position.positionId)),
      hands = p.hands :+ p.graveyard(position.positionId)
    )).map(newPlayer => {
      val newTableState = this.copy(
        players = newPlayer :: players.filterNot(player => player.nickname == newPlayer.nickname)
      ).removePosition(position).addedPosition(InHandCardPosition(newPlayer.hands.size - 1, position.player))

      newTableState
    }).getOrElse({
      this
    })
  }

  def moveCardFromGraveyardToTable(position: CardPosition): TableState = {
    val card = cardForPosition(position)

    players.find(p => p.nickname == position.player).map(p => p.copy(
      graveyard = p.graveyard.filterNot(c => c == p.graveyard(position.positionId)),
      onTable = p.onTable :+ p.graveyard(position.positionId)
    )).map(newPlayer => {
      val newTableState = this.copy(
        players = newPlayer :: players.filterNot(player => player.nickname == newPlayer.nickname)
      ).removePosition(position).addedPosition(OnTableCardPosition(newPlayer.onTable.size - 1, position.player))

      newTableState
    }).getOrElse({
      this
    })
  }

  def chargePlayerForCard(player: String, cost: ElementsRating, neutralDistribution: NeutralDistribution): TableState = {
    this.copy(
      players = players.find(p => p.nickname == player).map(p => p.copy(
        charm = p.charm -/(cost, AutomaticNeutralDistribution(cost, neutralDistribution, p.charm))
      )).toList ::: players.filterNot(p => p.nickname == player)
    )
  }

  def addUsedCard(used: UseCard): TableState = {
    this.copy(
      usedCards = used :: usedCards
    )
  }

  def addCounterUsedCard(counterUsed: CounterUseCard): TableState = {
    this.copy(
      counterUsedCards = counterUsed :: counterUsedCards
    )
  }

  def addAttackedCard(attacked: AttackWithCard): TableState = {
    this.copy(
      attackedCards = attacked :: attackedCards
    )
  }

  def addDefendedCard(defended: DefendWithCard): TableState = {
    this.copy(
      defendedCards = defended :: defendedCards
    )
  }

  def removeUsedCardCard(position: CardPositionRef): TableState = {
    val positionToRemove = this.positionForAlias(position)
    this.copy(
      usedCards = usedCards.filterNot(uc => this.positionForAlias(uc.usage.onPosition) =? positionToRemove)
    )
  }

  def removeCounterUsedCard(position: CardPositionRef): TableState = {
    this.copy(
      counterUsedCards = counterUsedCards.filterNot(uc => uc.usage.onPosition == position)
    )
  }

  def removeAttackedCard(position: CardPositionRef): TableState = {
    this.copy(
      attackedCards = attackedCards.filterNot(uc => {
        val attackedCard = positionForAlias(uc.attack.onPosition)
        val filteredCard = positionForAlias(position)
        if(attackedCard =? filteredCard) {
          debug("Filtering " + attackedCard)
          true
        } else {
          false
        }
      })
    )
  }

  def removeDefendedCard(position: CardPositionRef): TableState = {
    this.copy(
        defendedCards = defendedCards.filterNot(uc => {
        val defendedCard = positionForAlias(uc.defend.defendWith)
        val filteredCard = positionForAlias(position)
        if(defendedCard =? filteredCard) {
          debug("Filtering " + defendedCard)
          true
        } else {
          false
        }
      })
    )
  }

  def transformDeckForPlayer(player: String, deck: Array[CardHolder]): TableState = this.copy(
    players = players.find(p => p.nickname == player).map(p => p.copy(
      deck = deck
    )).toList ::: players.filterNot(p => p.nickname == player)
  )

  def addCardsToHandForPlayer(player: String, cards: Array[CardHolder]): TableState = {

    var positions: Array[CardPosition] = Array()
    val playerPlaysMatch = players.find(p => p.nickname == player).map(p => {
      val newHands = p.hands ++ cards
      positions = newHands.zipWithIndex.map(pos => InHandCardPosition(pos._2, p.nickname))
      p.copy(
        hands = p.hands ++ cards
      )
    })

    var newTableState = this.copy(
      players = playerPlaysMatch.toList ::: players.filterNot(p => p.nickname == player)
    )
    positions.map(pos => newTableState = newTableState.addedPosition(pos))
    newTableState
  }

  def toXml = {
    <tableState>
      <playersTurn>
        {playersTurn.map(p => <player nickname={p}/>)}
      </playersTurn>

      <fortune>
        {fortuneIsOnSide.toString}
      </fortune>

      <teams>
        {startingRollPositions.groupBy(_.position).map(team => {
        val members = team._2.map(p => p.user)
        val hp = teamHp.find(f => f.members.contains(members.head)).get
        <team hp={hp.hp.toString} startedLike={team._1.toString}>
          <members>
            {members.map(m => <member>
            {m}
          </member>)}
          </members>
        </team>
      })}
      </teams>

      <players>
        {players.map(p => {
        <player nickname={p.nickname}>
          <charm fire={p.charm.fire.toString} water={p.charm.water.toString} air={p.charm.air.toString} earth={p.charm.earth.toString}/>
          <hand>
            {p.hands.map(c => <card id={c.id}></card>)}
          </hand>
          <onTable>
            {p.onTable.map(c => <card id={c.id}></card>)}
          </onTable>
          <deck>
            {p.deck.map(c => <card id={c.id}></card>)}
          </deck>
          <graveyard>
            {p.graveyard.map(c => <card id={c.id}></card>)}
          </graveyard>
        </player>
      })}
      </players>

      <usedCards>
        {usedCards.map(uc => {
        val position = positionForAlias(uc.usage.onPosition)
        <cardUsage>
          <card id={cardForPositionRef(uc.usage.onPosition).map(ch => ch.id).getOrElse("")} player={position.player} position={position match {
          case p: InHandCardPosition => "InHand"
          case p: OnTableCardPosition => "OnTable"
          case p: OnGraveyardPosition => "Graveyard"
        }} index={position.positionId.toString}/>{if (uc.usage.target.isDefined) {
          <target>
            {uc.usage.target.get match {
            case t: CardIsTarget =>
                <card id={cardForPosition(t.position).map(ch => ch.id).getOrElse("")} player={t.position.player} position={t.position match {
                case p: InHandCardPosition => "InHand"
                case p: OnTableCardPosition => "OnTable"
                case p: OnGraveyardPosition => "Graveyard"
              }} index={t.position.positionId.toString}/>

            case t: CardRefIsTarget =>
              val tPosition = positionForAlias(t.position)
                <card id={cardForPosition(tPosition).map(ch => ch.id).getOrElse("")} player={tPosition.player} position={tPosition match {
                case p: InHandCardPosition => "InHand"
                case p: OnTableCardPosition => "OnTable"
                case p: OnGraveyardPosition => "Graveyard"
              }} index={tPosition.positionId.toString}/>

            case t: PlayerIsTarget =>
                <player nickname={t.playerName}/>

            case t: PlayerTeamIsTarget =>
              t.team.map(p => <player nickname={p}></player>)
          }}
          </target>
        }}
        </cardUsage>
      })}
      </usedCards>

      <counterUsedCards>
        {counterUsedCards.map(uc => {
        val position = positionForAlias(uc.usage.onPosition)
        <cardUsage>
          <card id={cardForPosition(position).map(ch => ch.id).getOrElse("")} player={position.player} position={position match {
          case p: InHandCardPosition => "InHand"
          case p: OnTableCardPosition => "OnTable"
          case p: OnGraveyardPosition => "Graveyard"
        }} index={position.positionId.toString}/>{if (uc.usage.target.isDefined) {
          <target>
            {uc.usage.target.get match {
            case t: CardIsTarget =>
                <card id={cardForPosition(t.position).get.id} player={t.position.player} position={t.position match {
                case p: InHandCardPosition => "InHand"
                case p: OnTableCardPosition => "OnTable"
                case p: OnGraveyardPosition => "Graveyard"
              }} index={t.position.positionId.toString}/>

            case t: CardRefIsTarget =>
              val tPosition = positionForAlias(t.position)
                <card id={cardForPosition(tPosition).map(ch => ch.id).getOrElse("")} player={tPosition.player} position={tPosition match {
                case p: InHandCardPosition => "InHand"
                case p: OnTableCardPosition => "OnTable"
                case p: OnGraveyardPosition => "Graveyard"
              }} index={tPosition.positionId.toString}/>

            case t: PlayerIsTarget =>
                <player nickname={t.playerName}/>

            case t: PlayerTeamIsTarget =>
              t.team.map(p => <player nickname={p}></player>)
          }}
          </target>
        }}
        </cardUsage>
      })}
      </counterUsedCards>


      <attackingCards>
        {attackedCards.map(a => {
        val position = positionForAlias(a.attack.onPosition)
          <card id={cardForPosition(position).map(ch => ch.id).getOrElse("")} player={position.player} position={position match {
          case p: InHandCardPosition => "InHand"
          case p: OnTableCardPosition => "OnTable"
          case p: OnGraveyardPosition => "Graveyard"
        }} index={position.positionId.toString}/>
      })}
      </attackingCards>

      <defendingCards>
        {defendedCards.map(d => {
        val dPosition = positionForAlias(d.defend.defendWith)
        <defending>
          <card id={cardForPosition(dPosition).map(ch => ch.id).getOrElse("")} player={dPosition.player} position={dPosition match {
          case p: InHandCardPosition => "InHand"
          case p: OnTableCardPosition => "OnTable"
          case p: OnGraveyardPosition => "Graveyard"
        }} index={dPosition.positionId.toString}/>
          <target>
            {d.defend.against match {
            case t: CardIsTarget =>
                <card id={cardForPosition(t.position).map(ch => ch.id).getOrElse("")} player={t.position.player} position={t.position match {
                case p: InHandCardPosition => "InHand"
                case p: OnTableCardPosition => "OnTable"
                case p: OnGraveyardPosition => "Graveyard"
              }} index={t.position.positionId.toString}/>

            case t: CardRefIsTarget =>
              val tPosition = positionForAlias(t.position)
                <card id={cardForPosition(tPosition).map(ch => ch.id).getOrElse("")} player={tPosition.player} position={tPosition match {
                case p: InHandCardPosition => "InHand"
                case p: OnTableCardPosition => "OnTable"
                case p: OnGraveyardPosition => "Graveyard"
              }} index={tPosition.positionId.toString}/>

            case t: PlayerIsTarget =>
                <player nickname={t.playerName}/>

            case t: PlayerTeamIsTarget =>
              t.team.map(p => <player nickname={p}></player>)


          }}
          </target>
        </defending>
      })}
      </defendingCards>
    </tableState>
  }
}

object TransformCardIsTargetToRef {
  def apply(target: CardTarget, tableState: TableState): Pair[TableState, CardTarget] = {
    target match {
      case t: CardIsTarget =>
        val newAliasPair = tableState.createNewAliasForPosition(t.position)
        newAliasPair._1 -> CardRefIsTarget(newAliasPair._2)
      case t => tableState -> t
    }
  }
}

object TransformRefToCardIsTarget {
  def apply(target: CardTarget, tableState: TableState): CardTarget = {
    target match {
      case t: CardRefIsTarget =>
        CardIsTarget(tableState.positionForAlias(t.position))
      case t => t
    }
  }
}


object MergeMatchTableStateAndDivergedTableState {
  def apply(pair: Pair[TableState, TableState]): TableState = {
    pair._2.copy(
      players = pair._2.players.map(p => {
        pair._1.players.find(pf => pf.nickname == p.nickname).map(found => {
          p.copy(
            connected = found.connected
          )
        }).getOrElse(p)
      })
    )
  }
}


object CardPositionAliasesReindex {
  def apply(aliases: HashMap[CardPositionRef, CardPosition], oldPosition: CardPosition, newPosition: CardPosition): Map[CardPositionRef, CardPosition] = {
    (aliases.filter(p => p._2 =? oldPosition).map(p => p._1 -> newPosition) ++ aliases.filter(p => p._2 != oldPosition))
  }
}