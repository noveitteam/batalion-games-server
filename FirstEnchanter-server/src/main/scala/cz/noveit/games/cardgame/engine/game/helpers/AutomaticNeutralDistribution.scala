package cz.noveit.games.cardgame.engine.game.helpers

import cz.noveit.games.cardgame.adapters.ElementsRating
import cz.noveit.games.cardgame.engine.card.CardHolder
import cz.noveit.games.cardgame.engine.player.{CharmOfElements, NeutralDistribution}

import scala.util.Random

/**
 * Created by arnostkuchar on 19.11.14.
 */
object AutomaticNeutralDistribution {
  def apply(price: ElementsRating, neutralDistribution: NeutralDistribution, charm: CharmOfElements): NeutralDistribution = {
    if (neutralDistribution =? NeutralDistribution.ZERO) {
      val afterPriceCost = charm -/(price, neutralDistribution)

      if (afterPriceCost.size >= price.neutral) {
        var temp = afterPriceCost.flattenToElementalCost
        var result = NeutralDistribution(0, 0, 0, 0)

        for (i <- 1 to price.neutral.toInt) {
          val index = Random.nextInt(temp.size)
          val value = temp(index)
          temp = temp.filter(f => f != value)
          result = NeutralDistribution(
            result.fire + value.fire.toInt,
            result.water + value.water.toInt,
            result.earth + value.earth.toInt,
            result.air + value.air.toInt
          )
        }
        result
      } else {
        neutralDistribution
      }
    } else {
      neutralDistribution
    }
  }
}
