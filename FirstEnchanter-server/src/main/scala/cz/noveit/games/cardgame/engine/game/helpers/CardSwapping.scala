package cz.noveit.games.cardgame.engine.game.helpers

import cz.noveit.games.cardgame.adapters.ElementsRating
import cz.noveit.games.cardgame.engine.card.CardHolder
import cz.noveit.games.cardgame.engine.player.{CharmOfElements, NeutralDistribution}

/**
 * Created by Wlsek on 1.9.2014.
 */

case class CardSwappingRequirements(numberOfCards: Int, numberOfElements: Int)


trait CardSwapping {

  val cardSwappingRequirements: CardSwappingRequirements

  def howManyCardSwappingPossible_?(availableCardsSize: Int, charm: CharmOfElements): Int = {
    var division: Int = availableCardsSize / cardSwappingRequirements.numberOfCards
    if (charm.total / cardSwappingRequirements.numberOfElements < division) division = charm.total / cardSwappingRequirements.numberOfElements
    division
  }

  def isSwappingPossibleWithThisDistribution_?(availableCardsSize: Int, swappedCardsSize: Int, charm: CharmOfElements, neutralDistribution: NeutralDistribution): Boolean = {
    (charm -/(ElementsRating(0, 0, 0, 0, 0), neutralDistribution)).affordAble_? && (availableCardsSize >= swappedCardsSize) && howManyCardSwappingPossible_?(swappedCardsSize, charm) > 1
  }
}
