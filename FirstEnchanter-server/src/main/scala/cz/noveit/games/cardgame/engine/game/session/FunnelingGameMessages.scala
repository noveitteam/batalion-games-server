package cz.noveit.games.cardgame.engine.game.session

import akka.actor.ActorRef

/**
 * Created by Wlsek on 26.8.2014.
 */
object FunnelingGameMessagesLock {
  def apply(lockPlayer: String, initialMatchData: MatchStateWaitingForPlayerEndTurn, sender: ActorRef, gameMessage: GameMessages, action: (MatchStateWaitingForPlayerEndTurn) => MatchStateWaitingForPlayerEndTurn): MatchStateWaitingForPlayerEndTurn = {
    if(initialMatchData.tableOwnerLock.isDefined) {
      initialMatchData.copy(
        cachedTableRequests = sender -> gameMessage :: initialMatchData.cachedTableRequests
      )
    } else {
      action(initialMatchData.copy(tableOwnerLock = Some(lockPlayer)))
    }
  }
}


object FunnelingGameMessagesUnlock {
  def apply(notify: ActorRef, initialMatchData: MatchStateWaitingForPlayerEndTurn, action: (MatchStateWaitingForPlayerEndTurn) => MatchData = (md: MatchStateWaitingForPlayerEndTurn) => md): MatchStateWaitingForPlayerEndTurn = {
    val toChain = if(initialMatchData.tableOwnerLock.isDefined) {
      initialMatchData.copy(tableOwnerLock =  None)
    } else {
      initialMatchData
    }

    if(!toChain.cachedTableRequests.isEmpty) {
      val reversed = toChain.cachedTableRequests.reverse
      notify.tell(reversed.head._2, reversed.head._1)
      toChain.copy(cachedTableRequests = reversed.tail.reverse)
    } else {
      toChain
    }
  }
}