package cz.noveit.games.cardgame.engine.game.session

import akka.actor.ActorRef
import cz.noveit.games.cardgame.engine.CrossHandlerMessage
import cz.noveit.games.cardgame.engine.bots.BotAction
import cz.noveit.games.cardgame.engine.card._
import cz.noveit.games.cardgame.engine.game.TableState
import cz.noveit.games.cardgame.engine.game.helpers.CardSwappingRequirements
import cz.noveit.games.cardgame.engine.player.{NeutralDistribution, CharmOfElements}
import cz.noveit.games.cardgame.recording.XmlRecording

import scala.collection.immutable.HashMap
import scala.concurrent.duration.FiniteDuration

/**
 * Created by Wlsek on 26.8.2014.
 */

trait GameMessages

/* --------------------------------------- Initialization messages ------------------------------------------ */
case class CannotComposePlayer(player: String) extends CrossHandlerMessage with GameMessages

/* --------------------------------------- Game Synchronize ------------------------------------------- */

case class StateAsk(state: State, tableState: TableState, timInState: Long, timeAnchor: Long) extends CrossHandlerMessage with GameMessages

case class StateAck(state: State, username: String) extends CrossHandlerMessage with GameMessages

/* ----------------------------------------------------------------------------------------------------- */

case class PlayerConnectsToMatch(playerName: String, handler: ActorRef) extends CrossHandlerMessage with GameMessages

case class PlayerDisconnectedFromMatch(playerName: String) extends CrossHandlerMessage with GameMessages


/* ----------------------------------------- Player roll messages -------------------------------------- */

trait PlayerRollPosition

case object PLAYER_ROLL_POSITION_ATTACKER extends PlayerRollPosition

case object PLAYER_ROLL_POSITION_DEFENDER extends PlayerRollPosition

case class AskPlayerRoll(position: PlayerRollPosition) extends CrossHandlerMessage with GameMessages

case class PlayerRoll(user: String, number: Int, position: PlayerRollPosition) extends CrossHandlerMessage with GameMessages

case class PlayerRollResult(winning: List[PlayerRoll], loosing: List[PlayerRoll]) extends CrossHandlerMessage with GameMessages with XmlRecording {
  def toXml = <playerRollResult>
    <winners>
      {winning.map(w => <player nickname={w.user} position={w.position.toString} rolled={w.number.toString}></player>)}
    </winners>
    <loosers>
      {loosing.map(w => <player nickname={w.user} position={w.position.toString} rolled={w.number.toString}></player>)}
    </loosers>
  </playerRollResult>
}

case object RollEndsByTime

/* -------------------------------------- Beggining ------------------------------------------------------------- */


case class CardWithPosition(card: CardHolder, position: Int)

case class PlayerDrawCards(player: String, id: List[CardWithPosition]) extends CrossHandlerMessage with GameMessages

case class PlayerGetsElements(player: String, addedElements: CharmOfElements) extends CrossHandlerMessage with GameMessages

/* -------------------------------------- Attacker player use card messages ------------------------------------- */

case class AskAttackerToUseCard(possibleUses: List[AttackerUsesCard]) extends CrossHandlerMessage with GameMessages

case object AskDefenderToWait extends CrossHandlerMessage with GameMessages

case class AttackerUsesCard(onPosition: CardPosition, target: Option[CardTarget] = None, card: CardHolder, neutralDistribution: NeutralDistribution = NeutralDistribution(0, 0, 0, 0), changes: List[TableStateChange] = List()) extends CrossHandlerMessage with GameMessages with BotAction with XmlRecording {
  def toXml = <cardUsage>
    <card id={card.id} player={onPosition.player} position={onPosition match {
    case p: InHandCardPosition => "InHand"
    case p: OnTableCardPosition => "OnTable"
    case p: OnGraveyardPosition => "Graveyard"
  }} index={onPosition.positionId.toString}/>{if (target.isDefined) {
      <target>
        {target.get match {
        case t: CardIsTarget =>
            <card player={t.position.player} position={t.position match {
            case p: InHandCardPosition => "InHand"
            case p: OnTableCardPosition => "OnTable"
            case p: OnGraveyardPosition => "Graveyard"
          }} index={t.position.positionId.toString}/>

        case t: PlayerIsTarget =>
            <player nickname={t.playerName}/>

        case t: PlayerTeamIsTarget =>
          t.team.map(p => <player nickname={p}></player>)

      }}
      </target>
    }}<neutralDistribution fire={neutralDistribution.fire.toString} water={neutralDistribution.water.toString} air={neutralDistribution.air.toString} earth={neutralDistribution.earth.toString}></neutralDistribution>
  </cardUsage>
}

case class AttackerUsedCardAccepted(position: CardPosition, target: Option[CardTarget] = None, neutralDistribution: NeutralDistribution, possibleUsages: List[AttackerUsesCard], card: CardHolder, changes: List[TableStateChange] = List()) extends CrossHandlerMessage with GameMessages

case class AttackerUsedCardDeclined(position: CardPosition, target: Option[CardTarget] = None, neutralDistribution: NeutralDistribution) extends CrossHandlerMessage with GameMessages

case class AttackerSwapsCards(onPosition: List[CardPosition], neutralDistribution: NeutralDistribution) extends CrossHandlerMessage with GameMessages with XmlRecording {
  def toXml = <cardSwap>
    {onPosition.map(op =>
        <card player={op.player} position={op match {
        case p: InHandCardPosition => "InHand"
        case p: OnTableCardPosition => "OnTable"
        case p: OnGraveyardPosition => "Graveyard"
      }} index={op.positionId.toString}/>
    )}<neutralDistribution fire={neutralDistribution.fire.toString} water={neutralDistribution.water.toString} air={neutralDistribution.air.toString} earth={neutralDistribution.earth.toString}></neutralDistribution>
  </cardSwap>
}

case class AttackerSwapsCardsAccepted(onPosition: List[CardPosition], neutralDistribution: NeutralDistribution, changes: List[TableStateChange]) extends CrossHandlerMessage with GameMessages

case class AttackerSwapsCardsDeclined(onPosition: List[CardPosition], neutralDistribution: NeutralDistribution) extends CrossHandlerMessage with GameMessages

case class AttackerSwappedCards(onPosition: List[CardPosition], neutralDistribution: NeutralDistribution) extends CrossHandlerMessage with GameMessages

case object AttackerTurnEndsByTime extends CrossHandlerMessage with GameMessages

case class AttackerEndsTurn(user: String) extends CrossHandlerMessage with GameMessages

case class AttackerAsksPossibleCardUses(player: String) extends CrossHandlerMessage with GameMessages

case class AttackerPossibleCardUses(possibleUses: List[AttackerUsesCard]) extends CrossHandlerMessage with GameMessages

/* -------------------------------------- Defender player counter-plays ------------------------------------- */

case object AskAttackerToWait extends CrossHandlerMessage with GameMessages

case class AskDefenderToUseCard(possibleUses: List[DefenderUsesCard]) extends CrossHandlerMessage with GameMessages

case class DefenderUsesCard(onPosition: CardPosition, target: Option[CardTarget] = None, card: CardHolder, neutralDistribution: NeutralDistribution = NeutralDistribution(0, 0, 0, 0), changes: List[TableStateChange] = List()) extends CrossHandlerMessage with GameMessages with BotAction with XmlRecording {
  def toXml = <cardUsage>
    <card id={card.id} player={onPosition.player} position={onPosition match {
    case p: InHandCardPosition => "InHand"
    case p: OnTableCardPosition => "OnTable"
    case p: OnGraveyardPosition => "Graveyard"
  }} index={onPosition.positionId.toString}/>{if (target.isDefined) {
      <target>
        {target.get match {
        case t: CardIsTarget =>
            <card player={t.position.player} position={t.position match {
            case p: InHandCardPosition => "InHand"
            case p: OnTableCardPosition => "OnTable"
            case p: OnGraveyardPosition => "Graveyard"
          }} index={t.position.positionId.toString}/>

        case t: PlayerIsTarget =>
            <player nickname={t.playerName}/>

        case t: PlayerTeamIsTarget =>
          t.team.map(p => <player nickname={p}></player>)


      }}
      </target>
    }}<neutralDistribution fire={neutralDistribution.fire.toString} water={neutralDistribution.water.toString} air={neutralDistribution.air.toString} earth={neutralDistribution.earth.toString}></neutralDistribution>
  </cardUsage>
}

case class DefenderUsedCardAccepted(position: CardPosition, target: Option[CardTarget] = None, neutralDistribution: NeutralDistribution, possibleUses: List[DefenderUsesCard], card: CardHolder, changes: List[TableStateChange] = List()) extends CrossHandlerMessage with GameMessages

case class DefenderUsedCardDeclined(position: CardPosition, target: Option[CardTarget] = None, neutralDistribution: NeutralDistribution) extends CrossHandlerMessage with GameMessages

case object DefenderTurnEndsByTime

case class DefenderEndsTurn(user: String) extends CrossHandlerMessage with GameMessages

case class DefenderAsksPossibleCardUses(player: String) extends CrossHandlerMessage with GameMessages

case class DefenderPossibleCardUses(possibleUses: List[DefenderUsesCard]) extends CrossHandlerMessage with GameMessages

/* -------------------------------------- Ally player attacking ------------------------------------------- */

case class AskAttackerToAttack(possibleAttacks: List[AttackerAttacks]) extends CrossHandlerMessage with GameMessages

case object AskDefenderToWaitForAttack extends CrossHandlerMessage with GameMessages

case class AttackerAttacks(withPosition: CardPosition, card: CardHolder, changes: List[TableStateChange] = List()) extends CrossHandlerMessage with GameMessages with BotAction with XmlRecording {
  def toXml = <attacking id={card.id} player={withPosition.player} position={withPosition match {
    case p: InHandCardPosition => "InHand"
    case p: OnTableCardPosition => "OnTable"
    case p: OnGraveyardPosition => "Graveyard"
  }} index={withPosition.positionId.toString}/>

}

case class AttackAccepted(position: CardPosition, possibleAttacks: List[AttackerAttacks], card: CardHolder, changes: List[TableStateChange] = List()) extends CrossHandlerMessage with GameMessages

case class AttackDeclined(position: CardPosition) extends CrossHandlerMessage with GameMessages

case class AskPossibleAttacks(player: String) extends CrossHandlerMessage with GameMessages

case class PossibleAttacks(attacks: List[AttackerAttacks]) extends CrossHandlerMessage with GameMessages

//case class CancelAttack(attack: AttackerAttacks)  extends CrossHandlerMessage

//case object AttackCancelAccepted  extends CrossHandlerMessage

//case class AttacksState(attacks: List[AttackerAttacks])  extends CrossHandlerMessage

/* --------------------------------------- Defender player defends ---------------------------------------- */

case object AskAttackerToWaitForDefend extends CrossHandlerMessage with GameMessages

case class AskDefenderToDefend(defends: List[DefenderDefends]) extends CrossHandlerMessage with GameMessages

case class DefenderDefends(withPosition: CardPosition, againts: CardTarget, defenderCard: CardHolder, neutralDistribution: NeutralDistribution = NeutralDistribution(0,0,0,0), changes: List[TableStateChange] = List()) extends CrossHandlerMessage with GameMessages with BotAction with XmlRecording {
  def toXml = <defending>
    <card id={defenderCard.id} player={withPosition.player} position={withPosition match {
    case p: InHandCardPosition => "InHand"
    case p: OnTableCardPosition => "OnTable"
    case p: OnGraveyardPosition => "Graveyard"
  }} index={withPosition.positionId.toString}/>
    <target>
      {againts match {
      case t: CardIsTarget =>
          <card player={t.position.player} position={t.position match {
          case p: InHandCardPosition => "InHand"
          case p: OnTableCardPosition => "OnTable"
          case p: OnGraveyardPosition => "Graveyard"
        }} index={t.position.positionId.toString}/>

      case t: PlayerIsTarget =>
          <player nickname={t.playerName}/>

      case t: PlayerTeamIsTarget =>
        t.team.map(p => <player nickname={p}></player>)


    }}
    </target>
  </defending>
}

case class DefendingAccepted(position: CardPosition, attacker: CardTarget, possibleDefends: List[DefenderDefends], defenderCard: CardHolder,  neutralDistribution: NeutralDistribution = NeutralDistribution(0,0,0,0), changes: List[TableStateChange] = List()) extends CrossHandlerMessage with GameMessages

case class DefendingDeclined(position: CardPosition, attacker: CardTarget) extends CrossHandlerMessage with GameMessages

case class AskPossibleDefending(player: String) extends CrossHandlerMessage with GameMessages

case class PossibleDefending(defends: List[DefenderDefends]) extends CrossHandlerMessage with GameMessages

/* ---------------------------------------- Evaluation and animation --------------------------------------- */

case class AnimationDone(player: String) extends CrossHandlerMessage with GameMessages

case object AnimationStateDoneByTimer


/* --------------------------------------- Config classes --------------------------------------------------- */

case class StartingRollPosition(user: String, position: PlayerRollPosition)

case class MatchSetup(turnTimers: HashMap[State, FiniteDuration], incomingEssencesPerRound: Int, limitEssencePool: Int, maxCardsInHand: Int, defaultArena: String, teamHp: Int, cardSwappingReq: CardSwappingRequirements) {
  def toXml =
    <matchSetup>
      <timers>
        {turnTimers.map(t => <timer name={t._1.toString} milis={t._2.toMillis.toString}/>)}
      </timers>
      <incomingEssencesPerRoud>
        {incomingEssencesPerRound.toString}
      </incomingEssencesPerRoud>
      <limitEssencePool>
        {limitEssencePool.toString}
      </limitEssencePool>
      <maxCardsInHand>
        {maxCardsInHand.toString}
      </maxCardsInHand>
      <defaultArena>
        {defaultArena}
      </defaultArena>
      <teamHp>
        {teamHp.toString}
      </teamHp>
    </matchSetup>
}
