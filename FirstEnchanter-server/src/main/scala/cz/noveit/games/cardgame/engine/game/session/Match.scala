package cz.noveit.games.cardgame.engine.game.session

import java.sql.Timestamp
import java.util.Date

import akka.actor._
import cz.noveit.database.{DatabaseCommand, DatabaseResult}
import cz.noveit.games.cardgame.WorldServices
import cz.noveit.games.cardgame.adapters.{Card, Deck, DeckAndCardsForUserWithGivenDeckId, ElementsRating, GetDeckAndCardsForUserWithGivenDeckId}
import cz.noveit.games.cardgame.engine.ServiceModuleMessage
import cz.noveit.games.cardgame.engine.card.{AskPossibleAttackPostionsForPlayer, AskPossibleCounterUsageForPlayer, AskPossibleDefendingPositionsForPlayer, AskPossibleUsageForPlayer, AttackWithCard, AutomaticPossibilityAutomaticBehaviorAfterAttack, AutomaticPossibilityAutomaticBehaviorAfterCounterUse, AutomaticPossibilityAutomaticBehaviorAfterDefends, AutomaticPossibilityAutomaticBehaviorAfterUse, CannotAttackWithCard, CannotCounterUseCard, CannotUseCard, CounterUseCard, DefendWithCard, EvaluateAttack, EvaluateAttackDone, EvaluateUsage, EvaluateUsageDone, InHandCardPosition, ParametrizedAttack, ParametrizedDefense, ParametrizedUsage, PossibleAttacksPositions, PossibleCounterUsages, PossibleDefendPositions, PossibleUsages, TableStateAfterParametrizedAttack, TableStateAfterParametrizedCounterUsage, TableStateAfterParametrizedDefense, TableStateAfterParametrizedUsage, UseCard, _}
import cz.noveit.games.cardgame.engine.card.helpers.generator.GenerateCards
import cz.noveit.games.cardgame.engine.game._
import cz.noveit.games.cardgame.engine.game.helpers.CardSwapping
import cz.noveit.games.cardgame.engine.handlers.{CardDatabaseAdapterFound, GetPlayerInMatchHandler, PlayerInMatchHandlerFound, _}
import cz.noveit.games.cardgame.engine.player.{CharmOfElements, CharmOfElementsRatio, PlayerPlaysMatch, PlayerPlaysWithActiveDeck}
import cz.noveit.games.cardgame.recording.{RecordAttackerUsesCard, RecordNewRound, RecordRollResult, _}
import org.joda.time.{DateTimeZone, DateTime}

import scala.collection.immutable.HashMap
import scala.concurrent.duration._
import scala.util.Random

/**
 * Created by Wlsek on 6.2.14.
 */

class Match(
             val startingSetup: MatchQuerySetup,
             val setup: MatchSetup,
             val startingRollPositions: List[StartingRollPosition],
             val activeDecks: List[PlayerPlaysWithActiveDeck]
             )
  extends Actor with ActorLogging
  with FSM[State, MatchData]
  with GameEndController
  with CardPositionToCardHolder
  with CardSwapping {

  log.info("| MATCH [{}] INITIALIZATING", {
    var str: String = "";
    startingSetup.players.map(p => str = str + "-" + p.player);
    str.substring(1, str.size - 1)
  })

  val matchResultController = context.actorSelection("/user/" + WorldServices.matchResultController)
  val cardController = context.actorOf(Props(classOf[CardController], self))
  val recording = context.actorOf(Props(classOf[Recording], setup, startingSetup))
  val cardSwappingRequirements = setup.cardSwappingReq


  val random = new Random()

  val enemyMode = startingSetup.enemyMode
  val matchMode = startingSetup.mode
  val arena: String = startingSetup.arenaId.map(a => a).getOrElse(setup.defaultArena)
  var teamHp: List[TeamHp] = List()
  val requiredPlayers = startingSetup.players.size

  val attackersDefenders = startingRollPositions.partition(p => p.position == PLAYER_ROLL_POSITION_ATTACKER)
  teamHp = TeamHp(attackersDefenders._1.map(p => p.user), setup.teamHp) :: TeamHp(attackersDefenders._2.map(p => p.user), setup.teamHp) :: Nil

  val conversation = context.actorOf(Props(classOf[MatchSessionConversation],
    context.actorSelection("/user/conversationController"),
    attackersDefenders._1.map(p => MatchConversationMember(p.user, startingSetup.isBot(p.user))),
    attackersDefenders._2.map(p => MatchConversationMember(p.user, startingSetup.isBot(p.user)))
  ))

  startingSetup.players.map(p => {
    val actor = context.actorOf(Props(classOf[PlayerComposer], self, p.replyTo, p.player, activeDecks.find(d => d.player.equals(p.player)).get.deckId))
  })

  startWith(INITIALIZING_MATCH, MatchStateWaitingForInitialization(TableState(
    List(), List(), arena, PLAYER_ROLL_POSITION_ATTACKER, startingRollPositions, teamHp, List(), List(), List(), List(), HashMap(), 1, TableLimits(setup.limitEssencePool, setup.maxCardsInHand)
  )))

  when(STATE_ACK, stateTimeout = 15 seconds) {
    case Event(StateAck(ackState, user), MatchStateWaitingForAck(originalState, actualState, newMatchData, ackUserExpected)) => {
      log.info("| StateAck | Camed from {}", user)
      if (ackUserExpected.size > 1) {
        stay using (MatchStateWaitingForAck(originalState, actualState, newMatchData, ackUserExpected.filterNot(u => u.equals(user))))
      } else {
        log.info("| StateAck | changing state to {}", actualState)
        goto(actualState) using (newMatchData)
      }
    }

    case Event(StateTimeout, MatchStateWaitingForAck(originalState, actualState, newMatchData, ackUserExpected)) => {
      goto(actualState) using (
        newMatchData match {
          case m: MatchStateWaitingForPlayerEndTurn =>
            if (ackUserExpected.size > 1) {
              ackUserExpected.map(ep => {
                m.tableState.players.map(p => p.handler ! ServiceModuleMessage(PlayerDisconnectedFromMatch(ep), ServiceHandlersModules.cardGameMatch))
              })

              m.copy(
                tableState = m.tableState.copy(players = m.tableState.players
                  .filter(p => ackUserExpected.contains(p.nickname))
                  .map(p => p.copy(connected = false)) ::: m.tableState.players.filter(p => !ackUserExpected.contains(p.nickname))
                )
              )
            } else {
              m
            }
          case m: MatchStateWaitingForPlayerRolls =>
            if (ackUserExpected.size > 1) {
              ackUserExpected.map(ep => {
                m.tableState.players.map(p => p.handler ! ServiceModuleMessage(PlayerDisconnectedFromMatch(ep), ServiceHandlersModules.cardGameMatch))
              })

              m.copy(
                tableState = m.tableState.copy(players = m.tableState.players
                  .filter(p => ackUserExpected.contains(p.nickname))
                  .map(p => p.copy(connected = false)) ::: m.tableState.players.filter(p => !ackUserExpected.contains(p.nickname))
                )
              )
            } else {
              m
            }
          case m: MatchStateWaitingForAnimationEnds =>
            if (ackUserExpected.size > 1) {
              ackUserExpected.map(ep => {
                m.tableState.players.map(p => p.handler ! ServiceModuleMessage(PlayerDisconnectedFromMatch(ep), ServiceHandlersModules.cardGameMatch))
              })

              m.copy(
                tableState = m.tableState.copy(players = m.tableState.players
                  .filter(p => ackUserExpected.contains(p.nickname))
                  .map(p => p.copy(connected = false)) ::: m.tableState.players.filter(p => !ackUserExpected.contains(p.nickname))
                )
              )
            } else {
              m
            }
        }
        )
    }
  }

  when(INITIALIZING_MATCH) {
    case Event(msg: PlayerPlaysMatch, state: MatchStateWaitingForInitialization) => {
      //   val newState = MatchStateWaitingForPlayerRolls(state.tableState.copy(players = msg :: state.tableState.players), List())
      val newTableState = state.tableState.copy(players = msg :: state.tableState.players)
      log.info("INIT - {} : {}", state.tableState.players.size, requiredPlayers)
      if (newTableState.players.size == requiredPlayers) {
        goto(STATE_ACK) using MatchStateWaitingForAck(
          INITIALIZING_MATCH,
          PLAYER_STARTS_ROLL,
          MatchStateWaitingForPlayerRolls(newTableState, List()),
          newTableState.players.map(f => f.nickname)
        )
      } else {
        stay using state.copy(tableState = newTableState)
      }
    }

    case Event(msg: CannotComposePlayer, ts: TableState) => {
      startingSetup.players.map(p => p.replyTo ! ServiceModuleMessage(msg, ServiceHandlersModules.cardGameMatch))
      log.info("Stopping match handler.")
      stop()
    }
  }

  when(PLAYER_STARTS_ROLL) {
    case Event(msg: PlayerRoll, matchData: MatchStateWaitingForPlayerRolls) => {
      if (matchData.rolls.size < requiredPlayers - 1) {
        stay using (matchData.copy(rolls = msg :: matchData.rolls))
      } else {
        val tableStateWithRolls = matchData.copy(rolls = msg :: matchData.rolls)
        var computedPair = (0.0, 0.0)

        log.info("Rolls: {}", tableStateWithRolls.rolls)

        tableStateWithRolls.tableState.players.map(p => tableStateWithRolls.rolls.find(r => r.user.equals(p.nickname)).map(roll => {
          roll.position match {
            case PLAYER_ROLL_POSITION_ATTACKER => computedPair = (computedPair._1 + roll.number, computedPair._2)
            case PLAYER_ROLL_POSITION_DEFENDER => computedPair = (computedPair._1, computedPair._2 + roll.number)
          }
        }))

        val attackersDefenders = tableStateWithRolls.rolls.partition(p => p.position == PLAYER_ROLL_POSITION_ATTACKER)
        computedPair = (computedPair._1 / attackersDefenders._1.size, computedPair._2 / attackersDefenders._2.size)

        log.info("Attackers {} | Defenders {}", attackersDefenders._1, attackersDefenders._2)
        computedPair = Math.round(computedPair._1).toDouble -> Math.round(computedPair._2).toDouble

        val defValues =
          (if ((computedPair._2 - 1) <= 0) {
            6
          } else {
            computedPair._2 - 1
          }) ::
            computedPair._2 ::
            (if ((computedPair._2 + 1) > 6) {
              1
            } else {
              computedPair._2 + 1
            }) ::
            Nil


        var playersTurn: List[String] = List()

        if (!defValues.contains(computedPair._1)) {
          recording ! RecordRollResult(PlayerRollResult(attackersDefenders._1, attackersDefenders._2))
          tableStateWithRolls.tableState.players.filter(p => p.connected == true).map(f => f.handler ! ServiceModuleMessage(PlayerRollResult(attackersDefenders._1, attackersDefenders._2), ServiceHandlersModules.cardGameMatch))
          playersTurn = attackersDefenders._1.map(p => p.user).toList
        } else {
          recording ! RecordRollResult(PlayerRollResult(attackersDefenders._2, attackersDefenders._1))
          tableStateWithRolls.tableState.players.filter(p => p.connected == true).map(f => f.handler ! ServiceModuleMessage(PlayerRollResult(attackersDefenders._2, attackersDefenders._1), ServiceHandlersModules.cardGameMatch))
          playersTurn = attackersDefenders._2.map(p => p.user).toList
        }

        val table = addElements(addCardsFromDeckToHands(tableStateWithRolls.tableState.copy(playersTurn = playersTurn)))
        goto(STATE_ACK) using (MatchStateWaitingForAck(INITIALIZING_MATCH, ATTACKER_TEAM_PLAY, MatchStateWaitingForPlayerEndTurn(None, table, List(), table.playersTurn), tableStateWithRolls.tableState.players.filter(p => p.connected == true).map(f => f.nickname)))
      }
    }

    case Event(RollEndsByTime, matchData: MatchStateWaitingForPlayerRolls) => {

      val playersRolled: List[PlayerRoll] = matchData.rolls

      var computedPair = (0.0, 0.0)

      playersRolled.map(roll => {
        roll.position match {
          case PLAYER_ROLL_POSITION_ATTACKER => computedPair = (computedPair._1 + roll.number, computedPair._2)
          case PLAYER_ROLL_POSITION_DEFENDER => computedPair = (computedPair._1, computedPair._2 + roll.number)
        }
      })

      val attackersDefenders = playersRolled.partition(p => p.position == PLAYER_ROLL_POSITION_ATTACKER)
      computedPair = (computedPair._1 / attackersDefenders._1.size, computedPair._2 / attackersDefenders._2.size)

      computedPair = Math.round(computedPair._1).toDouble -> Math.round(computedPair._2).toDouble

      val defValues =
        (if ((computedPair._2 - 1) <= 0) {
          6
        } else {
          computedPair._2 - 1
        }) ::
          computedPair._2 ::
          (if ((computedPair._2 + 1) > 6) {
            1
          } else {
            computedPair._2 + 1
          }) ::
          Nil

      var playersTurn: List[String] = List()

      if (!defValues.contains(computedPair._1)) {
        recording ! RecordRollResult(PlayerRollResult(attackersDefenders._1, attackersDefenders._2))
        matchData.tableState.players.filter(p => p.connected == true).map(f => f.handler ! ServiceModuleMessage(PlayerRollResult(attackersDefenders._1, attackersDefenders._2), ServiceHandlersModules.cardGameMatch))
        playersTurn = attackersDefenders._1.map(p => p.user).toList
      } else {
        recording ! RecordRollResult(PlayerRollResult(attackersDefenders._2, attackersDefenders._1))
        matchData.tableState.players.filter(p => p.connected == true).map(f => f.handler ! ServiceModuleMessage(PlayerRollResult(attackersDefenders._2, attackersDefenders._1), ServiceHandlersModules.cardGameMatch))
        playersTurn = attackersDefenders._2.map(p => p.user).toList
      }

      val table = addElements(addCardsFromDeckToHands(matchData.tableState.copy(playersTurn = playersTurn)))
      goto(STATE_ACK) using (MatchStateWaitingForAck(INITIALIZING_MATCH, ATTACKER_TEAM_PLAY, MatchStateWaitingForPlayerEndTurn(None, table, List(), table.playersTurn), table.players.filter(p => p.connected == true).map(f => f.nickname)))
    }
  }

  when(ATTACKER_TEAM_PLAY) {
    case Event(msg: AttackerSwapsCards, matchData: MatchStateWaitingForPlayerEndTurn) => {
      val afterLock = FunnelingGameMessagesLock(msg.onPosition.head.player, matchData, sender, msg, (md) => {
        val ts = md.tableState
        if (msg.onPosition.filter(f => !ts.playersTurn.contains(f.player)).isEmpty && msg.onPosition.size > 0) {
          ts.players.find(p => p.nickname == msg.onPosition.head.player).map(player => {
            if (
              isSwappingPossibleWithThisDistribution_?(player.hands.size, msg.onPosition.size, player.charm, msg.neutralDistribution)
            ) {

              val cards = GenerateCards(player.deck, msg.onPosition.size)
              val ids = msg.onPosition.map(op => op.positionId)
              val clearedHands: Array[CardHolder] = player.hands.zipWithIndex.filter(zipped => !ids.contains(zipped._2)).map(zipped => zipped._1).toArray
              val newHands = cards._2 ++ clearedHands
              var changes: List[TableStateChange] = List()

              newHands.zipWithIndex.map(zipped => {
                if (cards._2.contains(zipped._1)) {
                  changes = CardGenerated(InHandCardPosition(zipped._2, player.nickname), zipped._1) :: changes
                }
              })

              val filteredHands: Array[CardHolder] = player.hands.zipWithIndex.filter(zipped => ids.contains(zipped._2)).map(zipped => zipped._1).toArray

              val newTableState = ts.copy(
                players = player.copy(
                  hands = newHands,
                  graveyard = player.graveyard ++ filteredHands,
                  deck = cards._1
                ) :: ts.players.filterNot(p => p.nickname == player.nickname)
              )
              /*
                            ts.players.filter(p => ts.playersTurn.contains(p.nickname)).map(playerPlaying => {
                              playerPlaying.handler ! ServiceModuleMessage(AttackerSwapsCardsAccepted(msg.onPosition, msg.neutralDistribution, changes), ServiceHandlersModules.cardGameMatch)
                            })

                            ts.players.filterNot(p => ts.playersTurn.contains(p.nickname)).map(playerPlaying => {
                              playerPlaying.handler ! ServiceModuleMessage(AttackerSwappedCards(msg.onPosition, msg.neutralDistribution), ServiceHandlersModules.cardGameMatch)
                            })
              */

              cardController ! AskPossibleUsageForPlayer(
                newTableState.playersTurn, newTableState, AutomaticPossibilityAutomaticBehaviorAfterCardSwap(msg, changes)
              )

              md.copy(tableState = newTableState)
            } else {
              player.handler ! ServiceModuleMessage(AttackerSwapsCardsDeclined(msg.onPosition, msg.neutralDistribution), ServiceHandlersModules.cardGameMatch)
              md
            }
          }).getOrElse({
            sender ! ServiceModuleMessage(AttackerSwapsCardsDeclined(msg.onPosition, msg.neutralDistribution), ServiceHandlersModules.cardGameMatch)
            md
          })
        } else {
          sender ! ServiceModuleMessage(AttackerSwapsCardsDeclined(msg.onPosition, msg.neutralDistribution), ServiceHandlersModules.cardGameMatch)
          md
        }
      })

      //  val afterUnlock = FunnelingGameMessagesUnlock(self, afterLock)
      stay using afterLock
    }

    case Event(msg: AttackerAsksPossibleCardUses, matchData: MatchStateWaitingForPlayerEndTurn) => {
      stay using FunnelingGameMessagesLock(msg.player, matchData, sender, msg, (md) => {
        if (md.tableState.playersTurn.contains(msg.player)) {
          cardController ! AskPossibleUsageForPlayer(md.tableState.getPlayerTeam(msg.player), md.tableState, AutomaticPossibilityPlayerRequest)
          md
        } else {
          log.error("Wrong player turn. {}", msg.player)
          md
        }
      })
    }

    case Event(msg: PossibleUsages, matchData: MatchStateWaitingForPlayerEndTurn) => {

      log.debug("Getting possible usages: {}", msg.usages)

      val unlocked = FunnelingGameMessagesUnlock(self, matchData.copy(
        tableState = MergeMatchTableStateAndDivergedTableState(matchData.tableState -> msg.tableState)
      ))

      unlocked.tableState.players.find(p => msg.players.contains(p.nickname)).map(p => {
        msg.automatic match {
          case AutomaticPossibilitySubRound =>
            p.handler ! ServiceModuleMessage(AskAttackerToUseCard(msg.usages.map(usage => AttackerUsesCard(
              unlocked.tableState.positionForAlias(usage.onPosition),
              usage.target.map(t => TransformRefToCardIsTarget(t, unlocked.tableState)),
              usage.cardHolder,
              usage.neutralDistribution
            ))), ServiceHandlersModules.cardGameMatch)

          case AutomaticPossibilityAutomaticBehaviorAfterUse(originalRequest, changes) =>
            p.handler ! ServiceModuleMessage(AttackerUsedCardAccepted(
              unlocked.tableState.positionForAlias(originalRequest.usage.onPosition),
              originalRequest.usage.target.map(t => TransformRefToCardIsTarget(t, unlocked.tableState)),
              originalRequest.usage.neutralDistribution,
              msg.usages.map(usage => AttackerUsesCard(
                unlocked.tableState.positionForAlias(usage.onPosition),
                usage.target.map(t => TransformRefToCardIsTarget(t, unlocked.tableState)),
                usage.cardHolder,
                usage.neutralDistribution, changes)),
              originalRequest.usage.cardHolder
            ),
              ServiceHandlersModules.cardGameMatch
            )

          case AutomaticPossibilityAutomaticBehaviorAfterCardSwap(originalRequest, changes) =>
            /*
            p.handler ! ServiceModuleMessage(AttackerSwapsCardsAccepted(originalRequest.onPosition, originalRequest.neutralDistribution, changes),
              ServiceHandlersModules.cardGameMatch
            )*/

            unlocked.tableState.players.filter(p => unlocked.tableState.playersTurn.contains(p.nickname)).map(playerPlaying => {
              playerPlaying.handler ! ServiceModuleMessage(AttackerSwapsCardsAccepted(originalRequest.onPosition, originalRequest.neutralDistribution, changes), ServiceHandlersModules.cardGameMatch)
            })

            unlocked.tableState.players.filterNot(p => unlocked.tableState.playersTurn.contains(p.nickname)).map(playerPlaying => {
              playerPlaying.handler ! ServiceModuleMessage(AttackerSwappedCards(originalRequest.onPosition, originalRequest.neutralDistribution), ServiceHandlersModules.cardGameMatch)
            })

          case AutomaticPossibilityPlayerRequest =>
            p.handler ! ServiceModuleMessage(AttackerPossibleCardUses(
              msg.usages.map(usage => AttackerUsesCard(
                unlocked.tableState.positionForAlias(usage.onPosition),
                usage.target.map(t => TransformRefToCardIsTarget(t, unlocked.tableState)),
                usage.cardHolder,
                usage.neutralDistribution))
            ), ServiceHandlersModules.cardGameMatch)

          case _ => log.error("Unknown  AutomaticPossibility for context")
        }
      })

      stay using unlocked
    }

    case Event(msg: AttackerUsesCard, matchData: MatchStateWaitingForPlayerEndTurn) => {
      stay using (if (matchData.tableState.playersTurn.contains(msg.onPosition.player)) {
        FunnelingGameMessagesLock(msg.onPosition.player, matchData, sender, msg, (md) => {
          toCardHolder(msg.onPosition, md.tableState).map(c => {
            log.debug("[Match controller] {} uses {} .", msg.onPosition.player, c.id)
            val generated = md.tableState.createNewAliasForPosition(msg.onPosition)
            msg.target.map(t => {
              val transformTarget = TransformCardIsTargetToRef(t, generated._1)
              cardController ! UseCard(ParametrizedUsage(Some(transformTarget._2), generated._2, c, msg.neutralDistribution), transformTarget._1)
              md.copy(tableState = transformTarget._1)
            }).getOrElse({
              cardController ! UseCard(ParametrizedUsage(None, generated._2, c, msg.neutralDistribution), generated._1)
              md.copy(tableState = generated._1)
            })
          }).getOrElse(md)
        })
      } else {
        log.error("Wrong player turn. {}", msg.onPosition.player)
        matchData
      })
    }

    case Event(msg: TableStateAfterParametrizedUsage, matchData: MatchStateWaitingForPlayerEndTurn) => {

      val originalPosition = matchData.tableState.positionForAlias(msg.originalRequest.usage.onPosition)
      val newTableState = MergeMatchTableStateAndDivergedTableState(matchData.tableState -> msg.newTableState)
      val teamPlayers = newTableState.getPlayerTeam(originalPosition.player)

      log.debug("Team players {} for {}", teamPlayers, originalPosition.player)

      recording ! RecordAttackerUsesCard(AttackerUsesCard(
        originalPosition,
        msg.originalRequest.usage.target.map(t => TransformRefToCardIsTarget(t, newTableState)),
        msg.originalRequest.usage.cardHolder,
        msg.originalRequest.usage.neutralDistribution)
      )

      cardController ! AskPossibleUsageForPlayer(
        teamPlayers, newTableState, AutomaticPossibilityAutomaticBehaviorAfterUse(msg.originalRequest, msg.changes)
      )

      newTableState.players.filterNot(f => teamPlayers.contains(f.nickname)).map(p => {
        p.handler ! ServiceModuleMessage(
          AttackerUsesCard(
            newTableState.positionForAlias(msg.change.get.onPosition),
            msg.change.get.target.map(t => TransformRefToCardIsTarget(t, newTableState)),
            msg.change.get.cardHolder,
            msg.change.get.neutralDistribution,
            msg.changes
          ),
          ServiceHandlersModules.cardGameMatch
        )
      })

      stay using matchData.copy(tableState = newTableState)
    }

    case Event(msg: CannotUseCard, matchData: MatchStateWaitingForPlayerEndTurn) => {
      stay using FunnelingGameMessagesUnlock(self, matchData, (md) => {
        val originalPosition = md.tableState.positionForAlias(msg.originalRequest.usage.onPosition)
        md.tableState.players.find(p => p.nickname == originalPosition.player).map(
          f => f.handler ! ServiceModuleMessage(AttackerUsedCardDeclined(
            originalPosition,
            msg.originalRequest.usage.target.map(t => TransformRefToCardIsTarget(t, md.tableState)),
            msg.originalRequest.usage.neutralDistribution
          ),
            ServiceHandlersModules.cardGameMatch
          )
        )
        md
      })
    }

    case Event(msg: AttackerEndsTurn, matchData: MatchStateWaitingForPlayerEndTurn) => {
      if (matchData.tableState.playersTurn.contains(msg.user)) {
        if (!matchData.tableOwnerLock.isDefined) {
          val newMatchData = matchData.copy(endTurnUserLeft = matchData.endTurnUserLeft.filter(mu => mu != msg.user))
          if (newMatchData.endTurnUserLeft.size > 0) {
            stay using newMatchData
          } else {
            val playersTurn = newMatchData.tableState.players.filterNot(p => newMatchData.tableState.playersTurn.contains(p.nickname)).map(pt => pt.nickname)
            goto(STATE_ACK) using MatchStateWaitingForAck(
              ATTACKER_TEAM_PLAY,
              DEFENDER_TEAM_COUNTER_PLAY,
              newMatchData.copy(tableState = newMatchData.tableState.copy(playersTurn = playersTurn)),
              newMatchData.tableState.players.filter(p => p.connected == true).map(f => f.nickname)
            )
          }
        } else {
          stay using matchData.copy(cachedTableRequests = sender -> msg :: matchData.cachedTableRequests)
        }
      } else {
        log.error("Wrong player turn. {}", msg.user)
        stay using matchData
      }
    }

    case Event(AttackerTurnEndsByTime, matchData: MatchStateWaitingForPlayerEndTurn) => {
      if (!matchData.tableOwnerLock.isDefined) {
        val playersTurn = matchData.tableState.players.filterNot(p => matchData.tableState.playersTurn.contains(p.nickname)).map(pt => pt.nickname)
        goto(STATE_ACK) using MatchStateWaitingForAck(
          ATTACKER_TEAM_PLAY,
          DEFENDER_TEAM_COUNTER_PLAY,
          matchData.copy(tableState = matchData.tableState.copy(playersTurn = playersTurn)),
          matchData.tableState.players.filter(p => p.connected == true).map(f => f.nickname)
        )
      } else {
        stay using matchData.copy(cachedTableRequests = sender -> AttackerTurnEndsByTime :: matchData.cachedTableRequests)
      }
    }
  }

  when(DEFENDER_TEAM_COUNTER_PLAY) {

    case Event(msg: DefenderAsksPossibleCardUses, matchData: MatchStateWaitingForPlayerEndTurn) => {
      stay using FunnelingGameMessagesLock(msg.player, matchData, sender, msg, (md) => {
        if (md.tableState.playersTurn.contains(msg.player)) {
          cardController ! AskPossibleCounterUsageForPlayer(md.tableState.getPlayerTeam(msg.player), md.tableState, AutomaticPossibilityPlayerRequest)
          md
        } else {
          log.error("Wrong player turn. {}", msg.player)
          md
        }
      })
    }

    case Event(msg: PossibleCounterUsages, matchData: MatchStateWaitingForPlayerEndTurn) => {
      val unlocked = FunnelingGameMessagesUnlock(self, matchData.copy(
        tableState = MergeMatchTableStateAndDivergedTableState(matchData.tableState -> msg.tableState)
      ))

      unlocked.tableState.players.find(p => msg.players.contains(p.nickname)).map(p => {
        msg.automatic match {
          case AutomaticPossibilitySubRound =>
            p.handler ! ServiceModuleMessage(AskDefenderToUseCard(msg.counterUsages.map(cu => {
              DefenderUsesCard(
                unlocked.tableState.positionForAlias(cu.onPosition),
                cu.target.map(t => TransformRefToCardIsTarget(t, unlocked.tableState)),
                cu.cardHolder,
                cu.neutralDistribution
              )
            })), ServiceHandlersModules.cardGameMatch)

          case AutomaticPossibilityPlayerRequest =>
            p.handler ! ServiceModuleMessage(DefenderPossibleCardUses(msg.counterUsages.map(cu => {
              DefenderUsesCard(
                unlocked.tableState.positionForAlias(cu.onPosition),
                cu.target.map(t => TransformRefToCardIsTarget(t, unlocked.tableState)),
                cu.cardHolder,
                cu.neutralDistribution
              )
            })), ServiceHandlersModules.cardGameMatch)

          case AutomaticPossibilityAutomaticBehaviorAfterCounterUse(originalRequest, changes) =>
            p.handler ! ServiceModuleMessage(DefenderUsedCardAccepted(
              unlocked.tableState.positionForAlias(originalRequest.usage.onPosition),
              originalRequest.usage.target.map(t => TransformRefToCardIsTarget(t, unlocked.tableState)),
              originalRequest.usage.neutralDistribution,
              msg.counterUsages.map(cu => DefenderUsesCard(
                unlocked.tableState.positionForAlias(cu.onPosition),
                cu.target.map(t => TransformRefToCardIsTarget(t, unlocked.tableState)),
                cu.cardHolder,
                cu.neutralDistribution,
                changes
              )),
              originalRequest.usage.cardHolder
            ),
              ServiceHandlersModules.cardGameMatch
            )

          case _ => log.error("Unexpected AutomaticPossibility in this context.")
        }
      })

      stay using unlocked
    }


    case Event(msg: DefenderUsesCard, matchData: MatchStateWaitingForPlayerEndTurn) => {
      if (matchData.tableState.playersTurn.contains(msg.onPosition.player)) {
        stay using FunnelingGameMessagesLock(msg.onPosition.player, matchData, self, msg, (md) => {
          toCardHolder(msg.onPosition, matchData.tableState).map(c => {
            val generatedPosition = md.tableState.createNewAliasForPosition(msg.onPosition)
            md.copy(tableState = msg.target.map(t => {
              val targetGenerated = TransformCardIsTargetToRef(t, generatedPosition._1)

              cardController ! CounterUseCard(
                ParametrizedUsage(
                  Some(targetGenerated._2),
                  generatedPosition._2,
                  c,
                  msg.neutralDistribution
                ), targetGenerated._1)
              targetGenerated._1
            }).getOrElse({
              cardController ! CounterUseCard(
                ParametrizedUsage(
                  None,
                  generatedPosition._2,
                  c,
                  msg.neutralDistribution
                ), generatedPosition._1)
              generatedPosition._1
            }))
          }).getOrElse(md)
        })
      } else {
        log.error("Wrong player turn. {}", msg.onPosition.player)
        stay()
      }

    }

    case Event(msg: TableStateAfterParametrizedCounterUsage, matchData: MatchStateWaitingForPlayerEndTurn) => {

      val newTableState = MergeMatchTableStateAndDivergedTableState(matchData.tableState -> msg.newTableState)
      val position = newTableState.positionForAlias(msg.originalRequest.usage.onPosition)
      recording ! RecordDefenderUsesCard(
        DefenderUsesCard(
          position,
          msg.originalRequest.usage.target.map(t => TransformRefToCardIsTarget(t, newTableState)),
          msg.originalRequest.usage.cardHolder,
          msg.originalRequest.usage.neutralDistribution
        )
      )

      val team = newTableState.getPlayerTeam(position.player)
      cardController ! AskPossibleCounterUsageForPlayer(team, newTableState, AutomaticPossibilityAutomaticBehaviorAfterCounterUse(msg.originalRequest, msg.changes))

      newTableState.players.filterNot(f => team.contains(f.nickname)).map(p => {
        p.handler ! ServiceModuleMessage(
          DefenderUsesCard(
            position,
            msg.originalRequest.usage.target.map(t => TransformRefToCardIsTarget(t, newTableState)),
            msg.originalRequest.usage.cardHolder,
            msg.originalRequest.usage.neutralDistribution,
            msg.changes
          ),
          ServiceHandlersModules.cardGameMatch
        )
      })

      stay using (matchData.copy(tableState = newTableState))
    }

    case Event(msg: CannotCounterUseCard, matchData: MatchStateWaitingForPlayerEndTurn) => {
      stay using FunnelingGameMessagesUnlock(self, matchData, (md) => {
        val position = md.tableState.positionForAlias(msg.originalRequest.usage.onPosition)
        md.tableState.players.find(p => p.nickname == position.player).map(
          f => f.handler ! ServiceModuleMessage(DefenderUsedCardDeclined(
            position,
            msg.originalRequest.usage.target.map(t => TransformRefToCardIsTarget(t, md.tableState)),
            msg.originalRequest.usage.neutralDistribution),
            ServiceHandlersModules.cardGameMatch
          )
        )
        md
      })
    }

    case Event(msg: DefenderEndsTurn, matchData: MatchStateWaitingForPlayerEndTurn) => {
      if (matchData.tableState.playersTurn.contains(msg.user)) {
        if (!matchData.tableOwnerLock.isDefined) {
          val newMatchData = matchData.copy(endTurnUserLeft = matchData.endTurnUserLeft.filter(mu => mu != msg.user))
          if (newMatchData.endTurnUserLeft.size > 0) {
            stay using newMatchData
          } else {
            val playersTurn = newMatchData.tableState.players.filterNot(p => newMatchData.tableState.playersTurn.contains(p.nickname)).map(pt => pt.nickname)
            goto(STATE_ACK) using MatchStateWaitingForAck(
              DEFENDER_TEAM_COUNTER_PLAY,
              EVALUATION_AND_ANIMATION,
              MatchStateWaitingForAnimationEnds(matchData.tableState.copy(playersTurn = playersTurn), matchData.tableState.players.map(p => p.nickname), ATTACKER_TEAM_ATTACK),
              newMatchData.tableState.players.filter(p => p.connected == true).map(f => f.nickname)
            )
          }
        } else {
          stay using matchData.copy(cachedTableRequests = sender -> msg :: matchData.cachedTableRequests)
        }
      } else {
        log.error("Wrong player turn. {}", msg.user)
        stay using matchData
      }
    }

    case Event(DefenderTurnEndsByTime, matchData: MatchStateWaitingForPlayerEndTurn) => {
      if (!matchData.tableOwnerLock.isDefined) {
        val playersTurn = matchData.tableState.players.filterNot(p => matchData.tableState.playersTurn.contains(p.nickname)).map(pt => pt.nickname)
        goto(STATE_ACK) using MatchStateWaitingForAck(
          DEFENDER_TEAM_COUNTER_PLAY,
          EVALUATION_AND_ANIMATION,
          MatchStateWaitingForAnimationEnds(matchData.tableState.copy(playersTurn = playersTurn), matchData.tableState.players.map(p => p.nickname), ATTACKER_TEAM_ATTACK),
          matchData.tableState.players.filter(p => p.connected == true).map(f => f.nickname)
        )
      } else {
        stay using matchData.copy(cachedTableRequests = sender -> AttackerTurnEndsByTime :: matchData.cachedTableRequests)
      }
    }
  }

  when(ATTACKER_TEAM_ATTACK) {

    case Event(msg: AskPossibleAttacks, matchData: MatchStateWaitingForPlayerEndTurn) => {
      if (matchData.tableState.playersTurn.contains(msg.player)) {
        stay using FunnelingGameMessagesLock(msg.player, matchData, sender, msg, (md) => {
          cardController ! AskPossibleAttackPostionsForPlayer(md.tableState.getPlayerTeam(msg.player), md.tableState, AutomaticPossibilitySubRound)
          md
        })
      } else {
        log.error("Wrong player turn. {}", msg.player)
        stay()
      }
    }

    case Event(msg: PossibleAttacksPositions, matchData: MatchStateWaitingForPlayerEndTurn) => {
      val unlocked = FunnelingGameMessagesUnlock(self, matchData.copy(tableState = MergeMatchTableStateAndDivergedTableState(matchData.tableState -> msg.tableState)))
      unlocked.tableState.players.find(p => msg.players.contains(p.nickname)).map(p => {
        msg.automatic match {
          case AutomaticPossibilitySubRound =>
            p.handler ! ServiceModuleMessage(AskAttackerToAttack(msg.possibleAttacks.map(aa => {
              AttackerAttacks(unlocked.tableState.positionForAlias(aa.onPosition), aa.card)
            })), ServiceHandlersModules.cardGameMatch)

          case AutomaticPossibilityPlayerRequest =>
            p.handler ! ServiceModuleMessage(PossibleAttacks(msg.possibleAttacks.map(aa => {
              AttackerAttacks(unlocked.tableState.positionForAlias(aa.onPosition), aa.card)
            })), ServiceHandlersModules.cardGameMatch)

          case AutomaticPossibilityAutomaticBehaviorAfterAttack(originalRequest, changes) =>
            p.handler ! ServiceModuleMessage(AttackAccepted(
              unlocked.tableState.positionForAlias(originalRequest.attack.onPosition),
              msg.possibleAttacks.map(aa => AttackerAttacks(unlocked.tableState.positionForAlias(aa.onPosition), aa.card)),
              originalRequest.attack.card,
              changes
            ),
              ServiceHandlersModules.cardGameMatch
            )

          case _ => log.error("Unexpected AutomaticPossibility in this context")
        }
      })
      stay using unlocked
    }

    case Event(msg: AttackerAttacks, matchData: MatchStateWaitingForPlayerEndTurn) => {
      stay using FunnelingGameMessagesLock(msg.withPosition.player, matchData, sender, msg, (md) => {
        if (md.tableState.playersTurn.contains(msg.withPosition.player)) {
          val aliasGenerator = md.tableState.createNewAliasForPosition(msg.withPosition)
          toCardHolder(msg.withPosition, md.tableState).map(c => {
            cardController ! AttackWithCard(ParametrizedAttack(aliasGenerator._2, c), aliasGenerator._1)
            md.copy(tableState = aliasGenerator._1)
          }).getOrElse({
            md.copy(tableState = aliasGenerator._1)
          })
        } else {
          log.error("Wrong player turn. {}", msg.withPosition.player)
          md
        }
      })
    }

    case Event(msg: TableStateAfterParametrizedAttack, matchData: MatchStateWaitingForPlayerEndTurn) => {

      val newTableState = MergeMatchTableStateAndDivergedTableState(matchData.tableState -> msg.newTableState)
      val position = newTableState.positionForAlias(msg.originalRequest.attack.onPosition)

      recording ! RecordAttackerAttacks(AttackerAttacks(position, msg.originalRequest.attack.card))

      val team = newTableState.getPlayerTeam(position.player)
      cardController ! AskPossibleAttackPostionsForPlayer(team, newTableState, AutomaticPossibilityAutomaticBehaviorAfterAttack(msg.originalRequest, msg.changes))

      newTableState.players.filterNot(f => team.contains(f.nickname)).map(p => {
        p.handler ! ServiceModuleMessage(
          AttackerAttacks(position, msg.originalRequest.attack.card, msg.changes),
          ServiceHandlersModules.cardGameMatch
        )
      })

      stay using matchData.copy(tableState = newTableState)
    }

    case Event(msg: CannotAttackWithCard, matchData: MatchStateWaitingForPlayerEndTurn) => {
      stay using FunnelingGameMessagesUnlock(self, matchData, (md) => {
        val position = md.tableState.positionForAlias(msg.originalRequest.attack.onPosition)
        md.tableState.players.find(p => p.nickname == position.player).map(
          f => f.handler ! ServiceModuleMessage(AttackDeclined(
            position
          ), ServiceHandlersModules.cardGameMatch)
        )
        md
      })
    }


    case Event(msg: AttackerEndsTurn, matchData: MatchStateWaitingForPlayerEndTurn) => {

      if (matchData.tableState.playersTurn.contains(msg.user)) {
        if (!matchData.tableOwnerLock.isDefined) {
          val newMatchData = matchData.copy(endTurnUserLeft = matchData.endTurnUserLeft.filter(mu => mu != msg.user))
          if (newMatchData.endTurnUserLeft.size > 0) {
            stay using newMatchData
          } else {
            val playersTurn = newMatchData.tableState.players.filterNot(p => newMatchData.tableState.playersTurn.contains(p.nickname)).map(pt => pt.nickname)
            goto(STATE_ACK) using MatchStateWaitingForAck(
              ATTACKER_TEAM_ATTACK,
              DEFENDER_TEAM_DEFENDS,
              newMatchData.copy(tableState = newMatchData.tableState.copy(playersTurn = playersTurn)),
              newMatchData.tableState.players.filter(p => p.connected == true).map(f => f.nickname)
            )
          }
        } else {
          stay using matchData.copy(cachedTableRequests = sender -> msg :: matchData.cachedTableRequests)
        }
      } else {
        log.error("Wrong player turn. {}", msg.user)
        stay using matchData
      }
    }

    case Event(AttackerTurnEndsByTime, matchData: MatchStateWaitingForPlayerEndTurn) => {
      if (!matchData.tableOwnerLock.isDefined) {
        val playersTurn = matchData.tableState.players.filterNot(p => matchData.tableState.playersTurn.contains(p.nickname)).map(pt => pt.nickname)
        goto(STATE_ACK) using MatchStateWaitingForAck(
          ATTACKER_TEAM_ATTACK,
          DEFENDER_TEAM_DEFENDS,
          matchData.copy(tableState = matchData.tableState.copy(playersTurn = playersTurn)),
          matchData.tableState.players.filter(p => p.connected == true).map(f => f.nickname)
        )
      } else {
        stay using matchData.copy(cachedTableRequests = sender -> AttackerTurnEndsByTime :: matchData.cachedTableRequests)
      }
    }
  }


  when(DEFENDER_TEAM_DEFENDS) {
    case Event(msg: AskPossibleDefending, matchData: MatchStateWaitingForPlayerEndTurn) => {
      if (matchData.tableState.playersTurn.contains(msg.player)) {
        stay using FunnelingGameMessagesLock(msg.player, matchData, sender, msg, (md) => {
          cardController ! AskPossibleDefendingPositionsForPlayer(md.tableState.getPlayerTeam(msg.player), md.tableState, AutomaticPossibilitySubRound)
          md
        })
      } else {
        log.error("Wrong player turn. {}", msg.player)
        stay using (matchData)
      }
    }


    case Event(msg: PossibleDefendPositions, matchData: MatchStateWaitingForPlayerEndTurn) => {
      val unlocked = FunnelingGameMessagesUnlock(self, matchData.copy(tableState = MergeMatchTableStateAndDivergedTableState(matchData.tableState -> msg.tableState)))
      unlocked.tableState.players.find(p => msg.players.contains(p.nickname)).map(p => {
        msg.automatic match {
          case AutomaticPossibilitySubRound =>
            p.handler ! ServiceModuleMessage(
              AskDefenderToDefend(msg.possibleDefending.map(pd => DefenderDefends(
                unlocked.tableState.positionForAlias(pd.defendWith),
                TransformRefToCardIsTarget(pd.against, matchData.tableState),
                pd.card
              ))),
              ServiceHandlersModules.cardGameMatch)

          case AutomaticPossibilityPlayerRequest =>
            p.handler ! ServiceModuleMessage(
              PossibleDefending(msg.possibleDefending.map(pd => DefenderDefends(
                unlocked.tableState.positionForAlias(pd.defendWith),
                TransformRefToCardIsTarget(pd.against, matchData.tableState),
                pd.card
              ))),
              ServiceHandlersModules.cardGameMatch)

          case AutomaticPossibilityAutomaticBehaviorAfterDefends(originalRequest, changes) =>
            p.handler ! ServiceModuleMessage(DefendingAccepted(
              unlocked.tableState.positionForAlias(originalRequest.defend.defendWith),
              TransformRefToCardIsTarget(originalRequest.defend.against, matchData.tableState),
              msg.possibleDefending.map(pd => DefenderDefends(
                unlocked.tableState.positionForAlias(pd.defendWith),
                TransformRefToCardIsTarget(pd.against, matchData.tableState),
                pd.card
              )),
              originalRequest.defend.card,
              originalRequest.defend.neutralDistribution,
              changes
            ),
              ServiceHandlersModules.cardGameMatch
            )

          case _ => log.error("Unexpected AutomaticPossibility in this context")
        }
      })

      stay using unlocked
    }

    case Event(msg: DefenderDefends, matchData: MatchStateWaitingForPlayerEndTurn) => {
      if (matchData.tableState.playersTurn.contains(msg.withPosition.player)) {
        stay using FunnelingGameMessagesLock(msg.withPosition.player, matchData, sender, msg, (md) => {
          val aliasGenerator = matchData.tableState.createNewAliasForPosition(msg.withPosition)
          toCardHolder(msg.withPosition, aliasGenerator._1).map(c => {
            val transformTarget = TransformCardIsTargetToRef(msg.againts, aliasGenerator._1)
            cardController ! DefendWithCard(ParametrizedDefense(aliasGenerator._2, transformTarget._2, c, msg.neutralDistribution), transformTarget._1)
            md.copy(tableState = transformTarget._1)
          }).getOrElse({
            md.copy(tableState = aliasGenerator._1)
          })
        })

      } else {
        log.error("Wrong player turn. {}", msg.withPosition.player)
        stay()
      }
    }

    case Event(msg: TableStateAfterParametrizedDefense, matchData: MatchStateWaitingForPlayerEndTurn) => {

      val newTableState = MergeMatchTableStateAndDivergedTableState(matchData.tableState -> msg.newTableState)
      val position = newTableState.positionForAlias(msg.originalRequest.defend.defendWith)

      val team = newTableState.getPlayerTeam(position.player)

      recording ! RecordDefenderDefends(DefenderDefends(position, TransformRefToCardIsTarget(msg.originalRequest.defend.against, msg.newTableState), msg.originalRequest.defend.card))

      cardController ! AskPossibleDefendingPositionsForPlayer(team, newTableState, AutomaticPossibilityAutomaticBehaviorAfterDefends(msg.originalRequest, msg.changes))

      newTableState.players.filterNot(f => team.contains(f.nickname)).map(p => {
        p.handler ! ServiceModuleMessage(
          DefenderDefends(
            position,
            TransformRefToCardIsTarget(msg.originalRequest.defend.against, newTableState),
            msg.originalRequest.defend.card,
            msg.originalRequest.defend.neutralDistribution,
            msg.changes
          ),
          ServiceHandlersModules.cardGameMatch
        )
      })

      stay using matchData.copy(tableState = newTableState)
    }

    case Event(msg: CannotDefendWithCard, matchData: MatchStateWaitingForPlayerEndTurn) => {
      stay using FunnelingGameMessagesUnlock(self, matchData, (md) => {
        val position = md.tableState.positionForAlias(msg.originalRequest.defend.defendWith)
        md.tableState.players.find(p => p.nickname == position.player).map(
          f => f.handler ! ServiceModuleMessage(DefendingDeclined(
            position,
            TransformRefToCardIsTarget(msg.originalRequest.defend.against, md.tableState)
          ), ServiceHandlersModules.cardGameMatch)
        )
        md
      })
    }


    case Event(msg: DefenderEndsTurn, matchData: MatchStateWaitingForPlayerEndTurn) => {
      if (matchData.tableState.playersTurn.contains(msg.user)) {
        if (!matchData.tableOwnerLock.isDefined) {
          val newMatchData = matchData.copy(endTurnUserLeft = matchData.endTurnUserLeft.filter(mu => mu != msg.user))
          if (newMatchData.endTurnUserLeft.size > 0) {
            stay using newMatchData
          } else {
            val playersTurn = newMatchData.tableState.players.filterNot(p => newMatchData.tableState.playersTurn.contains(p.nickname)).map(pt => pt.nickname)
            goto(STATE_ACK) using MatchStateWaitingForAck(
              DEFENDER_TEAM_DEFENDS,
              EVALUATION_AND_ANIMATION,
              MatchStateWaitingForAnimationEnds(matchData.tableState.copy(playersTurn = playersTurn), matchData.tableState.players.map(p => p.nickname), FORTUNE_ROLL),
              newMatchData.tableState.players.filter(p => p.connected == true).map(f => f.nickname)
            )
          }
        } else {
          stay using matchData.copy(cachedTableRequests = sender -> msg :: matchData.cachedTableRequests)
        }
      } else {
        log.error("Wrong player turn. {}", msg.user)
        stay using matchData
      }
    }

    case Event(DefenderTurnEndsByTime, matchData: MatchStateWaitingForPlayerEndTurn) => {
      if (!matchData.tableOwnerLock.isDefined) {
        val playersTurn = matchData.tableState.players.filterNot(p => matchData.tableState.playersTurn.contains(p.nickname)).map(pt => pt.nickname)
        goto(STATE_ACK) using MatchStateWaitingForAck(
          DEFENDER_TEAM_DEFENDS,
          EVALUATION_AND_ANIMATION,
          MatchStateWaitingForAnimationEnds(matchData.tableState.copy(playersTurn = playersTurn), matchData.tableState.players.map(p => p.nickname), FORTUNE_ROLL),
          matchData.tableState.players.filter(p => p.connected == true).map(f => f.nickname)
        )
      } else {
        stay using matchData.copy(cachedTableRequests = sender -> AttackerTurnEndsByTime :: matchData.cachedTableRequests)
      }
    }
  }

  when(EVALUATION_AND_ANIMATION) {
    case Event(msg: EvaluateAttackDone, matchData: MatchStateWaitingForAnimationEnds) => {
      val newPlayerState = MergeMatchTableStateAndDivergedTableState(matchData.tableState -> msg.ts)

      newPlayerState.players.map(p => p.handler ! ServiceModuleMessage(msg, ServiceHandlersModules.cardGameMatch))
      setTimer("endTurn", AnimationStateDoneByTimer, setup.turnTimers(EVALUATION_AND_ANIMATION), false)

      recording ! RecordAttackAndDefenseChanges(msg.changesStack)
      recording ! RecordTableAfterAttackEvaluation(msg.ts)

      msg.changesStack.find(ch => ch.isInstanceOf[PlayersLost]).map(found => {
        val change: PlayersLost = found.asInstanceOf[PlayersLost]
        val winners = newPlayerState.players.filter(p => !change.players.contains(p.nickname))

        recording ! RecordMatchEnds(winners.map(w => w.nickname), change.players)
        matchResultController ! MatchEnds(winners.map(w => w.nickname), change.players, setup, startingSetup)
        log.info("Stopping match handler.")

        goto(MATCH_ENDED)
      }).getOrElse({
        stay using (matchData.copy(
          tableState = newPlayerState.copy(roundNumber = newPlayerState.roundNumber + 1,
            positionAliases = newPlayerState.positionAliases.filter(f => f._1.referenceLifeSpan != CardPositionRefLifeSpan.CardPositionRefLifeSpanOneRound),
           attackedCards = List(),
           defendedCards = List()
          )))
      })
    }

    case Event(msg: EvaluateUsageDone, matchData: MatchStateWaitingForAnimationEnds) => {
      val changes = msg.changesStack.reverse

      val newPlayerState = MergeMatchTableStateAndDivergedTableState(matchData.tableState -> msg.ts).copy(
        usedCards = List(),
        counterUsedCards = List()
      )

      newPlayerState.players.map(p => {
        log.debug("Sending EvaluateUsageDone to {}.", p.nickname)
        p.handler ! ServiceModuleMessage(msg.copy(changesStack = changes), ServiceHandlersModules.cardGameMatch)
      })

      recording ! RecordUsageAndCounterUsageChanges(changes)
      recording ! RecordTableAfterUsageEvaluation(msg.ts)

      setTimer("endTurn", AnimationStateDoneByTimer, setup.turnTimers(EVALUATION_AND_ANIMATION), false)
      changes.find(ch => ch.isInstanceOf[PlayersLost]).map(found => {
        val change: PlayersLost = found.asInstanceOf[PlayersLost]
        val winners = newPlayerState.players.filter(p => !change.players.contains(p.nickname))

        recording ! RecordMatchEnds(winners.map(w => w.nickname), change.players)
        matchResultController ! MatchEnds(winners.map(w => w.nickname), change.players, setup, startingSetup)
        log.info("Stopping match handler.")

        goto(MATCH_ENDED)
      }).getOrElse({
        stay using (matchData.copy(tableState = newPlayerState))
      })
    }

    case Event(AnimationStateDoneByTimer, matchData: MatchStateWaitingForAnimationEnds) => {
      matchData.nextState match {
        case ATTACKER_TEAM_ATTACK =>
          goto(STATE_ACK) using MatchStateWaitingForAck(
            EVALUATION_AND_ANIMATION,
            matchData.nextState,
            MatchStateWaitingForPlayerEndTurn(
              None,
              matchData.tableState,
              List(),
              matchData.tableState.playersTurn
            ),
            matchData.tableState.players.filter(p => p.connected == true).map(f => f.nickname)
          )

        case FORTUNE_ROLL =>
          goto(STATE_ACK) using MatchStateWaitingForAck(
            EVALUATION_AND_ANIMATION,
            matchData.nextState,
            MatchStateWaitingForPlayerRolls(matchData.tableState, List()),
            matchData.tableState.players.filter(p => p.connected == true).map(f => f.nickname)
          )
      }
    }

    case Event(msg: AnimationDone, matchData: MatchStateWaitingForAnimationEnds) => {
      val newMatchData = matchData.copy(endAnimationUserLeft = matchData.endAnimationUserLeft.filter(u => u != msg.player))
      if (newMatchData.endAnimationUserLeft.size > 0) {
        stay using (newMatchData)
      } else {
        newMatchData.nextState match {
          case ATTACKER_TEAM_ATTACK =>
            goto(STATE_ACK) using MatchStateWaitingForAck(
              EVALUATION_AND_ANIMATION,
              matchData.nextState,
              MatchStateWaitingForPlayerEndTurn(None, newMatchData.tableState, List(), newMatchData.tableState.playersTurn),
              newMatchData.tableState.players.filter(p => p.connected == true).map(f => f.nickname)
            )
          case FORTUNE_ROLL =>
            goto(STATE_ACK) using MatchStateWaitingForAck(
              EVALUATION_AND_ANIMATION,
              matchData.nextState,
              MatchStateWaitingForPlayerRolls(newMatchData.tableState, List()),
              newMatchData.tableState.players.filter(p => p.connected == true).map(f => f.nickname)
            )
        }
      }
    }
  }

  when(FORTUNE_ROLL) {
    case Event(msg: PlayerRoll, matchData: MatchStateWaitingForPlayerRolls) => {
      if (matchData.rolls.size < (matchData.tableState.players.filter(p => p.connected).size - 1)) {
        stay using (matchData.copy(rolls = msg :: matchData.rolls))
      } else {
        val tableStateWithRolls = matchData.copy(rolls = msg :: matchData.rolls)

        var computedPair = (0.0, 0.0)

        tableStateWithRolls.tableState.players.map(p => tableStateWithRolls.rolls.find(r => r.user.equals(p.nickname)).map(roll => {
          roll.position match {
            case PLAYER_ROLL_POSITION_ATTACKER => computedPair = (computedPair._1 + roll.number, computedPair._2)
            case PLAYER_ROLL_POSITION_DEFENDER => computedPair = (computedPair._1, computedPair._2 + roll.number)
          }
        }))

        val attackersDefenders = tableStateWithRolls.rolls.partition(p => p.position == PLAYER_ROLL_POSITION_ATTACKER)
        computedPair = (computedPair._1 / attackersDefenders._1.size, computedPair._2 / attackersDefenders._2.size)
        computedPair = Math.round(computedPair._1).toDouble -> Math.round(computedPair._2).toDouble
        val defValues =
          (if ((computedPair._2 - 1) <= 0) {
            6
          } else {
            computedPair._2 - 1
          }) ::
            computedPair._2 ::
            (if ((computedPair._2 + 1) > 6) {
              1
            } else {
              computedPair._2 + 1
            }) ::
            Nil

        val fortune = if(defValues.contains(attackersDefenders._1)) {
          recording ! RecordRollResult(PlayerRollResult(attackersDefenders._2, attackersDefenders._1))
          tableStateWithRolls.tableState.players.filter(p => p.connected == true).map(f => f.handler ! ServiceModuleMessage(PlayerRollResult(attackersDefenders._2, attackersDefenders._1), ServiceHandlersModules.cardGameMatch))
          PLAYER_ROLL_POSITION_ATTACKER
        } else {
          recording ! RecordRollResult(PlayerRollResult(attackersDefenders._1, attackersDefenders._2))
          tableStateWithRolls.tableState.players.filter(p => p.connected == true).map(f => f.handler ! ServiceModuleMessage(PlayerRollResult(attackersDefenders._1, attackersDefenders._2), ServiceHandlersModules.cardGameMatch))
          PLAYER_ROLL_POSITION_ATTACKER
        }

        val table =
          addElements(addCardsFromDeckToHands(matchData.tableState.copy(fortuneIsOnSide = fortune)))
            .copy(playersTurn = matchData.tableState.players.filterNot(p => matchData.tableState.playersTurn.contains(p.nickname)).map(pt => pt.nickname))

        goto(STATE_ACK) using (MatchStateWaitingForAck(FORTUNE_ROLL, ATTACKER_TEAM_PLAY,
          MatchStateWaitingForPlayerEndTurn(None, table, List(), table.playersTurn)
          , tableStateWithRolls.tableState.players.filter(p => p.connected == true).map(f => f.nickname)))
      }
    }

    case Event(RollEndsByTime, matchData: MatchStateWaitingForPlayerRolls) => {

      val playersRolled = matchData.tableState.players.filter(p => matchData.rolls.find(r => r.user == p.nickname).isEmpty)
        .map(p => PlayerRoll(p.nickname, random.nextInt(6) + 1, startingRollPositions.find(position => position.user.equals(p.nickname)).get.position)) ::: matchData.rolls

      var computedPair = (0.0, 0.0)

      matchData.tableState.players.map(p => playersRolled.find(r => r.user.equals(p.nickname)).map(roll => {
        roll.position match {
          case PLAYER_ROLL_POSITION_ATTACKER => computedPair = (computedPair._1 + roll.number, computedPair._2)
          case PLAYER_ROLL_POSITION_DEFENDER => computedPair = (computedPair._1, computedPair._2 + roll.number)
        }
      }))

      val attackersDefenders = playersRolled.partition(p => p.position == PLAYER_ROLL_POSITION_ATTACKER)
      computedPair = (computedPair._1 / attackersDefenders._1.size, computedPair._2 / attackersDefenders._2.size)
      computedPair = Math.round(computedPair._1).toDouble -> Math.round(computedPair._2).toDouble

      val defValues =
        (if ((computedPair._2 - 1) <= 0) {
          6
        } else {
          computedPair._2 - 1
        }) ::
          computedPair._2 ::
          (if ((computedPair._2 + 1) > 6) {
            1
          } else {
            computedPair._2 + 1
          }) ::
          Nil


      val fortune = if (!defValues.contains(computedPair._1)) {
        recording ! RecordRollResult(PlayerRollResult(attackersDefenders._1, attackersDefenders._2))
        matchData.tableState.players.filter(p => p.connected == true).map(f => f.handler ! ServiceModuleMessage(PlayerRollResult(attackersDefenders._1, attackersDefenders._2), ServiceHandlersModules.cardGameMatch))
        PLAYER_ROLL_POSITION_ATTACKER
      } else {
        recording ! RecordRollResult(PlayerRollResult(attackersDefenders._2, attackersDefenders._1))
        matchData.tableState.players.filter(p => p.connected == true).map(f => f.handler ! ServiceModuleMessage(PlayerRollResult(attackersDefenders._2, attackersDefenders._1), ServiceHandlersModules.cardGameMatch))
        PLAYER_ROLL_POSITION_ATTACKER
      }

      val table =
        addElements(addCardsFromDeckToHands(matchData.tableState.copy(fortuneIsOnSide = fortune)))
          .copy(playersTurn = matchData.tableState.players.filterNot(p => matchData.tableState.playersTurn.contains(p.nickname)).map(pt => pt.nickname))

      goto(STATE_ACK) using MatchStateWaitingForAck(
        FORTUNE_ROLL,
        ATTACKER_TEAM_PLAY,
        MatchStateWaitingForPlayerEndTurn(
          None,
          table,
          List(),
          table.playersTurn
        ),
        matchData.tableState.players.filter(p => p.connected == true).map(f => f.nickname)
      )
    }
  }

  when(MATCH_ENDED) {
    case Event(Rewarded, data) =>
     // stop()
      stay()

    case Event(e, data) =>
      stay()
  }


  whenUnhandled {
    case Event(e, s) =>
      log.warning("Match actor received unhandled request {} in state {}/{}", e, stateName, s)
      stay
  }

  onTransition {
    case x -> STATE_ACK =>
      log.info("Changing state {} to {}", x, STATE_ACK)
      nextStateData match {
        case MatchStateWaitingForAck(originalState, state, matchData, expected) =>
          val tableState = matchData match {
            case ms: MatchStateWaitingForInitialization => ms.tableState
            case ms: MatchStateWaitingForAnimationEnds => ms.tableState
            case ms: MatchStateWaitingForPlayerEndTurn => ms.tableState
            case ms: MatchStateWaitingForPlayerRolls => ms.tableState
          }
          cancelTimer("endTurn")
          tableState.players.map(c => {
            log.info("| StateAck | Sending state ask to {}", c.nickname)
            c.handler ! ServiceModuleMessage(StateAsk(state, tableState, setup.turnTimers(state).toMillis, new DateTime().toDateTime(DateTimeZone.UTC).getMillis), ServiceHandlersModules.cardGameMatch)
          })

          log.info("Expected {} replies", expected.size)
        case t => throw new UnsupportedOperationException("Unknown state " + t.toString + " for transition " + x.toString + " ->  STATE_ACK")
      }

    case x -> PLAYER_STARTS_ROLL =>
      log.info("Changing state {} to {}", x, PLAYER_STARTS_ROLL)
      nextStateData match {
        case matchData: MatchStateWaitingForPlayerRolls =>
          matchData.tableState.players.filter(p => p.connected == true).map(c => {
            val msg = AskPlayerRoll(startingRollPositions.find(p => p.user.equals(c.nickname)).get.position)
            c.handler ! ServiceModuleMessage(msg, ServiceHandlersModules.cardGameMatch)
            log.info("Sending {} to {}", msg, c.nickname)
          })
          setTimer("endTurn", RollEndsByTime, setup.turnTimers(PLAYER_STARTS_ROLL), false)
        case t => throw new UnsupportedOperationException("Unknown state: " + t)
      }

    case x -> ATTACKER_TEAM_PLAY =>
      log.info("Changing state {} to {}", x, ATTACKER_TEAM_PLAY)
      nextStateData match {
        case matchData: MatchStateWaitingForPlayerEndTurn =>
          log.info("STARTING {} ROUND.", matchData.tableState.roundNumber)

          val attackerDefenders = matchData.tableState.players.filter(p => p.connected == true)
            .partition(p => matchData.tableState.playersTurn.contains(p.nickname))

          val attackers = attackerDefenders._1.map(a => a.nickname)
          log.debug("Sending AskAttackerToUseCard to cardController to {} ", attackers)
          cardController ! AskPossibleUsageForPlayer(attackers, matchData.tableState, AutomaticPossibilitySubRound)

          recording ! RecordNewRound(
            matchData.tableState.roundNumber,
            attackers,
            matchData.tableState.players.filter(p => !attackers.contains(p.nickname)).map(p => p.nickname),
            matchData.tableState)

          attackerDefenders._2.map(a => {
            log.info("Sending {} to {}", AskDefenderToWait, a.nickname)
            a.handler ! ServiceModuleMessage(AskDefenderToWait, ServiceHandlersModules.cardGameMatch)
          })

          setTimer("endTurn", AttackerTurnEndsByTime, setup.turnTimers(ATTACKER_TEAM_PLAY), false)
        case t => throw new UnsupportedOperationException("Unknown state: " + t)
      }

    case x -> DEFENDER_TEAM_COUNTER_PLAY =>
      log.info("Changing state {} to {}", x, DEFENDER_TEAM_COUNTER_PLAY)
      nextStateData match {
        case matchData: MatchStateWaitingForPlayerEndTurn =>
          val attackerDefenders = matchData.tableState.players.filter(p => p.connected == true)
            .partition(p => matchData.tableState.playersTurn.contains(p.nickname))

          val defenders = attackerDefenders._1.map(a => a.nickname)
          log.info("Sending AskDefenderToUseCard to {}", defenders)
          cardController ! AskPossibleCounterUsageForPlayer(defenders, matchData.tableState, AutomaticPossibilitySubRound)

          attackerDefenders._2.map(a => {
            log.info("Sending {} to {}", AskAttackerToWait, a.nickname)
            a.handler ! ServiceModuleMessage(AskAttackerToWait, ServiceHandlersModules.cardGameMatch)
          })

          setTimer("endTurn", DefenderTurnEndsByTime, setup.turnTimers(DEFENDER_TEAM_COUNTER_PLAY), false)
        case t => throw new UnsupportedOperationException("Unknown state: " + t)
      }

    case x -> ATTACKER_TEAM_ATTACK =>
      log.info("Changing state {} to {}", x, ATTACKER_TEAM_PLAY)
      nextStateData match {
        case matchData: MatchStateWaitingForPlayerEndTurn =>
          val attackerDefenders = matchData.tableState.players.filter(p => p.connected == true)
            .partition(p => matchData.tableState.playersTurn.contains(p.nickname))

          val attackers = attackerDefenders._1.map(a => a.nickname)
          log.info("Sending AskAttackerToAttack to {}", attackers)
          cardController ! AskPossibleAttackPostionsForPlayer(attackers, matchData.tableState, AutomaticPossibilitySubRound)

          attackerDefenders._2.map(a => {
            log.info("Sending {} to {}", AskDefenderToWaitForAttack, a.nickname)
            a.handler ! ServiceModuleMessage(AskDefenderToWaitForAttack, ServiceHandlersModules.cardGameMatch)
          })

          setTimer("endTurn", AttackerTurnEndsByTime, setup.turnTimers(ATTACKER_TEAM_ATTACK), false)
        case t => throw new UnsupportedOperationException("Unknown state: " + t)
      }

    case x -> DEFENDER_TEAM_DEFENDS =>
      log.info("Changing state {} to {}", x, DEFENDER_TEAM_DEFENDS)
      nextStateData match {
        case matchData: MatchStateWaitingForPlayerEndTurn =>
          val attackerDefenders = matchData.tableState.players.filter(p => p.connected == true)
            .partition(p => matchData.tableState.playersTurn.contains(p.nickname))

          val defenders = attackerDefenders._1.map(a => a.nickname)
          log.info("Sending AskDefenderToDefend to {}", defenders)
          cardController ! AskPossibleDefendingPositionsForPlayer(defenders, matchData.tableState, AutomaticPossibilitySubRound)

          attackerDefenders._2.map(a => {
            log.info("Sending {} to {}", AskAttackerToWaitForDefend, a.nickname)
            a.handler ! ServiceModuleMessage(AskAttackerToWaitForDefend, ServiceHandlersModules.cardGameMatch)
          })

          setTimer("endTurn", DefenderTurnEndsByTime, setup.turnTimers(DEFENDER_TEAM_DEFENDS), false)
        case t => throw new UnsupportedOperationException("Unknown state: " + t)
      }

    case x -> EVALUATION_AND_ANIMATION =>
      stateData match {
        case sap: MatchStateWaitingForAck =>
          sap.originalState match {
            case DEFENDER_TEAM_COUNTER_PLAY =>
              log.info("Changing state {} to {}", DEFENDER_TEAM_COUNTER_PLAY, EVALUATION_AND_ANIMATION)
              nextStateData match {
                case data: MatchStateWaitingForAnimationEnds => cardController ! EvaluateUsage(data.tableState.copy(
                  usedCards = data.tableState.usedCards.reverse,
                  counterUsedCards = data.tableState.counterUsedCards.reverse
                ), List())
                case t => throw new UnsupportedOperationException("Unknown state: " + t)
              }

            case DEFENDER_TEAM_DEFENDS =>
              log.info("Changing state {} to {}", DEFENDER_TEAM_DEFENDS, EVALUATION_AND_ANIMATION)
              nextStateData match {
                case data: MatchStateWaitingForAnimationEnds => cardController ! EvaluateAttack(data.tableState.copy(
                  attackedCards = data.tableState.attackedCards,
                  defendedCards = data.tableState.defendedCards
                ), List())
                case t => throw new UnsupportedOperationException("Unknown state: " + t)
              }
          }
      }


    case x -> FORTUNE_ROLL =>
      log.info("Changing state {} to {}", x, FORTUNE_ROLL)
      nextStateData match {
        case matchData: MatchStateWaitingForPlayerRolls =>
          val attackerDefenders = matchData.tableState.players.filter(p => p.connected == true)
            .partition(p => matchData.tableState.playersTurn.contains(p.nickname))

          attackerDefenders._1.map(a => a.handler ! ServiceModuleMessage(AskPlayerRoll(PLAYER_ROLL_POSITION_ATTACKER), ServiceHandlersModules.cardGameMatch))
          attackerDefenders._2.map(a => a.handler ! ServiceModuleMessage(AskPlayerRoll(PLAYER_ROLL_POSITION_DEFENDER), ServiceHandlersModules.cardGameMatch))

          setTimer("endTurn", RollEndsByTime, setup.turnTimers(FORTUNE_ROLL), false)

        case t => throw new UnsupportedOperationException("Unknown state: " + t)
      }
  }


  onTermination {
    case StopEvent(reason, state, data) =>
      log.info("Killing match actor.")
  }


  def addCardsFromDeckToHands(ts: TableState): TableState = {
    var state = ts
    ts.players.map(player => {

      if (player.deck.size > 0 && player.hands.size < setup.maxCardsInHand) {
        log.debug("Deck size is {}", player.deck.size)
        val index = random.nextInt(player.deck.size)
        log.debug("Drawing card at {} while deck has size of {}", index, player.deck.size)
        val card = player.deck(index)
        val message = ServiceModuleMessage(PlayerDrawCards(player.nickname, List(CardWithPosition(card, player.hands.size))), ServiceHandlersModules.cardGameMatch)

        state.players.map(p => p.handler ! message)

        state = state.copy(players = player.copy(deck = player.deck.filterNot(d => d == card), hands = player.hands :+ card) :: state.players.filterNot(p => p.nickname.equals(player.nickname)))
      } else {
        if (player.hands.size == setup.maxCardsInHand) log.debug("{} has hands full!", player.nickname)
        if (player.deck.size == 0) log.debug("{} has no more cards in deck", player.nickname)
      }
    })
    state
  }

  def addElements(ts: TableState): TableState = {
    var state = ts
    ts.players.map(p => {

      val rating = 100.0 / (p.charmRatio.fire + p.charmRatio.water + p.charmRatio.earth + p.charmRatio.air)

      val fireRatio = p.charmRatio.fire * rating
      val waterRatio = fireRatio + (p.charmRatio.water * rating)
      val earthRatio = waterRatio + (p.charmRatio.earth * rating)

      var chancePool: List[CharmOfElements] = List()

      for (i <- 0 to 99) {
        if (i < fireRatio) {
          chancePool = CharmOfElements(1, 0, 0, 0) :: chancePool
        } else if (i < waterRatio) {
          chancePool = CharmOfElements(0, 1, 0, 0) :: chancePool
        } else if (i < earthRatio) {
          chancePool = CharmOfElements(0, 0, 1, 0) :: chancePool
        } else {
          chancePool = CharmOfElements(0, 0, 0, 1) :: chancePool
        }
      }

      var result = CharmOfElements(0, 0, 0, 0);

      for (i <- 1 to setup.incomingEssencesPerRound) {
        val charm = chancePool(random.nextInt(chancePool.length))
        result = CharmOfElements(result.fire + charm.fire, result.water + charm.water, result.earth + charm.earth, result.air + charm.air)
      }

      val message = ServiceModuleMessage(PlayerGetsElements(p.nickname, result), ServiceHandlersModules.cardGameMatch)

      state.players.map(p => p.handler ! message)

      state = state.copy(
        players = p.copy(
          charm = (CharmOfElements(p.charm.fire + result.fire, p.charm.water + result.water, p.charm.earth + result.earth, p.charm.air + result.air) cutBy (8))
        ) :: state.players.filterNot(pl => pl == p)
      )
    })

    state
  }

}


class PlayerComposer(matchActor: ActorRef, playerServiceHandler: ActorRef, player: String, activeDeckId: String) extends Actor with ActorLogging {
  context.setReceiveTimeout(15 seconds)

  playerServiceHandler ! ServiceModuleMessage(GetCardDatabaseAdapter, ServiceHandlersModules.cardGameCards)

  var deck: Option[Deck] = None
  var cards: List[Card] = List()

  def receive = {
    case ReceiveTimeout =>
      matchActor ! CannotComposePlayer(player)
      context.stop(self)

    case CardDatabaseAdapterFound(cardDatabaseAdapter) =>
      cardDatabaseAdapter ! DatabaseCommand(GetDeckAndCardsForUserWithGivenDeckId(player, activeDeckId), "")

    case PlayerInMatchHandlerFound(h) =>
      matchActor ! PlayerPlaysMatch(
        player,
        cards.map(c => CardHolderBuilder(c, deck.get)).toArray,
        new Array(0),
        new Array(0),
        new Array(0),
        CharmOfElementsRatio(
          deck.get.elementsRatio.fire,
          deck.get.elementsRatio.water,
          deck.get.elementsRatio.earth,
          deck.get.elementsRatio.air
        ),
        CharmOfElements(0, 0, 0, 0),
        h,
        true
      )
      context.stop(self)

    case DatabaseResult(msg, replyHas) =>
      msg match {
        case DeckAndCardsForUserWithGivenDeckId(d, c, user, deckId) =>
          cards = c
          deck = d
          if (cards.isEmpty | deck.isEmpty) {
            matchActor ! CannotComposePlayer(player)
            context.stop(self)
            // playerServiceHandler ! CannotComposePlayer(player.nickname)
          } else {
            playerServiceHandler ! ServiceModuleMessage(GetPlayerInMatchHandler(matchActor), ServiceHandlersModules.cardGameMatch)
          }
      }

    case t =>
      log.error("Unknown message: " + t)
  }
}

