package cz.noveit.games.cardgame.engine.game.session

import akka.actor.ActorRef
import cz.noveit.games.cardgame.engine.game.TableState

/**
 * Created by Wlsek on 26.8.2014.
 */
/* --------------------------------------- FSM Data ------------------------------------------------- */
trait MatchData

case class MatchStateWaitingForInitialization(tableState: TableState) extends MatchData

case class MatchStateWaitingForAck(originalState: State, state: State, newData: MatchData, ackUserLeft: List[String]) extends MatchData

//case class StateAckParameters
case class MatchStateWaitingForPlayerRolls(tableState: TableState, rolls: List[PlayerRoll]) extends MatchData
//case class TableStateWithPlayerRolls(tableState: TableState, rolls: List[PlayerRoll]) extends Data

case class MatchStateWaitingForPlayerEndTurn(tableOwnerLock: Option[String], tableState: TableState, cachedTableRequests: List[Pair[ActorRef, GameMessages]], endTurnUserLeft: List[String])  extends MatchData
//case class TableStateWithWaitForPlayerEndTurn(tableState: TableState, ended: List[String]) extends Data

case class MatchStateWaitingForAnimationEnds(tableState: TableState, endAnimationUserLeft: List[String], nextState: State) extends MatchData

