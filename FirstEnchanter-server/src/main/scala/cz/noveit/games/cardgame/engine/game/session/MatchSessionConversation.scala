package cz.noveit.games.cardgame.engine.game.session

import akka.actor.{ActorSelection, ActorLogging, ActorRef, Actor}
import cz.noveit.games.cardgame.WorldServices
import cz.noveit.games.cardgame.engine._
import cz.noveit.games.cardgame.engine.bots.{BotLeftConversation, BotJoinsConversation}
import cz.noveit.games.cardgame.engine.game.MatchQuerySetup

/**
 * Created by arnostkuchar on 03.11.14.
 */

case class MatchConversationMember(name: String, bot: Boolean)
class MatchSessionConversation(conversationController: ActorSelection, team1: List[MatchConversationMember], team2: List[MatchConversationMember]) extends Actor with ActorLogging {

  val team1Conversation = Conversation(self.toString() + "| " + team1.map(p => p.name.toString + " ") + "| " + System.currentTimeMillis().toString, self.toString() + "| " + team1.map(p => p.toString + " ") + "|", "Server", ConversationAutomaticCreationType.ConversationAutomaticCreationTypeBySystem, ConversationType.GameAllyConversationType, false, false, System.currentTimeMillis())
  val team2Conversation = Conversation(self.toString() + "| " + team2.map(p => p.name.toString + " ") + "| " + System.currentTimeMillis().toString, self.toString() + "| " + team2.map(p => p.toString + " ") + "|", "Server", ConversationAutomaticCreationType.ConversationAutomaticCreationTypeBySystem, ConversationType.GameAllyConversationType, false, false, System.currentTimeMillis())
  val allConversation   = Conversation(self.toString() + "|all| "  + System.currentTimeMillis().toString, self.toString() + "all", "Server", ConversationAutomaticCreationType.ConversationAutomaticCreationTypeBySystem, ConversationType.GameConversationAll, false, false, System.currentTimeMillis())

  conversationController ! CreateConversation(self, team1Conversation)
  conversationController ! CreateConversation(self, team2Conversation)
  conversationController ! CreateConversation(self, allConversation)

  def receive = {
    case ConversationCreationFailed(reason, conversation) =>
      log.error("Cannot create conversation {} for match with reason {}.", conversation, reason)

    case JoinConversationAutomaticallyFailed(conversationId) =>
      log.error("Cannot create conversation {} for match with reason {}.", conversationId)

    case JoinConversationAutomaticallySuccessful(conversationId, member) =>
      log.info("Player {} joined {} successful", member, conversationId)
      if((team1 ::: team2).exists(p => p.name == member && p.bot)) {
        context.actorSelection("/user/" + WorldServices.botConversationService) ! BotJoinsConversation(conversationId, member)
      }

    case ConversationCreated(conversation) =>
      if(conversation.id == team1Conversation.id) {
        team1.map(p => conversationController ! JoinConversationAutomatically(self, team1Conversation.id, p.name))
      } else if (conversation.id == team2Conversation.id) {
        team2.map(p => conversationController ! JoinConversationAutomatically(self, team2Conversation.id, p.name))
      } else if(conversation.id == allConversation.id) {
        (team1 ::: team2).map(p => conversationController ! JoinConversationAutomatically(self, allConversation.id, p.name))
      }

    case t =>
      log.error("MatchSessionConversation received unknown message!")
  }

  override def postStop {
    team1.map(p => {
      if(p.bot) {
        context.actorSelection("/user/" + WorldServices.botConversationService) ! BotLeftConversation(team1Conversation.id, p.name)
        context.actorSelection("/user/" + WorldServices.botConversationService) ! BotLeftConversation(allConversation.id, p.name)
      }
    })

    team2.map(p => {
      if(p.bot) {
        context.actorSelection("/user/" + WorldServices.botConversationService) ! BotLeftConversation(team2Conversation.id, p.name)
        context.actorSelection("/user/" + WorldServices.botConversationService) ! BotLeftConversation(allConversation.id, p.name)
      }
    })
  }
}
