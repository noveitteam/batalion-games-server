package cz.noveit.games.cardgame.engine.game.session

/**
 * Created by Wlsek on 26.8.2014.
 */
/* --------------------------------------- FSM States ------------------------------------------------- */
trait State

case object INITIALIZING_MATCH extends State

case object PLAYER_STARTS_ROLL extends State

case object FORTUNE_ROLL extends State

case object ATTACKER_TEAM_PLAY extends State

case object DEFENDER_TEAM_COUNTER_PLAY extends State

case object ATTACKER_TEAM_ATTACK extends State

case object DEFENDER_TEAM_DEFENDS extends State

case object EVALUATION_AND_ANIMATION extends State

case object MATCH_ENDED extends State

case object STATE_ACK extends State