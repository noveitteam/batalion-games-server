package cz.noveit.games.cardgame.engine.game.session

import cz.noveit.games.cardgame.engine.card._
import cz.noveit.games.cardgame.engine.handlers.helpers.serialization.{CharmOfElementsToProto, CardPositionToProto}
import cz.noveit.games.cardgame.engine.player.CharmOfElements
import cz.noveit.games.cardgame.recording.XmlRecording
import scala.collection.JavaConverters._

/**
 * Created by arnostkuchar on 23.09.14.
 */

trait TableStateChange extends XmlRecording {
  def toProtoMessage: com.google.protobuf.Message
}

case class StayOnTable(position: InHandCardPosition, card: CardHolder) extends TableStateChange {
  def toProtoMessage = {
    cz.noveit.proto.firstenchanter.FirstEnchanterMatch.StayOnTable
      .newBuilder
      .setPosition(
        cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardPosition
          .newBuilder
          .setPlace(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardPosition.CardPositionPlace.IN_HAND)
          .setPlaceId(position.positionId)
          .setPlayer(position.player)
          .build()
      )
      .build
  }

  def toXml = <cardStaysOnTable id={card.id} player={position.player} index={position.positionId.toString}></cardStaysOnTable>
}

case class PlayerPoolReceivedDamage(players: List[String], value: Int, from: CardPosition) extends TableStateChange {
  def toProtoMessage = {
    cz.noveit.proto.firstenchanter.FirstEnchanterMatch.PlayerPoolReceivedDamage
      .newBuilder
      .setValue(value)
      .addAllPlayers(players.asJava)
      .setFrom(CardPositionToProto(from))
      .build
  }

  def toXml = <playerPoolReceivedDamage value={value.toString}>
    <card player={from.player} index={from.positionId.toString}></card>
    <players>
      {players.map(p => <player nickname={p}></player>)}
    </players>
  </playerPoolReceivedDamage>
}

case class PlayerPoolReceivedHeal(players: List[String], value: Int, from: CardPosition) extends TableStateChange {
  def toProtoMessage = {
    cz.noveit.proto.firstenchanter.FirstEnchanterMatch.PlayerPoolReceivedHeal
      .newBuilder
      .setValue(value)
      .addAllPlayers(players.asJava)
      .setFrom(
        from match {
          case p: InHandCardPosition =>
            cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardPosition
              .newBuilder
              .setPlace(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardPosition.CardPositionPlace.IN_HAND)
              .setPlaceId(p.positionId)
              .setPlayer(p.player)
              .build()

          case p: OnTableCardPosition =>
            cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardPosition
              .newBuilder
              .setPlace(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardPosition.CardPositionPlace.ON_TABLE)
              .setPlaceId(p.positionId)
              .setPlayer(p.player)
              .build()
        }
      )
      .build
  }

  def toXml = <playerPoolReceivedHeal value={value.toString}>
    <card player={from.player} index={from.positionId.toString}></card>
    <players>
      {players.map(p => <player nickname={p}></player>)}
    </players>
  </playerPoolReceivedHeal>
}

case class PlayersLost(players: List[String]) extends TableStateChange {
  def toProtoMessage = {
    cz.noveit.proto.firstenchanter.FirstEnchanterMatch.PlayersLost
      .newBuilder()
      .addAllPlayers(players.asJava)
      .build
  }

  def toXml = <playersLost>
    {players.map(p => <player nickname={p}></player>)}
  </playersLost>
}


case class CardStripEquipment(card: CardPosition, equipmentTags: List[String]) extends TableStateChange {
  def toProtoMessage = {
    cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardStripEquipment
      .newBuilder()
      .setPosition(CardPositionToProto(card))
     /* .setEquipment(
        equip match {
          case e: CardStateEffectSourceWeapon =>
            cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardEquipment.WEAPON

          case e: CardStateEffectSourceRing =>
            cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardEquipment.SHIELD

          case e: CardStateEffectSourceShield =>
            cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardEquipment.RING
        }
      )*/
      .build
  }

  def toXml = <cardStripEquipmement>
    <card player={card.player} index={card.positionId.toString}></card>
    <equip>
      {/* equip.toString */}
    </equip>
  </cardStripEquipmement>
}

case class AddedEquipmentToCard(card: CardPosition, equipmentTags: List[String]) extends TableStateChange {
  def toProtoMessage = {
    cz.noveit.proto.firstenchanter.FirstEnchanterMatch.AddedEquipmentToCard
      .newBuilder()
      .setPosition(CardPositionToProto(card))
      /*.setEquipment(
        equip match {
          case e: CardStateEffectSourceWeapon =>
            cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardEquipment.WEAPON

          case e: CardStateEffectSourceRing =>
            cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardEquipment.SHIELD

          case e: CardStateEffectSourceShield =>
            cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardEquipment.RING
        }
      )*/
      .build
  }

  def toXml = <addedEquipmentToCard>
    <card player={card.player} index={card.positionId.toString}></card>
    <equip>
      {/*equip.toString*/}
    </equip>
  </addedEquipmentToCard>
}

case class CardChangeAttack(card: CardPosition, changeBy: Int) extends TableStateChange {
  def toProtoMessage = {
    cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardChangeAttack
      .newBuilder()
      .setPosition(CardPositionToProto(card))
      .setValue(changeBy)
      .build
  }

  def toXml = <cardChangeAttack player={card.player} index={card.positionId.toString} by={changeBy.toString}></cardChangeAttack>
}

case class CardChangeDefense(card: CardPosition, changeBy: Int) extends TableStateChange {
  def toProtoMessage = {
    cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardChangeDefense
      .newBuilder()
      .setPosition(CardPositionToProto(card))
      .setValue(changeBy)
      .build
  }

  def toXml = <cardChangeDefense player={card.player} index={card.positionId.toString} by={changeBy.toString}></cardChangeDefense>
}

case class CardBuffed(card: CardPosition) extends TableStateChange {
  def toProtoMessage = {
    cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardBuffed
      .newBuilder()
      .setPosition(CardPositionToProto(card))
      .build
  }

  def toXml = <cardBuffed player={card.player} index={card.positionId.toString}></cardBuffed>
}


case class CardConsumed(card: CardPosition, cardHolder: CardHolder) extends TableStateChange {
  def toProtoMessage = {
    cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardConsumed
      .newBuilder()
      .setPosition(CardPositionToProto(card))
      .build
  }

  def toXml = <cardConsumed id={cardHolder.id} player={card.player} index={card.positionId.toString}></cardConsumed>
}

case class CardDestroyerBy(defender: CardPosition, attacker: CardPosition) extends TableStateChange {
  def toProtoMessage = {
    cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardDestroyedBy
      .newBuilder()
      .setPosition(CardPositionToProto(defender))
      .setDestroyedBy(CardPositionToProto(attacker))
      .build
  }

  def toXml = <cardDestroyed>
    <defender player={defender.player} index={defender.positionId.toString}></defender>
    <attacker player={attacker.player} index={attacker.positionId.toString}></attacker>
  </cardDestroyed>
}

case class CardDefendendAgainst(defender: CardPosition, attacker: CardPosition) extends TableStateChange {
  def toProtoMessage = {
    cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardDefendedAgainst
      .newBuilder()
      .setPosition(CardPositionToProto(defender))
      .setAgainst(CardPositionToProto(attacker))
      .build
  }

  def toXml = <cardDefended>
    <defender player={defender.player} index={defender.positionId.toString}></defender>
    <attacker player={attacker.player} index={attacker.positionId.toString}></attacker>
  </cardDefended>
}

case class CardAttackCancel(canceledAttack: CardPosition, withPosition: CardPosition) extends TableStateChange {
  def toProtoMessage = {
    cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardAttackCancel
      .newBuilder()
      .setAttacker(CardPositionToProto(canceledAttack))
      .setCanceledBy(CardPositionToProto(withPosition))
      .build
  }

  def toXml = <canceledAttack>
    <attacker player={canceledAttack.player} index={canceledAttack.positionId.toString}></attacker>
    <canceledBy player={withPosition.player} index={withPosition.positionId.toString}></canceledBy>
  </canceledAttack>
}

case class CardGenerated(cardPosition: CardPosition, cardHolder: CardHolder) extends TableStateChange {
  def toProtoMessage = {
    cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardGenerated
      .newBuilder()
      .setPosition(CardPositionToProto(cardPosition))
      .setCard(
        cz.noveit.proto.firstenchanter.FirstEnchanterCards.CountableCardWithSkin
          .newBuilder
          .setCount(1)
          .setCardId(cardHolder.id)
          .setSkinId(cardHolder.skinId.map(s => s).getOrElse(-1))
          .build()
      )
      .build
  }

  def toXml = <cardGenerated id={cardHolder.id} player={cardPosition.player} index={cardPosition.positionId.toString}></cardGenerated>
}

case class ElementsGenerated(charm: CharmOfElements, player: String) extends TableStateChange {
  def toProtoMessage = {
    cz.noveit.proto.firstenchanter.FirstEnchanterMatch.ElementsGenerated
      .newBuilder()
      .setCharm(CharmOfElementsToProto(charm))
      .setPlayer(player)
      .build()
  }

  def toXml = <elementsGenerated fire={charm.fire.toString} water={charm.water.toString} air={charm.air.toString} earth={charm.earth.toString}>
    <to>
      {player}
    </to>
  </elementsGenerated>
}


case class ElementsAbsorbed(charm: CharmOfElements, player: String) extends TableStateChange {
  def toProtoMessage = {
    cz.noveit.proto.firstenchanter.FirstEnchanterMatch.ElementsAbsorbed
      .newBuilder()
      .setCharm(CharmOfElementsToProto(charm))
      .setPlayer(player)
      .build()
  }

  def toXml = <elementsAbsorbed fire={charm.fire.toString} water={charm.water.toString} air={charm.air.toString} earth={charm.earth.toString}>
    <from>
      {player}
    </from>
  </elementsAbsorbed>
}

case class ElementsTransformed(charm: CharmOfElements, from: String, to: String) extends TableStateChange {
  def toProtoMessage = {
    cz.noveit.proto.firstenchanter.FirstEnchanterMatch.ElementsTransformed
      .newBuilder()
      .setCharm(CharmOfElementsToProto(charm))
      .setFrom(from)
      .setTo(to)
      .build()
  }

  def toXml = <elementsTransformed fire={charm.fire.toString} water={charm.water.toString} air={charm.air.toString} earth={charm.earth.toString}>
    <from>
      {from}
    </from>
    <to>
      {to}
    </to>
  </elementsTransformed>
}