package cz.noveit.games.cardgame.engine.handlers

import cz.noveit.proto.serialization.{MessageSerializer, MessageEvent}
import akka.actor.{Props, ActorLogging, Actor}
import cz.noveit.games.cardgame.adapters._
import cz.noveit.database.{DatabaseCommand, DatabaseResult}
import cz.noveit.proto.firstenchanter.FirstEnchanterAccountDetails._
import org.bouncycastle.util.encoders.Base64
import cz.noveit.connector.websockets.WebSocketsContext
import cz.noveit.proto.firstenchanter.FirstEnchanterAccountDetails.GetAvatarList
import cz.noveit.proto.serialization.MessageEvent
import cz.noveit.games.cardgame.adapters.GetUnlockedAvatarsForUser
import cz.noveit.database.DatabaseResult
import cz.noveit.database.DatabaseCommand
import cz.noveit.games.cardgame.adapters.QueriedAccountState
import cz.noveit.games.cardgame.adapters.QueryAccountState
import cz.noveit.games.utils.ImageLoader
import cz.noveit.games.cardgame.engine.{ServiceModuleMessage, CrossHandlerMessage}

/**
 * Created by Wlsek on 19.2.14.
 */

case object GetUserDetails extends CrossHandlerMessage

trait AccountDetailsHandler extends PlayerService {
}


class AccountDetailsActor(val module: String, parameters: PlayerServiceParameters) extends CardsHandler with Actor with ActorLogging with MessageSerializer with ImageLoader {

  val playerHandler = parameters.playerActor
  val playerContext = parameters.context
  val player = parameters.player

  val databaseAdapter = context.actorOf(
    Props(
      classOf[AccountDetailsDatabaseAdapter],
      context.actorSelection("/user/databaseController"),
      parameters.databaseParams("databaseName"),
      parameters.databaseParams("userCollection"),
      parameters.databaseParams("userUnlockedCollection"),
      parameters.databaseParams("avatarVersionCollection"),
      parameters.databaseParams("transactionsCollection")
    ))

  def receive = {
    case msg: MessageEvent =>
      msg.message match {
        case m: GetAccountState =>
          databaseAdapter ! DatabaseCommand(QueryAccountState(player.nickname), msg.replyHash)

        case m: GetAvatarList =>
          databaseAdapter ! DatabaseCommand(GetUnlockedAvatarsForUser(player.nickname, m.getDevice.getNumber), msg.replyHash)

        case m: ChangeAvatar =>
          databaseAdapter ! DatabaseCommand(ChangeAvatarForUser(player.nickname, containsFolderId("./resources/avatars/standard/", m.getAvatarId), m.getAvatarId), msg.replyHash)
      }

    case msg: DatabaseResult => {
      msg.result match {
        case QueriedAccountState(ec, ssd, ws) =>
          val as = AccountState
            .newBuilder()
            .setEmailValidated(ec)
            .setSelectedStartingDeck(ssd)
            .setInGameMoney(ws)
            .build()

          val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(as, module, msg.replyHash)).toByteArray), "UTF-8")
          playerContext.asInstanceOf[WebSocketsContext].send(encodedString)

        case UnlockedAvatars(avatars) =>
          var al = AvatarList.newBuilder()
          avatars.map(id => {
            al.addListOfIds(id)
          })

          val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(al.build(), module, msg.replyHash)).toByteArray), "UTF-8")
          playerContext.asInstanceOf[WebSocketsContext].send(encodedString)

        case ChangeAvatarForUserFailed =>
          val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(ChangeAvatarSuccessful.newBuilder().build(), module, msg.replyHash)).toByteArray), "UTF-8")
          playerContext.asInstanceOf[WebSocketsContext].send(encodedString)

        case ChangeAvatarForUserSuccessful =>
          val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(ChangeAvatarFailed.newBuilder().build(), module, msg.replyHash)).toByteArray), "UTF-8")
          playerContext.asInstanceOf[WebSocketsContext].send(encodedString)
      }
    }

    case msg: ServiceModuleMessage =>
      msg.message match {
        case GetUserDetails => {
          sender ! parameters.player
        }
      }
  }
}