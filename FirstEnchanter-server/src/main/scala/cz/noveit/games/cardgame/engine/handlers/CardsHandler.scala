package cz.noveit.games.cardgame.engine.handlers

import akka.actor.{ActorRef, Props, Actor, ActorLogging}
import cz.noveit.proto.serialization.{MessageSerializer, MessageEvent}
import cz.noveit.games.cardgame.adapters._
import cz.noveit.proto.firstenchanter.FirstEnchanterCards._
import cz.noveit.database._
import org.bouncycastle.util.encoders.Base64
import cz.noveit.connector.websockets.WebSocketsContext
import cz.noveit.games.utils.ImageLoader
import cz.noveit.proto.firstenchanter.FirstEnchanterCards.StartingDeckItem
import cz.noveit.games.cardgame.adapters.StarterDecks
import cz.noveit.proto.serialization.MessageEvent
import scala.collection.JavaConverters._
import cz.noveit.games.cardgame.engine.{ServiceModuleMessage, CrossHandlerMessage}
import cz.noveit.games.cardgame.adapters.DeleteDeck
import cz.noveit.games.cardgame.adapters.FilteredAvailableCards
import cz.noveit.games.cardgame.adapters.GetAvailableCardsByFiltering
import cz.noveit.games.cardgame.adapters.DeckLightList
import cz.noveit.games.cardgame.adapters.SaveEditedDeck
import cz.noveit.games.cardgame.adapters.ModuleVersion
import cz.noveit.games.cardgame.adapters.SaveNewDeck
import cz.noveit.games.cardgame.adapters.Deck
import cz.noveit.games.cardgame.adapters.StarterDecks
import cz.noveit.games.cardgame.adapters.ReturnUpdatedCard
import cz.noveit.games.cardgame.adapters.SetStartingDeck
import cz.noveit.database.DatabaseCommand
import cz.noveit.games.cardgame.adapters.ElementsRating
import cz.noveit.games.cardgame.adapters.GetUpdatedCard
import cz.noveit.proto.serialization.MessageEvent
import cz.noveit.games.cardgame.adapters.GetDeckLightList
import cz.noveit.database.DatabaseResult

import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import scala.concurrent.Await
import cz.noveit.proto.firstenchanter.FirstEnchanterCards.CardDeck.ElementsRatio
import cz.noveit.games.cardgame.engine.resources.{CardSkinFromFile, DeckIconFromFile, GetDeckIconFromFile, GetCardSkinFromFile}
import cz.noveit.proto.firstenchanter.FirstEnchanterCards.AvailableCards

/**
 * Created by Wlsek on 10.2.14.
 */


case class AskOwnershipOfDeckForPlayer(player: String, deckId: String) extends CrossHandlerMessage with DatabaseCommandMessage

case class AskOwnershipResult(player: String, deckId: String, isOwner: Boolean) extends CrossHandlerMessage with DatabaseCommandResultMessage

trait CardsHandler extends PlayerService {
}

case object GetCardDatabaseAdapter extends CrossHandlerMessage
case class CardDatabaseAdapterFound(h: ActorRef)  extends CrossHandlerMessage

class CardsHandlerActor(val module: String, parameters: PlayerServiceParameters) extends CardsHandler with Actor with ActorLogging with MessageSerializer with ImageLoader {

  val playerHandler = parameters.playerActor
  val playerContext = parameters.context
  val player = parameters.player

  lazy val databaseAdapter = context.actorOf(
    Props(classOf[CardDatabaseAdapter],
      context.actorSelection("/user/databaseController"),
      parameters.databaseParams("databaseName"),
      parameters.databaseParams("cardsCollection"),
      parameters.databaseParams("decksCollection"),
      parameters.databaseParams("deckIconVersionCollection"),
      parameters.databaseParams("cardSkinVersionCollection"),
      parameters.databaseParams("userHasCardsCollection"),
      parameters.databaseParams("userUnlockedCollection"),
      parameters.databaseParams("userCollection")
    ))

  val deckIconLoader = context.actorSelection("/user/deckIconLoader")
  val cardSkinLoader = context.actorSelection("/user/cardSkinLoader")

  def receive = {
    case msg: MessageEvent =>
      msg.message match {

        case m: GetCardModuleVersion => {
          databaseAdapter ! DatabaseCommand(GetModuleVersions, msg.replyHash)
        }

        case m: UpdateCard => {
          databaseAdapter ! DatabaseCommand(GetUpdatedCard(m.getCardId), msg.replyHash)
        }

        case m: UpdateSkin => {
          cardSkinLoader ! GetCardSkinFromFile(m.getDevice.getNumber, m.getSkinId, msg.replyHash)
        }

        case m: UpdateDeckIcon => {
          deckIconLoader ! GetDeckIconFromFile(m.getDevice.getNumber, m.getIconId, msg.replyHash)
        }

        case m: GetDeckList => {
          databaseAdapter ! DatabaseCommand(GetDeckLightList(player.nickname), msg.replyHash)
        }

        case m: CreateDeck => {
          databaseAdapter ! DatabaseCommand(
            SaveNewDeck(
              Deck(
                "",
                m.getDeck.getName,
                m.getDeck.getIconId,
                ElementsRating(
                  m.getDeck.getElementsRatio.getWater,
                  m.getDeck.getElementsRatio.getFire,
                  m.getDeck.getElementsRatio.getEarth,
                  m.getDeck.getElementsRatio.getAir,
                0
                ),
                m.getDeck.getCardsList.asScala.toList.map(cws => {
                  cz.noveit.games.cardgame.adapters.CountableCardWithSkin(cws.getCardId, cws.getCount, Some(cws.getSkinId))
                }),
                player.nickname
              )
            )
            , msg.replyHash)
        }

        case m: RemoveDeck => {
          databaseAdapter ! DatabaseCommand(
            DeleteDeck(m.getDeckId, player.nickname)
            , msg.replyHash)
        }

        case m: SaveDeck => {
          databaseAdapter ! DatabaseCommand(
            SaveEditedDeck(
              Deck(
                m.getDeck.getDeckId,
                m.getDeck.getName,
                m.getDeck.getIconId,
                ElementsRating(
                  m.getDeck.getElementsRatio.getWater,
                  m.getDeck.getElementsRatio.getFire,
                  m.getDeck.getElementsRatio.getEarth,
                  m.getDeck.getElementsRatio.getAir,
                  0
                ),
                m.getDeck.getCardsList.asScala.toList.map(cws => {
                  cz.noveit.games.cardgame.adapters.CountableCardWithSkin(cws.getCardId, cws.getCount, Some(cws.getSkinId))
                }),
                player.nickname
              )
            )
            , msg.replyHash)
        }

        case m: GetAvailableCard => {
          databaseAdapter ! DatabaseCommand(GetAvailableCardsByFiltering(
            m.getRarityFilterList.asScala.map(r => r.getNumber).toList,
            m.getTagsFilterList.asScala.toList,
            m.getElementsFilterList.asScala.map(e => e.getNumber).toList,
            m.getNameRegExp,
            m.getTagRegExp,
            m.getSkip,
            m.getLimit,
            m.getCostLimit,
            player.nickname
          ), msg.replyHash)
        }

        case m: GetStartingDeckList => {
          databaseAdapter ! DatabaseCommand(GetStarterDecksList, msg.replyHash)
        }

        case m: SelectStartingDeck => {
          databaseAdapter ! DatabaseCommand(SetStartingDeck(m.getDeckId, player.nickname), msg.replyHash)
        }

        case m: GetDeck => {
          databaseAdapter ! DatabaseCommand(GetDeckAndCardsIdForUserWithGivenDeckId(player.nickname, m.getDeckId), msg.replyHash)
        }

        case m: GetAvailableDeckIcons => {
          databaseAdapter ! DatabaseCommand(GetAvailableDeckIconsFromDatabase(player.nickname), msg.replyHash)
        }

        case m: GetAvailableCardSkins => {
          databaseAdapter ! DatabaseCommand(GetAvailableCardSkinsFromDatabase(player.nickname), msg.replyHash)
        }
                       /*
        case m: GetCardDetail => {
          databaseAdapter ! DatabaseCommand(GetCard(player.nickname, m.getCardId), msg.replyHash)
        }
                */
        case t => throw new UnsupportedOperationException(t.toString())
      }

    case msg: ServiceModuleMessage => {
      msg.message match {
        case m: AskOwnershipOfDeckForPlayer =>
          try {
            implicit val timeout = Timeout(5 seconds)
            val future = databaseAdapter ? DatabaseCommand(msg.message.asInstanceOf[DatabaseCommandMessage], msg.replyHash)
            val ownershipResult = Await.result(future, timeout.duration).asInstanceOf[DatabaseResult]
            sender ! ServiceModuleMessage(ownershipResult.result.asInstanceOf[CrossHandlerMessage], "", msg.replyHash)
          } catch {
            case t: Throwable =>
              log.error("Asking database adapter failed. {}", t.toString)
          }

      }
    }

    case r: DatabaseResult =>
      r.result match {
        case m: StarterDecks =>
          val listBuilder = StartingDeckList.newBuilder()
          m.decks.foreach(d => {
            listBuilder.addDecks(
              StartingDeckItem
                .newBuilder()
                .setDeckId(d.id)
                .setName(d.name)
                .setIconId(d.iconId)
                .build()
            )
          })

          val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(listBuilder.build(), module, r.replyHash)).toByteArray), "UTF-8")
          playerContext.asInstanceOf[WebSocketsContext].send(encodedString)

        case m: ModuleVersion => {
          val cmvBuilder = CardModuleVersion.newBuilder()

          m.cardsVersion.map(p => {
            cmvBuilder.addCardVersions(CardModuleVersion.CardVersion.newBuilder().setCardId(p.cardId).setVersion(p.version).build())
          })

          m.deckIconsVersion.map(p => {
            cmvBuilder.addDeckIconVersions(CardModuleVersion.DeckIconVersion.newBuilder().setIconId(p.iconId).setVersion(p.version).build())
          })

          m.cardSkinsVersion.map(p => {
            cmvBuilder.addCardSkinVersions(CardModuleVersion.CardSkinVersion.newBuilder().setSkinId(p.skinId).setVersion(p.version).build())
          })

          val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(cmvBuilder.build(), module, r.replyHash)).toByteArray), "UTF-8")
          playerContext.asInstanceOf[WebSocketsContext].send(encodedString)

        }

        case AvailableCardSkinsFromDatabase(skins) => {
          val builder = AvailableCardSkins.newBuilder()
          skins.map(s => builder.addCardSkinId (s))
          val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(builder.build(), module, r.replyHash)).toByteArray), "UTF-8")
          playerContext.asInstanceOf[WebSocketsContext].send(encodedString)
        }

        case AvailableDeckIconsFromDatabase(deckIcons) => {
          val builder = AvailableDeckIcons.newBuilder()
          deckIcons.map(s => builder.addDeckIconId(s))
          val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(builder.build(), module, r.replyHash)).toByteArray), "UTF-8")
          playerContext.asInstanceOf[WebSocketsContext].send(encodedString)
        }

        case ReturnUpdatedCard(card) => {
          val c = cz.noveit.proto.firstenchanter.FirstEnchanterCards.Card.newBuilder
            .setId(card.id)
            .setName(card.name)
            .setAttack(card.attack)
            .setDefense(card.defense)
            .setVersion(card.version)
            .setRarity(CardRarity.valueOf(card.rarity))
            .setImageId(card.imageId)

          card.tags.map(t => c.addTags(t))
          card.description.map(d => c.addLocalizedDescription(LocalizedDescription.newBuilder.setLocale(d.locale).setDescription(d.description).build))
          card.price.map(p => c.addPrice(cz.noveit.proto.firstenchanter.FirstEnchanterCards.CardPrice.newBuilder.setElement(CardElements.valueOf(p.element)).setPrice(p.price).build))

          val uc = UpdatedCard.newBuilder().setCard(c.build())

          val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(uc.build(), module, r.replyHash)).toByteArray), "UTF-8")
          playerContext.asInstanceOf[WebSocketsContext].send(encodedString)
        }

        case DeckLightList(decks) => {
          val dl = DeckList.newBuilder()
          decks.map(d => {
            dl.addDecks(
              CardDeckLight.newBuilder()
                .setName(d.name)
                .setIconId(d.iconId)
                .setDeckId(d.id)
                .build()
            )
          })

          val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(dl.build(), module, r.replyHash)).toByteArray), "UTF-8")
          playerContext.asInstanceOf[WebSocketsContext].send(encodedString)
        }

        case m: DeckAndCardsIdForUserWithGivenDeckId => {
              val msgBuilder = DeckReturned.newBuilder
          m.deck.map(d => {
            val deckBuilder = CardDeck.newBuilder
            deckBuilder
              .setName(d.name)
              .setIconId(d.icon)
              .setDeckId(d.id)
              .setElementsRatio(
                ElementsRatio
                  .newBuilder
                  .setWater(d.elementsRatio.water)
                  .setFire(d.elementsRatio.fire)
                  .setEarth(d.elementsRatio.earth)
                  .setAir(d.elementsRatio.air)
                  .build()
              )

            d.cards.map(cws => {
              deckBuilder
                .addCards({
                val cwsBuilder =
                  cz.noveit.proto.firstenchanter.FirstEnchanterCards.CountableCardWithSkin.newBuilder

                cwsBuilder.setCardId(cws.cardId)
                cwsBuilder.setCount(cws.count)
                cws.skinId.map(skin => cwsBuilder.setSkinId(skin))
                cwsBuilder.build
              })
            })

            msgBuilder.setDeck(deckBuilder.build)
          })

          val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(msgBuilder.build, module, r.replyHash)).toByteArray), "UTF-8")
          playerContext.asInstanceOf[WebSocketsContext].send(encodedString)
        }


        case SaveNewDeckSuccessful => {
          val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(DeckCreationSuccessful.newBuilder().build(), module, r.replyHash)).toByteArray), "UTF-8")
          playerContext.asInstanceOf[WebSocketsContext].send(encodedString)
        }

        case SaveNewDeckFailed => {
          val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(DeckCreationFailed.newBuilder().build(), module, r.replyHash)).toByteArray), "UTF-8")
          playerContext.asInstanceOf[WebSocketsContext].send(encodedString)
        }

        case SaveEditedDeckSuccessful => {
          val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(SaveDeckSuccessful.newBuilder().build(), module, r.replyHash)).toByteArray), "UTF-8")
          playerContext.asInstanceOf[WebSocketsContext].send(encodedString)
        }

        case SaveEditedDeckFailed => {
          val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(SaveDeckFailed.newBuilder().build(), module, r.replyHash)).toByteArray), "UTF-8")
          playerContext.asInstanceOf[WebSocketsContext].send(encodedString)
        }

        case DeleteDeckSuccessful => {
          val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(RemoveDeckSuccessful.newBuilder().build(), module, r.replyHash)).toByteArray), "UTF-8")
          playerContext.asInstanceOf[WebSocketsContext].send(encodedString)
        }

        case DeleteDeckFailed => {
          val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(RemoveDeckFailed.newBuilder().build(), module, r.replyHash)).toByteArray), "UTF-8")
          playerContext.asInstanceOf[WebSocketsContext].send(encodedString)
        }

        case SetStartingDeckFailed => {
          val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(SelectStartingDeckFailed.newBuilder().build(), module, r.replyHash)).toByteArray), "UTF-8")
          playerContext.asInstanceOf[WebSocketsContext].send(encodedString)
        }

        case SetStartingDeckSuccessful => {
          val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(SelectStartingDeckSuccessful.newBuilder().build(), module, r.replyHash)).toByteArray), "UTF-8")
          playerContext.asInstanceOf[WebSocketsContext].send(encodedString)
        }

        case card: cz.noveit.games.cardgame.adapters.Card => {
          val c = cz.noveit.proto.firstenchanter.FirstEnchanterCards.Card.newBuilder
            .setId(card.id)
            .setName(card.name)
            .setAttack(card.attack)
            .setDefense(card.defense)
            .setVersion(card.version)
            .setRarity(CardRarity.valueOf(card.rarity))
            .setImageId(card.imageId)

          card.tags.map(t => c.addTags(t))
          card.description.map(d => c.addLocalizedDescription(LocalizedDescription.newBuilder.setLocale(d.locale).setDescription(d.description).build))
          card.price.map(p => c.addPrice(cz.noveit.proto.firstenchanter.FirstEnchanterCards.CardPrice.newBuilder.setElement(CardElements.valueOf(p.element)).setPrice(p.price).build))

          val uc = UpdatedCard.newBuilder().setCard(c.build())

          val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(uc.build(), module, r.replyHash)).toByteArray), "UTF-8")
          playerContext.asInstanceOf[WebSocketsContext].send(encodedString)
        }

        case FilteredAvailableCards(cards) =>
          val ac = AvailableCards.newBuilder()

          cards.map(c => ac.addCardIds(c))
          val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(ac.build(), module, r.replyHash)).toByteArray), "UTF-8")
          playerContext.asInstanceOf[WebSocketsContext].send(encodedString)

        case t => throw new UnsupportedOperationException(t.toString())
      }

    case msg: DeckIconFromFile =>
      val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(
        cz.noveit.proto.firstenchanter.FirstEnchanterCards.UpdatedDeckIcon.newBuilder
          .setBase64Image(msg.base64image)
          .setIconId(msg.id)
          .setVersion(msg.version)
          .build(),
        module, msg.replyHash
      )).toByteArray), "UTF-8")
      playerContext.asInstanceOf[WebSocketsContext].send(encodedString)

    case msg: CardSkinFromFile =>
      val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(
        cz.noveit.proto.firstenchanter.FirstEnchanterCards.UpdatedSkin.newBuilder
          .setBase64Image(msg.base64image)
          .setSkinId(msg.id)
          .setVersion(msg.version)
          .build(),
        module, msg.replyHash
      )).toByteArray), "UTF-8")
      playerContext.asInstanceOf[WebSocketsContext].send(encodedString)

    case t => throw new UnsupportedOperationException(t.toString())
  }

  def iconForId(id: Int): Array[Byte] = {
    loadImage("./resources/deckicons/standard", id).map(bi => convertImageToByteArray(bi).getOrElse(new Array[Byte](0))).getOrElse(new Array[Byte](0))
  }
}