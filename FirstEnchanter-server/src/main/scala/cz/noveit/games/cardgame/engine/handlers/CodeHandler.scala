package cz.noveit.games.cardgame.engine.handlers

import akka.actor.{ActorLogging, Actor}
import cz.noveit.proto.serialization.MessageEvent

/**
 * Created by Wlsek on 10.2.14.
 */
trait CodeHandler extends PlayerService {
}


class CodeHandlerActor(val module: String, parameters: PlayerServiceParameters) extends CardsHandler with Actor with ActorLogging {

  val playerHandler = parameters.playerActor
  val playerContext = parameters.context

  def receive = {
    case msg: MessageEvent =>
  }
}