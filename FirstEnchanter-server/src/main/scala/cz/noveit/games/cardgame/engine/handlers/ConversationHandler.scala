package cz.noveit.games.cardgame.engine.handlers

import cz.noveit.proto.serialization.{MessageSerializer, MessageEvent}
import akka.actor.{ActorRef, Props, Actor, ActorLogging}
import cz.noveit.games.cardgame.WorldServices
import cz.noveit.games.cardgame.engine._
import cz.noveit.connector.websockets.WebSocketsContext
import org.bouncycastle.util.encoders.Base64

import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import scala.concurrent.Await
import cz.noveit.games.cardgame.engine.JoinConversationByIdSuccessful
import cz.noveit.games.cardgame.engine.ServiceModuleMessage
import cz.noveit.games.cardgame.engine.Message
import cz.noveit.games.cardgame.engine.KickPlayerFromConversation
import cz.noveit.games.cardgame.engine.ConversationMembershipChanged
import cz.noveit.games.cardgame.engine.Conversation
import cz.noveit.games.cardgame.engine.SendMessageToPlayer
import cz.noveit.games.cardgame.engine.ConversationInviteDecline
import cz.noveit.proto.serialization.MessageEvent
import cz.noveit.games.cardgame.engine.JoinConversationByNameFailed
import cz.noveit.games.cardgame.engine.ListOfConversations
import cz.noveit.games.cardgame.engine.JoinConversationByNameSuccessful
import cz.noveit.games.cardgame.engine.BanPlayerFromConversation
import cz.noveit.games.cardgame.adapters.ConversationFound
import cz.noveit.games.cardgame.engine.UnBanPlayerFromConversation
import cz.noveit.games.cardgame.engine.ConversationHistory
import cz.noveit.games.cardgame.engine.PlayerConversationInvitation
import cz.noveit.games.cardgame.engine.JoinConversationByIdFailed
import cz.noveit.games.cardgame.engine.GetConversationHistory
import cz.noveit.games.cardgame.engine.JoinConversationByName
import cz.noveit.games.cardgame.engine.GetConversationInvitationsForPlayer
import cz.noveit.games.cardgame.engine.LeaveConversation
import cz.noveit.games.cardgame.engine.GetListOfBansForConversation
import cz.noveit.games.cardgame.engine.GetListOfConversationsForPlayer
import cz.noveit.games.cardgame.engine.SendMessageToConversation
import cz.noveit.games.cardgame.engine.JoinConversationById
import cz.noveit.games.cardgame.engine.InvitePlayerToJoinConversation
import cz.noveit.games.cardgame.engine.ConversationReceivedMessage

/**
 * Created by Wlsek on 15.5.14.
 */
class ConversationHandlerActor(val module: String, parameters: PlayerServiceParameters) extends CardsHandler with Actor with ActorLogging with MessageSerializer {

  val playerHandler = parameters.playerActor
  val playerContext = parameters.context

  val conversationController = context.actorSelection("/user/" + WorldServices.conversationService)
  conversationController ! GetListOfConversationsForPlayer(self, parameters.player.nickname, ConversationType.ConversationAll)
  var conversations: List[Conversation] = List()

  def receive = {
    case AddConversation(c) =>
      conversations = c :: conversations
      val message = cz.noveit.proto.firstenchanter.FirstEnchanterConversation.ConversationJoinSuccess
        .newBuilder()
        .setConversation(toProtobufConversation(c))
        .build()
      val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(message, module, "")).toByteArray), "UTF-8")
      playerContext.asInstanceOf[WebSocketsContext].send(encodedString)

    case ListOfConversations(c) => conversations = c

    case MessageEvent(message, module, replyHash, ctx) =>
      message match {
        case m: cz.noveit.proto.firstenchanter.FirstEnchanterConversation.JoinConversationById =>
          val handle = createResponseHandler((msg: Any) => {
            msg match {
              case JoinConversationByIdSuccessful(conversation, player) =>
                val message = cz.noveit.proto.firstenchanter.FirstEnchanterConversation.ConversationJoinSuccess
                  .newBuilder()
                  .setConversation(toProtobufConversation(conversation))
                  .build()
                val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(message, module, replyHash)).toByteArray), "UTF-8")
                playerContext.asInstanceOf[WebSocketsContext].send(encodedString)


              case JoinConversationByIdFailed(conversation) =>
                val message = cz.noveit.proto.firstenchanter.FirstEnchanterConversation.ConversationJoinFailed.newBuilder().build()
                val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(message, module, replyHash)).toByteArray), "UTF-8")
                playerContext.asInstanceOf[WebSocketsContext].send(encodedString)
            }
          })
          conversationController ! JoinConversationById(handle, m.getConversationId, parameters.player.nickname)

        case m: cz.noveit.proto.firstenchanter.FirstEnchanterConversation.JoinConversationByName =>
          val handle = createResponseHandler((msg: Any) => {
            msg match {
              case JoinConversationByNameSuccessful(conversation, player) =>
                val message = cz.noveit.proto.firstenchanter.FirstEnchanterConversation.ConversationJoinSuccess
                  .newBuilder()
                  .setConversation(toProtobufConversation(conversation))
                  .build()
                val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(message, module, replyHash)).toByteArray), "UTF-8")
                playerContext.asInstanceOf[WebSocketsContext].send(encodedString)

              case JoinConversationByNameFailed(conversation) =>
                val message = cz.noveit.proto.firstenchanter.FirstEnchanterConversation.ConversationJoinFailed.newBuilder().build()
                val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(message, module, replyHash)).toByteArray), "UTF-8")
                playerContext.asInstanceOf[WebSocketsContext].send(encodedString)
            }
          })
          conversationController ! JoinConversationByName(handle, m.getName, parameters.player.nickname)

        case m: cz.noveit.proto.firstenchanter.FirstEnchanterConversation.ConversationLeave =>
          conversationController ! LeaveConversation(m.getConversationId, parameters.player.nickname)

        case m: cz.noveit.proto.firstenchanter.FirstEnchanterConversation.GetConversationsList =>
          val handle = createResponseHandler((msg: Any) => {
            msg match {
              case ListOfConversations(conversations) =>
                val message = cz.noveit.proto.firstenchanter.FirstEnchanterConversation.ConversationsList.newBuilder()
                conversations.map(conversation => {
                  message.addConversations({
                    toProtobufConversation(conversation)
                  })
                })

                val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(message.build(), module, replyHash)).toByteArray), "UTF-8")
                playerContext.asInstanceOf[WebSocketsContext].send(encodedString)
            }
          })

          conversationController ! GetListOfConversationsForPlayer(handle, parameters.player.nickname, m.getConversationType match {
            case cz.noveit.proto.firstenchanter.FirstEnchanterConversation.GetConversationsList
              .ConversationType
              .ChatRoomConversationType =>
              ConversationType.ChatRoomConversationType
            case cz.noveit.proto.firstenchanter.FirstEnchanterConversation.GetConversationsList
              .ConversationType
              .GameAllyConversationType =>
              ConversationType.GameAllyConversationType
            case cz.noveit.proto.firstenchanter.FirstEnchanterConversation.GetConversationsList
              .ConversationType
              .GameEnemyConversationType =>
              ConversationType.GameConversationAll
            case cz.noveit.proto.firstenchanter.FirstEnchanterConversation.GetConversationsList
              .ConversationType
              .PrivateConversationType =>
              ConversationType.PrivateConversationType
          })

        case m: cz.noveit.proto.firstenchanter.FirstEnchanterConversation.InvitePlayerToConversation =>
          conversationController ! InvitePlayerToJoinConversation(m.getConversationId, m.getPlayerName, parameters.player.nickname)

        case m: cz.noveit.proto.firstenchanter.FirstEnchanterConversation.ConversationInvititationDecline =>
          conversationController ! ConversationInviteDecline(m.getConversationId, parameters.player.nickname)

        case m: cz.noveit.proto.firstenchanter.FirstEnchanterConversation.SendMessage =>
          conversationController ! SendMessageToConversation(
            if (m.hasPredefinedMessage) {
              Message(System.currentTimeMillis(), m.getPredefinedMessage.getNumber.toString, parameters.player.nickname, MessageType.PredefinedMessageType)
            } else {
              Message(System.currentTimeMillis(), m.getMessage, parameters.player.nickname, MessageType.NormalMessageType)
            },
            m.getConversationId
          )

        case m: cz.noveit.proto.firstenchanter.FirstEnchanterConversation.SendMessageToFriend =>
          implicit val timeout = Timeout(10 seconds)
          var future = playerHandler ? ServiceModuleMessage(IsFriend(m.getUserName), ServiceHandlersModules.cardGameSocial, "")
          val qphandler = Await.result(future, timeout.duration).asInstanceOf[Option[FriendHandlePair]]

          conversationController ! SendMessageToPlayer(
            Message(System.currentTimeMillis(), m.getMessage, parameters.player.nickname, MessageType.NormalMessageType),
            m.getUserName,
            qphandler.isDefined
          )

        case m: cz.noveit.proto.firstenchanter.FirstEnchanterConversation.GetConversationHistory =>
          val handle = createResponseHandler((msg: Any) => {
            msg match {
              case ConversationHistory(conversation, messages) =>
                val history = cz.noveit.proto.firstenchanter.FirstEnchanterConversation.ConversationHistory.newBuilder()
                messages.map(message => {
                  history.addMessages(
                    cz.noveit.proto.firstenchanter.FirstEnchanterConversation.Message.newBuilder()
                      .setBy(message.fromPlayer)
                      .setContent(message.content)
                      .setTime(message.time)
                      .setPredefined({
                      message.messageType match {
                        case MessageType.NormalMessageType =>
                          false
                        case MessageType.PredefinedMessageType =>
                          true
                      }
                    }).build()
                  )
                })
                history.setConversationId(conversation)

                val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(history.build(), module, replyHash)).toByteArray), "UTF-8")
                playerContext.asInstanceOf[WebSocketsContext].send(encodedString)
            }
          })
          conversationController ! GetConversationHistory(handle, m.getConversationId)

        case m: cz.noveit.proto.firstenchanter.FirstEnchanterConversation.GetInvitationsToConversations =>
          val handle = createResponseHandler((msg: Any) => {
            msg match {
              case ConversationInvitationsForPlayer(invitations) =>
                val invitationsBuilder = cz.noveit.proto.firstenchanter.FirstEnchanterConversation.InvitationsToConversations.newBuilder()
                invitations.map(inv => {
                  invitationsBuilder.addInvitations(
                    cz.noveit.proto.firstenchanter.FirstEnchanterConversation.ConversationInvitation
                      .newBuilder()
                      .setByUserName(inv.by)
                      .setConversationName(inv.conversationName)
                      .setConversationId(inv.conversationId)
                      .build()
                  )
                })

                val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(invitationsBuilder.build(), module, replyHash)).toByteArray), "UTF-8")
                playerContext.asInstanceOf[WebSocketsContext].send(encodedString)
            }
          })
          conversationController ! GetConversationInvitationsForPlayer(handle, parameters.player.nickname)

        case m: cz.noveit.proto.firstenchanter.FirstEnchanterConversation.KickPlayerFromConversation =>
          conversations.find(f => f.id == m.getConversationId).map(found => {
            if (found.creator == parameters.player.nickname) conversationController ! KickPlayerFromConversation(m.getConversationId, m.getUserName)
          })

        case m: cz.noveit.proto.firstenchanter.FirstEnchanterConversation.BanPlayerFromConversation =>
          conversations.find(f => f.id == m.getConversationId).map(found => {
            if (found.creator == parameters.player.nickname) conversationController ! BanPlayerFromConversation(m.getConversationId, m.getUserName)
          })


        case m: cz.noveit.proto.firstenchanter.FirstEnchanterConversation.UnBanPlayerFromConversation =>
          conversations.find(f => f.id == m.getConversationId).map(found => {
            if (found.creator == parameters.player.nickname) conversationController ! UnBanPlayerFromConversation(m.getConversationId, m.getUserName)
          })


        case m: cz.noveit.proto.firstenchanter.FirstEnchanterConversation.GetBannedPlayersFromConversation =>
          val handle = createResponseHandler((msg: Any) => {
            msg match {
              case ListOfBannedPlayersForConversation(conversationId, bannedUsers) =>
                val bannedUsersBuilder = cz.noveit.proto.firstenchanter.FirstEnchanterConversation.BannedPlayersFromConversation.newBuilder()
                bannedUsers.map(u => bannedUsersBuilder.addPlayers(u))

                val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(bannedUsersBuilder.build(), module, replyHash)).toByteArray), "UTF-8")
                playerContext.asInstanceOf[WebSocketsContext].send(encodedString)
            }
          })

          conversationController ! GetListOfBansForConversation(handle, m.getConversationId)

        case m: cz.noveit.proto.firstenchanter.FirstEnchanterConversation.CreateConversation =>
          val handle = createResponseHandler((msg: Any) => {
            msg match {
              case ConversationCreationFailed(reason, conversation) =>
                val failBuilder = cz.noveit.proto.firstenchanter.FirstEnchanterConversation.CreateConversationFailed.newBuilder()
                reason match {
                  case ConversationCreationFailedType.ConversationCreationFailedAlreadyExists =>
                    failBuilder.setReason(cz.noveit.proto.firstenchanter.FirstEnchanterConversation.CreateConversationFailed.CreateConversationFailedReason.AlreadyExists)
                  case ConversationCreationFailedType.ConversationCreationFailedUnknown =>
                    failBuilder.setReason(cz.noveit.proto.firstenchanter.FirstEnchanterConversation.CreateConversationFailed.CreateConversationFailedReason.Unknown)
                }

                val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(failBuilder.build(), module, replyHash)).toByteArray), "UTF-8")
                playerContext.asInstanceOf[WebSocketsContext].send(encodedString)

              case ConversationCreated(c) =>
                val createdBuilder = cz.noveit.proto.firstenchanter.FirstEnchanterConversation.CreateConversationSuccess.newBuilder()
                createdBuilder.setConversation(toProtobufConversation(c))
                val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(createdBuilder.build(), module, replyHash)).toByteArray), "UTF-8")
                playerContext.asInstanceOf[WebSocketsContext].send(encodedString)
            }
          })

          conversationController ! CreateConversation(handle, fromProtobufConversation(m.getConversation).copy(creationTime = System.currentTimeMillis(), creator = parameters.player.nickname))


        case m: cz.noveit.proto.firstenchanter.FirstEnchanterConversation.GetConversationMembers  =>
          val handle = createResponseHandler((msg: Any) => {
            msg match {
              case ConversationWithMembers(cId, members) =>
                val message = cz.noveit.proto.firstenchanter.FirstEnchanterConversation.ConversationWithMembers.newBuilder
                members.map(member => message.addMembers(member))
                message.setConversationId(cId)
                val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(message.build(), module, replyHash)).toByteArray), "UTF-8")
                playerContext.asInstanceOf[WebSocketsContext].send(encodedString)
            }
          })

          conversationController ! GetMembersOfConversation(handle, m.getConversationId)
      }

    case ServiceModuleMessage(message, module, hash) =>
      message match {
        case ConversationMembershipChanged(conversationId, player, change) =>
            if(conversations.exists(c => c.id == conversationId)) {
              change match {
                case ConversationMembershipChangeTypeJoin =>
                    val message = cz.noveit.proto.firstenchanter.FirstEnchanterConversation.MemberJoinedConversation
                      .newBuilder()
                      .setMember(player)
                      .setConversationId(conversationId)
                    val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(message.build(), module, hash)).toByteArray), "UTF-8")
                    playerContext.asInstanceOf[WebSocketsContext].send(encodedString)

                case ConversationMembershipChangeTypeLeft =>
                  if (player.equals(parameters.player.nickname)) {
                    conversations = conversations.filterNot(c => c.id.equals(conversationId))
                  }

                  val message = cz.noveit.proto.firstenchanter.FirstEnchanterConversation.MemberLeftConversation
                    .newBuilder()
                    .setMember(player)
                    .setConversationId(conversationId)
                  val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(message.build(), module, hash)).toByteArray), "UTF-8")
                  playerContext.asInstanceOf[WebSocketsContext].send(encodedString)

              }
            } else if(player.equals(parameters.player.nickname)) {
              log.debug("New conversation {} found for me {}.", conversationId, player)
              conversationController ! GetConversationDetails(context.actorOf(Props(classOf[GetConversationDetailsAndAddThemWorker], self, conversationId)), conversationId)
            }

        case ConversationReceivedMessage(conversationId, message) =>
          if (conversations.find(c => c.id  == conversationId).isDefined) {
              val messageReceivedBuilder = cz.noveit.proto.firstenchanter.FirstEnchanterConversation.ReceivedMessage.newBuilder()
            messageReceivedBuilder.setMessage(
              cz.noveit.proto.firstenchanter.FirstEnchanterConversation.Message.newBuilder()
                .setBy(message.fromPlayer)
                .setContent(message.content)
                .setTime(message.time)
                .setPredefined({
                message.messageType match {
                  case MessageType.NormalMessageType =>
                    false
                  case MessageType.PredefinedMessageType =>
                    true
                }
              }).build()
            )
            messageReceivedBuilder.setConversationId(conversationId)
            val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(messageReceivedBuilder.build(), module, hash)).toByteArray), "UTF-8")
            playerContext.asInstanceOf[WebSocketsContext].send(encodedString)
          }

        case PlayerConversationInvitation(conversation, by, whom, hash, nodule) =>
          if (whom.equals(parameters.player.nickname)) {
            conversation.creationType match {
              case ConversationAutomaticCreationType.ConversationAutomaticCreationTypeBySystem =>
                conversationController ! JoinConversationById(self, conversation.id, parameters.player.nickname)

              case ConversationAutomaticCreationType.ConversationAutomaticCreationTypeByUser =>
                val invitationBuilder = cz.noveit.proto.firstenchanter.FirstEnchanterConversation.ConversationInvitation.newBuilder()
                invitationBuilder.setByUserName(by)
                invitationBuilder.setConversationName(conversation.name)
                invitationBuilder.setConversationId(conversation.id)
                val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(invitationBuilder.build(), module, hash)).toByteArray), "UTF-8")
                playerContext.asInstanceOf[WebSocketsContext].send(encodedString)
            }

          }
      }

  }

  def createResponseHandler(handler: (Any) => Unit): ActorRef = {
    context.actorOf(Props(classOf[ConversationResponseHandler], handler))
  }

  def toProtobufConversation(c: Conversation): cz.noveit.proto.firstenchanter.FirstEnchanterConversation.Conversation = {
    val conversationBuilder = cz.noveit.proto.firstenchanter.FirstEnchanterConversation.Conversation.newBuilder()
    conversationBuilder.setId(c.id)
    conversationBuilder.setName(c.name)
    conversationBuilder.setCreator(c.creator)
    conversationBuilder.setPublic(c.public)
    conversationBuilder.setPerma(c.permanent)
    conversationBuilder.setCreationTime(c.creationTime)

    conversationBuilder.setCreationType(c.creationType match {
      case ConversationAutomaticCreationType.ConversationAutomaticCreationTypeBySystem =>
        cz.noveit.proto.firstenchanter.FirstEnchanterConversation.Conversation
          .ConversationAutomaticCreationType
          .ConversationAutomaticCreationTypeBySystem

      case ConversationAutomaticCreationType.ConversationAutomaticCreationTypeByUser =>
        cz.noveit.proto.firstenchanter.FirstEnchanterConversation.Conversation
          .ConversationAutomaticCreationType
          .ConversationAutomaticCreationTypeByUser
    })

    conversationBuilder.setConversationType(c.conversationType match {
      case ConversationType.ChatRoomConversationType =>
        cz.noveit.proto.firstenchanter.FirstEnchanterConversation.Conversation
          .ConversationType
          .ChatRoomConversationType
      case ConversationType.GameAllyConversationType =>
        cz.noveit.proto.firstenchanter.FirstEnchanterConversation.Conversation
          .ConversationType
          .GameAllyConversationType
      case ConversationType.GameConversationAll =>
        cz.noveit.proto.firstenchanter.FirstEnchanterConversation.Conversation
          .ConversationType
          .GameAllConversationType
      case ConversationType.PrivateConversationType =>
        cz.noveit.proto.firstenchanter.FirstEnchanterConversation.Conversation
          .ConversationType
          .PrivateConversationType
    })

    conversationBuilder.build()
  }

  def fromProtobufConversation(c: cz.noveit.proto.firstenchanter.FirstEnchanterConversation.Conversation): Conversation = {
    Conversation(
    c.getId,
    c.getName,
    c.getCreator, {
      c.getCreationType match {
        case cz.noveit.proto.firstenchanter.FirstEnchanterConversation.Conversation.ConversationAutomaticCreationType.ConversationAutomaticCreationTypeBySystem =>
          ConversationAutomaticCreationType.ConversationAutomaticCreationTypeBySystem
        case cz.noveit.proto.firstenchanter.FirstEnchanterConversation.Conversation
          .ConversationAutomaticCreationType
          .ConversationAutomaticCreationTypeByUser =>
          ConversationAutomaticCreationType.ConversationAutomaticCreationTypeByUser
      }
    }, {
      c.getConversationType match {
        case cz.noveit.proto.firstenchanter.FirstEnchanterConversation.Conversation
          .ConversationType
          .ChatRoomConversationType =>
          ConversationType.ChatRoomConversationType
        case cz.noveit.proto.firstenchanter.FirstEnchanterConversation.Conversation
          .ConversationType
          .GameAllyConversationType =>
          ConversationType.GameAllyConversationType
        case cz.noveit.proto.firstenchanter.FirstEnchanterConversation.Conversation
          .ConversationType
          .GameAllConversationType =>
          ConversationType.GameConversationAll
        case cz.noveit.proto.firstenchanter.FirstEnchanterConversation.Conversation
          .ConversationType
          .PrivateConversationType =>
          ConversationType.PrivateConversationType
      }
    },
    c.getPublic,
    c.getPerma,
    c.getCreationTime
    )
  }
}

class ConversationResponseHandler(handler: (Any) => {}) extends Actor with ActorLogging {
  def receive = {
    case t => handler(t)
      context.stop(self)
  }
}

case class AddConversation(conversation: Conversation)

class GetConversationDetailsAndAddThemWorker(parent: ActorRef, conversationId: String) extends Actor {
  def receive = {
    case msg: ConversationFound =>
      parent ! AddConversation(msg.conversation)
    context.stop(self)
  }
}
