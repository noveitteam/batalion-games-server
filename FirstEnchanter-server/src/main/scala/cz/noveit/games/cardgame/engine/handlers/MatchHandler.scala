package cz.noveit.games.cardgame.engine.handlers

import akka.actor._
import cz.noveit.games.cardgame.engine.game.session._
import cz.noveit.games.cardgame.engine.handlers.helpers.deserialization.{ProtoToNeutralDistribution, ProtoToCardTarget, ProtoToCardPosition}
import cz.noveit.games.cardgame.engine.handlers.helpers.serialization._
import cz.noveit.proto.firstenchanter.FirstEnchanterMatch.{MatchState}
import cz.noveit.proto.serialization.{MessageSerializer, MessageEvent}

import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import scala.concurrent.Await
import org.bouncycastle.util.encoders.Base64
import cz.noveit.connector.websockets.WebSocketsContext
import cz.noveit.games.cardgame.engine.game._
import cz.noveit.games.cardgame.engine.{PlayerOffline, CrossHandlerMessage, game, ServiceModuleMessage}
import cz.noveit.games.cardgame.engine.game.AskReady
import scala.Some
import cz.noveit.games.cardgame.engine.game.TableState
import cz.noveit.games.cardgame.engine.game.MatchQuerySetup
import cz.noveit.games.cardgame.engine.game.RetryFindMatch
import cz.noveit.proto.serialization.MessageEvent
import cz.noveit.games.cardgame.engine.player.{NeutralDistribution, PlayerIsTryingToFindGame}
import cz.noveit.games.cardgame.adapters.PlayerScore
import cz.noveit.games.cardgame.engine.game.PlayerReady
import cz.noveit.games.cardgame.engine.card._
import scala.collection.JavaConverters._

/**
 * Created by Wlsek on 10.2.14.
 */


trait MatchHandler extends PlayerService {
}

case object GetMatchHandler extends CrossHandlerMessage

case class GetPlayerInMatchHandler(matchHandler: ActorRef) extends CrossHandlerMessage

case class PlayerInMatchHandlerFound(h: ActorRef) extends CrossHandlerMessage


class MatchHandlerActor(val module: String, parameters: PlayerServiceParameters) extends CardsHandler with Actor with ActorLogging with MessageSerializer {

  val playerHandler = parameters.playerActor
  val playerContext = parameters.context
  val player = parameters.player

  val matchController = context.actorSelection("/user/matchController")
  var readyActor: Option[ActorRef] = None

  var playerInMatchHandler: Option[ActorRef] = None

  var lastQuerySetup: Option[MatchQuerySetup] = None



  def receive = {
    case msg: MessageEvent =>
      msg.message match {
        case m: cz.noveit.proto.firstenchanter.FirstEnchanterMatch.FindMatch =>
          try {

            implicit val timeout = Timeout(5 seconds)
            var future = playerHandler ? ServiceModuleMessage(GetPlayerLobbyForControl, ServiceHandlersModules.cardGameMatchLobby)
            val lobby = Await.result(future, timeout.duration).asInstanceOf[Option[MatchLobby]]

            var playersFindingMatch: List[MatchLobbyPlayer] = List()
            val test = lobby.exists(l => {
              playersFindingMatch = l.playersInLobby.filter(pil => pil.invitationState == MatchLobbyInvitationStateAccepted)
              val tv = toTeamSizeVector(playersFindingMatch)

              tv.exists(v => {
                l.matchType match {
                  case MatchGameModePractice =>
                    l.teamMode match {
                      case MatchTeamModeOneVsOne =>
                        l.enemyMode match {
                          case MatchGameEnemyModePVE =>
                            if (v ===(0, 1)) {
                              true
                            } else {
                              false
                            }
                          case MatchGameEnemyModePVP =>
                            if (v ===(0, 1) || v ==(1, 1)) {
                              true
                            } else {
                              false
                            }
                        }
                      case MatchTeamModeTwoVsTwo =>
                        l.enemyMode match {
                          case MatchGameEnemyModePVE =>
                            if (v ===(0, 1) || v ===(0, 2)) {
                              true
                            } else {
                              false
                            }
                          case MatchGameEnemyModePVP =>
                            if (v ===(0, 1) || v ===(0, 2) || v ===(1, 2) || v ==(2, 2)) {
                              true
                            } else {
                              false
                            }
                        }
                    }

                  case MatchGameModeRanked =>
                    l.teamMode match {
                      case MatchTeamModeOneVsOne =>
                        if (v ==(1, 0)) {
                          true
                        } else {
                          false
                        }
                      case MatchTeamModeTwoVsTwo =>
                        if (v ==(2, 0)) {
                          true
                        } else {
                          false
                        }
                    }
                }
                true
              })
              true
            })

            if (test) {
              val players = playersFindingMatch.map(p => {
                future = playerHandler ? ServiceModuleMessage(GetScoreBoardForPlayer(p.user.nickname), ServiceHandlersModules.cardGameScore, msg.replyHash)
                val score = Await.result(future, timeout.duration).asInstanceOf[PlayerScore]
                PlayerIsTryingToFindGame(p.user.nickname, p.teamToken, p.user.avatar.getOrElse(-1), score.rating, score.score, score.division, p.handler)
              })

              lastQuerySetup = Some(MatchQuerySetup(
                players,
                lobby.get.arena,
                lobby.get.matchType,
                lobby.get.enemyMode,
                lobby.get.teamMode,
                self
              ))

              lastQuerySetup.map(s => matchController ! FindMatch(s))

              val encodedString = new String(Base64.encode(serializeToMessage(
                MessageEvent(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.FindMatchStarted.newBuilder().build(), module, msg.replyHash)
              ).toByteArray), "UTF-8")

              playerContext.asInstanceOf[WebSocketsContext].send(encodedString)
            } else {
              val encodedString = new String(Base64.encode(serializeToMessage(
                MessageEvent(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.FindMatchError.newBuilder().setError(
                  cz.noveit.proto.firstenchanter.FirstEnchanterMatch.FindMatchError.FindMatchErrorType.UnsupportedNumberOfPlayers
                ).build(), module, msg.replyHash)
              ).toByteArray), "UTF-8")

              playerContext.asInstanceOf[WebSocketsContext].send(encodedString)
            }
          } catch {
            case t: Throwable =>
              log.error("Error while joining match for user: {} with text: {}", player.nickname, t.toString)
              val encodedString = new String(Base64.encode(serializeToMessage(
                MessageEvent(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.FindMatchError.newBuilder().setError(
                  cz.noveit.proto.firstenchanter.FirstEnchanterMatch.FindMatchError.FindMatchErrorType.UnsupportedGame
                ).build(), module, msg.replyHash)
              ).toByteArray), "UTF-8")

              playerContext.asInstanceOf[WebSocketsContext].send(encodedString)
          }

        case m: cz.noveit.proto.firstenchanter.FirstEnchanterMatch.StopSearch =>
          lastQuerySetup.map(s => matchController ! StopSearching(s, StopSearchingReasonByCaptain))

        case m: cz.noveit.proto.firstenchanter.FirstEnchanterMatch.AcceptMatch =>
          try {
            playerHandler ! ServiceModuleMessage(AskOwnershipOfDeckForPlayer(player.nickname, m.getActiveDeckId), ServiceHandlersModules.cardGameCards)

            implicit val timeout = Timeout(5 seconds)
            val future = playerHandler ? ServiceModuleMessage(AskOwnershipOfDeckForPlayer(player.nickname, m.getActiveDeckId), ServiceHandlersModules.cardGameCards)
            val smm = Await.result(future, timeout.duration).asInstanceOf[ServiceModuleMessage]

            if (smm.asInstanceOf[AskOwnershipResult].isOwner) {
              readyActor.map(ra => ra ! PlayerReady(player.nickname, m.getActiveDeckId))
              readyActor = None
            } else {
              log.error("Player {} doesnt own deck {}.", player.nickname, m.getActiveDeckId)
              readyActor.map(ra => ra ! PlayerDeclined(player.nickname))
              readyActor = None
            }
          } catch {
            case t: Throwable =>
              log.error("Error while waiting for AskOwnershipResult")
              readyActor.map(ra => ra ! PlayerDeclined(player.nickname))
              readyActor = None
          }

        case m: cz.noveit.proto.firstenchanter.FirstEnchanterMatch.DeclineMatch =>
          readyActor.map(ra => ra ! PlayerDeclined(player.nickname))
          readyActor = None


        case o =>
          playerInMatchHandler.map(pim => pim ! msg)

      }

    case msg: ServiceModuleMessage => {
      msg.message match {
        case mr:  MatchReward  => {
          log.debug("Sending match reward to client.")
          val encodedString = new String(Base64.encode(serializeToMessage(
            MessageEvent(
              cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchEnded
                .newBuilder
                .setEssences(mr.essences)
                .setMmrChange(mr.mmrChange)
                .setWin(mr.win)
                .build(), module
            )
          ).toByteArray), "UTF-8")
          playerContext.asInstanceOf[WebSocketsContext].send(encodedString)
          playerInMatchHandler.map(pim => context.stop(pim))
          playerInMatchHandler = None
        }
        case AskReady(replyTo) =>
          readyActor = Some(replyTo)
          val encodedString = new String(Base64.encode(serializeToMessage(
            MessageEvent(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchReady.newBuilder().build(), module, msg.replyHash)
          ).toByteArray), "UTF-8")
          playerContext.asInstanceOf[WebSocketsContext].send(encodedString)

        case RetryFindMatch(query) =>
          matchController ! FindMatch(query)

        case m: CannotComposePlayer =>
          val encodedString = new String(Base64.encode(serializeToMessage(
            MessageEvent(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CannotComposePlayer.newBuilder.setPlayer(m.player).build(), module, msg.replyHash)
          ).toByteArray), "UTF-8")
          playerContext.asInstanceOf[WebSocketsContext].send(encodedString)

        case PlayerOffline(player, handler) => {
          lastQuerySetup.map(qs => {
            if (qs.players.find(ptf => ptf.player == player).isDefined) {
              matchController ! StopSearching(qs, StopSearchingReasonPlayerOffline)
            }
          })
        }

        case StoppedSearching(reason) =>
          val encodedString = new String(Base64.encode(serializeToMessage(
            MessageEvent(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.StoppedSearching.newBuilder
              .setReason({
              reason match {
                case StopSearchingReasonByCaptain =>
                  cz.noveit.proto.firstenchanter.FirstEnchanterMatch.StoppedSearching.StoppedSearchingReason.ByCaptain
                case StopSearchingReasonPlayerOffline =>
                  cz.noveit.proto.firstenchanter.FirstEnchanterMatch.StoppedSearching.StoppedSearchingReason.PlayerOffline
              }
            }).build(), module, msg.replyHash)
          ).toByteArray), "UTF-8")
          playerContext.asInstanceOf[WebSocketsContext].send(encodedString)

        case GetPlayerInMatchHandler(matchHandler) => {
          sender ! PlayerInMatchHandlerFound(playerInMatchHandler.map(pim => {
            pim
          }).getOrElse({
            val actor = context.actorOf(Props(classOf[PlayerInMatchHandler], PlayerData(player.nickname, None, None), playerContext.asInstanceOf[WebSocketsContext], module, matchHandler))
            playerInMatchHandler = Some(actor)
            actor
          }))
        }
      }
    }
  }

  def toTeamSizeVector(playersInLobby: List[MatchLobbyPlayer]): Option[TeamVector] = {
    val teams = playersInLobby.groupBy(p => p.teamToken)
    if (teams.size == 1) {
      Some(TeamVector(teams.head._2.size, 0))
    } else if (teams.size == 2) {
      Some(TeamVector(teams.head._2.size, teams.last._2.size))
    } else {
      None
    }
  }

  override def postStop = {
    log.debug("Ending MatchHandler for player: " + player.nickname)
    readyActor.map(ra => ra ! Kill)
    playerInMatchHandler.map(pim => pim ! Kill)
  }
}

case class PlayerData(playerName: String, rollPosition: Option[PlayerRollPosition], tableState: Option[TableState])

class PlayerInMatchHandler(initialData: PlayerData, webContext: WebSocketsContext, module: String, matchHandler: ActorRef) extends Actor with ActorLogging with FSM[State, PlayerData] with MessageSerializer {
  startWith(INITIALIZING_MATCH, initialData)

  private var roundReplyHashForPosition: List[Pair[CardPosition, String]] = List()

  def popHashForPosition(position: CardPosition): Option[String] = {
    roundReplyHashForPosition.find(f => f._1 =? position).map(found => {
      roundReplyHashForPosition = roundReplyHashForPosition.filter(rfp => !(rfp._1 =? position))
      found._2
    })
  }

  def putHashForPosition(position: CardPosition, hash: String) = {
    if(roundReplyHashForPosition.find(f => f._1 =? position).isEmpty) {
      roundReplyHashForPosition = position -> hash :: roundReplyHashForPosition
    }
  }
  /*
    when(INITIALIZING_MATCH) {
      case Event(ServiceModuleMessage(StateAsk(state, ts), module, hsh), data: PlayerData) => {
        log.debug("Player {} received state ask,syncing and sending it to web sockets..", initialData.playerName, INITIALIZING_MATCH, state)
        webContext.send(
          serializeToString(
            MessageEvent(
              cz.noveit
                .proto
                .firstenchanter
                .FirstEnchanterMatch
                .StateAsk
                .newBuilder()
                .setPlayerState(TableStateToProto(ts, ts.getPlayerTeam(initialData.playerName)))
                .setState(MatchState.PLAYER_STARTS_ROLL)
                .build(),
              module
            )
          )
        )
        stay()
      }

      case Event(msg: cz.noveit.proto.firstenchanter.FirstEnchanterMatch.StateAck, data) => {
        msg.getState match {
          case MatchState.PLAYER_STARTS_ROLL =>
            goto(PLAYER_STARTS_ROLL)
          case other =>
            goto(PLAYER_STARTS_ROLL)
        }
      }
    }
  */

  when(INITIALIZING_MATCH) {
    case Event(ServiceModuleMessage(msg: StateAsk, module, rh), data) => {
      log.debug("Player match handler accepted state ask, forwarding it to client.")
      val encodedString = new String(Base64.encode(serializeToMessage(
        MessageEvent(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.StateAsk.newBuilder
          .setState(
            msg.state match {
              case PLAYER_STARTS_ROLL =>
                cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchState.PLAYER_STARTS_ROLL
            }
          )
          .setPlayerState(TableStateToProto(msg.tableState, msg.tableState.getPlayerTeam(data.playerName)))
          .setTimeInThisState(msg.timInState)
          .setTimeAnchor(msg.timeAnchor)
          .build(),
          module
        )
      ).toByteArray), "UTF-8")
      webContext.send(encodedString)

      stay using (data.copy(tableState = Some(msg.tableState)))
    }

    case Event(MessageEvent(msg: cz.noveit.proto.firstenchanter.FirstEnchanterMatch.StateAck, module, rh, ctx), data) => {
      log.debug("Player match handler accepted state ack, forwarding it to match.")
      val state = msg.getState match {
        case cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchState.PLAYER_STARTS_ROLL =>
          PLAYER_STARTS_ROLL
      }

      matchHandler ! StateAck(
        state,
        data.playerName
      )

      goto(state)
    }
  }
  when(PLAYER_STARTS_ROLL) {
    case Event(MessageEvent(msg: cz.noveit.proto.firstenchanter.FirstEnchanterMatch.PlayerRoll, module, rh, ctx), data) => {
      data.rollPosition.map(roll => {
        matchHandler ! PlayerRoll(data.playerName, msg.getRoll, roll)
      })
      stay()
    }

    case Event(ServiceModuleMessage(msg: PlayerGetsElements, mod, rh), data) => {
      webContext.send(
        serializeToString(
          MessageEvent(
            cz.noveit.proto.firstenchanter.FirstEnchanterMatch.ElementsGenerated
              .newBuilder()
              .setCharm(CharmOfElementsToProto(msg.addedElements))
              .setPlayer(msg.player)
              .build(),
            module
          )
        )
      )
      stay()
    }


    case Event(ServiceModuleMessage(msg: AskPlayerRoll, mod, rh), data) => {
      webContext.send(serializeToString(
        MessageEvent(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.AskPlayerRoll.newBuilder().setRollPositionAttacker(
          msg.position match {
            case PLAYER_ROLL_POSITION_ATTACKER => true
            case PLAYER_ROLL_POSITION_DEFENDER => false
          }
        ).build(), module)
      ))

      stay using (data.copy(rollPosition = Some(msg.position)))
    }

    case Event(ServiceModuleMessage(msg: PlayerRollResult, mod, rh), data) => {
      val rollResult = cz.noveit.proto.firstenchanter.FirstEnchanterMatch.PlayerRollResult.newBuilder()

      msg.winning.map(w => {
        rollResult.addWinnerPlayers(
          cz.noveit.proto.firstenchanter.FirstEnchanterMatch.PlayerRoll
            .newBuilder()
            .setPlayer(w.user)
            .setRoll(w.number)
            .setRollPositionAttacker(w.position match {
            case PLAYER_ROLL_POSITION_ATTACKER => true
            case PLAYER_ROLL_POSITION_DEFENDER => false
          }).build()
        )
      })

      msg.loosing.map(l => {
        rollResult.addLoserPlayers(
          cz.noveit.proto.firstenchanter.FirstEnchanterMatch.PlayerRoll
            .newBuilder()
            .setPlayer(l.user)
            .setRoll(l.number)
            .setRollPositionAttacker(l.position match {
            case PLAYER_ROLL_POSITION_ATTACKER => true
            case PLAYER_ROLL_POSITION_DEFENDER => false
          }).build()
        )
      })
      val encodedString = new String(Base64.encode(serializeToMessage(
        MessageEvent(rollResult.build, module)
      ).toByteArray), "UTF-8")
      webContext.send(encodedString)

      stay()
    }
  }

  when(FORTUNE_ROLL) {
    case Event(MessageEvent(msg: cz.noveit.proto.firstenchanter.FirstEnchanterMatch.PlayerRoll, module, rh, ctx), data) => {
      data.rollPosition.map(roll => {
        matchHandler ! PlayerRoll(data.playerName, msg.getRoll, roll)
      })
      stay()
    }

    case Event(ServiceModuleMessage(msg: AskPlayerRoll, mod, rh), data) => {
      webContext.send(serializeToString(
        MessageEvent(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.AskPlayerRoll.newBuilder().setRollPositionAttacker(
          msg.position match {
            case PLAYER_ROLL_POSITION_ATTACKER => true
            case PLAYER_ROLL_POSITION_DEFENDER => false
          }
        ).build(), module)
      ))
      stay using (data.copy(rollPosition = Some(msg.position)))
    }

    case Event(ServiceModuleMessage(msg: PlayerGetsElements, mod, rh), data) => {
      webContext.send(
        serializeToString(
          MessageEvent(
            cz.noveit.proto.firstenchanter.FirstEnchanterMatch.ElementsGenerated
              .newBuilder()
              .setCharm(CharmOfElementsToProto(msg.addedElements))
              .setPlayer(msg.player)
              .build(),
            module
          )
        )
      )
      stay()
    }

    case Event(ServiceModuleMessage(msg: PlayerDrawCards, mod, rh), data) => {
      webContext.send(
        serializeToString(
          MessageEvent(
            cz.noveit.proto.firstenchanter.FirstEnchanterMatch.PlayerDrawCards
              .newBuilder()
              .addAllCards(msg.id.map(c => {
              val builder = cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardOnCardPosition.newBuilder()

              if (data.tableState.get.getPlayerTeam(data.playerName).contains(msg.player)) {
                builder.setId(c.card.id)
                builder.setSkinId(c.card.skinId.getOrElse(-1))
              }

              builder.setPosition(CardPositionToProto(InHandCardPosition(c.position, msg.player)))
              builder.build()
            }).asJava)
              .build(),
            module
          )
        )
      )
      stay()
    }

    case Event(ServiceModuleMessage(msg: PlayerRollResult, mod, rh), data) => {
      val rollResult = cz.noveit.proto.firstenchanter.FirstEnchanterMatch.PlayerRollResult.newBuilder()

      msg.winning.map(w => {
        rollResult.addWinnerPlayers(
          cz.noveit.proto.firstenchanter.FirstEnchanterMatch.PlayerRoll
            .newBuilder()
            .setPlayer(w.user)
            .setRoll(w.number)
            .setRollPositionAttacker(w.position match {
            case PLAYER_ROLL_POSITION_ATTACKER => true
            case PLAYER_ROLL_POSITION_DEFENDER => false
          }).build()
        )
      })

      msg.loosing.map(l => {
        rollResult.addLoserPlayers(
          cz.noveit.proto.firstenchanter.FirstEnchanterMatch.PlayerRoll
            .newBuilder()
            .setPlayer(l.user)
            .setRoll(l.number)
            .setRollPositionAttacker(l.position match {
            case PLAYER_ROLL_POSITION_ATTACKER => true
            case PLAYER_ROLL_POSITION_DEFENDER => false
          }).build()
        )
      })

      webContext.send(serializeToString(MessageEvent(rollResult.build, module)))
      stay()
    }
  }

  when(ATTACKER_TEAM_PLAY) {
    case Event(MessageEvent(msg: cz.noveit.proto.firstenchanter.FirstEnchanterMatch.AttackerSwappsCards, module, rh, ctx), data) => {
      matchHandler ! AttackerSwapsCards(
        msg.getCardsList
          .asScala
          .filter(c => c.getPlayer == initialData.playerName)
          .map(p => ProtoToCardPosition(p)).toList
        ,
        ProtoToNeutralDistribution(msg.getDistribution)
      )
      stay()
    }

    case Event(ServiceModuleMessage(msg: AttackerSwapsCardsAccepted, mod, rh), data) => {
      webContext.send(
        serializeToString(MessageEvent(
          cz.noveit.proto.firstenchanter.FirstEnchanterMatch.AttackerSwappsCardsAccepted
            .newBuilder()
            .setDistribution(NeutralDistributionToProto(msg.neutralDistribution))
            .addAllCards(msg.onPosition.map(p => CardPositionToProto(p)).asJava)
            .addAllChanges(msg.changes.map(ch => SerializeProtoTableChange(ch.toProtoMessage)).asJava)
            .build(),
          module
        ))
      )
      stay()
    }

    case Event(ServiceModuleMessage(msg: AttackerSwapsCardsDeclined, mod, rh), data) => {
      webContext.send(
        serializeToString(MessageEvent(
          cz.noveit.proto.firstenchanter.FirstEnchanterMatch.AttackerSwappsCardsAccepted
            .newBuilder()
            .setDistribution(NeutralDistributionToProto(msg.neutralDistribution))
            .addAllCards(msg.onPosition.map(p => CardPositionToProto(p)).asJava)
            .build(),
          module
        ))
      )
      stay()
    }

    case Event(ServiceModuleMessage(msg: AttackerSwappedCards, mod, sh), data) => {
      webContext.send(
        serializeToString(MessageEvent(
          cz.noveit.proto.firstenchanter.FirstEnchanterMatch.AttackerSwappsedCards
            .newBuilder()
            .setDistribution(NeutralDistributionToProto(msg.neutralDistribution))
            .addAllCards(msg.onPosition.map(p => CardPositionToProto(p)).asJava)
            .build(),
          module
        ))
      )
      stay()
    }

    //Asking player to use cards
    case Event(ServiceModuleMessage(msg: AskAttackerToUseCard, mod, rh), data) => {
      webContext.send(serializeToString(
        MessageEvent(
          cz.noveit.proto.firstenchanter.FirstEnchanterMatch.AskAttackerToUseCard
            .newBuilder()
            .addAllPossibleUsages(
              msg.possibleUses.map(
                pu => {
                  val builder = cz.noveit.proto.firstenchanter.FirstEnchanterMatch.AttackerUsesCard
                    .newBuilder()
                  builder.setPosition(CardPositionToProto(pu.onPosition))
                  pu.target.map(target => builder.setTarget(CardTargetToProto(target)))
                  builder.setNeutralDistribution(NeutralDistributionToProto(pu.neutralDistribution))
                  builder.setId(pu.card.id)
                  builder.build
                }
              ).toList.asJava)
            .build()
          ,
          module
        )
      ))
      stay()
    }

    //Asking defender to wait
    case Event(ServiceModuleMessage(AskDefenderToWait, mod, rh), data) => {
      webContext.send(serializeToString(
        MessageEvent(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.AskDefenderToWait.newBuilder.build(), module)
      ))
      stay()
    }

    //Asking possible usages
    case Event(MessageEvent(msg: cz.noveit.proto.firstenchanter.FirstEnchanterMatch.AttackerAskPossibleUses, module, rh, ctx), data) => {
      if (data.playerName != msg.getUserName) {
        data.tableState.map(ts => {
          if (ts.arePlayersOnSameTeam(data.playerName, msg.getUserName)) {
            matchHandler ! AttackerAsksPossibleCardUses(msg.getUserName)
          }
        })
      } else {
        matchHandler ! AttackerAsksPossibleCardUses(data.playerName)
      }
      stay()
    }

    //Possible usages
    case Event(ServiceModuleMessage(msg: AttackerPossibleCardUses, mod, rh), data) => {
      webContext.send(serializeToString(
        MessageEvent(
          cz.noveit.proto.firstenchanter.FirstEnchanterMatch.AttackerPossibleCardUses
            .newBuilder()
            .addAllPossibleUsages(
              msg.possibleUses.map(
                pu => {
                  val builder = cz.noveit.proto.firstenchanter.FirstEnchanterMatch.AttackerUsesCard
                    .newBuilder()
                  builder.setPosition(CardPositionToProto(pu.onPosition))
                  pu.target.map(target => builder.setTarget(CardTargetToProto(target)))
                  builder.setNeutralDistribution(NeutralDistributionToProto(pu.neutralDistribution))
                  builder.setId(pu.card.id)
                  builder.build
                }
              ).toList.asJava)
            .build()
          ,
          module
        )
      ))
      stay()
    }

    //Card used by player
    case Event(MessageEvent(msg: cz.noveit.proto.firstenchanter.FirstEnchanterMatch.AttackerUsesCard, module, rh, ctx), data) => {
      val position = ProtoToCardPosition(msg.getPosition)
      val target: Option[CardTarget] = if (msg.hasTarget) {
        Some(ProtoToCardTarget(msg.getTarget))
      } else {
        None
      }

      val distribution = ProtoToNeutralDistribution(msg.getNeutralDistribution)

      if (position.player == data.playerName) {
        putHashForPosition(position, rh)
        matchHandler ! AttackerUsesCard(position, target, CardHolder.EMPTY, distribution, List())
      }
      stay()
    }

    //Confirmation of card usage
    case Event(ServiceModuleMessage(msg: AttackerUsedCardAccepted, mod, rh), data) => {
      webContext.send(serializeToString(
        MessageEvent(
          {
            val builder = cz.noveit.proto.firstenchanter.FirstEnchanterMatch.AttackerUsedCardAccepted.newBuilder()
            builder.setPosition(CardPositionToProto(msg.position))
            msg.target.map(t => builder.setTarget(CardTargetToProto(t)))
            builder.setNeutralDistribution(NeutralDistributionToProto(msg.neutralDistribution))
            builder.addAllPossibleUsages(
              msg.possibleUsages.map(
                pu => {
                  val builder = cz.noveit.proto.firstenchanter.FirstEnchanterMatch.AttackerUsesCard
                    .newBuilder()
                  builder.setPosition(CardPositionToProto(pu.onPosition))
                  pu.target.map(target => builder.setTarget(CardTargetToProto(target)))
                  builder.setNeutralDistribution(NeutralDistributionToProto(msg.neutralDistribution))
                  builder.setId(pu.card.id)
                  builder.build
                }
              ).toList.asJava)
            builder
          }.build,
          module,
          popHashForPosition(msg.position).getOrElse("")
        )
      ))
      stay()
    }

    //Card usage declined
    case Event(ServiceModuleMessage(msg: AttackerUsedCardDeclined, mod, rh), data) => {
      webContext.send(serializeToString(
        MessageEvent(
          {
            val builder = cz.noveit.proto.firstenchanter.FirstEnchanterMatch.AttackerUsedCardDeclined.newBuilder()
            builder.setPosition(CardPositionToProto(msg.position))
            builder.setNeutralDistribution(NeutralDistributionToProto(msg.neutralDistribution))
            msg.target.map(t => builder.setTarget(CardTargetToProto(t)))
            builder
          }.build,
          module,
          popHashForPosition(msg.position).getOrElse("")
        )
      ))
      stay()
    }

    //Team-mate or enemy used card
    case Event(ServiceModuleMessage(msg: AttackerUsesCard, mod, rh), data) => {
      data.tableState.map(ts => {
        webContext.send(serializeToString(
          MessageEvent(
            {
             // if (ts.arePlayersOnSameTeam(data.playerName, msg.onPosition.player)) {
                val builder = cz.noveit.proto.firstenchanter.FirstEnchanterMatch.AttackerUsesCard.newBuilder()
                builder.setPosition(CardPositionToProto(msg.onPosition))
                builder.setNeutralDistribution(NeutralDistributionToProto(msg.neutralDistribution))
                builder.setId(msg.card.id)
                msg.target.map(t => builder.setTarget(CardTargetToProto(t)))
                builder
           /*   } else {
                val builder = cz.noveit.proto.firstenchanter.FirstEnchanterMatch.DefenderNotificationAboutAttackerCardUse.newBuilder()
                builder.setCard(
                  cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardOnCardPosition.newBuilder()
                    .setPosition(CardPositionToProto(msg.onPosition))
                    .setId(msg.card.id)
                    .setSkinId(msg.card.skinId.getOrElse(-1))
                    .build()
                  )
                builder.setNeutralDistribution(NeutralDistributionToProto(msg.neutralDistribution))
                msg.target.map(t => CardTargetToProto(t))
                builder
              }*/
            }.build,
            module
          )
        ))
      })
      stay()
    }

    //End sub-turn by player
    case Event(MessageEvent(msg: cz.noveit.proto.firstenchanter.FirstEnchanterMatch.AttackerEndsTurn, module, rh, ctx), data) => {
      if (msg.getUserName == data.playerName) {
        matchHandler ! AttackerEndsTurn(data.playerName)
      }
      stay()
    }

    //Team-mate or enemy ended turn
    case Event(ServiceModuleMessage(msg: AttackerEndsTurn, mod, rh), data) => {
      webContext.send(serializeToString(
        MessageEvent(
          cz.noveit.proto.firstenchanter.FirstEnchanterMatch.AttackerEndsTurn
            .newBuilder
            .setUserName(msg.user)
            .build(),
          module)
      ))
      stay()
    }

  }

  when(DEFENDER_TEAM_COUNTER_PLAY) {
    //Asking player to use cards
    case Event(ServiceModuleMessage(msg: AskDefenderToUseCard, mod, rh), data) => {
      webContext.send(serializeToString(
        MessageEvent(
          cz.noveit.proto.firstenchanter.FirstEnchanterMatch.AskDefenderToUseCard
            .newBuilder()
            .addAllPossibleUsages(
              msg.possibleUses.map(
                pu => {
                  val builder = cz.noveit.proto.firstenchanter.FirstEnchanterMatch.DefenderUsesCard
                    .newBuilder()
                  builder.setPosition(CardPositionToProto(pu.onPosition))
                  pu.target.map(target => builder.setTarget(CardTargetToProto(target)))
                  builder.setNeutralDistribution(NeutralDistributionToProto(pu.neutralDistribution))
                  builder.setId(pu.card.id)
                  builder.build
                }
              ).toList.asJava)
            .build()
          ,
          module
        )
      ))
      stay()
    }

    //Asking defender to wait
    case Event(ServiceModuleMessage(AskAttackerToWait, mod, rh), data) => {
      webContext.send(serializeToString(
        MessageEvent(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.AskAttackerToWait.newBuilder.build(), module)
      ))
      stay()
    }

    //Asking possible usages
    case Event(MessageEvent(msg: cz.noveit.proto.firstenchanter.FirstEnchanterMatch.DefenderAskPossibleUses, module, rh, ctx), data) => {
      if (data.playerName != msg.getUserName) {
        data.tableState.map(ts => {
          if (ts.arePlayersOnSameTeam(data.playerName, msg.getUserName)) {
            matchHandler ! DefenderAsksPossibleCardUses(msg.getUserName)
          }
        })
      } else {
        matchHandler ! DefenderAsksPossibleCardUses(data.playerName)
      }
      stay()
    }

    //Possible usages
    case Event(ServiceModuleMessage(msg: DefenderPossibleCardUses, mod, rh), data) => {
      webContext.send(serializeToString(
        MessageEvent(
          cz.noveit.proto.firstenchanter.FirstEnchanterMatch.DefenderPossibleCardUses
            .newBuilder()
            .addAllPossibleUsages(
              msg.possibleUses.map(
                pu => {
                  val builder = cz.noveit.proto.firstenchanter.FirstEnchanterMatch.DefenderUsesCard
                    .newBuilder()
                  builder.setPosition(CardPositionToProto(pu.onPosition))
                  pu.target.map(target => builder.setTarget(CardTargetToProto(target)))
                  builder.setNeutralDistribution(NeutralDistributionToProto(pu.neutralDistribution))
                  builder.setId(pu.card.id)
                  builder.build
                }
              ).toList.asJava)
            .build()
          ,
          module
        )
      ))
      stay()
    }

    //Card used by player
    case Event(MessageEvent(msg: cz.noveit.proto.firstenchanter.FirstEnchanterMatch.DefenderUsesCard, module, rh, crx), data) => {
      val position = ProtoToCardPosition(msg.getPosition)
      val target: Option[CardTarget] = if (msg.hasTarget) {
        Some(ProtoToCardTarget(msg.getTarget))
      } else {
        None
      }

      val distribution = ProtoToNeutralDistribution(msg.getNeutralDistribution)

      if (position.player == data.playerName) {
        matchHandler ! DefenderUsesCard(position, target, CardHolder.EMPTY, distribution, List())
      }
      stay()
    }

    //Confirmation of card usage
    case Event(ServiceModuleMessage(msg: DefenderUsedCardAccepted, mod, rh), data) => {
      webContext.send(serializeToString(
        MessageEvent(
          {
            val builder = cz.noveit.proto.firstenchanter.FirstEnchanterMatch.DefenderUsedCardAccepted.newBuilder()
            builder.setPosition(CardPositionToProto(msg.position))
            msg.target.map(t => builder.setTarget(CardTargetToProto(t)))
            builder.setNeutralDistribution(NeutralDistributionToProto(msg.neutralDistribution))
            builder.addAllPossibleUsages(
              msg.possibleUses.map(
                pu => {
                  val builder = cz.noveit.proto.firstenchanter.FirstEnchanterMatch.DefenderUsesCard
                    .newBuilder()
                  builder.setPosition(CardPositionToProto(pu.onPosition))
                  builder.setNeutralDistribution(NeutralDistributionToProto(pu.neutralDistribution))
                  pu.target.map(target => builder.setTarget(CardTargetToProto(target)))
                  builder.setId(pu.card.id)
                  builder.build
                }
              ).toList.asJava)
            builder
          }.build,
          module
        )
      ))
      stay()
    }

    //Card usage declinedf
    case Event(ServiceModuleMessage(msg: DefenderUsedCardDeclined, mod, rh), data) => {
      webContext.send(serializeToString(
        MessageEvent(
          {
            val builder = cz.noveit.proto.firstenchanter.FirstEnchanterMatch.DefenderUsedCardDeclined.newBuilder()
            builder.setPosition(CardPositionToProto(msg.position))
            builder.setNeutralDistribution(NeutralDistributionToProto(msg.neutralDistribution))
            msg.target.map(t => builder.setTarget(CardTargetToProto(t)))
            builder
          }.build,
          module
        )
      ))
      stay()
    }

    //Team-mate or enemy used card
    case Event(ServiceModuleMessage(msg: DefenderUsesCard, mod, rh), data) => {
      webContext.send(serializeToString(
        MessageEvent(
          {
            val builder = cz.noveit.proto.firstenchanter.FirstEnchanterMatch.DefenderUsesCard.newBuilder()
            builder.setPosition(CardPositionToProto(msg.onPosition))
            builder.setNeutralDistribution(NeutralDistributionToProto(msg.neutralDistribution))
            builder.setId(msg.card.id)
            msg.target.map(t => builder.setTarget(CardTargetToProto(t)))
            builder
          }.build,
          module
        )
      ))

      stay()
    }

    //End sub-turn by player
    case Event(MessageEvent(msg: cz.noveit.proto.firstenchanter.FirstEnchanterMatch.DefenderEndsTurn, module, rh, ctx), data) => {
      if (msg.getUserName == data.playerName) {
        matchHandler ! DefenderEndsTurn(data.playerName)
      }
      stay()
    }

    //Team-mate or enemy ended turn
    case Event(ServiceModuleMessage(msg: DefenderEndsTurn, mod, rh), data) => {

      webContext.send(serializeToString(
        MessageEvent(
          cz.noveit.proto.firstenchanter.FirstEnchanterMatch.DefenderEndsTurn
            .newBuilder()
            .setUserName(msg.user)
            .build(),
          module
        )
      ))
      stay()
    }
  }

  when(ATTACKER_TEAM_ATTACK) {

    //Asking player to attacks
    case Event(ServiceModuleMessage(msg: AskAttackerToAttack, mod, rh), data) => {
      webContext.send(serializeToString(
        MessageEvent(
          cz.noveit.proto.firstenchanter.FirstEnchanterMatch.AskAttackerToAttack
            .newBuilder()
            .addAllPossibleAttacks(
              msg.possibleAttacks.map(
                pa => {
                  val builder = cz.noveit.proto.firstenchanter.FirstEnchanterMatch.AttackerAttacks
                    .newBuilder()
                  builder.setPosition(CardPositionToProto(pa.withPosition))
                  builder.setNeutralDistribution(NeutralDistributionToProto(NeutralDistribution(0, 0, 0, 0)))
                  builder.setId(pa.card.id)
                  builder.build
                }
              ).toList.asJava)
            .build()
          ,
          module
        )
      ))
      stay()
    }

    //Asking defender to wait
    case Event(ServiceModuleMessage(AskDefenderToWaitForAttack, mod, rh), data) => {
      webContext.send(serializeToString(
        MessageEvent(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.AskDefenderToWaitForAttack.newBuilder().build(), module)
      ))
      stay()
    }

    //Asking possible attacks
    case Event(MessageEvent(msg: cz.noveit.proto.firstenchanter.FirstEnchanterMatch.AskPossibleAttacks, module, ctx, rh), data) => {
      if (data.playerName != msg.getUserName) {
        data.tableState.map(ts => {
          if (ts.arePlayersOnSameTeam(data.playerName, msg.getUserName)) {
            matchHandler ! AskPossibleAttacks(msg.getUserName)
          }
        })

      } else {
        matchHandler ! AskPossibleAttacks(data.playerName)
      }
      stay()
    }

    //Possible attacks
    case Event(ServiceModuleMessage(msg: PossibleAttacks, mod, rh), data) => {
      webContext.send(serializeToString(
        MessageEvent(
          cz.noveit.proto.firstenchanter.FirstEnchanterMatch.PossibleAttacks
            .newBuilder()
            .addAllPossibleAttacks(
              msg.attacks.map(
                pa => {
                  val builder = cz.noveit.proto.firstenchanter.FirstEnchanterMatch.AttackerAttacks
                    .newBuilder()
                  builder.setPosition(CardPositionToProto(pa.withPosition))
                  builder.setNeutralDistribution(NeutralDistributionToProto(NeutralDistribution(0, 0, 0, 0)))
                  builder.setId(pa.card.id)
                  builder.build
                }
              ).toList.asJava)
            .build()
          ,
          module
        )
      ))
      stay()
    }

    //Card attacked by player
    case Event(MessageEvent(msg: cz.noveit.proto.firstenchanter.FirstEnchanterMatch.AttackerAttacks, module, rh, ctx), data) => {
      val position = ProtoToCardPosition(msg.getPosition)
      if (data.playerName == position.player) {
        putHashForPosition(position, rh)
        matchHandler ! AttackerAttacks(position, CardHolder.EMPTY, List())
      }
      stay()
    }


    //Confirmation of card attack
    case Event(ServiceModuleMessage(msg: AttackAccepted, mod, rh), data) => {
      webContext.send(serializeToString(
        MessageEvent(
          {
            val builder = cz.noveit.proto.firstenchanter.FirstEnchanterMatch.PlayerAttackAccepted.newBuilder()
            builder.setPosition(CardPositionToProto(msg.position))
            builder.setNeutralDistribution(NeutralDistributionToProto(NeutralDistribution.ZERO))
            builder.addAllPossibleAttacks(
              msg.possibleAttacks.map(
                pa => {
                  val builder = cz.noveit.proto.firstenchanter.FirstEnchanterMatch.AttackerAttacks
                    .newBuilder()
                  builder.setPosition(CardPositionToProto(pa.withPosition))
                  builder.setNeutralDistribution(NeutralDistributionToProto(NeutralDistribution(0, 0, 0, 0)))
                  builder.setId(pa.card.id)
                  builder.build

                }
              ).toList.asJava)
            builder
          }.build,
          module,
          popHashForPosition(msg.position).getOrElse("")
        )
      ))
      stay()
    }

    //Card attack declined
    case Event(ServiceModuleMessage(msg: AttackDeclined, mod, rh), data) => {
      webContext.send(serializeToString(
        MessageEvent(
          cz.noveit.proto.firstenchanter.FirstEnchanterMatch.PlayerAttackDeclined
            .newBuilder()
            .setPosition(CardPositionToProto(msg.position))
            .build(),
          module,
          popHashForPosition(msg.position).getOrElse("")
        )
      ))
      stay()
    }

    //Team-mate or enemy attacked with card
    case Event(ServiceModuleMessage(msg: AttackerAttacks, mod, rh), data) => {
      webContext.send(serializeToString(
        MessageEvent(
          cz.noveit.proto.firstenchanter.FirstEnchanterMatch.AttackerAttacks
            .newBuilder()
            .setPosition(CardPositionToProto(msg.withPosition))
            .setNeutralDistribution(NeutralDistributionToProto(NeutralDistribution(0, 0, 0, 0)))
            .setId(msg.card.id)
            .build(),
          module
        )
      ))
      stay()
    }

    //End sub-turn by player
    case Event(MessageEvent(msg: cz.noveit.proto.firstenchanter.FirstEnchanterMatch.AttackerEndsTurn, module, rh, ctx), data) => {
      if (msg.getUserName == data.playerName) {
        matchHandler ! AttackerEndsTurn(data.playerName)
      }
      stay()
    }

    //Team-mate or enemy ended turn
    case Event(ServiceModuleMessage(msg: AttackerEndsTurn, mod, rh), data) => {
      webContext.send(serializeToString(
        MessageEvent(
          cz.noveit.proto.firstenchanter.FirstEnchanterMatch.AttackerEndsTurn
            .newBuilder()
            .setUserName(msg.user)
            .build(),
          module
        )
      ))
      stay()
    }
  }

  when(DEFENDER_TEAM_DEFENDS) {
    //Asking player to use cards
    case Event(ServiceModuleMessage(msg: AskDefenderToDefend, mod, rh), data) => {
      webContext.send(serializeToString(
        MessageEvent(
          cz.noveit.proto.firstenchanter.FirstEnchanterMatch.AskDefenderToDefend
            .newBuilder()
            .addAllPossibleDefends(
              msg.defends.map(
                pd => {
                  val builder = cz.noveit.proto.firstenchanter.FirstEnchanterMatch.DefenderDefends
                    .newBuilder()
                  builder.setPosition(CardPositionToProto(pd.withPosition))
                  builder.setTarget(CardTargetToProto(pd.againts))
                  builder.setNeutralDistribution(NeutralDistributionToProto(pd.neutralDistribution))
                  builder.setId(pd.defenderCard.id)
                  builder.build
                }
              ).toList.asJava)
            .build()
          ,
          module
        )
      ))
      stay()
    }

    //Asking defender to wait
    case Event(ServiceModuleMessage(AskAttackerToWaitForDefend, mod, rh), data) => {
      webContext.send(serializeToString(
        MessageEvent(
          cz.noveit.proto.firstenchanter.FirstEnchanterMatch.AskAttackerToWaitForDefend
            .newBuilder()
            .build(),
          module
        )
      ))
      stay()
    }

    //Asking possible defends
    case Event(MessageEvent(msg: cz.noveit.proto.firstenchanter.FirstEnchanterMatch.AskPossibleDefending, module, rh, ctx), data) => {
      if (data.playerName != msg.getUserName) {
        data.tableState.map(ts => {
          if (ts.arePlayersOnSameTeam(data.playerName, msg.getUserName)) {
            matchHandler ! AskPossibleDefending(msg.getUserName)
          }
        })
      } else {
        matchHandler ! AskPossibleDefending(data.playerName)
      }
      stay()
    }

    //Possible defends
    case Event(ServiceModuleMessage(msg: PossibleDefending, mod, rh), data) => {
      webContext.send(serializeToString(
        MessageEvent(
          cz.noveit.proto.firstenchanter.FirstEnchanterMatch.PossibleDefending
            .newBuilder()
            .addAllPossibleDefends(
              msg.defends.map(
                pd => {
                  val builder = cz.noveit.proto.firstenchanter.FirstEnchanterMatch.DefenderDefends
                    .newBuilder()
                  builder.setPosition(CardPositionToProto(pd.withPosition))
                  builder.setTarget(CardTargetToProto(pd.againts))
                  builder.setNeutralDistribution(NeutralDistributionToProto(pd.neutralDistribution))
                  builder.setId(pd.defenderCard.id)
                  builder.build
                }
              ).toList.asJava)
            .build()
          ,
          module
        )
      ))
      stay()
    }

    //Card defends  by player
    case Event(MessageEvent(msg: cz.noveit.proto.firstenchanter.FirstEnchanterMatch.DefenderDefends, module, rh, ctx), data) => {
      val position = ProtoToCardPosition(msg.getPosition)
      val target = ProtoToCardTarget(msg.getTarget)
      val neutralDistribution = ProtoToNeutralDistribution(msg.getNeutralDistribution)

      if (data.playerName == position.player) {
        putHashForPosition(position, rh)
        matchHandler ! DefenderDefends(position, target, CardHolder.EMPTY, neutralDistribution, List())
      }
      stay()
    }

    //Confirmation of card defends
    case Event(ServiceModuleMessage(msg: DefendingAccepted, mod, rh), data) => {
      webContext.send(serializeToString(
        MessageEvent(
          cz.noveit.proto.firstenchanter.FirstEnchanterMatch.PlayerDefendsAccepted
            .newBuilder()
            .setPosition(CardPositionToProto(msg.position))
            .setTarget(CardTargetToProto(msg.attacker))
            .setNeutralDistribution(NeutralDistributionToProto(msg.neutralDistribution))
            .addAllPossibleDefends(
              msg.possibleDefends.map(
                pd => {
                  val builder = cz.noveit.proto.firstenchanter.FirstEnchanterMatch.DefenderDefends
                    .newBuilder()
                  builder.setPosition(CardPositionToProto(pd.withPosition))
                  builder.setTarget(CardTargetToProto(pd.againts))
                  builder.setNeutralDistribution(NeutralDistributionToProto(pd.neutralDistribution))
                  builder.setId(pd.defenderCard.id)
                  builder.build
                }
              ).toList.asJava)
            .build()
          ,
          module,
          popHashForPosition(msg.position).getOrElse("")
        )
      ))
      stay()
    }

    //Card defends declined
    case Event(ServiceModuleMessage(msg: DefendingDeclined, mod, rh), data) => {
      webContext.send(serializeToString(
        MessageEvent(
          cz.noveit.proto.firstenchanter.FirstEnchanterMatch.PlayerDefendsDeclined
            .newBuilder()
            .setPosition(CardPositionToProto(msg.position))
            .setTarget(CardTargetToProto(msg.attacker))
            .build()
          ,
          module,
          popHashForPosition(msg.position).getOrElse("")
        )
      ))
      stay()
    }

    //Team-mate or enemy defends again a card
    case Event(ServiceModuleMessage(msg: DefenderDefends, mod, rh), data) => {
      webContext.send(serializeToString(
        MessageEvent(
          cz.noveit.proto.firstenchanter.FirstEnchanterMatch.DefenderDefends
            .newBuilder()
            .setPosition(CardPositionToProto(msg.withPosition))
            .setTarget(CardTargetToProto(msg.againts))
            .setNeutralDistribution(NeutralDistributionToProto(msg.neutralDistribution))
            .setId(msg.defenderCard.id)
            .build()
          ,
          module
        )
      ))
      stay()
    }

    //End sub-turn by player
    case Event(MessageEvent(msg: cz.noveit.proto.firstenchanter.FirstEnchanterMatch.DefenderEndsTurn, module, rh, ctx), data) => {
      if (msg.getUserName == data.playerName) {
        matchHandler ! DefenderEndsTurn(data.playerName)
      }
      stay()
    }

    //Team-mate or enemy ended turn
    case Event(ServiceModuleMessage(msg: DefenderEndsTurn, mod, rh), data) => {
      webContext.send(serializeToString(
        MessageEvent(
          cz.noveit.proto.firstenchanter.FirstEnchanterMatch.DefenderEndsTurn
            .newBuilder()
            .setUserName(msg.user)
            .build(),
          module
        )
      ))
      stay()
    }
  }

  when(EVALUATION_AND_ANIMATION) {
    case Event(ServiceModuleMessage(msg: EvaluateUsageDone, mod, rh), data) => {
      val encodedString = new String(Base64.encode(serializeToMessage(
        MessageEvent(
          cz.noveit.proto.firstenchanter.FirstEnchanterMatch.EvaluateUsageDone
            .newBuilder
            .addAllChanges(
              msg.changesStack.map(change => {
                SerializeProtoTableChange(change.toProtoMessage)
              }).asJava
            ).build()
          , module)
      ).toByteArray), "UTF-8")
      webContext.send(encodedString)
      stay()
    }

    case Event(ServiceModuleMessage(msg: EvaluateAttackDone, mod, rh), data) => {
      val encodedString = new String(Base64.encode(serializeToMessage(
        MessageEvent(
          cz.noveit.proto.firstenchanter.FirstEnchanterMatch.EvaluateAttackDone
            .newBuilder
            .addAllChanges(
              msg.changesStack.map(change => {
                SerializeProtoTableChange(change.toProtoMessage)
              }).asJava
            ).build()
          , module)
      ).toByteArray), "UTF-8")
      webContext.send(encodedString)
      stay()
    }

    case Event(MessageEvent(msg: cz.noveit.proto.firstenchanter.FirstEnchanterMatch.AnimationDone, module, rh, ctx), data) => {
      matchHandler ! AnimationDone(data.playerName)
      stay()
    }
  }

  whenUnhandled {


    case Event(ServiceModuleMessage(msg: PlayerDisconnectedFromMatch, mod, rh), data) => {
      if (msg.playerName == data.playerName) {
        webContext.disconnect()
      }
      stay()
    }

    case Event(ServiceModuleMessage(msg: StateAsk, module, rh), data) => {
      val encodedString = new String(Base64.encode(serializeToMessage(
        MessageEvent(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.StateAsk.newBuilder
          .setState(
            msg.state match {
              case PLAYER_STARTS_ROLL =>
                cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchState.PLAYER_STARTS_ROLL
              case FORTUNE_ROLL =>
                cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchState.FORTUNE_ROLL
              case ATTACKER_TEAM_PLAY =>
                cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchState.ATTACKER_TEAM_PLAY
              case DEFENDER_TEAM_COUNTER_PLAY =>
                cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchState.DEFENDER_TEAM_COUNTER_PLAY
              case ATTACKER_TEAM_ATTACK =>
                cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchState.ATTACKER_TEAM_ATTACK
              case DEFENDER_TEAM_DEFENDS =>
                cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchState.DEFENDER_TEAM_DEFENDS
              case EVALUATION_AND_ANIMATION =>
                cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchState.EVALUATION_AND_ANIMATION
            }
          )
          .setPlayerState(TableStateToProto(msg.tableState, msg.tableState.getPlayerTeam(data.playerName)))
          .setTimeAnchor(msg.timeAnchor)
          .setTimeInThisState(msg.timInState)
          .build(),
          module
        )
      ).toByteArray), "UTF-8")
      webContext.send(encodedString)

      stay using (data.copy(tableState = Some(msg.tableState)))
    }

    case Event(MessageEvent(msg: cz.noveit.proto.firstenchanter.FirstEnchanterMatch.StateAck, module, rh, ctx), data) => {
      val state = msg.getState match {
        case cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchState.PLAYER_STARTS_ROLL =>
          PLAYER_STARTS_ROLL
        case cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchState.FORTUNE_ROLL =>
          FORTUNE_ROLL
        case cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchState.ATTACKER_TEAM_PLAY =>
          ATTACKER_TEAM_PLAY
        case cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchState.DEFENDER_TEAM_COUNTER_PLAY =>
          DEFENDER_TEAM_COUNTER_PLAY
        case cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchState.ATTACKER_TEAM_ATTACK =>
          ATTACKER_TEAM_ATTACK
        case cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchState.DEFENDER_TEAM_DEFENDS =>
          DEFENDER_TEAM_DEFENDS
        case cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchState.EVALUATION_AND_ANIMATION =>
          EVALUATION_AND_ANIMATION
      }

      matchHandler ! StateAck(
        state,
        data.playerName
      )
      log.debug("Changing state to: " + state)
      goto(state)
    }

    case Event(msg, data) =>
      log.error("Error in state: " + stateName + ", uknown message: " + msg.toString)
      stay()
  }

  onTermination {
    case StopEvent(reason, state, data) =>
      matchHandler ! PlayerDisconnectedFromMatch(data.playerName)
  }
}

object SerializeProtoTableChange {
  def apply(protoMessage: com.google.protobuf.Message): cz.noveit.proto.firstenchanter.FirstEnchanterMatch.TableChange = {
    cz.noveit.proto.firstenchanter.FirstEnchanterMatch.TableChange.newBuilder
      .setSerializedChange(protoMessage.toByteString)
      .setChangeName(protoMessage.getClass.getSimpleName)
      .setChangePackage(protoMessage.getClass.getPackage.getName)
      .build
  }
}
