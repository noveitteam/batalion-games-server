package cz.noveit.games.cardgame.engine.handlers

import akka.actor.{Terminated, ActorRef, ActorLogging, Actor}
import cz.noveit.proto.serialization.{MessageEvent, MessageSerializer}
import cz.noveit.games.cardgame.engine.game._
import scala.Some
import cz.noveit.games.cardgame.adapters.User
import cz.noveit.connector.websockets.WebSocketsContext
import org.bouncycastle.util.encoders.Base64
import cz.noveit.games.cardgame.engine.{GetServiceModuleHandler, CrossHandlerMessage, ServiceModuleMessage}

import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import scala.concurrent.Await

/**
 * Created by Wlsek on 22.4.14.
 */

trait MatchLobbyHandler extends PlayerService {
}

class TeamToken {
}

class LobbyToken {
}

trait MatchLobbyInvitationState

case object MatchLobbyInvitationStateInvited extends MatchLobbyInvitationState

case object MatchLobbyInvitationStateAccepted extends MatchLobbyInvitationState

case class MatchLobbyPlayer(teamToken: String, handler: ActorRef, user: User, invitationState: MatchLobbyInvitationState)

case class MatchLobby(author: ActorRef, matchType: MatchGameMode, enemyMode: MatchGameEnemyMode, teamMode: MatchTeamMode, playersInLobby: List[MatchLobbyPlayer], lobbyToken: LobbyToken, teamTokens: List[String], arena: Option[String] = None)

case class InvitePlayerToLobby(matchType: MatchGameMode, enemyMode: MatchGameEnemyMode, teamMode: MatchTeamMode, lobbyToken: LobbyToken, byUser: String) extends CrossHandlerMessage

case object InvitationAccepted

case object InvitationDeclined

case class PlayerJoinSuccess(lobby: MatchLobby)

case object PlayerJoinFailed

case object PlayerLeave

case class PlayerChangeTeam(teamToken: String)

case class PlayerChangedTeam(player: MatchLobbyPlayer)

case class PlayerKicked(player: MatchLobbyPlayer)

case class PlayerLeft(player: MatchLobbyPlayer)

case object MatchLobbyDisbanded

case class MatchLobbySetupChanged(matchType: MatchGameMode, enemyMode: MatchGameEnemyMode, teamMode: MatchTeamMode)

case object GetPlayerLobbyForControl extends CrossHandlerMessage

class MatchLobbyHandlerActor(val module: String, parameters: PlayerServiceParameters) extends MatchLobbyHandler with Actor with ActorLogging with MessageSerializer {

  case class InvitePlayerToLobbyPair(invitation: InvitePlayerToLobby, sender: ActorRef)

  val playerHandler = parameters.playerActor
  val playerContext = parameters.context

  var controllingLobby: Option[MatchLobby] = None
  var inLobby: Option[ActorRef] = None
  var lobbyInvitations: List[InvitePlayerToLobbyPair] = List()

  def receive = {

    case ServiceModuleMessage(message, mod, replyHash) =>
      message match {
        case invitation: InvitePlayerToLobby =>
          if (lobbyInvitations.find(inv => inv.invitation.lobbyToken equals invitation.lobbyToken).isEmpty) {
            lobbyInvitations = InvitePlayerToLobbyPair(invitation, sender) :: lobbyInvitations

            val invitationBuilder = cz.noveit.proto.firstenchanter.FirstEnchanterMatchLobby.PlayerLobbyInvitation.newBuilder()
            invitationBuilder.setByUserName(invitation.byUser)

            val matchSetupBuilder = cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.newBuilder()

            invitation.enemyMode match {
              case MatchGameEnemyModePVE =>
                matchSetupBuilder.setEnemyMode(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.GameEnemyMode.PVE)

              case MatchGameEnemyModePVP =>
                matchSetupBuilder.setEnemyMode(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.GameEnemyMode.PVP)
            }

            invitation.matchType match {
              case MatchGameModePractice =>
                matchSetupBuilder.setGameMode(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.GameMode.PRACTICE)

              case MatchGameModeRanked =>
                matchSetupBuilder.setGameMode(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.GameMode.RANKED)
            }

            invitation.teamMode match {
              case MatchTeamModeOneVsOne =>
                matchSetupBuilder.setTeamMode(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.TeamMode.ONEvONE)
              case MatchTeamModeTwoVsTwo =>
                matchSetupBuilder.setTeamMode(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.TeamMode.TWOvTWO)
            }

            matchSetupBuilder.setActiveDeckName("")
            invitationBuilder.setLobbyInvitationId(invitation.lobbyToken.toString)
            invitationBuilder.setMatchSetup(matchSetupBuilder.build())

            val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(invitationBuilder.build(), module, "")).toByteArray), "UTF-8")
            playerContext.asInstanceOf[WebSocketsContext].send(encodedString)
          }

        case GetPlayerLobbyForControl =>
          sender ! controllingLobby

        case t => if(module != mod) {
          playerHandler.tell(ServiceModuleMessage(t, mod, replyHash), sender)
        }
      }

    //Matchlobby controll messages
    case InvitationAccepted =>
      if (controllingLobby.isEmpty) sender ! PlayerJoinFailed
      controllingLobby.map(lobby => {
        val playerInLobby = lobby.playersInLobby.find(p => {
          p.handler == sender
        })
        if (playerInLobby.isEmpty) sender ! PlayerJoinFailed
        playerInLobby.map(found => {
          val newLobby = lobby.copy(playersInLobby = found.copy(invitationState = MatchLobbyInvitationStateAccepted) :: lobby.playersInLobby.filterNot(p => p.user.nickname.equals(found.user.nickname)))
          controllingLobby = Some(newLobby)
          lobby.playersInLobby.filter(p => p.invitationState == MatchLobbyInvitationStateAccepted).map(p => p.handler ! PlayerChangedTeam(found))
          found.handler ! PlayerJoinSuccess(newLobby)
        })
      })

    case InvitationDeclined =>
      controllingLobby.map(lobby => {
        lobby.playersInLobby.find(p => p.handler equals (sender)).map(found => {
          controllingLobby = Some(lobby.copy(playersInLobby = lobby.playersInLobby.filterNot(p => p.equals(found))))
        })
      })

    case PlayerLeave =>
      controllingLobby.map(lobby => {
        lobby.playersInLobby.find(p => p.handler equals (sender)).map(found => {
          controllingLobby = Some(lobby.copy(playersInLobby = lobby.playersInLobby.filterNot(p => p.equals(found))))
          lobby.playersInLobby.map(p => p.handler ! PlayerLeft(found))
        })
      })

    case PlayerChangeTeam(teamToken) =>
      controllingLobby.map(lobby => {
        if (lobby.teamTokens.contains(teamToken)) {
          lobby.playersInLobby.find(p => p.handler equals (sender)).map(found => {
            val newMemberDetails = found.copy(teamToken = teamToken);
            val newLobby = lobby.copy(playersInLobby = newMemberDetails :: lobby.playersInLobby.filterNot(p => p.equals(found)))
            controllingLobby = Some(newLobby)
            newLobby.playersInLobby.filter(p => p.invitationState == MatchLobbyInvitationStateAccepted).map(p => p.handler ! PlayerChangedTeam(newMemberDetails))
          })
        }
      })

    //Match lobby status messages
    case PlayerJoinFailed =>
      val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(cz.noveit.proto.firstenchanter.FirstEnchanterMatchLobby.PlayerLobbyInvitationAcceptFailed.newBuilder.build(), module, "")).toByteArray), "UTF-8")
      playerContext.asInstanceOf[WebSocketsContext].send(encodedString)

    case PlayerJoinSuccess(lobby) =>

      inLobby = Some(sender)

      val acceptSuccess = cz.noveit.proto.firstenchanter.FirstEnchanterMatchLobby.PlayerLobbyInvitationAcceptSuccess.newBuilder;
      lobby.teamTokens.map(t => acceptSuccess.addTeams(t))
      lobby.playersInLobby.filter(p => p.invitationState == MatchLobbyInvitationStateAccepted).map(p => {
        val joinBuilder = cz.noveit.proto.firstenchanter.FirstEnchanterMatchLobby.PlayerInLobbyJoin.newBuilder()
        joinBuilder.setTeam(p.teamToken)
        joinBuilder.setUserName(p.user.nickname)
        joinBuilder.setAvatarId(p.user.avatar.getOrElse(-1))

        acceptSuccess.addPlayers(joinBuilder.build())
      })

      val matchSetupBuilder = cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.newBuilder()

      lobby.enemyMode match {
        case MatchGameEnemyModePVE =>
          matchSetupBuilder.setEnemyMode(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.GameEnemyMode.PVE)

        case MatchGameEnemyModePVP =>
          matchSetupBuilder.setEnemyMode(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.GameEnemyMode.PVP)
      }

      lobby.matchType match {
        case MatchGameModePractice =>
          matchSetupBuilder.setGameMode(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.GameMode.PRACTICE)

        case MatchGameModeRanked =>
          matchSetupBuilder.setGameMode(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.GameMode.RANKED)
      }

      lobby.teamMode match {
        case MatchTeamModeOneVsOne =>
          matchSetupBuilder.setTeamMode(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.TeamMode.ONEvONE)
        case MatchTeamModeTwoVsTwo =>
          matchSetupBuilder.setTeamMode(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.TeamMode.TWOvTWO)
      }

      matchSetupBuilder.setActiveDeckName("")
      acceptSuccess.setMatchSetup(matchSetupBuilder.build())


      val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(acceptSuccess.build(), module, "")).toByteArray), "UTF-8")
      playerContext.asInstanceOf[WebSocketsContext].send(encodedString)


    case PlayerChangedTeam(player) =>
      val joinBuilder = cz.noveit.proto.firstenchanter.FirstEnchanterMatchLobby.PlayerInLobbyJoin.newBuilder()
      joinBuilder.setTeam(player.teamToken)
      joinBuilder.setUserName(player.user.nickname)
      joinBuilder.setAvatarId(player.user.avatar.getOrElse(-1))

      val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(joinBuilder.build(), module, "")).toByteArray), "UTF-8")
      playerContext.asInstanceOf[WebSocketsContext].send(encodedString)

    case PlayerKicked(player) =>
      if (this.parameters.player.nickname equals (player.user.nickname)) inLobby = None
      val leftBuilder = cz.noveit.proto.firstenchanter.FirstEnchanterMatchLobby.PlayerInLobbyLeft.newBuilder()
      leftBuilder.setUserName(player.user.nickname)

      val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(leftBuilder.build(), module, "")).toByteArray), "UTF-8")
      playerContext.asInstanceOf[WebSocketsContext].send(encodedString)

    case PlayerLeft(player) =>
      if (this.parameters.player.nickname equals (player.user.nickname)) inLobby = None
      val leftBuilder = cz.noveit.proto.firstenchanter.FirstEnchanterMatchLobby.PlayerInLobbyLeft.newBuilder()
      leftBuilder.setUserName(player.user.nickname)

      val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(leftBuilder.build(), module, "")).toByteArray), "UTF-8")
      playerContext.asInstanceOf[WebSocketsContext].send(encodedString)

    case MatchLobbyDisbanded =>
      inLobby = None
      val leftBuilder = cz.noveit.proto.firstenchanter.FirstEnchanterMatchLobby.MatchLobbyDisbanded.newBuilder()

      val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(leftBuilder.build(), module, "")).toByteArray), "UTF-8")
      playerContext.asInstanceOf[WebSocketsContext].send(encodedString)


    case msg: MatchLobbySetupChanged =>
      val matchLobbySetupChanged = cz.noveit.proto.firstenchanter.FirstEnchanterMatchLobby.MatchLobbySetupChanged.newBuilder()
      val matchSetupBuilder = cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.newBuilder()

      matchSetupBuilder.setActiveDeckName("")

      msg.enemyMode match {
        case MatchGameEnemyModePVE =>
          matchSetupBuilder.setEnemyMode(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.GameEnemyMode.PVE)

        case MatchGameEnemyModePVP =>
          matchSetupBuilder.setEnemyMode(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.GameEnemyMode.PVP)
      }

      msg.matchType match {
        case MatchGameModePractice =>
          matchSetupBuilder.setGameMode(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.GameMode.PRACTICE)

        case MatchGameModeRanked =>
          matchSetupBuilder.setGameMode(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.GameMode.RANKED)
      }

      msg.teamMode match {
        case MatchTeamModeOneVsOne =>
          matchSetupBuilder.setTeamMode(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.TeamMode.ONEvONE)
        case MatchTeamModeTwoVsTwo =>
          matchSetupBuilder.setTeamMode(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.TeamMode.TWOvTWO)
      }

      matchLobbySetupChanged.setMatchSetup(matchSetupBuilder.build())
      val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(matchLobbySetupChanged.build(), module, "")).toByteArray), "UTF-8")
      playerContext.asInstanceOf[WebSocketsContext].send(encodedString)


    case e: MessageEvent =>
      e.message match {
        case msg: cz.noveit.proto.firstenchanter.FirstEnchanterMatchLobby.GetListOfInvitations =>

          val listBuilder = cz.noveit.proto.firstenchanter.FirstEnchanterMatchLobby.ListOfInvitations.newBuilder()
          this.lobbyInvitations.map(pair => {
            val invitationBuilder = cz.noveit.proto.firstenchanter.FirstEnchanterMatchLobby.PlayerLobbyInvitation.newBuilder()
            invitationBuilder.setByUserName(pair.invitation.byUser)

            val matchSetupBuilder = cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.newBuilder()

            pair.invitation.enemyMode match {
              case MatchGameEnemyModePVE =>
                matchSetupBuilder.setEnemyMode(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.GameEnemyMode.PVE)

              case MatchGameEnemyModePVP =>
                matchSetupBuilder.setEnemyMode(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.GameEnemyMode.PVP)
            }

            pair.invitation.matchType match {
              case MatchGameModePractice =>
                matchSetupBuilder.setGameMode(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.GameMode.PRACTICE)

              case MatchGameModeRanked =>
                matchSetupBuilder.setGameMode(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.GameMode.RANKED)
            }

            pair.invitation.teamMode match {
              case MatchTeamModeOneVsOne =>
                matchSetupBuilder.setTeamMode(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.TeamMode.ONEvONE)
              case MatchTeamModeTwoVsTwo =>
                matchSetupBuilder.setTeamMode(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.TeamMode.TWOvTWO)
            }

            matchSetupBuilder.setActiveDeckName("")
            invitationBuilder.setLobbyInvitationId(pair.invitation.lobbyToken.toString)
            invitationBuilder.setMatchSetup(matchSetupBuilder.build())

            listBuilder.addInvitations(
              invitationBuilder.build()
            )
          })

          val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(listBuilder.build(), module, e.replyHash)).toByteArray), "UTF-8")
          playerContext.asInstanceOf[WebSocketsContext].send(encodedString)


        case msg: cz.noveit.proto.firstenchanter.FirstEnchanterMatchLobby.ChangeMatchLobbySetup =>
          msg.getSetup.getGameMode match {
            case cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.GameMode.PRACTICE =>
              controllingLobby.map(lobby => {
                controllingLobby = Some(lobby.copy(matchType = MatchGameModePractice))
              })
            case cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.GameMode.RANKED =>
              controllingLobby.map(lobby => {
                controllingLobby = Some(lobby.copy(matchType = MatchGameModeRanked))
              })
          }

          msg.getSetup.getEnemyMode match {
            case cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.GameEnemyMode.PVE =>
              controllingLobby.map(lobby => {
                controllingLobby = Some(lobby.copy(enemyMode = MatchGameEnemyModePVE))
              })
            case cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.GameEnemyMode.PVP =>
              controllingLobby.map(lobby => {
                controllingLobby = Some(lobby.copy(enemyMode = MatchGameEnemyModePVP))
              })
          }

          msg.getSetup.getTeamMode match {
            case cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.TeamMode.ONEvONE =>
              controllingLobby.map(lobby => {
                controllingLobby = Some(lobby.copy(teamMode = MatchTeamModeOneVsOne))
              })
            case cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.TeamMode.TWOvTWO =>
              controllingLobby.map(lobby => {
                controllingLobby = Some(lobby.copy(teamMode = MatchTeamModeTwoVsTwo))
              })
          }

          controllingLobby.map(lobby => {
            lobby.playersInLobby.map(p => p.handler ! MatchLobbySetupChanged(lobby.matchType, lobby.enemyMode, lobby.teamMode))
          })


        case msg: cz.noveit.proto.firstenchanter.FirstEnchanterMatchLobby.RequestCreateMatchLobby =>
          var teams:List[String] = List()

          controllingLobby = Some(
            MatchLobby(
              self,
              msg.getSetup.getGameMode match {
                case cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.GameMode.PRACTICE =>
                  teams = List(new TeamToken toString, new TeamToken toString)
                  MatchGameModePractice
                case cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.GameMode.RANKED =>
                  teams = List(new TeamToken toString)
                  MatchGameModeRanked
              },
              msg.getSetup.getEnemyMode match {
                case cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.GameEnemyMode.PVE =>
                  MatchGameEnemyModePVE
                case cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.GameEnemyMode.PVP =>
                  MatchGameEnemyModePVP
              },
              msg.getSetup.getTeamMode match {
                case cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.TeamMode.ONEvONE =>
                  MatchTeamModeOneVsOne
                case cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.TeamMode.TWOvTWO =>
                  MatchTeamModeTwoVsTwo
              }, List(MatchLobbyPlayer(teams(0), self, parameters.player, MatchLobbyInvitationStateAccepted)), new LobbyToken, teams
            )
          )

          controllingLobby.map(lobby => {

            val lobyCreationSuccess = cz.noveit.proto.firstenchanter.FirstEnchanterMatchLobby.RequestCreateMatchLobbySuccess.newBuilder

            lobby.teamTokens.map(t => lobyCreationSuccess.addTeams(t))
            lobby.playersInLobby.filter(p => p.invitationState == MatchLobbyInvitationStateAccepted).map(p => {
              val joinBuilder = cz.noveit.proto.firstenchanter.FirstEnchanterMatchLobby.PlayerInLobbyJoin.newBuilder()
              joinBuilder.setTeam(p.teamToken)
              joinBuilder.setUserName(p.user.nickname)
              joinBuilder.setAvatarId(p.user.avatar.getOrElse(-1))

              lobyCreationSuccess.addPlayers(joinBuilder.build())
            })

            val matchSetupBuilder = cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.newBuilder()

            lobby.enemyMode match {
              case MatchGameEnemyModePVE =>
                matchSetupBuilder.setEnemyMode(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.GameEnemyMode.PVE)

              case MatchGameEnemyModePVP =>
                matchSetupBuilder.setEnemyMode(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.GameEnemyMode.PVP)
            }

            lobby.matchType match {
              case MatchGameModePractice =>
                matchSetupBuilder.setGameMode(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.GameMode.PRACTICE)

              case MatchGameModeRanked =>
                matchSetupBuilder.setGameMode(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.GameMode.RANKED)
            }

            lobby.teamMode match {
              case MatchTeamModeOneVsOne =>
                matchSetupBuilder.setTeamMode(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.TeamMode.ONEvONE)
              case MatchTeamModeTwoVsTwo =>
                matchSetupBuilder.setTeamMode(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.MatchSetup.TeamMode.TWOvTWO)
            }

            matchSetupBuilder.setActiveDeckName("")
            lobyCreationSuccess.setMatchSetup(matchSetupBuilder.build())

            inLobby = Some(self)

            val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(lobyCreationSuccess.build(), module, "")).toByteArray), "UTF-8")

            playerContext.asInstanceOf[WebSocketsContext].send(encodedString)
          }).getOrElse({
            val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(
              cz.noveit.proto.firstenchanter.FirstEnchanterMatchLobby.RequestCreateMatchLobbyFailed.newBuilder.build()
              , module, "")).toByteArray), "UTF-8")
            playerContext.asInstanceOf[WebSocketsContext].send(encodedString)
          })


        case msg: cz.noveit.proto.firstenchanter.FirstEnchanterMatchLobby.InvitePlayerToLobby =>
          controllingLobby.map(lobby => {
            implicit val timeout = Timeout(10 seconds)
            var future = playerHandler ? ServiceModuleMessage(IsFriend(msg.getUserName), ServiceHandlersModules.cardGameSocial, "")
            val qphandler = Await.result(future, timeout.duration).asInstanceOf[Option[FriendHandlePair]]
            qphandler.map(p => p.playerHandler.map(ph => {
              future = ph ? ServiceModuleMessage(GetUserDetails, ServiceHandlersModules.cardGameAccountDetails, "")
              val user = Await.result(future, timeout.duration).asInstanceOf[User]
              future = ph ? GetServiceModuleHandler(module)
              val matchLobbyHandler = Await.result(future, timeout.duration).asInstanceOf[ActorRef]
              controllingLobby = Some(lobby.copy(playersInLobby = MatchLobbyPlayer(lobby.teamTokens(0), matchLobbyHandler, user, MatchLobbyInvitationStateInvited) :: lobby.playersInLobby))
              matchLobbyHandler ! ServiceModuleMessage(InvitePlayerToLobby(lobby.matchType, lobby.enemyMode, lobby.teamMode, lobby.lobbyToken, parameters.player.nickname), module, "")
            }))
          })

        case msg: cz.noveit.proto.firstenchanter.FirstEnchanterMatchLobby.PlayerLobbyInvitationAccept =>
          lobbyInvitations.find(p => p.invitation.lobbyToken.toString equals msg.getMatchLobbyToken).map(found => found.sender ! InvitationAccepted)
          lobbyInvitations = lobbyInvitations.filterNot(p => p.invitation.lobbyToken.toString equals msg.getMatchLobbyToken)

        case msg: cz.noveit.proto.firstenchanter.FirstEnchanterMatchLobby.PlayerLobbyInvitationDecline =>
          lobbyInvitations.find(p => p.invitation.lobbyToken.toString equals msg.getMatchLobbyToken).map(found => {
            found.sender ! InvitationDeclined
            lobbyInvitations = lobbyInvitations.filterNot(p => p equals (found))
          })

        case msg: cz.noveit.proto.firstenchanter.FirstEnchanterMatchLobby.PlayerChangeTeam =>
          inLobby.map(lobby => lobby ! PlayerChangeTeam(msg.getTeam))

        case msg: cz.noveit.proto.firstenchanter.FirstEnchanterMatchLobby.PlayerLeave =>
          inLobby.map(lobby => lobby ! PlayerLeave)

        case msg: cz.noveit.proto.firstenchanter.FirstEnchanterMatchLobby.MatchLobbyDisband =>
          controllingLobby.map(lobby => {
            lobby.playersInLobby.map(p => p.handler ! MatchLobbyDisbanded)
          })

          inLobby = None
          controllingLobby = None

        case msg: cz.noveit.proto.firstenchanter.FirstEnchanterMatchLobby.RequestPlayerChangeTeam =>
          controllingLobby.map(lobby => {
            if (lobby.teamTokens.contains(msg.getTeam) && !parameters.player.nickname.equals(msg.getUserName)) {
              lobby.playersInLobby.find(p => p.user.nickname equals msg.getUserName).map(found => {
                val newTeamMember = found.copy(teamToken = msg.getTeam)
                val newLobby = lobby.copy(playersInLobby = newTeamMember :: lobby.playersInLobby.filterNot(p => p.equals(found)))
                controllingLobby = Some(newLobby)
                newLobby.playersInLobby.filter(p => p.invitationState == MatchLobbyInvitationStateAccepted).map(p => p.handler ! PlayerChangedTeam(newTeamMember  ))
              })
            }
          })

        case msg: cz.noveit.proto.firstenchanter.FirstEnchanterMatchLobby.RequestPlayerKick =>
          controllingLobby.map(lobby => {
            if (!parameters.player.nickname.equals(msg.getUserName)) {
              lobby.playersInLobby.find(p => p.user.nickname equals msg.getUserName).map(found => {
                controllingLobby = Some(lobby.copy(playersInLobby = lobby.playersInLobby.filterNot(p => p.equals(found))))
                lobby.playersInLobby.map(p => p.handler ! PlayerKicked(found))
              })
            }
          })

        case t =>
          throw new UnsupportedOperationException(t.toString())
      }

  }

  override def postStop = {
    inLobby.map(lobby => lobby ! PlayerLeave)
    controllingLobby.map(lobby => {
      lobby.playersInLobby.filter(p => p.handler != self).map(p => p.handler ! MatchLobbyDisbanded)
    })
  }
}