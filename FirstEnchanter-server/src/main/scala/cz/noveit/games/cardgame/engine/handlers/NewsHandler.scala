package cz.noveit.games.cardgame.engine.handlers

import akka.actor.{Props, ActorLogging, Actor}
import cz.noveit.proto.serialization.{MessageSerializer, MessageEvent}
import cz.noveit.games.cardgame.adapters.{ListOfNews, GetNews, NewsDatabaseAdapter}
import cz.noveit.database.{DatabaseResult, DatabaseCommand}
import org.bouncycastle.util.encoders.Base64
import cz.noveit.connector.websockets.WebSocketsContext
import cz.noveit.proto.firstenchanter.FirstEnchanterNews._
import cz.noveit.proto.firstenchanter.FirstEnchanterNews.NewsList.Article

/**
 * Created by Wlsek on 11.2.14.
 */

trait NewsHandler extends PlayerService {
}

class NewsHandlerActor(val module: String, parameters: PlayerServiceParameters) extends CardsHandler with Actor with ActorLogging with MessageSerializer {

  val playerHandler = parameters.playerActor
  val playerContext = parameters.context

  lazy val newsDatabaseAdapter = context.actorOf(Props(classOf[NewsDatabaseAdapter],
    context.actorSelection("/user/databaseController"),
    parameters.databaseParams("databaseName"),
    parameters.databaseParams("newsCollection")
  ))


  def receive = {
    case e: MessageEvent =>
      e.message match {
        case m: GetLastNews =>
          if (m.hasLocalization) {
            newsDatabaseAdapter ! DatabaseCommand(GetNews(m.getSkip, m.getLimit, Some(m.getLocalization)), e.replyHash)
          } else {
            newsDatabaseAdapter ! DatabaseCommand(GetNews(m.getSkip, m.getLimit, None), e.replyHash)
          }
        case t => throw new UnsupportedOperationException(t.toString())
      }

    case r: DatabaseResult =>
      r.result match {
        case m: ListOfNews =>
          val listBuilder = NewsList.newBuilder()
          m.news.foreach(n => {
            listBuilder.addListOfArticles(
              Article.newBuilder().setDescription(n.description).setNewsId(n.newsId).setTimeStamp(n.dateTime).setTitle(n.title).build()
            )
          })

          val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(listBuilder.build(), module, r.replyHash)).toByteArray), "UTF-8")
          playerContext.asInstanceOf[WebSocketsContext].send(encodedString)
        case t => throw new UnsupportedOperationException(t.toString())
      }
  }
}