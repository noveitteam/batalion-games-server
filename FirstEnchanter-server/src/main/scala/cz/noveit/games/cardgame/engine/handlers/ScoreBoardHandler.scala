package cz.noveit.games.cardgame.engine.handlers

import cz.noveit.proto.serialization.{MessageSerializer, MessageEvent}
import akka.actor.{ActorRef, Props, ActorLogging, Actor}
import cz.noveit.games.cluster.dispatch.{ClusterMessageEventWithReplier, ClusterMessageEvent}
import cz.noveit.database.{DatabaseCommand, DatabaseResult}
import cz.noveit.proto.firstenchanter.FirstEnchanterScoreBoard._
import cz.noveit.games.cardgame.adapters._
import cz.noveit.connector.websockets.WebSocketsContext
import org.bouncycastle.util.encoders.Base64
import cz.noveit.games.cardgame.engine.{CrossHandlerMessage, ServiceModuleMessage}
import akka.pattern.ask
import akka.util.Timeout
import scala.collection.immutable.HashMap
import scala.concurrent.duration._
import scala.concurrent.Await
import cz.noveit.games.cardgame.adapters.DivisionForPlayerReturned
import cz.noveit.proto.serialization.MessageEvent
import cz.noveit.games.cardgame.adapters.GetDivisionForPlayer
import cz.noveit.games.cardgame.adapters.GetPlayerScore
import cz.noveit.database.DatabaseResult
import cz.noveit.database.DatabaseCommand

/**
 * Created by Wlsek on 28.2.14.
 */


case class GetScoreBoardForPlayer(player: String) extends CrossHandlerMessage

trait ScoreBoardHandler extends PlayerService {
}


class ScoreBoardHandlerActor(val module: String, parameters: PlayerServiceParameters) extends ScoreBoardHandler with Actor with ActorLogging with MessageSerializer {

  val playerHandler = parameters.playerActor
  val playerContext = parameters.context
  val player = parameters.player

  val databaseAdapter = context.actorOf(
    Props(
      classOf[ScoreBoardDatabaseAdapter],
      context.actorSelection("/user/databaseController"),
      parameters.databaseParams("databaseName"),
      parameters.databaseParams("userCollection"),
      parameters.databaseParams("scoreBoardCollection")
    ))

  def receive = {
    case msg: MessageEvent =>
      msg.message match {
        case m: GetScoreBoard =>
          databaseAdapter ! DatabaseCommand(GetDivisionForPlayer(player.nickname), msg.replyHash)

        case m: FindScoreBoardForPlayer =>
          databaseAdapter ! DatabaseCommand(GetDivisionForPlayer(m.getUserName), msg.replyHash)
      }

    case msg: ServiceModuleMessage =>
      msg.message match {
        case GetScoreBoardForPlayer(player) =>
          try {
            implicit val timeout = Timeout(5 seconds)
            val future = databaseAdapter ? DatabaseCommand(GetPlayerScore(player), msg.replyHash)
            val result = Await.result(future, timeout.duration).asInstanceOf[DatabaseResult]

            result.result match {
              case PlayerScoreReturned(returned) =>
                returned.map(score => sender ! score)
            }
          } catch {
            case t: Throwable => log.error("Exception while waiting for result {}", t.toString)
          }


      }

    case msg: DatabaseResult =>
      msg.result match {
        case DivisionForPlayerReturned(division) =>
          val encodedString = division.map(division => {
            val sbBuilder = ScoreBoard.newBuilder()
              .setId(division.divisionId)
              .setName(division.name)
              .setLevel(division.level)
              .setCreated(division.created)

            division.members.map(m => {
              sbBuilder.addPlayers(cz.noveit.proto.firstenchanter.FirstEnchanterScoreBoard.PlayerScore.newBuilder().setUserName(m.user).setActualScore(m.score).setRating(m.rating).build())
            })

            new String(Base64.encode(serializeToMessage(MessageEvent(
              cz.noveit.proto.firstenchanter.FirstEnchanterScoreBoard.ScoreBoardFound.newBuilder().setScoreBoard(sbBuilder.build()).build(), module, msg.replyHash)).toByteArray), "UTF-8")
          }).getOrElse({
            new String(Base64.encode(serializeToMessage(MessageEvent(cz.noveit.proto.firstenchanter.FirstEnchanterScoreBoard.NoScoreBoard.newBuilder.build(), module, msg.replyHash)).toByteArray), "UTF-8")
          })

          playerContext.asInstanceOf[WebSocketsContext].send(encodedString)


      }
  }
}