package cz.noveit.games.cardgame.engine.handlers

import akka.actor.{Props, ActorContext, ActorRef}
import cz.noveit.proto.serialization.MessageEventContext
import cz.noveit.games.cardgame.adapters.User
import scala.collection.immutable.HashMap

/**
 * Created by Wlsek on 10.2.14.
 */

case class PlayerServiceHandler(module: String, handler: ActorRef)
case class PlayerServiceParameters(context: MessageEventContext, playerActor: ActorRef,  player: User, databaseParams: HashMap[String, String] = new HashMap())

trait PlayerService {
  val module: String
  val playerHandler: ActorRef
  val playerContext: MessageEventContext
}


trait ServiceHandlersModuleFactory {
  def module(ctx: ActorContext, parameters: PlayerServiceParameters): ServiceHandlersModule
}



trait ServiceHandlersModule {
  var handlers: List[PlayerServiceHandler] = List()
  def configure(ctx: ActorContext, parameters: PlayerServiceParameters)
  def <--[T](module: String, ctx: ActorContext, parameters: PlayerServiceParameters)(implicit  m: Manifest[T]) = {
    handlers = PlayerServiceHandler(module, ctx.actorOf(Props(m.runtimeClass.asInstanceOf[Class[T]],module, parameters))) :: handlers
  }
}


