package cz.noveit.games.cardgame.engine.handlers

import akka.actor.ActorContext

/**
 * Created by Wlsek on 10.2.14.
 */


object ServiceHandlersModules {
  val  cardGameCards = "FirstEnchanterCards"
  val  cardGameCodeRedemption = "FirstEnchanterCodeRedemption"
  val  cardGameMatch = "FirstEnchanterMatch"
  val  cardGameNews = "FirstEnchanterNews"
  val  cardGameSocial = "FirstEnchanterSocial"
  val  cardGameStat = "FirstEnchanterStat"
  val  cardGameAccountDetails = "FirstEnchanterAccountDetails"
  val  cardGameScore = "FirstEnchanterScoreBoard"
  val  cardGameMatchLobby = "FirstEnchanterMatchLobby"
  val  cardGameConversation = "FirstEnchanterConversation"
  val  cardGameStore = "FirstEnchanterStore"
  val  cardGamePlayer = "FirstEnchanterPlayer"
}

class ServiceHandlersModuleFactoryImpl extends  ServiceHandlersModuleFactory {
  def module(ctx: ActorContext, parameters: PlayerServiceParameters): ServiceHandlersModule = {
    val module = new ServiceHandlersModuleImpl()
    module.configure(ctx, parameters)
    module
  }
}


class ServiceHandlersModuleImpl extends ServiceHandlersModule {
     def configure(ctx: ActorContext, parameters: PlayerServiceParameters) = {
       <--[CardsHandlerActor] (ServiceHandlersModules.cardGameCards, ctx, parameters)
       <--[CodeHandlerActor]  (ServiceHandlersModules.cardGameCodeRedemption, ctx, parameters)
       <--[MatchHandlerActor] (ServiceHandlersModules.cardGameMatch, ctx, parameters)
       <--[NewsHandlerActor]  (ServiceHandlersModules.cardGameNews, ctx, parameters)
       <--[SocialHandlerActor](ServiceHandlersModules.cardGameSocial, ctx, parameters)
       <--[StatHandlerActor]  (ServiceHandlersModules.cardGameStat, ctx, parameters)
       <--[AccountDetailsActor](ServiceHandlersModules.cardGameAccountDetails, ctx, parameters)
       <--[ScoreBoardHandlerActor](ServiceHandlersModules.cardGameScore, ctx, parameters)
       <--[MatchLobbyHandlerActor](ServiceHandlersModules.cardGameMatchLobby, ctx, parameters)
       <--[ConversationHandlerActor](ServiceHandlersModules.cardGameConversation, ctx, parameters)
       <--[StoreHandlerActor](ServiceHandlersModules.cardGameStore, ctx, parameters)
     }
}
