package cz.noveit.games.cardgame.engine.handlers

import akka.actor.{ActorRef, Props, ActorLogging, Actor}
import cz.noveit.proto.serialization.{MessageSerializer}
import cz.noveit.games.cardgame.engine._
import cz.noveit.games.cardgame.adapters._
import org.bouncycastle.util.encoders.Base64
import cz.noveit.connector.websockets.WebSocketsContext
import cz.noveit.games.cluster.dispatch.ClusterMessage
import akka.util.Timeout
import scala.concurrent.duration._

import cz.noveit.proto.firstenchanter.FirstEnchanterSocial._
import cz.noveit.games.cardgame.adapters.AddFriendRequestFailed
import cz.noveit.games.cardgame.adapters.AcceptFriendDone
import cz.noveit.games.cardgame.adapters.RemoveFriendDone
import cz.noveit.games.cardgame.adapters.RemoveFriend
import cz.noveit.games.cardgame.adapters.AllFriendShips
import scala.Some
import cz.noveit.games.cardgame.engine.PlayerOffline
import cz.noveit.database.DatabaseCommand
import cz.noveit.games.cardgame.engine.PlayerOnline
import cz.noveit.games.cardgame.engine.RequestPlayersStatus
import cz.noveit.games.cardgame.adapters.AddFriendRequestFailedUserDoesntExists
import cz.noveit.games.cardgame.adapters.GetFriendshipsForUser
import cz.noveit.proto.serialization.MessageEvent
import cz.noveit.games.cardgame.adapters.AddFriendRequestSuccessful
import cz.noveit.games.cardgame.adapters.AcceptFriend
import cz.noveit.database.DatabaseResult

/**
 * Created by Wlsek on 10.2.14.
 */
trait SocialHandler extends PlayerService {
}

case class RefreshFriendsForUsers(users: List[String]) extends PlayerControllerMessage

case class RefreshFriendships(user: String) extends ClusterMessage with PlayerControllerMessageWithPlayerNameAndServiceHandlerModule {
  val playerName = user
  val serviceHandlerModule  = ServiceHandlersModules.cardGameSocial
}

case object RefreshFriendships extends CrossHandlerMessage

case class InviteFriendToAParty(fromUser: String, replyTo: ActorRef) extends CrossHandlerMessage

case object InvitationToAPartyAccepted extends CrossHandlerMessage

case object InvitationToAPartyDeclined extends CrossHandlerMessage

case object DisbandAParty extends CrossHandlerMessage

case object GetPartyMember  extends CrossHandlerMessage

case class FriendHandlePair(userName: String, playerHandler: Option[ActorRef])

case class PartyMember(member: Option[FriendHandlePair]) extends CrossHandlerMessage

case class IsFriend(userName: String) extends CrossHandlerMessage

class SocialHandlerActor(val module: String, parameters: PlayerServiceParameters) extends CardsHandler with Actor with ActorLogging with MessageSerializer {

  val playerHandler = parameters.playerActor
  val playerContext = parameters.context
  val player = parameters.player

  lazy val socialDatabaseAdapter = context.actorOf(
    Props(classOf[SocialDatabaseAdapter],
      context.actorSelection("/user/databaseController"),
      parameters.databaseParams("databaseName"),
      parameters.databaseParams("userCollection"),
      parameters.databaseParams("friendshipsCollection")
    ))

  var friends: List[FriendHandlePair] = List()

  self ! ServiceModuleMessage(RefreshFriendships, module, "")

  def receive = {
    case MessageEvent(msg, module, replyHash, ctx) =>
      msg match {
        case msg: GetFriendList =>
          socialDatabaseAdapter ! DatabaseCommand(GetFriendshipsForUser(player.nickname), replyHash)

        case msg: FriendAdd =>
          socialDatabaseAdapter ! DatabaseCommand(AddFriendRequest(player.nickname, msg.getUserName), replyHash)

        case msg: FriendRemove =>
          socialDatabaseAdapter ! DatabaseCommand(RemoveFriend(player.nickname, msg.getUserName), replyHash)

        case msg: FriendRequestAccept =>
          socialDatabaseAdapter ! DatabaseCommand(AcceptFriend( msg.getUserName, player.nickname), replyHash)

        case msg: FriendRequestDecline =>
          socialDatabaseAdapter ! DatabaseCommand(RemoveFriend(msg.getUserName, player.nickname), replyHash)

        }

    case ServiceModuleMessage(msg, mod, replyHash) => {
      msg match {
        case PlayerOnline(user, handler) =>
          if (!friends.find(f => f.userName.equals(user.nickname)).isEmpty) {
            friends = FriendHandlePair(user.nickname, Some(handler)) :: friends.filterNot(f => f.userName.equals(user.nickname))
            val message = FriendOnline.newBuilder().setUserName(user.nickname).build()
            val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(message, module, "")).toByteArray), "UTF-8")
            playerContext.asInstanceOf[WebSocketsContext].send(encodedString)
          }

        case PlayerOffline(user, handler) =>
          if (!friends.find(f => f.userName.equals(user)).isEmpty) {
            friends = FriendHandlePair(user, None) :: friends.filterNot(f => f.userName.equals(user))
            playerHandler !  ServiceModuleMessage(PlayerOffline(user, handler), ServiceHandlersModules.cardGameMatch)
            val message = FriendOffline.newBuilder().setUserName(user).build()
            val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(message, module, "")).toByteArray), "UTF-8")
            playerContext.asInstanceOf[WebSocketsContext].send(encodedString)
          }

        case RefreshFriendships =>
          socialDatabaseAdapter ! DatabaseCommand(GetFriendshipsForUser(player.nickname), replyHash)

        case IsFriend(user) => {
          sender ! friends.find(f => f.userName == user)
        }

      }
    }

    case DatabaseResult(result, replyHash) =>
      result match {
        case AddFriendRequestFailedUserDoesntExists(user) =>
          val message = FriendAddFailed.newBuilder().setUserName(user).setReason(FriendAddFailed.Reason.USERDOESNTEXISTS).build()
          val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(message, module, replyHash)).toByteArray), "UTF-8")
          playerContext.asInstanceOf[WebSocketsContext].send(encodedString)

        case AddFriendRequestFailedAlreadyPresent(user) =>
          val message = FriendAddFailed.newBuilder().setUserName(user).setReason(FriendAddFailed.Reason.USERALREADYFRIEND).build()
          val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(message, module, replyHash)).toByteArray), "UTF-8")
          playerContext.asInstanceOf[WebSocketsContext].send(encodedString)

        case AddFriendRequestFailed(user) =>
          val message = FriendAddFailed.newBuilder().setUserName(user).setReason(FriendAddFailed.Reason.UNKNOWN).build()
          val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(message, module, replyHash)).toByteArray), "UTF-8")
          playerContext.asInstanceOf[WebSocketsContext].send(encodedString)

        case AddFriendRequestSuccessful(user) =>
          val message = FriendAddSuccessful.newBuilder().setUserName(user).build()
          val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(message, module, replyHash)).toByteArray), "UTF-8")
          playerContext.asInstanceOf[WebSocketsContext].send(encodedString)

          playerHandler ! RefreshFriendsForUsers(List(player.nickname, user))

        case AllFriendShips(friendships) =>
          val message = FriendList.newBuilder()
          friends = friendships.map(f => {

            val fBuilder = Friend.newBuilder()

            val friend = if(f.users._1.equals(player.nickname)) {

              Friend.FriendStatus.valueOf(f.state) match {
                case Friend.FriendStatus.WAITING_FOR_FRIEND_APROVAL =>
                  fBuilder.setStatus(Friend.FriendStatus.WAITING_FOR_FRIEND_APROVAL)
                case t =>
                  fBuilder.setStatus(Friend.FriendStatus.APROVED)
              }

              f.users._2
            }  else {
              Friend.FriendStatus.valueOf(f.state) match {
                case Friend.FriendStatus.WAITING_FOR_FRIEND_APROVAL =>
                  fBuilder.setStatus(Friend.FriendStatus.WAITING_FOR_YOUR_APROVAL)
                case t =>
                  fBuilder.setStatus(Friend.FriendStatus.APROVED)
              }

              f.users._1
            }

            fBuilder.setUserName(friend)
            message.addListOfFriends(fBuilder)
            FriendHandlePair(friend, None)
          }).toList

          val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(message.build(), module, replyHash)).toByteArray), "UTF-8")
          playerContext.asInstanceOf[WebSocketsContext].send(encodedString)

          friends.map(f => {
            playerHandler ! RequestPlayersStatus(f.userName, context.actorOf(Props(classOf[PlayerStateAskHandler], f.userName, module, self.asInstanceOf[ActorRef])))
          })

        case RemoveFriendDone(user) =>
          playerHandler ! RefreshFriendsForUsers(List(player.nickname, user))

        case AcceptFriendDone(user) =>
          playerHandler ! RefreshFriendsForUsers(List(player.nickname, user))
      }

    case t => throw new NotImplementedError("Message not implemented: " + t.toString)
  }
}
class PlayerStateAskHandler(askingUser: String, module: String, handler: ActorRef) extends Actor with ActorLogging {
  context.setReceiveTimeout(10 seconds)

  def receive = {
    case PlayerOnline(user, h) =>
      handler ! ServiceModuleMessage(PlayerOnline(user, h), module , "")
      context.stop(self)

    case Timeout =>
      handler ! ServiceModuleMessage(PlayerOffline(askingUser, handler), module, "")
      context.stop(self)
  }
}