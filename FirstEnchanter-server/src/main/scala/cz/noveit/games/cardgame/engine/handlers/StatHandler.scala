package cz.noveit.games.cardgame.engine.handlers

import cz.noveit.proto.firstenchanter.FirstEnchanterStat._
import cz.noveit.proto.firstenchanter.FirstEnchanterMatch._

import akka.actor.{Props, ActorLogging, Actor}
import cz.noveit.proto.serialization.{MessageSerializer, MessageEvent}
import cz.noveit.games.cardgame.adapters.{OverviewForUser, GetOverviewForUser, StatDatabaseAdapter}
import cz.noveit.database.{DatabaseResult, DatabaseCommand}
import org.bouncycastle.util.encoders.Base64
import cz.noveit.connector.websockets.WebSocketsContext

/**
 * Created by Wlsek on 10.2.14.
 */
trait StatHandler extends PlayerService {
}

class StatHandlerActor(val module: String, parameters: PlayerServiceParameters) extends StatHandler with Actor with ActorLogging with MessageSerializer {

  val playerHandler = parameters.playerActor
  val playerContext = parameters.context
  val player = parameters.player

  lazy val databaseAdapter = context.actorOf(
    Props(classOf[StatDatabaseAdapter],
      context.actorSelection("/user/databaseController"),
      parameters.databaseParams("databaseName"),
      parameters.databaseParams("statCollection"),
      parameters.databaseParams("matchCollection"),
      parameters.databaseParams("transactionsCollection"),
      parameters.databaseParams("scoreBoardCollection")
    ))

  def receive = {
    case msg: MessageEvent =>
      msg.message match {
        case m: GetUserGameStat =>
          databaseAdapter ! DatabaseCommand(GetOverviewForUser(player.nickname), msg.replyHash)
      }

    case msg: DatabaseResult =>
      msg.result match {
        case m: OverviewForUser => {
          val us = UserStats.newBuilder()

          us.setPractice1Vs1PVPGames(m.practiceStat.oneVsOne.pvp.games)
          us.setPractice1Vs1PVPWins(m.practiceStat.oneVsOne.pvp.wins)
          us.setPractice1Vs1PVPLoss(m.practiceStat.oneVsOne.pvp.loss)

          us.setPractice1Vs1PVEGames(m.practiceStat.oneVsOne.pve.games)
          us.setPractice1Vs1PVEWins(m.practiceStat.oneVsOne.pve.wins)
          us.setPractice1Vs1PVELoss(m.practiceStat.oneVsOne.pve.loss)

          us.setPractice2Vs2PVPGames(m.practiceStat.twoVsTwo.pvp.games)
          us.setPractice2Vs2PVPWins(m.practiceStat.twoVsTwo.pvp.wins)
          us.setPractice2Vs2PVPLoss(m.practiceStat.twoVsTwo.pvp.loss)

          us.setPractice2Vs2PVEGames(m.practiceStat.twoVsTwo.pve.games)
          us.setPractice2Vs2PVEWins(m.practiceStat.twoVsTwo.pve.wins)
          us.setPractice2Vs2PVELoss(m.practiceStat.twoVsTwo.pve.loss)

          us.setRanked1Vs1PVPGames(m.rankedStat.oneVsOne.games)
          us.setRanked1Vs1PVPWins(m.rankedStat.oneVsOne.wins)
          us.setRanked1Vs1PVPLoss(m.rankedStat.oneVsOne.loss)

          us.setRanked2Vs2PVPGames(m.rankedStat.twoVsTwo.games)
          us.setRanked2Vs2PVPWins(m.rankedStat.twoVsTwo.wins)
          us.setRanked2Vs2PVPLoss(m.rankedStat.twoVsTwo.loss)

          m.matches.map(pastMatch => {
            val setupBuilder = MatchSetup.newBuilder()
            setupBuilder.setGameMode(MatchSetup.GameMode.valueOf(pastMatch.gameType))
            setupBuilder.setEnemyMode(MatchSetup.GameEnemyMode.valueOf(pastMatch.enemyType))
            setupBuilder.setTeamMode(MatchSetup.TeamMode.valueOf(pastMatch.teamType))
            setupBuilder.setActiveDeckName(pastMatch.activeDeck)

            val matchBuilder =  cz.noveit.proto.firstenchanter.FirstEnchanterMatch.PastMatch
              .newBuilder()
              .setTime(pastMatch.started)
              .setDuration(pastMatch.duration)
              .setResult(MatchResult.valueOf(pastMatch.resultType))
              .setMatchSetup(setupBuilder.build)


            pastMatch.winners.map(winner => matchBuilder.addWinners(winner))
            pastMatch.losers.map(loser => matchBuilder.addLosers(loser))

            us.addMatches(matchBuilder.build)
          })

          m.transactions.map(transaction => {
            val transBuilder = Transaction.newBuilder()
            transBuilder.setCurrencyName(transaction.currency)
            transBuilder.setTime(transaction.time)
            transBuilder.setValue(transaction.value)
            transBuilder.setDetails(transaction.details)
            us.addTransactions(transBuilder.build)
          })

          us.setRating(m.rating)
          us.setCurrentRank(m.rankedRank)

          val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(us.build(), module, msg.replyHash)).toByteArray), "UTF-8")
          playerContext.asInstanceOf[WebSocketsContext].send(encodedString)

        }
      }
  }
}
