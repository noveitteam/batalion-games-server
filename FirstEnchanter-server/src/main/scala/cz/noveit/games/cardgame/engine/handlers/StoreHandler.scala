package cz.noveit.games.cardgame.engine.handlers

import akka.actor.{ActorRef, Props, ActorLogging, Actor}
import cz.noveit.proto.serialization.{MessageEvent, MessageSerializer}
import cz.noveit.games.cardgame.adapters._
import cz.noveit.connector.websockets.WebSocketsContext
import org.bouncycastle.util.encoders.Base64
import dispatch._
import scala.concurrent.ExecutionContext.Implicits.global


import spray.json._
import cz.noveit.games.cardgame.WorldServices
import cz.noveit.games.cardgame.engine.code.{CannotRedeemReason}
import cz.noveit.games.cardgame.engine.unlockable._
import cz.noveit.games.cardgame.adapters.UnlockAblesForStoreItem
import cz.noveit.games.cardgame.adapters.StoreItemFound
import cz.noveit.games.cardgame.adapters.GetStoreItem
import cz.noveit.database.DatabaseCommand
import cz.noveit.proto.serialization.MessageEvent
import cz.noveit.games.cardgame.adapters.SaveTransaction
import cz.noveit.games.cardgame.engine.code.CodeRedeemed
import cz.noveit.database.DatabaseResult
import cz.noveit.games.cardgame.adapters.AllStoreItems
import cz.noveit.games.cardgame.adapters.AvatarUnlocked
import cz.noveit.games.cardgame.engine.unlockable.IsUnlockAble
import cz.noveit.games.cardgame.engine.unlockable.CannotUnlockContent
import cz.noveit.games.cardgame.engine.code.CannotRedeemCode
import cz.noveit.games.cardgame.engine.unlockable.ContentUnlock
import cz.noveit.games.cardgame.adapters.CardsUnlocked
import cz.noveit.games.cardgame.engine.unlockable.ContentUnlocked
import cz.noveit.games.cardgame.adapters.BoosterStoreItem
import cz.noveit.games.cardgame.adapters.CompleteTransactionDetails
import cz.noveit.games.cardgame.adapters.DeckIconUnlocked
import cz.noveit.games.cardgame.adapters.GetUnlockAblesForStoreItem
import cz.noveit.games.cardgame.engine.code.RedeemCode
import cz.noveit.games.cardgame.adapters.CardSkinUnlocked
import cz.noveit.games.cardgame.adapters.UnlockAbleDeckForUserUnlocked
import cz.noveit.games.cardgame.engine.unlockable.UnlockAbleTable

/**
 * Created by Wlsek on 12.6.14.
 */

trait StoreHandler extends PlayerService {
}

class StoreHandlerActor(val module: String, parameters: PlayerServiceParameters) extends CardsHandler with Actor with ActorLogging with MessageSerializer {

  val playerHandler = parameters.playerActor
  val playerContext = parameters.context
  val player = parameters.player

  lazy val storeDatabaseAdapter = context.actorOf(
    Props(classOf[StoreDatabaseAdapters],
      context.actorSelection("/user/databaseController"),
      parameters.databaseParams("databaseName"),
      parameters.databaseParams("transactionsCollection"),
      parameters.databaseParams("storeItemsCollection"),
      parameters.databaseParams("userCollection")
    ))

  def receive = {
    case msg: MessageEvent => {
      msg.message match {
        case m: cz.noveit.proto.firstenchanter.FirstEnchanterStore.GetStoreItems =>
          context.actorOf(Props(classOf[GetStoreItemsActor], msg, playerContext.asInstanceOf[WebSocketsContext], storeDatabaseAdapter, player.nickname))

        case m: cz.noveit.proto.firstenchanter.FirstEnchanterStore.ValidateAppleStoreReceipt =>
          context.actorOf(Props(classOf[ValidateAppleStoreReceiptActor], msg, m, playerContext.asInstanceOf[WebSocketsContext], storeDatabaseAdapter, player.nickname))

        case m: cz.noveit.proto.firstenchanter.FirstEnchanterStore.LocalizedDetailsForItemInAppleStore =>
          context.actorOf(Props(classOf[LocalizedDetailsForItemInAppleStoreActor], m, storeDatabaseAdapter))

        case m: cz.noveit.proto.firstenchanter.FirstEnchanterStore.CodeRedemption =>
          context.actorOf(Props(classOf[RedeemCodeActor], msg, m , player.nickname, playerContext.asInstanceOf[WebSocketsContext]))

        case m: cz.noveit.proto.firstenchanter.FirstEnchanterStore.BuyItemForGameMoney =>
          context.actorOf(Props(classOf[GameMoneyBuyActor], msg, m , player.nickname, playerContext.asInstanceOf[WebSocketsContext],storeDatabaseAdapter))
      }
    }
  }
}

class GetStoreItemsActor(e: MessageEvent, ctx: WebSocketsContext, databaseHandler: ActorRef, user: String) extends Actor with ActorLogging with MessageSerializer {
  databaseHandler ! DatabaseCommand(FindAllStoreItems)

  val unlockAbleService = context.actorSelection("/user/" + WorldServices.unlockAbleService)
  var storeItems: List[StoreItem] = List()

  /*
  *   val message = cz.noveit.proto.firstenchanter.FirstEnchanterStore.StoreItems.newBuilder
          si.map(item => {
            item.storeItemType match {
              case StoreItemType.StoreItemTypeBooster =>
                message.addBoosters(
                  cz.noveit.proto.firstenchanter.FirstEnchanterStore.StoreItemBoosterPack
                    .newBuilder
                    .setItemId(item.storeId)
                    .setDeckId(item.asInstanceOf[BoosterStoreItem].deckIconId)
                    .build
                )
            }
          })

          ctx.send(new String(Base64.encode(serializeToMessage(MessageEvent(message.build(), e.module, e.replyHash)).toByteArray), "UTF-8"))
          context.stop(self)
  *
  * */

  def receive = {
    case DatabaseResult(r, hash) => {
      r match {
        case AllStoreItems(si) =>
          storeItems = si
          var askUnlockAbles: List[String] = List()
          si.map(it => askUnlockAbles = it.unlockAbles ::: askUnlockAbles)
          unlockAbleService ! IsUnlockAble(askUnlockAbles, user)
      }
    }

    case UnlockAbleTable(rows) =>
      val acceptedItems = rows.filter(r => r.isUnlockAble == true)
      val message = cz.noveit.proto.firstenchanter.FirstEnchanterStore.StoreItems.newBuilder
      storeItems.map(item => {
        if(item.unlockAbles.filter(un => acceptedItems.find(i => i.unlockAble == un).isDefined).size > 0){
          item.storeItemType match {
            case StoreItemType.StoreItemTypeBooster =>
              message.addBoosters(
                cz.noveit.proto.firstenchanter.FirstEnchanterStore.StoreItemBoosterPack
                  .newBuilder
                  .setItemId(item.storeId)
                  .setDeckId(item.asInstanceOf[BoosterStoreItem].deckIconId)
                  .build
              )
          }
        }
      })

      ctx.send(new String(Base64.encode(serializeToMessage(MessageEvent(message.build(), e.module, e.replyHash)).toByteArray), "UTF-8"))
      context.stop(self)

    case IsUnlockAbleCheckError =>
      val message = cz.noveit.proto.firstenchanter.FirstEnchanterStore.StoreItems.newBuilder
      ctx.send(new String(Base64.encode(serializeToMessage(MessageEvent(message.build(), e.module, e.replyHash)).toByteArray), "UTF-8"))
      context.stop(self)
  }
}

class ValidateAppleStoreReceiptActor(
                                      e: MessageEvent,
                                      message: cz.noveit.proto.firstenchanter.FirstEnchanterStore.ValidateAppleStoreReceipt,
                                      ctx: WebSocketsContext,
                                      databaseHandler: ActorRef,
                                      userName: String
                                      ) extends Actor with ActorLogging with MessageSerializer {

  var purchaseDate = 0L
  var productId = ""
  var unlockAbles: List[String] = List()
  var unlockedContent: List[UnlockedContent] = List()

  val unlockAbleService = context.actorSelection("/user/" + WorldServices.unlockAbleService)

  try {
    val jsonString = " { \"receipt-data\":\"" + message.getReceiptDataBase64.replace("\n", "").replace("\r", "") + "\"} "

    val svc = url("https://sandbox.itunes.apple.com/verifyReceipt")
      .POST
      .setContentType("application/json", "UTF-8")
      .setBody(jsonString.parseJson.toString().getBytes("UTF-8"))

    val receipt = Http(svc OK as.String)
    val result = JsonParser(receipt())
    //"status", "purchase_date_ms", "product_id"
    result.asJsObject.getFields("receipt", "status") match {
      case Seq(JsObject(obj), JsNumber(status)) => /*
               databaseHandler ! DatabaseCommand(SaveTransaction(
                message.getTransaction.getTransactionId,
                "",
                "",
                obj("purchase_date_ms").asInstanceOf[JsString].value.toLong,
                userName,
                0.0,
                obj("product_id").asInstanceOf[JsString].value))    */
        purchaseDate = obj("purchase_date_ms").asInstanceOf[JsString].value.toLong
        productId = obj("product_id").asInstanceOf[JsString].value
        databaseHandler ! DatabaseCommand(GetUnlockAblesForStoreItem(obj("product_id").asInstanceOf[JsString].value))
      case o =>
        val m = cz.noveit.proto.firstenchanter.FirstEnchanterStore.ValidateAppleStoreReceiptFailed.newBuilder
        m.setTransactionId(message.getTransaction.getTransactionId)
        ctx.send(new String(Base64.encode(serializeToMessage(MessageEvent(m.build(), e.module, e.replyHash)).toByteArray), "UTF-8"))
        context.stop(self)
    }

  } catch {
    case t: Throwable =>
      log.error("Error while getting response from apple store: " + t)
      val m = cz.noveit.proto.firstenchanter.FirstEnchanterStore.ValidateAppleStoreReceiptFailed.newBuilder
      m.setTransactionId(message.getTransaction.getTransactionId)
      ctx.send(new String(Base64.encode(serializeToMessage(MessageEvent(m.build(), e.module, e.replyHash)).toByteArray), "UTF-8"))
      context.stop(self)
  }


  def receive = {
    case DatabaseResult(r, h) => {
      r match {
        case UnlockAblesForStoreItem(ua) =>
          if (ua.isEmpty) {
            val m = cz.noveit.proto.firstenchanter.FirstEnchanterStore.ValidateAppleStoreReceiptFailed.newBuilder
            m.setTransactionId(message.getTransaction.getTransactionId)
            ctx.send(new String(Base64.encode(serializeToMessage(MessageEvent(m.build(), e.module, e.replyHash)).toByteArray), "UTF-8"))
            context.stop(self)
          } else {
            unlockAbles = ua
            consumeNextUnlockAble
          }

        case SaveTransactionSuccessful =>
          val m = cz.noveit.proto.firstenchanter.FirstEnchanterStore.ValidateAppleStoreReceiptSuccessful.newBuilder
          val u = cz.noveit.proto.firstenchanter.FirstEnchanterStore.UnlockedContent.newBuilder

          unlockedContent.map(un => {
            un match {
              case AvatarUnlocked(avatarId, usr) =>
                u.addUnlockedAvatars(avatarId)

              case CardSkinUnlocked(cardSkinId, usr) =>
                u.addUnlockedCardSkins(cardSkinId)

              case DeckIconUnlocked(deckIconId, usr) =>
                u.addUnlockedDeckIcons(deckIconId)

              case UnlockAbleDeckForUserUnlocked(d, usr) =>
                val deckBuilder = cz.noveit.proto.firstenchanter.FirstEnchanterCards.CardDeck.newBuilder
                deckBuilder
                  .setName(d.name)
                  .setIconId(d.icon)
                  .setDeckId(d.id)
                  .setElementsRatio(
                    cz.noveit.proto.firstenchanter.FirstEnchanterCards.CardDeck.ElementsRatio
                      .newBuilder
                      .setWater(d.elementsRatio.water)
                      .setFire(d.elementsRatio.fire)
                      .setEarth(d.elementsRatio.earth)
                      .setAir(d.elementsRatio.air)
                      .build()
                  )

                d.cards.map(cws => {
                  deckBuilder
                    .addCards({
                    val cwsBuilder =
                      cz.noveit.proto.firstenchanter.FirstEnchanterCards.CountableCardWithSkin.newBuilder

                    cwsBuilder.setCardId(cws.cardId)
                    cwsBuilder.setCount(cws.count)
                    cws.skinId.map(skin => cwsBuilder.setSkinId(skin))
                    cwsBuilder.build
                  })
                })
              case CardsUnlocked(cards, usr) =>
                cards.map(c => u.addUnlockedCards(c))
            }
          })

          m.setContent(u.build())

          m.setTransactionId(message.getTransaction.getTransactionId)
          ctx.send(new String(Base64.encode(serializeToMessage(MessageEvent(m.build(), e.module, e.replyHash)).toByteArray), "UTF-8"))
          context.stop(self)

        case SaveTransactionFailed =>
          val m = cz.noveit.proto.firstenchanter.FirstEnchanterStore.ValidateAppleStoreReceiptFailed.newBuilder
          m.setTransactionId(message.getTransaction.getTransactionId)
          ctx.send(new String(Base64.encode(serializeToMessage(MessageEvent(m.build(), e.module, e.replyHash)).toByteArray), "UTF-8"))

          context.stop(self)
      }
    }

    case ContentUnlocked(id, uc) =>
      unlockAbles = unlockAbles.filterNot(f => f == id)
      unlockedContent  = uc :: unlockedContent
      consumeNextUnlockAble

    case CannotUnlockContent(id) =>
      val m = cz.noveit.proto.firstenchanter.FirstEnchanterStore.ValidateAppleStoreReceiptFailed.newBuilder
      m.setTransactionId(message.getTransaction.getTransactionId)
      ctx.send(new String(Base64.encode(serializeToMessage(MessageEvent(m.build(), e.module, e.replyHash)).toByteArray), "UTF-8"))
      context.stop(self)
  }

  def consumeNextUnlockAble = {
    if (unlockAbles.isEmpty) {
      databaseHandler ! DatabaseCommand(SaveTransaction(
        message.getTransaction.getTransactionId,
        "",
        "",
        purchaseDate,
        userName,
        0.0,
        productId,
        TransactionType.TransactionTypeStore
      ))
    } else {
      unlockAbleService ! ContentUnlock(unlockAbles.head, UnlockAbleMethod.UnlockAbleMethodTransaction, userName)
    }
  }
}

class LocalizedDetailsForItemInAppleStoreActor(
                                                message: cz.noveit.proto.firstenchanter.FirstEnchanterStore.LocalizedDetailsForItemInAppleStore,
                                                databaseHandler: ActorRef
                                                ) extends Actor with ActorLogging with MessageSerializer {

  databaseHandler ! DatabaseCommand(CompleteTransactionDetails(
    message.getCurrency,
    message.getPrice,
    message.getItemId,
    message.getTransactionId
  ))

  def receive = {
    case DatabaseResult(r, hash) => {
      r match {
        case CompleteTransactionDetailsSuccessful =>
          log.info("Transaction details completed successfully. " + message.getTransactionId)
          context.stop(self);
        case CompleteTransactionDetailsFailed =>
          log.info("Transaction details completion failed. " + message.getTransactionId)
          context.stop(self)
      }
    }
  }
}

class RedeemCodeActor(
                       e: MessageEvent,
                       message: cz.noveit.proto.firstenchanter.FirstEnchanterStore.CodeRedemption,
                       user: String,
                       ctx: WebSocketsContext
                       ) extends Actor with ActorLogging with MessageSerializer {

  val redemptionHandler = context actorSelection ("/user/" + WorldServices.codeRedemptionService)
  redemptionHandler ! RedeemCode(message.getCode, user)

  def receive = {
    case CannotRedeemCode(code, reason) =>
      val m = cz.noveit.proto.firstenchanter.FirstEnchanterStore.CodeRedemptionFailed.newBuilder
      reason match {
        case CannotRedeemReason.CannotRedeemReasonAlreadyOwnsUnlockAble =>
          m.setReason(cz.noveit.proto.firstenchanter.FirstEnchanterStore.CodeRedemptionFailed.CodeRedemptionFailedReason.CodeRedemptionFailedReasonOwn )
        case CannotRedeemReason.CannotRedeemReasonGeneral =>
          m.setReason(cz.noveit.proto.firstenchanter.FirstEnchanterStore.CodeRedemptionFailed.CodeRedemptionFailedReason.CodeRedemptionFailedReasonGeneral)
      }
      ctx.send(new String(Base64.encode(serializeToMessage(MessageEvent(m.build(), e.module, e.replyHash)).toByteArray), "UTF-8"))
      context.stop(self)

    case CodeRedeemed(code, unlockAbles) =>
      val m = cz.noveit.proto.firstenchanter.FirstEnchanterStore.CodeRedemptionSuccess.newBuilder
      val u = cz.noveit.proto.firstenchanter.FirstEnchanterStore.UnlockedContent.newBuilder

      unlockAbles.map(un => {
        un match {
          case AvatarUnlocked(avatarId, usr) =>
            u.addUnlockedAvatars(avatarId)

          case CardSkinUnlocked(cardSkinId, usr) =>
            u.addUnlockedCardSkins(cardSkinId)

          case DeckIconUnlocked(deckIconId, usr) =>
            u.addUnlockedDeckIcons(deckIconId)

          case UnlockAbleDeckForUserUnlocked(d, usr) =>
            val deckBuilder = cz.noveit.proto.firstenchanter.FirstEnchanterCards.CardDeck.newBuilder
            deckBuilder
              .setName(d.name)
              .setIconId(d.icon)
              .setDeckId(d.id)
              .setElementsRatio(
                cz.noveit.proto.firstenchanter.FirstEnchanterCards.CardDeck.ElementsRatio
                  .newBuilder
                  .setWater(d.elementsRatio.water)
                  .setFire(d.elementsRatio.fire)
                  .setEarth(d.elementsRatio.earth)
                  .setAir(d.elementsRatio.air)
                  .build()
              )

            d.cards.map(cws => {
              deckBuilder
                .addCards({
                val cwsBuilder =
                  cz.noveit.proto.firstenchanter.FirstEnchanterCards.CountableCardWithSkin.newBuilder

                cwsBuilder.setCardId(cws.cardId)
                cwsBuilder.setCount(cws.count)
                cws.skinId.map(skin => cwsBuilder.setSkinId(skin))
                cwsBuilder.build
              })
            })

            u.addDecks(deckBuilder.build)
          case CardsUnlocked(cards, usr) =>
            cards.map(c => u.addUnlockedCards(c))
        }
      })

      m.setContent(u.build())

      ctx.send(new String(Base64.encode(serializeToMessage(MessageEvent(m.build(), e.module, e.replyHash)).toByteArray), "UTF-8"))
      context.stop(self)

  }

}

class GameMoneyBuyActor(
                         e: MessageEvent,
                         message: cz.noveit.proto.firstenchanter.FirstEnchanterStore.BuyItemForGameMoney,
                         user: String,
                         ctx: WebSocketsContext,
                         databaseHandler: ActorRef
                         ) extends Actor with ActorLogging with MessageSerializer {

  val unlockAbleService = context.actorSelection("/user/" + WorldServices.unlockAbleService)

  var unlockAbles: List[String] = List()
  var unlocked: List[UnlockedContent] = List()
  var item: Option[StoreItem] = None

  databaseHandler ! DatabaseCommand(GetStoreItem(message.getStoreItem))

  def receive = {
    case DatabaseResult(result, h) =>
      result match {
        case StoreItemFound(i) =>
          item = Some(i)
          unlockAbles = i.unlockAbles
          consumeNextUnlockAble

        case SaveTransactionSuccessful =>
          val m = cz.noveit.proto.firstenchanter.FirstEnchanterStore.ItemForGameMoneyBuySuccess.newBuilder
          val u = cz.noveit.proto.firstenchanter.FirstEnchanterStore.UnlockedContent.newBuilder

          unlocked.map(un => {
            un match {
              case AvatarUnlocked(avatarId, usr) =>
                u.addUnlockedAvatars(avatarId)

              case CardSkinUnlocked(cardSkinId, usr) =>
                u.addUnlockedCardSkins(cardSkinId)

              case DeckIconUnlocked(deckIconId, usr) =>
                u.addUnlockedDeckIcons(deckIconId)

              case UnlockAbleDeckForUserUnlocked(d, usr) =>
                val deckBuilder = cz.noveit.proto.firstenchanter.FirstEnchanterCards.CardDeck.newBuilder
                deckBuilder
                  .setName(d.name)
                  .setIconId(d.icon)
                  .setDeckId(d.id)
                  .setElementsRatio(
                    cz.noveit.proto.firstenchanter.FirstEnchanterCards.CardDeck.ElementsRatio
                      .newBuilder
                      .setWater(d.elementsRatio.water)
                      .setFire(d.elementsRatio.fire)
                      .setEarth(d.elementsRatio.earth)
                      .setAir(d.elementsRatio.air)
                      .build()
                  )

                d.cards.map(cws => {
                  deckBuilder
                    .addCards({
                    val cwsBuilder =
                      cz.noveit.proto.firstenchanter.FirstEnchanterCards.CountableCardWithSkin.newBuilder

                    cwsBuilder.setCardId(cws.cardId)
                    cwsBuilder.setCount(cws.count)
                    cws.skinId.map(skin => cwsBuilder.setSkinId(skin))
                    cwsBuilder.build
                  })
                })

                u.addDecks(deckBuilder.build)
              case CardsUnlocked(cards, usr) =>
                cards.map(c => u.addUnlockedCards(c))
            }
          })

            m.setContent(u.build())
          item.map(i => m.setPrice(i.inGameMoneyCost))
          ctx.send(new String(Base64.encode(serializeToMessage(MessageEvent(m.build(), e.module, e.replyHash)).toByteArray), "UTF-8"))
          context.stop(self)

        case CannotFindStoreItem | SaveTransactionFailed  =>
          val m = cz.noveit.proto.firstenchanter.FirstEnchanterStore.ItemForGameMoneyBuyFail .newBuilder
          ctx.send(new String(Base64.encode(serializeToMessage(MessageEvent(m.build(), e.module, e.replyHash)).toByteArray), "UTF-8"))
          context.stop(self)

      }

    case ContentUnlocked(id, content) =>
      unlockAbles = unlockAbles.filterNot(f => f == id)
      unlocked = content :: unlocked
      consumeNextUnlockAble

    case CannotUnlockContent =>
      val m = cz.noveit.proto.firstenchanter.FirstEnchanterStore.ItemForGameMoneyBuyFail .newBuilder
      ctx.send(new String(Base64.encode(serializeToMessage(MessageEvent(m.build(), e.module, e.replyHash)).toByteArray), "UTF-8"))
      context.stop(self)
  }

  def consumeNextUnlockAble = {
     if(unlockAbles.size > 0) {
       unlockAbleService ! ContentUnlock(unlockAbles.head, UnlockAbleMethod.UnlockAbleMethodTransaction, user)
     } else {
       item.map(i => {
         databaseHandler ! DatabaseCommand(SaveTransaction(self.hashCode().toString + System.currentTimeMillis().toString, "crystals", "", System.currentTimeMillis(), user, i.inGameMoneyCost, i.storeId, TransactionType.TransactionTypeInGameMoney))
       })

     }
  }
}