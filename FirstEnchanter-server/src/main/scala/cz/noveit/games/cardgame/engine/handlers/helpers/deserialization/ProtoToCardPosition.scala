package cz.noveit.games.cardgame.engine.handlers.helpers.deserialization

import cz.noveit.games.cardgame.engine.card.{OnTableCardPosition, OnGraveyardPosition, InHandCardPosition, CardPosition}

/**
 * Created by arnostkuchar on 22.09.14.
 */
object ProtoToCardPosition {
  def apply(position: cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardPosition): CardPosition  = {
    position.getPlace match {
      case cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardPosition.CardPositionPlace.IN_HAND =>
        InHandCardPosition(position.getPlaceId, position.getPlayer)
      case cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardPosition.CardPositionPlace.ON_GRAVEYARD =>
        OnGraveyardPosition(position.getPlaceId, position.getPlayer)
      case cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardPosition.CardPositionPlace.ON_TABLE =>
        OnTableCardPosition(position.getPlaceId, position.getPlayer)
    }
  }
}
