package cz.noveit.games.cardgame.engine.handlers.helpers.deserialization

import cz.noveit.games.cardgame.engine.card.{PlayerIsTarget, PlayerTeamIsTarget, CardIsTarget, CardTarget}
import cz.noveit.games.cardgame.engine.handlers.helpers.serialization.CardPositionToProto
import scala.collection.JavaConverters._
/**
 * Created by arnostkuchar on 22.09.14.
 */

object ProtoToCardTarget {
  def apply(target: cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardTarget ): CardTarget = {
    target.getType match {
      case cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardTarget.CardTargetType.CardTargetTypeCard =>
        CardIsTarget(ProtoToCardPosition(target.getCardPosition))
      case cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardTarget.CardTargetType.CardTargetTypePlayer =>
        PlayerIsTarget(target.getUserName)
      case cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardTarget.CardTargetType.CardTargetTypePlayerTeam =>
          PlayerTeamIsTarget(target.getTeamPlayersList.asScala.toList)
    }
  }
}