package cz.noveit.games.cardgame.engine.handlers.helpers.deserialization

import cz.noveit.games.cardgame.engine.player.NeutralDistribution

/**
 * Created by arnostkuchar on 22.09.14.
 */
object ProtoToNeutralDistribution {
  def apply(distribution: cz.noveit.proto.firstenchanter.FirstEnchanterMatch.NeutralDistribution): NeutralDistribution = {
    NeutralDistribution(
      distribution.getFire,
      distribution.getWater,
      distribution.getEarth,
      distribution.getAir
    )
  }
}
