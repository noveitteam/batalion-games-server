package cz.noveit.games.cardgame.engine.handlers.helpers.serialization

import cz.noveit.games.cardgame.engine.card.CardHolder
import cz.noveit.proto.firstenchanter.FirstEnchanterCards.CountableCardWithSkin

/**
 * Created by arnostkuchar on 19.09.14.
 */
object CardHolderToProto {
  def apply(card: CardHolder): CountableCardWithSkin = {
    CountableCardWithSkin
      .newBuilder()
      .setCardId(card.id)
      .setCount(1)
      .setSkinId(card.skinId.getOrElse(-1))
      .build()
  }
}
