package cz.noveit.games.cardgame.engine.handlers.helpers.serialization

import cz.noveit.games.cardgame.engine.card.{OnGraveyardPosition, OnTableCardPosition, InHandCardPosition, CardPosition}

/**
 * Created by arnostkuchar on 19.09.14.
 */
object CardPositionToProto {
  def apply(position: CardPosition): cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardPosition = {
    val builder = cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardPosition.newBuilder()
    position match {
      case p: InHandCardPosition =>
        builder.setPlace(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardPosition.CardPositionPlace.IN_HAND)
      case p: OnTableCardPosition =>
        builder.setPlace(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardPosition.CardPositionPlace.ON_TABLE)
      case p: OnGraveyardPosition =>
        builder.setPlace(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardPosition.CardPositionPlace.ON_GRAVEYARD)
    }
    builder.setPlayer(position.player)
    builder.setPlaceId(position.positionId)
    builder.build()
  }
}
