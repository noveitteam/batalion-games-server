package cz.noveit.games.cardgame.engine.handlers.helpers.serialization

import cz.noveit.games.cardgame.engine.card._
import cz.noveit.games.cardgame.engine.game.TableState
import scala.collection.JavaConverters._
/**
 * Created by arnostkuchar on 19.09.14.
 */
/*
message CardTarget {
	enum CardTargetType {
        CardTargetTypePlayer = 0;
		CardTargetTypePlayerTeam = 1;
		CardTargetTypeCard = 2;
	}

	required CardTargetType type= 1;
	optional string userName = 2;
	optional CardPosition cardPosition = 3;
}

 */
object CardTargetToProto {
  def apply(target: CardTarget) = {
    val builder = cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardTarget.newBuilder()
    target match {
      case t: CardIsTarget =>
        builder.setCardPosition(CardPositionToProto(t.position))
        builder.setType(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardTarget.CardTargetType.CardTargetTypeCard)
      case t: PlayerTeamIsTarget =>
        builder.setType(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardTarget.CardTargetType.CardTargetTypePlayerTeam )
        builder.addAllTeamPlayers(t.team.asJava)
      case t: PlayerIsTarget =>
        builder.setType(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardTarget.CardTargetType.CardTargetTypePlayer)
        builder.setUserName(t.playerName)
    }
    builder.build()
  }

  def apply(target: CardTarget, tableState: TableState) = {
    val builder = cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardTarget.newBuilder()
    target match {
      case t: CardRefIsTarget =>
        builder.setCardPosition(CardPositionToProto(tableState.positionForAlias(t.position)))
        builder.setType(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardTarget.CardTargetType.CardTargetTypeCard)
      case t: CardIsTarget =>
        builder.setCardPosition(CardPositionToProto(t.position))
        builder.setType(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardTarget.CardTargetType.CardTargetTypeCard)
      case t: PlayerTeamIsTarget =>
        builder.setType(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardTarget.CardTargetType.CardTargetTypePlayerTeam)
        builder.addAllTeamPlayers(t.team.asJava)
      case t: PlayerIsTarget =>
        builder.setType(cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardTarget.CardTargetType.CardTargetTypePlayer)
        builder.setUserName(t.playerName)

    }
    builder.build()
  }
}
