package cz.noveit.games.cardgame.engine.handlers.helpers.serialization

import cz.noveit.games.cardgame.engine.player.CharmOfElements
import cz.noveit.proto.firstenchanter.FirstEnchanterMatch.TableStateForPlayer.PlayerElementsCharm

/**
 * Created by arnostkuchar on 19.09.14.
 */
object CharmOfElementsToProto {
  def apply(charmOfElements: CharmOfElements): PlayerElementsCharm = {
      cz.noveit.proto.firstenchanter.FirstEnchanterMatch.TableStateForPlayer.PlayerElementsCharm
        .newBuilder
        .setFire(charmOfElements.fire)
        .setAir(charmOfElements.air)
        .setEarth(charmOfElements.earth)
        .setWater(charmOfElements.water)
        .build()
  }
}
