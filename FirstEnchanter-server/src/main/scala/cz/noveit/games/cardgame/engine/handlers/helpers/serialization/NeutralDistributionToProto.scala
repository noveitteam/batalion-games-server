package cz.noveit.games.cardgame.engine.handlers.helpers.serialization

import cz.noveit.games.cardgame.engine.player.NeutralDistribution

/**
 * Created by arnostkuchar on 22.09.14.
 */
object NeutralDistributionToProto {
  def apply(distribution: NeutralDistribution): cz.noveit.proto.firstenchanter.FirstEnchanterMatch.NeutralDistribution = {
    cz.noveit.proto.firstenchanter.FirstEnchanterMatch.NeutralDistribution
    .newBuilder()
    .setFire(distribution.fire)
    .setWater(distribution.water)
    .setEarth(distribution.earth)
    .setAir(distribution.air)
    .build()
  }
}
