package cz.noveit.games.cardgame.engine.handlers.helpers.serialization

import cz.noveit.games.cardgame.engine.card.OnGraveyardPosition
import cz.noveit.games.cardgame.engine.game.TableState
import cz.noveit.games.cardgame.engine.game.session.PLAYER_ROLL_POSITION_ATTACKER
import cz.noveit.games.cardgame.engine.player.NeutralDistribution
import cz.noveit.proto.firstenchanter.FirstEnchanterMatch.CardPosition.CardPositionPlace
import cz.noveit.proto.firstenchanter.FirstEnchanterMatch._
import cz.noveit.proto.firstenchanter.FirstEnchanterMatch.TableStateForPlayer.{CountableCardWithSkinOnPosition, PlayerPlaysTable}
import scala.collection.JavaConverters._

/**
 * Created by arnostkuchar on 19.09.14.
 */
object TableStateToProto {
  def apply(ts: TableState, alliesInContext: List[String]): TableStateForPlayer = {
    val builder = TableStateForPlayer.newBuilder()
    builder.setRoundNumber(ts.roundNumber)
    builder.addAllPlayersTurn(ts.playersTurn.asJava)
    builder.setFortuneOnAttackerSide(ts.fortuneIsOnSide == PLAYER_ROLL_POSITION_ATTACKER)
    builder.setMyTeamHp(ts.teamHp.find(hp => hp.members.contains(alliesInContext.head)).map(f => f.hp).getOrElse(-1))
    builder.setEnemyTeamHp(ts.teamHp.find(hp => !hp.members.contains(alliesInContext.head)).map(f => f.hp).getOrElse(-1))

    builder.addAllUsedCards(ts.usedCards.map(uc => {
     val b = AttackerUsesCard.newBuilder()
     b.setPosition(CardPositionToProto(ts.positionForAlias(uc.usage.onPosition)))
      b.setId(uc.usage.cardHolder.id)
      uc.usage.target.map(t => b.setTarget(CardTargetToProto(t, ts)))
      b.setNeutralDistribution(NeutralDistributionToProto(uc.usage.neutralDistribution))
     b.build()
    }).asJava)


    builder.addAllCounterUsedCards(ts.counterUsedCards.map(uc => {
      val b = DefenderUsesCard.newBuilder()
      b.setPosition(CardPositionToProto(ts.positionForAlias(uc.usage.onPosition)))
      b.setId(uc.usage.cardHolder.id)
      uc.usage.target.map(t => b.setTarget(CardTargetToProto(t, ts)))
      b.setNeutralDistribution(NeutralDistributionToProto(uc.usage.neutralDistribution))
      b.build()
    }).asJava)

    builder.addAllAttackedCards(ts.attackedCards.map(ac => {
      val b = AttackerAttacks.newBuilder()
      b.setId(ac.attack.card.id)
      b.setPosition(CardPositionToProto(ts.positionForAlias(ac.attack.onPosition)))
      b.setNeutralDistribution(NeutralDistributionToProto(NeutralDistribution(0,0,0,0)))
      b.build()
    }).asJava)

    builder.addAllDefendedCards(ts.defendedCards.map(dc => {
      val b = DefenderDefends.newBuilder()
      b.setId(dc.defend.card.id)
      b.setPosition(CardPositionToProto(ts.positionForAlias(dc.defend.defendWith)))
      b.setTarget(CardTargetToProto(dc.defend.against, ts))
      b.setNeutralDistribution(NeutralDistributionToProto(dc.defend.neutralDistribution))
      b.build()
    }).asJava)

    builder.addAllPlayers(ts.players.map(p => {
      val playerPlaysTable = PlayerPlaysTable.newBuilder
      playerPlaysTable.setPlayer(p.nickname)
      playerPlaysTable.setElements(CharmOfElementsToProto(p.charm))
      playerPlaysTable.setVisibleDecksSize(p.deck.size)
      playerPlaysTable.setVisibleGraveyardSize(p.graveyard.size)
      playerPlaysTable.setVisibleHandsSize(p.hands.size)
      playerPlaysTable.setVisibleTableSize(p.onTable.size)
      if (alliesInContext.contains(p.nickname)) {
        playerPlaysTable
          .addAllCards(
          (
            p.graveyard.zipWithIndex.map(zipped => {
              CountableCardWithSkinOnPosition
                .newBuilder()
                .setCard(CardHolderToProto(zipped._1))
                .setPosition(
                  CardPosition
                    .newBuilder()
                    .setPlace(CardPositionPlace.ON_GRAVEYARD)
                    .setPlaceId(zipped._2)
                    .setPlayer(p.nickname)
                    .build()
                )
                .build()
            }) ++ p.onTable.zipWithIndex.map(zipped => {
              CountableCardWithSkinOnPosition
                .newBuilder()
                .setCard(CardHolderToProto(zipped._1))
                .setPosition(
                  CardPosition
                    .newBuilder()
                    .setPlace(CardPositionPlace.ON_TABLE)
                    .setPlaceId(zipped._2)
                    .setPlayer(p.nickname)
                    .build()
                )
                .build()
            }) ++ p.hands.zipWithIndex.map(zipped => {
              CountableCardWithSkinOnPosition
                .newBuilder()
                .setCard(CardHolderToProto(zipped._1))
                .setPosition(
                  CardPosition
                    .newBuilder()
                    .setPlace(CardPositionPlace.IN_HAND)
                    .setPlaceId(zipped._2)
                    .setPlayer(p.nickname)
                    .build()
                )
                .build()
            })
            ).toList.asJava
        )
      } else {
        playerPlaysTable
          .addAllCards(
            (
              p.graveyard.zipWithIndex.map(zipped => {
                CountableCardWithSkinOnPosition
                  .newBuilder()
                  .setCard(CardHolderToProto(zipped._1))
                  .setPosition(
                    CardPosition
                      .newBuilder()
                      .setPlace(CardPositionPlace.ON_GRAVEYARD)
                      .setPlaceId(zipped._2)
                      .setPlayer(p.nickname)
                      .build()
                  )
                  .build()
              }) ++ p.onTable.zipWithIndex.map(zipped => {
                CountableCardWithSkinOnPosition
                  .newBuilder()
                  .setCard(CardHolderToProto(zipped._1))
                  .setPosition(
                    CardPosition
                      .newBuilder()
                      .setPlace(CardPositionPlace.ON_TABLE)
                      .setPlaceId(zipped._2)
                      .setPlayer(p.nickname)
                      .build()
                  )
                  .build()
              })
              ).toList.asJava
          )
      }

      playerPlaysTable.build()
    }).asJava)

    builder.build()

    /*

        players: List[PlayerPlaysMatch],
                       playersTurn: List[String],
                       activeArenaId: String,
                       fortuneIsOnSide: PlayerRollPosition,
                       startingRollPositions: List[StartingRollPosition],
                       teamHp: List[TeamHp],
                       usedCards: List[UseCard],
                       counterUsedCards: List[CounterUseCard],
                       attackedCards: List[AttackWithCard],
                       defendedCards: List[DefendWithCard],
                       positionAliases: HashMap[CardPositionRef, CardPosition],
                       roundNumber: Int,
                       limits: TableLimits
     */


  }
}
