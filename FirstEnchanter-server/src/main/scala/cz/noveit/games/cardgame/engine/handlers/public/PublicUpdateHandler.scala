package cz.noveit.games.cardgame.engine.handlers.public

import scala.collection.immutable.HashMap
import akka.actor._
import cz.noveit.proto.serialization.{MessageSerializer, MessageEventContext, MessageEvent}
import cz.noveit.database.{DatabaseCommand, DatabaseResult}
import cz.noveit.games.cardgame.adapters.{AvatarVersionsReturned, GetAvatarVersions, PublicUpdateDatabaseAdapter}
import cz.noveit.games.cardgame.engine.resources.{AvatarFromFile, GetAvatarFromFile}
import org.bouncycastle.util.encoders.Base64
import cz.noveit.connector.websockets.WebSocketsContext
import cz.noveit.games.cardgame.engine.resources.AvatarFromFile
import cz.noveit.proto.serialization.MessageEvent
import cz.noveit.games.cardgame.adapters.AvatarVersionsReturned
import cz.noveit.games.cardgame.adapters.GetAvatarVersions
import cz.noveit.games.cardgame.engine.resources.GetAvatarFromFile
import cz.noveit.database.DatabaseResult
import cz.noveit.database.DatabaseCommand


/**
 * Created by Wlsek on 21.3.14.
 */
class PublicUpdateHandler (val module: String, databaseController: ActorSelection, avatarLoader: ActorRef, val parameters: HashMap[String, String]) extends Actor with ActorLogging{

  lazy val databaseAdapter = context.actorOf(
    Props(classOf[PublicUpdateDatabaseAdapter],
      databaseController,
      parameters("databaseName"),
      parameters("avatarVersionCollection")
    ))

    def receive = {
      case e: MessageEvent =>
        e.message match {
          case msg: cz.noveit.proto.firstenchanter.FirstEnchanterPublicUpdate.GetAvatarVersions =>
            databaseAdapter.tell(DatabaseCommand(GetAvatarVersions(msg.getDevice.getNumber), e.replyHash), context.actorOf(Props(classOf[PublicUpdateResponder], module, e.context.get, e.replyHash, self)))
            //context.stop(self)

          case msg: cz.noveit.proto.firstenchanter.FirstEnchanterPublicUpdate.UpdateAvatar =>
            val newSender = context.actorOf(Props(classOf[PublicUpdateResponder], module, e.context.get, e.replyHash, self))
            avatarLoader.tell(GetAvatarFromFile(msg.getDevice.getNumber, msg.getId), newSender)
           // context.stop(self)
        }
    }
}

class PublicUpdateResponder(val module: String, msgContext: MessageEventContext, replyHash: String, parent: ActorRef) extends Actor with ActorLogging with MessageSerializer {
  def receive = {
    case r: DatabaseResult =>
      log.info("Message received")
      r.result match {
        case AvatarVersionsReturned(versions) =>
           val avBuilder =  cz.noveit.proto.firstenchanter.FirstEnchanterPublicUpdate.AvatarVersions.newBuilder
           versions.map(v => {
             avBuilder.addAvatars(
               cz.noveit.proto.firstenchanter.FirstEnchanterPublicUpdate.AvatarVersions.AvatarVersion.newBuilder.setId(v.id).setVersion(v.version).build()
             )
           })

          val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(
            avBuilder.build,
            module, replyHash
          )).toByteArray), "UTF-8")
          msgContext.asInstanceOf[WebSocketsContext].send(encodedString)
      }
      context.stop(parent)
      context.stop(self)

    case msg: AvatarFromFile =>
      val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(
        cz.noveit.proto.firstenchanter.FirstEnchanterPublicUpdate.UpdatedAvatar.newBuilder
          .setBase64Image(msg.base64image)
          .setId(msg.id)
          .setVersion(msg.version)
          .build(),
        module, replyHash
      )).toByteArray), "UTF-8")
      msgContext.asInstanceOf[WebSocketsContext].send(encodedString)
      context.stop(parent)
      context.stop(self)
  }
}

