package cz.noveit.games.cardgame.engine.player

import cz.noveit.games.cardgame.adapters.ElementsRating

/**
 * Created by Wlsek on 6.2.14.
 */

object NeutralDistribution {
  val ZERO = NeutralDistribution(0, 0, 0, 0)
}

case class NeutralDistribution(fire: Int, water: Int, earth: Int, air: Int) {

  def =?(nd: NeutralDistribution) = {
    this.fire == nd.fire && this.water == nd.water && this.earth == nd.earth && this.air == nd.air
  }

  def total = fire + water + earth + air

  def toList = {
    List('fire -> fire, 'water -> water, 'air -> air, 'earth -> earth)
  }

  def fromList(list: List[Pair[Symbol, Int]]): NeutralDistribution = NeutralDistribution(
    list.find(i => i._1 == 'fire).map(f => f._2).getOrElse(0),
    list.find(i => i._1 == 'water).map(f => f._2).getOrElse(0),
    list.find(i => i._1 == 'earth).map(f => f._2).getOrElse(0),
    list.find(i => i._1 == 'air).map(f => f._2).getOrElse(0)
  )

}

case class CharmOfElements(fire: Int, water: Int, earth: Int, air: Int) {
  def -/(cost: ElementsRating, neutralDistribution: NeutralDistribution): CharmOfElements = {
    CharmOfElements(
      this.fire - cost.fire.toInt - neutralDistribution.fire,
      this.water - cost.water.toInt - neutralDistribution.water,
      this.earth - cost.earth.toInt - neutralDistribution.earth,
      this.air - cost.air.toInt - neutralDistribution.air
    )
  }

  def +/(cost: ElementsRating, neutralDistribution: NeutralDistribution): CharmOfElements = {
    CharmOfElements(
      this.fire + cost.fire.toInt + neutralDistribution.fire,
      this.water + cost.water.toInt + neutralDistribution.water,
      this.earth + cost.earth.toInt + neutralDistribution.earth,
      this.air + cost.air.toInt + neutralDistribution.air
    )
  }

  def +(charm: CharmOfElements) = CharmOfElements(fire + charm.fire, water + charm.water, earth + charm.earth, air + charm.air)

  def -(charm: CharmOfElements) = CharmOfElements(fire - charm.fire, water - charm.water, earth - charm.earth, air - charm.air)

  def affordAble_? = {
    this.fire >= 0 && this.air >= 0 && this.water >= 0 && this.earth >= 0
  }

  def affordAble_?(cost: ElementsRating): Boolean = {
    if (cost.neutral > 0) {
      val charmForNeutral = this -/(cost, NeutralDistribution(0, 0, 0, 0))
      var neutralValue = cost.neutral
      var toReturn = true

      if (charmForNeutral.affordAble_?) {
        var matrix = List(
          CharmOfElements(charmForNeutral.fire, 0, 0, 0),
          CharmOfElements(0, charmForNeutral.water, 0, 0),
          CharmOfElements(0, 0, charmForNeutral.earth, 0),
          CharmOfElements(0, 0, 0, charmForNeutral.air)
        )

        while (neutralValue > 0) {
          matrix = matrix.sortWith((first, second) => {
            first.fire > second.fire ||
              first.fire > second.water ||
              first.fire > second.air ||
              first.fire > second.earth ||
              first.air > second.fire ||
              first.air > second.water ||
              first.air > second.air ||
              first.air > second.earth ||
              first.water > second.fire ||
              first.water > second.water ||
              first.water > second.air ||
              first.water > second.earth ||
              first.fire > second.fire ||
              first.fire > second.water ||
              first.fire > second.air ||
              first.earth > second.earth
          })

          if (
            (this -/(ElementsRating(1, 0, 0, 0, 0), NeutralDistribution(0, 0, 0, 0))).affordAble_?
          ) {
            matrix = CharmOfElements(matrix.head.fire - 1, 0, 0, 0) :: matrix.tail
          } else if (
            (this -/(ElementsRating(0, 1, 0, 0, 0), NeutralDistribution(0, 0, 0, 0))).affordAble_?
          ) {
            matrix = CharmOfElements(0, matrix.head.water - 1, 0, 0) :: matrix.tail
          } else if (
            (this -/(ElementsRating(0, 0, 1, 0, 0), NeutralDistribution(0, 0, 0, 0))).affordAble_?
          ) {
            matrix = CharmOfElements(0, 0, matrix.head.earth - 1, 0) :: matrix.tail
          } else if (
            (this -/(ElementsRating(0, 0, 0, 1, 0), NeutralDistribution(0, 0, 0, 0))).affordAble_?
          ) {
            matrix = CharmOfElements(0, 0, 0, matrix.head.air - 1) :: matrix.tail
          } else {
            toReturn = false
            neutralValue = 0
          }

          neutralValue = neutralValue - 1
        }

        toReturn
      } else {
        false
      }
    } else {
      (this -/(cost, NeutralDistribution(0, 0, 0, 0))).affordAble_?
    }
  }

  def invertByLimit(limit: Int): CharmOfElements = {
    CharmOfElements(
      fire - limit,
      water - limit,
      earth - limit,
      air - limit
    ).nullValuesBellowZero
  }

  def nullValuesBellowZero: CharmOfElements = {
    CharmOfElements(
      if (fire < 0) {
        fire + (fire * -1)
      } else {
        fire
      },
      if (water < 0) {
        water + (water * -1)
      } else {
        water
      },
      if (earth < 0) {
        earth + (earth * -1)
      } else {
        earth
      },
      if (air < 0) {
        air + (air * -1)
      } else {
        air
      }
    )
  }

  def cutBy(value: Int): CharmOfElements = {
    CharmOfElements(
      if (fire > value) {
        value
      } else {
        fire
      },
      if (water > value) {
        value
      } else {
        water
      },
      if (earth > value) {
        value
      } else {
        earth
      },
      if (air > value) {
        value
      } else {
        air
      }
    )
  }

  def total: Int = {
    earth + water + fire + air
  }

  def toList = {
    List('fire -> fire, 'water -> water, 'air -> air, 'earth -> earth)
  }

  def fromList(list: List[Pair[Symbol, Int]]): CharmOfElements = CharmOfElements(
    list.find(i => i._1 == 'fire).map(f => f._2).getOrElse(0),
    list.find(i => i._1 == 'water).map(f => f._2).getOrElse(0),
    list.find(i => i._1 == 'earth).map(f => f._2).getOrElse(0),
    list.find(i => i._1 == 'air).map(f => f._2).getOrElse(0)
  )

  def flattenToElementalCost = {
    (for (f <- (1 to this.water).toList) yield ElementsRating(0, 0, 0, 0, 0)) :::
      (for (f <- (1 to this.fire).toList) yield ElementsRating(0, 1, 0, 0, 0)) :::
      (for (f <- (1 to this.earth).toList) yield ElementsRating(0, 0, 1, 0, 0)) :::
      (for (f <- (1 to this.air).toList) yield ElementsRating(0, 0, 0, 1, 0))
  }

  def size = {
    this.fire + this.water + this.air + this.earth
  }

}