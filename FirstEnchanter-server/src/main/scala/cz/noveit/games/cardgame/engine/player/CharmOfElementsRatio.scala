package cz.noveit.games.cardgame.engine.player

/**
 * Created by Wlsek on 6.2.14.
 */
case class CharmOfElementsRatio (fire: Float, water: Float, earth: Float, air: Float)