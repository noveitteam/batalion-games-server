package cz.noveit.games.cardgame.engine.player

import akka.actor.ActorRef
import cz.noveit.games.cardgame.adapters.Card
import cz.noveit.games.cardgame.engine.card.{CardHolder}

/**
 * Created by Wlsek on 6.2.14.
 */


case class Player(nickname: String, email: String, wallet: Wallet, rating: Int, score: Int, division: Int)

case class PlayerIsTryingToFindGame(player: String,teamToken: String, avatarId: Int, rating: Int, score: Int, division: Int, replyTo: ActorRef, bot: Boolean = false)

case class PlayerPlaysWithActiveDeck(player: String, deckId: String)


case class PlayerPlaysMatch(
                             nickname: String,
                             val deck: Array[CardHolder],
                             val hands: Array[CardHolder],
                             val graveyard: Array[CardHolder],
                             val onTable: Array[CardHolder],
                             charmRatio: CharmOfElementsRatio,
                             charm: CharmOfElements,
                             handler: ActorRef,
                             connected: Boolean
                             )
