package cz.noveit.games.cardgame.engine.player

/**
 * Created by Wlsek on 7.2.14.
 */

case class Team(name: String)
case class TeamComposing(players: List[PlayerIsTryingToFindGame])
case class TeamPlayingGame(players: List[String])
