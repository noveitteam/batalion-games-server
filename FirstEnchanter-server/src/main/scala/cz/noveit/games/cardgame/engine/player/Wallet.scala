package cz.noveit.games.cardgame.engine.player

/**
 * Created by Wlsek on 6.2.14.
 */
case class Wallet (inGameMoney: Long, microMoney: Long)