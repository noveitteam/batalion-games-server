package cz.noveit.games.cardgame.engine.resources

import akka.actor.{ActorRef, Props, Actor, ActorLogging}
import cz.noveit.games.cardgame.adapters._
import scala.collection.immutable.HashMap
import java.nio.file._
import cz.noveit.games.cardgame.adapters.AvatarVersion
import cz.noveit.games.cardgame.adapters.UpdateVersionForAvatar
import cz.noveit.database.DatabaseCommand
import cz.noveit.games.cardgame.adapters.CreateVersionForAvatar
import java.io.{FileInputStream, ByteArrayOutputStream, File}
import javax.imageio.ImageIO
import org.bouncycastle.util.encoders.Base64
import org.bouncycastle.crypto.digests.MD5Digest
import org.apache.commons.io.{FilenameUtils, IOUtils}

/**
 * Created by Wlsek on 21.3.14.
 */

case class GetAvatarFromFile(device: Int, id: Int)
case class AvatarFromFile(id: Int, base64image: String, version: String)

class AvatarLoader(val parameters: HashMap[String, String]) extends Actor with ActorLogging {
  log.info("Starting avatar load and watch controller")
  val databaseAdapter = context.actorOf(
    Props(classOf[PublicUpdateDatabaseAdapter],
      context.actorSelection("/user/databaseController"),
      parameters("databaseName"),
      parameters("avatarVersionCollection")
    ))
      /*
  val watcher = context.actorOf(Props(classOf[ResourceFolderWatcher], self,List(
    WatchAbleDir("./resources/avatars/ipad/standard", 2, false),
    WatchAbleDir("./resources/avatars/ipad/unlockable", 2, true),
    WatchAbleDir("./resources/avatars/ipad_retina/standard", 3, false),
    WatchAbleDir("./resources/avatars/ipad_retina/unlockable", 3, true),
    WatchAbleDir("./resources/avatars/iphone/standard", 0, false),
    WatchAbleDir("./resources/avatars/iphone/unlockable", 0, true),
    WatchAbleDir("./resources/avatars/iphone_retina/standard", 1, false),
    WatchAbleDir("./resources/avatars/iphone_retina/unlockable", 1, true)
  ) ))
  watcher ! StartWatch
                */
  def receive = {
    case msg: GetAvatarFromFile =>
      context.actorOf(Props(classOf[ImageLoad], sender, "./resources/avatars/")) ! GetImageFromFile(msg.device, msg.id, "")

    case msg: ImageFromFile =>
      sender ! AvatarFromFile(msg.id, msg.base64image, msg.version)

    case FileChanged(id, hash, param) =>
      databaseAdapter ! DatabaseCommand(UpdateVersionForAvatar(AvatarVersion(id, hash, param.device, param.unlockAble)), "")

    case FileCreated(id, hash, param) =>
      databaseAdapter ! DatabaseCommand(CreateVersionForAvatar(AvatarVersion(id, hash, param.device, param.unlockAble)), "")

    case FileDeleted(id, param) =>
      databaseAdapter ! DatabaseCommand(RemoveVersionForAvatar(id, param.device), "")
  }
}


