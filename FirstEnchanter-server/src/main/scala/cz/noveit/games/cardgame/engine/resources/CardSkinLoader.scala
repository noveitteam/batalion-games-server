package cz.noveit.games.cardgame.engine.resources

import scala.collection.immutable.HashMap
import akka.actor.{ActorRef, Props, ActorLogging, Actor}
import cz.noveit.games.cardgame.adapters._
import cz.noveit.games.cardgame.adapters.RemoveCardSkinVersion
import cz.noveit.games.cardgame.adapters.UpdateCardSkinVersion
import cz.noveit.database.DatabaseCommand
import cz.noveit.games.cardgame.adapters.CreateCardSkinVersion

/**
 * Created by Wlsek on 4.4.14.
 */

case class GetCardSkinFromFile(device: Int, id: Int, replyHash: String)
case class CardSkinFromFile(id: Int, base64image: String, version: String, replyHash: String)

class CardSkinLoader(val parameters: HashMap[String, String]) extends Actor with ActorLogging {
  log.info("Starting card skin load and watch controller")

  val databaseAdapter = context.actorOf(
    Props(classOf[CardSkinVersionAdapter],
      context.actorSelection("/user/databaseController"),
      parameters("databaseName"),
      parameters("cardSkinVersionCollection")
    ))
         /*
  val watcher = context.actorOf(Props(classOf[ResourceFolderWatcher], self, List(
    WatchAbleDir("./resources/card_skins/ipad/standard", 2, false),
    WatchAbleDir("./resources/card_skins/ipad/unlockable", 2, true),
    WatchAbleDir("./resources/card_skins/ipad_retina/standard", 3, false),
    WatchAbleDir("./resources/card_skins/ipad_retina/unlockable", 3, true),
    WatchAbleDir("./resources/card_skins/iphone/standard", 0, false),
    WatchAbleDir("./resources/card_skins/iphone/unlockable", 0, true),
    WatchAbleDir("./resources/card_skins/iphone_retina/standard", 1, false),
    WatchAbleDir("./resources/card_skins/iphone_retina/unlockable", 1, true)
  )))

  watcher ! StartWatch
       */
  def receive = {
    case msg: GetCardSkinFromFile =>
      context.actorOf(Props(classOf[ImageLoad], sender, "./resources/card_skins/")) ! GetImageFromFile(msg.device, msg.id, msg.replyHash)

    case msg: ImageFromFile =>
      sender ! CardSkinFromFile(msg.id, msg.base64image, msg.version, msg.replyHash)

    case FileChanged(id, hash, param) =>
      databaseAdapter ! DatabaseCommand(UpdateCardSkinVersion(CardSkinVersion(id, hash, param.device, param.unlockAble)), "")

    case FileCreated(id, hash, param) =>
      databaseAdapter ! DatabaseCommand(CreateCardSkinVersion(CardSkinVersion(id, hash, param.device, param.unlockAble)), "")

    case FileDeleted(id, param) =>
      databaseAdapter ! DatabaseCommand(RemoveCardSkinVersion(id, param.device), "")
  }

}