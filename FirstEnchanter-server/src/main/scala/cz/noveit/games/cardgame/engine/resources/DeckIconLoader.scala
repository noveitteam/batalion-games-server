package cz.noveit.games.cardgame.engine.resources

/**
 * Created by Wlsek on 4.4.14.
 */
import scala.collection.immutable.HashMap
import akka.actor.{ActorRef, Props, ActorLogging, Actor}
import cz.noveit.games.cardgame.adapters._
import cz.noveit.database.DatabaseCommand

case class GetDeckIconFromFile(device: Int, id: Int, replyHash: String)
case class DeckIconFromFile(id: Int, base64image: String, version: String, replyHash: String)

class DeckIconLoader(val parameters: HashMap[String, String]) extends Actor with ActorLogging {
  log.info("Starting card skin load and watch controller")
  val databaseAdapter = context.actorOf(
    Props(classOf[CardSkinVersionAdapter],
      context.actorSelection("/user/databaseController"),
      parameters("databaseName"),
      parameters("deckIconVersionCollection")
    ))
   /*
  val watcher = context.actorOf(Props(classOf[ResourceFolderWatcher], self, List(
    WatchAbleDir("./resources/deck_icons/ipad/standard", 2, false),
    WatchAbleDir("./resources/deck_icons/ipad/unlockable", 2, true),
    WatchAbleDir("./resources/deck_icons/ipad_retina/standard", 3, false),
    WatchAbleDir("./resources/deck_icons/ipad_retina/unlockable", 3, true),
    WatchAbleDir("./resources/deck_icons/iphone/standard", 0, false),
    WatchAbleDir("./resources/deck_icons/iphone/unlockable", 0, true),
    WatchAbleDir("./resources/deck_icons/iphone_retina/standard", 1, false),
    WatchAbleDir("./resources/deck_icons/iphone_retina/unlockable", 1, true)
  )))

  watcher ! StartWatch
      */
  def receive = {
    case msg: GetDeckIconFromFile =>
      context.actorOf(Props(classOf[ImageLoad], sender, "./resources/deck_icons/")) ! GetImageFromFile(msg.device, msg.id, msg.replyHash)

    case msg: ImageFromFile =>
      sender ! DeckIconFromFile(msg.id, msg.base64image, msg.version, msg.replyHash)

    case FileChanged(id, hash, param) =>
      databaseAdapter ! DatabaseCommand(UpdateDeckIconVersion(DeckIconVersion(id, hash, param.device, param.unlockAble)), "")

    case FileCreated(id, hash, param) =>
      databaseAdapter ! DatabaseCommand(CreateDeckIconVersion(DeckIconVersion(id, hash, param.device, param.unlockAble)), "")

    case FileDeleted(id, param) =>
      databaseAdapter ! DatabaseCommand(RemoveDeckIconVersion(id, param.device), "")
  }

}
