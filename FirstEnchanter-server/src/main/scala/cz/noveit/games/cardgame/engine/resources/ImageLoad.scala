package cz.noveit.games.cardgame.engine.resources

import akka.actor.{ActorLogging, Actor, ActorRef}
import java.io.{FileInputStream, ByteArrayOutputStream, File}
import javax.imageio.ImageIO
import org.apache.commons.io.IOUtils
import org.bouncycastle.crypto.digests.MD5Digest
import org.bouncycastle.util.encoders.Base64

/**
 * Created by Wlsek on 4.4.14.
 */

case class GetImageFromFile(device: Int, id: Int, replyHash: String)
case class ImageFromFile(id: Int, base64image: String, version: String, replyHash: String)

class ImageLoad(replyTo: ActorRef, basePath: String) extends Actor with ActorLogging {
  def receive = {
    case GetImageFromFile(device, id, replyHash) =>
      try {
        var path = basePath
        device match {
          case 2 => path = path + "ipad"
          case 3 => path = path + "ipad_retina"
          case 0 => path = path + "iphone"
          case 1 => path = path + "iphone_retina"
        }

        var f = new File(path + "/standard/" + id + ".png");
        if (!f.exists() || f.isDirectory()) {
          f = new File(path + "/unlockable/" + id + ".png");
        }

        val bos = new ByteArrayOutputStream()
        val image = ImageIO.read(f);
        ImageIO.write(image, "png", bos)

        val fis = new FileInputStream(f);
        val bytes = IOUtils.toByteArray(fis)

        val digest = new MD5Digest()
        digest.update(bytes, 0, bytes.length)
        val md5 = new Array[Byte](digest.getDigestSize())
        digest.doFinal(md5, 0)
        fis.close()

        sender.tell(ImageFromFile(id, new String(Base64.encode(bos.toByteArray), "UTF-8"), new String(Base64.encode(md5), "UTF-8"), replyHash), replyTo)
      } catch {
        case t: Throwable => log.error("Error while getting image: " + t.getMessage)
      }
  }
}

