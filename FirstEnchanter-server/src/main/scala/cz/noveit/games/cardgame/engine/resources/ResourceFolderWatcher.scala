package cz.noveit.games.cardgame.engine.resources

import org.apache.commons.io.{FilenameUtils, IOUtils}
import java.nio.file._
import akka.actor.{Props, ActorRef, Actor, ActorLogging}
import java.util.concurrent.TimeUnit

import scala.collection.JavaConverters._
import java.io.FileInputStream
import org.bouncycastle.util.encoders.Base64
import org.bouncycastle.crypto.digests.MD5Digest
import scala.collection.immutable.HashMap
import cz.noveit.games.cardgame.engine.resources.FileChanged
import cz.noveit.games.cardgame.engine.resources.WatchAbleDir
import cz.noveit.games.cardgame.engine.resources.FileDeleted

/**
 * Created by Wlsek on 4.4.14.
 */


case object StartWatch
case class WatchAbleDir(dir: String, device: Int, unlockAble: Boolean)

class ResourceFolderWatcher(replyTo: ActorRef, watchAbleDirs: List[WatchAbleDir]) extends Actor with ActorLogging {
  log.info("Starting watcher")
  val fileSystem = FileSystems.getDefault()
  var keysForWatcher: HashMap[ActorRef, WatchKey] = HashMap()

  def receive = {
    case StartWatch =>
      watchAbleDirs.foreach(wa => {
        log.info("Starting to watch folder {}", wa.dir)
        context.actorOf(Props(classOf[SubFolderWatcher], wa, replyTo, self))
      })
  }
}


case object CheckEvents

case class FileCreated(id: Int, hash: String, dir: WatchAbleDir)

case class FileChanged(id: Int, hash: String, dir: WatchAbleDir)

case class FileDeleted(id: Int, dir: WatchAbleDir)

class SubFolderWatcher(param: WatchAbleDir, replyTo: ActorRef, parent: ActorRef) extends Actor with ActorLogging {
  val dir = Paths.get(param.dir)
  val watcher = dir.getFileSystem().newWatchService()
  dir.register(watcher, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_MODIFY)
  self ! CheckEvents

  def receive = {
    case CheckEvents =>
      log.info("Checking events")
      check
  }

  def check = {
    try {
      val key = watcher.poll(5000, TimeUnit.MILLISECONDS)
      if (key != null) {
        key.pollEvents().asScala.map(event => {
          event.kind match {
            case StandardWatchEventKinds.ENTRY_CREATE =>
              log.info("{} was created", event.context())
              val id = FilenameUtils.removeExtension(event.context.asInstanceOf[Path].toString()).toInt
              val fis = new FileInputStream(param.dir  + "/" + event.context.asInstanceOf[Path].toString)
              val bytes = IOUtils.toByteArray(fis)

              val digest = new MD5Digest()
              digest.update(bytes, 0, bytes.length)
              val md5 = new Array[Byte](digest.getDigestSize())
              digest.doFinal(md5, 0)
              fis.close()
              replyTo ! FileChanged(id, new String(Base64.encode(md5), "UTF-8"), param)

            case StandardWatchEventKinds.ENTRY_DELETE =>
              log.info("{} was deleted", event.context())
              val id = FilenameUtils.removeExtension(event.context.asInstanceOf[Path].toString()).toInt
              replyTo ! FileDeleted(id, param)

            case StandardWatchEventKinds.ENTRY_MODIFY =>
              log.info("{} was edited", event.context())
              val id =  FilenameUtils.removeExtension(event.context.asInstanceOf[Path].toString()).toInt
              val fis = new FileInputStream(param.dir + "/" + event.context.asInstanceOf[Path].toString)
              val bytes = IOUtils.toByteArray(fis)

              val digest = new MD5Digest()
              digest.update(bytes, 0, bytes.length)
              val md5 = new Array[Byte](digest.getDigestSize())
              digest.doFinal(md5, 0)
              fis.close()
              replyTo ! FileChanged(id, new String(Base64.encode(md5), "UTF-8"), param)

          }
        })
        if (key.reset) self ! CheckEvents
      } else {
        self ! CheckEvents
      }
    } catch {
      case e: Exception =>
        log.error("Error: " + e.fillInStackTrace());
        self ! CheckEvents
    }
  }
}