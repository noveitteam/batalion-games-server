package cz.noveit.games.cardgame.engine.tags

/**
 * Created by Wlsek on 6.2.14.
 */


trait Tag

trait ElementTags extends Tag
case object Water  extends  ElementTags
case object Fire   extends  ElementTags
case object Air    extends  ElementTags
case object Earth  extends  ElementTags

trait RarityTags extends Tag
case object Common extends RarityTags
case object Uncommon extends RarityTags
case object Rare extends RarityTags
case object Hero extends RarityTags

trait FightAbilityTags extends Tag
case object Melee extends FightAbilityTags
case object Ranged extends  FightAbilityTags

trait CardTypeTags extends Tag
case object Spell extends CardTypeTags
case object Equip extends CardTypeTags
case object Item extends CardTypeTags
case object Monster extends CardTypeTags
case object Reaction extends CardTypeTags

trait EquipmentTags extends Tag
case object Weapon extends EquipmentTags
case object Shield extends EquipmentTags
case object Armor extends EquipmentTags

trait AreaOfEffectTags extends Tag
case object AOE extends AreaOfEffectTags
case object Single extends AreaOfEffectTags
case object EnchanterOnly extends AreaOfEffectTags
case object MonsterOnly extends AreaOfEffectTags

trait BasicEffectTags extends Tag
case object  Damage extends BasicEffectTags
case object  Healing extends BasicEffectTags











