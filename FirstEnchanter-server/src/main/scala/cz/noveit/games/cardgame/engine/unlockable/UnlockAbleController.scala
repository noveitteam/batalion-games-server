package cz.noveit.games.cardgame.engine.unlockable

import akka.actor.{ActorRef, Props, Actor, ActorLogging}
import scala.collection.immutable.HashMap
import cz.noveit.games.cardgame.adapters._
import cz.noveit.database.{DatabaseResult, DatabaseCommand}
import cz.noveit.games.cardgame.engine.unlockable.CannotUnlockContent
import cz.noveit.games.cardgame.adapters.GetUnlockAble
import cz.noveit.games.cardgame.engine.unlockable.ContentUnlock
import cz.noveit.games.cardgame.adapters.CannotGetUnlockAble
import cz.noveit.games.cardgame.engine.unlockable.UnlockAbleRow
import cz.noveit.database.DatabaseResult
import cz.noveit.games.cardgame.engine.unlockable.IsUnlockAble
import cz.noveit.database.DatabaseCommand

/**
 * Created by Wlsek on 17.6.14.
 */


object UnlockAbleType extends Enumeration {
  val UnlockAbleTypeCardSkin, UnlockAbleTypeAvatar, UnlockAbleTypeDeckIcon, UnlockAbleTypeBooster, UnlockAbleTypeDeck = Value
}

object UnlockAbleMethod extends Enumeration {
  val UnlockAbleMethodTransaction, UnlockAbleMethodRedemptionCode = Value
}

trait UnlockedContent

case class ContentUnlock(unlockAbleId: String, unlockAbleMethod: UnlockAbleMethod.Value, user: String, parameters: HashMap[String, String] = HashMap())

case class ContentUnlocked(unlockAbleId: String, unlockedContent: UnlockedContent)

case class CannotUnlockContent(unlockAbleId: String)

case class IsUnlockAble(unlockAbles: List[String], userInContext: String)

case class UnlockAbleRow(unlockAble: String, isUnlockAble: Boolean)

case class UnlockAbleTable(unlockAbles: List[UnlockAbleRow])

case object IsUnlockAbleCheckError

class UnlockAbleController(databaseController: ActorRef, databaseParameters: HashMap[String, String], handlers: Seq[Pair[UnlockAbleType.Value, Class[_]]]) extends Actor with ActorLogging {
  def receive = {
    case msg: ContentUnlock =>
      context actorOf (Props(classOf[UnlockAbleRouter], databaseController, databaseParameters, handlers, msg, sender))

    case msg: IsUnlockAble =>
      context actorOf (Props(classOf[IsUnlockAbleHandler], databaseController, databaseParameters, msg, sender))
  }
}

class UnlockAbleRouter(databaseController: ActorRef, databaseParameters: HashMap[String, String], handlers: Seq[Pair[UnlockAbleType.Value, Class[_]]], request: ContentUnlock, replyTo: ActorRef) extends Actor with ActorLogging {
  val unlockAbleDatabaseAdapter = context.actorOf(
    Props(
      classOf[UnlockAbleDatabaseAdapter],
      context.actorSelection("/user/databaseController"),
      databaseParameters("databaseName"),
      databaseParameters("unlockAblesCollection"),
      databaseParameters("unlockAblesHistoryCollection"),
      databaseParameters("userHasCardsCollection"),
      databaseParameters("userUnlockedCollection"),
      databaseParameters("decksCollection")
    )
  )

  unlockAbleDatabaseAdapter ! DatabaseCommand(GetUnlockAble(request.unlockAbleId))

  def receive = {
    case DatabaseResult(result, h) =>
      result match {
        case CannotGetUnlockAble(id) =>
          replyTo ! CannotUnlockContent(request.unlockAbleId)
          context.stop(self)

        case u: UnlockAble =>
          handlers.find(h => h._1 == u.unlockAbleType).map(f => {
            context.actorOf(Props(f._2, request.unlockAbleId, request.parameters, u.unlockAbleType, request.unlockAbleMethod, replyTo, databaseController, databaseParameters, request.user, u, self))
          })
        // context.stop(self)
      }
  }
}


class IsUnlockAbleHandler(databaseController: ActorRef, databaseParameters: HashMap[String, String], request: IsUnlockAble, replyTo: ActorRef) extends Actor with ActorLogging {
  val unlockAbleDatabaseAdapter = context.actorOf(
    Props(
      classOf[UnlockAbleDatabaseAdapter],
      context.actorSelection("/user/databaseController"),
      databaseParameters("databaseName"),
      databaseParameters("unlockAblesCollection"),
      databaseParameters("unlockAblesHistoryCollection"),
      databaseParameters("userHasCardsCollection"),
      databaseParameters("userUnlockedCollection"),
      databaseParameters("decksCollection")
    )
  )

  unlockAbleDatabaseAdapter ! DatabaseCommand(GetUnlockAblesAlreadyInHistoryForUser(request.unlockAbles, request.userInContext))

  var unlockAblesInHistory: List[UnlockAbleAlreadyInHistory] = List()
  var unlockAbles: List[UnlockAble] = List()

  def receive = {
    case DatabaseResult(result, h) =>
      result match {
        case UnlockAblesAlreadyInHistory(unlockAbles) =>
          unlockAblesInHistory = unlockAbles
          unlockAbleDatabaseAdapter ! DatabaseCommand(GetUnlockAbles(request.unlockAbles.filter(f =>
            unlockAblesInHistory.filter(fi => fi.unlockAbleType match {
              case (
                UnlockAbleType.UnlockAbleTypeAvatar |
                UnlockAbleType.UnlockAbleTypeCardSkin |
                UnlockAbleType.UnlockAbleTypeDeck |
                UnlockAbleType.UnlockAbleTypeDeckIcon) =>
                true
              case o =>
                false
            }).find(fi => fi.unlockAbleId == f).isEmpty
          )))

        case UnlockAbles(un) =>
          unlockAbles = un
          unlockAbleDatabaseAdapter ! DatabaseCommand(GetOwnedCardsForUser(request.userInContext))

        case OwnedCardsForUser(user, cards) =>
          var unlockCheckTable: List[UnlockAbleRow] = List()
          unlockAblesInHistory.filter(f => f.unlockAbleType != UnlockAbleType.UnlockAbleTypeBooster).map(u => unlockCheckTable = UnlockAbleRow(u.unlockAbleId, false) :: unlockCheckTable)
          unlockAbles.map(u => {
            u.unlockAbleType match {
              case UnlockAbleType.UnlockAbleTypeBooster =>
                unlockCheckTable = UnlockAbleRow(u.unlockAbleId,u.asInstanceOf[UnlockAbleBooster].boosterTable.filter(f => cards.find(c => c == f.cardId).isEmpty).size > 0) :: unlockCheckTable
              case o =>
                unlockCheckTable = UnlockAbleRow(u.unlockAbleId, true) :: unlockCheckTable
            }
          })

          replyTo ! UnlockAbleTable(unlockCheckTable)


        case CannotGetOwndedCardsForUser =>
          replyTo ! IsUnlockAbleCheckError
          context.stop(self)

        case CannotGetUnlockablesInHistory =>
          replyTo ! IsUnlockAbleCheckError
          context.stop(self)

        case CannotGetUnlockAbles =>
          replyTo ! IsUnlockAbleCheckError
          context.stop(self)
      }
  }
}
