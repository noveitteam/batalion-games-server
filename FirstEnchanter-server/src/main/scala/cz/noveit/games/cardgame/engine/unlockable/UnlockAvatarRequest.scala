package cz.noveit.games.cardgame.engine.unlockable

import scala.collection.immutable.HashMap
import akka.actor._
import cz.noveit.games.cardgame.adapters._
import cz.noveit.games.cardgame.adapters.UnlockAbleAvatar
import cz.noveit.database.DatabaseResult
import cz.noveit.database.DatabaseCommand
import cz.noveit.games.cardgame.engine.unlockable.CannotUnlockContent
import cz.noveit.games.cardgame.adapters.UnlockAvatar
import cz.noveit.games.cardgame.adapters.CannotUnlockAvatar
import cz.noveit.games.cardgame.engine.unlockable.ContentUnlocked
import cz.noveit.games.cardgame.adapters.UnlockAbleAvatar
import cz.noveit.database.DatabaseResult
import cz.noveit.games.cardgame.adapters.AvatarUnlocked
import cz.noveit.database.DatabaseCommand

/**
 * Created by Wlsek on 17.6.14.
 */
class UnlockAvatarRequest(
                             id: String,
                             parameters: HashMap[String, String],
                             unlockAbleType: UnlockAbleType.Value,
                             unlockAbleMethod: UnlockAbleMethod.Value,
                             s: ActorRef,
                             databaseController: ActorRef,
                             databaseParameters: HashMap[String, String],
                             user: String,
                             u: UnlockAbleAvatar,
                             parentRouter: ActorRef
                           ) extends Actor with ActorLogging
{

  val unlockAbleDatabaseAdapter = context.actorOf(
    Props(
      classOf[UnlockAbleDatabaseAdapter],
      context.actorSelection("/user/databaseController"),
      databaseParameters("databaseName"),
      databaseParameters("unlockAblesCollection"),
      databaseParameters("unlockAblesHistoryCollection"),
      databaseParameters("userHasCardsCollection"),
      databaseParameters("userUnlockedCollection"),
      databaseParameters("decksCollection")
    )
  )

  unlockAbleDatabaseAdapter ! DatabaseCommand(UnlockAvatar(u.avatarId, user))

  def receive = {
    case DatabaseResult(result, h) => {
      result match {
        case CannotUnlockAvatar(avatarId, user) =>
          s ! CannotUnlockContent(id)
          parentRouter ! Kill
          context.stop(self)

        case m: AvatarUnlocked =>
          unlockAbleDatabaseAdapter ! DatabaseCommand(WriteUnlockAbleLog(id, unlockAbleType, unlockAbleMethod, System.currentTimeMillis() ,parameters.get("transactionId"), parameters.get("redemptionCode"), user))
          s ! ContentUnlocked(id, m)
          parentRouter ! Kill
          context.stop(self)
      }
    }
  }
}