package cz.noveit.games.cardgame.engine.unlockable

import akka.actor._
import scala.collection.immutable.HashMap
import cz.noveit.games.cardgame.adapters._
import cz.noveit.database.{DatabaseResult, DatabaseCommand}
import cz.noveit.games.cardgame.adapters.UnlockAbleCardSkin
import cz.noveit.games.cardgame.adapters.UnlockCardSkin
import cz.noveit.games.cardgame.adapters.CannotUnlockCardSkin
import cz.noveit.games.cardgame.engine.unlockable.CannotUnlockContent
import cz.noveit.games.cardgame.adapters.UnlockAbleCardSkin
import cz.noveit.games.cardgame.adapters.CardSkinUnlocked
import cz.noveit.games.cardgame.engine.unlockable.ContentUnlocked
import cz.noveit.games.cardgame.adapters.UnlockCardSkin
import cz.noveit.database.DatabaseResult
import cz.noveit.database.DatabaseCommand

/**
 * Created by Wlsek on 17.6.14.
 */
class UnlockCardSkinRequest(
                             id: String,
                             parameters: HashMap[String, String],
                             unlockAbleType: UnlockAbleType.Value,
                             unlockAbleMethod: UnlockAbleMethod.Value,
                             s: ActorRef,
                             databaseController: ActorRef,
                             databaseParameters: HashMap[String, String],
                             user: String,
                             u: UnlockAbleCardSkin,
                             parentRouter: ActorRef
                             ) extends Actor with ActorLogging
{

  val unlockAbleDatabaseAdapter = context.actorOf(
    Props(
      classOf[UnlockAbleDatabaseAdapter],
      context.actorSelection("/user/databaseController"),
      databaseParameters("databaseName"),
      databaseParameters("unlockAblesCollection"),
      databaseParameters("unlockAblesHistoryCollection"),
      databaseParameters("userHasCardsCollection"),
      databaseParameters("userUnlockedCollection"),
      databaseParameters("decksCollection")
    )
  )

  unlockAbleDatabaseAdapter ! DatabaseCommand(UnlockCardSkin(u.skinId, user))

  def receive = {
    case DatabaseResult(result, h) => {
      result match {
        case CannotUnlockCardSkin(skinId, user) =>
          s ! CannotUnlockContent(id)
          parentRouter ! Kill
          context.stop(self)

        case m: CardSkinUnlocked =>
          unlockAbleDatabaseAdapter ! DatabaseCommand(WriteUnlockAbleLog(id, unlockAbleType, unlockAbleMethod, System.currentTimeMillis() ,parameters.get("transactionId"), parameters.get("redemptionCode"), user))
          s ! ContentUnlocked(id, m)
          parentRouter ! Kill
          context.stop(self)

        case t =>
          log.error("Unknown message: " + t)
      }
    }

    case t =>
      log.error("Unknown message: " + t)
  }
}
