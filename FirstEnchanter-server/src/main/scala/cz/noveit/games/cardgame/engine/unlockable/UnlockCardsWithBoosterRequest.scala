package cz.noveit.games.cardgame.engine.unlockable

import akka.actor._
import scala.collection.immutable.HashMap
import cz.noveit.games.cardgame.adapters._
import cz.noveit.games.cardgame.adapters.GetOwnedCardsForUser
import cz.noveit.database.DatabaseResult
import cz.noveit.database.DatabaseCommand
import scala.util.Random

import scala.util.control._
import cz.noveit.games.cardgame.engine.unlockable.CannotUnlockContent
import cz.noveit.games.cardgame.adapters.CannotUnlockCards
import cz.noveit.games.cardgame.adapters.UnlockAbleBooster
import cz.noveit.games.cardgame.adapters.CardsUnlocked
import cz.noveit.games.cardgame.engine.unlockable.ContentUnlocked
import cz.noveit.games.cardgame.adapters.BoosterTableRow
import cz.noveit.database.DatabaseCommand
import cz.noveit.games.cardgame.adapters.OwnedCardsForUser
import cz.noveit.games.cardgame.adapters.GetOwnedCardsForUser
import cz.noveit.games.cardgame.adapters.UnlockCards
import cz.noveit.database.DatabaseResult


/**
 * Created by Wlsek on 17.6.14.
 */
class UnlockCardsWithBoosterRequest(
                                     id: String,
                                     parameters: HashMap[String, String],
                                     unlockAbleType: UnlockAbleType.Value,
                                     unlockAbleMethod: UnlockAbleMethod.Value,
                                     s: ActorRef,
                                     databaseController: ActorRef,
                                     databaseParameters: HashMap[String, String],
                                     user: String,
                                     val u: UnlockAbleBooster,
                                     parentRouter: ActorRef
                                     ) extends Actor with ActorLogging
{

  val unlockAbleDatabaseAdapter = context.actorOf(
    Props(
      classOf[UnlockAbleDatabaseAdapter],
      context.actorSelection("/user/databaseController"),
      databaseParameters("databaseName"),
      databaseParameters("unlockAblesCollection"),
      databaseParameters("unlockAblesHistoryCollection"),
      databaseParameters("userHasCardsCollection"),
      databaseParameters("userUnlockedCollection"),
      databaseParameters("decksCollection")
    )
  )

  unlockAbleDatabaseAdapter ! DatabaseCommand(GetOwnedCardsForUser(user))

  def receive = {
    case DatabaseResult(result, h) => {
      result match {
        case OwnedCardsForUser(user, ownedCards) =>
          try{
          var cards: List[BoosterTableRow] = List()
          val possible = u.boosterTable.filter(f => !ownedCards.contains(f.cardId))

          u.boosterLimitations.map(limit => {
            var limitedCards = possible.filter(f => f.rarity == limit.rarity)
            var counter = 0
            while(limit.min > counter) {
              val card = pickCardIdFromReducedBoosterTable(limitedCards)
              limitedCards = limitedCards.filterNot(f => f.cardId == card)
              cards = card :: cards
              counter += 1
            }
          })

          var possibleRarities = u.boosterLimitations.filter(limit => {
            cards.filter(c => c.rarity == limit.rarity).size <= limit.max
          })

          var possibleCards = u.boosterTable.filter(f => possibleRarities.find(r => r.rarity == f.rarity).isDefined && ownedCards.find(c => c == f.cardId).isEmpty && cards.find(c => c.cardId == f.cardId).isEmpty)

          while(cards.size < u.requiredSize && possibleCards.size > 0 && possibleRarities.size > 0) {
            if(possibleCards.size > 0) cards = pickCardIdFromReducedBoosterTable(possibleCards) :: cards
            possibleRarities = u.boosterLimitations.filter(limit => {
              cards.filter(c => c.rarity == limit.rarity).size <= limit.max
            })
            possibleCards = u.boosterTable.filter(f => possibleRarities.find(r => r.rarity == f.rarity).isDefined && ownedCards.find(c => c == f.cardId).isEmpty && cards.find(c => c.cardId == f.cardId).isEmpty)
           }

          unlockAbleDatabaseAdapter ! DatabaseCommand(UnlockCards(cards.map(c=>c.cardId), user))
          } catch {
            case t: Throwable =>
              s ! CannotUnlockContent(id)
              parentRouter ! Kill
              context.stop(self)
          }

        case  CannotGetOwndedCardsForUser =>
          s ! CannotUnlockContent(id)
          parentRouter ! Kill
          context.stop(self)

        case m: CannotUnlockCards =>
          s ! CannotUnlockContent(id)
          parentRouter ! Kill
          context.stop(self)

        case m: CardsUnlocked =>
          unlockAbleDatabaseAdapter ! DatabaseCommand(WriteUnlockAbleLog(id, unlockAbleType, unlockAbleMethod, System.currentTimeMillis() ,parameters.get("transactionId"), parameters.get("redemptionCode"), user))
          s ! ContentUnlocked(id, m)
          parentRouter  ! Kill
          context.stop(self)
      }
    }
  }


  def pickCardIdFromReducedBoosterTable(bt: List[BoosterTableRow]): BoosterTableRow = {
    val scale = 1 / bt.foldLeft(0.0)(_ + _.chance)
    val seed = Random.nextFloat
    var inCounter = 0.0
    var cardId = bt.head
    val loop = new Breaks;
    loop.breakable{
      for(row <- bt){
        inCounter += row.chance * scale
        if(seed < inCounter){
          cardId = row
          loop.break;
        }
      }
    }

    cardId
  }
}
