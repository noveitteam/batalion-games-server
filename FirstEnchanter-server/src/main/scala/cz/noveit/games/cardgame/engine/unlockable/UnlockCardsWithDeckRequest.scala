package cz.noveit.games.cardgame.engine.unlockable

import scala.collection.immutable.HashMap
import akka.actor._
import cz.noveit.games.cardgame.adapters._
import cz.noveit.database.DatabaseResult
import cz.noveit.database.DatabaseCommand
import cz.noveit.games.cardgame.engine.unlockable.CannotUnlockContent
import cz.noveit.games.cardgame.adapters.GetUnlockAbleDeckDetails
import cz.noveit.games.cardgame.adapters.CannotGetDeckDetails
import cz.noveit.games.cardgame.adapters.UnlockDeckForUser
import cz.noveit.games.cardgame.adapters.UnlockAbleDeckDetails
import cz.noveit.games.cardgame.adapters.UnlockAbleDeck
import cz.noveit.games.cardgame.adapters.UnlockAbleDeckForUserUnlocked
import cz.noveit.games.cardgame.adapters.CannotUnlockUnlockAbleDeckForUser
import cz.noveit.games.cardgame.engine.unlockable.ContentUnlocked
import cz.noveit.database.DatabaseResult
import cz.noveit.database.DatabaseCommand

/**
 * Created by Wlsek on 17.6.14.
 */
class UnlockCardsWithDeckRequest (
                                   id: String,
                                   parameters: HashMap[String, String],
                                   unlockAbleType: UnlockAbleType.Value,
                                   unlockAbleMethod: UnlockAbleMethod.Value,
                                   s: ActorRef,
                                   databaseController: ActorRef,
                                   databaseParameters: HashMap[String, String],
                                   user: String,
                                   u: UnlockAbleDeck,
                                   parentRouter: ActorRef
                                   ) extends Actor with ActorLogging
{

  val unlockAbleDatabaseAdapter = context.actorOf(
    Props(
      classOf[UnlockAbleDatabaseAdapter],
      context.actorSelection("/user/databaseController"),
      databaseParameters("databaseName"),
      databaseParameters("unlockAblesCollection"),
      databaseParameters("unlockAblesHistoryCollection"),
      databaseParameters("userHasCardsCollection"),
      databaseParameters("userUnlockedCollection"),
      databaseParameters("decksCollection")
    )
  )

  unlockAbleDatabaseAdapter ! DatabaseCommand(GetUnlockAbleDeckDetails(u.deckId))

  def receive = {
    case DatabaseResult(result, h) => {
      result match {

        case UnlockAbleDeckDetails(deck) =>
          unlockAbleDatabaseAdapter ! DatabaseCommand(UnlockDeckForUser(deck, user))

        case m: UnlockAbleDeckForUserUnlocked =>
          unlockAbleDatabaseAdapter ! DatabaseCommand(WriteUnlockAbleLog(id, unlockAbleType, unlockAbleMethod, System.currentTimeMillis() ,parameters.get("transactionId"), parameters.get("redemptionCode"), user))
          s ! ContentUnlocked(id, m)
          parentRouter ! Kill
          context.stop(self)

        case CannotGetDeckDetails(deck) =>
          s ! CannotUnlockContent(id)
          parentRouter ! Kill
          context.stop(self)

        case CannotUnlockUnlockAbleDeckForUser(deck, user) =>
          s ! CannotUnlockContent(id)
          parentRouter ! Kill
          context.stop(self)
      }
    }
  }
}
