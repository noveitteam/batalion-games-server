package cz.noveit.games.cardgame.engine.unlockable

import akka.actor._
import scala.collection.immutable.HashMap
import cz.noveit.games.cardgame.adapters._
import cz.noveit.database.{DatabaseResult, DatabaseCommand}
import cz.noveit.games.cardgame.engine.unlockable.CannotUnlockContent
import cz.noveit.games.cardgame.adapters.UnlockDeckIcon
import cz.noveit.games.cardgame.adapters.DeckIconUnlocked
import cz.noveit.games.cardgame.adapters.CannotUnlockDeckIcon
import cz.noveit.games.cardgame.engine.unlockable.ContentUnlocked
import cz.noveit.database.DatabaseResult
import cz.noveit.games.cardgame.adapters.UnlockAbleDeckIcon
import cz.noveit.database.DatabaseCommand

/**
 * Created by Wlsek on 17.6.14.
 */
class UnlockDeckIconRequest (
                             id: String,
                             parameters: HashMap[String, String],
                             unlockAbleType: UnlockAbleType.Value,
                             unlockAbleMethod: UnlockAbleMethod.Value,
                             s: ActorRef,
                             databaseController: ActorRef,
                             databaseParameters: HashMap[String, String],
                             user: String,
                             u: UnlockAbleDeckIcon,
                             parentRouter: ActorRef
                             ) extends Actor with ActorLogging
{

  val unlockAbleDatabaseAdapter = context.actorOf(
    Props(
      classOf[UnlockAbleDatabaseAdapter],
      context.actorSelection("/user/databaseController"),
      databaseParameters("databaseName"),
      databaseParameters("unlockAblesCollection"),
      databaseParameters("unlockAblesHistoryCollection"),
      databaseParameters("userHasCardsCollection"),
      databaseParameters("userUnlockedCollection"),
      databaseParameters("decksCollection")
    )
  )

  unlockAbleDatabaseAdapter ! DatabaseCommand(UnlockDeckIcon(u.deckIcon, user))

  def receive = {
    case DatabaseResult(result, h) => {
      result match {
        case CannotUnlockDeckIcon(deckIcon, user) =>
          s ! CannotUnlockContent(id)
          parentRouter ! Kill
          context.stop(self)

        case m: DeckIconUnlocked =>
          unlockAbleDatabaseAdapter ! DatabaseCommand(WriteUnlockAbleLog(id, unlockAbleType, unlockAbleMethod, System.currentTimeMillis() ,parameters.get("transactionId"), parameters.get("redemptionCode"), user))
          s ! ContentUnlocked(id, m)
          parentRouter ! Kill
          context.stop(self)
      }
    }
  }
}
