package cz.noveit.games.cardgame.logging

import akka.event.LoggingAdapter
import akka.event.Logging
import akka.actor.{ActorSystem, ActorContext}
import org.slf4j.{Marker, Logger, LoggerFactory}
;

/**
 * Created by Wlsek on 11.8.14.
 */

trait NoActorLogging  {
   val log = LoggerFactory.getLogger(this.getClass)

  def getName: String = log.getName

  def isTraceEnabled: Boolean = log.isTraceEnabled

  def trace(msg: String): Unit = log.trace(msg)

  def trace(format: String, arg: scala.Any): Unit = log.trace(format, arg)

  def trace(format: String, arg1: scala.Any, arg2: scala.Any): Unit = log.trace(format, arg1, arg2)

  def trace(format: String, arguments: Array[AnyRef]): Unit = log.trace(format, arguments)

  def trace(msg: String, t: Throwable): Unit = log.trace(msg, t)

  def isTraceEnabled(marker: Marker): Boolean =  log.isTraceEnabled(marker)

  def trace(marker: Marker, msg: String): Unit =  log.trace(marker, msg)

  def trace(marker: Marker, format: String, arg: scala.Any): Unit =  log.trace(marker, format, arg)

  def trace(marker: Marker, format: String, arg1: scala.Any, arg2: scala.Any): Unit =  log. trace(marker, format, arg1, arg2)

  def trace(marker: Marker, format: String, argArray: Array[AnyRef]): Unit =  log.trace(marker, format, argArray)

  def trace(marker: Marker, msg: String, t: Throwable): Unit =  log.trace(marker, msg, t)

  def isDebugEnabled: Boolean =  log.isDebugEnabled

  def debug(msg: String): Unit =  log.debug(msg)

  def debug(format: String, arg: scala.Any): Unit =  log.debug(format, arg)

  def debug(format: String, arg1: scala.Any, arg2: scala.Any): Unit =  log.debug(format, arg1, arg2)

  def debug(format: String, arguments: Array[AnyRef]): Unit =  log.debug(format, arguments)

  def debug(msg: String, t: Throwable): Unit =  log.debug(msg, t)

  def isDebugEnabled(marker: Marker): Boolean =  log.isDebugEnabled(marker)

  def debug(marker: Marker, msg: String): Unit =  log.debug(marker, msg)

  def debug(marker: Marker, format: String, arg: scala.Any): Unit =  log.debug(marker, format, arg)

  def debug(marker: Marker, format: String, arg1: scala.Any, arg2: scala.Any): Unit =  log.debug(marker, format, arg1, arg2)

  def debug(marker: Marker, format: String, arguments: Array[AnyRef]): Unit =  log.debug(marker, format, arguments)

  def debug(marker: Marker, msg: String, t: Throwable): Unit =  log.debug(marker, msg, t)

  def isInfoEnabled: Boolean =  log.isInfoEnabled

  def info(msg: String): Unit =  log.info(msg)

  def info(format: String, arg: scala.Any): Unit =  log.info(format, arg)

  def info(format: String, arg1: scala.Any, arg2: scala.Any): Unit = log.info(format, arg1, arg2)

  def info(format: String, arguments: Array[AnyRef]): Unit =  log.info(format, arguments)

  def info(msg: String, t: Throwable): Unit =  log.info(msg, t)

  def isInfoEnabled(marker: Marker): Boolean =  log.isInfoEnabled(marker)

  def info(marker: Marker, msg: String): Unit =  log.info(marker, msg)

  def info(marker: Marker, format: String, arg: scala.Any): Unit =  log.info(marker, format, arg)

  def info(marker: Marker, format: String, arg1: scala.Any, arg2: scala.Any): Unit =  log.info(marker, format, arg1, arg2)

  def info(marker: Marker, format: String, arguments: Array[AnyRef]): Unit =  log.info(marker, format, arguments)

  def info(marker: Marker, msg: String, t: Throwable): Unit =  log.info(marker, msg, t)

  def isWarnEnabled: Boolean =  log.isWarnEnabled

  def warn(msg: String): Unit =  log.warn(msg)

  def warn(format: String, arg: scala.Any): Unit =  log.warn(format, arg)

  def warn(format: String, arguments: Array[AnyRef]): Unit =  log.warn(format, arguments)

  def warn(format: String, arg1: scala.Any, arg2: scala.Any): Unit =  log.warn(format, arg1, arg2)

  def warn(msg: String, t: Throwable): Unit =  log.warn(msg, t)

  def isWarnEnabled(marker: Marker): Boolean =  log.isWarnEnabled(marker)

  def warn(marker: Marker, msg: String): Unit =  log.warn(marker, msg)

  def warn(marker: Marker, format: String, arg: scala.Any): Unit =  log.warn(marker, format, arg)

  def warn(marker: Marker, format: String, arg1: scala.Any, arg2: scala.Any): Unit =  log.warn(marker, format, arg1, arg2)

  def warn(marker: Marker, format: String, arguments: Array[AnyRef]): Unit =  log.warn(marker, format, arguments)

  def warn(marker: Marker, msg: String, t: Throwable): Unit =  log.warn(marker, msg, t)

  def isErrorEnabled: Boolean =  log.isErrorEnabled

  def error(msg: String): Unit =  log. error(msg)

  def error(format: String, arg: scala.Any): Unit =  log.error(format, arg)

  def error(format: String, arg1: scala.Any, arg2: scala.Any): Unit =  log.error(format, arg1, arg2)

  def error(format: String, arguments: Array[AnyRef]): Unit =  log.error(format, arguments)

  def error(msg: String, t: Throwable): Unit =  log.error(msg, t)

  def isErrorEnabled(marker: Marker): Boolean =  log.isErrorEnabled(marker)

  def error(marker: Marker, msg: String): Unit =  log.error(marker, msg)

  def error(marker: Marker, format: String, arg: scala.Any): Unit =  log.error(marker, format, arg)

  def error(marker: Marker, format: String, arg1: scala.Any, arg2: scala.Any): Unit =  log.error(marker, format, arg1, arg2)

  def error(marker: Marker, format: String, arguments: Array[AnyRef]): Unit =  log.error(marker, format, arguments)

  def error(marker: Marker, msg: String, t: Throwable): Unit =  log.error(marker, msg, t)
}
