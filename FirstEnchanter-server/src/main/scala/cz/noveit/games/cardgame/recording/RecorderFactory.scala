package cz.noveit.games.cardgame.recording

import cz.noveit.games.cardgame.engine.game.session.MatchSetup
import cz.noveit.games.cardgame.engine.game.{MatchQuerySetup}
import akka.actor.{Props, ActorContext, ActorRef}

/**
 * Created by Wlsek on 5.8.14.
 */
object RecorderFactory {
    def apply(setup: MatchSetup, querySetup: MatchQuerySetup)(implicit actorContext: ActorContext): ActorRef = {
        actorContext.actorOf(Props(classOf[Recording], setup, querySetup))
    }
}
