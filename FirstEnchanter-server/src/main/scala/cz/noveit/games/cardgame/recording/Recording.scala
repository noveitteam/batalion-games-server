package cz.noveit.games.cardgame.recording

import cz.noveit.games.cardgame.engine.game._
import cz.noveit.games.cardgame.engine.game.session._
import scala.xml.{Elem, Node, XML}
import akka.actor.{Actor, ActorLogging}
import java.net.URLEncoder
import org.joda.time.DateTime
import java.io.File
import cz.noveit.games.cardgame.engine.game.TableState
import cz.noveit.games.cardgame.engine.game.MatchQuerySetup
import scala.xml.transform.{RuleTransformer, RewriteRule}

/**
 * Created by Wlsek on 5.8.14.
 */

trait XmlRecording {
  def toXml: Node
}

case class RecordNewRound(number: Int, attackers: List[String], defenders: List[String], initialTableState: TableState)

case class RecordRollResult(rollResult: PlayerRollResult)

case class RecordAttackerUsesCard(us: AttackerUsesCard)

case class RecordDefenderUsesCard(us: DefenderUsesCard)

case class RecordTableAfterUsageEvaluation(tableState: TableState)

case class RecordAttackerAttacks(at: AttackerAttacks)

case class RecordDefenderDefends(de: DefenderDefends)

case class RecordTableAfterAttackEvaluation(tableState: TableState)

case class RecordUsageAndCounterUsageChanges(changes: List[TableStateChange])

case class RecordAttackAndDefenseChanges(changes: List[TableStateChange])

case class RecordMatchEnds(winners: List[String], losers: List[String])

class Recording(matchSetup: MatchSetup, querySetup: MatchQuerySetup) extends Actor with ActorLogging {

  val matchesFolder = try {
    context.system.settings.config.getString("world.recording.folder")
  } catch {
    case t: Throwable =>
      "matches"
  }

  val fileName = URLEncoder.encode(querySetup.players.map(p => p.player).sorted.foldLeft(new StringBuilder) {
    (sb, s) => sb append (s + "_")
  }.toString +
    new DateTime().toString("dd_MM_yyyy_HH_mm_ss_SSS"),
    "UTF-8"
  )

  log.debug("Creating Recording.")

  val file = matchesFolder + File.separator + fileName + ".xml"

  XML.save(file,
    <match started={new DateTime().toString("dd-MM-yyyy_HH:mm:ss:SSS")}>
      {matchSetup.toXml}{querySetup.toXml}<rounds>
    </rounds>
    </match>
    , "UTF-8", true, null)

  var runningRound: Option[Node] = None

  def receive = {
    case nr: RecordNewRound =>
      if (runningRound.isDefined) {
        XML.save(file, new RuleTransformer(new AddChildrenTo("rounds", runningRound.get)).transform(XML.load(file)).head, "UTF-8", true, null)
      }

      runningRound = Some(
        <round number={nr.number.toString} started={new DateTime().toString("dd-MM-yyyy_HH:mm:ss:SSS")}>

          <attackers>
            {nr.attackers.map(a => <player nickname={a}></player>)}
          </attackers>

          <defenders>
            {nr.defenders.map(d => <player nickname={d}></player>)}
          </defenders>

          <fortuneRolls>
          </fortuneRolls>

          <attackerUsesCard>
          </attackerUsesCard>

          <defenderUsesCard>
          </defenderUsesCard>

          <attackerAttacks>
          </attackerAttacks>

          <defenderDefends>
          </defenderDefends>

          <changesAfterUsageEvaluate>
          </changesAfterUsageEvaluate>

          <changesAfterAttackEvaluate>
          </changesAfterAttackEvaluate>

          <initialTableState>
            {nr.initialTableState.toXml}
          </initialTableState>

          <tableStateAfterUsageEvaluate>
          </tableStateAfterUsageEvaluate>

          <tableStateAfterAttackEvaluate>
          </tableStateAfterAttackEvaluate>
        </round>
      )

    case r: RecordRollResult =>
      runningRound.map(rr => runningRound = Some(new RuleTransformer(new AddChildrenTo("fortuneRolls", r.rollResult.toXml)).transform(rr).head))

    case r: RecordAttackerUsesCard =>
      runningRound.map(rr => runningRound = Some(new RuleTransformer(new AddChildrenTo("attackerUsesCard", r.us.toXml)).transform(rr).head))

    case r: RecordDefenderUsesCard =>
      runningRound.map(rr => runningRound = Some(new RuleTransformer(new AddChildrenTo("defenderUsesCard", r.us.toXml)).transform(rr).head))

    case r: RecordAttackerAttacks =>
      runningRound.map(rr => runningRound = Some(new RuleTransformer(new AddChildrenTo("attackerAttacks", r.at.toXml)).transform(rr).head))

    case r: RecordDefenderDefends =>
      runningRound.map(rr => runningRound = Some(new RuleTransformer(new AddChildrenTo("defenderDefends", r.de.toXml)).transform(rr).head))

    case r: RecordTableAfterUsageEvaluation =>
      runningRound.map(rr => runningRound = Some(new RuleTransformer(new AddChildrenTo("tableStateAfterUsageEvaluate", r.tableState.toXml)).transform(rr).head))

    case r: RecordTableAfterAttackEvaluation =>
      runningRound.map(rr => runningRound = Some(new RuleTransformer(new AddChildrenTo("tableStateAfterAttackEvaluate", r.tableState.toXml)).transform(rr).head))

    case r: RecordUsageAndCounterUsageChanges =>
      r.changes.map(ch => {
        runningRound.map(rr => runningRound = Some(new RuleTransformer(new AddChildrenTo("changesAfterUsageEvaluate", ch.toXml)).transform(rr).head))
      })

    case r: RecordAttackAndDefenseChanges =>
      r.changes.map(ch => {
        runningRound.map(rr => runningRound = Some(new RuleTransformer(new AddChildrenTo("changesAfterAttackEvaluate", ch.toXml)).transform(rr).head))
      })

    case r: RecordMatchEnds =>
      val lastRoundAdded = new RuleTransformer(new AddChildrenTo("rounds", runningRound.get)).transform(XML.load(file)).head
      val matchEndsAdded = new RuleTransformer(new AddChildrenTo("rounds",
      <matchEnding>
        <winners>{r.winners.map(w => <player nickname={w}></player>)}</winners>
        <losers>{r.losers.map(w => <player nickname={w}></player>)}</losers>
      </matchEnding>
      ))
        .transform(lastRoundAdded)
        .head

      XML.save(file, matchEndsAdded, "UTF-8", true, null)
  }


  def renameTableStateTag(tableState: Node, newName: String): Node = tableState match {
    case Elem(prefix, "tableState", attribs, scope, child@_*) =>
      Elem(prefix, newName, attribs, scope, true, child: _*)
    case other => other
  }
}


class AddChildrenTo(label: String, newChild: Node) extends RewriteRule {
  override def transform(n: Node) = n match {
    case n@Elem(_, `label`, _, _, _*) => addChild(n, newChild)
    case other => other
  }

  def addChild(n: Node, newChild: Node) = n match {
    case Elem(prefix, label, attribs, scope, child@_*) =>
      Elem(prefix, label, attribs, scope, true, child ++ newChild: _*)
    case _ => sys.error("Can only add children to elements!")
  }
}