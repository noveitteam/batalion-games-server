package cz.noveit.games.cluster

import cz.noveit.games.cluster.dispatch.ClusterMessage


/**
 * Created by Wlsek on 30.1.14.
 */
case class GameWorkerUserMetrics (users: Int) extends CustomMetrics with ClusterMessage
case class GameWorkerGamesMetric (games: Int) extends CustomMetrics with ClusterMessage