import akka.actor.{ActorRef, Props, ActorSystem}
import com.typesafe.config.ConfigFactory
import cz.noveit.games.cardgame.engine.game._
import cz.noveit.games.cardgame.engine.game.FindMatch
import cz.noveit.games.cardgame.engine.game.MatchQuerySetup
import cz.noveit.games.cardgame.engine.player.PlayerIsTryingToFindGame
import cz.noveit.games.cardgame.engine.player.PlayerIsTryingToFindGame
import cz.noveit.games.cluster.balancer.ClusterBalancer
import cz.noveit.games.cluster.ClusterListener
import cz.noveit.games.cluster.dispatch.ClusterDispatcher
import cz.noveit.games.cluster.registry.ClusterRegistry
import dummy.DummyClusterRegistry
import dummy.matchfinder.DummyMatchHandlerOk
import java.io.File
import org.scalatest.FlatSpec

/**
 * Created by Wlsek on 4.3.14.
 */
class FindMatchTests extends FlatSpec {
  "Creation of Match controller" should "create controller without exception" in {
    val as = ActorSystem("TestAS")
    val clusterDispatcher = as actorOf (Props[ClusterDispatcher])
    val matchController = as actorOf(Props(classOf[MatchController], clusterDispatcher), "matchController")
    as.shutdown()
    assert(true)
  }

  "Creation of Match controller in cluster" should "created controller without exception" in {
    var toEnd: List[ActorSystem] = List()
    try {
      val config = ConfigFactory.parseFile(new File("unittest-application.conf"))


      val node1 = ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeOne"))
      toEnd = node1 :: toEnd

      val node2 = ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeTwo"))
      toEnd = node2 :: toEnd

      val node3 = ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeThree"))
      toEnd = node3 :: toEnd

      val node4 = ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeFor"))
      toEnd = node4 :: toEnd

      try {

        val registry1 = node1 actorOf(Props[ClusterRegistry], "clusterRegistry")
        val dispatcher1 = node1 actorOf Props[ClusterDispatcher]
        val matchController1 = node1 actorOf(Props(classOf[MatchController], dispatcher1), "matchController")


        val registry2 = node2 actorOf(Props[ClusterRegistry], "clusterRegistry")
        val dispatcher2 = node2 actorOf Props[ClusterDispatcher]
        val matchController2 = node2 actorOf(Props(classOf[MatchController], dispatcher2), "matchController")

        val registry3 = node3 actorOf(Props[ClusterRegistry], "clusterRegistry")
        val dispatcher3 = node3 actorOf Props[ClusterDispatcher]
        val matchController3 = node3 actorOf(Props(classOf[MatchController], dispatcher3), "matchController")

        val registry4 = node4 actorOf(Props[ClusterRegistry], "clusterRegistry")
        val dispatcher4 = node4 actorOf Props[ClusterDispatcher]
        val matchController4 = node4 actorOf(Props(classOf[MatchController], dispatcher4), "matchController")


      } catch {
        case t: Throwable =>
          fail("Exception thrown: " + t.toString)

      } finally {
        node1.shutdown()
        node2.shutdown()
        node3.shutdown()
        node4.shutdown()
        Thread.sleep(500)
      }
    } catch {
      case t: Throwable =>
        fail("Exception thrown: " + t.toString)
        toEnd.map(t => t.shutdown())
    }
  }

  "Find match" should "should find match for 4 different players" in {
    var toEnd: List[ActorSystem] = List()

    try {
      val config = ConfigFactory.parseFile(new File("unittest-application.conf"))

      val node1 = ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeOne"))
      toEnd = node1 :: toEnd
      Thread.sleep(500)

      val node2 = ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeTwo"))
      toEnd = node2 :: toEnd

      val node3 = ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeThree"))
      toEnd = node3 :: toEnd

      val node4 = ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeFor"))
      toEnd = node4 :: toEnd

      try {

        val registry1 = node1 actorOf(Props[ClusterRegistry], "clusterRegistry")
        val dispatcher1 = node1 actorOf Props[ClusterDispatcher]
        val matchController1 = node1 actorOf(Props(classOf[MatchController], dispatcher1), "matchController")
        val balancer1 = node1 actorOf (Props(classOf[ClusterBalancer], dispatcher1))
        val listener1 = node1 actorOf(Props(classOf[ClusterListener], balancer1), "clusterListener")

        val registry2 = node2 actorOf(Props[ClusterRegistry], "clusterRegistry")
        val dispatcher2 = node2 actorOf Props[ClusterDispatcher]
        val matchController2 = node2 actorOf(Props(classOf[MatchController], dispatcher2), "matchController")
        val balancer2 = node2 actorOf (Props(classOf[ClusterBalancer], dispatcher2))
        val listener2 = node2 actorOf(Props(classOf[ClusterListener], balancer2), "clusterListener")

        val registry3 = node3 actorOf(Props[ClusterRegistry], "clusterRegistry")
        val dispatcher3 = node3 actorOf Props[ClusterDispatcher]
        val matchController3 = node3 actorOf(Props(classOf[MatchController], dispatcher3), "matchController")
        val balancer3 = node3 actorOf (Props(classOf[ClusterBalancer], dispatcher3))
        val listener3 = node3 actorOf(Props(classOf[ClusterListener], balancer3), "clusterListener")

        val registry4 = node4 actorOf(Props[ClusterRegistry], "clusterRegistry")
        val dispatcher4 = node4 actorOf Props[ClusterDispatcher]
        val matchController4 = node4 actorOf(Props(classOf[MatchController], dispatcher4), "matchController")
        val balancer4 = node4 actorOf (Props(classOf[ClusterBalancer], dispatcher4))
        val listener4 = node4 actorOf(Props(classOf[ClusterListener], balancer4), "clusterListener")

        Thread.sleep(10000)
        println("-----------------------------------------------------------------------------------------------------")

        val handler1 = makeOkHandlerForPlayer(node1, "Dan", matchController1)

        matchController1 ! FindMatch(
          MatchQuerySetup(List(PlayerIsTryingToFindGame("Dan", 1500, 40, 1, handler1)), None, MatchGameModePractice, MatchGameEnemyModePVP, 4)
        )


        val handler2 = makeOkHandlerForPlayer(node2, "Nasti", matchController2)

        matchController2 ! FindMatch(
          MatchQuerySetup(List(PlayerIsTryingToFindGame("Nasti", 1500, 40, 1, handler2)), None, MatchGameModePractice, MatchGameEnemyModePVP, 4)
        )

        val handler3 = makeOkHandlerForPlayer(node3, "Arnost", matchController3)

        matchController3 ! FindMatch(
          MatchQuerySetup(List(PlayerIsTryingToFindGame("Arnost", 1500, 40, 1, handler3)), None, MatchGameModePractice, MatchGameEnemyModePVP, 4)
        )


        val handler4 = makeOkHandlerForPlayer(node4, "Libushka", matchController4)
        matchController4 ! FindMatch(
          MatchQuerySetup(List(PlayerIsTryingToFindGame("Libushka", 1500, 40, 1, handler4)), None, MatchGameModePractice, MatchGameEnemyModePVP, 4)
        )

        Thread.sleep(30000)

      } catch {
        case t: Throwable =>
          fail("Exception thrown: " + t.toString)
      } finally {
        node1.shutdown()
        node2.shutdown()
        node3.shutdown()
        node4.shutdown()
        Thread.sleep(500)
      }
    } catch {
      case t: Throwable =>
        fail("Exception thrown: " + t.toString)
        toEnd.map(t => t.shutdown())
    }
  }


  def makeOkHandlerForPlayer(as: ActorSystem, player: String, matchController: ActorRef) = {
    as.actorOf(Props(classOf[DummyMatchHandlerOk], player, matchController))
  }
}
