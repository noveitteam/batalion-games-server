import akka.actor.{Props, ActorSystem}
import cz.noveit.games.cardgame.adapters.Card
import cz.noveit.games.cardgame.adapters.Card
import cz.noveit.games.cardgame.adapters.Deck
import cz.noveit.games.cardgame.adapters.Deck
import cz.noveit.games.cardgame.adapters.ElementsRating
import cz.noveit.games.cardgame.adapters.ElementsRating
import cz.noveit.games.cardgame.adapters.{ElementsRating, Card, Deck}
import cz.noveit.games.cardgame.engine.game._
import cz.noveit.games.cardgame.engine.game.CannotComposePlayer
import cz.noveit.games.cardgame.engine.game.MatchQuerySetup
import cz.noveit.games.cardgame.engine.game.MatchQuerySetup
import cz.noveit.games.cardgame.engine.game.MatchQuerySetup
import cz.noveit.games.cardgame.engine.game.MatchSetup
import cz.noveit.games.cardgame.engine.game.MatchSetup
import cz.noveit.games.cardgame.engine.game.MatchSetup
import cz.noveit.games.cardgame.engine.game.StartingRollPosition
import cz.noveit.games.cardgame.engine.game.StartingRollPosition
import cz.noveit.games.cardgame.engine.game.StateAckParameters
import cz.noveit.games.cardgame.engine.handlers.GetCardDatabaseAdapter
import cz.noveit.games.cardgame.engine.player._
import cz.noveit.games.cardgame.engine.player.Player
import cz.noveit.games.cardgame.engine.player.PlayerIsTryingToFindGame
import cz.noveit.games.cardgame.engine.player.Wallet
import dummy.game._
import dummy.game.DummyCardDatabaseDeck
import dummy.game.DummyCardDatabaseDeck
import dummy.game.DummyCardDatabaseRow
import dummy.game.DummyCardDatabaseRow
import org.scalatest.FlatSpec
import scala.collection.immutable.HashMap
import scala.concurrent.duration.FiniteDuration
import akka.testkit.{TestActorRef, TestFSMRef}

import akka.actor._
import akka.util._
import scala.concurrent.duration._
import akka.pattern.ask
import scala.concurrent.Await

/**
 * Created by Wlsek on 26.2.14.
 */
class MatchTests2Vs2 extends FlatSpec {
  "Match" should "initialize, when all 4 player are composed." in {

    val actorSystem = ActorSystem("Test-AS")

    val goodData = List(
      DummyCardDatabaseRow("Dan", List(DummyCardDatabaseDeck(
        Deck("1", "deck", 1, ElementsRating(1, 1, 1, 1), List("1"), "Dan"),
        List(
          Card("1", "card1", 1, 1, 1, "v1", 1, List(), List(), List())
        )))),
      DummyCardDatabaseRow("Nasti", List(DummyCardDatabaseDeck(
        Deck("1", "deck", 1, ElementsRating(1, 1, 1, 1), List("1"), "Nasti"),
        List(
          Card("1", "card1", 1, 1, 1, "v1", 1, List(), List(), List())
        )))),
      DummyCardDatabaseRow("Tomas", List(DummyCardDatabaseDeck(
        Deck("1", "deck", 1, ElementsRating(1, 1, 1, 1), List("1"), "Tomas"),
        List(
          Card("1", "card1", 1, 1, 1, "v1", 1, List(), List(), List())
        )))),
      DummyCardDatabaseRow("Arnost", List(DummyCardDatabaseDeck(
        Deck("1", "deck", 1, ElementsRating(1, 1, 1, 1), List("1"), "Arnost"),
        List(
          Card("1", "card1", 1, 1, 1, "v1", 1, List(), List(), List())
        ))))
    )

    val cardDatabase = actorSystem.actorOf(Props(classOf[DummyCardDatabaseHandler], goodData))
    val playerHandler = actorSystem.actorOf(Props(classOf[DummyPlayerHandler], cardDatabase))

    val player1 = PlayerIsTryingToFindGame("Dan", 1500, 50, 1, playerHandler)
    val player2 = PlayerIsTryingToFindGame("Nasti", 1500, 50, 1, playerHandler)
    val player3 = PlayerIsTryingToFindGame("Tomas", 1500, 50, 1, playerHandler)
    val player4 = PlayerIsTryingToFindGame("Arnost", 1500, 50, 1, playerHandler)

    val setups = List(
      MatchQuerySetup(List(player1, player2), None, MatchGameModePractice, MatchGameEnemyModePVP, 4),
      MatchQuerySetup(List(player3, player4), None, MatchGameModePractice, MatchGameEnemyModePVP, 4)
    )

    val setup = MatchSetup(HashMap[String, FiniteDuration](
      "playersStartRoll" -> 20.seconds,
      "attackerPlays" -> 20.seconds,
      "defenderCounterPlays" -> 20.seconds,
      "attackerAttacks" -> 20.seconds,
      "playersRoll" -> 20.seconds
    ), 2, 8, "1")

    val startingRollPosition: List[StartingRollPosition] = List(
      StartingRollPosition("Dan", PLAYER_ROLL_POSITION_ATTACKER),
      StartingRollPosition("Nasti", PLAYER_ROLL_POSITION_ATTACKER),
      StartingRollPosition("Tomas", PLAYER_ROLL_POSITION_DEFENDER),
      StartingRollPosition("Arnost", PLAYER_ROLL_POSITION_DEFENDER)
    )

    implicit val system = actorSystem
    val game = TestFSMRef(new Match(setups, setup, startingRollPosition, List(
      PlayerPlaysWithActiveDeck("Dan", "1"),
      PlayerPlaysWithActiveDeck("Nasti", "1"),
      PlayerPlaysWithActiveDeck("Tomas", "1"),
      PlayerPlaysWithActiveDeck("Arnost", "1")
    )))
    Thread.sleep(500)

    implicit val timeout = Timeout(5 seconds)
    val future = playerHandler ? GetMessages
    val messages = Await.result(future, timeout.duration).asInstanceOf[List[Any]]

    assert(messages.filter(p => p == GetCardDatabaseAdapter).size == 4)

    Thread.sleep(500)

    assert(game.stateName == STATE_ACK)
    assert(game.stateData.isInstanceOf[StateAckParameters])
    assert(game.stateData.asInstanceOf[StateAckParameters].state == PLAYER_STARTS_ROLL)

    actorSystem.shutdown()

    Thread.sleep(500)
  }

  "Match" should "stop and send CannotComposePlayer, when some player has error while initializating." in {

    val actorSystem = ActorSystem("Test-AS")

    val goodData = List(
      DummyCardDatabaseRow("Dan", List(DummyCardDatabaseDeck(
        Deck("1", "deck", 1, ElementsRating(1, 1, 1, 1), List("1"), "Dan"),
        List(
          Card("1", "card1", 1, 1, 1, "v1", 1, List(), List(), List())
        )))),
      DummyCardDatabaseRow("Nasti", List(DummyCardDatabaseDeck(
        Deck("1", "deck", 1, ElementsRating(1, 1, 1, 1), List("1"), "Nasti"),
        List(
          Card("1", "card1", 1, 1, 1, "v1", 1, List(), List(), List())
        )))),
      DummyCardDatabaseRow("Tomas", List(DummyCardDatabaseDeck(
        Deck("1", "deck", 1, ElementsRating(1, 1, 1, 1), List("1"), "Tomas"),
        List(
          Card("1", "card1", 1, 1, 1, "v1", 1, List(), List(), List())
        ))))
    )

    val cardDatabase = actorSystem.actorOf(Props(classOf[DummyCardDatabaseHandler], goodData))
    val playerHandler = actorSystem.actorOf(Props(classOf[DummyPlayerHandler], cardDatabase))

    val player1 = PlayerIsTryingToFindGame("Dan", 1500, 50, 1, playerHandler)
    val player2 = PlayerIsTryingToFindGame("Nasti", 1500, 50, 1, playerHandler)
    val player3 = PlayerIsTryingToFindGame("Tomas", 1500, 50, 1, playerHandler)
    val player4 = PlayerIsTryingToFindGame("Arnost", 1500, 50, 1, playerHandler)

    val setups = List(
      MatchQuerySetup(List(player1, player2), None, MatchGameModePractice, MatchGameEnemyModePVP, 4),
      MatchQuerySetup(List(player3, player4), None, MatchGameModePractice, MatchGameEnemyModePVP, 4)
    )

    val setup = MatchSetup(HashMap[String, FiniteDuration](
      "playersStartRoll" -> 20.seconds,
      "attackerPlays" -> 20.seconds,
      "defenderCounterPlays" -> 20.seconds,
      "attackerAttacks" -> 20.seconds,
      "playersRoll" -> 20.seconds
    ), 2, 8, "1")

    val startingRollPosition: List[StartingRollPosition] = List(
      StartingRollPosition("Dan", PLAYER_ROLL_POSITION_ATTACKER),
      StartingRollPosition("Nasti", PLAYER_ROLL_POSITION_ATTACKER),
      StartingRollPosition("Tomas", PLAYER_ROLL_POSITION_DEFENDER),
      StartingRollPosition("Arnost", PLAYER_ROLL_POSITION_DEFENDER)
    )

    implicit val system = actorSystem
    val game = TestFSMRef(new Match(setups, setup, startingRollPosition, List(
      PlayerPlaysWithActiveDeck("Dan", "1"),
      PlayerPlaysWithActiveDeck("Nasti", "1"),
      PlayerPlaysWithActiveDeck("Tomas", "1"),
      PlayerPlaysWithActiveDeck("Arnost", "1")
    )))
    Thread.sleep(500)

    implicit val timeout = Timeout(5 seconds)
    val future = playerHandler ? GetMessages
    val messages = Await.result(future, timeout.duration).asInstanceOf[List[Any]]

    assert(messages.filter(p => p == GetCardDatabaseAdapter).size == 4)
    assert(messages.filter(p => p.isInstanceOf[CannotComposePlayer]).size == 4)

    messages.filter(p => p.isInstanceOf[CannotComposePlayer]).map(msg => assert(msg.asInstanceOf[CannotComposePlayer].player == "Arnost"))

    actorSystem.shutdown()

    Thread.sleep(500)
  }

  "Match" should "successfully control whole turn, when everything ok" in {

    val actorSystem = ActorSystem("Test-AS")

    val goodData = List(
      DummyCardDatabaseRow("Dan", List(DummyCardDatabaseDeck(
        Deck("1", "deck", 1, ElementsRating(1, 1, 1, 1), List("1"), "Dan"),
        List(
          Card("1", "card1", 1, 1, 1, "v1", 1, List(), List(), List())
        )))),
      DummyCardDatabaseRow("Nasti", List(DummyCardDatabaseDeck(
        Deck("1", "deck", 1, ElementsRating(1, 1, 1, 1), List("1"), "Nasti"),
        List(
          Card("1", "card1", 1, 1, 1, "v1", 1, List(), List(), List())
        )))),
      DummyCardDatabaseRow("Tomas", List(DummyCardDatabaseDeck(
        Deck("1", "deck", 1, ElementsRating(1, 1, 1, 1), List("1"), "Tomas"),
        List(
          Card("1", "card1", 1, 1, 1, "v1", 1, List(), List(), List())
        )))),
      DummyCardDatabaseRow("Arnost", List(DummyCardDatabaseDeck(
        Deck("1", "deck", 1, ElementsRating(1, 1, 1, 1), List("1"), "Arnost"),
        List(
          Card("1", "card1", 1, 1, 1, "v1", 1, List(), List(), List())
        ))))
    )


    implicit val system = actorSystem

    val cardDatabase = actorSystem.actorOf(Props(classOf[DummyCardDatabaseHandler], goodData))

    val reporter = TestActorRef(new GameReporter(4))

    val playerHandler1 = actorSystem.actorOf(Props(classOf[FullDummyPlayerHandler], cardDatabase, "Dan", reporter.actorRef))
    val playerHandler2 = actorSystem.actorOf(Props(classOf[FullDummyPlayerHandler], cardDatabase, "Nasti", reporter.actorRef))
    val playerHandler3 = actorSystem.actorOf(Props(classOf[FullDummyPlayerHandler], cardDatabase, "Tomas", reporter.actorRef))
    val playerHandler4 = actorSystem.actorOf(Props(classOf[FullDummyPlayerHandler], cardDatabase, "Arnost", reporter.actorRef))

    val player1 = PlayerIsTryingToFindGame("Dan", 1500, 50, 1, playerHandler1)
    val player2 = PlayerIsTryingToFindGame("Nasti", 1500, 50, 1, playerHandler2)
    val player3 = PlayerIsTryingToFindGame("Tomas", 1500, 50, 1, playerHandler3)
    val player4 = PlayerIsTryingToFindGame("Arnost", 1500, 50, 1, playerHandler4)

    val setups = List(
      MatchQuerySetup(List(player1, player2), None, MatchGameModePractice, MatchGameEnemyModePVP, 4),
      MatchQuerySetup(List(player3, player4), None, MatchGameModePractice, MatchGameEnemyModePVP, 4)
    )

    val setup = MatchSetup(HashMap[String, FiniteDuration](
      "playersStartRoll" -> 20.seconds,
      "attackerPlays" -> 20.seconds,
      "defenderCounterPlays" -> 20.seconds,
      "attackerAttacks" -> 20.seconds,
      "playersRoll" -> 20.seconds
    ), 2, 8, "1")

    val startingRollPosition: List[StartingRollPosition] = List(
      StartingRollPosition("Dan", PLAYER_ROLL_POSITION_ATTACKER),
      StartingRollPosition("Nasti", PLAYER_ROLL_POSITION_ATTACKER),
      StartingRollPosition("Tomas", PLAYER_ROLL_POSITION_DEFENDER),
      StartingRollPosition("Arnost", PLAYER_ROLL_POSITION_DEFENDER)
    )


    val game = TestFSMRef(new Match(setups, setup, startingRollPosition, List(
      PlayerPlaysWithActiveDeck("Dan", "1"),
      PlayerPlaysWithActiveDeck("Nasti", "1"),
      PlayerPlaysWithActiveDeck("Tomas", "1"),
      PlayerPlaysWithActiveDeck("Arnost", "1")
    )))



    Thread.sleep(2000)
    //  println(reporter.underlyingActor.actualTurn)
    println(reporter.underlyingActor.turns)

    assert(!reporter.underlyingActor.turns.isEmpty)

    var lastAttackers: scala.collection.mutable.Set[String] = scala.collection.mutable.Set.empty[String]
    var lastDefenders: scala.collection.mutable.Set[String] = scala.collection.mutable.Set.empty[String]

    reporter.underlyingActor.turns.map(t => {
      val attackers: scala.collection.mutable.Set[String] = scala.collection.mutable.Set.empty[String]
      val defenders: scala.collection.mutable.Set[String] = scala.collection.mutable.Set.empty[String]

      t.useCard.map(u => attackers.add(u.userName))
      t.waitForCounterCard.map(u => attackers.add(u.userName))
      t.attack.map(u => attackers.add(u.userName))

      t.waitForCard.map(u => defenders.add(u.userName))
      t.useCounterCard.map(u => defenders.add(u.userName))
      t.waitForAttack.map(u => defenders.add(u.userName))

      assert(attackers.filter(p => defenders.contains(p)).isEmpty)
      assert(lastAttackers.filter(p => attackers.contains(p)).isEmpty)
      assert(lastDefenders.filter(p => defenders.contains(p)).isEmpty)
      assert(attackers.size == defenders.size)

      lastAttackers = attackers
      lastDefenders = defenders
    })
    actorSystem.shutdown()
  }
}
