
import cz.noveit.games.cardgame.adapters.User
import cz.noveit.games.cardgame.adapters.User
import cz.noveit.games.cardgame.adapters.User
import cz.noveit.games.cardgame.adapters.User
import cz.noveit.games.cardgame.engine._
import cz.noveit.games.cardgame.engine.CreatePlayerForContext
import cz.noveit.games.cardgame.engine.CreatePlayerForContext
import cz.noveit.games.cardgame.engine.handlers._
import cz.noveit.games.cardgame.engine.PlayerOnline
import cz.noveit.games.cluster.balancer.ClusterBalancer
import cz.noveit.games.cluster.ClusterListener
import cz.noveit.games.cluster.dispatch.ClusterMessageEvent
import cz.noveit.games.cluster.dispatch.DispatchMessageBroadcast
import cz.noveit.games.cluster.dispatch.{DispatchMessageBroadcast, ClusterMessageEvent, ClusterDispatcher}
import cz.noveit.games.cluster.registry.SubscribeActorForModuleName
import cz.noveit.games.cluster.registry.{ClusterRegistry, SubscribeActorForModuleName}
import cz.noveit.proto.serialization.MessageEventContext
import cz.noveit.service.ContextDestroy
import cz.noveit.service.ContextDestroy
import dummy._
import java.io.File
import org.scalatest.FlatSpec
import com.typesafe.config.{Config, ConfigFactory}
import akka.actor.{ActorRef, ActorContext, Props, ActorSystem}

import akka.pattern.ask
import akka.util.Timeout
import scala.collection.immutable.HashMap
import scala.concurrent.duration._
import scala.concurrent.Await

/**
 * Created by Wlsek on 12.2.14.
 */
case class NodeConfigPair(system: ActorSystem, config: Config)

class PlayerControllerTest extends FlatSpec {
  "PlayerController" should "subscribe to cluster registry" in {

    val config = ConfigFactory.parseFile(new File("unittest-application.conf"))

    val pServiceFactory = new ServiceHandlersModuleFactoryImpl
    val pHandlerFactory = new PlayerHandlerFactory {
      def playerHandler(actorContext: ActorContext, user: User, messageEventContext: MessageEventContext, parent: ActorRef, serviceHandlersFactory: ServiceHandlersModuleFactory, databaseParameters: HashMap[String, String]): ActorRef = {
        actorContext.actorOf(Props(classOf[DummyPlayerHandler], user, messageEventContext, parent, serviceHandlersFactory))
      }
    }

    val node1 = ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeOne"))
    val registry1 = node1 actorOf Props[DummyClusterRegistry]
    val dispatcher1 = node1 actorOf Props[ClusterDispatcher]
    val pc1 = node1 actorOf Props(classOf[PlayerController], dispatcher1, registry1, pServiceFactory, pHandlerFactory)

    val node2 = ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeTwo"))
    val registry2 = node2 actorOf Props[DummyClusterRegistry]
    val dispatcher2 = node2 actorOf Props[ClusterDispatcher]
    val pc2 = node1 actorOf Props(classOf[PlayerController], dispatcher2, registry2, pServiceFactory, pHandlerFactory)

    val node3 = ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeThree"))
    val registry3 = node3 actorOf Props[DummyClusterRegistry]
    val dispatcher3 = node3 actorOf Props[ClusterDispatcher]
    val pc3 = node1 actorOf Props(classOf[PlayerController], dispatcher3, registry3, pServiceFactory, pHandlerFactory)

    val node4 = ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeFor"))
    val registry4 = node4 actorOf Props[DummyClusterRegistry]
    val dispatcher4 = node4 actorOf Props[ClusterDispatcher]
    val pc4 = node1 actorOf Props(classOf[PlayerController], dispatcher4, registry4, pServiceFactory, pHandlerFactory)

    implicit val timeout = Timeout(5 seconds)
    val future1 = registry1 ? DummyClusterRegistryMessages
    val result1 = Await.result(future1, timeout.duration).asInstanceOf[List[Any]]

    val future2 = registry2 ? DummyClusterRegistryMessages
    val result2 = Await.result(future2, timeout.duration).asInstanceOf[List[Any]]

    val future3 = registry3 ? DummyClusterRegistryMessages
    val result3 = Await.result(future3, timeout.duration).asInstanceOf[List[Any]]

    val future4 = registry4 ? DummyClusterRegistryMessages
    val result4 = Await.result(future4, timeout.duration).asInstanceOf[List[Any]]

    assert(result1.size == 1)
    assert(result2.size == 1)
    assert(result3.size == 1)
    assert(result4.size == 1)

    assert(result1.head.asInstanceOf[SubscribeActorForModuleName].actor.equals(pc1) && result1.head.asInstanceOf[SubscribeActorForModuleName].moduleName.equals("CardGamePlayer"))
    assert(result2.head.asInstanceOf[SubscribeActorForModuleName].actor.equals(pc2) && result1.head.asInstanceOf[SubscribeActorForModuleName].moduleName.equals("CardGamePlayer"))
    assert(result3.head.asInstanceOf[SubscribeActorForModuleName].actor.equals(pc3) && result1.head.asInstanceOf[SubscribeActorForModuleName].moduleName.equals("CardGamePlayer"))
    assert(result4.head.asInstanceOf[SubscribeActorForModuleName].actor.equals(pc4) && result1.head.asInstanceOf[SubscribeActorForModuleName].moduleName.equals("CardGamePlayer"))

    node1.shutdown()
    node2.shutdown()
    node3.shutdown()
    node4.shutdown()

    Thread.sleep(5000)
  }

  "PlayerController" should "create user handler for CreatePlayerForContext" in {

    val config = ConfigFactory.parseFile(new File("unittest-application.conf"))

    val pServiceFactory = new ServiceHandlersModuleFactoryImpl

    val node1 = ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeOne"))
    val registry1 = node1 actorOf Props[DummyClusterRegistry]
    val dispatcher1 = node1 actorOf Props[ClusterDispatcher]
    val f1 = new DummyPlayerHandlerFactory()
    val pc1 = node1 actorOf Props(classOf[PlayerController], dispatcher1, registry1, pServiceFactory, f1)

    val node2 = ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeTwo"))
    val registry2 = node2 actorOf Props[DummyClusterRegistry]
    val dispatcher2 = node2 actorOf Props[ClusterDispatcher]
    val f2 = new DummyPlayerHandlerFactory()
    val pc2 = node1 actorOf Props(classOf[PlayerController], dispatcher2, registry2, pServiceFactory, f2)

    val node3 = ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeThree"))
    val registry3 = node3 actorOf Props[DummyClusterRegistry]
    val dispatcher3 = node3 actorOf Props[ClusterDispatcher]
    val f3 = new DummyPlayerHandlerFactory()
    val pc3 = node1 actorOf Props(classOf[PlayerController], dispatcher3, registry3, pServiceFactory, f3)

    val node4 = ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeFor"))
    val registry4 = node4 actorOf Props[DummyClusterRegistry]
    val dispatcher4 = node4 actorOf Props[ClusterDispatcher]
    val f4 = new DummyPlayerHandlerFactory()
    val pc4 = node1 actorOf Props(classOf[PlayerController], dispatcher4, registry4, pServiceFactory, f4)

    pc1 ! CreatePlayerForContext(User("user11", "", "", 0L, None), new DummyMessageEventContext)
    pc1 ! CreatePlayerForContext(User("user12", "", "", 0L, None), new DummyMessageEventContext)
    pc1 ! CreatePlayerForContext(User("user13", "", "", 0L, None), new DummyMessageEventContext)

    pc2 ! CreatePlayerForContext(User("user21", "", "", 0L, None), new DummyMessageEventContext)
    pc2 ! CreatePlayerForContext(User("user22", "", "", 0L, None), new DummyMessageEventContext)
    pc2 ! CreatePlayerForContext(User("user23", "", "", 0L, None), new DummyMessageEventContext)
    pc2 ! CreatePlayerForContext(User("user24", "", "", 0L, None), new DummyMessageEventContext)

    pc3 ! CreatePlayerForContext(User("user31", "", "", 0L, None), new DummyMessageEventContext)

    pc4 ! CreatePlayerForContext(User("user41", "", "", 0L, None), new DummyMessageEventContext)
    pc4 ! CreatePlayerForContext(User("user42", "", "", 0L, None), new DummyMessageEventContext)

    Thread.sleep(500)

    implicit val timeout = Timeout(5 seconds)
    val future1 = pc1 ? GetNumberOfPlayers
    val result1 = Await.result(future1, timeout.duration).asInstanceOf[Int]

    val future2 = pc2 ? GetNumberOfPlayers
    val result2 = Await.result(future2, timeout.duration).asInstanceOf[Int]

    val future3 = pc3 ? GetNumberOfPlayers
    val result3 = Await.result(future3, timeout.duration).asInstanceOf[Int]

    val future4 = pc4 ? GetNumberOfPlayers
    val result4 = Await.result(future4, timeout.duration).asInstanceOf[Int]

    node1.shutdown()
    node2.shutdown()
    node3.shutdown()
    node4.shutdown()

    assert(result1 == 3)
    assert(result2 == 4)
    assert(result3 == 1)
    assert(result4 == 2)

    Thread.sleep(5000)
  }

  "PlayerController" should "pass CluesterEventMessage with PlayerOnline message to player handler" in {


    val config = ConfigFactory.parseFile(new File("unittest-application.conf"))

    val pServiceFactory = new ServiceHandlersModuleFactoryImpl

    val node1 = ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeOne"))

    val counter = node1 actorOf Props[DummyPlayerHandlerCounter]

    val registry1 = node1 actorOf(Props[ClusterRegistry], "clusterRegistry")
    val dispatcher1 = node1 actorOf Props[ClusterDispatcher]
    val f1 = new DummyPlayerHandlerFactoryWithCounter(counter)
    val pc1 = node1 actorOf Props(classOf[PlayerController], dispatcher1, registry1, pServiceFactory, f1)
    val cb1 = node1 actorOf(Props(classOf[ClusterBalancer], dispatcher1))
    val cl1 = node1 actorOf(Props(classOf[ClusterListener], cb1), "clusterListener")

    val node2 = ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeTwo"))
    val registry2 = node2 actorOf(Props[ClusterRegistry], "clusterRegistry")
    val dispatcher2 = node2 actorOf Props[ClusterDispatcher]
    val f2 = new DummyPlayerHandlerFactoryWithCounter(counter)
    val pc2 = node2 actorOf Props(classOf[PlayerController], dispatcher2, registry2, pServiceFactory, f2)
    val cb2 = node2 actorOf(Props(classOf[ClusterBalancer], dispatcher2))
    val cl2 = node2 actorOf(Props(classOf[ClusterListener], cb2), "clusterListener")

    val node3 = ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeThree"))
    val registry3 = node3 actorOf(Props[ClusterRegistry], "clusterRegistry")
    val dispatcher3 = node3 actorOf Props[ClusterDispatcher]
    val f3 = new DummyPlayerHandlerFactoryWithCounter(counter)
    val pc3 = node3 actorOf Props(classOf[PlayerController], dispatcher3, registry3, pServiceFactory, f3)
    val cb3 = node3 actorOf(Props(classOf[ClusterBalancer], dispatcher3))
    val cl3 = node3 actorOf(Props(classOf[ClusterListener], cb3), "clusterListener")

    val node4 = ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeFor"))
    val registry4 = node4 actorOf(Props[ClusterRegistry], "clusterRegistry")
    val dispatcher4 = node4 actorOf Props[ClusterDispatcher]
    val f4 = new DummyPlayerHandlerFactoryWithCounter(counter)
    val pc4 = node4 actorOf Props(classOf[PlayerController], dispatcher4, registry4, pServiceFactory, f4)
    val cb4 = node4 actorOf(Props(classOf[ClusterBalancer], dispatcher4))
    val cl4 = node4 actorOf(Props(classOf[ClusterListener], cb4), "clusterListener")

    Thread.sleep(10000)

    pc1 ! CreatePlayerForContext(User("user11", "", "", 0L, None), new DummyMessageEventContext)
    pc1 ! CreatePlayerForContext(User("user12", "", "", 0L, None), new DummyMessageEventContext)
    pc1 ! CreatePlayerForContext(User("user13", "", "", 0L, None), new DummyMessageEventContext)

    pc2 ! CreatePlayerForContext(User("user21", "", "", 0L, None), new DummyMessageEventContext)
    pc2 ! CreatePlayerForContext(User("user22", "", "", 0L, None), new DummyMessageEventContext)
    pc2 ! CreatePlayerForContext(User("user23", "", "", 0L, None), new DummyMessageEventContext)
    pc2 ! CreatePlayerForContext(User("user24", "", "", 0L, None), new DummyMessageEventContext)

    pc3 ! CreatePlayerForContext(User("user31", "", "", 0L, None), new DummyMessageEventContext)

    pc4 ! CreatePlayerForContext(User("user41", "", "", 0L, None), new DummyMessageEventContext)
    pc4 ! CreatePlayerForContext(User("user42", "", "", 0L, None), new DummyMessageEventContext)

    Thread.sleep(5000)

    implicit val timeout = Timeout(5 seconds)
    val future = counter ? GetActors
    val result = Await.result(future, timeout.duration).asInstanceOf[List[ActorRef]]

    var resultSizeCondition = result.size

    dispatcher1 ! DispatchMessageBroadcast(ClusterMessageEvent(PlayerOnline(User("user55", "", "", 0L, None), pc4), "CardGamePlayer", ""))
                                                  Thread.sleep(5000)
    var onlineListConditions: List[List[Any]] = List()

    try {
      result.map(h => {
        val fut = h ? DummyPlayerHandlerMessages
        val res = Await.result(fut, timeout.duration).asInstanceOf[List[Any]]
        onlineListConditions = res :: onlineListConditions
      })
    } finally {
      node1.shutdown()
      node2.shutdown()
      node3.shutdown()
      node4.shutdown()
    }

    Thread.sleep(5000)

    assert(resultSizeCondition == 10)
    assert(onlineListConditions.size == 10)

    onlineListConditions.map(s => {
      println("///size: " + s.size  )
      println("-------------------------------------------------: " + s)
      assert(s.size == 11)
    })

  }

  "PlayerController" should "pass CluesterEventMessage with PlayerOffline message to player handler" in {


    val config = ConfigFactory.parseFile(new File("unittest-application.conf"))

    val pServiceFactory = new ServiceHandlersModuleFactoryImpl

    val node1 = ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeOne"))

    val counter = node1 actorOf Props[DummyPlayerHandlerCounter]

    val registry1 = node1 actorOf(Props[ClusterRegistry], "clusterRegistry")
    val dispatcher1 = node1 actorOf Props[ClusterDispatcher]
    val f1 = new DummyPlayerHandlerFactoryWithCounter(counter)
    val pc1 = node1 actorOf Props(classOf[PlayerController], dispatcher1, registry1, pServiceFactory, f1)
    val cb1 = node1 actorOf(Props(classOf[ClusterBalancer], dispatcher1))
    val cl1 = node1 actorOf(Props(classOf[ClusterListener], cb1), "clusterListener")

    val node2 = ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeTwo"))
    val registry2 = node2 actorOf(Props[ClusterRegistry], "clusterRegistry")
    val dispatcher2 = node2 actorOf Props[ClusterDispatcher]
    val f2 = new DummyPlayerHandlerFactoryWithCounter(counter)
    val pc2 = node2 actorOf Props(classOf[PlayerController], dispatcher2, registry2, pServiceFactory, f2)
    val cb2 = node2 actorOf(Props(classOf[ClusterBalancer], dispatcher2))
    val cl2 = node2 actorOf(Props(classOf[ClusterListener], cb2), "clusterListener")

    val node3 = ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeThree"))
    val registry3 = node3 actorOf(Props[ClusterRegistry], "clusterRegistry")
    val dispatcher3 = node3 actorOf Props[ClusterDispatcher]
    val f3 = new DummyPlayerHandlerFactoryWithCounter(counter)
    val pc3 = node3 actorOf Props(classOf[PlayerController], dispatcher3, registry3, pServiceFactory, f3)
    val cb3 = node3 actorOf(Props(classOf[ClusterBalancer], dispatcher3))
    val cl3 = node3 actorOf(Props(classOf[ClusterListener], cb3), "clusterListener")

    val node4 = ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeFor"))
    val registry4 = node4 actorOf(Props[ClusterRegistry], "clusterRegistry")
    val dispatcher4 = node4 actorOf Props[ClusterDispatcher]
    val f4 = new DummyPlayerHandlerFactoryWithCounter(counter)
    val pc4 = node4 actorOf Props(classOf[PlayerController], dispatcher4, registry4, pServiceFactory, f4)
    val cb4 = node4 actorOf(Props(classOf[ClusterBalancer], dispatcher4))
    val cl4 = node4 actorOf(Props(classOf[ClusterListener], cb4), "clusterListener")

    Thread.sleep(10000)

    val ctx1 =  new DummyMessageEventContext()
    val ctx2 =  new DummyMessageEventContext()
    val ctx3 =  new DummyMessageEventContext()
    val ctx4 =  new DummyMessageEventContext()
    val ctx5 =  new DummyMessageEventContext()
    val ctx6 =  new DummyMessageEventContext()
    val ctx7 =  new DummyMessageEventContext()
    val ctx8 =  new DummyMessageEventContext()
    val ctx9 =  new DummyMessageEventContext()
    val ctx10 =  new DummyMessageEventContext()

    pc1 ! CreatePlayerForContext(User("user11", "", "", 0L, None), ctx1)
    pc1 ! CreatePlayerForContext(User("user12", "", "", 0L, None), ctx2)
    pc1 ! CreatePlayerForContext(User("user13", "", "", 0L, None), ctx3)

    pc2 ! CreatePlayerForContext(User("user21", "", "", 0L, None), ctx4)
    pc2 ! CreatePlayerForContext(User("user22", "", "", 0L, None), ctx5)
    pc2 ! CreatePlayerForContext(User("user23", "", "", 0L, None), ctx6)
    pc2 ! CreatePlayerForContext(User("user24", "", "", 0L, None), ctx7)

    pc3 ! CreatePlayerForContext(User("user31", "", "", 0L, None), ctx8)

    pc4 ! CreatePlayerForContext(User("user41", "", "", 0L, None), ctx9)
    pc4 ! CreatePlayerForContext(User("user42", "", "", 0L, None), ctx10)

    Thread.sleep(5000)

    implicit val timeout = Timeout(5 seconds)
    val future = counter ? GetActors
    val result = Await.result(future, timeout.duration).asInstanceOf[List[ActorRef]]

    pc1 ! ContextDestroy(ctx1)

    Thread.sleep(5000)

    var resultSizeCondition = result.size

   /* dispatcher1 ! DispatchMessageBroadcast(ClusterMessageEvent(PlayerOnline(User("user55", "", "", 0L, None), pc4), "CardGamePlayer", ""))
    Thread.sleep(5000)    */

    var onlineListConditions: List[List[Any]] = List()

    try {
      result.map(h => {
        try {
        val fut = h ? DummyPlayerHandlerMessages
        val res = Await.result(fut, timeout.duration).asInstanceOf[List[Any]]

        onlineListConditions = res :: onlineListConditions
        } catch {

          case t: Throwable =>
        }
      })

    } finally {
      node1.shutdown()
      node2.shutdown()
      node3.shutdown()
      node4.shutdown()
    }

    Thread.sleep(5000)

    assert(resultSizeCondition == 10)
    assert(onlineListConditions.size == 9)

    onlineListConditions.map(s => {
      println("///size: " + s.size  )
      println("-------------------------------------------------: " + s)
      assert(s.size == 11)
    })
  }

  "PlayerController" should "pass CluesterEventMessage with RefreshFriendships(username) message to player handler when there is user with that name" in {


    val config = ConfigFactory.parseFile(new File("unittest-application.conf"))

    val pServiceFactory = new ServiceHandlersModuleFactoryImpl

    val node1 = ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeOne"))

    val counter = node1 actorOf Props[DummyPlayerHandlerCounter]

    val registry1 = node1 actorOf(Props[ClusterRegistry], "clusterRegistry")
    val dispatcher1 = node1 actorOf Props[ClusterDispatcher]
    val f1 = new DummyPlayerHandlerFactoryWithCounter(counter)
    val pc1 = node1 actorOf Props(classOf[PlayerController], dispatcher1, registry1, pServiceFactory, f1)
    val cb1 = node1 actorOf(Props(classOf[ClusterBalancer], dispatcher1))
    val cl1 = node1 actorOf(Props(classOf[ClusterListener], cb1), "clusterListener")

    val node2 = ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeTwo"))
    val registry2 = node2 actorOf(Props[ClusterRegistry], "clusterRegistry")
    val dispatcher2 = node2 actorOf Props[ClusterDispatcher]
    val f2 = new DummyPlayerHandlerFactoryWithCounter(counter)
    val pc2 = node2 actorOf Props(classOf[PlayerController], dispatcher2, registry2, pServiceFactory, f2)
    val cb2 = node2 actorOf(Props(classOf[ClusterBalancer], dispatcher2))
    val cl2 = node2 actorOf(Props(classOf[ClusterListener], cb2), "clusterListener")

    val node3 = ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeThree"))
    val registry3 = node3 actorOf(Props[ClusterRegistry], "clusterRegistry")
    val dispatcher3 = node3 actorOf Props[ClusterDispatcher]
    val f3 = new DummyPlayerHandlerFactoryWithCounter(counter)
    val pc3 = node3 actorOf Props(classOf[PlayerController], dispatcher3, registry3, pServiceFactory, f3)
    val cb3 = node3 actorOf(Props(classOf[ClusterBalancer], dispatcher3))
    val cl3 = node3 actorOf(Props(classOf[ClusterListener], cb3), "clusterListener")

    val node4 = ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeFor"))
    val registry4 = node4 actorOf(Props[ClusterRegistry], "clusterRegistry")
    val dispatcher4 = node4 actorOf Props[ClusterDispatcher]
    val f4 = new DummyPlayerHandlerFactoryWithCounter(counter)
    val pc4 = node4 actorOf Props(classOf[PlayerController], dispatcher4, registry4, pServiceFactory, f4)
    val cb4 = node4 actorOf(Props(classOf[ClusterBalancer], dispatcher4))
    val cl4 = node4 actorOf(Props(classOf[ClusterListener], cb4), "clusterListener")

    Thread.sleep(10000)

    pc1 ! CreatePlayerForContext(User("user11", "", "", 0L, None), new DummyMessageEventContext)
    pc1 ! CreatePlayerForContext(User("user12", "", "", 0L, None), new DummyMessageEventContext)
    pc1 ! CreatePlayerForContext(User("user13", "", "", 0L, None), new DummyMessageEventContext)

    pc2 ! CreatePlayerForContext(User("user21", "", "", 0L, None), new DummyMessageEventContext)
    pc2 ! CreatePlayerForContext(User("user22", "", "", 0L, None), new DummyMessageEventContext)
    pc2 ! CreatePlayerForContext(User("user23", "", "", 0L, None), new DummyMessageEventContext)
    pc2 ! CreatePlayerForContext(User("user24", "", "", 0L, None), new DummyMessageEventContext)

    pc3 ! CreatePlayerForContext(User("user31", "", "", 0L, None), new DummyMessageEventContext)

    pc4 ! CreatePlayerForContext(User("user41", "", "", 0L, None), new DummyMessageEventContext)
    pc4 ! CreatePlayerForContext(User("user42", "", "", 0L, None), new DummyMessageEventContext)

    Thread.sleep(5000)

    dispatcher1 ! DispatchMessageBroadcast(ClusterMessageEvent(RefreshFriendships("user21"), "CardGamePlayer", ""))
    dispatcher1 ! DispatchMessageBroadcast(ClusterMessageEvent(RefreshFriendships("user22"), "CardGamePlayer", ""))
    dispatcher1 ! DispatchMessageBroadcast(ClusterMessageEvent(RefreshFriendships("user23"), "CardGamePlayer", ""))
    dispatcher2 ! DispatchMessageBroadcast(ClusterMessageEvent(RefreshFriendships("user24"), "CardGamePlayer", ""))
    dispatcher2 ! DispatchMessageBroadcast(ClusterMessageEvent(RefreshFriendships("user11"), "CardGamePlayer", ""))
    dispatcher3 ! DispatchMessageBroadcast(ClusterMessageEvent(RefreshFriendships("user12"), "CardGamePlayer", ""))
    dispatcher3 ! DispatchMessageBroadcast(ClusterMessageEvent(RefreshFriendships("user13"), "CardGamePlayer", ""))
    dispatcher4 ! DispatchMessageBroadcast(ClusterMessageEvent(RefreshFriendships("user31"), "CardGamePlayer", ""))
    dispatcher4 ! DispatchMessageBroadcast(ClusterMessageEvent(RefreshFriendships("user41"), "CardGamePlayer", ""))
    dispatcher4 ! DispatchMessageBroadcast(ClusterMessageEvent(RefreshFriendships("user42"), "CardGamePlayer", ""))



    implicit val timeout = Timeout(5 seconds)
    val future = counter ? GetActors
    val result = Await.result(future, timeout.duration).asInstanceOf[List[ActorRef]]

    var resultSizeCondition = result.size

    Thread.sleep(5000)
    var onlineListConditions: List[List[Any]] = List()

    try {
      result.map(h => {
        val fut = h ? DummyPlayerHandlerMessages
        val res = Await.result(fut, timeout.duration).asInstanceOf[List[Any]]
        onlineListConditions = res :: onlineListConditions
      })
    } finally {
      node1.shutdown()
      node2.shutdown()
      node3.shutdown()
      node4.shutdown()
    }

    Thread.sleep(5000)

    assert(resultSizeCondition == 10)
    assert(onlineListConditions.size == 10)

    onlineListConditions.map(s => {
      println("///size: " + s.size  )
      println("-------------------------------------------------: " + s)
      assert(s.size == 11)
    })
  }
}
