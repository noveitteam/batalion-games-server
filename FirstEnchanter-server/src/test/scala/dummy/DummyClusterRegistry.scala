package dummy

import akka.actor.{Actor, ActorLogging}

/**
 * Created by Wlsek on 12.2.14.
 */



case object DummyClusterRegistryMessages
class DummyClusterRegistry extends Actor with ActorLogging  {
  var messages: List[Any] = List()
  def receive = {
    case DummyClusterRegistryMessages =>
      sender ! messages

    case t => messages = t :: messages
  }
}
