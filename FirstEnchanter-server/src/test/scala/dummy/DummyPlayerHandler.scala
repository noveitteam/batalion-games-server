package dummy

import akka.actor.{ActorRef, Actor, ActorLogging}
import cz.noveit.games.cardgame.engine.handlers.ServiceHandlersModuleFactory
import cz.noveit.proto.serialization.MessageEventContext
import cz.noveit.games.cardgame.adapters.User

/**
 * Created by Wlsek on 12.2.14.
 */

case object DummyPlayerHandlerMessages

class DummyPlayerHandler(user: User, ctx: MessageEventContext, playerController: ActorRef, serviceModule: ServiceHandlersModuleFactory) extends Actor with ActorLogging {
  var messages: List[Any] = List()

  def receive = {
    case DummyPlayerHandlerMessages =>
      sender ! messages
    case t =>
      messages = t :: messages
  }
}


case class Register(actor: ActorRef)

class DummyPlayerHandlerCounting(user: User, ctx: MessageEventContext, playerController: ActorRef, serviceModule: ServiceHandlersModuleFactory, counter: ActorRef) extends Actor with ActorLogging {
  var messages: List[Any] = List()
  counter ! Register(self)

  def receive = {
    case DummyPlayerHandlerMessages =>
      println("dummy messages!: " + messages.size)
      sender ! messages
    case t =>
      messages = t :: messages
  }
}

case object GetActors

class DummyPlayerHandlerCounter extends Actor with ActorLogging {
  var actors: List[ActorRef] = List()

  def receive = {
    case Register(actor) =>
      actors = actor :: actors
    case GetActors =>
      sender ! actors

  }
}