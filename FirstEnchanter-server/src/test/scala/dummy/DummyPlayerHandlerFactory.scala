package dummy

import akka.actor.{Props, ActorContext, ActorRef}
import cz.noveit.games.cardgame.engine.PlayerHandlerFactory
import cz.noveit.games.cardgame.adapters.User
import cz.noveit.proto.serialization.MessageEventContext
import cz.noveit.games.cardgame.engine.handlers.ServiceHandlersModuleFactory
import scala.collection.immutable.HashMap

/**
 * Created by Wlsek on 12.2.14.
 */

case class DummyPlayerHandlerFactoryPair(user: User, handler: ActorRef)

class DummyPlayerHandlerFactory() extends PlayerHandlerFactory {

  def playerHandler(actorContext: ActorContext, user: User, messageEventContext: MessageEventContext, parent: ActorRef, serviceHandlersFactory: ServiceHandlersModuleFactory, databaseParameters: HashMap[String, String]): ActorRef = {
    val actor = actorContext.actorOf(Props(classOf[DummyPlayerHandler], user, messageEventContext, parent, serviceHandlersFactory))
    actor
  }
}

class DummyPlayerHandlerFactoryWithCounter(counter: ActorRef) extends PlayerHandlerFactory {

  def playerHandler(actorContext: ActorContext, user: User, messageEventContext: MessageEventContext, parent: ActorRef, serviceHandlersFactory: ServiceHandlersModuleFactory,  databaseParameters: HashMap[String, String]): ActorRef = {
    val actor = actorContext.actorOf(Props(classOf[DummyPlayerHandlerCounting], user, messageEventContext, parent, serviceHandlersFactory, counter: ActorRef))
    actor
  }
}


