package dummy.game

import akka.actor.{ActorLogging, ActorRef, Actor}
import cz.noveit.games.cardgame.adapters.{DeckAndCardsForUserWithGivenDeckId, GetDeckAndCardsForUserWithGivenDeckId, Card, Deck}
import cz.noveit.database.{DatabaseResult, DatabaseCommand}

/**
 * Created by Wlsek on 26.2.14.
 */

case class DummyCardDatabaseRow(player: String, decks: List[DummyCardDatabaseDeck])

case class DummyCardDatabaseDeck(deck: Deck, cards: List[Card])

class DummyCardDatabaseHandler(rows: List[DummyCardDatabaseRow]) extends Actor with ActorLogging {
  def receive = {
    case msg: DatabaseCommand => {
      msg.command match {
        case GetDeckAndCardsForUserWithGivenDeckId(user, deckId) => {
          sender ! DatabaseResult(rows.find(r => r.player.equals(user)).map(r => {
            r.decks.find(d => d.deck.id.equals(deckId)).map(d => {
              DeckAndCardsForUserWithGivenDeckId(Some(d.deck), d.cards, user, deckId)
            }).getOrElse(DeckAndCardsForUserWithGivenDeckId(None, List(), user, deckId))
          }).getOrElse(DeckAndCardsForUserWithGivenDeckId(None, List(), user, deckId)), "")
        }
      }
    }
  }
}
