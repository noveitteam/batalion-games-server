package dummy.game

import akka.actor.{ActorLogging, ActorRef, Actor}
import cz.noveit.games.cardgame.engine.handlers.GetCardDatabaseAdapter
import cz.noveit.games.cardgame.engine.game._
import scala.util.Random
import cz.noveit.games.cardgame.engine.game.PlayerRoll
import cz.noveit.games.cardgame.engine.game.StateAsk
import cz.noveit.games.cardgame.engine.game.AskPlayerRoll
import cz.noveit.games.cardgame.engine.game.StateAck

/**
 * Created by Wlsek on 26.2.14.
 */

case object GetMessages

class DummyPlayerHandler(cardAdapter: ActorRef) extends Actor with ActorLogging {
  var messages: List[Any] = List()

  def receive = {
    case GetCardDatabaseAdapter =>
      sender ! cardAdapter
      messages = GetCardDatabaseAdapter :: messages

    case GetMessages =>
      sender ! messages

    case t => messages = t :: messages
  }
}

class FullDummyPlayerHandler(cardAdapter: ActorRef, playerName: String, reporter: ActorRef) extends Actor with ActorLogging {
  val random = new Random()
  var messages: List[Any] = List()

  def receive = {
    case GetCardDatabaseAdapter =>
      sender ! cardAdapter
      messages = GetCardDatabaseAdapter :: messages


    case msg: StateAsk =>
      Thread.sleep(200)
      sender ! StateAck(msg.state, playerName)
      messages = msg :: messages

    case msg: AskPlayerRoll =>
      val roll = PlayerRoll(playerName, random.nextInt(6) + 1, msg.position)
      sender ! roll
      messages = msg :: messages

      reporter ! IRolled(playerName, roll.number)

    case AskAttackerToUseCard     =>
      sender !  AttackerEndsTurn(playerName)
      messages = AskAttackerToUseCard :: messages
      reporter ! IHaveToUseCard(playerName)

    case AskAttackerToAttack =>
      sender !  AttackerEndsTurn(playerName)
      messages = AskAttackerToUseCard :: messages
      reporter ! IHaveToAttack(playerName)

    case AskDefenderToUseCard =>
      sender !  DefenderEndsTurn(playerName)
      messages = AskDefenderToUseCard :: messages
      reporter ! IHaveToCounter(playerName)

    case AskDefenderToWait =>
      messages = AskDefenderToWait :: messages
      reporter ! IHaveToWaitForAttackerToUseCard(playerName)

    case AskDefenderToWaitForAttack =>
      messages = AskDefenderToWaitForAttack :: messages
      reporter ! IHaveToWaitForAttack(playerName)

    case AskAttackerToWait =>
      messages = AskAttackerToWait :: messages
      reporter ! IHaveToWaitForCounter(playerName)

    case GetMessages =>
      sender ! messages

    case t => messages = t :: messages
  }


}