package dummy.game

import akka.actor.{ActorLogging, Actor, ActorRef}

/**
 * Created by Wlsek on 26.2.14.
 */


case class IRolled(userName: String, roll: Int)

case class IHaveToUseCard(userName: String)

case class IHaveToWaitForAttackerToUseCard(userName: String)

case class IHaveToCounter(userName: String)

case class IHaveToWaitForCounter(userName: String)

case class IHaveToAttack(userName: String)

case class IHaveToWaitForAttack(userName: String)


case class OneTurn(
                    rolled: List[IRolled],
                    useCard: List[IHaveToUseCard],
                    waitForCard: List[IHaveToWaitForAttackerToUseCard],
                    useCounterCard: List[IHaveToCounter],
                    waitForCounterCard: List[IHaveToWaitForCounter],
                    attack: List[IHaveToAttack],
                    waitForAttack: List[IHaveToWaitForAttack]
                    )

class GameReporter(numberOfPlayers: Int) extends Actor with ActorLogging {

  var afterStartRoll = true
  var actualTurn: Option[OneTurn] = None
  var turns: List[OneTurn] = List()

  def receive = {
    case msg: IRolled =>

      prepareTurn
      testForRolled

      actualTurn.map(t => {
        actualTurn = Some(t.copy(rolled = msg :: t.rolled))
      })

    case msg: IHaveToUseCard =>
      prepareTurn

      actualTurn.map(t => {
        actualTurn = Some(t.copy(useCard = msg :: t.useCard))
      })

    case msg: IHaveToWaitForAttackerToUseCard =>
      prepareTurn

      actualTurn.map(t => {
        actualTurn = Some(t.copy(waitForCard = msg :: t.waitForCard))
      })

    case msg: IHaveToCounter =>
      prepareTurn

      actualTurn.map(t => {
        actualTurn = Some(t.copy(useCounterCard = msg :: t.useCounterCard))
      })


    case msg: IHaveToWaitForCounter =>
      prepareTurn

      actualTurn.map(t => {
        actualTurn = Some(t.copy(waitForCounterCard = msg :: t.waitForCounterCard))
      })

    case msg: IHaveToAttack =>
      prepareTurn

      actualTurn.map(t => {
        actualTurn = Some(t.copy(attack = msg :: t.attack))
      })


    case msg: IHaveToWaitForAttack =>
      prepareTurn

      actualTurn.map(t => {
        actualTurn = Some(t.copy(waitForAttack = msg :: t.waitForAttack))
      })

  }

  def prepareTurn = {
    if (actualTurn.isEmpty) {
      actualTurn = Some(OneTurn(List(), List(), List(), List(), List(), List(), List()))
    }
  }

  def testForRolled = {
    if (actualTurn.get.rolled.size == numberOfPlayers-1 && afterStartRoll) {
      afterStartRoll = false
    } else if (actualTurn.get.rolled.size == numberOfPlayers) {
      turns = actualTurn.get :: turns
      actualTurn = Some(OneTurn(List(), List(), List(), List(), List(), List(), List()))
    }
  }

}
