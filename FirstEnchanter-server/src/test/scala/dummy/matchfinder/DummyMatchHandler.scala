package dummy.matchfinder

import akka.actor.{ActorRef, ActorLogging, Actor}
import cz.noveit.games.cardgame.engine.{CrossHandlerMessage, ServiceModuleMessage}
import cz.noveit.games.cardgame.engine.game._
import cz.noveit.games.cardgame.engine.game.AskReady
import cz.noveit.games.cardgame.engine.game.PlayerDeclined
import cz.noveit.games.cardgame.engine.ServiceModuleMessage
import cz.noveit.games.cardgame.engine.game.PlayerReady
import cz.noveit.games.cardgame.engine.game.FindMatch

/**
 * Created by Wlsek on 4.3.14.
 */
class DummyMatchHandlerOk(playerName: String, matchController: ActorRef) extends Actor with ActorLogging {

  var messages: List[CrossHandlerMessage] = List()

  def receive = {
    case ServiceModuleMessage(message, module, replyHash) =>
      message match {
        case m: AskReady =>
          m.replyTo ! PlayerReady(playerName, "1")
          messages = m :: messages

        case RetryFindMatch(query) =>
          matchController ! FindMatch(query)

        case t => messages = t :: messages
      }
  }
}

class DummyMatchHandlerFail(playerName: String, matchController: ActorRef) extends Actor with ActorLogging {

  var messages: List[CrossHandlerMessage] = List()

  def receive = {
    case ServiceModuleMessage(message, module, replyHash) =>
      message match {
        case m: AskReady =>
          m.replyTo ! PlayerDeclined(playerName)
          messages = m :: messages

        case RetryFindMatch(query) =>
          matchController ! FindMatch(query)

        case t => messages = t :: messages
      }
  }
}
