package cz.noveit.games.cluster


import _root_.dispatch._
import com.typesafe.config.{Config, ConfigFactory}
import akka.actor.{Props, Actor, ActorSystem}
import java.io.File
import cz.noveit.games.cardgame.World

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}
import scala.util.parsing.json.JSON
import scala.collection.immutable.HashMap

/**
 * Created by Wlsek on 28.1.14.
 */
object TestGate extends App  {

  case class NodeConfigPair(system: ActorSystem, config: Config)

  override def main(args: Array[String]): Unit = {
   // val config = ConfigFactory.parseFile(new File("test-application.conf"))
      /*
    val nodes = List(
      NodeConfigPair(ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeOne")), config.getConfig("systemSeedNodeOne")),
      NodeConfigPair(ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeTwo")), config.getConfig("systemSeedNodeTwo")),
      NodeConfigPair(ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeThree")), config.getConfig("systemSeedNodeThree")),
      NodeConfigPair(ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeFor")), config.getConfig("systemSeedNodeFor"))
    )

    val worlds = nodes.map(node => new World()(node.system, node.config)).toList

    Console.readLine()

    worlds.map(world => world.destroy)
    nodes.map(node => node.system.shutdown())   */
    val axs = ActorSystem("Yeep")
    axs.actorOf(Props[TestActor])

    try {
        val svc = url("https://sandbox.itunes.apple.com/verifyReceipt").POST
        val receipt = Http(svc OK as.String)
        val result = JSON.parseFull(receipt())
       result match  {
         case Some(m: Map[String, Any]) => println(m)
         case None =>
       }
    } catch {
      case t: Throwable => println("Error: " + t)
    }

    Console.readLine()
    axs.shutdown()
  }
}

class TestActor extends Actor {
  try {      /*
  val future = Post("https://sandbox.itunes.apple.com/verifyReceipt","") ~> sendReceive
  future onSuccess {
    case s => println("success")
  }

  future onFailure {
    case f => println("failed")
  } */
  } catch {
    case t: Throwable => println(t)
  }

  def receive = {
    case t =>
  }
}





