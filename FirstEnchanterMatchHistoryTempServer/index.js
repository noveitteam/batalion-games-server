
var http = require('http');
var url = require('url')
var fs = require('fs');
var express = require('express');
var app = express()

var server = app.listen(8181, function() {
    console.log('Listening on port %d', server.address().port);
});

app.get('/', function(req, res) {

    if (req.query.xmlFile) {

        try {

            var filePath = '../matches/' + req.query.xmlFile
            console.log(filePath)
            var fileData = fs.readFileSync(filePath, 'UTF-8');

            res.contentType("application/json");
            res.send(req.param("callback") + "({\"xml\":\"" + new Buffer(fileData.trim()).toString('base64') + "\"})"); //)+ ")"({xml" : '"' + + '"})'})

        } catch (exception) {
            console.log(exception)
            res.status(500);
            res.contentType("application/json");
            res.send()
        }
    } else {
        res.contentType("application/json");
        var files = fs.readdirSync('../matches/').map(function(v) {
            return {name: v,
                time: fs.statSync('../matches/' + v).mtime.getTime()
            };
        })
                .sort(function(a, b) {
            return  b.time - a.time;
        })
                .map(function(v) {
            return v.name;
        });
        res.send(req.query.callback + "(" + JSON.stringify(files) + ")");
    }

});
