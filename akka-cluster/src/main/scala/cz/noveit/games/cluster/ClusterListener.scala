package cz.noveit.games.cluster

import akka.cluster.{Member, MemberStatus, Cluster}
import akka.cluster.ClusterEvent._

import cz.noveit.games.cluster.balancer.{Metrics, RemoveMember, AddMember, MembersSnapshot }
import akka.actor.{Actor, ActorLogging, ActorRef}

/**
 * Created by Wlsek on 29.1.14.
 */



class ClusterListener(balance: ActorRef) extends Actor with ActorLogging {

  val cluster = Cluster(context.system)

  override def preStart(): Unit = {
    cluster.subscribe(self, classOf[ClusterDomainEvent])
  }

  override def postStop(): Unit = cluster.unsubscribe(self)

  def receive = {
    case state: CurrentClusterState =>
      log.info("Yay! Got snapshot from cluster with {} nodes", state.members.size.toString)
      balance ! MembersSnapshot(state.members)

    case MemberUp(member) =>
      log.info("Hey, i recognize new member! [{}]", member.address)
      balance ! AddMember(member)

    case UnreachableMember(member) =>
      log.info("No, i lost contact to member! [{}]", member.address)
      balance ! RemoveMember(member)

    case ReachableMember(member) =>
      log.info("Yes, i have contact to member! [{}]", member.address)
      balance ! AddMember(member)

    case MemberRemoved(member, previousStatus) =>
      log.info("I removed member [{}] after state [{}]", member.address, previousStatus.toString)
      balance ! RemoveMember(member)

    case MemberExited(member) =>
      log.info("No! Can't be happening, [{}] left cluster", member.address)
      balance ! RemoveMember(member)

    case ClusterMetricsChanged(metrics) =>
      log.info("Got sweet metrics, proccessing them.")
      balance ! Metrics(metrics)

    case msg: CustomMetrics =>
      log.info("Got custom metrics, proccessing them." )
      balance ! msg

  }

}
