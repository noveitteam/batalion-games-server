package cz.noveit.games.cluster.balancer

import akka.actor.{ActorRef, Actor, ActorLogging}
import akka.cluster.{NodeMetrics, Member}
import scala.collection.SortedSet
import akka.cluster.StandardMetrics.{Cpu, HeapMemory}
import cz.noveit.games.cluster.dispatch.OrderedMembers

/**
 * Created by Wlsek on 29.1.14.
 */

case class MembersSnapshot(members: Set[Member])
case class AddMember(member: Member)
case class RemoveMember(member: Member)
case class Metrics(metrics: Set[NodeMetrics])

case object GetBestMember
case object GetAllMembers

class ClusterBalancer(dispatcher: ActorRef) extends Actor with ActorLogging {
      case class MetricPair(member: Member, usage: Float)
      var nodes: Set[Member] = Set.empty[Member]

      def receive = {
        case MembersSnapshot(members) =>
          members.map(m => nodes += m)
          dispatcher ! OrderedMembers(nodes)

        case AddMember(member) =>
          nodes += member
          dispatcher ! OrderedMembers(nodes)

        case RemoveMember(member) =>
          nodes -= member
          dispatcher ! OrderedMembers(nodes)

        case Metrics(metrics) =>
           var metricsNodes: List[MetricPair] = List()

           nodes.map(n => {

             metricsNodes = metrics.find(m => m.address.equals(n.address)).map(m => {
               var metricPair =  MetricPair(n, 1)
                m match {
                  case HeapMemory(address, timestamp, used, committed, max) =>
                    metricPair = max
                       .map(maxMem => MetricPair(n, (maxMem - used) /  maxMem.toFloat) )
                       .getOrElse(MetricPair(n,((used + committed) - used) / (used + committed).toFloat))

               }
               metricPair
             }).getOrElse(MetricPair(n, 1)) :: metricsNodes
           })
         nodes = metricsNodes.sortBy(pair => pair.usage).map(pair => pair.member).toSet[Member]
         dispatcher ! OrderedMembers(nodes)
      }
}
