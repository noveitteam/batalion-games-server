package cz.noveit.games.cluster.dispatch

import akka.actor.{RootActorPath, ActorRef, Actor, ActorLogging}
import scala.concurrent.Await

import akka.pattern.ask
import scala.concurrent.duration.Duration
import scala.collection.SortedSet
import akka.cluster.{Cluster, Member}
import cz.noveit.games.cluster.{CustomMetrics}
import sun.reflect.generics.reflectiveObjects.NotImplementedException

/**
 * Created by Wlsek on 28.1.14.
 */

case class OrderedMembers(members: Set[Member])

case class DispatchMessageBalanced(e: ClusterMessageEvent)
case class DispatchMessageBroadcast(e: ClusterMessageEvent)
case class DispatchServiceBroadcast(e: ClusterServiceEvent)
case class DispatchMessageBroadcastWithoutMe(e: ClusterMessageEvent)

case class DispatchCustomMetrics(e: CustomMetrics)



case object GetMembers


class ClusterDispatcher extends Actor with ActorLogging  {

    var nodes: Set[Member] = Set.empty[Member]

  val cluster = Cluster.get(context.system)


    def receive = {
      case msg: OrderedMembers =>
       // println(msg)
        nodes = msg.members

      case msg: DispatchMessageBalanced =>
        context.actorSelection(RootActorPath(nodes.head.address) / "user" / "clusterRegistry") ! msg.e

      case msg: DispatchMessageBroadcast =>
        log.info("Dispatching broadcast message to " + nodes.size + " nodes." )
        nodes.map(n => {
          context.actorSelection(RootActorPath(n.address) / "user" / "clusterRegistry") ! msg.e
        })

      case msg: DispatchServiceBroadcast =>
        log.info("Dispatching service broadcast message to " + nodes.size + " nodes." )
        nodes.map(n => {
          context.actorSelection(RootActorPath(n.address) / "user" / "clusterRegistry") ! msg.e
        })

      case msg: DispatchMessageBroadcastWithoutMe =>
        log.info("Dispatching broadcast message to " + nodes.size + " nodes." )
        nodes.filterNot(n => n.address.equals(cluster.selfAddress)).map(n => {
          context.actorSelection(RootActorPath(n.address) / "user" / "clusterRegistry") ! msg.e
        })

      case DispatchCustomMetrics(metrics) =>
        nodes.map(n => context.actorSelection(RootActorPath(n.address) / "user" / "clusterListener") ! metrics)

      case GetMembers =>
        sender ! nodes

      case t => throw new UnsupportedOperationException("Unknown message: " + t.toString)
    }
}

