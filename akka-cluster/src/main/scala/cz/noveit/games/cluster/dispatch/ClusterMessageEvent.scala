package cz.noveit.games.cluster.dispatch

import java.math.BigInteger
import java.security.SecureRandom
import akka.actor.ActorRef

/**
 * Created by Wlsek on 28.1.14.
 */
trait ClusterMessage
case class ClusterMessageEvent (message: ClusterMessage, moduleName: String, replyHash: String = "")
case class ClusterMessageEventWithReplier (message: ClusterMessage, replyTo: ActorRef, replyHash: String = "")
case class ClusterServiceEvent(message: ClusterMessage, service: String)

trait ReplyHasCreator {
  val random = new SecureRandom();

  def createReplyHash {
    new BigInteger(130, random).toString(32);
  }
}