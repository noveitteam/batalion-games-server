package cz.noveit.games.cluster.registry

import akka.actor.{ActorRef, Actor, ActorLogging}
import cz.noveit.games.cluster.dispatch.{ClusterServiceEvent, ClusterMessageEvent}

/**
 * Created by Wlsek on 29.1.14.
 */



case class SubscribeActorForModuleName(actor: ActorRef, moduleName: String)
case class UnSubscribeActor(actor: ActorRef)
case class UnSubscribeModule(moduleName: String)

class ClusterRegistry  extends Actor with ActorLogging  {

  case class RegistryPair(module: String, actor: ActorRef)
  var listeners: List[RegistryPair] = List()

  def receive = {
    case SubscribeActorForModuleName(actor, module) =>
      if(listeners.find(p => p.module.equals(module)).isEmpty) listeners = RegistryPair(module, actor) :: listeners

    case UnSubscribeActor(actor) =>
      listeners = listeners.filterNot(pair => pair.actor.equals(actor))

    case UnSubscribeModule(module) =>
      listeners = listeners.filterNot(pair => pair.module.equals(module))

    case e: ClusterMessageEvent =>
      listeners.find(pair => pair.module.equals(e.moduleName)).map(pair => pair.actor ! e)

    case e: ClusterServiceEvent =>
      context.actorSelection("/user/" + e.service) ! e

    case t => throw new UnsupportedOperationException("Unknown message: " + t)

  }
}
