import akka.actor.{Props, ActorSystem}
import akka.cluster.Member
import com.typesafe.config.ConfigFactory
import cz.noveit.games.cluster.balancer.ClusterBalancer
import cz.noveit.games.cluster.ClusterListener
import cz.noveit.games.cluster.dispatch.{GetMembers, ClusterDispatcher}
import cz.noveit.games.cluster.registry.ClusterRegistry
import java.io.File
import org.scalatest.FlatSpec
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import scala.concurrent.Await
import scala.Predef._


/**
 * Created by Wlsek on 12.2.14.
 */

class ClusterTests extends FlatSpec {
  "Cluster" should "consist of 4 members" in {

    val config = ConfigFactory.parseFile(new File("unittest-application.conf"))

    val node1 = ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeOne"))
    val registry1 = node1 actorOf(Props[ClusterRegistry], "clusterRegistry")
    val dispatcher1 = node1 actorOf Props[ClusterDispatcher]
    val cb1 = node1 actorOf(Props(classOf[ClusterBalancer], dispatcher1))
    val cl1 = node1 actorOf(Props(classOf[ClusterListener], cb1), "clusterListener")

    val node2 = ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeTwo"))
    val registry2 = node2 actorOf(Props[ClusterRegistry], "clusterRegistry")
    val dispatcher2 = node2 actorOf Props[ClusterDispatcher]
    val cb2 = node2 actorOf(Props(classOf[ClusterBalancer], dispatcher2))
    val cl2 = node2 actorOf(Props(classOf[ClusterListener], cb2), "clusterListener")

    val node3 = ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeThree"))
    val registry3 = node3 actorOf(Props[ClusterRegistry], "clusterRegistry")
    val dispatcher3 = node3 actorOf Props[ClusterDispatcher]
    val cb3 = node3 actorOf(Props(classOf[ClusterBalancer], dispatcher3))
    val cl3 = node3 actorOf(Props(classOf[ClusterListener], cb3), "clusterListener")

    val node4 = ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeFor"))
    val registry4 = node4 actorOf(Props[ClusterRegistry], "clusterRegistry")
    val dispatcher4 = node4 actorOf Props[ClusterDispatcher]
    val cb4 = node4 actorOf(Props(classOf[ClusterBalancer], dispatcher4))
    val cl4 = node4 actorOf(Props(classOf[ClusterListener], cb4), "clusterListener")

     Thread.sleep(10000)

    implicit val timeout = Timeout(5 seconds)
    val future1 = dispatcher1 ? GetMembers
    val result1 = Await.result(future1, timeout.duration).asInstanceOf[Set[Member]]

    val future2 = dispatcher2 ? GetMembers
    val result2 = Await.result(future2, timeout.duration).asInstanceOf[Set[Member]]

    val future3 = dispatcher3 ? GetMembers
    val result3 = Await.result(future3, timeout.duration).asInstanceOf[Set[Member]]

    val future4 = dispatcher4 ? GetMembers
    val result4 = Await.result(future4, timeout.duration).asInstanceOf[Set[Member]]

    node1.shutdown()
    node2.shutdown()
    node3.shutdown()
    node4.shutdown()

    println(result1)
    println(result2)
    println(result3)
    println(result4)

    assert(result1.size == 4)
    assert(result2.size == 4)
    assert(result3.size == 4)
    assert(result4.size == 4)

  }
}