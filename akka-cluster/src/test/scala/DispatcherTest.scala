import akka.actor.{Props, ActorSystem}
import com.typesafe.config.ConfigFactory
import cz.noveit.games.cluster.balancer.ClusterBalancer
import cz.noveit.games.cluster.ClusterListener
import cz.noveit.games.cluster.dispatch.{ClusterMessageEvent, DispatchMessageBroadcast, ClusterDispatcher}
import cz.noveit.games.cluster.registry.ClusterRegistry
import dummy.{GetDummyMessages, DummyMessage, DummyService}
import java.io.File
import org.scalatest.FlatSpec
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import scala.concurrent.Await

/**
 * Created by Wlsek on 13.2.14.
 */
class DispatcherTest extends FlatSpec {
  "ClusterDispatcher" should "dispatch broadcast message" in {

    val config = ConfigFactory.parseFile(new File("unittest-application.conf"))

    val node1 = ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeOne"))
    val registry1 = node1 actorOf(Props[ClusterRegistry], "clusterRegistry")
    val dispatcher1 = node1 actorOf Props[ClusterDispatcher]
    val cb1 = node1 actorOf(Props(classOf[ClusterBalancer], dispatcher1))
    val cl1 = node1 actorOf(Props(classOf[ClusterListener], cb1), "clusterListener")
    val ds1 = node1 actorOf(Props(classOf[DummyService], dispatcher1, registry1))

    val node2 = ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeTwo"))
    val registry2 = node2 actorOf(Props[ClusterRegistry], "clusterRegistry")
    val dispatcher2 = node2 actorOf Props[ClusterDispatcher]
    val cb2 = node2 actorOf(Props(classOf[ClusterBalancer], dispatcher2))
    val cl2 = node2 actorOf(Props(classOf[ClusterListener], cb2), "clusterListener")
    val ds2 = node2 actorOf(Props(classOf[DummyService], dispatcher2, registry2))

    val node3 = ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeThree"))
    val registry3 = node3 actorOf(Props[ClusterRegistry], "clusterRegistry")
    val dispatcher3 = node3 actorOf Props[ClusterDispatcher]
    val cb3 = node3 actorOf(Props(classOf[ClusterBalancer], dispatcher3))
    val cl3 = node3 actorOf(Props(classOf[ClusterListener], cb3), "clusterListener")
    val ds3 = node3 actorOf(Props(classOf[DummyService], dispatcher3, registry3))

    val node4 = ActorSystem("CardGameCluster", config.getConfig("systemSeedNodeFor"))
    val registry4 = node4 actorOf(Props[ClusterRegistry], "clusterRegistry")
    val dispatcher4 = node4 actorOf Props[ClusterDispatcher]
    val cb4 = node4 actorOf(Props(classOf[ClusterBalancer], dispatcher4))
    val cl4 = node4 actorOf(Props(classOf[ClusterListener], cb4), "clusterListener")
    val ds4 = node4 actorOf(Props(classOf[DummyService], dispatcher4, registry4))

    Thread.sleep(10000)

    dispatcher1 ! DispatchMessageBroadcast(ClusterMessageEvent(DummyMessage("dummieeeee!"), "DummyService", ""))

    Thread.sleep(500)

    implicit val timeout = Timeout(5 seconds)
    val future1 = ds1 ? GetDummyMessages
    val result1 = Await.result(future1, timeout.duration).asInstanceOf[List[Any]]

    val future2 = ds2 ? GetDummyMessages
    val result2 = Await.result(future2, timeout.duration).asInstanceOf[List[Any]]

    val future3 = ds3 ? GetDummyMessages
    val result3 = Await.result(future3, timeout.duration).asInstanceOf[List[Any]]

    val future4 = ds4 ? GetDummyMessages
    val result4 = Await.result(future4, timeout.duration).asInstanceOf[List[Any]]

    node1.shutdown()
    node2.shutdown()
    node3.shutdown()
    node4.shutdown()

    assert(result1.size == 1)
    assert(result2.size == 1)
    assert(result3.size == 1)
    assert(result4.size == 1)



  }
}