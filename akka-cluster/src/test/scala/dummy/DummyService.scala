package dummy

import akka.actor.{ActorRef, Actor, ActorLogging}
import cz.noveit.games.cluster.registry.SubscribeActorForModuleName
import cz.noveit.games.cluster.dispatch.ClusterMessage

/**
 * Created by Wlsek on 13.2.14.
 */

case class DummyMessage(dummy: String) extends ClusterMessage
case object GetDummyMessages
class DummyService(dispatcher: ActorRef, registry: ActorRef) extends Actor with ActorLogging {
    val moduleName = "DummyService"
    registry ! SubscribeActorForModuleName(self, moduleName)

    var messages: List[Any] = List()

    def receive = {
      case GetDummyMessages =>
        sender ! messages

      case t =>
        messages = t :: messages
    }
}
