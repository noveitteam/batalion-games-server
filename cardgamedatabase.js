var testUsers = [
  {
   "userName" : "dan",
   "password" : "56b1db8133d9eb398aabd376f07bf8ab5fc584ea0b8bd6a1770200cb613ca005",
   "email" : "hawuawu@gmail.com",
   "lastLogged" : NumberLong(0),
   "inGameMoney" : NumberLong(0),
   "microTransactionMoney"  : NumberLong(0),
   "emailConfirmed" : false,
   "startingDeckSelected" : false
  },
   {
     "userName" : "jan",
     "password" : "56b1db8133d9eb398aabd376f07bf8ab5fc584ea0b8bd6a1770200cb613ca005",
     "email" : "wlsek@atlas.cz",
     "lastLogged" : NumberLong(0) ,
     "inGameMoney" : NumberLong(0),
      "microTransactionMoney"  : NumberLong(0),
      "emailConfirmed" : false,
      "startingDeckSelected" : false
    },
     {
       "userName" : "petr",
       "password" : "56b1db8133d9eb398aabd376f07bf8ab5fc584ea0b8bd6a1770200cb613ca005",
       "email" : "novakd11@gmail.com",
       "lastLogged" : NumberLong(0),
       "inGameMoney" : NumberLong(0),
       "microTransactionMoney"  : NumberLong(0),
        "emailConfirmed" : false,
        "startingDeckSelected" : false
      }
]

var cards = [
        {
            "_id" : ""
            "name" : "",
            "description" : [
                {"locale" : "" , "description" : ""}
            ],
            "attack" : 1,
            "defense" : 1,
            "version" : "1",
            "rarity" : 0, //1,2,3,4..
            "price" : [
                {"element" : 0, "price" : 5}, //water
                {"element" : 1, "price" : 5}, //fire
                {"element" : 2, "price" : 5}, //air
                {"element" : 3, "price" : 5}    //earth
            ],
            "tags"  : ["a", "b", "c", "d"],
            "imageId" : 0
        }
]

var userUnlockedUnlockAbles = [
  {
    "user" : "",
    "unlockedAvatars"   : [1,2,3,4,5,6,7,8,9],
    "unlockedSkins"     : [1,2,3,4,5,6,7,8,9],
    "unlockedDeckIcons" : [1,2,3,4,5,6,7,8,9]
  }
]



var userHasCards = [
    {
        "user" : "",
        "cards" : ["c1", "c2"]
    }
]

var skinVersions = [
    {
        "skinId" : 0,
        "version" : "1",
        "device"  : 0,
        "unlockable" = false
    }
]

var deckIconVersions = [
    {
        "iconId" : 0,
        "version" : "1",
         "device"  : 0,
        "unlockable" = false
    }
]

var avatarsVersion = [
    {
        "avatarId" : 0,
        "version" : "1",
        "device"  : 0,
        "unlockable" = false
    }
]

var decks = [
    {
        "user" : "",
        "name" : "deckName",
        "cards" : ["id1", "id2"],
        "elementsRating" : {
            "fire" : 0.0,
            "earth" : 0.0,
            "water" : 0.0,
            "air" : 0.0
        } ,
        "iconId" : 1,
        "setSkins" : [{"cardId" : "id1", "skinId" : 0}]
    }
]

var userStats = [
    {
        "user" : "",
        "mmrating" : NumberInt(0),
        "practice" : {
          "1v1" : {
            "pve" : {"games" : 0, "win" : 0, "loss" : 0},
            "pvp" : {"games" : 0, "win" : 0, "loss" : 0}
          },
          "2v2" : {
             "pve" : {"games" : 0, "win" : 0, "loss" : 0},
             "pvp" : {"games" : 0, "win" : 0, "loss" : 0}
          }
        },
        "ranked" : {
           "1v1" :  {"games" : 0, "win" : 0, "loss" : 0},
           "2v2" :  {"games" : 0, "win" : 0, "loss" : 0}
        }
    }
]

var matchResults = [
    {
        "gameType"      : 0, //ranked, practice
        "enemyType"     : 0, //pvp, pve
        "teamType"      : 0, //1v1, 2v2
        "resultType"    : 0, //standard, teamSurrender
        "losers"        : ["a", "b"],
        "winners"       : ["c", "d"],
        "duration"      : NumberLong(0),
        "started"       : NumberLong(0)
    },
      {
            "gameType"      : 0, //ranked, practice
            "enemyType"     : 0, //pvp, pve
            "teamType"      : 0, //1v1, 2v2
            "resultType"    : 0, //standard, teamSurrender
            "losers"        : ["c", "d"],
            "winners"       : ["a", "b"],
            "duration"      : NumberLong(0),
            "started"       : NumberLong(0)
        },
        {
                    "gameType"      : 0, //ranked, practice
                    "enemyType"     : 0, //pvp, pve
                    "teamType"      : 0, //1v1, 2v2
                    "resultType"    : 0, //standard, teamSurrender
                    "losers"        : ["c", "d"],
                    "winners"       : ["1", "b"],
                    "duration"      : NumberLong(0),
                    "started"       : NumberLong(0)
                }
]

var friendships = [
    {
        "users" : ["dan", "petr"],
        "state" : 0
    }

]

/* db.ScoreBoard.update({"name": "silver"},{"$pull" : {"players" : {"user" : "petr"}}, "$inc" : {"size" : -1}});*/


/*  db.ScoreBoard.update({"name": "silver"},{"$pull" : {"players" : {"user" : "mana"}}});*/
/* db.ScoreBoard.update({"name": "silver", "players.user" : "dan"},{"$unset" : {"players.$.transaction" : ""}});*/
/*db.ScoreBoard.update({"name": "silver", "players.user":"mana"},{"$set" : {"players.$.transaction" : 1}});*/

var scoreboard = [
    {
     {
        "name"      : "",
        "created"   : NumberLong(0),
        "level"     : 0,
        "size"      : 0,
        "players"   : [
            {"user" : "", "score" : 0, "rating" -> 0}
        ]
     }
    }
]

var transactions = [
    {
        "value" : NumberInt(0),
        "currency" : "",
        "details" : "",
        "time" : NumberLong(0)
    }
]


//UnlockAbleTypeCardSkin, UnlockAbleTypeAvatar, UnlockAbleTypeDeckIcon, UnlockAbleTypeBooster, UnlockAbleTypeDeck
val unlockAbles = {
    {
        "unlockAbleId" : "cz.blabla.blabla.bla",
        "unlockAbleType" : "UnlockAbleTypeCardSkin",
        "skinId"  : NumberInt(0)
    },
    {
        "unlockAbleId" : "cz.blabla.blabla.bla",
        "unlockAbleType" : "UnlockAbleTypeAvatar",
        "avatarId"  : NumberInt(0)
    },
    {
        "unlockAbleId" : "cz.blabla.blabla.bla",
        "unlockAbleType" : "UnlockAbleTypeDeckIcon",
        "deckIconId"  : NumberInt(0)
    },
    {
        "unlockAbleId" : "cz.blabla.blabla.bla",
        "unlockAbleType" : "UnlockAbleTypeBooster",
        "boosterRequiredSize" : NumberInt(10),
        "boosterLimitations" : [
          {
            "rarity" : "common",
            "min" : NumberInt(0),
            "max" : NumberInt(10)
          }
        ],
        "boosterTable" : {
             "cardId": ObjectId("5360d1e7bd54f6f578b14a1f"),
             "chance": NumberInt(2),
             "rarity" : "common"
        }
    },
    {
        "unlockAbleId" : "cz.blabla.blabla.bla",
        "unlockAbleType" : "UnlockAbleTypeDeck",
        "deckName" : "blabla",
        "cardTable" : [
            {
               "cardId": ObjectId("5360d1e7bd54f6f578b14a1f"),
               "count": NumberInt(2)
            }
        ]
    },
}

                  //UnlockAbleMethodTransaction, UnlockAbleMethodRedemptionCode
val unlockAbleHistory = {
   {
    "time" : NumberLong(1000),
    "unlockAbleId" : "",
    "unlockAbleType" : "",
    "unlockAbleMethodType" : "UnlockAbleMethodTransaction",
    "transactionId"  : ""
   },
      {
       "time" : NumberLong(1000),
       "unlockAbleId" : "",
       "unlockAbleType" : "",
       "unlockAbleMethodType" : "UnlockAbleMethodRedemptionCode",
       "redemptionCode"  : "185-555-566-488"
      }
}