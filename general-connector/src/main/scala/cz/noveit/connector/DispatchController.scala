package cz.noveit.connector

import cz.noveit.proto.serialization.{MessageEventContext, MessageSerializer, MessageEvent}
import cz.noveit.connector.websockets.WebSocketsContext
import akka.actor._
import org.bouncycastle.util.encoders.Base64
import scala.concurrent.duration.Duration
import cz.noveit.service.ContextDestroy

/**
 * Created by Wlsek on 28.1.14.
 */

case class SendMessageToContext(e: MessageEvent, ctx: MessageEventContext)
case class SendMessageToContextAndRegisterReplyRoute(e: MessageEvent, ctx: MessageEventContext, timeout: Option[Long] = None)
case class MessageEventTimeOuted(e: MessageEvent)

class DispatchController(registry: ActorRef) extends Actor with ActorLogging {

  case class ReplyRoute(replyHash: String, receiver: ActorRef, context: MessageEventContext)
  var replyRoutes: List[ReplyRoute] = List()

  case class TimeOutRoute(timeout: Long, timeOuter: ActorRef, receiver: ActorRef, replyHash: String, context: MessageEventContext)
  var timeoutRoutes: List[TimeOutRoute] = List()

  def receive = {
    case msg: SendMessageToContext =>
      val task = context actorOf(Props[MessageDispatchTask])
      task ! msg

    case msg: SendMessageToContextAndRegisterReplyRoute =>
      msg.timeout.map(timeout => {
        val timeOuter = context actorOf(Props(classOf[MessageDispatchTimeOuter], timeout, msg.e, sender));
        timeoutRoutes = TimeOutRoute(timeout, timeOuter, sender, msg.e.replyHash, msg.ctx) :: timeoutRoutes
      })

      replyRoutes = ReplyRoute(msg.e.replyHash, sender, msg.ctx) :: replyRoutes

      val task = context actorOf(Props[MessageDispatchTask])
      task ! msg

    case msg: MessageEvent =>
      replyRoutes.find(route => route.replyHash.equals(msg.replyHash)).map(route => {
        timeoutRoutes.find(tRoute => tRoute.replyHash.equals(route.replyHash)).map(tRoute => context.stop(tRoute.timeOuter))
        route.receiver ! msg
      })

      replyRoutes = replyRoutes.filterNot(route => route.replyHash.equals(msg.replyHash))
      timeoutRoutes = timeoutRoutes.filterNot(route => route.replyHash.equals(msg.replyHash))

    case msg: MessageEventTimeOuted =>
       replyRoutes = replyRoutes.filterNot(route => route.replyHash.equals(msg.e.replyHash))
       timeoutRoutes = timeoutRoutes.filterNot(route => route.replyHash.equals(msg.e.replyHash))

    case cd: ContextDestroy =>
      replyRoutes = replyRoutes.filterNot(route => route.context.equals(cd.context))
      timeoutRoutes = timeoutRoutes.filterNot(route => route.context.equals(cd.context))
  }
}

case class MessageDispatchTimeOuter(timeout: Long, e: MessageEvent, receiver: ActorRef, controller: ActorRef) extends Actor with ActorLogging {
  context.setReceiveTimeout(Duration.create(timeout, "millis"))

  def receive = {
    case ReceiveTimeout =>
      controller ! MessageEventTimeOuted(e)
      receiver ! MessageEventTimeOuted(e)
      context.stop(self)
  }
}

class MessageDispatchTask extends Actor with ActorLogging with MessageSerializer {
    def receive = {
      case msg: SendMessageToContext =>

        if(msg.ctx.isInstanceOf[WebSocketsContext]) msg.ctx.asInstanceOf[WebSocketsContext]
          .send( new String(Base64.encode(serializeToMessage(msg.e).toByteArray), "UTF-8"))
        context.stop(self)

      case msg: SendMessageToContextAndRegisterReplyRoute =>
        if(msg.ctx.isInstanceOf[WebSocketsContext]) msg.ctx.asInstanceOf[WebSocketsContext]
          .send( new String(Base64.encode(serializeToMessage(msg.e).toByteArray), "UTF-8"))
        context.stop(self)
    }
}