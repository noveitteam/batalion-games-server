package cz.noveit.connector.websockets

import io.backchat.hookup._
import io.backchat.hookup.Error
import io.backchat.hookup.TextMessage
import io.backchat.hookup.Disconnected
import akka.actor.{ActorRef, ActorContext, Actor, ActorLogging}
import sun.misc.BASE64Decoder
import cz.noveit.proto.serialization.{MessageEvent, MessageEventContext, MessageDeserializer}
import cz.noveit.proto.Messages.MessagesPackage.Message
import org.bouncycastle.util.encoders.Base64
import cz.noveit.service._

/**
 * Created by Wlsek on 3.1.14.
 */

class WebSocketsContext(registry: ActorRef) extends HookupServerClient with MessageDeserializer with MessageEventContext {

    override def receive = {
      case Connected ⇒
      case Disconnected(_) ⇒
        registry ! ContextDestroy(this)

      case m @ Error(exOpt) ⇒
        println("error")
      case m: TextMessage ⇒
        println(m)
        implicit val sender = this
        deserialize(Message.parseFrom(Base64.decode(m.content))).map(e => registry ! MessageEvent(e.message, e.module, e.replyHash, Some(this)))
      case t => throw new UnsupportedOperationException(t.toString())
    }

}
