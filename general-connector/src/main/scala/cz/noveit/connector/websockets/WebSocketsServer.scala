package cz.noveit.connector.websockets

import akka.actor.{ActorRef, Props, Actor, ActorLogging}
import javax.net.ssl.{KeyManagerFactory, SSLContext}
import java.io.FileInputStream
import java.security.KeyStore
import io.backchat.hookup.{HookupServer, ServerInfo}
import com.typesafe.config.Config

/**
 * Created by Wlsek on 3.1.14.
 */


sealed trait WebSocketsServerMessages
case class StartWebSocketServer(serverName: String, serviceRegistry: ActorRef) extends WebSocketsServerMessages
case object StopWebSocketServer extends  WebSocketsServerMessages

class WebSocketsServer(config: Config) extends  Actor with ActorLogging {

  var server: Option[HookupServer] = None;

  def receive = {
    case msg: StartWebSocketServer =>

      val sslC: SSLContext = SSLContext.getInstance("TLS");

      val storepass: Array[Char] = "sorrydude12214".toCharArray();
      val keypass:  Array[Char] = "sorrydude12214".toCharArray();
      val storename = "server.jks";

      val kmf: KeyManagerFactory = KeyManagerFactory.getInstance("SunX509");
      val fin: FileInputStream = new FileInputStream(storename);
      val ks: KeyStore = KeyStore.getInstance("JKS");
      ks.load(fin, storepass);

      kmf.init(ks, keypass);
      sslC.init(kmf.getKeyManagers(), null, null)

      val bindOnPort = config.getConfig("web-socket-server").getInt("port")
      val address = config.getConfig("web-socket-server").getString("address")

      val info: ServerInfo = new ServerInfo(msg.serverName, port = bindOnPort, listenOn = address){
      //  override val sslContext = Some(sslC);
      }

      this.server = Some((HookupServer(info) {
        new WebSocketsContext(msg.serviceRegistry)
      }))

      server.map(s => s.start)

    case StopWebSocketServer =>
      server.map(s => s.stop)
  }
}
