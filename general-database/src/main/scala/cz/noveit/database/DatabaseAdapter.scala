package cz.noveit.database

import akka.actor.{ActorLogging, Actor}

/**
 * Created by Wlsek on 6.1.14.
 */

trait DatabaseAdapter extends Actor with ActorLogging

trait DatabaseAdapterMessage
trait DatabaseCommandMessage extends DatabaseAdapterMessage
trait DatabaseCommandResultMessage extends DatabaseAdapterMessage

case class DatabaseCommand(command: DatabaseCommandMessage, replyHash: String = "")
case class DatabaseResult(result: DatabaseCommandResultMessage, replyHash: String = "")