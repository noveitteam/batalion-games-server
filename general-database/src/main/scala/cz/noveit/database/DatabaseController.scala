package cz.noveit.database

import akka.actor.{ActorRef, Props, ActorLogging, Actor}
import com.mongodb.casbah.{MongoCollection, MongoConnection}
import com.mongodb.{WriteConcern, ServerAddress}

/**
 * Created by Wlsek on 6.1.14.
 */



case class DatabaseControllerSettings(nodes: List[ServerAddress])

case class GetCollection(databaseName: String, collectionName: String)
case class ProcessTaskOnCollection(task: (MongoCollection) => Unit, databaseName: String , collectionName: String)
case class ProcessTaskOnCollectionAndReplyToSender(task: (MongoCollection, ActorRef) => Unit, databaseName: String , collectionName: String)

class DatabaseController(databaseControllerSettings: DatabaseControllerSettings) extends Actor with ActorLogging {

  val connectionsPool = for( x <- 0 to 4) yield ({
    val connection = MongoConnection(databaseControllerSettings.nodes)
    connection.setWriteConcern( WriteConcern.SAFE)
    connection
  })
  var actualPointer = 0;

  def receive = {
    case g: GetCollection =>
      sender ! connectionsPool(actualPointer)(g.databaseName)(g.collectionName)
      shift

    case ptoc: ProcessTaskOnCollection =>
      val task = context.actorOf(Props(classOf[DatabaseControllerTask], ptoc.task))
      task ! connectionsPool(actualPointer)(ptoc.databaseName)(ptoc.collectionName)

    case ptoc: ProcessTaskOnCollectionAndReplyToSender =>
      val task = context.actorOf(Props(classOf[DatabaseControllerTaskWithSender], ptoc.task, sender))
      task ! connectionsPool(actualPointer)(ptoc.databaseName)(ptoc.collectionName)
  }

  def shift = {
    if(actualPointer + 1 >= connectionsPool.size ){
      actualPointer = 0
    } else {
      actualPointer += 1
    }
  }
}

class DatabaseControllerTask(task: (MongoCollection) => Unit) extends  Actor with ActorLogging {
  def receive = {
    case collection: MongoCollection =>
      task(collection)
      context.stop(self)
  }
}

class DatabaseControllerTaskWithSender(task: (MongoCollection, ActorRef) => Unit, s: ActorRef) extends  Actor with ActorLogging {
  def receive = {
    case collection: MongoCollection =>
      task(collection, s)
      context.stop(self)
  }
}