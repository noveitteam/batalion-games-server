package cz.noveit.proto.serialization

import cz.noveit.proto.Messages


/**
 * Created with IntelliJ IDEA.
 * User: Wlsek
 * Date: 12.12.12
 * Time: 13:41
 * To change this template use File | Settings | File Templates.
 */
trait MessageDeserializer {
  def <<[B](msg: Messages.MessagesPackage.Message): Option[MessageEvent] = {
    deserialize(msg)
  }

  def deserialize(msg: Messages.MessagesPackage.Message): Option[MessageEvent] = {
    try {
      val classOfProto = Class.forName(msg.getPackage() + "." + msg.getModule.name() +  "$" + msg.getMessageName + "$Builder")

      val merge = classOfProto.getMethod("mergeFrom", classOf[Array[Byte]])
      val build = classOfProto.getMethod("build")

      val declaredConstructors = classOfProto.getDeclaredConstructors;

      declaredConstructors.map(c => {
        c.setAccessible(true)
      })

      declaredConstructors.find(c => c.getParameterTypes.length == 0).map(bdinstance => {
        val builder = bdinstance.newInstance()

        merge.invoke(builder, msg.getMessage().toByteArray())

        val protoMessage: com.google.protobuf.Message = build.invoke(builder).asInstanceOf[com.google.protobuf.Message]
        MessageEvent(protoMessage,msg.getModule.name(), msg.getReplyHash)
      })
    } catch {
      case t: Throwable =>
    //    error("Parsing message failed!: " + t.getMessage)
        t.printStackTrace()
        None
    }
  }
}