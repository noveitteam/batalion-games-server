package cz.noveit.proto.serialization


/**
 * Created with IntelliJ IDEA.
 * User: Wlsek
 * Date: 12.12.12
 * Time: 13:41
 * To change this template use File | Settings | File Templates.
 */


trait MessageEventContext
case class MessageEvent(message: com.google.protobuf.Message, module: String, replyHash: String = "", context: Option[MessageEventContext] = None)

