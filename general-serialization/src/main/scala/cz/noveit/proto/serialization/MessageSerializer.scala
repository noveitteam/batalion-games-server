package cz.noveit.proto.serialization

import cz.noveit.proto.Messages
import cz.noveit.proto.Messages.Module
import com.google.protobuf.ByteString
import org.bouncycastle.util.encoders.Base64


/**
 * Created with IntelliJ IDEA.
 * User: Wlsek
 * Date: 28.11.12
 * Time: 13:24
 * To change this template use File | Settings | File Templates.
 */
trait MessageSerializer {
  def >>[B](msg: MessageEvent): Messages.MessagesPackage.Message = {
    serializeToMessage(msg)
  }

  def serializeToMessage(msg: MessageEvent): Messages.MessagesPackage.Message = {
    val builder = Messages.MessagesPackage.Message.newBuilder();

    builder.setMessage(msg.message.toByteString)
    builder.setReplyHash(msg.replyHash)
    builder.setModule(Module.valueOf(msg.module))
    builder.setMessageName(msg.message.getClass.getSimpleName)
    builder.setPackage(msg.message.getClass.getPackage.getName)
    builder.build()
  }

  def serializeToString(msg: MessageEvent) = {
    new String(Base64.encode(serializeToMessage(msg).toByteArray), "UTF-8")
  }
}
