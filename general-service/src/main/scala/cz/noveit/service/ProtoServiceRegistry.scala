package cz.noveit.service

import akka.actor.{ActorRef, Actor, ActorLogging}
import cz.noveit.proto.serialization.{MessageEventContext, MessageEvent}

/**
 * Created by Wlsek on 3.1.14.
 */

sealed trait ServiceRegistryMessages
case class RegisterServiceMessage(messages: List[String]) extends ServiceRegistryMessages
case class UnregisterServiceMessage(messages: List[String]) extends ServiceRegistryMessages
case object UnregisterAllServiceMessage extends ServiceRegistryMessages
case object GetListenersSize extends ServiceRegistryMessages
case class ContextDestroy(context: MessageEventContext)  extends  ServiceRegistryMessages

class ProtoServiceRegistry extends Actor with ActorLogging {

  case class RegistryPair(message: String, responder: ActorRef)
  var listeners: List[RegistryPair] = List()

  override def receive = {

      case e: MessageEvent =>
        println(listeners)
        listeners.filter(pair =>
          pair.message.equals(e.message.getClass.getSimpleName)
        ).foreach(found =>
          found.responder ! e
        )

      case cd: ContextDestroy =>
        listeners.foreach(l => l.responder ! cd)

      case r: RegisterServiceMessage  =>
        r.messages.foreach(m => listeners = RegistryPair(m, sender) :: listeners)

      case u: UnregisterServiceMessage =>
        u.messages.foreach(m => listeners = listeners filterNot(pair => {
          pair.message.equals(m)
        }))


      case UnregisterAllServiceMessage =>
        listeners = listeners.filterNot(pair => pair.responder equals(sender))

      case GetListenersSize =>
        sender ! listeners.size

      case t => throw new UnsupportedOperationException(t.toString());
    }
}
