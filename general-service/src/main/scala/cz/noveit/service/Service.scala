package cz.noveit.service

import akka.actor.{ActorRef, ActorLogging, Actor}
import scala.collection.mutable.{Map, SynchronizedMap, HashMap}
import cz.noveit.proto.serialization.MessageEventContext

/**
 * Created by Wlsek on 6.1.14.
 */
trait Service extends Actor with ActorLogging {
  val moduleName: String;
  val mutableRegistry: Map[MessageEventContext, ActorRef] = new HashMap[MessageEventContext, ActorRef] with SynchronizedMap[MessageEventContext, ActorRef]

  def getServiceContext(context: MessageEventContext): Option[ActorRef] = {
    mutableRegistry
      .find(p => context.equals(p._1))
      .map(f => f._2)
  }

  def registerContext(messageContext: MessageEventContext, serviceContext: ActorRef) = {
    mutableRegistry += messageContext -> serviceContext
  }

  def removeContext(messageContext: MessageEventContext): Option[ActorRef] = {
    mutableRegistry.remove(messageContext)
  }
}
