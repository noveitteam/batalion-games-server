package cz.noveit.service

import akka.actor.ActorRef

/**
 * Created by Wlsek on 7.1.14.
 */
trait ServiceRegistration {
  def <--[T](service: ActorRef, registry: ActorRef)(implicit  m: Manifest[T]) = {
    implicit def sender = service
    registry ! RegisterServiceMessage(List(sn[T]))
  }

  def <--(messages: List[String], service: ActorRef, registry: ActorRef) {
    implicit def sender = service
    registry ! RegisterServiceMessage(messages)
  }

  def sn[T]()(implicit  m: Manifest[T]): String = {
    m.runtimeClass.getSimpleName
  }

}
