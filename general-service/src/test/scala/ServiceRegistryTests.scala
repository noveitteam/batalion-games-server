import akka.actor.{Actor, ActorSystem, Props}

import cz.noveit.proto.serialization.MessageEvent
import cz.noveit.service._
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import dummy._
import org.scalatest.{BeforeAndAfterAll, Suites, FlatSpec}
import scala.concurrent.Await

/**
 * Created by Wlsek on 6.1.14.
 */

class ServiceRegistryTests extends FlatSpec with ServiceRegistration {

  "RegisterServiceMessage" should "register listeners" in {
    val actorSystem = ActorSystem("SorryDudeTest")
    val registry = actorSystem actorOf Props[ProtoServiceRegistry]

    implicit var sender = actorSystem actorOf Props[DummyActor]
    <--(List(sn[DummyMessage], sn[DummyMessageB], sn[DummyMessageC]), sender, registry)

    sender = actorSystem actorOf Props[DummyActor]
    <--(List(sn[DummyMessage]), sender, registry)

    sender = actorSystem actorOf Props[DummyActor]
    <--(List(sn[DummyMessageB], sn[DummyMessageC]), sender, registry)

    implicit val timeout = Timeout(5 seconds)
    val future = registry ? GetListenersSize
    val result = Await.result(future, timeout.duration).asInstanceOf[Int]

    assert(result == 6)
    actorSystem.shutdown()
  }


  "UnregisterServiceMessage" should "unregister listeners" in {
    val actorSystem = ActorSystem("SorryDudeTest")
    val registry = actorSystem actorOf Props[ProtoServiceRegistry]

    implicit var sender = actorSystem actorOf Props[DummyActor]
    <--(List(sn[DummyMessage], sn[DummyMessageB], sn[DummyMessageC]), sender, registry)

    sender = actorSystem actorOf Props[DummyActor]
    <--(List(sn[DummyMessage]), sender, registry)

    sender = actorSystem actorOf Props[DummyActor]
    <--(List(sn[DummyMessageB], sn[DummyMessageC]), sender, registry)

    implicit val timeout = Timeout(5 seconds)
    var future = registry ? GetListenersSize
    var result = Await.result(future, timeout.duration).asInstanceOf[Int]
    assert(result == 6)

    registry ! UnregisterServiceMessage(List(sn[DummyMessage]))
    future = registry ? GetListenersSize
    result = Await.result(future, timeout.duration).asInstanceOf[Int]
    println(result)
    assert(result == 4)

    registry ! UnregisterServiceMessage(List(sn[DummyMessageB]))
    future = registry ? GetListenersSize
    result = Await.result(future, timeout.duration).asInstanceOf[Int]
    assert(result == 2)

    registry ! UnregisterServiceMessage(List(sn[DummyMessageC]))
    future = registry ? GetListenersSize
    result = Await.result(future, timeout.duration).asInstanceOf[Int]
    assert(result == 0)

    actorSystem.shutdown()
  }

  "UnregisterServiceMessage" should "unregister listeners when more items in list supplied" in {
    val actorSystem = ActorSystem("SorryDudeTest")
    val registry = actorSystem actorOf Props[ProtoServiceRegistry]

    implicit var sender = actorSystem actorOf Props[DummyActor]
    val sender1 = sender
    <--(List(sn[DummyMessage], sn[DummyMessageB], sn[DummyMessageC]), sender, registry)

    sender = actorSystem actorOf Props[DummyActor]
    val sender2 = sender
    <--(List(sn[DummyMessage]), sender, registry)

    sender = actorSystem actorOf Props[DummyActor]
    val sender3 = sender
    <--(List(sn[DummyMessageB], sn[DummyMessageC]), sender, registry)

    implicit val timeout = Timeout(5 seconds)

    sender = sender1
    registry ! UnregisterAllServiceMessage
    var future = registry ? GetListenersSize
    var result = Await.result(future, timeout.duration).asInstanceOf[Int]
    assert(result == 3)

    sender = sender2
    registry ! UnregisterAllServiceMessage
    future = registry ? GetListenersSize
    result = Await.result(future, timeout.duration).asInstanceOf[Int]
    assert(result == 2)

    sender = sender3
    registry ! UnregisterAllServiceMessage
    future = registry ? GetListenersSize
    result = Await.result(future, timeout.duration).asInstanceOf[Int]
    assert(result == 0)

    actorSystem.shutdown()
  }


  "Sending MessageEvent" should "dispatch DummyMessage to sender1, sender2" in {
    val actorSystem = ActorSystem("SorryDudeTest")
    val registry = actorSystem actorOf Props[ProtoServiceRegistry]

    implicit var sender = actorSystem actorOf Props[DummyActor]
    implicit val timeout = Timeout(5 seconds)

    val sender1 = sender
    <--(List(sn[DummyMessage], sn[DummyMessageB], sn[DummyMessageC]), sender, registry)

    sender = actorSystem actorOf Props[DummyActor]
    val sender2 = sender
    <--(List(sn[DummyMessage]), sender, registry)

    sender = actorSystem actorOf Props[DummyActor]
    val sender3 = sender
    <--(List(sn[DummyMessageB], sn[DummyMessageC]), sender, registry)

    val messageEvent = MessageEvent(new DummyMessage, "")

    registry ! messageEvent
    Thread.sleep(1000)

    var future = sender1 ? GetLastEvent
    var result = Await.result(future, timeout.duration).asInstanceOf[LastEvent]
    assert(result.messageEvent.get.equals(messageEvent))

    future = sender2 ? GetLastEvent
    result = Await.result(future, timeout.duration).asInstanceOf[LastEvent]
    assert(result.messageEvent.get.equals(messageEvent))

    future = sender3 ? GetLastEvent
    result = Await.result(future, timeout.duration).asInstanceOf[LastEvent]
    assert(result.messageEvent.isEmpty)

  }


  "Sending MessageEvent" should "dispatch DummyMessageB to sender1, sender3" in {
    val actorSystem = ActorSystem("SorryDudeTest")
    val registry = actorSystem actorOf Props[ProtoServiceRegistry]

    implicit var sender = actorSystem actorOf Props[DummyActor]
    implicit val timeout = Timeout(5 seconds)

    val sender1 = sender
    <--(List(sn[DummyMessage], sn[DummyMessageB], sn[DummyMessageC]), sender, registry)

    sender = actorSystem actorOf Props[DummyActor]
    val sender2 = sender
    <--(List(sn[DummyMessage]), sender, registry)

    sender = actorSystem actorOf Props[DummyActor]
    val sender3 = sender
    <--(List(sn[DummyMessageB], sn[DummyMessageC]), sender, registry)

    val messageEvent = MessageEvent(new DummyMessageB, "")

    registry ! messageEvent
    Thread.sleep(1000)

    var future = sender1 ? GetLastEvent
    var result = Await.result(future, timeout.duration).asInstanceOf[LastEvent]
    assert(result.messageEvent.get.equals(messageEvent))

    future = sender2 ? GetLastEvent
    result = Await.result(future, timeout.duration).asInstanceOf[LastEvent]
    assert(result.messageEvent.isEmpty)

    future = sender3 ? GetLastEvent
    result = Await.result(future, timeout.duration).asInstanceOf[LastEvent]
    assert(result.messageEvent.get.equals(messageEvent))
  }

  "Sending MessageEvent" should "dispatch DummyMessageC to sender1, sender3" in {
    val actorSystem = ActorSystem("SorryDudeTest")
    val registry = actorSystem actorOf Props[ProtoServiceRegistry]

    implicit var sender = actorSystem actorOf Props[DummyActor]
    implicit val timeout = Timeout(5 seconds)

    val sender1 = sender
    <--(List(sn[DummyMessage], sn[DummyMessageB], sn[DummyMessageC]), sender, registry)

    sender = actorSystem actorOf Props[DummyActor]
    val sender2 = sender
    <--(List(sn[DummyMessage]), sender, registry)

    sender = actorSystem actorOf Props[DummyActor]
    val sender3 = sender
    <--(List(sn[DummyMessageB], sn[DummyMessageC]), sender, registry)

    val messageEvent = MessageEvent(new DummyMessageC, "")

    registry ! messageEvent
    Thread.sleep(1000)

    var future = sender1 ? GetLastEvent
    var result = Await.result(future, timeout.duration).asInstanceOf[LastEvent]
    assert(result.messageEvent.get.equals(messageEvent))

    future = sender2 ? GetLastEvent
    result = Await.result(future, timeout.duration).asInstanceOf[LastEvent]
    assert(result.messageEvent.isEmpty)

    future = sender3 ? GetLastEvent
    result = Await.result(future, timeout.duration).asInstanceOf[LastEvent]
    assert(result.messageEvent.get.equals(messageEvent))
  }

}

/*
class EndpointTests extends Suites(new ServiceRegistryTests) with BeforeAndAfterAll {


override def afterAll(configMap: Map[String, Any]) {
println("After!")  // shut down the web server
}
}                                                    */