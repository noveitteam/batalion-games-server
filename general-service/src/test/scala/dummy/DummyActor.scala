package dummy

import akka.actor.{ActorLogging, Actor}
import cz.noveit.proto.serialization.MessageEvent
import cz.noveit.service.Service

/**
 * Created by Wlsek on 6.1.14.
 */

case object GetLastEvent
case class LastEvent(messageEvent:  Option[MessageEvent])

class DummyActor extends Actor with ActorLogging with Service {
    var lastMessage: Option[MessageEvent] = None
     def receive = {
       case e: MessageEvent =>
        lastMessage = Some(e)
       //  println(lastMessage)
       case GetLastEvent =>
       //  println(lastMessage)
        sender ! LastEvent(lastMessage)
     }
}
