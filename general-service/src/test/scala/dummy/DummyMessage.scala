package dummy

import com.google.protobuf.{UnknownFieldSet, CodedOutputStream, ByteString, Message}
import com.google.protobuf.Message.Builder
import java.io.OutputStream
import com.google.protobuf.Descriptors.{Descriptor, FieldDescriptor}
import java.util

/**
 * Created by Wlsek on 6.1.14.
 */
class DummyMessage extends Message
{
  def getDefaultInstanceForType: Message = ???

  def getDescriptorForType: Descriptor = ???

  def getAllFields: util.Map[FieldDescriptor, AnyRef] = ???

  def hasField(field: FieldDescriptor): Boolean = ???

  def getField(field: FieldDescriptor): AnyRef = ???

  def getRepeatedFieldCount(field: FieldDescriptor): Int = ???

  def getRepeatedField(field: FieldDescriptor, index: Int): AnyRef = ???

  def getUnknownFields: UnknownFieldSet = ???

  def writeTo(output: CodedOutputStream): Unit = ???

  def getSerializedSize: Int = ???

  def toByteString: ByteString = ???

  def toByteArray: Array[Byte] = ???

  def writeTo(output: OutputStream): Unit = ???

  def writeDelimitedTo(output: OutputStream): Unit = ???

  def newBuilderForType(): Builder = ???

  def toBuilder: Builder = ???

  def isInitialized: Boolean = ???
}

class DummyMessageB extends  Message
{
  def getDefaultInstanceForType: Message = ???

  def getDescriptorForType: Descriptor = ???

  def getAllFields: util.Map[FieldDescriptor, AnyRef] = ???

  def hasField(field: FieldDescriptor): Boolean = ???

  def getField(field: FieldDescriptor): AnyRef = ???

  def getRepeatedFieldCount(field: FieldDescriptor): Int = ???

  def getRepeatedField(field: FieldDescriptor, index: Int): AnyRef = ???

  def getUnknownFields: UnknownFieldSet = ???

  def writeTo(output: CodedOutputStream): Unit = ???

  def getSerializedSize: Int = ???

  def toByteString: ByteString = ???

  def toByteArray: Array[Byte] = ???

  def writeTo(output: OutputStream): Unit = ???

  def writeDelimitedTo(output: OutputStream): Unit = ???

  def newBuilderForType(): Builder = ???

  def toBuilder: Builder = ???

  def isInitialized: Boolean = ???
}

class DummyMessageC extends Message
{
  def getDefaultInstanceForType: Message = ???

  def getDescriptorForType: Descriptor = ???

  def getAllFields: util.Map[FieldDescriptor, AnyRef] = ???

  def hasField(field: FieldDescriptor): Boolean = ???

  def getField(field: FieldDescriptor): AnyRef = ???

  def getRepeatedFieldCount(field: FieldDescriptor): Int = ???

  def getRepeatedField(field: FieldDescriptor, index: Int): AnyRef = ???

  def getUnknownFields: UnknownFieldSet = ???

  def writeTo(output: CodedOutputStream): Unit = ???

  def getSerializedSize: Int = ???

  def toByteString: ByteString = ???

  def toByteArray: Array[Byte] = ???

  def writeTo(output: OutputStream): Unit = ???

  def writeDelimitedTo(output: OutputStream): Unit = ???

  def newBuilderForType(): Builder = ???

  def toBuilder: Builder = ???

  def isInitialized: Boolean = ???
}