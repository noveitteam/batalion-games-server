package cz.noveit.smtp

import akka.actor.{Props, Actor, ActorLogging}
import org.apache.commons.mail.{DefaultAuthenticator, HtmlEmail}

/**
 * Created by Wlsek on 14.2.14.
 */

case class SmtpConfig(tls: Boolean = false,
                      ssl: Boolean = false,
                      port: Int = 25,
                      host: String,
                      user: String,
                      password: String)

case class EmailMessage(subject: String,
                        recipient: String,
                        from: String,
                        text: String,
                        html: String)

class SmtpService(config: SmtpConfig) extends Actor with ActorLogging {
  def receive = {
    case email: EmailMessage =>
        val actor = context.actorOf(Props(classOf[SmtpWorker], config))
        actor ! email
  }
}

class SmtpWorker(config: SmtpConfig) extends Actor with ActorLogging {
  def receive = {
    case msg: EmailMessage =>
       try{
         // Create the email message
         val email = new HtmlEmail()
         email.setStartTLSEnabled(config.tls)
         email.setSSLOnConnect(config.ssl)
         email.setSmtpPort(config.port)
         email.setHostName(config.host)
         email.setAuthenticator(new DefaultAuthenticator(
           config.user,
           config.password
         ))
         email.setHtmlMsg(msg.html)
           .setTextMsg(msg.text)
           .addTo(msg.recipient)
           .setFrom(msg.from)
           .setSubject(msg.subject)
           .send()
       }  catch {
         case t: Throwable =>
          log.error("Cannot send email: " + t.getMessage)
       } finally {
         context.stop(self)
       }
  }
}
