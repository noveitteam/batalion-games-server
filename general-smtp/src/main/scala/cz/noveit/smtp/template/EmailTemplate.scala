package cz.noveit.smtp.template

/**
 * Created by Wlsek on 14.2.14.
 */
class EmailTemplate(sourceHtml: String) {
  var html = sourceHtml

  def apply(pair: Pair[String, String]) = {
    html = html.replaceAll(pair._1, pair._2)
  }

  override def toString = {
    html
  }
}
