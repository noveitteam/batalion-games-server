import akka.actor.{Props, ActorSystem}
import cz.noveit.smtp.{EmailMessage, SmtpConfig, SmtpService}
import cz.noveit.smtp.template.EmailTemplate
import java.io.File
import org.scalatest.FlatSpec

/**
 * Created by Wlsek on 14.2.14.
 */
class SmtpServiceTests extends FlatSpec {

  val smtpConfig = SmtpConfig(
    true,
    true,
    465,
    "smtp.gmail.com",
    "Hawuawu@gmail.com",
    "ihiefheqhqaummej"
  )

  "SmtpService " should "send email to my inbox" in {

    val actorSystem = ActorSystem("SmtpTests")

    val lines = scala.io.Source.fromFile("emailtemplates/registration.html").mkString
    val template = new EmailTemplate(lines)
    template("%USERNAME%" -> "Hawuawu")
    template("%EMAILVALIDATIONLINKGAME%" -> "http://google.com")
    template("%EMAILVALIDATIONLINKBROWSER%" -> "http://google.com")

    val service = actorSystem actorOf (Props(classOf[SmtpService], smtpConfig))
    service ! EmailMessage("Welcome!", "hawuawu@gmail.com", "registration@firstenchanter.com", "Thank you for registration, please enter: 1234 for email confirmation.", template.toString)
  }

}
