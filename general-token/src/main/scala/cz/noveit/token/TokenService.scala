package cz.noveit.token

import akka.actor._
import cz.noveit.token.adapters._
import scala.util.Random
import scala.concurrent.duration._
import cz.noveit.token.adapters.CreateTokenForEntity
import cz.noveit.token.adapters.QueryTokenValidity
import cz.noveit.token.adapters.TokenCreated
import cz.noveit.token.adapters.TokenCreationFailedTokenExists
import cz.noveit.token.adapters.QueryTokenValid
import cz.noveit.database.DatabaseCommand
import cz.noveit.token.adapters.QueryTokenInvalid
import cz.noveit.database.DatabaseResult

/**
 * Created by Wlsek on 14.2.14.
 */

case class Token(key: String, created: Long, validity: Long)

case class TokenContext(replyTo: ActorRef, entity: String)

case class GenerateToken(ctx: TokenContext, validity: Long)

case class GeneratedToken(ctx: TokenContext, token: Token)

case class TokenValidate(ctx: TokenContext, key: String)

case class TokenValid(ctx: TokenContext)

case class TokenInvalid(ctx: TokenContext)

class TokenService(databaseController: ActorRef, databaseName: String, tokenCollection: String, cleanTime: Long) extends Actor with ActorLogging {

  val tokenDatabase = context.actorOf(Props(classOf[TokenDatabaseAdapter], databaseController, databaseName, tokenCollection))
  val cleaner = context.actorOf(Props(classOf[TokenCleaner], tokenDatabase, cleanTime))

  def receive = {
    case msg: GenerateToken =>
      val actor = context actorOf (Props(classOf[TokenWorker], tokenDatabase))
      actor ! msg

    case msg: TokenValidate =>
      val actor = context actorOf (Props(classOf[TokenWorker], tokenDatabase))
      actor ! msg

    case t => throw new UnsupportedOperationException("Unknown message: " + t)
  }
}

class TokenWorker(database: ActorRef) extends Actor with ActorLogging {


  def receive = {
    case GenerateToken(ctx, validity) =>
  /*    val uuid = UUID.randomUUID().toString() + System.currentTimeMillis().toString + ctx.entity
      val md = MessageDigest.getInstance("SHA-256")

      md.update(uuid.getBytes("UTF-8"))
      val digest = new String(Base64.encode(md.digest()), "UTF-8")
           println(digest)        */

      val digest = generateDigest(16)
      println(digest)
      database ! DatabaseCommand(CreateTokenForEntity(ctx, Token(digest, System.currentTimeMillis(), validity)), "")

    case TokenValidate(ctx, token) =>
      database ! DatabaseCommand(QueryTokenValidity(ctx, token), "")

    case DatabaseResult(r, hash) =>
      r match {
        case TokenCreated(ctx, token) =>
          ctx.replyTo ! GeneratedToken(ctx, token)
          context.stop(self)
        case TokenCreationFailedTokenExists(ctx, validity) =>
          println("failed")
          self ! GenerateToken(ctx, validity)
        case QueryTokenValid(ctx, token) =>
          ctx.replyTo ! TokenValid(ctx)
          context.stop(self)
        case QueryTokenInvalid(ctx, token) =>
          ctx.replyTo ! TokenInvalid(ctx)
          context.stop(self)
      }

    case t => throw new UnsupportedOperationException("Unknown message: " + t)
  }


  def generateDigest(length: Int): String = {
    var result = ""
    val rnd = new Random()

    for(i <- 1 to 16){
      result = result + Integer.toHexString(rnd.nextInt(15))
    }

    result
  }
}

class TokenCleaner (database: ActorRef, cleanTime: Long) extends Actor with ActorLogging {

  context.setReceiveTimeout(Duration.create(cleanTime, "millis"))
  log.info("TokenCleaner started")

  def receive = {
    case ReceiveTimeout =>
      log.info("Cleaning tokens")
      database ! DatabaseCommand(InvalidateNotValidTokens, "")

    case t => throw new UnsupportedOperationException("Unknown message: " + t)
  }
}
