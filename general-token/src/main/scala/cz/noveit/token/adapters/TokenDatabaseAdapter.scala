package cz.noveit.token.adapters

import akka.actor.{Actor, ActorLogging, ActorRef}
import cz.noveit.database._
import com.mongodb.casbah.Imports._
import com.mongodb.casbah.commons.MongoDBObject
import org.bson.types.ObjectId
import cz.noveit.database.ProcessTaskOnCollection
import cz.noveit.database.DatabaseResult
import cz.noveit.database.DatabaseCommand
import cz.noveit.token.{Token, TokenContext}
import com.mongodb.MongoException.DuplicateKey

/**
 * Created by Wlsek on 11.2.14.
 */

sealed trait TokenDatabaseMessages extends DatabaseAdapterMessage

case class CreateTokenForEntity(ctx: TokenContext, token: Token) extends TokenDatabaseMessages with DatabaseCommandMessage

case class TokenCreated(ctx: TokenContext, token: Token) extends TokenDatabaseMessages with DatabaseCommandResultMessage

case class TokenCreationFailedTokenExists(ctx: TokenContext, validity: Long) extends TokenDatabaseMessages with DatabaseCommandResultMessage


case class InvalidateTokenForEntity(entity: String, token: Token) extends TokenDatabaseMessages with DatabaseCommandMessage

case class QueryTokenValidity(ctx: TokenContext, token: String) extends TokenDatabaseMessages with DatabaseCommandMessage

case class QueryTokenValid(ctx: TokenContext, token: String) extends TokenDatabaseMessages with DatabaseCommandResultMessage

case class QueryTokenInvalid(ctx: TokenContext, token: String) extends TokenDatabaseMessages with DatabaseCommandResultMessage

case object InvalidateNotValidTokens extends TokenDatabaseMessages with DatabaseCommandMessage

class TokenDatabaseAdapter(val databaseController: ActorRef, databaseName: String, tokenCollection: String) extends Actor with ActorLogging {

  def receive = {
    case dc: DatabaseCommand =>
      dc.command match {
        case m: CreateTokenForEntity =>
          val replyTo = sender

          databaseController ! ProcessTaskOnCollection(
            (c: MongoCollection) => {
              try {
                c.save(MongoDBObject("key" -> m.token.key, "entity" -> m.ctx.entity, "created" -> System.currentTimeMillis, "validity" -> m.token.validity.toString))
                replyTo ! DatabaseResult(TokenCreated(m.ctx, m.token), dc.replyHash)
              } catch {
                case t: DuplicateKey =>
                  log.error(t.getMessage)
                  replyTo ! DatabaseResult(TokenCreationFailedTokenExists(m.ctx, m.token.validity), dc.replyHash)
                case t: Throwable => log.error("Cannot create token: " + t.getMessage)
              }

            }, databaseName, tokenCollection
          )

        case m: InvalidateTokenForEntity =>
          databaseController ! ProcessTaskOnCollection(
            (c: MongoCollection) => {
              try {
                c.remove(MongoDBObject("key" -> m.token.key, "entity" -> m.entity))
              } catch {
                case t: Throwable => log.error("Cannot invalidate token: " + t.getMessage)
              }

            }, databaseName, tokenCollection
          )

        case m: QueryTokenValidity =>
          val replyTo = sender
          databaseController ! ProcessTaskOnCollection(
            (c: MongoCollection) => {
              try {
                if (!c.findOne(MongoDBObject("key" -> m.token, "entity" -> m.ctx.entity)).isEmpty) {
                  replyTo ! DatabaseResult(QueryTokenValid(m.ctx, m.token), dc.replyHash)
                } else {
                  replyTo ! DatabaseResult(QueryTokenInvalid(m.ctx, m.token), dc.replyHash)
                }
              } catch {
                case t: Throwable => log.error("Cannot invalidate token: " + t.getMessage)
              }

            }, databaseName, tokenCollection
          )

        case InvalidateNotValidTokens =>
          databaseController ! ProcessTaskOnCollection(
            (c: MongoCollection) => {
              try {
                val min = c.find(MongoDBObject()).sort(MongoDBObject("validity" -> 1)).limit(1).toList.headOption.getOrElse(MongoDBObject()).getAs[Long]("validity").getOrElse(0L)

                val created = System.currentTimeMillis() - min
                c.find(MongoDBObject("created" -> MongoDBObject("$lte" -> created))).foreach(r => {

                  r.getAs[Long]("validity").map(v => {
                    r.getAs[Long]("created").map(cr => {
                      if (cr < (System.currentTimeMillis - v)) {
                        r.getAs[ObjectId]("_id").map(id => {
                          c.remove(MongoDBObject("_id" -> id))
                        })
                      }
                    })
                  })
                })

              } catch {
                case t: Throwable => log.error("Cannot invalidate token: " + t.getMessage)
              }

            }, databaseName, tokenCollection
          )
      }
    case t => throw new UnsupportedOperationException(t.toString)
  }
}
