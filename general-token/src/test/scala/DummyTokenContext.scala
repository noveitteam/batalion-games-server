import akka.actor.{Actor, ActorLogging}

/**
 * Created by Wlsek on 14.2.14.
 */
case object GetDummyTokenContextMessages
class DummyTokenContext extends Actor with ActorLogging {
  var messages: List[Any] = List()
    def receive = {
      case GetDummyTokenContextMessages => sender ! messages
      case t =>  messages = t :: messages
    }
}
