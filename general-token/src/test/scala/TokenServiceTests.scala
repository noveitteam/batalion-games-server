import akka.actor.{ActorLogging, Props, ActorSystem}
import com.mongodb.casbah.commons.MongoDBObject
import com.mongodb.casbah.Imports._
import com.mongodb.{WriteConcern, ServerAddress}
import cz.noveit.database.DatabaseControllerSettings
import cz.noveit.database.ProcessTaskOnCollection
import cz.noveit.database.{ProcessTaskOnCollection, DatabaseControllerSettings, DatabaseController}
import cz.noveit.token._
import cz.noveit.token.GeneratedToken
import cz.noveit.token.GenerateToken
import cz.noveit.token.TokenContext
import org.scalatest.FlatSpec

import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import scala.concurrent.Await

/**
 * Created by Wlsek on 14.2.14.
 */
class TokenServiceTests extends FlatSpec  {

  val database = "TestDatabase"
  val tokenCollection = "Tokens"

  "GenerateToken" should "generate token" in {
    val as = ActorSystem("TokenUnitTest")
    val addresses = List(new ServerAddress("127.0.0.1"))
    val databaseController = as.actorOf(Props(classOf[DatabaseController], DatabaseControllerSettings(addresses)))
    val tokenService = as.actorOf(Props(classOf[TokenService], databaseController, database, tokenCollection, 15000L))

    databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
      c.ensureIndex(MongoDBObject("key" -> 1), MongoDBObject("unique" -> 1))
      c.ensureIndex(MongoDBObject("validity" -> 1))
    }, database, tokenCollection)

    val replier = as.actorOf(Props[DummyTokenContext])
    tokenService ! GenerateToken(TokenContext(replier, "dan"), 1000L)

    Thread.sleep(2000)

    implicit val timeout = Timeout(5 seconds)
    val future1 = replier ? GetDummyTokenContextMessages

    val result1 = Await.result(future1, timeout.duration).asInstanceOf[List[Any]]
    assert(result1.size == 1)

    val msg: GeneratedToken = result1.head.asInstanceOf[GeneratedToken]

    databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
      assert(c.findOne(MongoDBObject("entity" -> "dan")).map(ent => {
        assert(ent.getAs[String]("key").getOrElse("").equals(msg.token.key))
        ent
      }).isDefined)
    }, database, tokenCollection)

    databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
      c.remove(MongoDBObject())
    }, database, tokenCollection)

    as.shutdown()

    Thread.sleep(500)
  }

  "ValidateToken" should "validate token successfully" in {
    val as = ActorSystem("TokenUnitTest")
    val addresses = List(new ServerAddress("127.0.0.1"))
    val databaseController = as.actorOf(Props(classOf[DatabaseController], DatabaseControllerSettings(addresses)))
    val tokenService = as.actorOf(Props(classOf[TokenService], databaseController, database, tokenCollection, 15000L))

    databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
      c.ensureIndex(MongoDBObject("key" -> 1), MongoDBObject("unique" -> 1))
      c.ensureIndex(MongoDBObject("validity" -> 1))
    }, database, tokenCollection)

    databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
      c.save(MongoDBObject("key" -> "123456789", "entity" -> "dan", "validity" -> Long.MaxValue))
    }, database, tokenCollection)

    Thread.sleep(1000)

    val replier = as.actorOf(Props[DummyTokenContext])
    tokenService ! TokenValidate(TokenContext(replier, "dan"), "123456789")

    Thread.sleep(1000)

    implicit val timeout = Timeout(5 seconds)
    val future1 = replier ? GetDummyTokenContextMessages

    val result1 = Await.result(future1, timeout.duration).asInstanceOf[List[Any]]
    assert(result1.size == 1)

    val msg: TokenValid = result1.head.asInstanceOf[TokenValid]

    databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
      c.remove(MongoDBObject())
    }, database, tokenCollection)

    as.shutdown()

    Thread.sleep(500)

  }

  "ValidateToken" should "validate token unsuccesfully" in {
    val as = ActorSystem("TokenUnitTest")
    val addresses = List(new ServerAddress("127.0.0.1"))
    val databaseController = as.actorOf(Props(classOf[DatabaseController], DatabaseControllerSettings(addresses)))
    val tokenService = as.actorOf(Props(classOf[TokenService], databaseController, database, tokenCollection, 15000L))

    databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
      c.ensureIndex(MongoDBObject("key" -> 1), MongoDBObject("unique" -> 1))
      c.ensureIndex(MongoDBObject("validity" -> 1))
    }, database, tokenCollection)

    databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
      c.save(MongoDBObject("key" -> "123456789", "entity" -> "dan", "validity" -> Long.MaxValue))
    }, database, tokenCollection)

    Thread.sleep(1000)

    val replier = as.actorOf(Props[DummyTokenContext])
    val replier2 =  as.actorOf(Props[DummyTokenContext])
    tokenService ! TokenValidate(TokenContext(replier, "dan"), "1234567891")
    tokenService ! TokenValidate(TokenContext(replier2, "dan"), "")

    Thread.sleep(1000)

    implicit val timeout = Timeout(5 seconds)
    val future1 = replier ? GetDummyTokenContextMessages
    val future2  = replier2 ? GetDummyTokenContextMessages

    val result1 = Await.result(future1, timeout.duration).asInstanceOf[List[Any]]
    assert(result1.size == 1)

    val result2 = Await.result(future2, timeout.duration).asInstanceOf[List[Any]]
    assert(result2.size == 1)

    val msg: TokenInvalid = result1.head.asInstanceOf[TokenInvalid]
    val msg2: TokenInvalid = result2.head.asInstanceOf[TokenInvalid]

    databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
      c.remove(MongoDBObject())
    }, database, tokenCollection)

    as.shutdown()

    Thread.sleep(500)
  }

  "TokenCleaner" should "remove not valid tokens" in {
    val as = ActorSystem("TokenUnitTest")
    val addresses = List(new ServerAddress("127.0.0.1"))
    val databaseController = as.actorOf(Props(classOf[DatabaseController], DatabaseControllerSettings(addresses)))

    databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
      c.ensureIndex(MongoDBObject("key" -> 1), MongoDBObject("unique" -> 1))
      c.ensureIndex(MongoDBObject("validity" -> 1))
    }, database, tokenCollection)

    databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
      c.save(MongoDBObject("key" -> "123456789", "entity" -> "dan", "validity" -> 1000L, "created" -> System.currentTimeMillis))
      c.save(MongoDBObject("key" -> "123456788", "entity" -> "dan", "validity" -> 1000L, "created" -> System.currentTimeMillis))
      c.save(MongoDBObject("key" -> "123456787", "entity" -> "dan", "validity" -> 10000L, "created" -> System.currentTimeMillis))
    }, database, tokenCollection)

    databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
      assert(c.find(MongoDBObject()).size == 3)
    }, database, tokenCollection)

    val tokenService = as.actorOf(Props(classOf[TokenService], databaseController, database, tokenCollection, 1000L))

    Thread.sleep(4000)

    databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
      assert(c.find(MongoDBObject()).size == 1)
    }, database, tokenCollection)

    Thread.sleep(500)


    databaseController ! ProcessTaskOnCollection((c: MongoCollection) => {
      c.remove(MongoDBObject())
    }, database, tokenCollection)

    as.shutdown()

    Thread.sleep(500)
  }
 }
