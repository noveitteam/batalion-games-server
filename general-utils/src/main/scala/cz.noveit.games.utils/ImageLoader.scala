package cz.noveit.games.utils

import java.awt.image.BufferedImage
import javax.imageio.ImageIO
import java.io.{FilenameFilter, ByteArrayOutputStream, File}

/**
 * Created by Wlsek on 18.2.14.
 */


trait ImageLoader {

  case class BufferedImageAvatar(bi: BufferedImage, id: Int)

  def loadImage(path: String, id: Int): Option[BufferedImage] = {
    try {
      val image = ImageIO.read(new File((path + id.toString + ".png")))
      Some(image)
    } catch {
      case t: Throwable => None
    }
  }

  def convertImageToByteArray(image: BufferedImage): Option[Array[Byte]] = {
    try {
      val baos = new ByteArrayOutputStream()
      ImageIO.write(image, "png", baos)
      baos.flush()
      val array = baos.toByteArray()
      baos.close()
      Some(array)
    } catch {
      case t: Throwable => None
    }
  }

  def loadImagesFromFolder(folder: String): List[BufferedImage] = {
    val fnFilter = new FilenameFilter() {
      override def accept(dir: File, name: String): Boolean = {
        name.endsWith(".png")
      }
    }

    val directory = new File(folder)

    if (directory.isDirectory) {
      directory.listFiles(fnFilter).map(f => {
        ImageIO.read(f)
      }).toList
    } else {
      List()
    }

  }

  def loadAvatarsFromFolder(folder: String): List[BufferedImageAvatar] = {
    val fnFilter = new FilenameFilter() {
      override def accept(dir: File, name: String): Boolean = {
        name.endsWith(".png")
      }
    }

    val directory = new File(folder)

    if (directory.isDirectory) {
      directory.listFiles(fnFilter).map(f => {
        try{
          BufferedImageAvatar(ImageIO.read(f), f.getName.replace(".png", "").toInt)
        }
      }).toList
    } else {
      List()
    }

  }

  def containsFolderId(folder: String, id: Int): Boolean = {
    val fnFilter = new FilenameFilter() {
      override def accept(dir: File, name: String): Boolean = {
        name.equals({id.toString + ".png"})
      }
    }

    val directory = new File(folder)

    if (directory.isDirectory) {
      !directory.listFiles(fnFilter).isEmpty
    } else {
      false
    }
  }


}
