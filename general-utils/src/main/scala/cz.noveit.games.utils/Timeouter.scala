package cz.noveit.games.utils

import akka.actor.{ReceiveTimeout, ActorRef, ActorLogging, Actor}
import scala.concurrent.duration.Duration

/**
 * Created by Wlsek on 24.2.14.
 */
class TimeOuter(replyTo: ActorRef, timeout: Long) extends Actor with ActorLogging {
  context.setReceiveTimeout(Duration.create(timeout.toString + "millis"))
  def receive = {
    case ReceiveTimeout =>
      replyTo ! ReceiveTimeout
      context.stop(self)
    case t =>
      context.stop(self)
  }

}
