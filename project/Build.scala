import sbt._
import Keys._
import sbtassembly.Plugin._
import AssemblyKeys._
import sbtprotobuf.{ProtobufPlugin => PB}
import tools.nsc.io.File
import com.earldouglas.xsbtwebplugin.{WebPlugin => WP}


object NoveitServersBuild extends Build {

  // -------------------------------------------------------- [Libraries] --------------------------------------------------------

  val commons = Seq("commons-lang" % "commons-lang" % "2.6")
  val logback = Seq("ch.qos.logback" % "logback-classic" % "1.1.2")
  val inject = Seq("com.google.inject" % "guice" % "3.0")
  val bouncy = Seq("org.bouncycastle" % "bcprov-jdk15on" % "1.47")
  val casbah = Seq("org.mongodb" % "casbah_2.10" % "2.6.4")
  val scalatest = Seq("org.scalatest" % "scalatest_2.10.0" % "2.0.M5")
  val commonsIo = Seq("commons-io" % "commons-io" % "2.4")
  val akka = Seq("com.typesafe.akka" % "akka-actor_2.10" % "2.3.4", "com.typesafe.akka" % "akka-transactor_2.10" % "2.3.4", "com.typesafe.akka" % "akka-slf4j_2.10" % "2.3.4") ++ Seq("org.scala-lang" % "scala-reflect" % "2.10.3")
  val akkaCluster = Seq("com.typesafe.akka" % "akka-cluster_2.10" % "2.3.4")
  val akkaTestkit = Seq("com.typesafe.akka" % "akka-testkit_2.10" % "2.3.4")
  val webSockets = Seq("io.backchat.hookup" % "hookup_2.10" % "0.2.3")
  val commonsEmail = Seq("org.apache.commons" % "commons-email" % "1.3.2")
  val httpClient = Seq("net.databinder.dispatch" %% "dispatch-core" % "0.11.1")
  val json = Seq("io.spray" %% "spray-json" % "1.3.0")
  val generatePdf = Seq("org.apache.pdfbox" % "pdfbox" % "1.8.5")
  val chatter = Seq("ca.pjer" % "chatter-bot-api" % "1.3.2")
  val scalaFx = Seq("org.scalafx" %% "scalafx" % "2.2.60-R9")


  //========================================================= [General] =========================================================

  lazy val utils = Project(id = "General-Utils", base = file("general-utils"), settings = Project.defaultSettings ++ Seq(
    scalaVersion := "2.10.3",
    libraryDependencies ++= scalatest ++ akka ++ commons ++ logback ++ inject ++ bouncy ++ commonsIo
  ))

  lazy val smtp = Project(id = "General-Smtp", base = file("general-smtp"), settings = Project.defaultSettings ++ Seq(
    scalaVersion := "2.10.3",
    libraryDependencies ++= scalatest ++ akka ++ commonsEmail
  ))

  lazy val protoSerialization = Project(id = "General-Serialization", base = file("general-serialization"), settings = Project.defaultSettings ++ PB.protobufSettings ++ Seq(
    scalaVersion := "2.10.3"
  ) ++ Seq(
    sourceDirectory in PB.protobufConfig := new java.io.File("./messages/general/"),
    javaSource in PB.protobufConfig <<= (sourceDirectory in Compile)(_ / "general-serialization/src/main/java"),
    version in PB.protobufConfig := "2.5.0"
  )) dependsOn (utils)

  lazy val webSocketsConnector = Project(id = "General-Connector", base = file("general-connector"), settings = Project.defaultSettings ++ Seq(
    scalaVersion := "2.10.3",
    libraryDependencies ++= webSockets ++ scalatest ++ bouncy
  )) dependsOn(protoSerialization, services)

  lazy val database = Project(id = "General-Database", base = file("general-database"), settings = Project.defaultSettings ++ Seq(
    scalaVersion := "2.10.3",
    libraryDependencies ++= casbah ++ scalatest ++ akka
  ))

  lazy val tokenizer = Project(id = "General-Token", base = file("general-token"), settings = Project.defaultSettings ++ Seq(
    scalaVersion := "2.10.3",
    libraryDependencies ++= scalatest ++ akka ++ bouncy
  )) dependsOn (database)

  lazy val services = Project(id = "General-Service", base = file("general-service"), settings = Project.defaultSettings ++ Seq(
    scalaVersion := "2.10.3",
    libraryDependencies ++= scalatest ++ akka
  )) dependsOn (protoSerialization)


  lazy val cluster = Project(id = "Akka-Cluster", base = file("akka-cluster"), settings = Project.defaultSettings ++ Seq(
    scalaVersion := "2.10.3",
    libraryDependencies ++= scalatest ++ akkaCluster
  ))


  //========================================================= [Sorry Dude game] =========================================================

  lazy val sorryDude = Project(id = "SorryDude-Server", base = file("sorrydude-server"), settings = Project.defaultSettings ++ assemblySettings ++ Seq(
    name := "SorryDude",
    version := "v0.1a",
    scalaVersion := "2.10.3",
    libraryDependencies ++= commons ++ commonsIo ++ inject ++ bouncy ++ scalatest ++ akka
  ) ++ Seq(
    test in assembly := {},
    jarName in assembly := "sorrydude_server.jar",
    mergeStrategy in assembly <<= (mergeStrategy in assembly) {
      (old) => {
        /* case x if x startsWith "org\\eclipse\\jetty" =>
        println(x)
        MergeStrategy.first*/
        case "rootdoc.txt" =>
          //old(x
          MergeStrategy.last
        case x =>
          old(x)
      }
    }
  )) dependsOn(sorryDudeProtoMessages, webSocketsConnector, database, services)

  lazy val sorryDudeProtoMessages = Project(id = "SorryDude-Proto", base = file("sorrydude-proto"), settings = Project.defaultSettings ++ PB.protobufSettings ++ Seq(
    scalaVersion := "2.10.3"
  ) ++ Seq(
    sourceDirectory in PB.protobufConfig := new java.io.File("./messages/sorrydude/"),
    javaSource in PB.protobufConfig <<= (sourceDirectory in Compile)(_ / "sorrydude-proto/src/main/java")
  ))


  //========================================================= [Card game] =========================================================

  lazy val cardGameProjectName = "FirstEnchanter"

  lazy val cardGameRootProject = Project(id = cardGameProjectName + "-Server", base = file(cardGameProjectName + "-server"), settings = Project.defaultSettings ++ assemblySettings ++ Seq(
    name := cardGameProjectName,
    version := "v0.1a",
    scalaVersion := "2.10.3",
    libraryDependencies ++= commons ++ commonsIo ++ inject ++ bouncy ++ scalatest ++ akka ++ akkaTestkit ++ httpClient ++ json ++ chatter
  ) ++ Seq(
    test in assembly := {},
    jarName in assembly := cardGameProjectName + "_server.jar",
    mergeStrategy in assembly <<= (mergeStrategy in assembly) {
      (old) => {
        /* case x if x startsWith "org\\eclipse\\jetty" =>
        println(x)
        MergeStrategy.first*/
        case "rootdoc.txt" =>
          //old(x
          MergeStrategy.last
        case x =>
          old(x)
      }
    }
  )) dependsOn(cardGameProtoMessages, webSocketsConnector, database, services, cluster, smtp, tokenizer, utils)

  lazy val cardGameTestClusterProject = Project(id = cardGameProjectName + "-testCluster", base = file(cardGameProjectName + "-test-cluster"), settings = Project.defaultSettings ++ Seq(
    name := cardGameProjectName + "testCluster",
    version := "v0.1a",
    scalaVersion := "2.10.3",
    libraryDependencies ++= commons ++ commonsIo ++ inject ++ bouncy ++ scalatest ++ akka ++ httpClient
  )) dependsOn(webSocketsConnector, database, services, cluster, cardGameRootProject)


  lazy val cardGameProtoMessages = Project(id = cardGameProjectName + "-Proto", base = file(cardGameProjectName + "-proto"), settings = Project.defaultSettings ++ PB.protobufSettings ++ Seq(
    scalaVersion := "2.10.3"
  ) ++ Seq(
    sourceDirectory in PB.protobufConfig := new java.io.File("./messages/" + cardGameProjectName + "/"),
    javaSource in PB.protobufConfig <<= (sourceDirectory in Compile)(_ / "java"), //(_ / {cardGameProjectName + "-proto/src/main/java"}),
    version in PB.protobufConfig := "2.5.0"
  ))

  lazy val cardGameAdminWeb = Project(id = cardGameProjectName + "-adminweb", base = file(cardGameProjectName + "-adminweb"), settings = Project.defaultSettings ++ WP.webSettings ++ assemblySettings ++ Seq(
    scalaVersion := "2.10.3",
    libraryDependencies ++= {
      val liftVersion = "2.5.1"
      Seq(
        "net.liftweb" %% "lift-webkit" % liftVersion % "compile",
        "org.eclipse.jetty" % "jetty-webapp" % "8.1.7.v20120910" % "container,test,compile",
        "org.eclipse.jetty.orbit" % "javax.servlet" % "3.0.0.v201112011016" % "container,compile" artifacts Artifact("javax.servlet", "jar", "jar")
      )
    },
    libraryDependencies ++= commons ++ commonsIo ++ inject ++ bouncy ++ scalatest ++ akka
  )) dependsOn (cluster)


  lazy val generateCards = Project(id = cardGameProjectName + "-print", base = file(cardGameProjectName + "-print"), settings = Project.defaultSettings ++ Seq(
    scalaVersion := "2.10.3",
    libraryDependencies ++= commons ++ commonsIo ++ inject ++ bouncy ++ scalatest ++ akka ++ generatePdf
  )) dependsOn(database, utils)


  lazy val aiTest = Project(id = cardGameProjectName + "-aiTest", base = file(cardGameProjectName + "-aiTest"), settings = Project.defaultSettings ++ Seq(
    scalaVersion := "2.10.3",
    libraryDependencies ++= commons ++ commonsIo ++ inject ++ bouncy ++ scalatest ++ akka
  )) dependsOn (cardGameRootProject)


  val javaHome = if (System.getenv("JAVA_HOME") == null) {
    System.getProperty("JAVA_HOME")
  } else {
    System.getenv("JAVA_HOME")
  }

  println(javaHome)

  lazy val gameTest = Project(id = cardGameProjectName + "-gameTest", base = file(cardGameProjectName + "-gameTest"), settings = Project.defaultSettings ++ Seq(
    scalaVersion := "2.10.3",
    libraryDependencies ++= commons ++ commonsIo ++ inject ++ bouncy ++ scalatest ++ akka ++ scalaFx ++ json,
    unmanagedJars in Compile += Attributed.blank(file(javaHome + "/jre/lib/jfxrt.jar")),
    fork := false
  )) dependsOn (cardGameRootProject)

  lazy val lightweightServer = Project(id = cardGameProjectName + "-lightweightServer", base = file(cardGameProjectName + "-lightweightServer"), settings = Project.defaultSettings ++ Seq(
    scalaVersion := "2.10.3",
    libraryDependencies ++= commons ++ commonsIo ++ inject ++ bouncy ++ scalatest ++ akka
  )) dependsOn (cardGameRootProject)

  lazy val localPlay = Project(id = cardGameProjectName + "-localPlay", base = file(cardGameProjectName + "-localPlay"), settings = Project.defaultSettings ++ Seq(
    scalaVersion := "2.10.3",
    libraryDependencies ++= commons ++ commonsIo ++ inject ++ bouncy ++ scalatest ++ akka
  )) dependsOn(gameTest, lightweightServer)

  lazy val cardDefinitionGenerator = Project(id = cardGameProjectName + "-cardDefinitionGenerator", base = file(cardGameProjectName + "-cardDefinitionGenerator"), settings = Project.defaultSettings ++ Seq(
    scalaVersion := "2.10.3",
    libraryDependencies ++= commons ++ commonsIo ++ inject ++ bouncy ++ scalatest ++ akka ++ casbah ++ json
  )) dependsOn(cardGameRootProject)


  //lazy val root = (project in file(".")).enablePlugins(CardGeneratorPlugin)

  lazy val compileAndCopyResources = task {
    import sbt.Process._
    "cp -R src/main/resources/* main/scala-2.10/test-classes"
  }

}
