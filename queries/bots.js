function Bot(avatarId, name, challengeId) {
  this.avatarId = avatarId
  this.name = name
  this.challengeId = challengeId

    this.sync = function sync(db) {
    db.botDetails.update(
    {"botName" : this.name},
     {"$set" : {"botName" : this.name, "avatarId" : NumberInt(this.avatarId), "challengeId" : NumberInt(this.challengeId)}},
     true
     )
  }
}
