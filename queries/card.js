
function Card(name, version, description, attack, defense, cost, tags, imageId, rarity, targetingType) {
    this.name = name
    this.version = version
    this.description = description
    this.attack = attack
    this.defense = defense
    this.cost = cost
    this.tags = tags
    this.imageId = imageId
    this.rarity = rarity
    this.targetingType = targetingType //None, MyTeam, EnemyTeam, AllPlayers, AllyCards, EnemyCards, AllCards, All

    this.sync = function sync (db) {
     var object = {
                                "_id" : "cz.bataliongames.firstenchanter.cards." + this.name.replace(/ /g, ""),
                                "name" : this.name,
                                "description" : [
                                  {"locale" : defaultLocale , "description" : this.description}
                                 ],
                                 "attack" : NumberInt(this.attack),
                                 "defense" : NumberInt(this.defense),
                                 "version" : this.version,
                                 "rarity" : NumberInt(this.rarity), //1,2,3,4..
                                 "price" : [
                                   {"element" : NumberInt(0), "price" : NumberInt(this.cost.water)}, //water
                                   {"element" : NumberInt(1), "price" : NumberInt(this.cost.fire)}, //fire
                                   {"element" : NumberInt(2), "price" : NumberInt(this.cost.air)}, //air
                                   {"element" : NumberInt(3), "price" : NumberInt(this.cost.earth)},   //earth
                                   {"element" : NumberInt(4), "price" : NumberInt(this.cost.random)}
                                 ],
                                 "tags"  : this.tags,
                                 "imageId" : NumberInt(this.imageId),
                                 "targetingType" : this.targetingType
                           }

        var size = db.cards.find({"name" : this.name}).size()

        if(size > 0) {
            db.cards.update({"name" : this.name}, object)
        } else {
            db.cards.save(object)
        }

        return  db.cards.find({"name" : this.name})[0]
    }
}