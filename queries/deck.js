function ElementsRating(water, fire, air, earth) {
    this.water = water
    this.fire = fire
    this.air = air
    this.earth = earth
}

function Deck(name, iconId, cards, rating, startingDeck) {
    this.name = name
    this.iconId = iconId
    this.cards = cards
    this.startingDeck = startingDeck
    this.rating = rating

    this.sync = function sync (db) {
              var object = {
                "iconId" : NumberInt(iconId),
                "elementsRating" : {
                    "fire"  : rating.fire,
                    "earth" : rating.earth,
                    "water" : rating.water,
                    "air"   : rating.air
                },
                "name" :    name,
                "startingDeck" : startingDeck,
                "cards" : cards
              }

              var size = db.decks.find({"name" : this.name}).size()

              if(size > 0) {
                  db.decks.update({"name" : this.name}, object)
              } else {
                  db.decks.save(object)
              }

              return  db.decks.find({"name" : this.name})
    }
}