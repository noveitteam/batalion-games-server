load('queries/elementalCost.js')
load('queries/card.js')
load('queries/deck.js')
load('queries/utils.js')
load('queries/bots.js')

var defaultLocale = "en-US"
var connection = new Mongo();
var db = connection.getDB("FirstEnchanter");
//Targeting type: None, MyTeam, EnemyTeam, AllPlayers, AllyCards, EnemyCards, AllCards, All, AllEnemy, AllAlly
var traitor = new Card("Traitor", "v1.0",  "Converts attacking enemy unit to your side, damaging his master instead and execution follows.", 0, 0, new RandomCost(1), ["Reaction", "Harmful", "Spell", "Traitor"], 0, 0, "EnemyCards").sync(db)
var reinforces = new Card("Reinforces", "v1.0", "You get 4 cards.",  0, 0, new RandomCost(4), ["Spell", "Buff"], 0, 0, "None").sync(db)
var shieldingBuff = new Card("Blessing of Defender", "v1.0", "You get +2 defense.", 0, 2, new RandomCost(2), ["Reaction", "Spell", "Buff", "AOE", "Defense increase"], 0,0, "AllyCards").sync(db)
var attackingBuff = new Card("Blessing of Attacker", "v1.0", "You get +2 attack", 2, 0, new RandomCost(2), ["Reaction", "Spell", "Buff", "AOE",  "Attack increase"], 0,0, "AllyCards").sync(db)
var shield = new Card("Shield", "v1.0", "Adds + 2 defense to soldier type unit. \n Synergy with disarm. Usable on soldier type class.", 0, 2, new RandomCost(1), ["Equip", "Defense increase", "Soldier only", "Disarm synergy"], 0,0, "AllyCards").sync(db)
var enchantedShield = new Card("Enchanted shield", "v1.0", "Adds + 1 defense to your soldier type unit. When enchanter defends with unit while having this shield, unit steals 2 elements from attacker. \n Synergy with disarm. \n Usable on soldier type class.", 0, 1, new RandomCost(2), ["Equip", "Defense increase", "Soldier only", "Disarm synergy", "Steals elements while defending"], 0, 0, "AllyCards").sync(db)
var ring = new Card("Ring", "v1.0", "Adds +2 attack to mage type unit. Amplifies every elemental spell. Element is from mage.", 2, 0, new RandomCost(1), ["Equip", "Attack increase", "Mage only", "Minor elemental amplify"], 0, 0, "AllyCards").sync(db)
var disarm = new Card("Disarm", "v1.0", "Disarms equip from target unit, or remove 1 card from enemy enchanter. If you have bad fortune, you are losing your equip too.", 0, 0, new RandomCost(1), ["Skill", "Require shield"], 0, 0, "AllEnemy").sync(db)
var parry = new Card("Parry", "v1.0", "Parry attacking unit.", 0, 0, new RandomCost(0), ["Reaction", "Skill", "Require sword", "Cancel attacking unit"], 0, 0, "EnemyCards").sync(db)
var oneHandSword = new Card("One Hand Sword", "v1.0", "Adds +2 attack to archer, or soldier type unit. \n Synergy with parry.", 2, 0, new RandomCost(1), ["Equip", "Attack increase", "Archer or soldier only"], 0, 0, "AllyCards").sync(db)
var spellReflect = new Card("Spell react", "v1.0", "Reflects single target harmful spells.", 0, 0, new RandomCost(1), ["Spell", "Spell reflection"], 0, 0, "EnemyCards").sync(db)
var elementalSynchronization = new Card("Elemental Synchronization", "v1.0", "Summon 4 cards and 4 elements. Require having elemental type creep on table.", 0,0, new RandomCost(0), ["Spell", "Require elemental"], 0, 0, "None").sync(db)
var sacrifice = new Card("Sacrifice", "v1.0", "Kills all attacking unit", 0, 0, new RandomCost(5), ["Reaction", "Spell", "Ultimate", "Sacrifice", "Aoe", "All attacking unit"], 0, 0, "None").sync(db)

var fireMage = new Card("Fire Mage", "v1.0", "Basic mage class unit", 3,2, new FireCost(2), ["Creep", "Mage", "Fire element", "Magic Boost"], 0, 0, "None").sync(db)
var fireSoldier = new Card("Fire Soldier", "v1.0", "Basic soldier class unit", 2, 4, new FireCost(2), ["Creep", "Soldier", "Fire element"], 0,0, "None").sync(db)
var fireArcher = new Card("Fire Archer", "v1.0", "Basic archer class unit", 4, 2, new FireCost(2), ["Creep", "Archer", "Fire element"] , 0, 0, "None").sync(db)
var fireElemental = new Card("Fire Golem", "v1.0", "Basic elemental class unit. Adds +1 to fire damage.", 1, 4, new FireCost(1), ["Creep", "Elemental", "Fire element", "Slight elemental amplify"], 0, 0, "None").sync(db)
var fireball = new Card("Fireball", "v1.0", "Deals 3 spell damage.  Can by amplified by elemental amplify", 3, 0, new FireCost(1), ["Harmful", "Spell", "Fire element", "Amplified able"], 0, 0, "AllEnemy").sync(db)
var rageOfFire = new Card("Rage of Fire", "v1.0", "Destroys all cards on table for both teams.", 1000, 0, new FireCost(5), ["Harmful", "Spell", "AOE", "Both teams", "Fire element"], 0, 0, "None").sync(db)
var fireTotem  = new Card("Fire Totem", "v1.0", "Absorbs 1 single target harmful spells", 0, 1, new FireCost(1), ["Totem", "Spell absorption"], 0, 0, "None").sync(db)
var searingTouch = new Card("Searing Touch", "v1.0", "Recovers 4 hp.", 4, 0, new FireCost(1), ["Spell", "Healing", "Amplified able"], 0, 0, "MyTeam").sync(db)

var earthMage = new Card("Earth Mage", "v1.0", "Basic mage class unit", 3,2, new EarthCost(2), ["Creep", "Mage", "Earth element", "Magic Boost"], 0, 0, "None").sync(db)
var earthSoldier = new Card("Earth Soldier", "v1.0", "Basic soldier class unit", 2, 4, new EarthCost(2), ["Creep", "Soldier", "Earth element"], 0,0, "None").sync(db)
var earthArcher = new Card("Earth Archer", "v1.0", "Basic archer class unit", 4, 2, new EarthCost(2), ["Creep", "Archer", "Earth element"] , 0, 0, "None").sync(db)
var earthElemental = new Card("Earth Golem", "v1.0", "Basic elemental class unit. Adds +1 to fire damage.", 1, 4, new EarthCost(1), ["Creep", "Elemental", "Earth element", "Slight elemental amplify"], 0, 0, "None").sync(db)
var rageOfEarth = new Card("Rage of Earth", "v1.0", "Destroys all cards on table for both teams.", 1000, 0, new EarthCost(5), ["Harmful", "Spell", "AOE", "Both teams", "Earth element"], 0, 0, "None").sync(db)
var earthTotem  = new Card("Earth Totem", "v1.0", "Absorbs 1 single target harmful spells", 0, 1, new EarthCost(1), ["Totem", "Spell absorption"], 0, 0, "None").sync(db)
var earthFall = new Card("Earth fall", "v1.0", "Deals 3 spell damage.  Can by amplified by elemental amplify", 3, 0, new EarthCost(1), ["Harmful", "Spell", "Earth element", "Amplified able"], 0, 0, "AllEnemy").sync(db)
var hardTouch = new Card("Hard Touch", "v1.0", "Recovers 4 hp.", 4, 0, new EarthCost(1), ["Spell", "Healing", "Amplified able"], 0, 0, "MyTeam").sync(db)

var airMage = new Card("Air Mage", "v1.0", "Basic mage class unit", 3,2, new AirCost(2), ["Creep", "Mage", "Air element", "Magic Boost"], 0, 0, "None").sync(db)
var airSoldier = new Card("Air Soldier", "v1.0", "Basic soldier class unit", 2, 4, new AirCost(2), ["Creep", "Soldier", "Air element"], 0,0, "None").sync(db)
var airArcher = new Card("Air Archer", "v1.0", "Basic archer class unit", 4, 2, new AirCost(2), ["Creep", "Archer", "Air element"] , 0, 0, "None").sync(db)
var airElemental = new Card("Air Golem", "v1.0", "Basic elemental class unit. Adds +1 to fire damage.", 1, 4, new AirCost(1), ["Creep", "Elemental", "Air element", "Slight elemental amplify"], 0, 0, "None").sync(db)
var rageOfAir = new Card("Rage of Air", "v1.0", "Destroys all cards on table for both teams.", 1000, 0, new AirCost(5), ["Harmful", "Spell", "AOE", "Both teams", "Air element"], 0, 0, "None").sync(db)
var airTotem  = new Card("Air Totem", "v1.0", "Absorbs 1 single target harmful spells", 0, 1, new AirCost(1), ["Totem", "Spell absorption"], 0, 0, "None").sync(db)
var lightingBolt = new Card("Lighting Bolt", "v1.0", "Deals 3 spell damage.  Can by amplified by elemental amplify", 3, 0, new AirCost(1), ["Harmful", "Spell", "Air element", "Amplified able"], 0, 0, "AllEnemy").sync(db)
var lightingTouch = new Card("Lighting Touch", "v1.0", "Recovers 4 hp.", 4, 0, new AirCost(1), ["Spell", "Healing", "Amplified able"], 0, 0, "MyTeam").sync(db)

var waterMage = new Card("Water Mage", "v1.0", "Basic mage class unit", 3,2, new WaterCost(2), ["Creep", "Mage", "Water element", "Magic Boost"], 0, 0, "None").sync(db)
var waterSoldier = new Card("Water Soldier", "v1.0", "Basic soldier class unit", 2, 4, new WaterCost(2), ["Creep", "Soldier", "Water element"], 0,0, "None").sync(db)
var waterArcher = new Card("Water Archer", "v1.0", "Basic archer class unit", 4, 2, new WaterCost(2), ["Creep", "Archer", "Water element"] , 0, 0, "None").sync(db)
var waterElemental = new Card("Water Golem", "v1.0", "Basic elemental class unit. Adds +1 to fire damage.", 1, 4, new WaterCost(1), ["Creep", "Elemental", "Water element", "Slight elemental amplify"], 0, 0, "None").sync(db)
var rageOfWater = new Card("Rage of Water", "v1.0", "Destroys all cards on table for both teams.", 1000, 0, new WaterCost(5), ["Harmful", "Spell", "AOE", "Both teams", "Fire element"], 0, 0, "None").sync(db)
var waterTotem  = new Card("Water Totem", "v1.0", "Absorbs 1 single target harmful spells", 0, 1, new WaterCost(1), ["Totem", "Spell absorption"], 0, 0, "None").sync(db)
var waterBall = new Card("Water Ball", "v1.0", "Deals 3 spell damage.  Can by amplified by elemental amplify", 3, 0, new WaterCost(1), ["Harmful", "Spell", "Water element", "Amplified able"], 0, 0, "AllEnemy").sync(db)
var calmingTouch = new Card("Calming Touch", "v1.0", "Recovers 4 hp.", 4, 0, new WaterCost(1), ["Spell", "Healing", "Amplified able"], 0, 0, "MyTeam").sync(db)


var waterStartDeck = new Deck("Water deck", 2, [
 toDeckHasCardObject(shieldingBuff._id, NumberInt(2)),
 toDeckHasCardObject(attackingBuff._id, NumberInt(2)),
  // toDeckHasCardObject(shield._id, NumberInt(1)),
  //  toDeckHasCardObject(enchantedShield._id, NumberInt(3)),
  //  toDeckHasCardObject(ring._id, NumberInt(4)),
  //  toDeckHasCardObject(oneHandSword._id, NumberInt(4)),
    toDeckHasCardObject(waterMage._id, NumberInt(4)),
 //   toDeckHasCardObject(waterSoldier._id, NumberInt(4)),
  //  toDeckHasCardObject(waterArcher._id, NumberInt(4)),
 //   toDeckHasCardObject(waterElemental._id, NumberInt(4)),
 //   toDeckHasCardObject(waterBall._id, NumberInt(3)),
  //  toDeckHasCardObject(disarm._id, NumberInt(3)),
  //  toDeckHasCardObject(parry._id, NumberInt(3)),
 //   toDeckHasCardObject(spellReflect._id, NumberInt(3)),
 //   toDeckHasCardObject(waterTotem._id, NumberInt(3)),
 //   toDeckHasCardObject(calmingTouch._id, NumberInt(3))
] , new ElementsRating(1, 0, 0,0), true).sync(db)

var fireStartDeck = new Deck("Fire deck", 1, [
 toDeckHasCardObject(shieldingBuff._id, NumberInt(2)),
 toDeckHasCardObject(attackingBuff._id, NumberInt(2)),
  //  toDeckHasCardObject(shield._id, NumberInt(1)),
  //  toDeckHasCardObject(enchantedShield._id, NumberInt(3)),
  //  toDeckHasCardObject(ring._id, NumberInt(4)),
  //  toDeckHasCardObject(oneHandSword._id, NumberInt(4)),
    toDeckHasCardObject(fireMage._id, NumberInt(4)),
   // toDeckHasCardObject(fireSoldier._id, NumberInt(4)),
  //  toDeckHasCardObject(fireArcher._id, NumberInt(4)),
//   toDeckHasCardObject(fireElemental._id, NumberInt(4)),
  //  toDeckHasCardObject(fireball._id, NumberInt(3)),
  //  toDeckHasCardObject(disarm._id, NumberInt(3)),
   // toDeckHasCardObject(parry._id, NumberInt(3)),
   // toDeckHasCardObject(spellReflect._id, NumberInt(3)),
  //  toDeckHasCardObject(fireTotem._id, NumberInt(3)),
  //  toDeckHasCardObject(searingTouch._id, NumberInt(3))
], new ElementsRating(0, 1, 0,0), true).sync(db)

var airStartDeck = new Deck("Air deck", 4, [
 toDeckHasCardObject(shieldingBuff._id, NumberInt(2)),
 toDeckHasCardObject(attackingBuff._id, NumberInt(2)),
 //   toDeckHasCardObject(shield._id, NumberInt(1)),
 //   toDeckHasCardObject(enchantedShield._id, NumberInt(3)),
 //   toDeckHasCardObject(ring._id, NumberInt(4)),
 //   toDeckHasCardObject(oneHandSword._id, NumberInt(4)),
    toDeckHasCardObject(airMage._id, NumberInt(4)),
 //   toDeckHasCardObject(airSoldier._id, NumberInt(4)),
 //   toDeckHasCardObject(airArcher._id, NumberInt(4)),
 //   toDeckHasCardObject(airElemental._id, NumberInt(4)),
 //  toDeckHasCardObject(lightingBolt._id, NumberInt(3)),
//    toDeckHasCardObject(disarm._id, NumberInt(3)),
  //  toDeckHasCardObject(parry._id, NumberInt(3)),
 //   toDeckHasCardObject(spellReflect._id, NumberInt(3)),
//    toDeckHasCardObject(airTotem._id, NumberInt(3)),
//    toDeckHasCardObject(lightingTouch._id, NumberInt(3))
] , new ElementsRating(0, 0, 1,0), true).sync(db)

var earthStartDeck = new Deck("Earth deck", 3, [
 toDeckHasCardObject(shieldingBuff._id, NumberInt(2)),
 toDeckHasCardObject(attackingBuff._id, NumberInt(2)),
 //   toDeckHasCardObject(shield._id, NumberInt(1)),
//    toDeckHasCardObject(enchantedShield._id, NumberInt(3)),
 //   toDeckHasCardObject(ring._id, NumberInt(4)),
     //toDeckHasCardObject(oneHandSword._id, NumberInt(4)),
    toDeckHasCardObject(earthMage._id, NumberInt(4)),
 //  toDeckHasCardObject(earthSoldier._id, NumberInt(4)),
 //   toDeckHasCardObject(earthArcher._id, NumberInt(4)),
 //  toDeckHasCardObject(earthElemental._id, NumberInt(4)),
//    toDeckHasCardObject(earthFall._id, NumberInt(3)),
  //  toDeckHasCardObject(disarm._id, NumberInt(3)),
 //   toDeckHasCardObject(parry._id, NumberInt(3)),
 //   toDeckHasCardObject(spellReflect._id, NumberInt(3)),
 //   toDeckHasCardObject(earthTotem._id, NumberInt(3)),
//    toDeckHasCardObject(hardTouch._id, NumberInt(3))
] , new ElementsRating(0, 0, 0, 1), true).sync(db)

var anakyn = new Bot(0, "Anakyn Skywalker", 0)
var luke = new Bot(0, "Luke Skywalker", 0)

var anakynsDeck = db.decks.findOne({"name" : "Water deck"})
anakynsDeck.user = anakyn.name
delete anakynsDeck._id

var lukesDeck = db.decks.findOne({"name" : "Water deck"})
lukesDeck.user = luke.name
delete lukesDeck._id

db.botDetails.drop()

db.decks.remove({"user" : anakyn.name})
db.decks.remove({"user" : luke.name})

anakyn.sync(db)
luke.sync(db)

db.decks.save(anakynsDeck)
db.decks.save(lukesDeck)