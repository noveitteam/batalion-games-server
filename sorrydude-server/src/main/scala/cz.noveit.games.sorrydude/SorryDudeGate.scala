package cz.noveit.games.sorrydude

import akka.actor.{ActorRef, Props, ActorSystem}
import cz.noveit.connector.websockets.{StopWebSocketServer, StartWebSocketServer, WebSocketsServer}
import cz.noveit.games.sorrydude.services.NewsService
import cz.noveit.proto.Messages.MessagesPackage.Message
import cz.noveit.service.{ServiceRegistration, ProtoServiceRegistry, RegisterServiceMessage, Service}
import cz.noveit.proto.sorrydude.news.SorryDudeNews.GetLastNews
;

/**
 * Created with IntelliJ IDEA.
 * User: Wlsek
 * Date: 20.5.13
 * Time: 15:59
 */

object SorryDudeGate extends App with ServiceRegistration {

  override def main(args: Array[String]): Unit = {
    val actorSystem = ActorSystem("SorryDude")

    val webSocketsServer = actorSystem actorOf Props[WebSocketsServer]
    val registry = actorSystem actorOf Props[ProtoServiceRegistry]

    val newService = actorSystem actorOf(Props(classOf[NewsService], "SorryDudeNews"))
    <--[GetLastNews](newService, registry)

    webSocketsServer ! StartWebSocketServer("Sorry dude websocket server", registry)

    Console.readLine()

    webSocketsServer ! StopWebSocketServer

    actorSystem.shutdown()
  }


}
