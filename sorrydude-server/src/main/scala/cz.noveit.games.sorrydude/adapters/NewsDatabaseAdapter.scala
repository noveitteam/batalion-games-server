package cz.noveit.games.sorrydude.adapters

import akka.actor.{Props, Actor, ActorLogging}
import cz.noveit.database._
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import scala.concurrent.Await
import com.mongodb.casbah.Imports._

/**
 * Created by Wlsek on 6.1.14.
 */

case class News(title: String, description: String, dateTime: Long, newsId: String)

sealed trait NewsDatabaseMessages extends DatabaseAdapterMessage

case class GetNews(skip: Int, size: Int, localization: Option[String]) extends DatabaseCommandMessage with NewsDatabaseMessages

case class ListOfNews(news: List[News]) extends DatabaseCommandResultMessage with NewsDatabaseMessages

class AuthorizationDatabaseAdapter extends Actor with ActorLogging {

  val databaseController = context actorOf (Props[DatabaseController])

  def receive = {
    case c: DatabaseCommand =>
      c.command match {
        case m: GetNews =>
          val replyTo = sender

          implicit val timeout = Timeout(5 seconds)
          val future = databaseController ? GetCollection("SorryDude", "News")
          val collection = Await.result(future, timeout.duration).asInstanceOf[MongoCollection]

          val result = m.localization.map(
            l => collection.find(MongoDBObject("localization" -> l)).skip(m.skip).limit(m.size)
          ).getOrElse(
              collection.find(MongoDBObject("localization" -> "en-US")).skip(m.skip).limit(m.size)
            )

          replyTo ! DatabaseResult(
            ListOfNews(
              result.map(
                  row => News(
                    row.getAs[String]("title").get,
                    row.getAs[String]("description").get,
                    row.getAs[Long]("created").get,
                    row.getAs[ObjectId]("_id").get.toString())
                ).toList
            ),
            c.replyHash
          )

      }
    case t => throw new UnsupportedOperationException(t.toString)
  }
}
