package cz.noveit.games.sorrydude.services

import akka.actor.{Props, Actor, ActorLogging}
import cz.noveit.proto.serialization.{MessageSerializer, MessageEvent}
import cz.noveit.games.sorrydude.adapters.{ListOfNews, GetNews, NewsDatabaseAdapter}
import cz.noveit.connector.websockets.WebSocketsContext
import cz.noveit.service.{ContextDestroy, ServiceContext, Service}
import cz.noveit.database.{DatabaseCommand, DatabaseResult, DatabaseCommandResultMessage}
import sun.misc.BASE64Encoder
import cz.noveit.proto.sorrydude.news.SorryDudeNews.{NewsList, GetLastNews}
import cz.noveit.proto.sorrydude.news.SorryDudeNews.NewsList.Article
import org.bouncycastle.util.encoders.Base64


/**
 * Created by Wlsek on 6.1.14.
 */
class NewsService(module: String) extends Service {
  val moduleName = module;

  def receive = {
    case e: MessageEvent =>
      e.context.map(messageContext => {
           getServiceContext(messageContext)
             .map(serviceContext => serviceContext)
             .getOrElse({
             val serviceContext = context.actorOf(Props(classOf[NewsServiceContext], messageContext.asInstanceOf[WebSocketsContext], moduleName))
             registerContext(messageContext, serviceContext)
             serviceContext
           }) ! e
      })

    case ds: ContextDestroy =>
      removeContext(ds.context)
        .map(a => context.stop(a))

    case t => throw new UnsupportedOperationException(t.toString())
  }
}

class NewsServiceContext(socketContext: WebSocketsContext, module: String) extends ServiceContext with MessageSerializer {
  lazy val newsDatabaseAdapter = context.actorOf(Props[NewsDatabaseAdapter])
  val moduleName = module;

  def receive = {
    case e: MessageEvent =>
      e.message match {
        case m: GetLastNews =>
          if (m.hasLocalization) {
            newsDatabaseAdapter ! DatabaseCommand(GetNews(m.getSkip, m.getLimit, Some(m.getLocalization)), e.replyHash)
          } else {
            newsDatabaseAdapter ! DatabaseCommand(GetNews(m.getSkip, m.getLimit, None), e.replyHash)
          }

        case t => throw new UnsupportedOperationException(t.toString())
      }

    case r: DatabaseResult =>
      r.result match {
        case m: ListOfNews =>
          val listBuilder = NewsList.newBuilder()
          m.news.foreach(n => {
            listBuilder.addListOfArticles(
              Article.newBuilder().setDescription(n.description).setNewsId(n.newsId).setTimeStamp(n.dateTime).setTitle(n.title).build()
            )
          })

          val encodedString = new String(Base64.encode(serializeToMessage(MessageEvent(listBuilder.build(),moduleName, r.replyHash)).toByteArray), "UTF-8")
          socketContext.send(encodedString)
        case t => throw new UnsupportedOperationException(t.toString())
      }

    case t => throw new UnsupportedOperationException(t.toString())
  }

 override def postStop = {
   context.stop(newsDatabaseAdapter)
 }
}